<?php
namespace App\Http\Helpers;
use Auth;
class helpers{

  public static function utf8D($cad){
    return utf8_decode($cad);
  }
  public function get_userType(){
    return (Auth::user()->superuser==1?'superuser':'admin');
  }
  public function get_country(){
    return Auth::user()->country;
  }
  public function getMonths($month){
    $mes =  ["1"=>"Enero","2"=>"Febrero",
            "3"=>"Marzo","4"=>"Abril",
            "5"=>"Mayo","6"=>"Junio","7"=>"Julio",
            "8"=>"Agosto","9"=>"Septiembre","10"=>"Octubre",
            "11"=>"Noviembre","12"=>"Diciembre"
          ];
    return $mes[$month];
  }
}
?>
