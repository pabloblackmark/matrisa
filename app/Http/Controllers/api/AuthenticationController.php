<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\apiModels\colaboradoresTokenModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Validation\ValidationException;
use App\adminModels\client_colab_assocModel;

class AuthenticationController extends Controller
{
  //this method adds new users
  public function createAccount(Request $request)
  {
     $attr = $request->validate([
         'name' => 'required',
         'password' => 'required|string|min:6|confirmed'
     ]);

     $user = colaboradoresTokenModel::create([
         'name' => $attr['name'],
         'password' => bcrypt($attr['password']),
         'email' => $attr['email']
     ]);

     return $this->success([
         'token' => $user->createToken('tokens')->plainTextToken
     ]);
  }
   //use this method to signin users
  public function signin(Request $request)
  {
    $request->validate([
       'codeUser' => 'required',
       'password' => 'required'
   ]);

   $user = colaboradoresTokenModel::where('id', $request->codeUser)->first();
   // dd(Hash::check($request->password, $user->password));

   if (! $user || ! Hash::check($request->password, $user->password)) {
     return response()->json([
     'message' => 'Invalid login details'
                ], 401);
    }
    // $workIng=client_colab_assocModel::get();
    // $clients = $shedules = [];
    // $days = $this->daysEsp();
    // $days = $days[date('l')];
    // foreach($workIng AS $valus){
    // $shed =$valus->getJobs()->first();
    // if(!empty($shed)){
    //   $horarios = $shed->getShedules()->where("day",$days)->get();
    //   // dd($horarios);
    //   foreach($horarios AS $vlone){
    //       $shedules[] = ["entry"=>$vlone->entry,"output"=>$vlone->exit];
    //   }
    // }
    // $clients[]=["idL"=> $valus->id,"name"=> $valus->getClientInfo()->first()->getClient()->first()->clientName,"shedule"=>$shedules];
    // }
   return response()->json([
              'access_token' => $user->createToken('API Token')->plainTextToken,
              // 'companies'=>$clients,
              'token_type' => 'Bearer',
              'idUser' =>$request->codeUser,
              'name' =>$user->nombres.' '.$user->apellidos,

   ]);
  }

   // this method signs out users by removing tokens
  public function signout()
  {
     auth()->user()->tokens()->delete();

     return [
         'message' => 'Tokens Revoked'
     ];
  }

}
