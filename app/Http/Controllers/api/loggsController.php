<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\apiModels\loggsModel;
use Carbon\Carbon;
use App\adminModels\client_colab_assocModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsAssocModel;
use App\adminModels\company_colab_assocModel;
use App\adminModels\horaryModel;
use App\adminModels\petitionsModel;
class loggsController extends Controller
{
    public function __construct()
    {
       $this->middleware('cors');
    }
    public function index()
    {
      return response()->json([
          "status" => true,
          "data" =>[],
        ], 200);
    }
    public function store(Request $request)
    {
      $hourMin =$hourMinTmp = (isset($request->hour)?$request->hour:date("H:i:s"));
      $dateDAY = (!empty($request->dateTest)?date("Y-m-d",strtotime($request->dateTest)):date("Y-m-d"));
      // dd($hourMinTmp);
      // dd($request);
      // $hourMin = '08:00:00';
      // $hourMin =$hourMinTmp= '15:15:00';
      // $hourMin =$hourMinTmp= '16:45:00';
      $hour=$hourTmp=$hourTmp2 = $this->hourSeconds($hourMin);
      // date_default_timezone_set('America/Guatemala');
      // setlocale(LC_ALL, 'es_GT');
       $workIng=client_colab_assocModel::where(["idColab"=>auth()->user()->id,"status"=>1])->get();
       $clients = $shedules = [];
       $clientID = $howEnterBefH =0;
       $chekBefar = $timeBefEnter = '00:00:00';
       $idLInks = $idUnique = $idHorar = null;
       $enterBefore = 'false';
       $days = $this->daysEsp();
       $days = $days[date('l')];
       $days = (isset($request->day)?$request->day:$days);
       $betw = 'false';
       $chekBefTxt = $enterBefore = $hasWork = 'false';
       $logsMo = null;
       $hourcalExit = $hourMin;

       ////**************COMPROBACION horario intermedio****************////
       if(isset($request->idFollow)){
         $logsMo = loggsModel::find($request->idFollow);
         // dd( $logsMo->dateDay,$dateDAY);
         $getShedules = horaryModel::find($logsMo->idDay);
         $hourBefExit = $this->bettwenMin($hourMin ,$getShedules->exit);
         if(!empty($hourBefExit)&&$logsMo->dateDay==$dateDAY){
           // dd(!empty($hourBefExit),$logsMo->dateDay==$dateDAY);
           $hourBefExitSec = $this->hourSeconds($hourBefExit);
           $betw = ($hourBefExitSec<=3600&&$request->typo=='salida'||
                    empty($logsMo->hourExitBetw)&&$hourBefExitSec<=3600
                    &&$request->typo=='entrada'?'true':'false');
         }else if(empty($hourBefExit)||!empty($hourBefExit)&&$logsMo->dateDay!=$dateDAY){
           $betw = 'true';
         }
       }
       // dd($betw);
       ////**************FINALIZA horario intermedio****************////
       $companyId = null;
       if(strtolower($request->typo)=='entrada'&&empty($logsMo))
       {
         // dd($workIng);
         foreach($workIng AS $valus){
           $shed = $valus->getJobs()->first();
           // dd($shed);
           if(!empty($shed)){
             // $days
             $horarios = $shed->getShedules()->where("day",$days)->get();
             // dd($horarios,$days);
             foreach($horarios AS $vlone){
                 $afterTime = $this->bettwenMin($hourMinTmp,$vlone->entry);
                 // dd($hourMinTmp,$vlone->entry, $afterTime);
                  if($afterTime!==false&&$this->hourSeconds($afterTime)>0)
                  {
                    //$hourTmp =$this->hourSeconds($vlone->entry); //si entra antes de la hora establece la hora inicial
                    // $hourMinTmp = $vlone->entry;
                    // $chekBefar =  $chekBef;
                    $chekBefTxt = 'true';

                    $limit = $this->hourSeconds('00:30:00');
                    // $hourCalc = $vlone->enter;
                    $minHourEnter = $this->hourSeconds($vlone->entry);
                    $limitCalc = $minHourEnter - $limit;
                    $entrada = $this->hourSeconds($hourMin);
                    if($entrada<=$limitCalc){
                      // $timeBefEnter = $this->bettwenMin($hourMin,$vlone->entry);
                      $secsBetw = $this->hourSeconds($afterTime);
                      $howEnterBefH = floor($secsBetw / 1800);
                    }else{
                      $chekBef = '00:00:00';
                    }
                  }
                  else if($afterTime===false)
                  {
                    $afterTime = '00:00:00';
                  }
                  // dd( $afterTime);
                  // dd($vlone);
                  // dd($horarios,$afterTime);
                  // dd($vlone->entry,$hourMin,$limit,$minHourEnter,$limitCalc,$entrada,$howEnterBefH,$timeBefEnter);
                  // if($hourTmp>=$this->hourSeconds($vlone->entry)&&$hourTmp<=$this->hourSeconds($vlone->exit))
                  // {
                   $clientInfo = $valus->getClientInfo()->first()->getClient()->first();
                   // dd($clientInfo);
                   $clientID =$clientInfo->id;
                   $shedules = ["entry"=>$vlone->entry,"output"=>$vlone->exit];
                   $clients= [$vlone->id,$valus->idClient,$valus->idBranch,$valus->idArea,$valus->idPuesto];
                   $idUnique = auth()->user()->id.$vlone->id.$valus->idClient.$valus->idBranch.$valus->idArea.$valus->idPuesto;
                   $idLInks = $valus->id;
                   $idBranch = $valus->idBranch;
                   $idArea = $valus->idArea;
                   $idHorar = $vlone->id;
                   $companyId = $valus->getClientInfo()->first()->idCompany;
                   // dd($companyId);
                   $hasWork = true;
                   // break;
                 // }

               }

           }
         }

         // dd($shedules["entry"]);
         $ontime=0;
         $outime='00:00:00';
         $ontimeTxt=$outimeTxt='true';
           if($hasWork)
           {
             //si entra tarde
             $dateInterval = $this->bettwenMin($shedules["entry"],$hourMinTmp );
             // dd($shedules["entry"],$hourMinTmp,$dateInterval);
             // dd($shedules["entry"],$hourMinTmp,$vlone->entry,$dateInterval);
             if($dateInterval){
               $tosedc = $this->hourSeconds($dateInterval);
               if($tosedc>900){
                 $outime=$dateInterval;
                 $expl = explode(":",$dateInterval);
                 $ontimeTxt = ((INT)$expl[1]>1?'true':'false');
               }
             }
             // dd($shedules);
             // dd();
             $logMdl = new loggsModel;
             $logMdl->latitude = $request->latid;
             $logMdl->longitud = $request->longid;
             $logMdl->userId = auth()->user()->id;
             $logMdl->ontime = $ontimeTxt;
             $logMdl->idFollower = $idUnique;
             $logMdl->hourEnter = $hourMin;
             $logMdl->outTimeEnterTxt = $ontimeTxt;
             $logMdl->outTimeEnter = $outime;

             $logMdl->befTimeEnter = $hourMinTmp;

             $logMdl->extraHourDay = $afterTime;
             $logMdl->extraHourDayMiddle = $howEnterBefH;
             $logMdl->befTimeEnterTxt = $chekBefTxt;

             $logMdl->dayWeek = $days;
             $logMdl->dateDay = $dateDAY;
             $logMdl->idBranch = $idBranch;
             $logMdl->idClient = $clientID;
             $logMdl->idCompany = $companyId;
             $logMdl->idArea = $idArea;
             $logMdl->idLink = $idLInks;
             $logMdl->idDay = $idHorar;
             // dd($logMdl);
             $logMdl->save();
             $idEnter = $logMdl->id;
             $saveFollow = colaboradores::find(auth()->user()->id);
             $saveFollow->idWRegister = $logMdl->id;
             $saveFollow->save();
             return response()->json([
               "status" => true,
               "data"=>["idFollow"=>$idEnter],
               "message" => "Guardado"
             ], 200);
           }
           else
           {
             return response()->json([
               "status" => false,
               "message" => "No hay horarios de entrada"
             ], 404);
           }
       }
       else if(strtolower($request->typo)=='salida'&&!empty($logsMo)&&$betw!=='true')
       {
         // dd('Salida nueva');
         $logsMo->hourExitBetw = $hourMin;
         $logsMo->latitudeExitBtw = $request->latid;
         $logsMo->longitudExitBtw = $request->longid;
         $logsMo->save();
         return response()->json([
           "status" => true,
           "data"=>["idFollow"=>$request->idFollow],
           "message" => "Salida entre horario Guardado"
         ], 200);
         die();
       }
       else if(strtolower($request->typo)=='entrada'&&!empty($logsMo)&&$betw!=='true')
       {
         // dd('ss');
         $logsMo->hourEnterBetw = $hourMin;
         $logsMo->latitudeEnterBtw = $request->latid;
         $logsMo->longitudEnterBtw = $request->longid;
         $logsMo->save();
         return response()->json([
           "status" => true,
           "data"=>["idFollow"=>$request->idFollow],
           "message" => "Entrada entre horario Guardado"
         ], 200);
       }
       else if(strtolower($request->typo=='salida')&&!empty($logsMo))
       {
         // 356111
         // dd($hourMin);

         $exitBefore = false;
         // $logsMo = loggsModel::find($request->idFollow);
         if(!empty($logsMo)){
           $ontime=$howHours=$hourTm=$hourMinTmp=$exitTotal= 0;
           $extraT = $outime = $exitHour = '00:00:00';
           //tiempo trabajado desde que marco hasta el horario predefinido
           $horaSal = $hourCalc = $hourMin;
           $enterHour =$logsMo->hourEnter;
           $exitHTime = $this->bettwenMin($getShedules->exit,($hourMin== '00:00:00'?'24:00:00':$hourMin));


           $workDone = $this->bettwenMin($logsMo->hourEnter,($hourMin== '00:00:00'?'24:00:00':$hourMin));
           // dd($getShedules->exit,$hourMin);
           // dd($exitHTime,$workDone,$getShedules->exit,$hourMin);
           $workDoneSec = ($workDone==false?0:$this->hourSeconds($workDone));
           // dd($this->hourSeconds('00:00:00'));
           $horaTotalDay = $this->bettwenMin($enterHour,($hourMin== '00:00:00'?'24:00:00':$hourMin));
           // dd($horaSal);
           // dd($workDone, $horaTotalDay);
           if($exitHTime!==false)
           {
             $exitHSec = $this->hourSeconds($exitHTime);
             $limit = $this->hourSeconds('00:30:00');
             $hourCalc = $getShedules->exit;
             $horaSal = $this->hourSeconds($getShedules->exit);
             $limit = $horaSal + $limit;
             $salida = $this->hourSeconds($hourMin);
             if($salida>=$limit){
               $exitBefore = true;
               $howHours = floor($exitHSec / 1800);
             }
             // else{
             //   $exitHTime = '00:00:00';
             // }
           }
           else if($exitHTime===false)
           {
             // dd($horaSal.'sss');
             // dd($getShedules->exit);
             $horaSalTime = $this->hourSeconds($hourMin);
             // dd($logsMo->dateDay,$dateDAY,$horaSal);
             if($logsMo->dateDay!=$dateDAY&&$horaSalTime>0)
             {
               // dd($horaSal.'ss');
               $getHourNight = $this->bettwenMin($getShedules->exit,'25:00');
               $getHourDay = $this->bettwenMin('01:00',$hourMin);
               $getHourTot = $this->bettwenMin($logsMo->hourEnter,$getShedules->exit);
               // dd($this->hourSeconds($getHourTot),$this->hourSeconds($getHourNight),$this->hourSeconds($getHourDay));
               $hourcalExit = $this->hourSeconds($getHourTot)+$this->hourSeconds($getHourNight)+$this->hourSeconds($getHourDay);
               $horaTotalDay = $this->secondsHour($hourcalExit);
               // dd($hourcalExit);
             }
             else
             {
               $exitHTime = '00:00:00';
               // dd($exitHTime);
               // $exitHTime = $exitHTimeTmp;
             }
           }
           // dd($exitHTimeTmp);
           // dd($getHourNight,$getHourDay,$getHourTot,$hourcalExit,$horaTotalDay);
           // dd($this->hourSeconds($getShedules->entry),$horaSal,$hourMin);
           // dd($hourcalExit);
           //HORAS MIXTAS
           if($this->hourSeconds($getShedules->entry)=='43200'&&$this->hourSeconds($getShedules->exit)=='64800')
           {
             // dd('asdf');
              $exitMixTime = $this->bettwenMin($getShedules->exit,$hourMin);
              if($exitMixTime!==false){
                $exitmixSec = $this->hourSeconds($exitMixTime);
                $limit = $this->hourSeconds('00:30:00');
                $horaSalExit = $this->hourSeconds($getShedules->exit);
                // $hourCalc = $getShedules->exit;
                $limit = $horaSalExit + $limit;
                $salida = $this->hourSeconds($hourMin);
                if($salida>=$limit){
                  $exitMixText = true;
                  $howMixHours = floor($exitmixSec / 1800);
                }else{
                  $exitMixText = '00:00:00';
                }
              }
              // dd($horaSal.'asdfadf');
           }//HORAS EXTRAS NOCTURNAS - HORARIO NOCTURNO
           else if($this->hourSeconds($getShedules->entry)=='64800'&&$this->hourSeconds($getShedules->exit)=='0')
           {
              $exitNHTime = $this->bettwenMin($getShedules->exit,($hourMin== '00:00:00'?'24:00:00':$hourMin));
              if($exitNHTime!==false)
              {
                $exitHSec = $this->hourSeconds($exitNHTime);
                $limit = $this->hourSeconds('00:30:00');
                $hourCalc = $getShedules->exit;
                $horaSal = $this->hourSeconds($getShedules->exit);
                $limit = $horaSal + $limit;
                $salida = $this->hourSeconds($hourMin);
                if($salida>=$limit){
                  $exitBefore = true;
                  $howHours = floor($exitHSec / 1800);
                  // dd($getShedules->entry,$getShedules->exit);
                  $horaTotalDay = $this->bettwenMin($getShedules->entry,($getShedules->exit=='00:00:00'?'24:00:00':$getShedules->exit));
                  // dd($exitHSec,$this->hourSeconds($horaTotalDay));
                  $workDone = $this->hourSeconds($horaTotalDay)+$exitHSec;
                  $workDone = $this->secondsHour($workDone);
                  // dd($horaTotalDay,$workDone);
                  // $workDone = $this->secondsHour($horaTotalDay);
                  // dd($workDone,$horaTotalDay);
                }
                else
                {
                  $exitHTime = '00:00:00';
                }
              }
              else
              {
                $exitHTime = '00:00:00';
              }
              // dd( $horaTotalDay);
            }


            // dd($exitHTime);
            // dd($exitNHTime,$howHours);
            $chekBef = $this->bettwenMin($logsMo->hourEnter,$getShedules->entry);
            if($chekBef!==false&&$this->hourSeconds($chekBef)>0){
              $enterHour =$getShedules->entry; //si entra antes de la hora establece la hora inicial
            }

            // extraHourDay
           // dd($getShedules->exit);

           $logsMo->latitudeExit = $request->latid;
           $logsMo->longitudExit = $request->longid;
           $logsMo->hourExit = $hourMin;
           $logsMo->hourDay = $horaTotalDay; //horas dia
           $logsMo->hourIdeal = $workDone; //horas reales(con extras)

           if(!empty($exitMixText)){
             $logsMo->extraHourMix = $exitMixTime; //hora extra
             $logsMo->extraHourMixMiddle = $howMixHours;
             // dd($howMixHours,$exitMixText,$exitMixTime);
             // "extraHourMix","extraHourMixMiddle","extraPayMix"
           }else{
             $logsMo->extraHourNight = $exitHTime; //hora extra
             $logsMo->extraHourNightMiddle = $howHours;
           }
           $logsMo->save();
           colaboradores::where('idWRegister',auth()->user()->idWRegister)->update(["idWRegister"=>null]);
           return response()->json([
             "status" => true,
             // "data"=>["idFollow"=>$idEnter],
             "message" => "Guardado"
           ], 200);
         }else{
           return response()->json([
             "status" => false,
             "message" => "Aun no ha empezado un horario"
           ], 400);
         }
       }
       else
       {
         return response()->json([
           "status" => false,
           "message" => "Aun tiene un horario abierto"
         ], 400);
       }
    }
    public function shedules(){

      $workIng=client_colab_assocModel::where(["idColab"=>auth()->user()->id,"status"=>1])->get();
      // dd($workIng);
      $empresas_pertenece = company_colab_assocModel::select('idCompany')->where("idColab",auth()->user()->id)->get()->toArray();
      $empresas_colab = [];
      if(count($empresas_pertenece)>0){
        foreach($empresas_pertenece as $value){
          $empresas_colab[] = $value["idCompany"];
        }
      }
      $clientsArr = [];
      $days = $this->daysEsp();
      $days = $days[date('l')];
      foreach($workIng AS $valus){
        $taskList =  $shedules = [];
        $shed =$valus->getJobs()->first();
        $clients = $valus->getInfoClient()->first();
        // dd($clients);
        $cliente_empresa = companyclientsAssocModel::where("idClient",$clients->id)->whereIn("idCompany",$empresas_colab)->get()->toArray();

        // dd($cliente_empresa);
        if(!empty($shed)){
          $taskAssi = $shed->tasks()->get();
          foreach($taskAssi AS $vlist){
            $taskList[$vlist["id"]] = ["ids"=>$vlist["id"],
                           "name"=>$vlist["name"],
                           "descript"=>$vlist["description"]];
          }
          // dd($taskList);
          $horarios = $shed->getShedules()->where("day",$days)->get();
          // if()
          // dd($horarios);
          foreach($horarios AS $vlone){
            $theFollow = null;
            if(isset($valus->id)&&isset($vlone->id)&&isset($clients->id)){
              $theFollow = loggsModel::where("idLink",$valus->id)
              ->where("idDay",$vlone->id)
              ->where("idClient",$clients->id)
              ->where("userId",auth()->user()->id)
              ->where("dateDay",date("Y-m-d"))
              ->first();
              $taskin = [];
              $tasksaved = (!empty($theFollow->tasks)?json_decode($theFollow->tasks,TRUE):[]);
              foreach($tasksaved AS $valtask){
                  $taskin[$valtask["idT"]]=$valtask["quantity"];
              }
              // dd($taskin);
              foreach($taskAssi AS $vlist){
                $taskList[$vlist["id"]]["done"]=(isset($taskin[$vlist["id"]])?$taskin[$vlist["id"]]:0);
              }
            }

              $shedules[] = ["idFollow"=>(!empty($theFollow)?  $theFollow->id:null),
                             "entry"=>$vlone->entry,
                             "output"=>$vlone->exit];
          }
        }
        if(count($cliente_empresa)>0){
          $clientsArr[]=["idL"=> $valus->id,
          // "idClient"=> $clients->id,
          "name"=> $clients->clientName,
          "shedule"=>$shedules,
          "tasks"=>array_values($taskList)];
        }
      }
        // dd($clients);
         return response()->json(['data'=>$clientsArr]);
      // }
      // else
      // {
      //   return response()->json([
      //     "status" => false,
      //     "message" => "No hay horarios de entrada"
      //   ], 404);
      // }
    }
    public function storeWorkday(Request $request)
    {
       if(isset($request->idFollow)){
         $logsMo = loggsModel::find($request->idFollow);
         $priceTaskCol = $logsMo->getInfoCola()->first();
         // dd($priceTaskCol->hora_extra_diurna);
         $idPs = $logsMo->getClientAsoc()->first()->getJobs()->first();
         $getjsbs = json_decode($request->tastkDone,TRUE);
         $tDone = [];
         $sumTOt = 0;
         foreach($getjsbs AS $vat){
           $tDone[$vat["idT"]] = $vat["quantity"];
           $sumTOt+= $vat["quantity"];
         }
         $atsks = $idPs->tasks()->get();
         $limitTask = [];
         $totDone = $doneAdd = $doneLess = 0;
         $tmpDone = $totDoneBase = 0;
         if(count($atsks)){
           foreach($atsks AS $taskP){
             if(isset($tDone[$taskP->id])){
               if($tDone[$taskP->id]>$taskP->count){
                  $doneAdd+= ($tDone[$taskP->id]-$taskP->count);
               }else if($tDone[$taskP->id]<$taskP->count){
                 $doneLess+= ($taskP->count-$tDone[$taskP->id]);
               }
               $totDoneBase+= $taskP->count;
               // $limitTask[$taskP->id] = $taskP->count;
             }

           }
           $totDone = $totDoneBase-$doneLess;
         }
         // dd($atsks,$totDoneBase,$totDone,$doneAdd,$doneLess,($doneAdd*$priceTaskCol->hora_extra_diurna));
         $logsMo->tasks = $request->tastkDone;
         $logsMo->quantityTask = $totDone;
         // $logsMo->priceTask = $sumTOt;
         $logsMo->baseTasks = $totDoneBase;
         $logsMo->complTask = ($sumTOt>= $totDone?'true':'false' );
         $logsMo->aditTask = $doneAdd;
         $logsMo->lessTask = $doneLess;
         $logsMo->priceAdiTask = ($doneAdd*$priceTaskCol->hora_extra_diurna);
         $logsMo->save();

             return response()->json([
               "status" => true,
               "message" => "Guardado"
             ], 200);
       }
       else
       {
         return response()->json([
           "status" => false,
           "message" => "Faltan datos"
         ], 404);
       }

    }
    public function getPetitions(){
      $petTypes=petitionsModel::get();
      $petit=[];
      foreach($petTypes AS $valus){
          $petit[]= ["id"=>$valus->id,"name"=>$valus->name];
        }

         return response()->json(['data'=>$petit]);
    }
}
