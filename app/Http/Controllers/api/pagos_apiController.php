<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\company_colab_assocModel;

use App\apiModels\loggsPlanillaModel;
use App\apiModels\userHorasModel;
use App\apiModels\userViaticosModel;

use App\apiModels\userPagosModel;

class pagos_apiController extends Controller
{

    public function __construct() {
      
      $this->middleware('cors');

    }


    public function total_pago(Request $request) {


      $log_pago = userPagosModel::select('id','estado')
                                                ->where("fecha_pago",$request->fecha_pago)
                                                ->where("id_colab",$request->id_colab)
                                                // ->where('id_empresa',$request->id_empresa)
                                                // ->where('id_cliente',$request->id_cliente)
                                                ->first();

      $estado_pago = 'No Procesado';

      if ($log_pago!='') {
          $estado_pago = $log_pago['estado'];
      }                                          


      $log_planilla = loggsPlanillaModel::where("id_colab",$request->id_colab)
                                        // ->where('id_empresa',$request->id_empresa)
                                        // ->where('id_cliente',$request->id_cliente)
                                        ->where("fecha_pago",$request->fecha_pago)
                                        ->first();

      $log_horas = userHorasModel::where("id_colab",$request->id_colab)
                                        // ->where('id_empresa',$request->id_empresa)
                                        // ->where('id_cliente',$request->s)
                                        ->where("fecha_pago",$request->fecha_pago)
                                        ->first();

      $log_viaticos = userViaticosModel::where("id_colab",$request->id_colab)
                                        // ->where('id_empresa',$request->id_empresa)
                                        // ->where('id_cliente',$request->id_cliente)
                                        ->where("fecha_pago",$request->fecha_pago)
                                        ->first();

      $total_planilla = 0;
      $total_horas = 0;
      $total_viaticos = 0;

      $array_planilla = array();
      $array_horas = array();
      $array_viaticos = array();
                    
      if ($log_planilla!='') {
          $total_planilla = $log_planilla['total'];

          // $array_planilla = (array) $log_planilla;
              $array_planilla['id'] = $log_planilla->id;
              $array_planilla['fecha_inicio'] = $log_planilla->fecha_inicio;
              $array_planilla['fecha_fin'] = $log_planilla->fecha_fin;
              $array_planilla['tipo'] = $log_planilla->tipo;
              $array_planilla['sueldo'] = $log_planilla->sueldo;
              $array_planilla['total'] = $log_planilla->total;
              $array_planilla['bono_ley'] = $log_planilla->bono_ley;
              $array_planilla['bono_extra'] = $log_planilla->bono_extra;
              $array_planilla['bono_antiguedad'] = $log_planilla->bono_antiguedad;
              $array_planilla['desc_dia'] = $log_planilla->desc_dia;
              $array_planilla['desc_septimo'] = $log_planilla->desc_septimo;
              $array_planilla['desc_igss'] = $log_planilla->desc_igss;
              $array_planilla['descuentos'] = $log_planilla->descuentos;
              $array_planilla['total_bonos'] = $log_planilla->total_bonos;
              $array_planilla['total_descuentos'] = $log_planilla->total_descuentos;
              $array_planilla['estado'] = $log_planilla->estado;
              $array_planilla['fecha_pago'] = $log_planilla->fecha_pago;

      }

      if ($log_horas!='') {
          $total_horas = $log_horas['total'];

              $array_horas['id'] = $log_horas->id;
              $array_horas['fecha_inicio'] = $log_horas->fecha_inicio;
              $array_horas['fecha_fin'] = $log_horas->fecha_fin;
              $array_horas['tipo'] = $log_horas->tipo;
              $array_horas['total'] = $log_horas->total;
              $array_horas['extra_dia'] = $log_horas->extra_dia;
              $array_horas['extra_noche'] = $log_horas->extra_noche;
              $array_horas['extra_especial'] = $log_horas->extra_especial;
              $array_horas['extra_mixta'] = $log_horas->extra_mixta;
              $array_horas['extra_metas'] = $log_horas->extra_metas;
              $array_horas['desc_igss'] = $log_horas->desc_igss;
              $array_horas['metas'] = $log_horas->metas;
              $array_horas['estado'] = $log_horas->estado;
              $array_horas['fecha_pago'] = $log_horas->fecha_pago;

      }

      if ($log_viaticos!='') {
          $total_viaticos = $log_viaticos['total'];

              $array_viaticos['id'] = $log_viaticos->id;
              $array_viaticos['fecha_inicio'] = $log_viaticos->fecha_inicio;
              $array_viaticos['fecha_fin'] = $log_viaticos->fecha_fin;
              $array_viaticos['tipo'] = $log_viaticos->tipo;
              $array_viaticos['total'] = $log_viaticos->total;
              $array_viaticos['viaticos'] = $log_viaticos->viaticos;
              $array_viaticos['movil'] = $log_viaticos->movil;
              $array_viaticos['extras'] = $log_viaticos->extras;
              $array_viaticos['estado'] = $log_viaticos->estado;
              $array_viaticos['fecha_pago'] = $log_viaticos->fecha_pago;

      }

      $total = $total_planilla + $total_horas + $total_viaticos;


      return response()->json([
                "data" => ["planilla"=>$array_planilla,
                           "horas"=>$array_horas,
                           "viaticos"=>$array_viaticos,
                           "total"=>$total,
                           "estado_pago"=>$estado_pago],
                "status"=>true
              ], 200);

    }





    public function guardar_pago(Request $request) {

      $colaborador = company_colab_assocModel::where("idColab", $request->id_colab)->where("status", 1)->first();

      $model = new userPagosModel;
      $data = $request->only($model->getFillable());

      if ($colaborador!='') {
          $data['id_empresa'] = $colaborador->idCompany;
      }

      $model->fill($data)->save();

      return response()->json([
                "data" => ["id_pago"=>$model->id,
                           "estado"=>$model->estado],
                "status"=>true
              ], 200);

    }

}
