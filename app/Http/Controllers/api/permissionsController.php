<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use App\apiModels\ecopetitionModel;
use App\apiModels\simplepetitionModel;
use App\apiModels\complaimentsModel;
class permissionsController extends Controller
{
    public function __construct()
    {
       $this->middleware('cors');
    }
    public function index()
    {
      return response()->json([
          "status" => true,
          "data" =>[],
        ], 200);
    }
    public function setPetitionTime(Request $request)
    {
      if(isset($request->idUser)){
          $perms = new timepermisionsModel;
          $perms->idUser = $request->idUser;
          $perms->dateOut = $request->dateOut;
          $perms->dateEnter = $request->dateEnter;
          $perms->deteRepEnter = $request->deteRepEnter;
          $perms->deteRepOut = $request->deteRepOut;
          $perms->description = $request->descript;
          $perms->save();
          return response()->json([
             "status" => true,
             "message" => "Guardado"
          ], 200);
       }else{
         return response()->json([
           "status" => false,
           "message" => "Aun no ha empezado un horario"
         ], 400);
       }

    }
    public function setPetitionTimeDefault(Request $request)
    {
      if(isset($request->idUser)){
          $permissions = ['Fallecimiento'=>3,
                        'Matrimonio'=>5,
                        'Nacimiento'=> 2,
                        'Citatorio'=>0.5,
                        'IGSS'=>0,
                        'Enfermedad'=> 1];
          $perms = new defaulttimepermisionsModel;
          $perms->idUser = $request->idUser;
          $perms->datePerm = $request->datePerm;
          $perms->typos = $request->type;
          $perms->status = '0';
          $perms->typeOther = $request->typeOther;
          $perms->description = $request->descript;
          $perms->days = (strtolower($request->type)=='otro'?$request->days:$permissions[$request->type]);
          $perms->save();
          return response()->json([
             "status" => true,
             "message" => "Guardado"
          ], 200);
       }else{
         return response()->json([
           "status" => false,
           "message" => "Aun no ha empezado un horario"
         ], 400);
       }

    }
    public function economicpetition(Request $request)
    {
      if(isset($request->idUser)){
          $perms = new ecopetitionModel;
          $perms->idUser = $request->idUser;
          $perms->typos = $request->type;
          $perms->applyTo = $request->applyTo;
          $perms->otherAplyText = $request->otherAplyText;
          $perms->comment = $request->comment;
          $perms->descript = $request->descript;
          $perms->status = '0';
          $perms->save();
          return response()->json([
             "status" => true,
             "message" => "Guardado"
          ], 200);
       }else{
         return response()->json([
           "status" => false,
           "message" => "Aun no ha empezado un horario"
         ], 400);
       }

    }
    public function simplepetitions(Request $request)
    {
      if(isset($request->idUser)){
          $perms = new simplepetitionModel;
          $perms->idUser = $request->idUser;
          $perms->comment = $request->comment;
          $perms->idType = $request->idType;
          $perms->status = '0';
          $perms->save();
          return response()->json([
             "status" => true,
             "message" => "Guardado"
          ], 200);
       }else{
         return response()->json([
           "status" => false,
           "message" => "Aun no ha empezado un horario"
         ], 400);
       }

    }
    public function complaintments(Request $request)
    {
      if(isset($request->idUser)){
          $perms = new complaimentsModel;
          $perms->idUser = $request->idUser;
          $perms->comment = $request->comment;
          $perms->status = '0';
          $perms->save();
          return response()->json([
             "status" => true,
             "message" => "Guardado"
          ], 200);
       }else{
         return response()->json([
           "status" => false,
           "message" => "Aun no ha empezado un horario"
         ], 400);
       }

    }
}
