<?php

namespace App\Http\Controllers\api;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\UserAdmin;
use App\adminModels\mensajesModel;
use App\adminModels\colaboradores;


class mensajes_userController extends Controller
{

  public function __construct(Request $request)
  
  {
      $this->middleware('cors');

      if ( $request->id_user ) {
          $this->id_user = $request->id_user;
      } else {
          $this->id_user = Auth()->user()->id;
      }
      

  }


  public function index() {

    $id_user = $this->id_user;


    $mensajes = mensajesModel::where('de_user', $id_user)->orWhere('para_user', $id_user)->orderBy('updated_at', 'DESC')->get();

    $mensajes_data = array();

    // dd($mensajes);

    if (count($mensajes)>0) {
        foreach($mensajes AS $mensaje) {

            $mensaje_data['id'] = $mensaje->id;
            $mensaje_data['de'] = $mensaje->de;
            $mensaje_data['para'] = $mensaje->para;
            $mensaje_data['titulo'] = $mensaje->titulo; 
            $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
            $mensaje_data['tipo'] = $mensaje->tipo;
            $mensaje_data['status'] = $mensaje->status;

            if (($mensaje->tipo==0 && $mensaje->status==0) || ($mensaje->tipo==1 && $mensaje->status==2)) {
                $mensaje_data['leido'] = 0;
            } else {
                $mensaje_data['leido'] = 1;
            }

            $mensajes_data[] = $mensaje_data;

        }

        return response()->json([
              "status" => true,
              "data" => $mensajes_data
            ], 200);

    } else {
        return response()->json([
              "status" => false,
              "message" =>"No hay mensajes.",
            ], 400);
    }

  }


    public function show(Request $request)
    {

        $mensaje = mensajesModel::find($request->id);

        // dd($mensaje);

        if ($mensaje!='') {
            $mensaje_data['id'] = $mensaje->id;
            $mensaje_data['de'] = $mensaje->de;
            $mensaje_data['para'] = $mensaje->para;
            $mensaje_data['titulo'] = $mensaje->titulo; 
            $mensaje_data['mensaje'] = $mensaje->mensaje;
            $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->created_at));
            $mensaje_data['tipo'] = $mensaje->tipo;
            $mensaje_data['status'] = $mensaje->status;

            $mensaje_data['respuestas'] = json_decode($mensaje->respuestas, true);


            if (($mensaje->tipo==0)&&($mensaje->status == 0)) {
                $mensaje->status = 1;
            }
            $mensaje->save();


            return response()->json([
                  "status" => true,
                  "data" => $mensaje_data
                ], 200);

        } else {
            return response()->json([
                  "status" => false,
                  "message" =>"Error.",
                ], 400);
        }

    }


    public function create(Request $request)
    {

        $admins = userAdmin::get();

        foreach($admins AS $admin) {
            $admins_data['id'] = $admin->id;
            $admins_data['nombre'] = $admin->name;
        }

        return response()->json([
                  "status" => true,
                  "data" => $admins_data
                ], 200);

    }


  public function store(Request $request) {   

    $model = new mensajesModel;
    $data = $request->only($model->getFillable());

    $data['de_user'] = $this->id_user;
    $data['de'] = colaboradores::get_name($this->id_user);
    $data['para'] = UserAdmin::get_name($request->para_admin);
    $data['tipo'] = 1;

    $model->fill($data)->save();

    return response()->json([
                  "status" => true,
                  "message" => 'Mensaje Enviado!'
                ], 200);

  }



  public function update(Request $request) {

    // id de mensaje, respuesta

    $id = $request->id;
    $id_user = $this->id_user;

    $mensaje = mensajesModel::find($id);

    if ($mensaje!='') {
        $respuestas = json_decode($mensaje->respuestas, true);

        $respuesta["de"] = colaboradores::get_name($id_user);
        $respuesta["fecha"] = date('d-m-Y h:i');
        $respuesta["respuesta"] = $request->respuesta;
        $respuesta["tipo"] = 1;

        $respuestas[] = $respuesta;

        $mensaje->respuestas = json_encode($respuestas);

        if ($mensaje->tipo == 1) {
            $mensaje->status = 0; // enviado
        } else {
            $mensaje->status = 2; // respondido
        }

        $mensaje->save();

        return response()->json([
                      "status" => true,
                      "message" => 'Mensaje Respondido!'
                    ], 200);

    } else {
        return response()->json([
              "status" => false,
              "message" =>"Error",
            ], 400);
    } 

  }


  public function alerta(Request $request) {

    $id_user = $this->id_user; 

    $mensajes = mensajesModel::where('de_user', $id_user)->orWhere('para_user', $id_user)->orderBy('updated_at', 'DESC')->get();


    $mensajes_data = array();


    if (count($mensajes)>0) {
        foreach($mensajes AS $mensaje) {


            if (($mensaje->tipo==0 && $mensaje->status==0) || ($mensaje->tipo==1 && $mensaje->status==2)) {

                $mensaje_data['id'] = $mensaje->id;
                $mensaje_data['de'] = $mensaje->de;
                $mensaje_data['para'] = $mensaje->para;
                $mensaje_data['titulo'] = $mensaje->titulo; 
                $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
                $mensaje_data['tipo'] = $mensaje->tipo;
                // $mensaje_data['status'] = $mensaje->status;

                if ( $mensaje->tipo==0 && $mensaje->status==0 && $mensaje->respuestas=='') {
                    
                    $mensaje_data['status'] = 'Nuevo';

                } else { 
                    $mensaje_data['status'] = 'Respuesta';
                }

                $mensajes_data[] = $mensaje_data;

            }

        }

        return response()->json([
              "status" => true,
              "data" => $mensajes_data
            ], 200);

    } else {
        return response()->json([
              "status" => false,
              "message" =>"No hay mensajes.",
            ], 400);
    }

  }


}
