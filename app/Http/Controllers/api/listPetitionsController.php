<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use App\apiModels\ecopetitionModel;
use App\apiModels\simplepetitionModel;
use App\apiModels\complaimentsModel;
class listPetitionsController extends Controller
{
    public function __construct()
    {
       $this->middleware('cors');
    }
    public function listPetitions(){
      $hours = $selfHours = $ecoPerm = $simple = $compla = $tots = [];
      $status = ['noresponse','aproved','unapproved'];
      $hourperm=timepermisionsModel::where("idUser",auth()->user()->id)->orderBy("created_at","DESC")->get();
      $defhourperm=defaulttimepermisionsModel::where("idUser",auth()->user()->id)->orderBy("created_at","DESC")->get();
      $ecopetit=ecopetitionModel::where("idUser",auth()->user()->id)->orderBy("created_at","DESC")->get();
      $simplepetit=simplepetitionModel::where("idUser",auth()->user()->id)->orderBy("created_at","DESC")->get();
      $complaint=complaimentsModel::where("idUser",auth()->user()->id)->orderBy("created_at","DESC")->get();
      foreach($hourperm AS $val){
        $hours[] = ["created"=>date("d-m-Y",strtotime($val->created_at)),
                    "dateOut"=>$val->dateOut,
                    "dateEnter"=>$val->dateEnter,
                    "deteRepEnter"=>$val->deteRepEnter,
                    "deteRepOut"=>$val->deteRepOut,
                    "description"=>$val->description,
                    "status"=>$status[$val->status],
                    "reponse"=>$val->reponse,
                    "responseDate"=>date("d-m-Y",strtotime($val->responseDate)),
                  ];
      }
      foreach($defhourperm AS $val){
        $selfHours[] = ["created"=>date("d-m-Y",strtotime($val->created_at)),
                        "datePerm"=>$val->datePerm,
                        "type"=>$val->typos,
                        "typeOther"=>$val->typeOther,
                        "description"=>$val->description,
                        "status"=>$status[$val->status],
                        "reponse"=>$val->reponse,
                        "responseDate"=>date("d-m-Y",strtotime($val->responseDate)),
                  ];
      }
      foreach($ecopetit AS $val){
        $ecoPerm[] = ["created"=>date("d-m-Y",strtotime($val->created_at)),
                      "type"=>$val->typos,
                      "applyTo"=>$val->applyTo,
                      "otherApplyText"=>$val->otherAplyText,
                      "comment"=>$val->comment,
                      "description"=>$val->descript,
                      "status"=>$status[$val->status],
                      "reponse"=>$val->reponse,
                      "responseDate"=>date("d-m-Y",strtotime($val->responseDate)),
                  ];
      }
      foreach($simplepetit AS $val){
        $simple[$val->status] = ["created"=>date("d-m-Y",strtotime($val->created_at)),
                                "comment"=>$val->comment,
                                "status"=>$status[$val->status],
                                "reponse"=>$val->reponse,
                                "responseDate"=>date("d-m-Y",strtotime($val->responseDate)),
                  ];
      }
      foreach($complaint AS $val){
        $compla[] = ["created"=>date("d-m-Y",strtotime($val->created_at)),
                                "comment"=>$val->comment,
                                "status"=>$status[$val->status],
                                "reponse"=>$val->reponse,
                                "responseDate"=>date("d-m-Y",strtotime($val->responseDate)),
                               ];
      }
             return response()->json([
               "status" => true,
               "data"=>["permDates"=>$hours,"permDateDef"=>$selfHours,
                        "permEco"=>$ecoPerm,"petition"=>$simple,
                        "complain"=>$compla],
               "message" => "Guardado"
             ], 200);
       // }
       // else
       // {
       //   return response()->json([
       //     "status" => false,
       //     "message" => "Faltan datos"
       //   ], 404);
       // }

    }

}
