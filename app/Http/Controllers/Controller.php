<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Helper\Helper;
use App\adminModels\roles;
use App\adminModels\roles_names;
use App\adminModels\ordersModel;
use App\apiModels\paymentsModel;
use App\adminModels\archivosModel;
use App\adminModels\changeLogModel;
use App\adminModels\stockIngresosModel;
use App\adminModels\stockEgresosModel;
use App\adminModels\productosModel;
use App\adminModels\stockModel;
use App\adminModels\stockIngresosEquipoModel;
use App\adminModels\stockEgresosEquipoModel;
use App\adminModels\equiposModel;
use App\adminModels\stockEquiposModel;
use App\adminModels\bodegasModel;
use App\adminModels\companyclientsBranchModel;
use App\adminModels\companyclientsModel;
use App\adminModels\companiesModel;
use App\adminModels\companyclientsAssocModel;
use App\adminModels\company_colab_assocModel;
use App\adminModels\colaboradores;
use App\adminModels\alertasModel;

use Auth;
use Illuminate\Support\Facades\Storage;
use nusoap_client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function list_sidebar()
    {
      // dd(!Helper::superAdmin());
      if(!Helper::superAdmin()){
          $varm=roles::find(Auth::user()->roleUS)->nameroles->where('publc','1');
      }else{
        $varm=roles_names::orderBy('naccess', 'ASC')->get();
      }

      // dd($varm);
      $icns=array();
      foreach($varm AS $elemts){
        $grupi=($elemts->groupacc!=''?$elemts->groupacc:'NA');
        $icns[str_replace(" ","",$grupi)][]=array("group"=>$elemts->groupacc,
                                                  "name"=>$elemts->naccess,
                                                  "perm"=>$elemts->archaccess);
        $groups[str_replace(" ","",$grupi)]=array("icon"=>$elemts->iconaccess,
                                                  "perm"=>$elemts->archaccess,
                                                  "name"=>$elemts->groupacc);
      }
      return array("groups"=>$groups,"access"=>$icns);
    }
    public function  randem($longitud,$nmbr=0)
    {
            $this->long=$longitud;
            $this->key ='';
            $patrn=array('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ','1234567890');
            $this->pattern = $patrn[$nmbr];
            $this->max = strlen($this->pattern)-1;
            for($i=0;$i < $this->long;$i++) $this->key.= $this->pattern[mt_rand(0,$this->max)];
            return $this->key;
    }
    public function superuser(){
      return Helper::superAdmin();
    }
    public function get_country(){
      return Auth::user()->country;
    }
    public function getUserId(){
      return Auth::user()->id;
    }
    public function userName(){
      return Auth::user()->usersys;
    }
    public function uploadFiles($file,$path,$owname=1)
    {
       $extension = $file->getClientOriginalExtension();
       $name= ($owname==1?$this->randem(35).'.'.$extension:$file->getClientOriginalName());
       $filePath = $path . $name;
       // dd($file);
       Storage::disk('digitalocean')->put($filePath, file_get_contents($file));
       return $filePath;
    }
    public function deleteFiles($files)
    {
      Storage::delete($files);
    }
    public function getOrder($token){
        // if($idOrder!==null){
        $order = ordersModel::where("tokenProd",$token)->first();
          // dd($order);
        // }
        // else{
        //   $order = ordersModel::where("tokenProd",$token)
        //                       ->where("statusOrder","1")->first();
        // }
        $recib = [];
        $suma = 0;
        if(!empty($order)){
          foreach($order->details()->get() AS $valu){
            $procu = $valu->productIs()->first();
            $imag = (json_decode($procu->images)!=null? json_decode($procu->images):null);
            $imag = ($imag != null ? asset('storage/'.$imag[0]) : null);
            $recib[] = [
                        "idItem"=>$valu->id,
                        "idProd"=>$procu->id,
                        "product"=>$procu->title,
                        "image"=>$imag,
                        "description"=>$procu->description,
                        "price"=>$procu->price,
                        "quantity"=>$valu["quantity"],
                        "subtotal"=>$procu->price*$valu["quantity"]];
            $suma += $procu->price * $valu["quantity"];
          }
        }
        // dd("testeo");
        $discount = 0;
        if(!empty($order->userBuy)){
          $usfist = $order->getBuyer()->first();
          if(!empty($usfist->typeS)){
            $typo = $usfist->get_typeSus()->first();
            if($suma>=$typo->amountmin&&$suma<=$typo->amountmax){
              $discount = $typo->discount;
            }
          }
        }

        if (count($recib)){
            return [
                'recib' => $recib,
                'suma' => $suma,
                'discount' => round(($discount * $suma) / 100, 2),
                'totalDis' => round($suma - (($discount * $suma) / 100), 2)
            ];
        }else{
            return false;
        }
    }

    public function generalReversePay($data){
        // $idPayment = $request->id; traerlo de la tabla payments
        $this->pdata = new paymentsModel;

        $auditNumber = $data['auditNumber'];
        $client_ip = $data['client_ip'];

        $client = new nusoap_client('https://epaytestvisanet.com.gt/paymentcommerce.asmx?WSDL', true);

        $url = "https://epaytestvisanet.com.gt/paymentcommerce.asmx?WSDL";
        $param=array(
            'posEntryMode' => "012" //Método de entrada
            ,'pan' => ""
            ,'expdate' => ""
            ,'amount' => ""
            ,'track2Data' => ""
            ,'cvv2' => ""
            ,'paymentgwIP' => "190.149.69.135" //IP WebService Visanet
            ,'shopperIP' => $client_ip //"190.149.168.54" //IP Cliente que realiza la compra
            ,'merchantServerIP' => "67.205.167.98" //IP Comercio integrado a VisaNet
            ,'merchantUser' => "76B925EF7BEC821780B4B21479CE6482EA415896CF43006050B1DAD101669921" //Usuario
            ,'merchantPasswd' => "DD1791DB5B28DDE6FBC2B9951DFED4D97B82EFD622B411F1FC16B88B052232C7" //Password
            ,'terminalId' => "77788881" //Terminal
            ,'merchant' => "00575123" //Afiliacion
            ,'messageType' => "0400" //Mensaje de REVERSA
            ,'auditNumber' => $auditNumber // "990628" //Correlativo ciclico de transaccion de 000001 hasta 999999
            ,'additionalData' => "" //Datos adicionales cuotas o puntos
        );
        $params = array(array('AuthorizationRequest' => $param));

        $client = new nusoap_client($url, 'wsdl');
        $client->connection_timeout = 10;

        try
        {
            $result = $client->call('AuthorizationRequest',$params);
        }
        catch(SoapFault $e)
        {
            $result['response']['responseText'] = $this->parseResponseCode($result['response']['responseCode']);

            $this->pdata->no_tarjeta = '';
            $this->pdata->amount = '';
            $this->pdata->shopper_ip = $client_ip;
            $this->pdata->status = 'failed';
            $this->pdata->user_id = '';
            $this->pdata->tokenO =  '';

            $this->pdata->audit = $result['response']['auditNumber'];
            $this->pdata->reference = $result['response']['referenceNumber'];
            $this->pdata->authorization = $result['response']['authorizationNumber'];
            $this->pdata->response = $result['response']['responseCode'];
            $this->pdata->responseText = $result['response']['responseText'];
            $this->pdata->messageType = $result['response']['messageType'];
            $this->pdata->transactionType = 'Reversa';
            $this->pdata->save();

            return false;
        }

        $result['response']['responseText'] = $this->parseResponseCode($result['response']['responseCode']);

        $this->pdata->no_tarjeta = '';
        $this->pdata->amount = 0;
        $this->pdata->shopper_ip = $client_ip;
        $this->pdata->status = 'success';
        $this->pdata->user_id = 0;
        $this->pdata->tokenO =  '';

        $this->pdata->audit = $result['response']['auditNumber'];
        $this->pdata->reference = $result['response']['referenceNumber'];
        $this->pdata->authorization = $result['response']['authorizationNumber'];
        $this->pdata->response = $result['response']['responseCode'];
        $this->pdata->responseText = $result['response']['responseText'];
        $this->pdata->messageType = $result['response']['messageType'];
        $this->pdata->transactionType = 'Reversa';
        $this->pdata->save();

        return $result;
    }

    public function parseResponseCode($code){
        switch($code){
            case '00':
                return 'Aprobada';
                break;
            case '01':
            case '02':
                return 'Refiérase al emisor';
                break;
            case '05':
                return 'Transacción no aceptada';
                break;
            case '12':
                return 'Transacción inválida';
                break;
            case '13':
                return 'Monto inválido';
                break;
            case '19':
                return 'Transacción no realizada, intente de nuevo';
                break;
            case '31':
                return 'Tarjeta no soportada por switch';
                break;
            case '35':
                return 'Transacción ya ha sido ANULADA';
                break;
            case '36':
                return 'Transacción a ANULAR no EXISTE';
                break;
            case '37':
                return 'Transacción de ANULACION REVERSADA';
                break;
            case '38':
                return 'Transacción a ANULAR con Error';
                break;
            case '41':
                return 'Tarjeta Extraviada';
                break;
            case '43':
                return 'Tarjeta Robada';
                break;
            case '51':
                return 'No tiene fondos disponibles';
                break;
            case '57':
                return 'Transacción no permitida';
                break;
            case '58':
                return 'Transacción no permitida en la terminal';
                break;
            case '65':
                return 'Límite de actividad excedido';
                break;
            case '80':
                return 'Fecha de Expiración inválida';
                break;
            case '89':
                return 'Terminal inválida';
                break;
            case '91':
                return 'Emisor no disponible';
                break;
            case '94':
                return 'Transacción duplicada';
                break;
            case '96':
                return 'Error del sistema, intente más tarde';
                break;
            default:
                return 'Error desconocido';
                break;
        }
    }
    public static function get_depmunic($ret='all')
    {
      	$deptos["Alta Verapaz"]=array('Chahal',
      					'Lanquín',
      					'San Juan Chamelco',
      					'Santa María Cahabón',
      					'Tucurú',
      					'Chisec',
      					'Panzós',
      					'San Pedro Carchá',
      					'Senahú',
      					'Cobán',
      					'Raxruhá',
      					'Santa Catalina La Tinta',
      					'Tactic',
      					'Fray Bartolomé de las Casas',
      					'San Cristóbal Verapaz',
      					'Santa Cruz Verapaz',
      					'Tamahú');
      	$deptos['Baja Verapaz']=array('Cubulco',
      				'Salamá',
      				'Granados',
      				'San Jerónimo',
      				'Purulhá',
      				'San Miguel Chicaj',
      				'Rabinal',
      				'Santa Cruz el Chol');
      	$deptos['Chimaltenango']=array('Acatenango',
      					'Patzicía',
      					'San José Poaquil',
      					'Santa Cruz Balanyá',
      					'Chimaltenango',
      					'Patzún',
      					'San Juan Comalapa',
      					'Tecpán',
      					'El Tejar',
      					'Pochuta',
      					'San Martín Jilotepeque',
      					'Yepocapa',
      					'Parramos',
      					'San Andrés Itzapa',
      					'Santa Apolonia',
      					'Zaragoza');
      	$deptos['Chiquimula']=array('Camotán',
      					'Ipala',
      					'San Jacinto',
      					'Chiquimula',
      					'Jocotán',
      					'San José La Arada',
      					'Concepción Las Minas',
      					'Olopa',
      					'San Juan Ermita',
      					'Esquipulas',
      					'Quezaltepeque');

      	$deptos['El Progreso']=array('El Jícaro',
      					'San Antonio La Paz',
      					'Guastatoya',
      					'San Cristóbal Acasaguastlán',
      					'Morazán',
      					'Sanarate',
      					'San Agustín Acasaguastlán',
      					'Sansare');

      	$deptos['Escuintla']=array('Escuintla',
      				'La Gomera',
      				'San José',
      				'Tiquisate',
      				'Guanagazapa',
      				'Masagua',
      				'San Vicente Pacaya',
      				'Iztapa',
      				'Nueva Concepción',
      				'Santa Lucía Cotzumalguapa',
      				'La Democracia',
      				'Palín',
      				'Siquinalá');

      	$deptos['Guatemala']=array('Amatitlán',
      					'Guatemala',
      					'San José Pinula',
      					'San Pedro Sacatepéquez',
      					'Villa Nueva',
      					'Chinautla',
      					'Mixco',
      					'San Juan Sacatepéquez',
      					'San Raymundo',
      					'Chuarrancho',
      					'Palencia',
      					'San Miguel Petapa',
      					'Santa Catarina Pinula',
      					'Fraijanes',
      					'San José del Golfo',
      					'San Pedro Ayampuc',
      					'Villa Canales');

      	$deptos['Huehuetenango']=array( 'Aguacatán',
      					'Cuilco',
      					'La Libertad',
      					'San Gaspar Ixchil',
      					'San Mateo Ixtatán',
      					'San Rafael La Independencia',
      					'Santa Ana Huista',
      					'Santiago Chimaltenango',
      					'Chiantla',
      					'Huehuetenango',
      					'Malacatancito',
      					'San Ildefonso Ixtahuacán',
      					'San Miguel Acatán',
      					'San Rafael Petzal',
      					'Santa Bárbara',
      					'Tectitán',
      					'Colotenango',
      					'Jacaltenango',
      					'Nentón',
      					'San Juan Atitán',
      					'San Pedro Necta',
      					'San Sebastián Coatán',
      					'Santa Cruz Barillas',
      					'Todos Santos Cuchumatánes',
      					'Concepción Huista',
      					'La Democracia',
      					'San Antonio Huista',
      					'San Juan Ixcoy',
      					'San Pedro Soloma',
      					'San Sebastián',
      					'Santa Eulalia',
      					'Unión Cantinil');

      	$deptos['Izabal']=array( 'El Estor',
      					'Puerto Barrios',
      					'Livingston',
      					'Los Amates',
      					'Morales');
      	$deptos['Jalapa']=array(	'Jalapa',
      					'San Luis Jilotepeque',
      					'Mataquescuintla',
      					'San Manuel Chaparrón',
      					'Monjas',
      					'San Pedro Pinula',
      					'San Carlos Alzatate');
      	$deptos['Jutiapa']=array( 'Agua Blanca',
      					'Conguaco',
      					'Jerez',
      					'Quesada',
      					'Zapotitlán',
      					'Asunción Mita',
      					'El Adelanto',
      					'Jutiapa',
      					'San José Acatempa',
      					'Atescatempa',
      					'El Progreso',
      					'Moyuta',
      					'Santa Catarina Mita',
      					'Comapa',
      					'Jalpatagua',
      					'Pasaco',
      					'Yupiltepeque');
      	$deptos['Petén']=array('Dolores',
      					'Melchor de Mencos',
      					'San Francisco',
      					'Sayaxché',
      					'Flores',
      					'Poptún',
      					'San José',
      					'La Libertad',
      					'San Andrés',
      					'San Luis',
      					'Las Cruces',
      					'San Benito',
      					'Santa Ana');


      	$deptos['Quetzaltenango']=array( 'Almolonga',
      					'Coatepeque',
      					'Flores Costa Cuca',
      					'Olintepeque',
      					'San Carlos Sija',
      					'San Mateo',
      					'Cabricán',
      					'Colomba',
      					'Génova',
      					'Palestina de Los Altos',
      					'San Francisco La Unión',
      					'San Miguel Sigüilá',
      					'Cajolá',
      					'Concepción Chiquirichapa',
      					'Huitán',
      					'Quetzaltenango',
      					'San Juan Ostuncalco',
      					'Sibilia',
      					'Cantel',
      					'El Palmar',
      					'La Esperanza',
      					'Salcajá',
      					'San Martín Sacatepéquez',
      					'Zunil');

      	$deptos['Quiché']=array('Canillá',
                                      'Chichicastenango',
                                      'Joyabaj',
                                      'Sacapulas',
                                      'San Juan Cotzal',
                                      'Zacualpa',
                                      'Chajul',
                                      'Chinique',
                                      'Nebaj',
                                      'San Andrés Sajcabajá',
                                      'San Pedro Jocopilas',
                                      'Chicamán',
                                      'Cunén',
                                      'Pachalum',
                                      'San Antonio Ilotenango',
                                      'Santa Cruz del Quiché',
                                      'Chiché',
                                      'Ixcán',
                                      'Patzité',
                                      'San Bartolomé Jocotenango',
                                      'Uspantán');

      	$deptos['Retalhuleu']=array(	'Champerico',
      					'San Andrés Villa Seca',
      					'Santa Cruz Muluá',
      					'El Asintal',
      					'San Felipe',
      					'Nuevo San Carlos',
      					'San Martín Zapotitlán',
      					'Retalhuleu',
      					'San Sebastián');

      	$deptos['Sacatepéquez']=array( 'Alotenango',
      					'Magdalena Milpas Altas',
      					'San Lucas Sacatepéquez',
      					'Santa María de Jesús',
      					'La Antigua Guatemala',
      					'Pastores',
      					'San Miguel Dueñas',
      					'Santiago Sacatepéquez',
      					'Ciudad Vieja',
      					'San Antonio Aguas Calientes',
      					'Santa Catarina Barahona',
      					'Santo Domingo Xenacoj',
      					'Jocotenango',
      					'San Bartolomé Milpas Altas',
      					'Santa Lucía Milpas Altas',
      					'Sumpango');

      	$deptos['San Marcos']=array('Ayutla',
      					'El Quetzal',
      					'Ixchiguán',
      					'Ocós',
      					'San Cristóbal Cucho',
      					'San Miguel Ixtahuacán',
      					'Sibinal',
      					'Tejutla',
      					'Catarina',
      					'El Rodeo',
      					'La Reforma',
      					'Pajapita',
      					'San José Ojetenam',
      					'San Pablo',
      					'Sipacapa',
      					'Comitancillo',
      					'El Tumbador',
      					'Malacatán',
      					'Río Blanco',
      					'San Lorenzo',
      					'San Pedro Sacatepéquez',
      					'Tacaná',
      					'Concepción Tutuapa',
      					'Esquipulas Palo Gordo',
      					'Nuevo Progreso',
      					'San Antonio Sacatepéquez',
      					'San Marcos',
      					'San Rafael Pie de La Cuesta',
      					'Tajumulco');

      	$deptos['Santa Rosa']=array('Barberena',
      					'Guazacapán',
      					'San Juan Tecuaco',
      					'Santa Rosa de Lima',
      					'Casillas',
      					'Nueva Santa Rosa',
      					'San Rafaél Las Flores',
      					'Taxisco',
      					'Chiquimulilla',
      					'Oratorio',
      					'Santa Cruz Naranjo',
      					'Cuilapa',
      					'Pueblo Nuevo Viñas',
      					'Santa María Ixhuatán');

      	$deptos['Sololá']=array( 'Concepción',
      					'San Antonio Palopó',
      					'San Marcos La Laguna',
      					'Santa Catarina Palopó',
      					'Santa María Visitación',
      					'Nahualá',
      					'San José Chacayá',
      					'San Pablo La Laguna',
      					'Santa Clara La Laguna',
      					'Santiago Atitlán',
      					'Panajachel',
      					'San Juan La Laguna',
      					'San Pedro La Laguna',
      					'Santa Cruz La Laguna',
      					'Sololá',
      					'San Andrés Semetabaj',
      					'San Lucas Tolimán',
      					'Santa Catarina Ixtahuacan',
      					'Santa Lucía Utatlán');

      	$deptos['Suchitepéquez']=array('Chicacao',
      					'Pueblo Nuevo',
      					'San Bernardino',
      					'San Juan Bautista',
      					'Santa Bárbara',
      					'Cuyotenango',
      					'Río Bravo',
      					'San Francisco Zapotitlán',
      					'San Lorenzo',
      					'Santo Domingo',
      					'Mazatenango',
      					'Samayac',
      					'San Gabriel',
      					'San Miguel Panán',
      					'Santo Tomás La Unión',
      					'Patulul',
      					'San Antonio',
      					'San José El Ídolo',
      					'San Pablo Jocopilas',
      					'Zunilito');

      	$deptos['Totonicapán']=array('Momostenango',
      					'San Francisco El Alto',
      					'San Andrés Xecul',
      					'Santa Lucía La Reforma',
      					'San Bartolo',
      					'Santa María Chiquimula',
      					'San Cristóbal Totonicapán',
      					'Totonicapán');
      	$deptos['Zacapa']=array('Cabañas',
                                      'La Unión',
                                      'Usumatlán',
                                      'Estanzuela',
                                      'Río Hondo',
                                      'Zacapa',
                                      'Gualán',
                                      'San Diego',
                                      'Huité',
                                      'Teculután');
        return $deptos;
    }
    public function get_files($filesTo)
    {
      if(!empty($filesTo)){
           $lkd = asset('storage/'.$filesTo);
           return $lkd;
      }
    }
    public function uploadFilesLocal($file,$path,$owname=1)
    {
       $extension = $file->getClientOriginalExtension();
       $name= ($owname==1?$this->randem(35).'.'.$extension:$file->getClientOriginalName());
       $filePath = $path . $name;
       Storage::put('public/'.$filePath, file_get_contents($file));
       return $filePath;
    }

    public function generateRandomFileManagerId($reference){
        $id = $reference . '_' . $this->randomizedId(8);
        $rep = archivosModel::where('id_file_manager', $id)->get();

        if (count($rep) > 0){
            return $this->generatedRandomFileManagerId($reference);
        }

        return $id;
    }

    ////// es lo mismo que el anterior pero con un nombre mas general
    public function generateRandomManagerId($reference){
        $id = $reference . '_' . $this->randomizedId(8);
        $rep = archivosModel::where('id_file_manager', $id)->get();

        if (count($rep) > 0){
            return $this->generatedRandomFileManagerId($reference);
        }

        return $id;
    }

    public function randomizedId($size){
        $a = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
        $b = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $c = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
        $all = array_merge($a, $b, $c);

        $rand = '';
        for ($i = 0; $i < $size; $i++){
            $pos = rand(0, count($all) - 1);
            $rand .= $all[$pos];
        }
        return $rand;
    }

    public function changeLog($params){
        $request = isset($params['request']) ? $params['request'] : null;
        $area = isset($params['area']) ? $params['area'] : null;
        $type = isset($params['type']) ? $params['type'] : null;
        $element_id = isset($params['element_id']) ? $params['element_id'] : null;

        $userModel = Auth::user();
        $user = $userModel->name.'-'.$userModel->usersys;

        $baseModel = new changeLogModel();
        $data = $request->only($baseModel->getFillable());
        $data['user'] = $user;
        $data['area'] = $area;
        $data['type'] = $type;
        $data['element_id'] = $element_id;

        $baseModel->fill($data)->save();
    }

    public function daysEsp(){
      return ["Sunday"=>"Domingo","Monday"=>"Lunes",
              "Tuesday"=>"Martes","Wednesday"=>"Miércoles",
              "Thursday"=>"Jueves","Friday"=>"Viernes","Saturday"=>"Sábado"];
    }
    public function monthEsp(){
      return ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    }
    public function calcularStock($bodega_id){
        //// se cargan todos los ingresos de bodega_id
        $ingresos = stockIngresosModel::where('bodega_id', $bodega_id)->get();

        //// se extraen los ids de los diferentes productos como llaves en un arreglo
        $equipos_id_arr = [];
        foreach($ingresos as $t){
            $equipos_id_arr[$t->producto_id] = 1;
        }

        /// se cicla a traves de cada producto para hayar sus movimientos
        foreach($equipos_id_arr as $key => $temp){
            ///------------- se inicializan los datos de este producto
            $producto_id = $key;
            $medida = productosModel::find($producto_id)->medida_final_id;
            $cantidad = 0;
            $costo_unitario = null; // se inicializa asi para evaluar si ya se lleno antes
            $valor_total = 0;

            $ingresos_temp = stockIngresosModel::where('bodega_id', $bodega_id)->where('producto_id', $key)->get();
            $egresos_temp = stockEgresosModel::where('bodega_id', $bodega_id)->where('producto_id', $key)->get();

            //// se mezclan los movimientos de ingreso y egreso de bodega y se coloca la fecha como llave y se ordenan por esta
            $movimientos_ord = [];;
            foreach($ingresos_temp as $v){
                $v->tipo_movimiento = 'ingreso';
                $movimientos_ord[strtotime($v->created_at)] = $v;
            }
            foreach($egresos_temp as $v){
                $v->tipo_movimiento = 'egreso';
                $movimientos_ord[strtotime($v->created_at)] = $v;
            }
            ksort($movimientos_ord); // se ordena por la llave que es la fecha en formato de tiempo absoluto

            /// se cicla a traves de cada movimiento de los productos para saber su costo en ese momento y aplicar prorrateo
            // si existe el id del producto hay al menos un movimiento de ingreso
            foreach($movimientos_ord as $temp){
                if ($temp->tipo_movimiento == 'ingreso'){
                    // cuando ingresan los productos el costo_unitario puede subir o bajar dependiendo del costo al que ingresaron los nuevos productos
                    // se toman los valores que se van acumulando y se consideran los nuevos valores del ingreso
                    if ($costo_unitario != null){
                        $prorrateo = $this->prorratear($costo_unitario_full, $cantidad, $temp->costo_unitario, $temp->cantidad);
                        $costo_unitario = $prorrateo['costo_unitario'];
                        $costo_unitario_full = $prorrateo['costo_unitario_full'];
                    }else{
                        $costo_unitario = $temp->costo_unitario;
                        $costo_unitario_full = $temp->costo_unitario;
                    }
                    $cantidad += $temp->cantidad;
                }else{
                    // cuando egresan los productos el costo_unitario se mantiene
                    $cantidad -= $temp->cantidad;
                }
            }
            $valor_total = $costo_unitario_full * $cantidad;

            ///se guardan o se actualizan los datos de este producto--------
            $stock_query = stockModel::where('bodega_id', $bodega_id)->where('producto_id', $producto_id)->get();

            if(count($stock_query) > 0){
                $stock = stockModel::where('bodega_id', $bodega_id)->where('producto_id', $producto_id)->first();
            }else{
                $stock = new stockModel;
            }

            $data = [
                'producto_id' => $producto_id,//
                'cantidad' => $cantidad,
                'medida_id' => $medida,
                'costo_unitario' => $costo_unitario,
                'valor_total' => $valor_total,
                'bodega_id' => $bodega_id
            ];

            $stock->fill($data)->save();
        }

    }

    public function prorratear($valor1, $cantidad1, $valor2, $cantidad2){
        $cantidadTotal = $cantidad1 + $cantidad2;
        $valorTotal = ($cantidad1 * $valor1) + ($cantidad2 * $valor2);
        return [
            'costo_unitario' => round(($valorTotal / $cantidadTotal) * 100) / 100, // por motivos de visualizacion
            'costo_unitario_full' => $valorTotal / $cantidadTotal // por motivos de precision en los decimales
        ];
    }

    public function calcularStockEquipo($bodega_id){
        //// se cargan todos los ingresos de bodega_id
        $ingresos = stockIngresosEquipoModel::where('bodega_id', $bodega_id)->get();

        //// se extraen los ids de los diferentes productos como llaves en un arreglo
        $equipos_id_arr = [];
        foreach($ingresos as $t){
            $equipos_id_arr[$t->equipo_id] = 1;
        }

        /// se cicla a traves de cada equipo para hayar sus movimientos
        foreach($equipos_id_arr as $key => $temp){
            ///------------- se inicializan los datos de este equipo
            $equipo_id = $key;
            $medida = equiposModel::find($equipo_id)->medida_id;
            $cantidad = 0;
            $costo_unidad = null; // se inicializa asi para evaluar si ya se lleno antes
            $valor_total = 0;

            $ingresos_temp = stockIngresosEquipoModel::where('bodega_id', $bodega_id)->where('equipo_id', $key)->get();
            $egresos_temp = stockEgresosEquipoModel::where('bodega_id', $bodega_id)->where('equipo_id', $key)->get();

            //// se mezclan los movimientos de ingreso y egreso de bodega y se coloca la fecha como llave y se ordenan por esta
            $movimientos_ord = [];;
            foreach($ingresos_temp as $v){
                $v->tipo_movimiento = 'ingreso';
                $movimientos_ord[strtotime($v->created_at)] = $v;
            }
            foreach($egresos_temp as $v){
                $v->tipo_movimiento = 'egreso';
                $movimientos_ord[strtotime($v->created_at)] = $v;
            }
            ksort($movimientos_ord); // se ordena por la llave que es la fecha en formato de tiempo absoluto

            /// se cicla a traves de cada movimiento de los equipos para saber su costo en ese momento y aplicar prorrateo
            // si existe el id del equipo hay al menos un movimiento de ingreso
            foreach($movimientos_ord as $temp){
                if ($temp->tipo_movimiento == 'ingreso'){
                    // cuando ingresan los equipos el costo_unidad puede subir o bajar dependiendo del costo al que ingresaron los nuevos equipos
                    // se toman los valores que se van acumulando y se consideran los nuevos valores del ingreso
                    if ($costo_unidad != null){
                        $prorrateo = $this->prorratear($costo_unidad_full, $cantidad, $temp->costo_unidad, $temp->cantidad);
                        $costo_unidad = $prorrateo['costo_unitario'];
                        $costo_unidad_full = $prorrateo['costo_unitario_full'];
                    }else{
                        $costo_unidad = $temp->costo_unidad;
                        $costo_unidad_full = $temp->costo_unidad;
                    }
                    $cantidad += $temp->cantidad;
                }else{
                    // cuando egresan los equipos el costo_unidad se mantiene
                    $cantidad -= $temp->cantidad;
                }
            }
            $valor_total = $costo_unidad_full * $cantidad;

            ///se guardan o se actualizan los datos de este equipo--------
            $stock_query = stockEquiposModel::where('bodega_id', $bodega_id)->where('equipo_id', $equipo_id)->get();

            if(count($stock_query) > 0){
                $stock = stockEquiposModel::where('bodega_id', $bodega_id)->where('equipo_id', $equipo_id)->first();
            }else{
                $stock = new stockEquiposModel;
            }

            $data = [
                'equipo_id' => $equipo_id,//
                'cantidad' => $cantidad,
                'medida_id' => $medida,
                'costo_unitario' => $costo_unidad,
                'valor_total' => $valor_total,
                'bodega_id' => $bodega_id
            ];

            $stock->fill($data)->save();
        }

    }
    public function hourSeconds($hour){
      $arr = explode(':', $hour);
      return ((INT)$arr[0] * 3600) + ((INT)$arr[1] * 60);
    }
    public function convertToHoursMins($time, $format = '%02d:%02d') {
      if ($time < 1) {
          return;
      }
      $hours = floor($time / 60);
      $minutes = ($time % 60);
      return sprintf($format, $hours, $minutes);
    }
    public function hourMinutes($hour){
      $arr = explode(':', $hour);
      return ((INT)$arr[0] * 60) + ((INT)$arr[1]);
    }
    public function secondsHour($hour){
      $hours = floor($hour / 3600);
      $minutes = floor(($hour / 60) % 60);
      $seconds = $hour % 60;
      return ($hours<=9?'0':'').$hours.":".($minutes<=9?'0':'').$minutes.":".($seconds<=9?'0':'').$seconds;
    }

    public function bettwenMin($hr1,$hr2)
    {
      $time1 = $this->hourSeconds($hr1);
      $time2 = $this->hourSeconds($hr2);
      $difference = $time2 - $time1;
      if($difference<0){
        return false;
      }else if($hr1==0&&$hr2==0){
        return false;
      }else{
        $difference = $difference/3600;
        $whole = floor($difference);      // 1
        $fraction = $difference - $whole;
        $minut = (INT)round($fraction*60);
        return ($whole<10?'0':'').$whole.':'.($minut<10?'0':'').$minut.':00';
      }
    }

    public function info_bodega($bodega_id){
        $bodega = bodegasModel::find($bodega_id);

        if ($bodega->tipo == 'cliente'){
            $sede = companyclientsBranchModel::where('bodega_id', $bodega_id)->first();
            $cliente = companyclientsModel::find($sede->idClient);

            $element = [
                'cliente' => $cliente,
                'sede' => $sede,
            ];

        }else{
            $company = companiesModel::where('bodega_id', $bodega_id)->first();
            $clientes_assoc = companyclientsAssocModel::where('idCompany', $company->id)->get();
            $clientes = [];
            foreach($clientes_assoc as $temp){
                $clientes[] = companyclientsModel::find($temp->idClient);
            }


            $colaboradores = [];
            $colaboradores_assoc = company_colab_assocModel::where('idCompany', $company->id)->get();
            $colaboradores = [];
            foreach($colaboradores_assoc as $temp){
                $colaboradores[] = colaboradores::find($temp->idColab);
            }

            $element = [
                'company' => $company,
                'clientes' => $clientes,
                'colaboradores' => $colaboradores
            ];
        }

        return $element;
    }

    public static function list_alertas(){

        $alertas_data = alertasModel::get();
        $alertas = [];
        foreach($alertas_data as $temp){
            $alertas[] = $temp;
        }

        return $alertas;
    }

    public function revisar_precio_producto($producto_id){
        $producto = productosModel::find($producto_id);
        $stock = stockModel::where('producto_id', $producto_id)->get();
        foreach($stock as $temp){
            $ganancia_neta = $temp->costo_unitario * $producto->porcentaje_ganancia / 100;
            $precio_sugerido = $temp->costo_unitario + $ganancia_neta;

            $alerta_anterior = alertasModel::where('referencia', 'producto' . $producto_id . 'bodega' . $temp->bodega_id)->get();

            if ($producto->precio_cliente < $precio_sugerido){
                if (count($alerta_anterior) < 1){
                    $bodega = bodegasModel::find($temp->bodega_id);
                    $alerta = new alertasModel;

                    $alerta->fill(
                        [
                            'elemento_id' => $producto_id,
                            'referencia' => 'producto' . $producto_id . 'bodega' . $temp->bodega_id,
                            'tipo' => 'precio',
                            'mensaje' => 'El precio del producto <b>' . $producto->nombre . '</b> de bodega <b>' . $bodega->nombre . '</b> se encuentra por debajo del sugerido: Q.' . number_format($precio_sugerido, 2),
                            'nombre_modulo' => 'productos',
                            'url' => 'productos'
                        ]
                    );
                    $alerta->save();
                }

            }else{
                $alerta = alertasModel::where('referencia', 'producto' . $producto_id . 'bodega' . $temp->bodega_id)->get();
                foreach($alerta as $temp){
                    $temp->delete();
                }
            }
        }
    }

    public function revisar_stock_producto($producto_id){
        $producto = productosModel::find($producto_id);
        $stock = stockModel::where('producto_id', $producto_id)->get();
        foreach($stock as $temp){

            $alerta_anterior = alertasModel::where('referencia', 'producto' . $producto_id . 'bodega' . $temp->bodega_id . 'stock')->get();

            if ($temp->cantidad < $producto->cantidad_minima_aceptable){
                if (count($alerta_anterior) < 1){
                    $bodega = bodegasModel::find($temp->bodega_id);
                    $alerta = new alertasModel;

                    $alerta->fill(
                        [
                            'elemento_id' => $producto_id,
                            'referencia' => 'producto' . $producto_id . 'bodega' . $temp->bodega_id . 'stock',
                            'tipo' => 'stock',
                            'mensaje' => 'La cantidad del producto <b>' . $producto->nombre . '</b> de bodega <b>' . $bodega->nombre . '</b> es menora a la mínima aceptable: ' . $producto->cantidad_minima_aceptable,
                            'nombre_modulo' => 'productos',
                            'url' => 'productos'
                        ]
                    );
                    $alerta->save();
                }

            }else{
                $alerta = alertasModel::where('referencia', 'producto' . $producto_id . 'bodega' . $temp->bodega_id . 'stock')->get();
                foreach($alerta as $temp){
                    $temp->delete();
                }
            }
        }
    }
}
