<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\unidadesModel;
use App\adminModels\conversionesModel;

class conversionesController extends Controller
{

    private $baseModel;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->baseModel = new conversionesModel;
    }

    public function index(){
        $data = $this->baseModel::orderBy("created_at")->get();

        $tipos = [
            ['volumen', 'volumen'],
            ['longitud', 'longitud'],
            ['contable', 'contable'],
            ['otro', 'otro'],
        ];

        $operaciones = [
            ['dividir', 'dividir'],
            ['multiplicar', 'multiplicar'],
        ];

        $unidades = unidadesModel::get();
        $unidades_arr = [[null, '- - -']];
        $unidades_obj = [];
        foreach($unidades as $temp){
            $unidades_arr[] = [$temp->id, $temp->nombre];
            $unidades_obj[$temp->id] = $temp;
        }

        foreach($data as $temp){
            $temp->unidad_inicial = unidadesModel::find($temp->unidad_inicial_id);
            $temp->unidad_final = unidadesModel::find($temp->unidad_final_id);
        }


        return view('admin.productos.conversionesIndex', [
                'menubar'=> $this->list_sidebar(),
                'data' => $data,
                'tipos' => $tipos,
                'unidades_arr' => $unidades_arr,
                'unidades_obj' => $unidades_obj,
                'operaciones' => $operaciones
            ]);
    }

    public function store(Request $request) {
        $data = $request->only($this->baseModel->getFillable());
        $this->baseModel->fill($data)->save();
        return redirect()->back()->with('success','Guardado correctamente!');
    }

    public function update(Request $request, $id) {
        $model = $this->baseModel::find($id);
        $data = $request->only($this->baseModel->getFillable());
        $model->fill($data)->save();
        return redirect()->back()->with('info','Actualizado correctamente!');
    }

    public function destroy($id) {
        try {
        $this->baseModel::destroy($id);
            return redirect()->back()->with('warning','Borrado correctamente');
        }catch (\Exception $e) {
            return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
        }
    }
}
