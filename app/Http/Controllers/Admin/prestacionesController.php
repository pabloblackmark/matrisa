<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\adminModels\colaboradores;

class prestacionesController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'prestaciones';
    }



    public function index(Request $request) {


        $colab = colaboradores::find($request->id_colab);

        $sueldo = $colab->sueldo*30;


        // $fecha_inicio = "2020-01-01";
        $fecha_inicio = $colab->fecha_ingreso;
        $fecha_fin = date('Y-m-d');


        $data['inicio'] = date_format(date_create($fecha_inicio), 'd-m-Y');
        $data['fin'] = date_format(date_create($fecha_fin), 'd-m-Y');


        $inicio = date_create($fecha_inicio);
        $fin = date_create($fecha_fin);

        $diff = date_diff($inicio,$fin);


        $dias = $diff->format('%a')+1;
        $meses = ($diff->format('%y')*12) + $diff->format('%m');
        $anos = $diff->format('%y');


        $data['dias'] = $dias;
        $data['meses'] = $meses;
        $data['anos'] = $anos;

        
        $data['sueldo'] = $sueldo;


        // indemnizacion
        $indemnizacion = ($sueldo/365)*$dias;
        $data['indemnizacion'] = number_format($indemnizacion, 2, '.', ',');


        //vacaciones
        //$dias_vacas = 20;
        $dias_vacas = ($dias*15)/365;
        $dias_vacas = round($dias_vacas, 0, PHP_ROUND_HALF_DOWN);

        // $data['dias_vacas'] = $dias_vacas;

        // $vacas = ($sueldo/365)*($dias_vacas/30)*$dias;
        $vacas = ($sueldo/365)*(15/30)*$dias;
        $data['vacas'] = number_format($vacas, 2, '.', ',');


        // dias ciclo presente
        $ano_actual = date('Y')."-01-01";
        $ano_actual = date_create($ano_actual);

        $diff_actual = date_diff($ano_actual, $fin);

        $dias_actual = $diff_actual->format('%a')+1;
        $data['dias_actual'] = $dias_actual;




        // aguinaldo

        $last_year = date("Y", strtotime ('-1 year')) ;

        // echo $last_year;

        $ano_aguinaldo = $last_year."-11-30";
        $ano_aguinaldo = date_create($ano_aguinaldo);

        $diff_aguinaldo = date_diff($ano_aguinaldo, $fin);

        $dias_aguinaldo = $diff_aguinaldo->format('%a')+1;
        $data['dias_aguinaldo'] = $dias_aguinaldo;

        $aguinaldo = ($sueldo/365)*$dias_aguinaldo;
        $data['aguinaldo'] = number_format($aguinaldo, 2, '.', ',');


        // bono 14
        $ano_bono_14 = $last_year."-06-30";
        $ano_bono_14 = date_create($ano_bono_14);

        $diff_bono_14 = date_diff($ano_bono_14, $fin);

        $dias_bono_14 = $diff_bono_14->format('%a')+1;
        $data['dias_bono_14'] = $dias_bono_14;

        $bono_14 = ($sueldo/365)*$dias_bono_14;
        $data['bono_14'] = number_format($bono_14, 2, '.', ',');



        // salario prendiente
        $mes_actual = date('Y')."-".date('m')."-01";
        $mes_actual = date_create($mes_actual);

        $diff_mes = date_diff($mes_actual, $fin);

        $dias_mes = $diff_mes->format('%a')+1;
        $data['dias_mes'] = $dias_mes;



        // TOTAL a pagar
        $total = $vacas + $aguinaldo + $bono_14;
        $data['total'] = number_format($total, 2, '.', ',');


            return view('admin.prestaciones.show',
                      ['menubar'=> $this->list_sidebar(),
                       'data' => $data,
                       'colab' => $colab
                      ]);
    }



    public function liquidacion(Request $request) {

        $colab = colaboradores::find($request->id_colab);

        $sueldo = $colab->sueldo*30;


        // $fecha_inicio = "2020-01-01";
        $fecha_inicio = $colab->fecha_ingreso;
        $fecha_fin = date('Y-m-d');


        $data['sueldo'] = $sueldo;
        $data['inicio'] = date_format(date_create($fecha_inicio), 'd-m-Y');
        $data['fin'] = date_format(date_create($fecha_fin), 'd-m-Y');


        $inicio = date_create($fecha_inicio);
        $fin = date_create($fecha_fin);

        $diff = date_diff($inicio,$fin);


        $dias = $diff->format('%a')+1;
        $meses = ($diff->format('%y')*12) + $diff->format('%m');
        $anos = $diff->format('%y');


        $data['dias'] = $dias;
        $data['meses'] = $meses;
        $data['anos'] = $anos;

        


        // indemnizacion
        $indemnizacion = ($sueldo/365)*$dias;
        $data['indemnizacion'] = number_format($indemnizacion, 2, '.', ',');


        //vacaciones
        //$dias_vacas = 20;
        $dias_vacas = ($dias*15)/365;
        $dias_vacas = round($dias_vacas, 0, PHP_ROUND_HALF_DOWN);

        // $data['dias_vacas'] = $dias_vacas;

        // $vacas = ($sueldo/365)*($dias_vacas/30)*$dias;
        $vacas = ($sueldo/365)*(15/30)*$dias;
        $data['vacas'] = number_format($vacas, 2, '.', ',');


        // dias ciclo presente
        $ano_actual = date('Y')."-01-01";
        $ano_actual = date_create($ano_actual);

        $diff_actual = date_diff($ano_actual, $fin);

        $dias_actual = $diff_actual->format('%a')+1;
        $data['dias_actual'] = $dias_actual;




        // aguinaldo

        $last_year = date("Y", strtotime ('-1 year')) ;

        // echo $last_year;

        $ano_aguinaldo = $last_year."-11-30";
        $ano_aguinaldo = date_create($ano_aguinaldo);

        $diff_aguinaldo = date_diff($ano_aguinaldo, $fin);

        $dias_aguinaldo = $diff_aguinaldo->format('%a')+1;
        $data['dias_aguinaldo'] = $dias_aguinaldo;

        $aguinaldo = ($sueldo/365)*$dias_aguinaldo;
        $data['aguinaldo'] = number_format($aguinaldo, 2, '.', ',');


        // bono 14
        $ano_bono_14 = $last_year."-06-30";
        $ano_bono_14 = date_create($ano_bono_14);

        $diff_bono_14 = date_diff($ano_bono_14, $fin);

        $dias_bono_14 = $diff_bono_14->format('%a')+1;
        $data['dias_bono_14'] = $dias_bono_14;

        $bono_14 = ($sueldo/365)*$dias_bono_14;
        $data['bono_14'] = number_format($bono_14, 2, '.', ',');



        // salario prendiente
        $mes_actual = date('Y')."-".date('m')."-01";
        $mes_actual = date_create($mes_actual);

        $diff_mes = date_diff($mes_actual, $fin);

        $dias_mes = $diff_mes->format('%a')+1;
        $data['dias_mes'] = $dias_mes;

        $salario = ($sueldo/30)*$dias_mes;
        $data['salario'] = number_format($salario, 2, '.', ',');


        // bonificacion incentivo pendiente
        $bono = 1000;
        $data['bono'] = $bono;
        $pago_bono = ($bono/30)*$dias_mes;
        $data['pago_bono'] = number_format($pago_bono, 2, '.', ',');

        // horas extras pendiente
        $hora_extra = 100;
        $horas = 13;

        $data['hora_extra'] = $hora_extra;
        $data['horas'] = $horas;

        $pago_horas = $hora_extra*$horas;
        $data['pago_extra'] = number_format($pago_horas, 2, '.', ',');

        // TOTAL a pagar
        $total = $indemnizacion + $vacas + $aguinaldo + $bono_14 + $salario + $pago_bono + $pago_horas;
        $data['total'] = number_format($total, 2, '.', ',');


            return view('admin.liquidacion.show',
                      ['menubar'=> $this->list_sidebar(),
                       'data' => $data,
                       'colab' => $colab
                      ]);
    }



    public function prestaciones_bk_original(Request $request) {

        $fecha_inicio = "2020-01-01";
        $fecha_fin = date('Y-m-d');

        // $fecha_inicio = "2015-06-23";
        // $fecha_fin = "2011-06-23";

        $data['inicio'] = "2020-01-01";
        $data['fin'] = date('Y-m-d');


        $inicio = date_create($fecha_inicio);
        $fin = date_create($fecha_fin);

        $diff = date_diff($inicio,$fin);


        $dias = $diff->format('%a')+1;
        $meses = ($diff->format('%y')*12) + $diff->format('%m');
        $anos = $diff->format('%y');


        $data['dias'] = $dias;
        $data['meses'] = $meses;
        $data['anos'] = $anos;

        $sueldo = 7000;
        $data['sueldo'] = $sueldo;


        // indemnizacion
        $indemnizacion = ($sueldo/365)*$dias;
        $data['indemnizacion'] = number_format($indemnizacion, 2, '.', ',');


        //vacaciones
        //$dias_vacas = 20;
        $dias_vacas = ($dias*15)/365;
        $dias_vacas = round($dias_vacas, 0, PHP_ROUND_HALF_DOWN);

        // $data['dias_vacas'] = $dias_vacas;

        // $vacas = ($sueldo/365)*($dias_vacas/30)*$dias;
        $vacas = ($sueldo/365)*(15/30)*$dias;
        $data['vacas'] = number_format($vacas, 2, '.', ',');


        // dias ciclo presente
        $ano_actual = date('Y')."-01-01";
        $ano_actual = date_create($ano_actual);

        $diff_actual = date_diff($ano_actual, $fin);

        $dias_actual = $diff_actual->format('%a')+1;
        $data['dias_actual'] = $dias_actual;




        // aguinaldo

        $last_year = date("Y", strtotime ('-1 year')) ;

        // echo $last_year;

        $ano_aguinaldo = $last_year."-11-30";
        $ano_aguinaldo = date_create($ano_aguinaldo);

        $diff_aguinaldo = date_diff($ano_aguinaldo, $fin);

        $dias_aguinaldo = $diff_aguinaldo->format('%a')+1;
        $data['dias_aguinaldo'] = $dias_aguinaldo;

        $aguinaldo = ($sueldo/365)*$dias_aguinaldo;
        $data['aguinaldo'] = number_format($aguinaldo, 2, '.', ',');


        // bono 14
        $ano_bono_14 = $last_year."-06-30";
        $ano_bono_14 = date_create($ano_bono_14);

        $diff_bono_14 = date_diff($ano_bono_14, $fin);

        $dias_bono_14 = $diff_bono_14->format('%a')+1;
        $data['dias_bono_14'] = $dias_bono_14;

        $bono_14 = ($sueldo/365)*$dias_bono_14;
        $data['bono_14'] = number_format($bono_14, 2, '.', ',');



        // salario prendiente
        $mes_actual = date('Y')."-".date('m')."-01";
        $mes_actual = date_create($mes_actual);

        $diff_mes = date_diff($mes_actual, $fin);

        $dias_mes = $diff_mes->format('%a')+1;
        $data['dias_mes'] = $dias_mes;

        $salario = ($sueldo/30)*$dias_mes;
        $data['salario'] = number_format($salario, 2, '.', ',');


        // bonificacion incentivo pendiente
        $bono = 1000;
        $data['bono'] = $bono;
        $pago_bono = ($bono/30)*$dias_mes;
        $data['pago_bono'] = number_format($pago_bono, 2, '.', ',');

        // horas extras pendiente
        $hora_extra = 100;
        $horas = 13;

        $data['hora_extra'] = $hora_extra;
        $data['horas'] = $horas;

        $pago_horas = $hora_extra*$horas;
        $data['pago_extra'] = number_format($pago_horas, 2, '.', ',');

        // TOTAL a pagar
        $total = $indemnizacion + $vacas + $aguinaldo + $bono_14 + $salario + $pago_bono + $pago_horas;
        $data['total'] = number_format($total, 2, '.', ',');


            return view('admin.prestaciones.show',
                      ['menubar'=> $this->list_sidebar(),
                       'data' => $data
                      ]);
    }

}
