<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\warehouseProductsModel;
class productsWarehouseController extends Controller
{
  private $baseModel;
  public function __construct()
  {
    $this->middleware('auth:admin');
    $this->baseModel=new warehouseProductsModel;
  }
  public function store(Request $request) {
    $data = $request->only($this->baseModel->getFillable());
    $data["datein"] = date("Y-m-d",strtotime(str_replace("/","-",$request->datein)));
    $this->baseModel->fill($data)->save();
    return redirect()->route("admin.products.edit",$request->id_product)->with('success','Guardado correctamente!');
  }
  public function update(Request $request, $id) {
    // dd($request->imgs);
    $model = $this->baseModel::find($id);
    $data = $request->only($this->baseModel->getFillable());
    $imabs = [];
    if($request->archvo){
      $data["images"] = json_decode($model->images,TRUE);
      foreach($request->archvo AS $fils){
        $data["images"][] = self::uploadFiles($fils,'uploadfiles/');
      }
    }
    $data["images"] = json_encode($request->imgs);
    if(!empty($request->colors))
      $data["colors"] = array_merge(json_decode($model->colors,TRUE),$request->colors);
    if(!empty($request->sizes))
      $data["sizes"] = array_merge(json_decode($model->sizes,TRUE),$request->sizes);
    $model->fill($data)->save();
    return redirect()->route("admin.products.edit",$id)->with('info','Actualizado correctamente!');
  }
  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }
  public function delFile($id,$idfrom){
    $fieto = base64_decode($id);
    $finded = $this->baseModel::find($idfrom);
    $rep = json_decode($finded->images);
    $newF = [];
    foreach($rep AS $sl){
      if($sl!==$fieto){
        $newF[] = $sl;
      }
    }
    // dd($newF,$fieto);
    $finded->images = json_encode($newF);
    $finded->save();
    $this->deleteFiles($fieto);
    return redirect()->route("admin.products.edit",$idfrom)->with('warning','Borrado correctamente');
  }
}
