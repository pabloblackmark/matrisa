<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\horaryModel;
use Illuminate\Support\Facades\Storage;

class horaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function show($id){

      $reference = explode('_', $id);
      $name = $reference[0];
      $breadcrumb = session('breadcrumb') != null ? session('breadcrumb') : '';

      $files = horaryModel::where('id_horary_manager', $id)->orderBy("id")->get();

      // foreach($files as $tl){
      //     $tl['file'] = Storage::url($tl->url);
      // }

      return view('admin.horary.show',
            [
                'menubar'=> $this->list_sidebar(),
                'files'=>$files,
                'id_file_manager' => $id,
                'breadcrumb' => $breadcrumb,
                'name' => $name
            ]);
    }

    public function store(Request $request, $id) {

      $archivos = new horaryModel;
      $data = $request->only($archivos->getFillable());
      $data['id_horary_manager'] = $id;
      // $data['admin_horary'] = $this->hourMinutes($request->admin_horary);
      // dd($request->horas_diurnas,$request->horas_nocturnas,
      //   $request->tiempo_receso,$request->horas_mixtas);
      $data['horas_diurnas'] = $this->hourMinutes($request->horas_diurnas);
      $data['horas_nocturnas'] = $this->hourMinutes($request->horas_nocturnas);
      $data['tiempo_receso'] = $this->hourMinutes($request->tiempo_receso);
      $data['horas_mixtas'] = $this->hourMinutes($request->horas_mixtas);
      // dd($data);
      if ($request->tipo == "semana"){
          if ($request->lunes){
              $archivos = new horaryModel;
              $data['day'] = "Lunes";
              $archivos->fill($data)->save();
          }

          if ($request->martes){
              $archivos = new horaryModel;
              $data['day'] = "Martes";
              $archivos->fill($data)->save();
          }

          if ($request->miercoles){
              $archivos = new horaryModel;
              $data['day'] = "Miércoles";
              $archivos->fill($data)->save();
          }

          if ($request->jueves){
              $archivos = new horaryModel;
              $data['day'] = "Jueves";
              $archivos->fill($data)->save();
          }

          if ($request->viernes){
              $archivos = new horaryModel;
              $data['day'] = "Viernes";
              $archivos->fill($data)->save();
          }

          if ($request->sabado){
              $archivos = new horaryModel;
              $data['day'] = "Sábado";
              $archivos->fill($data)->save();
          }

          if ($request->domingo){
              $archivos = new horaryModel;
              $data['day'] = "Domingo";
              $archivos->fill($data)->save();
          }

      }else{

          $archivos->fill($data)->save();
      }


      return redirect()->back()->with('success','Guardado correctamente!');
    }

    public function update(Request $request, $id_file_manager, $id) {
      $archivos = new horaryModel;
      $elm = $archivos::find($id);
      $data = $request->only($archivos->getFillable());
      $data['horas_diurnas'] = $this->hourMinutes($request->horas_diurnas);
      $data['horas_nocturnas'] = $this->hourMinutes($request->horas_nocturnas);
      $data['tiempo_receso'] = $this->hourMinutes($request->tiempo_receso);
      $data['horas_mixtas'] = $this->hourMinutes($request->horas_mixtas);
      $elm->fill($data)->save();

      return redirect()->back()->with('info','Actualizado correctamente!');
    }

    public function destroy($id_file_manager, $id_file) {

      $elm = horaryModel::find($id_file);

      $elm->delete($id_file);

      return redirect()->back()->with('warning','Borrado correctamente!');
    }

}
