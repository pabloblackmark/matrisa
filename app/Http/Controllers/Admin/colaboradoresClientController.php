<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companyclientsModel;
use App\adminModels\companiesModel;
use App\adminModels\puestosModel;
use App\adminModels\tasksModel;
use App\adminModels\horaryModel;
use App\adminModels\companyclientsBranchModel;
use App\adminModels\companyclientsBranchAreaModel;

class colaboradoresClientController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'colaboradoresCompany';
    }

    public function index($company_id, $client_id, $assoc_id){ //id company, id cliente e id asociacion en tabla
        // status 0 es disponible y no asignado antes, status = 1 asignado osea no disponible, status = 2 es estuvo asignado pero ya no, osea disponible
        // asignados a esta compania
        $data_company_colab_assoc = company_colab_assocModel::where('idCompany', $company_id)->where('status', '1')->get();
        $company_assigned = [];
        foreach($data_company_colab_assoc as $d){
            $company_assigned[] = $d->idColab;
        }

        $data_colabs = colaboradores::whereIn('id', $company_assigned)->get();

        // asignados el actual cliente
        $data_client_colab_assoc = client_colab_assocModel::where('idClient', $client_id)->where('status', '1')->get();
        $assi = [];
        foreach($data_client_colab_assoc as $temp){
            $assi[$temp->id] = [
                'id' => $temp->id,
                'sede' => $this->sede($temp->idBranch),
                'puesto' => $this->puesto($temp->idPuesto)['nombre'],
                'tareas' => $this->puesto($temp->idPuesto)['tareas'],
                'area' => $this->area($temp->idArea),
                'colaborador' => $this->colaborador($temp->idColab),
                'tipo' => $temp->tipo,
                'inicio' => $temp->inicio,
                'fin' => $temp->fin,
                'hora_diurna' => $this->puesto($temp->idPuesto)['hora_diurna'],
                'hora_nocturna' => $this->puesto($temp->idPuesto)['hora_nocturna'],
                'extra_diurna' => $this->puesto($temp->idPuesto)['extra_diurna'],
                'extra_nocturna' => $this->puesto($temp->idPuesto)['extra_nocturna'],
                'extra_especial' => $this->puesto($temp->idPuesto)['extra_especial'],
                'viaticos' => $this->puesto($temp->idPuesto)['viaticos'],
                'viaticos_movil' => $this->puesto($temp->idPuesto)['viaticos_movil'],
            ];
        }

        // filtrar los disponibles aun para esta empresa y no asignados a ningun cliente
        $data4 = client_colab_assocModel::where('status', '1')->get();
        $client_assigned = [];
        foreach($data4 as $d4){
            $client_assigned[] = $d4->idColab;
        }

        // Filtro para colaboradores ya asignados
        // $company_assigned_filtered = [];
        // foreach($company_assigned as $t0){
        //     if (!in_array($t0, $client_assigned)){
        //         $company_assigned_filtered[] = $t0;
        //     }
        // }

        $company_assigned_filtered = $company_assigned;

        $data2 = colaboradores::whereIn('id', $company_assigned_filtered)->get();

        $disp = ["Sin definir" => []];
        foreach($data2 as $k => $v){
            if ($v->puesto){
                $disp[$v->puesto][$v->id] = $v->apellidos . ', ' . $v->nombres;
            }else{
                $disp["Sin definir"][$v->id] = $v->apellidos . ', ' . $v->nombres;;
            }
        }

        $company = isset(companiesModel::find($company_id)['company']) ? companiesModel::find($company_id)['company'] : '';
        $client = isset(companyclientsModel::find($client_id)['clientName']) ? companyclientsModel::find($client_id)['clientName'] : '';

        //puestos
        $puestos_data = puestosModel::get();
        $puestos = [['', '- - -']];
        foreach($puestos_data as $d){
            //$tareas = tasksModel::where('id_task_manager', $d->id_task_manager);
            $horary = horaryModel::where('id_horary_manager', $d->id_horary_manager)->get();
            //El puesto tiene horarios programados
            if (count($horary) > 0){
                $puestos[] = [$d->id, $d->nombre];
            }
        }

        //sedes
        $sede_data = companyclientsBranchModel::where('idClient', $client_id)->get();
        $sede = [['', '- - -']];
        foreach($sede_data as $d){
            $sede[] = [$d->id, $d->namebranch];
        }

        //areas
        $areas = [];
        foreach($sede_data as $d){
            $area_data = companyclientsBranchAreaModel::where('idBranch', $d->id)->get();
            foreach($area_data as $d2){
                $areas[$d->id][$d2->id] = $d2->name;
            }
        }

        return view('admin.colaboradores.assignClient',
              ['menubar'=> $this->list_sidebar(),
               'assi' => $assi,
               'disp' => $disp,
               'idCompany' => $company_id,
               'idClient' => $client_id,
               'companyName' => $company,
               'clientName' => $client,
               'puestos' => $puestos,
               'sede' => $sede,
               'areas' => $areas,
               'idAssocCLient' => $assoc_id
              ]);
    }

    public function assign(Request $request, $idCompany, $idClient, $idAssoc){
        $model = new client_colab_assocModel;
        $data = $request->only($model->getFillable());
        $data['status'] = 1;
        $data['idClient'] = $idClient;
        $model->fill($data)->save();
        return redirect()->back()->with('success','Asignado correctamente');
    }

    public function unassign($company_id, $client_id, $id){
        $elem = client_colab_assocModel::find($id);
        $elem->status = 2;
        $elem->save();

        return redirect()->back()->with('success','Desasignado correctamente');
    }

    public function show($id=null){
        $edit = false;
        $data = [];

        if ($id != null){
            $data = colaboradores::find($id);
            $edit = true;
        }

        return view('admin.colaboradores.show',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'edit' => $edit
              ]);
    }

    public function store(Request $request) {
        $model = new colaboradores;
        $data = $request->only($model->getFillable());
        try{

            if ($request->file('foto')){
                $foto = $request->file('foto')->storePublicly('fotos', 'public');
                $data['foto'] = $foto;
            }

            $model->fill($data)->save();

            return redirect($this->back)->with('success','Guardado correctamente!');
        }catch(\Exception $e){
            return redirect()->back()->with('warning','No se pudieron guardar los datos: ' . $e);
        }

    }

    public function update(Request $request, $id) {
        $model = new colaboradores;
        $finded = $model::find($id);
        $dataMod = $request->only($model->getFillable());

        if ($request->file('foto')){
            $foto = $request->file('foto')->storePublicly('fotos', 'public');
            $dataMod['foto'] = $foto;
        }

        $finded->fill($dataMod)->save();
        return redirect($this->back)->with('info','Actualizado correctamente!');
    }

    public function destroy($id) {
        try {
            colaboradores::destroy($id);
            return redirect()->back()->with('warning','Borrado correctamente!');
          }catch (\Exception $e) {
           return redirect()->back()->with('error','No se puede eliminar porque se está utilizando por un usuario del módulo -Personal-. Primero debe eliminar esta configuración.'.$e->getCode());
        }
    }

    public function sede($id){
        $data = companyclientsBranchModel::find($id);
        if ($data != null){
            return $data->namebranch;
        }
        return '';
    }

    public function puesto($id){
        $data = puestosModel::find($id);
        if ($data != null){
            $tasks = puestosModel::find($id)->tasks;
            return [
                'nombre' => $data->nombre,
                'tareas' => $tasks,
                'hora_diurna' => $data->payDT,
                'hora_nocturna' => $data->payNT,
                'extra_diurna' => $data->payEDT,
                'extra_nocturna' => $data->payENT,
                'extra_especial' => $data->extraPay,
                'viaticos' => $data->perDiem,
                'viaticos_movil' => $data->movil,
                ];
        }
        return [
            'nombre' => '',
            'tareas' => [],
            'hora_diurna' => '',
            'hora_nocturna' => '',
            'extra_diurna' => '',
            'extra_nocturna' => '',
            'extra_especial' => '',
            'viaticos' => '',
            'viaticos_movil' => '',
        ];
    }

    public function area($id){
        $data = companyclientsBranchAreaModel::find($id);
        if ($data != null){
            return $data->name;
        }
        return '';
    }

    public function colaborador($id){
        $data = colaboradores::find($id);
        if ($data != null){
            return $data->nombres . ' ' . $data->apellidos;
        }
        return '';
    }


}
