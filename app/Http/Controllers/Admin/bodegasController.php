<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\bodegasModel;
use App\adminModels\companiesModel;
use App\adminModels\companyclientsModel;
use App\adminModels\companyclientsBranchModel;
use App\adminModels\companyclientsAssocModel;

class bodegasController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
        $this->back = 'bodegas';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = bodegasModel::where('tipo', 'empresa')->get();
        $empresas = companiesModel::get();
        foreach($data as $temp){
            $temp->empresa = companiesModel::where('bodega_id', $temp->id)->first();
        }

        return view('admin.bodegas.index',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $elem = new bancosModel;
        $elem->fill($request->only($elem->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd($id);
        $data = [];
        return view('admin.bodegas.stock',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showBodegasClientes($company_id)
    {
        $assoc = companyclientsAssocModel::where('idCompany', $company_id)->get();
        $empresa = companiesModel::find($company_id);
        $clientes =
        $data = [];
        foreach($assoc as $temp){
            $cliente = companyclientsModel::find($temp->idClient);
            $sedes = companyclientsBranchModel::where('idClient', $temp->idClient)->get();
            foreach($sedes as $temp2){
                $data[] = [
                    'bodega' => bodegasModel::find($temp2->bodega_id),
                    'cliente' => $cliente
                ];
            }
        }
        return view('admin.bodegas.indexClientes',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'empresa' => $empresa,
                'company_id' => $company_id
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = new bancosModel;
        $elem = $model->find($id);
        $elem->fill($request->only($model->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        bancosModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }
}
