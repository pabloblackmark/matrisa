<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\roles_names;

class permissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->indexUrl= 'admin.permissions.index';
    }
    public function index(){
      $listof=roles_names::orderBy("groupacc")->get();
      return view('admin.permission.show',
            ['menubar'=> $this->list_sidebar(),
             'permissions'=>$listof
            ]);

    }

    public function store(Request $request) {
      $validator = $request->validate([
          'naccess' => 'required',
          'archaccess' => 'required',
      ]);
      $access = new roles_names;
      $data = $request->only($access->getFillable());
      $data["publc"] = (isset($data["publc"])?1:0);
      $access->fill($data)->save();
      return redirect()->route($this->indexUrl)
      ->with('success','Guardado correctamente!');
    }
    public function destroy($id) {
      roles_names::destroy($id);
      return redirect()->route($this->indexUrl)
      ->with('warning','Borrado correctamente!');
    }
    public function update(Request $request, $id) {
      $accesscL = new roles_names;
      $access = $accesscL::find($id);
      $data = $request->only($accesscL->getFillable());
      $data["publc"] = (isset($data["publc"])?1:0);
      $access->fill($data)->save();
      // dd($access);
      // return redirect('pages/aracislemler/'.$vehicle->id);
      return redirect()->route($this->indexUrl)
      ->with('info','Actualizado correctamente!');
    }
}
