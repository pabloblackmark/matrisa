<?php

namespace App\Http\Controllers\admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\UserAdmin;
use App\adminModels\roles;
use App\adminModels\mensajesModel;
use App\adminModels\colaboradores;

use App\adminModels\mensajes_gruposModel;

use App\adminModels\companiesModel;

use Illuminate\Support\Facades\Hash;

use Session;


use App\adminModels\company_colab_assocModel;


class mensajesController extends Controller
{
  protected $redirecUlr;

  public function __construct(Request $request)
  {
      $this->middleware('auth:admin');


  }

  public function guardar_sesion($request) {

      if ($request->id_empresa=='---')  {
            session(['id_empresa' => '']);

            $array_colabs = array();
            session(['array_colabs' => $array_colabs]);
      }elseif ($request->id_empresa!='') {
            session(['id_empresa' => $request->id_empresa]);

            $lista_colabs = company_colab_assocModel::where('idCompany', $request->id_empresa)->get();

            $array_colabs = array();

            foreach($lista_colabs AS $colab) {
                $array_colabs[] = $colab->idColab;
            }

            session(['array_colabs' => $array_colabs]);

      }

  }

  public function index(Request $request) {

    $this->guardar_sesion($request);
    $empresas = companiesModel::get();

    // echo session('id_empresa');
    
    $mensajes = mensajesModel::where('de_admin', Auth::user()->id)->orWhere('para_admin', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

    $mensajes_data = array();
    

    foreach($mensajes AS $mensaje) {

        if (session('id_empresa')!='') {

            if (in_array($mensaje->de_user, session('array_colabs'))||in_array($mensaje->para_user, session('array_colabs'))) {

                $mensaje_data['id'] = $mensaje->id;
                $mensaje_data['de'] = $mensaje->de;
                $mensaje_data['para'] = $mensaje->para;
                $mensaje_data['titulo'] = $mensaje->titulo; 
                $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
                $mensaje_data['tipo'] = $mensaje->tipo;
                $mensaje_data['status'] = $mensaje->status;
                $mensaje_data['mensaje'] = $mensaje->mensaje;
                $mensaje_data['respuestas'] = json_decode($mensaje->respuestas, true);

                if ($mensaje->status==0) {
                    $mensaje_data['status_texto'] = 'Enviado';
                } else if ($mensaje->status==1) {
                    $mensaje_data['status_texto'] = 'Leido';
                } else if ($mensaje->status==2) {
                    $mensaje_data['status_texto'] = 'Respondido';
                }

                $mensajes_data[] = $mensaje_data;

            }

        } else {

            $mensaje_data['id'] = $mensaje->id;
            $mensaje_data['de'] = $mensaje->de;
            $mensaje_data['para'] = $mensaje->para;
            $mensaje_data['titulo'] = $mensaje->titulo; 
            $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
            $mensaje_data['tipo'] = $mensaje->tipo;
            $mensaje_data['status'] = $mensaje->status;
            $mensaje_data['mensaje'] = $mensaje->mensaje;
            $mensaje_data['respuestas'] = json_decode($mensaje->respuestas, true);

            if ($mensaje->status==0) {
                $mensaje_data['status_texto'] = 'Enviado';
            } else if ($mensaje->status==1) {
                $mensaje_data['status_texto'] = 'Leido';
            } else if ($mensaje->status==2) {
                $mensaje_data['status_texto'] = 'Respondido';
            }

            $mensajes_data[] = $mensaje_data;

        }            

    }
    



    return view('admin.mensajes.index',
          ['menubar'=> $this->list_sidebar(),
           'mensajes'=>$mensajes_data,
           'empresas'=>$empresas
          ]);
  }



   public function show(Request $request, $id)
    {
        $this->guardar_sesion($request);
        $empresas = companiesModel::get();

        $mensaje = mensajesModel::find($id);

        $mensaje_display = array();

        if ($mensaje!='') {
            $mensaje_display['id'] = $mensaje->id;
            $mensaje_display['de'] = $mensaje->de;
            $mensaje_display['para'] = $mensaje->para;
            $mensaje_display['titulo'] = $mensaje->titulo; 
            $mensaje_display['mensaje'] = $mensaje->mensaje;
            $mensaje_display['fecha'] = date('d-m-Y h:i',strtotime($mensaje->created_at));
            $mensaje_display['tipo'] = $mensaje->tipo;
            $mensaje_display['status'] = $mensaje->status;

            $mensaje_display['respuestas'] = json_decode($mensaje->respuestas, true);

            if (($mensaje->tipo==1)&&($mensaje->status == 0)) {
                $mensaje->status = 1;
            }

            $mensaje->save();
        }


        //// todos los mensajes

        $mensajes = mensajesModel::where('de_admin', Auth::user()->id)->orWhere('para_admin', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();


        $mensajes_data = array();

        foreach($mensajes AS $mensaje) {

            if (session('id_empresa')!='') {

                if (in_array($mensaje->de_user, session('array_colabs'))||in_array($mensaje->para_user, session('array_colabs'))) {

                    $mensaje_data['id'] = $mensaje->id;
                    $mensaje_data['de'] = $mensaje->de;
                    $mensaje_data['para'] = $mensaje->para;
                    $mensaje_data['titulo'] = $mensaje->titulo; 
                    $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
                    $mensaje_data['tipo'] = $mensaje->tipo;
                    $mensaje_data['status'] = $mensaje->status;
                    $mensaje_data['mensaje'] = $mensaje->mensaje;
                    $mensaje_data['respuestas'] = json_decode($mensaje->respuestas, true);

                    if ($mensaje->status==0) {
                        $mensaje_data['status_texto'] = 'Enviado';
                    } else if ($mensaje->status==1) {
                        $mensaje_data['status_texto'] = 'Leido';
                    } else if ($mensaje->status==2) {
                        $mensaje_data['status_texto'] = 'Respondido';
                    }

                    $mensajes_data[] = $mensaje_data;

                }

            } else {

                $mensaje_data['id'] = $mensaje->id;
                $mensaje_data['de'] = $mensaje->de;
                $mensaje_data['para'] = $mensaje->para;
                $mensaje_data['titulo'] = $mensaje->titulo; 
                $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
                $mensaje_data['tipo'] = $mensaje->tipo;
                $mensaje_data['status'] = $mensaje->status;
                $mensaje_data['mensaje'] = $mensaje->mensaje;
                $mensaje_data['respuestas'] = json_decode($mensaje->respuestas, true);

                if ($mensaje->status==0) {
                    $mensaje_data['status_texto'] = 'Enviado';
                } else if ($mensaje->status==1) {
                    $mensaje_data['status_texto'] = 'Leido';
                } else if ($mensaje->status==2) {
                    $mensaje_data['status_texto'] = 'Respondido';
                }

                $mensajes_data[] = $mensaje_data;

            }            

        }

        return view('admin.mensajes.show', ['menubar'=> $this->list_sidebar(),
           'mensaje'=>$mensaje_display,
           'mensajes'=>$mensajes_data,
           'empresas'=>$empresas
          ]);
    }


/*
  public function enviados() {


    // $mensajes = mensajesModel::where('de', Auth::user()->id)->orWhere('para', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

    $mensajes = mensajesModel::where('de', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

    $mensajes_data = array();

    foreach($mensajes AS $mensaje) {

        $mensaje_data['de'] = UserAdmin::get_name($mensaje->de);
        $mensaje_data['para'] = colaboradores::get_name($mensaje->para);
        $mensaje_data['titulo'] = $mensaje->titulo; 
        $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->created_at));

        $mensajes_data[] = $mensaje_data;

    }
    

    return view('admin.mensajes.enviados',
          ['menubar'=> $this->list_sidebar(),
           'mensajes'=>$mensajes_data
          ]);
  }
*/

    public function create(Request $request)
    {
        $this->guardar_sesion($request);
        $empresas = companiesModel::get();

        $user = Auth::user();

        if (session('id_empresa')!='') {
            $colaboradores = colaboradores::whereIn('id', session('array_colabs'))->get();
        } else {
            $colaboradores = colaboradores::get();
        }

        $grupos = mensajes_gruposModel::where('id_admin', Auth::user()->id)->orderBy('id', 'DESC')->get();

        return view('admin.mensajes.create', ['menubar'=> $this->list_sidebar(),
           'user'=>$user,
           'colaboradores'=>$colaboradores,
           'grupos'=>$grupos,
           'empresas'=>$empresas
          ]);
    }


  public function store(Request $request) {
    /*
    $validator = $request->validate([
        'name' => 'required',
        'password' => 'required',
        'usersys' => 'required',
    ]);
    */

    $model = new mensajesModel;
    $data = $request->only($model->getFillable());

    $data['de'] = UserAdmin::get_name($request->de_admin);



    if ($request->para_user=='g' || $request->para_user=='v') {

        $ids_miembros = array();

        if ($request->para_user=='g') {

            $grupo = mensajes_gruposModel::find($request->para_grupo);

            if ($grupo!='') {
                $ids_miembros = explode(',', $grupo->ids_miembros);

                $tipo_mensaje = "GRUPO ".$grupo->nombre;
            }
            

        } else {

            $ids_miembros = $request->ids;

            $tipo_mensaje = "VARIOS ";
        }


        if ($ids_miembros=='' || $ids_miembros==null) {
            $ids_miembros = array();
        }

        foreach($ids_miembros AS $miembro) {

            $model_todos = new mensajesModel;

            $data['para'] = $tipo_mensaje.' - '.colaboradores::get_name($miembro);
            $data['para_user'] = $miembro;

            $model_todos->fill($data)->save();

        }



    } elseif ($request->para_user==0) {

        if (session('id_empresa')!='') {
            $colaboradores = colaboradores::whereIn('id', session('array_colabs'))->get();
        } else {
            $colaboradores = colaboradores::get();
        }

        foreach($colaboradores AS $colaborador) {

            $model_todos = new mensajesModel;

            $data['para'] = 'EN GENERAL - '.colaboradores::get_name($colaborador->id);

            $data['para_user'] = $colaborador->id;

            $model_todos->fill($data)->save();

        }


    } else {
        $data['para'] = colaboradores::get_name($request->para_user);

        $model->fill($data)->save();
    } 

    

    return redirect('mensajes')->with('success','Mensaje Enviado!');
  }



  public function destroy($id,$idx) {

    mensajesModel::destroy($id);

    return redirect('mensajes')->with('success','Mensaje Borrado!');
  }



  public function update(Request $request, $id) {

    $mensaje = mensajesModel::find($id);
    $respuestas = json_decode($mensaje->respuestas, true);

    $id_user =  Auth::user()->id;

    $respuesta["de"] = UserAdmin::get_name($id_user);
    $respuesta["fecha"] = date('d-m-Y h:i');
    $respuesta["respuesta"] = $request->respuesta;
    $respuesta["tipo"] = 0;

    $respuestas[] = $respuesta;

    $mensaje->respuestas = json_encode($respuestas);

    if ($mensaje->tipo == 0 ) {
        $mensaje->status = 0; // enviado
    } else {
        $mensaje->status = 2; // respondido
    }

    $mensaje->save();


    return redirect()->back()->with('success','Mensaje Respondido!');


  }
}
