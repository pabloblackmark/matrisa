<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\cumplesModel;

class cumplesController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'cumples';
    }

    public function index() {

        $data = cumplesModel::first();

        /* preparaciones para breadcrumb y file manager*/
        $breadcrumb = [
            'base' => 'Cumpleaños',
            'inicio' => 'cumpleaños'
        ];

        session(['breadcrumb' => $breadcrumb]);


        return view('admin.cumples.show',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'breadcrumb' => $breadcrumb,
              ]);
    }

/*
    public function show($id=null){
        $edit = false;
        $data = [];

        if ($id != null){
            $data = colaboradores::find($id);
            $edit = true;
        }

        $pdata = puestosModel::get();
        $puestos = [];
        foreach($pdata as $tpd){
            $puestos[] = $tpd->nombre;
        }

        $pdata = ContratosModel::get();
        $contratos = [];
        foreach($pdata as $tpd){
            $contratos[] = $tpd->name;
        }

        $pdata = BancosModel::get();
        $bancos = [];
        foreach($pdata as $tpd){
            $bancos[] = $tpd->nombre;
        }

        return view('admin.colaboradores.show',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'edit' => $edit,
               'depmun' => $this->get_depmunic(),
               'puestos' => $puestos,
               'contratos' => $contratos,
               'bancos' => $bancos
              ]);
    }
*/

    public function update(Request $request) {
        
        $model = new cumplesModel;
        $cumple = $model::first();
        $dataModel = $request->only($model->getFillable());

        if ($request->file('imagen')){
            $imagen = $request->file('imagen')->storePublicly('cumples', 'public');
            $dataModel['imagen'] = $imagen;
        }

        if ($cumple!='') {
            $cumple->fill($dataModel)->save();
        } else {
            $model->fill($dataModel)->save();
        }

        /*$this->changeLog([
                          'area' => 'colaboradores',
                          'type' => 'edicion',
                          'request' => $request,
                          'element_id' => $id
                      ]);
        */
        

        return redirect($this->back)->with('success','Actualizado correctamente!');
    }



    public function birthday(Request $request) {


    }

}
