<?php

namespace App\Http\Controllers\admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\UserAdmin;
use App\adminModels\roles;
use App\adminModels\mensajesModel;
use App\adminModels\colaboradores;

use App\adminModels\mensajes_gruposModel;

use Illuminate\Support\Facades\Hash;

use App\adminModels\companiesModel;
use App\adminModels\company_colab_assocModel;

class mensajes_gruposController extends Controller
{
  protected $redirecUlr;
  public function __construct()
  {
      $this->middleware('auth:admin');
  }


  public function guardar_sesion($request) {

      if ($request->id_empresa=='---')  {
            session(['id_empresa' => '']);

            $array_colabs = array();
            session(['array_colabs' => $array_colabs]);
      }elseif ($request->id_empresa!='') {
            session(['id_empresa' => $request->id_empresa]);

            $lista_colabs = company_colab_assocModel::where('idCompany', $request->id_empresa)->get();

            $array_colabs = array();

            foreach($lista_colabs AS $colab) {
                $array_colabs[] = $colab->idColab;
            }

            session(['array_colabs' => $array_colabs]);

      }

  }


  public function index(Request $request) {

    $this->guardar_sesion($request);
    $empresas = companiesModel::get();

    if (session('id_empresa')!='') {
        $grupos = mensajes_gruposModel::where('id_admin', Auth::user()->id)->where('id_empresa', session('id_empresa'))->orderBy('id', 'DESC')->get();
    } else {
        $grupos = mensajes_gruposModel::where('id_admin', Auth::user()->id)->orderBy('id', 'DESC')->get();
    }

    return view('admin.mensajes_grupos.index',
          ['menubar'=> $this->list_sidebar(),
           'grupos'=>$grupos,
           'empresas'=>$empresas
          ]);
  }



   public function show(Request $request, $id)
    {

        $this->guardar_sesion($request);
        $empresas = companiesModel::get();

        $user = Auth::user();

        $grupo = mensajes_gruposModel::find($id);

        if (session('id_empresa')!='') {
            $colaboradores = colaboradores::whereIn('id', session('array_colabs'))->get();
        } else {
            $colaboradores = colaboradores::get();
        }

        return view('admin.mensajes_grupos.show', ['menubar'=> $this->list_sidebar(),
           'grupo'=>$grupo,
           'user'=>$user,
           'colaboradores'=>$colaboradores,
           'empresas'=>$empresas
          ]);
    }


    public function create(Request $request)
    {

        $this->guardar_sesion($request);
        $empresas = companiesModel::get();

        $user = Auth::user();

        if (session('id_empresa')!='') {
            $colaboradores = colaboradores::whereIn('id', session('array_colabs'))->get();
        } else {
            $colaboradores = colaboradores::get();
        }


        return view('admin.mensajes_grupos.create', ['menubar'=> $this->list_sidebar(),
           'user'=>$user,
           'colaboradores'=>$colaboradores,
           'empresas'=>$empresas,
          ]);
    }


  public function store(Request $request) {

    $model = new mensajes_gruposModel;
    $data = $request->only($model->getFillable());

    $data['ids_miembros'] = implode(",", $request->ids_miembros);
    $data['id_empresa'] = session('id_empresa');

    $model->fill($data)->save();

    return redirect('mensajes_grupos')->with('success','Grupo Creado!');
  }



  public function destroy($id,$idx) {

    mensajes_gruposModel::destroy($id);

    return redirect('mensajes_grupos')->with('success','Grupo Borrado!');
  }



  public function update(Request $request, $id) {

    $grupo = mensajes_gruposModel::find($id);

    $id_user = Auth::user()->id;

    $data = $request->only($grupo->getFillable());

    $data['ids_miembros'] = implode(",", $request->ids_miembros);
    $data['id_empresa'] = session('id_empresa');

    $grupo->fill($data)->save();


    return redirect('mensajes_grupos')->with('success','Grupo Actualizado!');


  }
}
