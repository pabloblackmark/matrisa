<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\roles_names;
use App\adminModels\roles;
class rolesController extends Controller
{
  protected $redirecUlr;
  public function __construct()
  {
      $this->middleware('auth:admin');
  }
  public function index(){
    $listof=roles::where("country",$this->get_country())
    ->with('nameroles')->orderBy('nameRole')->get();
    $permiss=roles_names::where("publc","1")->orderBy('naccess')->get();
    // dd($permiss);
    foreach($permiss AS $miss){
      $permission[]=array($miss['id'],$miss['naccess']);
    }
    return view('admin.role.show',
          ['menubar'=> $this->list_sidebar(),
           'roles'=>$listof,
           'permission'=>$permission
          ]);
  }

  public function store(Request $request) {
    $validator = $request->validate([
        'nameRole' => 'required',
    ]);
    $model = new roles;
    $data = $request->only($model->getFillable());

    $data["country"]=$this->get_country();
    $model->fill($data)->save();
    $model->nameroles()->sync($request->acceds);
    return redirect()->back()->with('success','Guardado correctamente!');
  }
  public function destroy($id) {
    try {
        roles::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente!');
      }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque se está utilizando por un usuario del módulo -Personal-. Primero debe eliminar esta configuración.'.$e->getCode());
    }
  }
  public function update(Request $request, $id) {
    $model = new roles;
    $finded = $model::find($id);
    $dataMod = $request->only($model->getFillable());
        $finded->fill($dataMod)->save();
    $finded->nameroles()->sync($request->acceds);
    return redirect()->back()->with('info','Actualizado correctamente!');
  }
}
