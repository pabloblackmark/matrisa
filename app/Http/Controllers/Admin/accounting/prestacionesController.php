<?php

namespace App\Http\Controllers\admin\accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\loggsPlanillaModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;

use App\adminModels\changeLogModel;
use App\Http\Controllers\Admin\changeLogController;


use App\apiModels\userViaticosModel;
use App\apiModels\prestacionesModel;


use App\Http\Controllers\Admin\companies\companyclientsController;
use App\Http\Controllers\Admin\companies\clientsBranchOfficeController;
use App\Http\Controllers\Admin\companies\clientsBranchAreaController;


class prestacionesController extends Controller
{

    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'admin.prestaciones.show';
    }

    public function index(Request $request){

        $data = [];
        $clientes = "";
        $sucursales = "";
        $areas = "";

        $config_empresa = null;

        // si hay filtro
        if ($request->id_empresa != null){

            // colaboradores filtro
            if ($request->id_cliente!=null && $request->id_sucursal!=null && $request->id_area!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("idArea", $request->id_area)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null && $request->id_sucursal!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("status", 1)->distinct("idColab")->get();
            } else {
                $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("status", 1)->distinct("idColab")->get();
            }


            $clientes = companyclientsController::get_clientes($request);
            $sucursales = clientsBranchOfficeController::get_sucursales($request);
            $areas = clientsBranchAreaController::get_areas($request);

            // configuracion de empresa para calculo de igsss
            $config_empresa = companiesModel::find($request->id_empresa);

        }


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $companies = [];
      foreach($data0 as $d){
          $companies[] = [$d->id,$d->company];
      }

      
      $model = new prestacionesModel;

          return view('admin.prestaciones.index',
                ['menubar'=> $this->list_sidebar(),
                 'companies' => $companies,
                 'colabs'=>$data,
                 'yr'=>date("Y"),
                 'monthSel'=>12,
                 'months'=>$this->monthEsp(),
                 'request'=>$request,
                 'clientes'=>$clientes,
                 'sucursales'=>$sucursales,
                 'areas'=>$areas,
                 'config_empresa'=>$config_empresa,
                 'model' => $model
                ]);
    }


   public function prestaciones_colab(Request $request)
   {

      $id_colab = $request->id_colab;
      $id_empresa = $request->id_empresa;


      $years = (!empty($nYr)?$nYr:date("Y"));
      
      if ($id_colab != null){

          $colab = colaboradores::find($id_colab);

      }


      $log_prestaciones = prestacionesModel::whereYear("fecha_pago",$request->ano)
                                  ->where("id_colab",$id_colab)
                                  ->where('id_empresa',$id_empresa)
                                  ->first();

      $config_empresa = companiesModel::find($request->id_empresa);

      // change_log
      $data_changelog = array();

      if ($log_prestaciones!='') {
          $data_changelog = changeLogModel::where('area', 'prestaciones')->where('element_id',  $log_prestaciones->id)->orderby('created_at', 'desc')->get();
          $data_changelog = changeLogController::extraer_data($data_changelog);
      }


      $model = new prestacionesModel;

      return view('admin.prestaciones.show',
            ['menubar'=> $this->list_sidebar(),
             'colab'=> $colab,
             'log_prestaciones'=>$log_prestaciones,
             'request'=>$request,
             'config_empresa'=>$config_empresa,
             'data_changelog' => $data_changelog,
             'model' => $model
            ]);
    }



    function aprobar_colab(Request $request){

      $model = new prestacionesModel;

      $json = $request->datos_prestaciones;

      $data = json_decode($json, true);
      $data['estado'] = $request->estado;
      $data['fecha_pago'] = $request->fecha_pago;
      
      $model->fill($data)->save();


      $this->changeLog([
                    'area' => 'prestaciones',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $model->id,
                ]);


      return redirect()->back()->with('success','Guardado correctamente!');
      
    }



    function aprobar_prestaciones(Request $request){

      foreach($request->pago as $datos_prestaciones){

          $model = new prestacionesModel;

          $json = $datos_prestaciones;

          $data = json_decode($json, true);
          $data['estado'] = 'Procesado';
          $data['fecha_pago'] = $request->fecha_pago;
          
          $model->fill($data)->save();
      }
      
      $url_redirect = 'prestaciones?id_empresa='.$request->id_empresa.'&id_cliente='.$request->id_cliente.'&id_sucursal='.$request->id_sucursal.'&id_area='.$request->id_area.'&tipo='.$request->tipo.'&ano='.$request->ano;

      return redirect($url_redirect)->with('success','Procesado correctamente!');
      
    }



    function update_prestaciones(Request $request){

      $prestaciones = prestacionesModel::find($request->id_prestaciones);
      $prestaciones->estado = $request->estado;
      $prestaciones->fecha_pago = $request->fecha_pago;
      $prestaciones->save();


      $this->changeLog([
                    'area' => 'prestaciones',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $prestaciones->id,
                ]);


      return redirect()->back()->with('success','Actualizado correctamente!');
      
    }




}
