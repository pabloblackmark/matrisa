<?php

namespace App\Http\Controllers\admin\accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\loggsPlanillaModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;

use App\apiModels\descuentosModel;
use App\apiModels\logdescuentosModel;

use DB;
use DateTime;
use DatePeriod;
use DateInterval;

use App\adminModels\changeLogModel;
use App\Http\Controllers\Admin\changeLogController;

use App\Http\Controllers\Admin\companies\companyclientsController;
use App\Http\Controllers\Admin\companies\clientsBranchOfficeController;
use App\Http\Controllers\Admin\companies\clientsBranchAreaController;

use App\Http\Controllers\Admin\accounting\pagosController;

class payrollController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
    }

    public function index(Request $request){

        $data = [];
        $clientes = "";
        $sucursales = "";
        $areas = "";

        $config_empresa = null;

        // si hay filtro
        if ($request->id_empresa != null){

            // colaboradores de la empresa
            // $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->get();

            // colaboradores filtro
            if ($request->id_cliente!=null && $request->id_sucursal!=null && $request->id_area!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("idArea", $request->id_area)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null && $request->id_sucursal!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("status", 1)->distinct("idColab")->get();
            } else {
                $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("status", 1)->distinct("idColab")->get();
            }


            $clientes = companyclientsController::get_clientes($request);
            $sucursales = clientsBranchOfficeController::get_sucursales($request);
            $areas = clientsBranchAreaController::get_areas($request);

            // configuracion de empresa para calculo de igsss
            $config_empresa = companiesModel::find($request->id_empresa);

        }


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $companies = [];
      foreach($data0 as $d){
          $companies[] = [$d->id,$d->company];
      }
      // dd($companies);
          return view('admin.companies.planillasListColabs',
                ['menubar'=> $this->list_sidebar(),
                 'companies' => $companies,
                 'colabs'=>$data,
                 'yr'=>date("Y"),
                 'monthSel'=>12,
                 'months'=>$this->monthEsp(),
                 'request'=>$request,
                 'clientes'=>$clientes,
                 'sucursales'=>$sucursales,
                 'areas'=>$areas,
                 'config_empresa'=>$config_empresa
                ]);
    }


   public function payrollinfo(Request $request)
   {

      $id_colab = $request->id_colab;
      $id_empresa = $request->id_empresa;

      $id_cliente = $request->id_cliente;
      $id_sucursal = $request->id_sucursal;
      $id_area = $request->id_area;

      $inicio = $request->inicio;
      $fin = $request->fin;


      $fecha_inicio = date_create($inicio);
      $fecha_fin = date_create($fin);

      $diff_dias = date_diff($fecha_inicio, $fecha_fin);
      $dias_laborados = $diff_dias->format('%d')+1;


      $dia_inicio = date_format($fecha_inicio, 'd');
      $dia_fin = date_format($fecha_fin, 'd');
      $mes_fecha = date_format($fecha_fin, 'm');


      if ($dia_inicio==1&&$dia_fin==31) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==16&&$dia_fin==31) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 15;
      }


      $month = 12;
      $years = $nYr = 2021;

      $dias_laborados = $dias_laborados;
      // $dias_laborados = (!empty($month)?date("t",strtotime($nYr.'-'.$month.'-01')):date("t"));

      $years = (!empty($nYr)?$nYr:date("Y"));
      $edit = false;
      $data = $addPays = $extraHours = [];
      
      if ($id_colab != null){
        $days = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
        $daysEng = ['Monday'=>'Lunes',
                 'Tuesday'=>'Martes',
                 'Wednesday'=>'Miércoles',
                 'Thursday'=>'Jueves',
                 'Friday'=>'Viernes',
                 'Saturday'=>'Sábado',
                 'Sunday'=>'Domingo'];
        $baseDay = ['dayWeek'=>'','dateDay'=>'','ontime'=>'','id'=>0,'hourEnter'=>null,
                    'hourExit'=>null,'hourEnterBetw'=>null,'hourExitBetw'=>null,'outTimeEnter'=>null,'hourDay'=>null,
                    'extraHourDay'=>null,'hourNight'=>null,'extraHourNight'=>null,'extraHourEspecial'=>null,'estado'=>'',
                    'clientName'=>'','tasks'=>'','baseTasks'=>null,'aditTask'=>0];

        // $dateDays = [$years.'-'.($month<10?'0':'').$month.'-01',$years.'-'.($month<10?'0':'').$month.'-'.$dias_laborados];
        $dateDays = [$inicio, $fin];


        $data = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->where(function ($query){
                              $query->orWhere('typeday', 'aprobado')
                                    ->orWhere('typeday', 'descuento_septimo')
                                    ->orWhere('typeday', 'descuento');
                          })
                          ->orderBy("dateDay")
                          ->get();


        $septCount = $disCount = 0;
        foreach($data AS $valors){
          if($valors->typeday=='descuento_septimo'){
              $septCount++;
          }
          else if($valors->typeday=='descuento'){
              $disCount++;
          }
          // $septCount = $disCount = 0;
        }

        
        $descuentos=[];
        // dd($discount,$discountdays,);
        $colab = colaboradores::find($id_colab);

        $extraDPay = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("extraPayDay");
        $extraNPay = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("extraPayNight");
        $extraEPay = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("extraPayEspec");
        $extraMPay = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("extraPayMix");

        $extraHours = ["extraPayDay"=>$extraDPay,"extraPayNight"=>$extraNPay,
                       "extraPayEspec"=>$extraEPay,"extraPayMix"=>$extraMPay];

        /*
        $viatics = $colab->viaticos*loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id)
                          ->where('idCompany',$idComp)->sum("viatics");
        $movil = $colab->movil*loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id)
                          ->where('idCompany',$idComp)->sum("viaticsMovil");
        $viaticsExtras = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id)
                          ->where('idCompany',$idComp)->sum("viaticsAdded");
        */


        // $addPays = ["viatics"=>$viatics,"movil"=>$movil,"extras"=>$viaticsExtras];
        $addPays = ["viatics"=>0,"movil"=>0,"extras"=>0];
      }

      $log_planilla = loggsPlanillaModel::select('id','estado','fecha_pago')
                                  ->where("fecha_inicio",$inicio)
                                  ->where("fecha_fin",$fin)
                                  ->where("id_colab",$id_colab)
                                  ->where('id_empresa',$id_empresa)
                                  // ->where('id_cliente',$id_cliente)
                                  // ->where('id_sucursal',$id_sucursal)
                                  // ->where('id_area',$id_area)
                                  ->first();

      $config_empresa = companiesModel::find($request->id_empresa);

      // change_log
      $data_chagelog = [];

      if ($log_planilla!='') {
          $data_chagelog = changeLogModel::where('area', 'planilla')->where('element_id',  $log_planilla->id)->orderby('created_at', 'desc')->get();
          $data_chagelog = changeLogController::extraer_data($data_chagelog);
      }


      return view('admin.companies.reportPlantilla',
            ['menubar'=> $this->list_sidebar(),
             'data' => $data,
             'colabInfo'=> $colab,
             'idCompany'=>$id_empresa,
             'idColab'=>$id_colab,
             'yr' => $years,
             'month' => $month,
             'dias_laborados'=>$dias_laborados,
             // 'daysLabos'=>$countDays,
             'extraPays'=>$addPays,
             'payment'=>$log_planilla,
             'hoursExtras'=>$extraHours,
             'discounts'=>['septCount'=>$septCount,'disCount'=>$disCount],
             'request'=>$request,
             'config_empresa'=>$config_empresa,
             'data_chagelog' => $data_chagelog
            ]);
    }



    function aprovationPayroll(Request $request){

      $model = new loggsPlanillaModel;

      $json = $request->datos_planilla;

      $data = json_decode($json, true);
      $data['estado'] = $request->estado;
      $data['fecha_pago'] = $request->fecha_pago;

      $descuentos = $data['descuentos'];
      $data['descuentos'] = json_encode($data['descuentos']);

      $model->fill($data)->save();


      // actualizar tabla de descuentos
      foreach($descuentos as $desc) {

          $descuento = descuentosModel::find($desc['id']);

          $saldo = $descuento->saldo;
          $no_cuota = $descuento->cuota_actual;

          $saldo = $saldo - $desc['pago'];
          $no_cuota++;

          $descuento->saldo = $saldo;
          $descuento->cuota_actual = $no_cuota;

          if ($descuento->tipo != 'manutencion') {
              if ($descuento->saldo==0) {
                  $descuento->estado = "Pagado";
              }
          }

          $descuento->save();

          // log de descuento
          $log_descuento = new logdescuentosModel;
          $log_descuento->id_empresa = $data['id_empresa'];
          $log_descuento->id_colab = $data['id_colab'];
          $log_descuento->id_desc = $desc['id'];
          $log_descuento->fecha = $request->fecha_pago;
          $log_descuento->pago = $desc['pago'];
          $log_descuento->no_cuota =  $no_cuota;
          $log_descuento->saldo = $saldo;
          $log_descuento->save();
      }
      


      $this->changeLog([
                    'area' => 'planilla',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $model->id,
                ]);


      // generar boleta de pago
      $generar_pago = pagosController::generar_pago($request);


      return redirect()->back()->with('success','Guardado correctamente!');
      
    }



    function aprobar_planilla(Request $request){

      
      foreach($request->pago as $datos_planilla){

          $model = new loggsPlanillaModel;

          $json = $datos_planilla;

          $data = json_decode($json, true);
          $data['estado'] = 'Procesado';
          $data['fecha_pago'] = $request->fecha_pago;
          
          $descuentos = $data['descuentos'];
          $data['descuentos'] = json_encode($data['descuentos']);

          // dd($data);

          $model->fill($data)->save();

          // actualizar tabla de descuentos
          foreach($descuentos as $desc) {

              $descuento = descuentosModel::find($desc['id']);

              $saldo = $descuento->saldo;
              $no_cuota = $descuento->cuota_actual;

              $saldo = $saldo - $desc['pago'];
              $no_cuota++;

              $descuento->saldo = $saldo;
              $descuento->cuota_actual = $no_cuota;

              if ($descuento->tipo != 'manutencion') {
                  if ($descuento->saldo==0) {
                      $descuento->estado = "Pagado";
                  }
              }

              $descuento->save();

              // log de descuento
              $log_descuento = new logdescuentosModel;
              $log_descuento->id_empresa = $data['id_empresa'];
              $log_descuento->id_colab = $data['id_colab'];
              $log_descuento->id_desc = $desc['id'];
              $log_descuento->fecha = $request->fecha_pago;
              $log_descuento->pago = $desc['pago'];
              $log_descuento->no_cuota =  $no_cuota;
              $log_descuento->saldo = $saldo;
              $log_descuento->save();
          }


          // generar boleta de pago
          $request->id_colab = $data['id_colab'];
          $generar_pago = pagosController::generar_pago($request);
      }

      
      $url_redirect = 'planillas?id_empresa='.$request->id_empresa.'&id_cliente='.$request->id_cliente.'&id_sucursal='.$request->id_sucursal.'&id_area='.$request->id_area.'&inicio='.$request->inicio.'&fin='.$request->fin;

      return redirect($url_redirect)->with('success','Procesado correctamente!');
      // return redirect()->back()->with('success','Guardado correctamente!');
      
    }



    function update_planilla(Request $request){

      $planilla = loggsPlanillaModel::find($request->id_planilla);
      $planilla->estado = $request->estado;
      $planilla->fecha_pago = $request->fecha_pago;
      $planilla->save();


      $this->changeLog([
                    'area' => 'planilla',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $planilla->id,
                ]);


      return redirect()->back()->with('success','Actualizado correctamente!');
      
    }



    static function get_planilla(Request $request) {


      $id_colab = $request->id_colab;
      $id_empresa = $request->id_empresa;

      $inicio = $request->inicio;
      $fin = $request->fin;

      $fecha_inicio = date_create($inicio);
      $fecha_fin = date_create($fin);

      $diff_dias = date_diff($fecha_inicio, $fecha_fin);
      $dias_laborados = $diff_dias->format('%d')+1;


      $dia_inicio = date_format($fecha_inicio, 'd');
      $dia_fin = date_format($fecha_fin, 'd');
      $mes_fecha = date_format($fecha_fin, 'm');


      if ($dia_inicio==1&&$dia_fin==31) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==16&&$dia_fin==31) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 15;
      }

      if ($id_colab != null){

        $dateDays = [$inicio, $fin];


        $data = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->where(function ($query){
                              $query->orWhere('typeday', 'aprobado')
                                    ->orWhere('typeday', 'descuento_septimo')
                                    ->orWhere('typeday', 'descuento');
                          })
                          ->orderBy("dateDay")
                          ->get();


        $septCount = $disCount = 0;
        foreach($data AS $valors){
          if($valors->typeday=='descuento_septimo'){
              $septCount++;
          }
          else if($valors->typeday=='descuento'){
              $disCount++;
          }
        }

        $descuentos=['septCount'=>$septCount,'disCount'=>$disCount];

        $colab = colaboradores::find($id_colab);

      }

      $config_empresa = companiesModel::find($request->id_empresa);

      $salario = $colab->sueldo * $dias_laborados;



            $bono_extra = $colab->bono_extra * $dias_laborados;
            $bono_antiguedad = $colab->antiguedad * $dias_laborados;
            $bono_ley = $colab->bono_ley * $dias_laborados;


            // calculo igss
            if ($config_empresa->calculo_sobre=='sueldo base') {

                $igss = $salario * ($config_empresa->iggs_laboral/100);

            } elseif ($config_empresa->calculo_sobre=='sueldo total') {

                if ($config_empresa->bono_extra==1) {
                    $total_calcular = $salario + $bono_extra + $antiguedad;
                } else {
                    $total_calcular = $salario + $antiguedad;
                }

                $igss = $total_calcular * ($config_empresa->iggs_laboral/100);
            }


            $desc_dia = $descuentos["disCount"] * $colab->sueldo;
            $desc_septimo = $descuentos["septCount"] * ($colab->sueldo*2);


            $total_bonos = $bono_ley + $bono_extra + $bono_antiguedad;


            $array_planilla = array(
                                  'salario'=>$salario,
                                  'bono_ley'=>$bono_ley,
                                  'bono_extra'=>$bono_extra,
                                  'bono_antiguedad'=>$bono_antiguedad,
                                  'desc_dia'=>$desc_dia,
                                  'desc_septimo'=>$desc_septimo,
                                  'desc_igss'=>$igss,
                                  'total_bonos'=>$total_bonos
            );



        return $array_planilla;

    }

}
