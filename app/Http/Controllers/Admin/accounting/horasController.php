<?php

namespace App\Http\Controllers\admin\accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\loggsPlanillaModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;

use App\adminModels\changeLogModel;
use App\Http\Controllers\Admin\changeLogController;


use App\apiModels\userHorasModel;

use App\Http\Controllers\Admin\companies\companyclientsController;
use App\Http\Controllers\Admin\companies\clientsBranchOfficeController;
use App\Http\Controllers\Admin\companies\clientsBranchAreaController;

use App\Http\Controllers\Admin\accounting\pagosController;

class horasController extends Controller
{

    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'admin.horas.show';
    }

    public function index(Request $request){

        $data = [];
        $clientes = "";
        $sucursales = "";
        $areas = "";

        $config_empresa = null;

        // si hay filtro
        if ($request->id_empresa != null){

            // colaboradores de la empresa
            // $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->get();

            // colaboradores filtro
            if ($request->id_cliente!=null && $request->id_sucursal!=null && $request->id_area!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("idArea", $request->id_area)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null && $request->id_sucursal!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("status", 1)->distinct("idColab")->get();
            } else {
                $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("status", 1)->distinct("idColab")->get();
            }

            $clientes = companyclientsController::get_clientes($request);
            $sucursales = clientsBranchOfficeController::get_sucursales($request);
            $areas = clientsBranchAreaController::get_areas($request);

            // configuracion de empresa para calculo de igsss
            $config_empresa = companiesModel::find($request->id_empresa);

        }


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $companies = [];
      foreach($data0 as $d){
          $companies[] = [$d->id,$d->company];
      }
      // dd($companies);

      $model = new userHorasModel;


          return view('admin.horas.index',
                ['menubar'=> $this->list_sidebar(),
                 'companies' => $companies,
                 'colabs'=>$data,
                 'yr'=>date("Y"),
                 'monthSel'=>12,
                 'months'=>$this->monthEsp(),
                 'request'=>$request,
                 'clientes'=>$clientes,
                 'sucursales'=>$sucursales,
                 'areas'=>$areas,
                 'config_empresa'=>$config_empresa,
                 'model'=>$model
                ]);
    }


   public function horas_colab(Request $request)
   {

      $id_colab = $request->id_colab;
      $id_empresa = $request->id_empresa;

      $id_cliente = $request->id_cliente;
      $id_sucursal = $request->id_sucursal;
      $id_area = $request->id_area;

      $inicio = $request->inicio;
      $fin = $request->fin;


      $fecha_inicio = date_create($inicio);
      $fecha_fin = date_create($fin);

      $diff_dias = date_diff($fecha_inicio, $fecha_fin);
      $dias_laborados = $diff_dias->format('%d')+1;


      $dia_inicio = date_format($fecha_inicio, 'd');
      $dia_fin = date_format($fecha_fin, 'd');
      $mes_fecha = date_format($fecha_fin, 'm');


      if ($dia_inicio==1&&$dia_fin==31) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==16&&$dia_fin==31) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 15;
      }


      $month = 12;
      $years = $nYr = 2021;

      $dias_laborados = $dias_laborados;
      // $dias_laborados = (!empty($month)?date("t",strtotime($nYr.'-'.$month.'-01')):date("t"));

      $years = (!empty($nYr)?$nYr:date("Y"));
      $edit = false;
      $data = $horas_extras = [];

      if ($id_colab != null){

        $colab = colaboradores::find($id_colab);

        /*

        $extra_dia = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          //->where('idClient',$id_cliente)
                          //->where('idBranch',$id_sucursal)
                          //->where('idArea',$id_area)
                          ->sum("extraPayDay");

        $extra_noche = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          //->where('idClient',$id_cliente)
                          //->where('idBranch',$id_sucursal)
                          //->where('idArea',$id_area)
                          ->sum("extraPayNight");

        $extra_especial = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("extraPayEspec");

        $extra_mixta = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("extraPayMix");

        $extra_metas = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("priceAdiTask");

        $conteo_metas = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("aditTask");

        $horas_extras = ["extra_dia"=>$extra_dia,"extra_noche"=>$extra_noche,
                       "extra_especial"=>$extra_especial,"extra_mixta"=>$extra_mixta,"extra_metas"=>$extra_metas,"conteo_metas"=>$conteo_metas];

        */

        $horas_extras = $this->get_horas($request);



        $dateDays = [$inicio, $fin];

        $data = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->Where('typeday', 'aprobado')
                          ->orderBy("dateDay")
                          ->get();
      }


      $log_horas = userHorasModel::select('id','estado','fecha_pago')
                                  ->where("fecha_inicio",$inicio)
                                  ->where("fecha_fin",$fin)
                                  ->where("id_colab",$id_colab)
                                  ->where('id_empresa',$id_empresa)
                                  // ->where('id_cliente',$id_cliente)
                                  // ->where('id_sucursal',$id_sucursal)
                                  // ->where('id_area',$id_area)
                                  ->first();

      $config_empresa = companiesModel::find($request->id_empresa);

      // change_log
      $data_changelog = array();

      if ($log_horas!='') {
          $data_changelog = changeLogModel::where('area', 'horas')->where('element_id',  $log_horas->id)->orderby('created_at', 'desc')->get();
          $data_changelog = changeLogController::extraer_data($data_changelog);
      }

      return view('admin.horas.show',
            ['menubar'=> $this->list_sidebar(),
             'colabInfo'=> $colab,
             'idCompany'=>$id_empresa,
             'idColab'=>$id_colab,
             'yr' => $years,
             'month' => $month,
             'dias_laborados'=>$dias_laborados,
             'log_horas'=>$log_horas,
             'horas_extras'=>$horas_extras,
             'request'=>$request,
             'config_empresa'=>$config_empresa,
             'data' => $data,
             'data_changelog' => $data_changelog
            ]);
    }



    function aprobar_colab(Request $request){

      $model = new userHorasModel;

      $json = $request->datos_horas;

      $data = json_decode($json, true);
      $data['estado'] = $request->estado;
      $data['fecha_pago'] = $request->fecha_pago;

      $model->fill($data)->save();


      $this->changeLog([
                    'area' => 'horas',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $model->id,
                ]);

      // generar boleta de pago
      $request->id_colab = $data['id_colab'];
      $request->id_empresa = $data['id_empresa'];
      $request->id_cliente = $data['id_cliente'];

      $generar_pago = pagosController::generar_pago($request);

      return redirect()->back()->with('success','Guardado correctamente!');

    }



    function aprobar_horas(Request $request){

      foreach($request->pago as $datos_horas){

          $model = new userHorasModel;

          $json = $datos_horas;

          $data = json_decode($json, true);
          $data['estado'] = 'Procesado';
          $data['fecha_pago'] = $request->fecha_pago;

          $model->fill($data)->save();

          // generar boleta de pago
          $request->id_colab = $data['id_colab'];
          $request->id_empresa = $data['id_empresa'];
          $request->id_cliente = $data['id_cliente'];

          $generar_pago = pagosController::generar_pago($request);
      }

      $url_redirect = 'horas?id_empresa='.$request->id_empresa.'&id_cliente='.$request->id_cliente.'&id_sucursal='.$request->id_sucursal.'&id_area='.$request->id_area.'&inicio='.$request->inicio.'&fin='.$request->fin;

      return redirect($url_redirect)->with('success','Procesado correctamente!');

    }



    function update_horas(Request $request){

      $horas = userHorasModel::find($request->id_horas);
      $horas->estado = $request->estado;
      $horas->fecha_pago = $request->fecha_pago;
      $horas->save();


      $this->changeLog([
                    'area' => 'horas',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $horas->id,
                ]);


      return redirect()->back()->with('success','Actualizado correctamente!');

    }




    static function get_horas(Request $request) {

        $total_horas = 0;

        $rango_fechas = [$request->inicio, $request->fin];
        $id_colab = $request->id_colab;
        $id_empresa = $request->id_empresa;


        $extra_dia = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("extraPayDay");

        $extra_noche = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("extraPayNight");

        /*
        $extra_especial = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("extraPayEspec");
        */

        $extra_mixta = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("extraPayMix");

        $extra_metas = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("priceAdiTask");

        // conteo de horas                  
        $dia = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("extraHourDayMiddle");

        $noche = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("extraHourNightMiddle");

        $mixta = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("extraHourMixMiddle");                  

        $metas = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("aditTask");

        $dia = $dia/2;
        $noche = $noche/2;
        $mixta = $mixta/2;

        $total_horas = $extra_dia + $extra_noche + $extra_mixta + $extra_metas;


        $horas_extras = ["extra_dia"=>$extra_dia,
                         "extra_noche"=>$extra_noche,
                         "extra_mixta"=>$extra_mixta,
                         "extra_metas"=>$extra_metas,
                         "dia"=>$dia,
                         "noche"=>$noche,
                         "mixta"=>$mixta,
                         "metas"=>$metas,
                         "total_horas"=>$total_horas
                       ];


        return $horas_extras;

    }

}
