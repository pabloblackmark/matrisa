<?php

namespace App\Http\Controllers\admin\accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\loggsPlanillaModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;

use App\adminModels\changeLogModel;
use App\Http\Controllers\Admin\changeLogController;


use App\apiModels\userViaticosModel;
use App\apiModels\descuentosModel;
use App\apiModels\logdescuentosModel;
use App\adminModels\bancosModel;
use App\adminModels\alertasModel;

use App\Http\Controllers\Admin\companies\companyclientsController;
use App\Http\Controllers\Admin\companies\clientsBranchOfficeController;
use App\Http\Controllers\Admin\companies\clientsBranchAreaController;

use App\Http\Controllers\Admin\colaboradoresCompanyController;


class descuentosController extends Controller
{

    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'admin.descuentos.show';
    }

    public function index(Request $request){

        $descuentos = [];
        $colaboradores = "";
        $sucursales = "";
        $areas = "";

        $config_empresa = null;

        // si hay filtro
        if ($request->id_empresa != null){

            // colaboradores filtro

            if ($request->id_colab != null) {
                $ids_colabs = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("idColab", $request->id_colab)->where("status", 1)->distinct("idColab")->get();
            } else {
                $ids_colabs = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("status", 1)->distinct("idColab")->get();
            }

            $colaboradores = colaboradoresCompanyController::get_colaboradores($request);

            // configuracion de empresa para calculo de igsss
            $config_empresa = companiesModel::find($request->id_empresa);


            $array_ids = array();

            foreach ($ids_colabs as $ids) {
                $array_ids[] = $ids->idColab;
            }

            // descuentos

            // aplicar filtros
            if ($request->tipo != null) {
                $descuentos = descuentosModel::whereIn("id_colab", $array_ids)
                                          ->where('id_empresa',$request->id_empresa)
                                          ->where('tipo',$request->tipo)
                                          ->get();
            } else {
                $descuentos = descuentosModel::whereIn("id_colab", $array_ids)
                                          ->where('id_empresa',$request->id_empresa)
                                          ->get();
            }

        }


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $empresas = [];
      foreach($data0 as $d){
          $empresas[] = [$d->id,$d->company];
      }

      $modelo = new colaboradores;


          return view('admin.descuentos.index',
                ['menubar'=> $this->list_sidebar(),
                 'empresas' => $empresas,
                 'descuentos'=>$descuentos,
                 'request'=>$request,
                 'colaboradores'=>$colaboradores,
                 'config_empresa'=>$config_empresa,
                 'modelo'=>$modelo
                ]);
    }



    public function create(Request $request)
    {


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $empresas = [];
      foreach($data0 as $d){
          $empresas[] = [$d->id,$d->company];
      }

      $bancos = bancosModel::orderBy('nombre')->get();

      return view('admin.descuentos.create', ['menubar'=> $this->list_sidebar(),
           'empresas'=>$empresas,
           'bancos'=>$bancos
      ]);

    }


    public function store(Request $request) {

        $model = new descuentosModel;
        $data = $request->only($model->getFillable());

        $data['saldo'] = $request->monto;

        $model->fill($data)->save();

        $url_redirect = 'descuentos?id_empresa='.$request->id_empresa.'&id_colab='.$request->id_colab.'&tipo='.$request->tipo;


        // crea alerta a Gerencia
        $alerta = new alertasModel; /////////// modelo de la alerta

            $alerta->fill(
                [
                    'elemento_id' => $model->id, ///////// se guarda el id del producto
                    'referencia' => 'descuento_' . $model->id, /////se crea la referencia
                    'tipo' => 'aprobar_descuento', ///////// el tipo de alerta es por precio
                    'mensaje' => 'Tienes un descuento por aprobar, tipo: <b>' . $model->tipo . '</b> ID: '.$model->id,
                    'nombre_modulo' => 'descuentos', ////////// aquí se produjo la alerta
                    'url' => 'descuentos/'.$model->id /////// hacia aqui redirecciona la alerta
                ]
            );
        $alerta->save();
        

        return redirect($url_redirect)->with('success','Creado correctamente!');

    }



   public function show(Request $request, $id)
   {

      $descuento = descuentosModel::find($id);

      $colab = $colab = colaboradores::find($descuento->id_colab);

      $log_descuento = logdescuentosModel::where("id_desc", $id)->get();


      // change_log
      $data_changelog = array();

      if ($descuento!='') {
          $data_changelog = changeLogModel::where('area', 'descuentos')->where('element_id',  $descuento->id)->orderby('created_at', 'desc')->get();
          $data_changelog = changeLogController::extraer_data($data_changelog);
      }


      return view('admin.descuentos.show',
            ['menubar'=> $this->list_sidebar(),
             'descuento' => $descuento,
             'colabInfo'=> $colab,
             'log_descuento'=>$log_descuento,
             'request'=>$request,
             'data_changelog' => $data_changelog
            ]);
    }


    function update_descuento(Request $request){

      $descuento = descuentosModel::find($request->id_descuento);
      $descuento->estado = $request->estado;
      $descuento->save();


      $this->changeLog([
                    'area' => 'descuentos',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $descuento->id,
                ]);


        $alerta = alertasModel::where('referencia', 'descuento_' . $descuento->id)->get();

        if ($alerta!='') {
            foreach($alerta as $temp){
                $temp->delete();
            }
        }

      return redirect()->back()->with('success','Actualizado correctamente!');

    }



    // trae listado de descuentos del colaborador y valida la fecha de pago
    static function get_descuentos(Request $request) {

        $fecha_fin = date_create($request->fin);
        $dia_fin = date_format($fecha_fin, 'd');
        $mes_fecha = date_format($fecha_fin, 'm');


        if ($dia_fin==30 || $dia_fin==31 || ($dia_fin==28&&$mes_fecha==2) ) {

            $dia_pago = "Fin de Mes";
        } elseif($dia_fin==15) {
            $dia_pago = "Quincena";
        }

        $descuentos = descuentosModel::where("id_colab", $request->id_colab)
                                       ->where('id_empresa',$request->id_empresa)
                                       ->where('estado','Activo')
                                       ->get();

        $array_descuentos = array();

        foreach ($descuentos as $desc) {

            $es_fecha = false;

            // si es fecha de debito
            if ($desc->fecha_debito == $dia_pago  || $desc->fecha_debito == 'Quincena y Fin de Mes') {
                $es_fecha = true;
            }

            if ($desc->tipo == 'bancario' || $desc->tipo == 'embargo' || $desc->tipo == 'otros') {

                // primera cuota
                if ($desc->cuota_actual==0) {

                    $fecha_primera_cuota = date_create($desc->primera_cuota);

                    if ($fecha_primera_cuota==$fecha_fin) {
                        $es_fecha = true;
                    } else {
                        $es_fecha = false;
                    }
                }

                $monto_pago = $desc->valor_cuota;
            } else {
                $monto_pago = $desc->monto;
            }

            if ($desc->fecha_debito == 'Quincena y Fin de Mes') {
                $monto_pago = $monto_pago/2;
            }

            if ($es_fecha==true) {

                $array_descuentos[] = array(
                      "id"=>$desc->id,
                      "tipo"=>$desc->tipo,
                      "pago"=>$monto_pago,
                );

            }   

        }

        return $array_descuentos;
    }




    static function get_saldos(Request $request) {


        $descuentos = descuentosModel::where("id_colab", $request->id_colab)
                                       ->where('id_empresa',$request->id_empresa)
                                       ->where('estado','Activo')
                                       ->get();

        $array_descuentos = array();

        foreach ($descuentos as $desc) {

            if ($desc->tipo == 'manutencion') {
                
                $array_descuentos[] = array(
                      "id"=>$desc->id,
                      "tipo"=>$desc->tipo,
                      "pago"=>$desc->monto,
                );

            } else {
                
                $array_descuentos[] = array(
                      "id"=>$desc->id,
                      "tipo"=>$desc->tipo,
                      "pago"=>$desc->saldo,
                );

            }
        }

        return $array_descuentos;
    }




}
