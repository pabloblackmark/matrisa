<?php

namespace App\Http\Controllers\admin\accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\loggsPlanillaModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;

use App\adminModels\changeLogModel;
use App\Http\Controllers\Admin\changeLogController;


use App\apiModels\userViaticosModel;

use App\Http\Controllers\Admin\companies\companyclientsController;
use App\Http\Controllers\Admin\companies\clientsBranchOfficeController;
use App\Http\Controllers\Admin\companies\clientsBranchAreaController;

use App\Http\Controllers\Admin\accounting\pagosController;

class viaticosController extends Controller
{

    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'admin.viaticos.show';
    }

    public function index(Request $request){

        $data = [];
        $clientes = "";
        $sucursales = "";
        $areas = "";

        $config_empresa = null;

        // si hay filtro
        if ($request->id_empresa != null){

            // colaboradores filtro
            if ($request->id_cliente!=null && $request->id_sucursal!=null && $request->id_area!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("idArea", $request->id_area)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null && $request->id_sucursal!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("status", 1)->distinct("idColab")->get();

            } else if ($request->id_cliente!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("status", 1)->distinct("idColab")->get();  
            } else {
                $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("status", 1)->distinct("idColab")->get();
            }

            $clientes = companyclientsController::get_clientes($request);
            $sucursales = clientsBranchOfficeController::get_sucursales($request);
            $areas = clientsBranchAreaController::get_areas($request);

            // configuracion de empresa para calculo de igsss
            $config_empresa = companiesModel::find($request->id_empresa);

        }


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $companies = [];
      foreach($data0 as $d){
          $companies[] = [$d->id,$d->company];
      }

      $model = new userViaticosModel;


          return view('admin.viaticos.index',
                ['menubar'=> $this->list_sidebar(),
                 'companies' => $companies,
                 'colabs'=>$data,
                 'yr'=>date("Y"),
                 'monthSel'=>12,
                 'months'=>$this->monthEsp(),
                 'request'=>$request,
                 'clientes'=>$clientes,
                 'sucursales'=>$sucursales,
                 'areas'=>$areas,
                 'config_empresa'=>$config_empresa,
                 'model'=>$model
                ]);
    }


   public function viaticos_colab(Request $request)
   {

      $id_colab = $request->id_colab;
      $id_empresa = $request->id_empresa;

      $id_cliente = $request->id_cliente;
      $id_sucursal = $request->id_sucursal;
      $id_area = $request->id_area;

      $inicio = $request->inicio;
      $fin = $request->fin;


      $fecha_inicio = date_create($inicio);
      $fecha_fin = date_create($fin);

      $diff_dias = date_diff($fecha_inicio, $fecha_fin);
      $dias_laborados = $diff_dias->format('%d')+1;


      $dia_inicio = date_format($fecha_inicio, 'd');
      $dia_fin = date_format($fecha_fin, 'd');
      $mes_fecha = date_format($fecha_fin, 'm');


      if ($dia_inicio==1&&$dia_fin==31) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==1&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 30;
      } elseif ($dia_inicio==16&&$dia_fin==31) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==28&&$mes_fecha==2) {
          $dias_laborados = 15;
      } elseif ($dia_inicio==16&&$dia_fin==29&&$mes_fecha==2) {
          $dias_laborados = 15;
      }


      $month = 12;
      $years = $nYr = 2021;

      $dias_laborados = $dias_laborados;

      $years = (!empty($nYr)?$nYr:date("Y"));
      $edit = false;
      $data = $addPays = $array_viaticos = [];
      
      if ($id_colab != null){
        $days = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
        $daysEng = ['Monday'=>'Lunes',
                 'Tuesday'=>'Martes',
                 'Wednesday'=>'Miércoles',
                 'Thursday'=>'Jueves',
                 'Friday'=>'Viernes',
                 'Saturday'=>'Sábado',
                 'Sunday'=>'Domingo'];
        $baseDay = ['dayWeek'=>'','dateDay'=>'','ontime'=>'','id'=>0,'hourEnter'=>null,
                    'hourExit'=>null,'hourEnterBetw'=>null,'hourExitBetw'=>null,'outTimeEnter'=>null,'hourDay'=>null,
                    'extraHourDay'=>null,'hourNight'=>null,'extraHourNight'=>null,'extraHourEspecial'=>null,'estado'=>'',
                    'clientName'=>'','tasks'=>'','baseTasks'=>null,'aditTask'=>0];


        $dateDays = [$inicio, $fin];

        $data = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->where('typeday', 'aprobado')
                          ->orderBy("dateDay")
                          ->get();


        $colab = colaboradores::find($id_colab);



        $viaticos = $colab->viaticos*loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("viatics");

        $movil = $colab->movil*loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("viaticsMovil");

        $extras = loggsModel::whereBetween('dateDay', $dateDays)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          // ->where('idClient',$id_cliente)
                          // ->where('idBranch',$id_sucursal)
                          // ->where('idArea',$id_area)
                          ->sum("viaticsAdded");



        $array_viaticos = ["viaticos"=>$viaticos,"movil"=>$movil,"extras"=>$extras];


      }


      $log_viaticos = userViaticosModel::select('id','estado','fecha_pago')
                                  ->where("fecha_inicio",$inicio)
                                  ->where("fecha_fin",$fin)
                                  ->where("id_colab",$id_colab)
                                  ->where('id_empresa',$id_empresa)
                                  // ->where('id_cliente',$id_cliente)
                                  // ->where('id_sucursal',$id_sucursal)
                                  // ->where('id_area',$id_area)
                                  ->first();

      $config_empresa = companiesModel::find($request->id_empresa);

      // change_log
      $data_changelog = array();

      if ($log_viaticos!='') {
          $data_changelog = changeLogModel::where('area', 'viaticos')->where('element_id',  $log_viaticos->id)->orderby('created_at', 'desc')->get();
          $data_changelog = changeLogController::extraer_data($data_changelog);
      }

      return view('admin.viaticos.show',
            ['menubar'=> $this->list_sidebar(),
             'data' => $data,
             'colabInfo'=> $colab,
             'idCompany'=>$id_empresa,
             'idColab'=>$id_colab,
             'dias_laborados'=>$dias_laborados,
             'log_viaticos'=>$log_viaticos,
             'array_viaticos'=>$array_viaticos,
             'request'=>$request,
             'config_empresa'=>$config_empresa,
             'data' => $data,
             'data_changelog' => $data_changelog
            ]);
    }



    function aprobar_colab(Request $request){

      $model = new userViaticosModel;

      $json = $request->datos_viaticos;

      $data = json_decode($json, true);
      $data['estado'] = $request->estado;
      $data['fecha_pago'] = $request->fecha_pago;
      
      $model->fill($data)->save();


      $this->changeLog([
                    'area' => 'viaticos',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $model->id,
                ]);

      // generar boleta de pago
      $request->id_colab = $data['id_colab'];
      $request->id_empresa = $data['id_empresa'];
      $request->id_cliente = $data['id_cliente'];
      $generar_pago = pagosController::generar_pago($request);

      return redirect()->back()->with('success','Guardado correctamente!');
      
    }



    function aprobar_viaticos(Request $request){

      foreach($request->pago as $datos_viaticos){

          $model = new userViaticosModel;

          $json = $datos_viaticos;

          $data = json_decode($json, true);
          $data['estado'] = 'Procesado';
          $data['fecha_pago'] = $request->fecha_pago;
          
          $model->fill($data)->save();

          // generar boleta de pago
          $request->id_colab = $data['id_colab'];
          $request->id_empresa = $data['id_empresa'];
          $request->id_cliente = $data['id_cliente'];
          $generar_pago = pagosController::generar_pago($request);
      }
      
      $url_redirect = 'viaticos?id_empresa='.$request->id_empresa.'&id_cliente='.$request->id_cliente.'&id_sucursal='.$request->id_sucursal.'&id_area='.$request->id_area.'&inicio='.$request->inicio.'&fin='.$request->fin;

      return redirect($url_redirect)->with('success','Procesado correctamente!');
      
    }



    function update_viaticos(Request $request){

      $viaticos = userViaticosModel::find($request->id_viaticos);
      $viaticos->estado = $request->estado;
      $viaticos->fecha_pago = $request->fecha_pago;
      $viaticos->save();


      $this->changeLog([
                    'area' => 'viaticos',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $viaticos->id,
                ]);


      return redirect()->back()->with('success','Actualizado correctamente!');
      
    }




    static function get_viaticos(Request $request) {


        $rango_fechas = [$request->inicio, $request->fin];
        $id_colab = $request->id_colab;
        $id_empresa = $request->id_empresa;


        $colab = colaboradores::find($id_colab);

        $viaticos = $colab->viaticos*loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("viatics");

        $movil = $colab->movil*loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("viaticsMovil");

        $extras = loggsModel::whereBetween('dateDay', $rango_fechas)
                          ->where("userId",$id_colab)
                          ->where('idCompany',$id_empresa)
                          ->sum("viaticsAdded");

        $total_viaticos = $viaticos + $movil + $extras;


        $array_viaticos = ["viaticos"=>$viaticos,"movil"=>$movil,"extras"=>$extras,"total_viaticos"=>$total_viaticos];


        return $array_viaticos;

    }

}
