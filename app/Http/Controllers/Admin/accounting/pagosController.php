<?php

namespace App\Http\Controllers\admin\accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use PDF;

use App\adminModels\changeLogModel;
use App\Http\Controllers\Admin\changeLogController;


use App\apiModels\loggsPlanillaModel;
use App\apiModels\userHorasModel;
use App\apiModels\userViaticosModel;

use App\apiModels\userPagosModel;

use App\Http\Controllers\Admin\companies\companyclientsController;
use App\Http\Controllers\Admin\companies\clientsBranchOfficeController;
use App\Http\Controllers\Admin\companies\clientsBranchAreaController;

class pagosController extends Controller
{

    public function __construct(){
      $this->middleware('auth:admin');
    }

    public function index(Request $request){

        $data = [];
        $clientes = "";
        $sucursales = "";
        $areas = "";

        $config_empresa = null;

        // si hay filtro
        if ($request->id_empresa != null){

            // colaboradores filtro
            if ($request->id_cliente!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("status", 1)->distinct("idColab")->get();
            } else {
                $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("status", 1)->distinct("idColab")->get();
            }


            $clientes = companyclientsController::get_clientes($request);
            $sucursales = clientsBranchOfficeController::get_sucursales($request);
            $areas = clientsBranchAreaController::get_areas($request);

            // configuracion de empresa para calculo de igsss
            $config_empresa = companiesModel::find($request->id_empresa);

        }


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $companies = [];
      foreach($data0 as $d){
          $companies[] = [$d->id,$d->company];
      }

      $modelo_planilla = new loggsPlanillaModel;
      $modelo_horas = new userHorasModel;
      $modelo_viaticos = new userViaticosModel;

      $modelo_pagos = new userPagosModel;

          return view('admin.pagos.index',
                ['menubar'=> $this->list_sidebar(),
                 'companies' => $companies,
                 'colabs'=>$data,
                 'request'=>$request,
                 'clientes'=>$clientes,
                 'config_empresa'=>$config_empresa,
                 'modelo_pagos'=>$modelo_pagos,
                 'modelo_planilla'=>$modelo_planilla,
                 'modelo_horas'=>$modelo_horas,
                 'modelo_viaticos'=>$modelo_viaticos,
                 'modelo_pagos'=>$modelo_pagos
                ]);
    }


   public function pagos_colab(Request $request)
   {

      $id_colab = $request->id_colab;
      $id_empresa = $request->id_empresa;
      $id_cliente = $request->id_cliente;

      $fecha_pago = $request->fecha_pago;


      $data = $array_pagos = [];
      
      if ($id_colab != null){
        $days = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
        $daysEng = ['Monday'=>'Lunes',
                 'Tuesday'=>'Martes',
                 'Wednesday'=>'Miércoles',
                 'Thursday'=>'Jueves',
                 'Friday'=>'Viernes',
                 'Saturday'=>'Sábado',
                 'Sunday'=>'Domingo'];
        $baseDay = ['dayWeek'=>'','dateDay'=>'','ontime'=>'','id'=>0,'hourEnter'=>null,
                    'hourExit'=>null,'hourEnterBetw'=>null,'hourExitBetw'=>null,'outTimeEnter'=>null,'hourDay'=>null,
                    'extraHourDay'=>null,'hourNight'=>null,'extraHourNight'=>null,'extraHourEspecial'=>null,'estado'=>'',
                    'clientName'=>'','tasks'=>'','baseTasks'=>null,'aditTask'=>0];



        $colab = colaboradores::find($id_colab);

      }


      $log_pago = userPagosModel::select('id','estado')
                                                ->where("fecha_pago",$request->fecha_pago)
                                                ->where("id_colab",$id_colab)
                                                // ->where('id_empresa',$request->id_empresa)
                                                // ->where('id_cliente',$request->id_cliente)
                                                ->first();


      $log_planilla = loggsPlanillaModel::where("id_colab",$id_colab)
                                        ->where('id_empresa',$request->id_empresa)
                                        // ->where('id_cliente',$request->id_cliente)
                                        ->where("fecha_pago",$request->fecha_pago)
                                        ->first();

      $log_horas = userHorasModel::where("id_colab",$id_colab)
                                        ->where('id_empresa',$request->id_empresa)
                                        // ->where('id_cliente',$request->id_cliente)
                                        ->where("fecha_pago",$request->fecha_pago)
                                        ->first();

      $log_viaticos = userViaticosModel::where("id_colab",$id_colab)
                                        ->where('id_empresa',$request->id_empresa)
                                        // ->where('id_cliente',$request->id_cliente)
                                        ->where("fecha_pago",$request->fecha_pago)
                                        ->first();


      $planilla = 0;
      $horas = 0;
      $viaticos = 0;
                    
      if ($log_planilla!='') {
          $planilla = $log_planilla['total'];
      }

      if ($log_horas!='') {
          $horas = $log_horas['total'];
      }

      if ($log_viaticos!='') {
          $viaticos = $log_viaticos['total'];
      }

      $total = $planilla + $horas + $viaticos;



      return view('admin.pagos.show',
            ['menubar'=> $this->list_sidebar(),
             'colabInfo'=> $colab,
             'idCompany'=>$id_empresa,
             'idColab'=>$id_colab,
             'log_planilla'=>$log_planilla,
             'log_horas'=>$log_horas,
             'log_viaticos'=>$log_viaticos,
             'log_pago'=>$log_pago,
             'request'=>$request
            ]);
    }



   static function generar_pago(Request $request)
   {

      // dd($request->id_colab);

      $log_pago = userPagosModel::where("fecha_pago",$request->fecha_pago)
                                                ->where("id_colab",$request->id_colab)
                                                ->first();



      if ($log_pago=='') {

          $log_planilla = loggsPlanillaModel::where("id_colab",$request->id_colab)
                                            ->where('id_empresa',$request->id_empresa)
                                            ->where("fecha_pago",$request->fecha_pago)
                                            ->where("estado","Procesado")
                                            ->first();

          $log_horas = userHorasModel::where("id_colab",$request->id_colab)
                                            ->where('id_empresa',$request->id_empresa)
                                            ->where("fecha_pago",$request->fecha_pago)
                                            ->where("estado","Procesado")
                                            ->first();

          $log_viaticos = userViaticosModel::where("id_colab",$request->id_colab)
                                            ->where('id_empresa',$request->id_empresa)
                                            ->where("fecha_pago",$request->fecha_pago)
                                            ->where("estado","Procesado")
                                            ->first();


          if ($log_planilla!='' && $log_horas!='' && $log_viaticos!='') {
                  
              $model = new userPagosModel;
              $data = $request->only($model->getFillable());

              $data['id_empresa'] = $request->id_empresa;
              $data['id_cliente'] = $request->id_cliente;
              $data['id_colab'] = $request->id_colab;

              $data['total_planilla'] = $log_planilla['total'];
              $data['id_planilla'] = $log_planilla['id'];

              $data['total_horas'] = $log_horas['total'];
              $data['id_horas'] = $log_horas['id'];
              
              $data['total_viaticos'] = $log_viaticos['total'];
              $data['id_viaticos'] = $log_viaticos['id'];

              $data['total'] = $log_planilla['total'] + $log_horas['total'] + $log_viaticos['total'];
              
              $data['estado'] = "Pendiente de Aprobar";

              $model->fill($data)->save();

              return true;
          }

      } else {

          return false;

      }

    }



    public function export_pdf(Request $request) {


      $nombre_pdf = "demo_boleta.pdf";
      

      $log_pago = userPagosModel::find($request->id_pago);

      $colab = colaboradores::find($log_pago->id_colab);
      $empresa = companiesModel::find($log_pago->id_empresa);
      // $cliente = companyclientsController::find($log_pago->id_cliente);

      $log_planilla = loggsPlanillaModel::find($log_pago->id_planilla);

      $log_horas = userHorasModel::find($log_pago->id_horas);

      $log_viaticos = userViaticosModel::find($log_pago->id_viaticos);




      $data['id'] = $log_pago->id; 
      $data['id_colab'] = $colab->id;
      $data['nombre_colab'] = $colab->nombres.' '.$colab->apellidos;
      $data['puesto'] = $colab->puesto;
      $data['tipo_pago'] = $colab->tipo_pago;
      $data['estatus'] = $colab->estatus;
      $data['pago_sueldo'] = $colab->pago_sueldo;

      $data['nombre_empresa'] = $empresa->company;

      $data['fecha_pago'] = date("d-m-Y", strtotime($log_planilla->fecha_pago));

      $data['desde'] = date("d-m-Y", strtotime($log_planilla->fecha_inicio));
      $data['hasta'] = date("d-m-Y", strtotime($log_planilla->fecha_fin));
      $data['sueldo'] = $log_planilla->sueldo;
      $data['total'] = $log_pago->total;
      $data['total_ingresos'] = $log_planilla->total + $log_horas->total + $log_viaticos->total;
      $data['total_descuentos'] = $log_planilla->total_descuentos;

      $data['bono_ley'] = $log_planilla->bono_ley;
      $data['desc_igss'] = $log_planilla->desc_igss;


      // todos los datos
      $array_planilla['id'] = $log_planilla->id;
      $array_planilla['fecha_inicio'] = $log_planilla->fecha_inicio;
      $array_planilla['fecha_fin'] = $log_planilla->fecha_fin;
      $array_planilla['tipo'] = $log_planilla->tipo;
      $array_planilla['sueldo'] = $log_planilla->sueldo;
      $array_planilla['total'] = $log_planilla->total;
      $array_planilla['bono_ley'] = $log_planilla->bono_ley;
      $array_planilla['bono_extra'] = $log_planilla->bono_extra;
      $array_planilla['bono_antiguedad'] = $log_planilla->bono_antiguedad;
      $array_planilla['desc_dia'] = $log_planilla->desc_dia;
      $array_planilla['desc_septimo'] = $log_planilla->desc_septimo;
      $array_planilla['desc_igss'] = $log_planilla->desc_igss;
      $array_planilla['descuentos'] = json_decode($log_planilla->descuentos,true);
      $array_planilla['total_bonos'] = $log_planilla->total_bonos;
      $array_planilla['total_descuentos'] = $log_planilla->total_descuentos;
      $array_planilla['estado'] = $log_planilla->estado;
      $array_planilla['fecha_pago'] = $log_planilla->fecha_pago;

      $data['planilla'] = $array_planilla;

      // horas extras
      $array_horas['id'] = $log_horas->id;
      $array_horas['fecha_inicio'] = $log_horas->fecha_inicio;
      $array_horas['fecha_fin'] = $log_horas->fecha_fin;
      $array_horas['tipo'] = $log_horas->tipo;
      $array_horas['total'] = $log_horas->total;
      $array_horas['extra_dia'] = $log_horas->extra_dia;
      $array_horas['extra_noche'] = $log_horas->extra_noche;
      $array_horas['extra_especial'] = $log_horas->extra_especial;
      $array_horas['extra_mixta'] = $log_horas->extra_mixta;
      $array_horas['extra_metas'] = $log_horas->extra_metas;
      $array_horas['desc_igss'] = $log_horas->desc_igss;
      $array_horas['metas'] = $log_horas->metas;
      $array_horas['estado'] = $log_horas->estado;
      $array_horas['fecha_pago'] = $log_horas->fecha_pago;

      $data['horas'] = $array_horas;


      $array_viaticos['id'] = $log_viaticos->id;
      $array_viaticos['fecha_inicio'] = $log_viaticos->fecha_inicio;
      $array_viaticos['fecha_fin'] = $log_viaticos->fecha_fin;
      $array_viaticos['tipo'] = $log_viaticos->tipo;
      $array_viaticos['total'] = $log_viaticos->total;
      $array_viaticos['viaticos'] = $log_viaticos->viaticos;
      $array_viaticos['movil'] = $log_viaticos->movil;
      $array_viaticos['extras'] = $log_viaticos->extras;
      $array_viaticos['estado'] = $log_viaticos->estado;
      $array_viaticos['fecha_pago'] = $log_viaticos->fecha_pago;

      $data['viaticos'] = $array_viaticos;


      $pdf = PDF::loadView('admin.pagos.pdf', $data);

      return $pdf->download($nombre_pdf);

    }


}
