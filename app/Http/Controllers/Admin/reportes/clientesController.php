<?php

namespace App\Http\Controllers\admin\reportes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;

use App\adminModels\puestosModel;

use App\apiModels\loggsModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use PDF;

use App\adminModels\changeLogModel;
use App\Http\Controllers\Admin\changeLogController;


use App\apiModels\loggsPlanillaModel;
use App\apiModels\userHorasModel;
use App\apiModels\userViaticosModel;

use App\apiModels\userPagosModel;

use App\Http\Controllers\Admin\companies\companyclientsController;
use App\Http\Controllers\Admin\companies\clientsBranchOfficeController;
use App\Http\Controllers\Admin\companies\clientsBranchAreaController;


class clientesController extends Controller
{

    public function __construct(){
      $this->middleware('auth:admin');
    }


public static function sum_time($times) {
        $i = 0;
        foreach ($times as $time) {
            sscanf($time, '%d:%d', $hour, $min);
            $i += $hour * 60 + $min;
        }

        if($h = floor($i / 60)) {
            $i %= 60;
        }

        // return sprintf('%02d:%02d', $h, $i)
        return $h;
}


    public function index(Request $request){

        $data = [];
        $clientes = "";
        $sucursales = "";
        $areas = "";

        $config_empresa = null;

        // si hay filtro
        if ($request->id_empresa != null){

            // colaboradores filtro
            if ($request->id_cliente!=null && $request->id_sucursal!=null && $request->id_area!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->where("idArea", $request->id_area)->get();

            } else if ($request->id_cliente!=null && $request->id_sucursal!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->where("idBranch", $request->id_sucursal)->get();

            } else if ($request->id_cliente!=null) {
                $data = client_colab_assocModel::where("idClient", $request->id_cliente)->get();
            } else {
                $data = company_colab_assocModel::where("idCompany", $request->id_empresa)->get();
            }


            $clientes = companyclientsController::get_clientes($request);
            $sucursales = clientsBranchOfficeController::get_sucursales($request);
            $areas = clientsBranchAreaController::get_areas($request);

            // configuracion de empresa para calculo de igsss
            $config_empresa = companiesModel::find($request->id_empresa);

        }


        // echo $data;


      // datos de empresas
      $data0 = companiesModel::orderBy('company')->get();
      $companies = [];
      foreach($data0 as $d){
          $companies[] = [$d->id,$d->company];
      }

      $modelo_planilla = new loggsPlanillaModel;
      $modelo_horas = new userHorasModel;
      $modelo_viaticos = new userViaticosModel;

      $modelo_pagos = new userPagosModel;


      $modelo_logs = new loggsModel;


      $modelo_puestos = new puestosModel;


          return view('admin.reportes.clientes.index',
                ['menubar'=> $this->list_sidebar(),
                 'companies' => $companies,
                 'colabs'=>$data,
                 'request'=>$request,
                 'clientes'=>$clientes,
                 'config_empresa'=>$config_empresa,
                 'modelo_pagos'=>$modelo_pagos,
                 'modelo_planilla'=>$modelo_planilla,
                 'modelo_horas'=>$modelo_horas,
                 'modelo_viaticos'=>$modelo_viaticos,
                 'modelo_pagos'=>$modelo_pagos,
                 'modelo_logs'=>$modelo_logs,
                 'modelo_puestos'=>$modelo_puestos
                ]);
    }


   public function reporte_colab(Request $request)
   {

      $id_colab = $request->id_colab;
      $id_empresa = $request->id_empresa;
      $id_cliente = $request->id_cliente;

      $fecha_pago = $request->fecha_pago;


      $data = $array_pagos = [];
      
      if ($id_colab != null){
        $days = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
        $daysEng = ['Monday'=>'Lunes',
                 'Tuesday'=>'Martes',
                 'Wednesday'=>'Miércoles',
                 'Thursday'=>'Jueves',
                 'Friday'=>'Viernes',
                 'Saturday'=>'Sábado',
                 'Sunday'=>'Domingo'];
        $baseDay = ['dayWeek'=>'','dateDay'=>'','ontime'=>'','id'=>0,'hourEnter'=>null,
                    'hourExit'=>null,'hourEnterBetw'=>null,'hourExitBetw'=>null,'outTimeEnter'=>null,'hourDay'=>null,
                    'extraHourDay'=>null,'hourNight'=>null,'extraHourNight'=>null,'extraHourEspecial'=>null,'estado'=>'',
                    'clientName'=>'','tasks'=>'','baseTasks'=>null,'aditTask'=>0];

        $colab = colaboradores::find($id_colab);

        $puesto = puestosModel::where("nombre",$colab->puesto)->first();

      }


      $reporte_colab = $this->get_reporte_colab($request);


      $log = loggsModel::whereBetween("dateDay", [$request->inicio, $request->fin])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->Where('typeday', 'aprobado')
                                                ->orderBy("dateDay")
                                                ->get();


      return view('admin.reportes.clientes.show',
            ['menubar'=> $this->list_sidebar(),
             'colabInfo'=> $colab,
             'puesto'=>$puesto,
             'idCompany'=>$id_empresa,
             'idColab'=>$id_colab,
             'reporte_colab'=>$reporte_colab,
             'log'=>$log,
             'request'=>$request
            ]);
    }



    public function export_pdf(Request $request) {


      $nombre_pdf = "demo_boleta.pdf";
      

      $log_pago = userPagosModel::find($request->id_pago);

      $colab = colaboradores::find($log_pago->id_colab);
      $empresa = companiesModel::find($log_pago->id_empresa);
      // $cliente = companyclientsController::find($log_pago->id_cliente);

      $log_planilla = loggsPlanillaModel::find($log_pago->id_planilla);

      $log_horas = userHorasModel::find($log_pago->id_horas);

      $log_viaticos = userViaticosModel::find($log_pago->id_viaticos);




      $data['id'] = $log_pago->id; 
      $data['id_colab'] = $colab->id;
      $data['nombre_colab'] = $colab->nombres.' '.$colab->apellidos;
      $data['puesto'] = $colab->puesto;
      $data['tipo_pago'] = $colab->tipo_pago;
      $data['estatus'] = $colab->estatus;
      $data['pago_sueldo'] = $colab->pago_sueldo;

      $data['nombre_empresa'] = $empresa->company;

      $data['fecha_pago'] = date("d-m-Y", strtotime($log_planilla->fecha_pago));

      $data['desde'] = date("d-m-Y", strtotime($log_planilla->fecha_inicio));
      $data['hasta'] = date("d-m-Y", strtotime($log_planilla->fecha_fin));
      $data['sueldo'] = $log_planilla->sueldo;
      $data['total'] = $log_pago->total;
      $data['total_ingresos'] = $log_planilla->total + $log_horas->total + $log_viaticos->total;
      $data['total_descuentos'] = $log_planilla->total_descuentos;

      $data['bono_ley'] = $log_planilla->bono_ley;
      $data['desc_igss'] = $log_planilla->desc_igss;


      // todos los datos
      $array_planilla['id'] = $log_planilla->id;
      $array_planilla['fecha_inicio'] = $log_planilla->fecha_inicio;
      $array_planilla['fecha_fin'] = $log_planilla->fecha_fin;
      $array_planilla['tipo'] = $log_planilla->tipo;
      $array_planilla['sueldo'] = $log_planilla->sueldo;
      $array_planilla['total'] = $log_planilla->total;
      $array_planilla['bono_ley'] = $log_planilla->bono_ley;
      $array_planilla['bono_extra'] = $log_planilla->bono_extra;
      $array_planilla['bono_antiguedad'] = $log_planilla->bono_antiguedad;
      $array_planilla['desc_dia'] = $log_planilla->desc_dia;
      $array_planilla['desc_septimo'] = $log_planilla->desc_septimo;
      $array_planilla['desc_igss'] = $log_planilla->desc_igss;
      $array_planilla['descuentos'] = json_decode($log_planilla->descuentos,true);
      $array_planilla['total_bonos'] = $log_planilla->total_bonos;
      $array_planilla['total_descuentos'] = $log_planilla->total_descuentos;
      $array_planilla['estado'] = $log_planilla->estado;
      $array_planilla['fecha_pago'] = $log_planilla->fecha_pago;

      $data['planilla'] = $array_planilla;

      // horas extras
      $array_horas['id'] = $log_horas->id;
      $array_horas['fecha_inicio'] = $log_horas->fecha_inicio;
      $array_horas['fecha_fin'] = $log_horas->fecha_fin;
      $array_horas['tipo'] = $log_horas->tipo;
      $array_horas['total'] = $log_horas->total;
      $array_horas['extra_dia'] = $log_horas->extra_dia;
      $array_horas['extra_noche'] = $log_horas->extra_noche;
      $array_horas['extra_especial'] = $log_horas->extra_especial;
      $array_horas['extra_mixta'] = $log_horas->extra_mixta;
      $array_horas['extra_metas'] = $log_horas->extra_metas;
      $array_horas['desc_igss'] = $log_horas->desc_igss;
      $array_horas['metas'] = $log_horas->metas;
      $array_horas['estado'] = $log_horas->estado;
      $array_horas['fecha_pago'] = $log_horas->fecha_pago;

      $data['horas'] = $array_horas;


      $array_viaticos['id'] = $log_viaticos->id;
      $array_viaticos['fecha_inicio'] = $log_viaticos->fecha_inicio;
      $array_viaticos['fecha_fin'] = $log_viaticos->fecha_fin;
      $array_viaticos['tipo'] = $log_viaticos->tipo;
      $array_viaticos['total'] = $log_viaticos->total;
      $array_viaticos['viaticos'] = $log_viaticos->viaticos;
      $array_viaticos['movil'] = $log_viaticos->movil;
      $array_viaticos['extras'] = $log_viaticos->extras;
      $array_viaticos['estado'] = $log_viaticos->estado;
      $array_viaticos['fecha_pago'] = $log_viaticos->fecha_pago;

      $data['viaticos'] = $array_viaticos;


      $pdf = PDF::loadView('admin.reportes.clientes.pdf', $data);

      return $pdf->download($nombre_pdf);

    }


    static function get_reporte_colab(Request $request) {

          // info colaborador
          $colabInfo = colaboradores::find($request->id_colab);

          // puesto
          $puesto = puestosModel::where("nombre",$colabInfo->puesto)->first();


          // filtro por fechas de inicio y fin
          $from = date('Y-m-d',strtotime($request->inicio));
          $to = date('Y-m-d',strtotime($request->fin));


          // log horas laborales
          $horas_log = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->Where('typeday', 'aprobado')
                                                ->get();

          $horas_dia = 0;
          $horas_noche = 0;
          $horas_mixta = 0;

          foreach ($horas_log as $dia) {
                            
              $horarioInfo = $dia->getHorary()->first();

              $horas_dia += (int)$horarioInfo->horas_diurnas;
              $horas_noche += (int)$horarioInfo->horas_nocturnas;
              $horas_mixta += (int)$horarioInfo->horas_mixtas;

          }                          

          if ($horas_dia>0) {
              $horas_dia = $horas_dia/60;
          }

          if ($horas_noche>0) {
              $horas_noche = $horas_noche/60;
          }

          if ($horas_mixta>0) {
              $horas_mixta = $horas_mixta/60;
          }

          $pago_horas_dia = $horas_dia * $puesto->payDT;
          $pago_horas_noche = $horas_noche * $puesto->payNT;
          $pago_horas_mixta = $horas_mixta * $puesto->payDT;

          $pago_total_horas = $pago_horas_dia + $pago_horas_noche + $pago_horas_mixta;


          // Horas extras
          $extras_dia = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->sum("extraHourDayMiddle");

          $extras_noche = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->sum("extraHourNightMiddle");

          $extras_mixta = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->sum("extraHourMixMiddle");

          $extras_especial = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->sum("aditTask");

          $extras_dia = $extras_dia/2;
          $pago_extras_dia = $extras_dia * $puesto->payEDT;

          $extras_noche = $extras_noche/2;
          $pago_extras_noche = $extras_noche * $puesto->payENT;

          $extras_mixta = $extras_mixta/2;
          $pago_extras_mixta = $extras_mixta * $puesto->extraPay;

          $pago_extras_especial = $extras_especial * $puesto->points;

          $pago_total_extras = $pago_extras_dia + $pago_extras_noche + $pago_extras_mixta + $pago_extras_especial;


          // Viaticos
          $viaticos = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->sum("viatics");

          $viaticos_movil = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->sum("viaticsMovil");

          $viaticos_extra = loggsModel::whereBetween("dateDay", [$from, $to])
                                                ->where("userId",$request->id_colab)
                                                ->where('idCompany',$request->id_empresa)
                                                ->where('idClient',$request->id_cliente)
                                                ->sum("viaticsAdded");


          $pago_viaticos = $viaticos * $puesto->perDiem;
          $pago_viaticos_movil = $viaticos_movil * $puesto->movil;
          $pago_viaticos_extra = $viaticos_extra + ( $viaticos_extra * ($puesto->viaticos_extra/100) );


          $pago_total_viaticos = $pago_viaticos + $pago_viaticos_movil + $pago_viaticos_extra;

          $total = $pago_total_horas + $pago_total_extras + $pago_total_viaticos;



          $reporte_colab = ["horas_dia"=>$horas_dia,
                            "horas_noche"=>$horas_noche,
                            "horas_mixta"=>$horas_mixta,
                            "pago_horas_dia"=>$pago_horas_dia,
                            "pago_horas_noche"=>$pago_horas_noche,
                            "pago_horas_mixta"=>$pago_horas_mixta,
                            "pago_total_horas"=>$pago_total_horas,
                            
                            "extras_dia"=>$extras_dia,
                            "extras_noche"=>$extras_noche,
                            "extras_mixta"=>$extras_mixta,
                            "extras_especial"=>$extras_especial,
                            "pago_extras_dia"=>$pago_extras_dia,
                            "pago_extras_noche"=>$pago_extras_noche,
                            "pago_extras_mixta"=>$pago_extras_mixta,
                            "pago_extras_especial"=>$pago_extras_especial,
                            "pago_total_extras"=>$pago_total_extras,

                            "viaticos"=>$viaticos,
                            "viaticos_movil"=>$viaticos_movil,
                            "viaticos_extra"=>$viaticos_extra,
                            "pago_viaticos"=>$pago_viaticos,
                            "pago_viaticos_movil"=>$pago_viaticos_movil,
                            "pago_viaticos_extra"=>$pago_viaticos_extra,
                            "pago_total_viaticos"=>$pago_total_viaticos,

                            "total"=>$total
                          ];

          return $reporte_colab;
    }

}
