<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\unidadesModel;

class unidadesController extends Controller
{

    private $baseModel;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->baseModel = new unidadesModel;
    }

    public function index(){
        $data = $this->baseModel::orderBy("created_at")->get();
        $tipos = [
            ['volumen', 'volumen'],
            ['peso', 'peso'],
            ['longitud', 'longitud'],
            ['tiempo', 'tiempo'],
            ['contable', 'contable'],
            ['otro', 'otro'],
        ];
        return view('admin.productos.unidadesIndex', [
                'menubar'=> $this->list_sidebar(),
                'data' => $data,
                'tipos' => $tipos
            ]);
    }

    public function store(Request $request) {
        $data = $request->only($this->baseModel->getFillable());
        $this->baseModel->fill($data)->save();
        return redirect()->back()->with('success','Guardado correctamente!');
    }

    public function update(Request $request, $id) {
        $model = $this->baseModel::find($id);
        $data = $request->only($this->baseModel->getFillable());
        $model->fill($data)->save();
        return redirect()->back()->with('info','Actualizado correctamente!');
    }

    public function destroy($id) {
        try {
        $this->baseModel::destroy($id);
            return redirect()->back()->with('warning','Borrado correctamente');
        }catch (\Exception $e) {
            return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
        }
    }
}
