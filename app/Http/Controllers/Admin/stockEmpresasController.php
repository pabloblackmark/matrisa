<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\productosModel;
use App\adminModels\stockIngresosModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\companyclientsBranchModel;
use App\adminModels\stockEgresosModel;
use App\adminModels\stockModel;
use App\adminModels\unidadesModel;
use App\adminModels\companyclientsAssocModel;

class stockEmpresasController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $elem = new facturasModel;
        $data = $request->only($elem->getFillable());
        $data['bodega_id'] = $id;
        $elem->fill($data);
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company_id, $bodega_id)
    {
        $bodega = bodegasModel::find($bodega_id);
        $productos_query = productosModel::get();

        $productos = [];
        $productos_arr = [];
        $productos_obj = [];
        foreach($productos_query as $temp){
            $productos[$temp->id] = $temp->nombre;
            $productos_arr[] = [$temp->id, $temp->nombre];
            $productos_obj[$temp->id] = $temp;
        }

        $colaboradores_query = colaboradores::get();
        $colaboradores = [['', '- - -']];
        foreach($colaboradores_query as $temp){
            $colaboradores[] = [$temp->id, $temp->apellidos . ', ' . $temp->nombres];
        }

        $asociados = [];
        $asociados_query = companyclientsAssocModel::where('idCompany', $company_id)->get();
        foreach($asociados_query as $temp){
            $asociados[] = $temp->idClient;
        }

        $clientes_query = companyclientsModel::whereIn('id', $asociados)->get();

        $clientes = [['', '- - -']];
        $clientesSedes = [['', '- - -']];
        foreach($clientes_query as $temp){
            $clientes[] = [$temp->id, $temp->clientName];
            $sedes = companyclientsBranchModel::where('idClient', $temp->id)->get();
            foreach($sedes as $temp2){
                $clientesSedes[] = [$temp2->bodega_id, $temp->clientName . ' - Sede ' . $temp2->namebranch];
            }
        }

        $unidades_query = unidadesModel::get();
        $unidades = [['', '']];
        foreach($unidades_query as $temp){
            $unidades[] = [$temp->id, $temp->nombre];
        }

        $this->calcularStock($bodega_id);
        $data = stockModel::where('bodega_id', $bodega_id)->get();
        foreach($data as $temp){
            $prod = productosModel::find($temp->producto_id);
            $temp->producto_element = $prod;
            $medi = unidadesModel::find($temp->medida_id);
            $temp->medida_element = $medi;
        }

        return view('admin.stock.empresaIndexProducto',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega_id' => $bodega_id,
                'bodega' => $bodega,
                'productos' => $productos,
                'productos_arr' => $productos_arr,
                'productos_obj' => $productos_obj,
                'unidades' => $unidades,
                'colaboradores' => $colaboradores,
                'clientes' => $clientes,
                'clientesSedes' => $clientesSedes
            ]);
    }

    public function showProductos($bodega_id, $factura_id)
    {
        $factura = facturasModel::find($factura_id);
        $bodega = bodegasModel::find($bodega_id);
        $data = stockIngresosModel::where('factura_id', $factura_id)->get();

        $productos_query = productosModel::get();
        $productos = [['', '- - -']];
        foreach($productos_query as $temp){
            $productos[] = [$temp->id, $temp->nombre . ' ' . $temp->codigo];
        }

        $suma = 0;
        foreach($data as $temp){
            $suma += $temp->valor_total;
        }

        // $medidas_query = medidasModel::get();
        $medidas = [['', '- - -'], [1, 'litro']];
        // foreach($medidas_query as $temp){
        //     $medidas[] = [$temp->id, $temp->nombre . ' ' . $temp->codigo];
        // }

        return view('admin.facturas.productos',
            [
                'menubar' => $this->list_sidebar(),
                'alertas' => $this->list_alertas(),
                'data' => $data,
                'bodega' => $bodega,
                'factura' => $factura,
                'productos' => $productos,
                'medidas' => $medidas,
                'suma' => $suma
            ]);
    }

    public function storeProductos(Request $request, $bodega_id, $factura_id)
    {

        $elem = new stockIngresosModel;
        $data = $request->only($elem->getFillable());
        $data['bodega_id'] = $bodega_id;
        $data['factura_id'] = $factura_id;

        $elem->fill($data)->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProductos(Request $request, $bodega_id, $factura_id, $ingreso_id)
    {
        $elem = new stockIngresosModel;
        $elem = $elem->find($ingreso_id);
        $elem->fill($request->only($elem->getFillable()));
        $elem->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }


    /**
     * Egreso de producto
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function egreso(Request $request, $company_id, $bodega_id, $movimiento_stock_id)
    {
        $egreso = new stockEgresosModel;
        $data_egreso = $request->only($egreso->getFillable());
        $data_egreso['bodega_id'] = $bodega_id;
        $data_egreso['valor_total'] = $request->costo_unitario * $request->cantidad;
        $data_egreso['cantidad_medida'] = $request->cantidad;
        $egreso->fill($data_egreso);
        $egreso->save();

        //////---------ingreso a la bodega cliente
        $ingreso = new stockIngresosModel;
        $data_ingreso = $request->only($ingreso->getFillable());

        $data_ingreso['bodega_id'] = $request->receptor_bodega_id;
        $data_ingreso['producto_id'] = $request->producto_id;
        $data_ingreso['cantidad_medida'] = $request->cantidad;
        $data_ingreso['medida_id'] = $request->medida;
        $data_ingreso['valor_total'] = $request->costo_unitario * $request->cantidad;
        $ingreso->fill($data_ingreso);
        $ingreso->save();

        $this->calcularStock($bodega_id);
        $this->revisar_stock_producto($request->producto_id);

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        bancosModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }

}
