<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\changeLogModel;
use App\adminModels\companiesModel;
use App\adminModels\companyclientsModel;
use App\adminModels\colaboradores;
use App\adminModels\puestosModel;
use App\adminModels\providers;

use App\apiModels\loggsPlanillaModel;
use App\apiModels\userHorasModel;
use App\apiModels\userViaticosModel;

use App\apiModels\prestacionesModel;
use App\apiModels\liquidacionModel;

use App\apiModels\descuentosModel;

class changeLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $data = changeLogModel::orderby('created_at', 'desc')->get();

        $data = $this->extraer_data($data);

        return view('admin.changeLog.index',
            [
                'menubar'=> $this->list_sidebar(),
                'data' => $data
        ]);
    }

    public function show($tipo, $element_id){
        $data = [];
        $data = changeLogModel::where('area', $tipo)->where('element_id', $element_id)->orderby('created_at', 'desc')->get();
        $data = $this->extraer_data($data);

        return view('admin.changeLog.index',
            [
                'menubar'=> $this->list_sidebar(),
                'data' => $data
        ]);
    }

    public static function extraer_data($data){
        foreach($data as $temp){
            $element_id = $temp->element_id;
            $temp->element = '';

            if ($element_id != null){
                switch ($temp->area){
                    case 'company':
                        $element = companiesModel::find($element_id);
                        $temp->element = 'Empresa: ' . $element->company;
                        break;

                    case 'clients':
                        $element = companyclientsModel::find($element_id);
                        $temp->element = 'Cliente: ' . $element->clientName;
                        break;

                    case 'colaboradores':
                        $element = colaboradores::find($element_id);
                        $temp->element = 'Colaborador: ' . $element->apellidos . ', ' . $element->nombres;
                        break;

                    case 'puestos':
                        $element = puestosModel::find($element_id);
                        $temp->element = 'Puesto: ' . $element->nombre;
                        break;

                    case 'planilla':
                        $element = loggsPlanillaModel::find($element_id);
                        $temp->element = 'Planilla: ' . $element->id;
                        break;

                    case 'horas':
                        $element = userHorasModel::find($element_id);
                        $temp->element = 'Horas: ' . $element->id;
                        break;

                    case 'viaticos':
                        $element = userViaticosModel::find($element_id);
                        $temp->element = 'Viaticos: ' . $element->id;
                        break;

                    case 'prestaciones':
                        $element = prestacionesModel::find($element_id);
                        $temp->element = 'Prestaciones: ' . $element->id;
                        break;

                    case 'liquidacion':
                        $element = liquidacionModel::find($element_id);
                        $temp->element = 'Liquidacion: ' . $element->id;
                        break;                                                

                    case 'descuentos':
                        $element = descuentosModel::find($element_id);
                        $temp->element = 'Descuento: ' . $element->id;
                        break;

                    default:
                        $temp->element = '';
                        break;


                  }
            }
        }

        return $data;
    }

}
