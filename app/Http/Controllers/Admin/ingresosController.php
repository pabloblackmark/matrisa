<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\productosModel;
use App\adminModels\stockIngresosModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\stockEgresosModel;
use App\adminModels\unidadesModel;

class ingresosController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($bodega_id)
    {
        $bodega = bodegasModel::find($bodega_id);
        $data = stockIngresosModel::where('bodega_id', $bodega_id)->get();
        foreach($data as $temp){
            $prod = productosModel::find($temp->producto_id);
            $temp->producto = $prod;
            $medida = unidadesModel::find($temp->medida_id);
            $temp->medida_element = $medida;
            $medida_final = unidadesModel::find($temp->medida);
            $temp->medida_final_element = $medida_final;
        }

        return view('admin.bodegas.ingresos',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega
            ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        bancosModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }
}
