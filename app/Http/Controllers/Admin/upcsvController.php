<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\puestosModel;
use App\adminModels\colaboradores;

class upcsvController extends Controller
{
  protected $redirecUlr;
  public function __construct()
  {

  }
  public function show($typo){
    return view('welcome',[
      "tipos"=>$typo
    ]);
  }

  public function store(Request $request, $typo="puestos") {
    // dd('POST');
    // dd($request->file('archivo'));
    if(!empty($request->file('archivo'))){

      $csvFile = self::uploadFilesLocal($request->file('archivo'),'csvsUp/');
      $dataCsv = array_map('str_getcsv', file(asset('storage/'.$csvFile)));
      // $dataCsv = asset('storage/'.$csvFile);
      // dd($dataCsv);
      // dd(asset('storage/'.$csvFile));
      if($typo=='puestos'){
        foreach($dataCsv AS $valus){
          $nombre = trim($valus[0]);

          if ($nombre!= ""){
              $descripcion = trim($valus[1]);

              $model = new puestosModel;
              $model->nombre = $nombre;
              $model->descripcion = $descripcion;

              $model->payDT = isset($valus[2]) ? trim($valus[2]) : 0;
              $model->payNT = isset($valus[3]) ? trim($valus[3]) : 0;
              $model->payEDT = isset($valus[4]) ? trim($valus[4]) : 0;
              $model->payENT = isset($valus[5]) ? trim($valus[5]) : 0;
              $model->extraPay = isset($valus[6]) ? trim($valus[6]) : 0;
              $model->perDiem = isset($valus[7]) ? trim($valus[7]) : 0;
              $model->movil = isset($valus[8]) ? trim($valus[8]) : 0;
              $model->points = isset($valus[9]) ? trim($valus[9]) : 0;

              $model->id_task_manager = $this->generateRandomManagerId($nombre);
              $model->id_horary_manager = $this->generateRandomManagerId($nombre);
              $model->save();
          }

        }

      }else if($typo=='colabs'){
        $heads = $dataCsv[0];
        unset($dataCsv[0]);
        $insertL = [];
        // dd($heads,$dataCsv);
        foreach($dataCsv AS $valus){
          foreach($valus AS $ky=>$valor){
            if($valor===0 || empty($valor)){
              $valor = '0';
            }
            $insertL[$heads[$ky]] = $valor;
          }
          $insertL["fecha_ingreso"] = date("Y-m-d",strtotime($insertL["fecha_ingreso"]));
          $insertL["fecha"] = date("Y-m-d",strtotime($insertL["fecha"]));
          // dd($valus,$insertL);
          $exit = colaboradores::find($valus[0]);
          if(empty($exit)){
            $model = new colaboradores;
            $model->fill($insertL)->save();
          }
          // dd($insertL,$exit );
        }
      }



          return redirect()->back()->with('Guardado');
      }
  }
}
