<?php

namespace App\Http\Controllers\admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\UserAdmin;
use App\adminModels\roles;
use App\adminModels\mensajesModel;
use App\adminModels\colaboradores;

use App\adminModels\codigosModel;
use App\adminModels\providers;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Mail;


 use App\Mail\CodigoEmail;
 use Illuminate\Support\HtmlString;

 use App\adminModels\facturasModel;
 use App\adminModels\stockIngresosModel;
 use App\adminModels\productosModel;
 use App\adminModels\unidadesModel;

class codigosController extends Controller
{
  protected $redirecUlr;
  public function __construct()
  {
      $this->middleware('auth:admin');
  }


  public function index(Request $request) {

    $proveedores = providers::get();

    foreach($proveedores AS $proveedor) {
        $proveedor_nombre[$proveedor->id] = $proveedor->nombre;
    }


    if ($request->id_provider!='') {
        $codigos = codigosModel::where('id_admin', Auth::user()->id)->Where('id_provider', $request->id_provider)->orderBy('id', 'DESC')->get();
    } else {
        $codigos = codigosModel::where('id_admin', Auth::user()->id)->orderBy('id', 'DESC')->get();
    }
    

    return view('admin.codigos.index',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$codigos,
           'proveedores'=>$proveedores,
           'proveedor_nombre'=>$proveedor_nombre,
           'request'=>$request
          ]);
  }



   public function show(Request $request, $id)
    {

        $user = Auth::user();

        $codigo = codigosModel::find($id);

        $colaboradores = colaboradores::get();

        return view('admin.codigos.show', ['menubar'=> $this->list_sidebar(),
           'codigo'=>$codigo,
           'user'=>$user,
           'colaboradores'=>$colaboradores,
          ]);
    }


    public function create(Request $request)
    {

        $user = Auth::user();

        $colaboradores = colaboradores::get();

        return view('admin.codigos.create', ['menubar'=> $this->list_sidebar(),
           'user'=>$user,
           'colaboradores'=>$colaboradores,
          ]);
    }


  public function store(Request $request) {

    // id_provider, id_factura, no_factura, valor, info

    $model = new codigosModel;
    $data = $request->only($model->getFillable());

    $data['id_admin'] =  Auth::user()->id;

    $data['codigo'] =  $request->id_provider."-CP-".date("Y")."-".date("m")."-";

    
    $today = date("Y-m-d");
    $data['fecha_pago'] = date("Y-m-d", strtotime("$today +1 month"));

    $model->fill($data)->save();

    $model->codigo = $model->codigo.$model->id;
    $codigo = $model->codigo;

    $model->save();




    // envia mensaje por correo
    $proveedores = providers::get();

    foreach($proveedores AS $proveedor) {
        $proveedor_nombre[$proveedor->id] = $proveedor->nombre;
    }


    // busca datos de el proveedor
    $proveedor = providers::find($model->id_provider);


    $fecha_pago = date("d-m-Y", strtotime($model->fecha_pago));

    $mensaje = "Se emite este correo como contraseña de pago a favor de ".$proveedor_nombre[$model->id_provider]." con fecha ".$fecha_pago." con los datos siguientes:<br><br>Contraseña de pago: ".$model->codigo."<br>Factura: ".$model->no_factura."<br>Valor de factura: Q ".$model->valor."<br>Detalle de factura: ".$model->info;


    $subject = "Contraseña de pago: ".$model->codigo;

    $data_messege = ['subject' => $subject,
                     'message' => new HtmlString($mensaje)];;

    Mail::to('eddy@inblackmark.com')->send(new CodigoEmail($data_messege));



    return redirect('codigos')->with('success','Codigo Creado!');
  }



  public static function crear_codigo($id_factura) {

    // id_provider, id_factura, no_factura, valor, info
    $factura = facturasModel::find($id_factura);

    $data['id_provider'] = $factura->proveedor_id;
    $data['id_factura'] = $factura->id;
    $data['no_factura'] = $factura->numero;
    $data['valor'] = $factura->total;
    $data['info'] = $factura->info;

    $model = new codigosModel;

    $data['id_admin'] =  Auth::user()->id;

    $data['codigo'] =  $factura->proveedor_id."-CP-".date("Y")."-".date("m")."-";

    
    $today = date("Y-m-d");
    $data['fecha_pago'] = date("Y-m-d", strtotime("$today +1 month"));

    $model->fill($data)->save();

    $model->codigo = $model->codigo.$model->id;
    $codigo = $model->codigo;

    $model->save();





    // detalle de la factura

    $data_detalle = stockIngresosModel::where('factura_id', $id_factura)->get();

        $productos = productosModel::get();
        $productos_obj = [];
        foreach($productos as $temp){
            $productos_obj[$temp->id] = $temp;
        }

        $unidades = unidadesModel::get();
        $unidades_obj = [];
        foreach($unidades as $temp){
            $unidades_obj[$temp->id] = $temp;
        }

    $mensaje_data = '<table id="productos_factura_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Medida</th>
                                <th>Cantidad final</th>
                                <th>Medida final</th>
                                <th>Costo unitario (medida final)</th>
                                <th>Valor total</th>
                            </tr>
                        </thead>

                        <tbody>';
                            foreach ($data_detalle as $value) {
                $mensaje_data .='<tr>
                                    <td>'. $productos_obj[$value->producto_id]->nombre .'</td>
                                    <td align="center">'. $value->cantidad_medida .'</td>
                                    <td align="center">'. $unidades_obj[$value->medida_id]->nombre .'</td>
                                    <td align="center">'. $value->cantidad .'</td>
                                    <td align="center">'. $unidades_obj[$value->medida]->nombre .'</td>
                                    <td align="center">Q. '. number_format($value->costo_unitario, 2) .'</td>
                                    <td><h5>Q. '. number_format($value->valor_total, 2) .'</h5></td>
                                </tr>';
                            }


        $mensaje_data .= '</tbody>
                    </table>';    


    //dd($mensaje_data);


    // envia mensaje por correo

    // busca datos de el proveedor
    $proveedor = providers::find($factura->proveedor_id);

    $fecha_pago = date("d-m-Y", strtotime($model->fecha_pago));


    $mensaje = "Se emite este correo como contraseña de pago a favor ".$proveedor->nombre." con fecha ".$fecha_pago." con los datos siguientes:<br><br>Contraseña de pago: ".$model->codigo."<br>Factura: ".$model->no_factura."<br>Valor: <strong>Q ".$model->valor."</strong><br>Detalle de factura: ".$model->info;



    $subject = "Contraseña de pago: ".$model->codigo;

    $data_message = ['subject' => $subject,
                     'mensaje' => new HtmlString($mensaje),
                     'detalle' => new HtmlString($mensaje_data)
                   ];


    // dd($data_message);

    Mail::to($proveedor->email)->send(new CodigoEmail($data_message));
    // Mail::to('fredyvillavicencio@gmail.com')->send(new CodigoEmail($data_message));

    return true;
  }


  public function ver_email(Request $request) {


    $factura = facturasModel::find($request->id_factura);

    $data = stockIngresosModel::where('factura_id', $request->id_factura)->get();

        $productos = productosModel::get();
        $productos_obj = [];
        foreach($productos as $temp){
            $productos_obj[$temp->id] = $temp;
        }

        $unidades = unidadesModel::get();
        $unidades_obj = [];
        foreach($unidades as $temp){
            $unidades_obj[$temp->id] = $temp;
        }


    return view('emails.codigo_full',
          ['mensaje'=>'Texto del mensaje completo.',
           'data'=>$data,
           'factura'=>$factura,
           'productos_obj' => $productos_obj,
           'unidades_obj' => $unidades_obj,
          ]);


  }




  public function destroy($id) {

    codigosModel::destroy($id);

    return redirect('codigos')->with('success','codigo Borrado!');
  }



  public function update(Request $request, $id) {

    $codigo = codigosModel::find($id);

    $data = $request->only($codigo->getFillable());

    $data['id_admin'] =  Auth::user()->id;

    $codigo->fill($data)->save();


    return redirect('codigos')->with('success','codigo Actualizado!');


  }



  public static function cambio_estado($data) {

    // id_factura, status

    $codigo = codigosModel::Where('id_factura', $data["id_factura"])->get();

    if ($codigo!='') {
        $codigo->status = $data["status"];
        $codigo->save();

        return true;
    } else {
        return false;
    }

  }



}
