<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
class colaboradoresCompanyController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'colaboradoresCompany';
    }

    public function index($id){
        // status 0 es disponible y no asignado antes, status = 1 asignado osea no disponible, status = 2 es estuvo asignado pero ya no, osea disponible
        $data0 = company_colab_assocModel::where('idCompany', $id)->where('status', '1')->get();
        $colab = [];
        foreach($data0 as $d){
            $colab[] = $d->idColab;
        }
        $data1 = colaboradores::whereIn('id', $colab)->get();
        $assi = [];
        foreach($data0 as $d0){
            foreach($data1 as $d1){
                if ($d1->id == $d0->idColab){
                    $assi[$d0->id] = [
                        'id' => $d1->id,
                        'nombres' => $d1->nombres,
                        'apellidos' => $d1->apellidos,
                        'puesto' => $d1->puesto
                    ];
                }
            }
        }

        // dd($assi);

        $data = company_colab_assocModel::where('status', '1')->get();

        $occupied = [];
        foreach($data as $d){
            $occupied[] = $d->idColab;
        }
        // dd($occupied);

        $data2 = colaboradores::whereNotIn('id', $occupied)->get();

        $disp = ["Sin definir" => []];
        foreach($data2 as $k => $v){
            if ($v->puesto){
                $disp[$v->puesto][$v->id] = $v->nombres . ' ' . $v->apellidos;
            }else{
                $disp["Sin definir"][$v->id] = $v->nombres . ' ' . $v->apellidos;
            }
        }

        $nameCompany = isset(companiesModel::find($id)['company']) ? companiesModel::find($id)['company'] : '';

        return view('admin.colaboradores.assign',
              ['menubar'=> $this->list_sidebar(),
               'assi' => $assi,
               'disp' => $disp,
               'idCompany' => $id,
               'nameCompany' => $nameCompany
              ]);
    }

    public function assign(Request $request){

        $model = new company_colab_assocModel;
        $data = $request->only($model->getFillable());
        $data['status'] = 1;
        $model->fill($data)->save();

        return redirect()->back()->with('success','Asignado correctamente');
    }

    public function unassign($cid, $id){

        $elem = company_colab_assocModel::find($id);
        // dd($elem);
        $elem->status = 2;
        $elem->save();

        return redirect()->back()->with('success','Desasignado correctamente');
    }

    public function show($id=null){
        $edit = false;
        $data = [];

        if ($id != null){
            $data = colaboradores::find($id);
            $edit = true;
        }

        return view('admin.colaboradores.show',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'edit' => $edit
              ]);
    }

    public function store(Request $request) {
        $model = new colaboradores;
        $data = $request->only($model->getFillable());
        try{

            if ($request->file('foto')){
                $foto = $request->file('foto')->storePublicly('fotos', 'public');
                $data['foto'] = $foto;
            }

            $model->fill($data)->save();

            return redirect($this->back)->with('success','Guardado correctamente!');
        }catch(\Exception $e){
            return redirect()->back()->with('warning','No se pudieron guardar los datos: ' . $e);
        }

    }

    public function update(Request $request, $id) {
        $model = new colaboradores;
        $finded = $model::find($id);
        $dataMod = $request->only($model->getFillable());

        if ($request->file('foto')){
            $foto = $request->file('foto')->storePublicly('fotos', 'public');
            $dataMod['foto'] = $foto;
        }

        $finded->fill($dataMod)->save();
        return redirect($this->back)->with('info','Actualizado correctamente!');
    }

    public function destroy($id) {
        try {
            colaboradores::destroy($id);
            return redirect()->back()->with('warning','Borrado correctamente!');
          }catch (\Exception $e) {
           return redirect()->back()->with('error','No se puede eliminar porque se está utilizando por un usuario del módulo -Personal-. Primero debe eliminar esta configuración.'.$e->getCode());
        }
    }
    public function workReportx($id,$idComp,$nWk=null,$nYr=null){

      $weekNumb = (!empty($nWk)?$nWk:date("W"));
      $years = (!empty($nYr)?$nYr:date("Y"));
      $week_array = $this->getStartAndEndDate($weekNumb,$years);
      // dd($week_array);
      $edit = false;
      $data = [];
      if ($id != null){
        $days = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
        $daysEng = ['Monday'=>'Lunes',
                 'Tuesday'=>'Martes',
                 'Wednesday'=>'Miércoles',
                 'Thursday'=>'Jueves',
                 'Friday'=>'Viernes',
                 'Saturday'=>'Sábado',
                 'Sunday'=>'Domingo'];
        // dd(date("l"),date("L"));
        $baseDay = ['dayWeek'=>'','dateDay'=>'','ontime'=>'','id'=>0,'hourEnter'=>null,
                    'hourExit'=>null,'hourEnterBetw'=>null,'hourExitBetw'=>null,'outTimeEnter'=>null,'hourDay'=>null,
                    'extraHourDay'=>null,'hourNight'=>null,'extraHourNight'=>null,'extraHourEspecial'=>null,'estado'=>'',
                    'clientName'=>'','tasks'=>'','baseTasks'=>null,'aditTask'=>0];
        /***permiso sin recuperacion de tiempo***/
        $tiemDefault = defaulttimepermisionsModel::where("idUser",$id)->
        whereBetween('datePerm', [$week_array["week_start"],$week_array["week_end"]])
        ->get();
        $dateDefPerm = [];
        foreach($tiemDefault AS $permis){
          $date = $permis->datePerm;
          $date = strtotime($date);
          $date = strtotime("+".($permis->days-1)." day", $date);
          // echo date('M d, Y', $date);
          // dd($permis->datePerm,$date,date('Y-m-d', $date));
          $period = new DatePeriod(
                         new DateTime($permis->datePerm),
                         new DateInterval('P1D'),
                         new DateTime(date('Y-m-d',$date).' 23:59:59')
                    );
          // dd($period);
          // $dateDefPerm = [];
          foreach ($period as $key => $value) {
              $dayP = $value->format('l');
              $dateDefPerm[$daysEng[$dayP]] = [$value->format("Y-m-d"),
                                               ((strtolower($permis->typos)=='otro')?$permis->typeOther:$permis->typos),
                                              '<b>Tipo:</b>'.((strtolower($permis->typos)=='otro')?$permis->typeOther:$permis->typos).'<br>'.
                                              '<b>Descripción:</b>'.$permis->description];
          }
        }
        // dd($dateDefPerm);
        /***permiso con recuperacion de tiempo***/
        $timeperms = timepermisionsModel::where("idUser",$id)->
        whereBetween('dateOut', [$week_array["week_start"],$week_array["week_end"]])
        ->get();
        $datePerms = [];
        foreach($timeperms AS $permis){
          $period = new DatePeriod(
                         new DateTime($permis->dateOut),
                         new DateInterval('P1D'),
                         new DateTime($permis->dateEnter)
                    );
          // $datePerms = [];
          foreach ($period as $key => $value) {
              $dayP = $value->format('l');
              $datePerms[$daysEng[$dayP]] = [$permis->dateOut,$permis->dateEnter,
                                            '<b>Descripción:</b>'.$permis->description];
          }
        }
        /***fechas de los dias de la semana***/
        $dayts = [];
        $datesDays = new DatePeriod(
                       new DateTime($week_array["week_start"]),
                       new DateInterval('P1D'),
                       new DateTime($week_array["week_end"].' 23:59:59')
                  );

        foreach($datesDays AS $vals){
          $dayP = $vals->format('l');
          $dayts[$daysEng[$dayP]] = $vals->format("Y-m-d");
        }
        // dd($dayts);
        $dateDays = [];
        $data = loggsModel::whereBetween('dateDay', [$week_array["week_start"],$week_array["week_end"]])
                          ->where("userId",$id)
                          ->where('idCompany',$idComp)->get();
        $ordeDay = [];
        // dd($data);
        foreach($data AS $datPo){

          $ordeDay[$datPo->dayWeek][$datPo->idDay][$datPo->idLink] = ['dayWeek'=>$datPo->dayWeek,'dateDay'=>$datPo->dateDay,
                                         'ontime'=>$datPo->ontime,'id'=>$datPo->id,
                                         'viatics'=>$datPo->viatics,'viaticsAdded'=>$datPo->viaticsAdded,
                                         'hourEnter'=>$datPo->hourEnter,
                                         'hourExit'=>($datPo->hourExit!='00:00:00'?$datPo->hourExit:null),
                                         'hourEnterBetw'=>($datPo->hourEnterBetw!='00:00:00'?$datPo->hourEnterBetw:null),
                                         'hourExitBetw'=>($datPo->hourExitBetw!='00:00:00'?$datPo->hourExitBetw:null),
                                         'outTimeEnter'=>($datPo->outTimeEnter!='00:00:00'?$datPo->outTimeEnter:null),
                                         'hourDay'=>($datPo->hourDay!='00:00:00'?$datPo->hourDay:null),
                                         'extraHourDay'=>($datPo->extraHourDay!='00:00:00'?$datPo->extraHourDay:null),
                                         'hourNight'=>($datPo->hourNight!='00:00:00'?$datPo->hourNight:null),
                                         'extraHourNight'=>($datPo->extraHourNight!='00:00:00'?$datPo->extraHourNight:null),
                                         'extraHourEspecial'=>($datPo->extraHourEspecial!='00:00:00'?$datPo->extraHourEspecial:null),
                                         'estado'=>'asistencia','clientName'=>$datPo->getInfoClient()->first()->clientName,
                                         'aproved'=>(!empty($datPo->aproved)?$datPo->aproved:0),
                                         'shedlId'=>$datPo->idDay,'baseTasks'=>$datPo->baseTasks,
                                         'tasks'=>$datPo->tasks,'quantityTask'=>$datPo->quantityTask,'complTask'=>$datPo->complTask,
                                         'aditTask'=>$datPo->aditTask,'lessTask'=>$datPo->lessTask,'priceAdiTask'=>$datPo->priceAdiTask,];
        }
        // dd($ordeDay,$datePerms,$dateDefPerm);
        // dd($ordeDay);
        $colab = colaboradores::find($id);

        $workIng=client_colab_assocModel::where("idColab",$id)->get();
        $wokingWeek = [];
        $wokWClient = [];
        // dd($workIng);
        foreach($workIng AS $valus){
          $shed =$valus->getJobs()->first();
          $client = $valus->getClientInfo()->first()->getClient()->first();
          if(!empty($shed)){
            $horarios = $shed->getShedules()->get();
            // dd($horarios);
            foreach($horarios AS $vlone){
              if(!isset($ordeDay[$vlone->day][$vlone->id][$valus->id])
                &&!array_key_exists($vlone->day,$datePerms)
                &&!array_key_exists($vlone->day,$dateDefPerm))
              {

                $tmp = $baseDay;
                $tmp['estado']='Ausencia';
                $tmp['dayWeek']=$vlone["day"];
                $tmp['clientName']=$client->clientName;
                $tmp['idBranch']=$valus->idBranch;
                $tmp['idClient']=$client->id;
                $tmp['idLink']=$valus->id;
                $tmp['idDay']=$vlone->id;
                $tmp['idArea']=$valus->idArea;
                $tmp['entry']=$vlone->entry;
                $tmp['exit']=$vlone->exit;
                $tmp['dateDay'] = $dayts[$vlone["day"]];
                $tmp['idCompany']=$valus->getCompany()->first()->idCompany;
                $tmp['aproved']=-1;
                // $orderDays[$dasy][] = $tmp;
                $taskAssi = $shed->tasks()->get();
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"]];
                  }
                  $tmp['tasks']=json_encode($taskList);
                }
                $ordeDay[$vlone->day][$vlone->id][$valus->id] = $tmp;
              }
              else if(isset($ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks']))
              {
                $taskAssi = $shed->tasks()->get();
                // dd($taskAssi);
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  $tasksaved = (!empty($ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks'])?json_decode($ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks'],TRUE):[]);
                  foreach($tasksaved AS $valtask){
                      $taskin[$valtask["idT"]]=$valtask["quantity"];
                  }
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"],
                                   "descript"=>(isset($taskin[$vlist["id"]])?$taskin[$vlist["id"]]:[])];
                  }
                  // dd($trackTask);
                  $ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks']=json_encode($taskList);
                }

                // dd($ordeDay);
              }
              else if(array_key_exists($vlone["day"],$datePerms))
              {
                $tmp = $baseDay;
                $tmp['estado']='Permiso';
                $tmp['dayWeek']=$vlone["day"];
                $tmp['clientName']=$client->clientName;
                $tmp['idBranch']=$valus->idBranch;
                $tmp['idClient']=$client->id;
                $tmp['idLink']=$valus->id;
                $tmp['idDay']=$vlone->id;
                $tmp['idArea']=$valus->idArea;
                $tmp['entry']=$vlone->entry;
                $tmp['exit']=$vlone->exit;
                $tmp['dateDay'] = $dayts[$vlone["day"]];
                $tmp['idCompany']=$valus->getCompany()->first()->idCompany;
                $tmp['descripDay']=$datePerms[$vlone["day"]][2];
                $tmp['aproved']=-1;
                $taskAssi = $shed->tasks()->get();
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"]];
                  }
                  $tmp['tasks']=json_encode($taskList);
                }
                $ordeDay[$vlone->day][$vlone->id][$valus->id] = $tmp;
              }
              else if(array_key_exists($vlone["day"],$dateDefPerm))
              {
                $tmp = $baseDay;
                $tmp['estado']='Permiso '.$dateDefPerm[$vlone["day"]][1];
                $tmp['dayWeek']=$vlone["day"];
                $tmp['clientName']=$client->clientName;
                $tmp['idBranch']=$valus->idBranch;
                $tmp['idClient']=$client->id;
                $tmp['idLink']=$valus->id;
                $tmp['idDay']=$vlone->id;
                $tmp['idArea']=$valus->idArea;
                $tmp['entry']=$vlone->entry;
                $tmp['exit']=$vlone->exit;
                $tmp['dateDay'] = $dayts[$vlone["day"]];
                $tmp['idCompany']=$valus->getCompany()->first()->idCompany;
                $tmp['descripDay']=$dateDefPerm[$vlone["day"]][2];
                $tmp['aproved']=-1;
                $taskAssi = $shed->tasks()->get();
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"]];
                  }
                  $tmp['tasks']=json_encode($taskList);
                }
                $ordeDay[$vlone->day][$vlone->id][$valus->id] = $tmp;
              }
            }
          }
          // dd($ordeDay,$taskin,$trackTask);
        }
        // dd($ordeDay);
        $orderDays = [];
        foreach($days AS $ky=>$dasy){
          if(isset($ordeDay[$dasy])){
            // dd($tmp);
            $orde = $ordeDay[$dasy];
            ksort($orde);
            $orderDays[$dasy] = $orde;
            // dd($wokingWeek);
          }
          else{
            foreach($workIng as $valuesAssoc)
            {
              $tmp = $baseDay;
              $tmp['estado']='Sin horario';
              $tmp['dateDay'] = $dayts[$dasy];
              $tmp['dayWeek']=$dasy;
              $tmp['idClient']=$valuesAssoc->getClientInfo()->first()->id;
              $tmp['clientName']=$valuesAssoc->getClientInfo()->first()->getClient()->first()->clientName;
              $tmp['idBranch']=$valuesAssoc->idBranch;
              $tmp['idLink']=$valuesAssoc->id;
              $tmp['aproved']=-1;
              $tmp['idArea']=$valuesAssoc->idArea;
              $tmp['idCompany']=$valus->getClientInfo()->first()->idCompany;
              $orderDays[$dasy][$ky][$valuesAssoc->id] = $tmp;
            }
          }
        }
        // dd($orderDays);
        // dd($wokingWeek);


        $ANIOS = loggsModel::select(DB::raw("EXTRACT(YEAR FROM created_at) AS year"))->
        groupByRaw('year')->get();
        // $ANIOS = $ANIOS;
        // dd($ANIOS);

        // $thyeas = loggsModel::where("userId",$id)->where('idCompany',$idComp)->groupBy("ye")->get();
        // $company =
        // dd($colab);
      }
      // dd($ANIOS->anios);
      return view('admin.companies.Mlaboremploy',
            ['menubar'=> $this->list_sidebar(),
             'data' => $orderDays,
             'colabInfo'=> $colab,
             'idCompany'=>$idComp,
             'idColab'=>$id,
             'years'=> $ANIOS,
             'yr' => $years,
             'wk' => $weekNumb,
             'week' => $week_array,
             'idUser' =>$id
            ]);
      // dd($id);
    }
    function workReportSavex(Request $request,$id,$idComp){
      $model = new loggsModel;
      $colab = colaboradores::find($id);
      // dd($colab->hora_extra_diurna,$colab->hora_extra_nocturna,$colab->hora_extra_especial);
      if(!empty($request->idPer)){
        $howHours = '';
        $finded = $model::find($request->idPer);
        $dataMod = $request->only($model->getFillable());
        $outenter = $this->bettwenMin($dataMod["hourEnter"],$dataMod["hourExit"]);
        $dataMod["hourDay"] = $outenter;
        if(isset($dataMod["extraHourDay"])){
          $salida = $this->hourSeconds($dataMod["extraHourDay"]);
          $exitBefore = true;
          $dataMod["extraHourDayMiddle"] = floor($salida / 1800);
          $dataMod["extraPayDay"] = $dataMod["extraHourDayMiddle"]*$colab->hora_extra_diurna;
        }
        if(isset($dataMod["extraHourNight"])){
          $salida = $this->hourSeconds($dataMod["extraHourNight"]);
          // $exitBefore = true;
          $dataMod["extraHourNightMiddle"] = floor($salida / 1800);
          $dataMod["extraPayNight"] = $dataMod["extraHourNightMiddle"]*$colab->hora_extra_diurna;
        }
        if(isset($dataMod["extraHourEspecial"])){
          $salida = $this->hourSeconds($dataMod["extraHourEspecial"]);
          // $exitBefore = true;
          $dataMod["extraHourEspecMiddle"] = floor($salida / 1800);
          $dataMod["extraPayEspec"] = $dataMod["extraHourEspecMiddle"]*$colab->hora_extra_especial;
        }
        $finded->fill($dataMod)->save();
      }else{
        $data = $request->only($model->getFillable());
        $outenter = $this->bettwenMin($data["hourEnter"],$data["hourExit"]);
        $data["hourDay"] = $outenter;
        if(isset($data["extraHourDay"])){
          $salida = $this->hourSeconds($data["extraHourDay"]);
          $exitBefore = true;
          $data["extraHourDayMiddle"] = floor($salida / 1800);
          $data["extraPayDay"] = $data["extraHourDayMiddle"]*$colab->hora_extra_diurna;
        }
        if(isset($data["extraHourNight"])){
          $salida = $this->hourSeconds($data["extraHourNight"]);
          // $exitBefore = true;
          $data["extraHourNightMiddle"] = floor($salida / 1800);
          $data["extraPayNight"] = $data["extraHourNightMiddle"]*$colab->hora_extra_diurna;
        }
        if(isset($data["extraHourEspecial"])){
          $salida = $this->hourSeconds($data["extraHourEspecial"]);
          // $exitBefore = true;
          $data["extraHourEspecMiddle"] = floor($salida / 1800);
          $data["extraPayEspec"] = $data["extraHourEspecMiddle"]*$colab->hora_extra_especial;
        }
        $data["latitude"] = '0';
        $data["longitud"] = '0';
        $data["idFollower"] = $this->randem(10);
        $data["hourIdeal"] = $outenter;
        $data["hourDay"] = $outenter;
        $data["dateDay"] = date("Y-m-d",strtotime($request->dateDay));
        $model->fill($data)->save();
      }
      return redirect()->back()->with('success','Guardado correctamente!');
      // dd($id,$idComp,$request);
    }
    private function getStartAndEndDate($week, $year) {
      $dto = new DateTime();
      $dto->setISODate($year, $week);
      $ret['week_start'] = $dto->format('Y-m-d');
      $dto->modify('+6 days');
      $ret['week_end'] = $dto->format('Y-m-d');
      return $ret;
    }
    public function updateWorkDonex(Request $request,$id,$idComp,$nYr,$nWk){
      $listas = [];
      $ideal= $totDone = $addit = $lessdone = 0;
      // dd($request);
      $finded = loggsModel::find($request->idFollower);
      if(!empty($finded)){
        foreach($request->altu AS $ky=>$valus){
          $finl = explode("_",$ky);
          $listas[] = ["idT"=>$finl[0],"quantity"=>$valus];
          $ideal+=$finl[1];
          $totDone+=$valus;
        }
        if($totDone>=$ideal){
          $addit=($totDone-$ideal);
        }else if($totDone<=$ideal){
          $lessdone = $ideal-$totDone;
        }
        // dd($ideal,$totDone,$request,$listas,$id,$idComp,$nWk,$nYr);
        $finded->quantityTask = $totDone;
        // $finded->priceTask = $sumTOt;
        $finded->baseTasks = $ideal;
        $finded->complTask = ($totDone>= $ideal?'true':'false' );
        $finded->aditTask = $addit;
        $finded->lessTask = $lessdone;
        $finded->tasks = json_encode($listas);
        $finded->save();
      }
      return redirect()->back()->with('success','Guardado correctamente!');
    }


    public static function get_colaboradores(Request $request)
    {

        $colaboradores = company_colab_assocModel::where("idCompany", $request->id_empresa)->where("status", 1)->get();

        if (count($colaboradores)>0) {

            $html_options = '<option value="" selected>-- todos --</option>';

            foreach ($colaboradores as $colab) {
                if ($request->id_colab==$colab->idColab) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }

                $colab_info = colaboradores::find($colab->idColab);

                $html_options .= '<option value="'.$colab_info->id.'" '.$selected.'>'.$colab_info->nombres.' '.$colab_info->apellidos.'</option>';
            }

        } else {
           $html_options = '';
        }

        return $html_options;
    }


}
