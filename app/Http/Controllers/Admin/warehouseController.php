<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\warehouseModel;
use Illuminate\Support\Facades\DB;
class warehouseController extends Controller
{
  private $baseModel;
  public function __construct(){
    $this->middleware('auth:admin');
    $this->baseModel=new warehouseModel;
    $this->routeTo='admin.warehouse.index';
  }
  public function index(){
    $listof=$this->baseModel::orderBy("created_at")->get();
    return view('admin.warehouse.show',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$listof]);
  }
  public function store(Request $request) {
    $data = $request->only($this->baseModel->getFillable());
    $this->baseModel->fill($data)->save();
    return redirect()->route($this->routeTo)->with('success','Guardado correctamente!');
  }
  public function update(Request $request, $id) {
    $model = $this->baseModel::find($id);
    $data = $request->only($this->baseModel->getFillable());
    $model->fill($data)->save();
    return redirect()->route($this->routeTo)->with('info','Actualizado correctamente!');
  }
  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }
  public function products(){
    $resul = [];
    $listof=$datas = DB::table('admin_werehouse_products')
        ->join('admin_products',
        'admin_products.id', '=', 'admin_werehouse_products.id_product')
        ->join('admin_users',
        'admin_users.id', '=', 'admin_werehouse_products.whom')
       ->select('admin_products.title',
                'admin_werehouse_products.*',
                'admin_users.name')
       ->where([])
       ->get();
   $wwhare=warehouseModel::orderBy("name")->get();
   $warehouse = [];
   foreach($wwhare AS $colorsm){
     $warehouse[$colorsm["id"]]=$colorsm["name"];
   }
    foreach($listof AS $le=>$VALU){
      $resul[] = ["num"=>($le+1),
                  "id"=>$VALU->id_product,
                  "warehouse"=>$warehouse[$VALU->id_werhouse],
                  "title"=>$VALU->title,
                  "date"=>date("M d, Y",strtotime($VALU->datein)),
                  "egress"=>$VALU->egress,
                  "amount"=>$VALU->amount,
                  "total"=>($VALU->amount-$VALU->egress),
                  "whom"=>$VALU->name,
                ];
    }
    return view('admin.warehouse.filterprods',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$resul]);
  }
}
