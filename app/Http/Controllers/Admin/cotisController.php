<?php

namespace App\Http\Controllers\admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\UserAdmin;

use App\adminModels\cotisModel;
use App\adminModels\companiesModel;
use App\adminModels\companyclientsModel;
use App\adminModels\companyclientsBranchModel;

use PDF;
use App\adminModels\productosModel;
use App\adminModels\puestosModel;

class cotisController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth:admin');
  }


  public function index(Request $request) {


    $empresas = companiesModel::get();

    $clientes = companyclientsModel::get();

    if ($request->id_cliente!='') {
        $cotis = cotisModel::where('id_admin', Auth::user()->id)->where('id_cliente', $request->id_cliente)->orderBy('created_at', 'desc')->get();
    } else {
        $cotis = cotisModel::where('id_admin', Auth::user()->id)->orderBy('created_at', 'desc')->get();
    }

    return view('admin.cotis.index',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$cotis,
           'empresas'=>$empresas,
           'clientes'=>$clientes,
           'request'=>$request
          ]);
  }



   public function show(Request $request, $id)
    {

        $user = Auth::user();

        $coti = cotisModel::find($id);


        return view('admin.cotis.show', ['menubar'=> $this->list_sidebar(),
           'user'=>$user,
           'coti'=>$coti,
          ]);
    }


    public function create(Request $request)
    {

        $user = Auth::user();

        $empresas = companiesModel::get();
        $clientes = companyclientsModel::get();
        $sedes = companyclientsBranchModel::get();



        // servicios
        $servicios = puestosModel::orderBy("created_at")->get();

        // productos
        $productos = productosModel::orderBy("created_at")->get();


        return view('admin.cotis.create', ['menubar'=> $this->list_sidebar(),
           'user'=>$user,
           'clientes'=>$clientes,
           'empresas'=>$empresas,
           'sedes'=>$sedes,
           'request'=>$request,
           'servicios'=>$servicios,
           'productos'=>$productos
          ]);
    }


  public function store(Request $request) {


    $model = new cotisModel;
    $data = $request->only($model->getFillable());

    $data['no'] = $request->id_cliente."-".date("Y")."-".date("m")."-";

    $empresa = companiesModel::find($request->id_empresa);
    $cliente = companyclientsModel::find($request->id_cliente);
    $sede = companyclientsBranchModel::find($request->id_sede);

    $data['empresa'] = $empresa->company;
    $data['cliente'] = $cliente->clientName;
    $data['sede'] = $sede->namebranch;
    $data['nit'] = $cliente->nit;

    // $date = strtotime($request->fecha);
    // $data['fecha'] = date('m-d-Y', $date);

    
    $productos_array = array();
    $servicios_array = array();
    $indice = 0;

    if ($request->producto) {
        foreach($request->producto as $producto) {

            $productos_array[] = array(
                                  'id_prod'=> $request->id_prod[$indice],
                                  'no_prod'=> $request->no_prod[$indice],
                                  'producto'=> $producto,
                                  'desc_prod'=> $request->desc_prod[$indice],
                                  'total_prod'=> number_format($request->total_prod[$indice], 2),
                                );

            $indice++;
        }
    }

    $indice = 0;

    if ($request->servicio) {
        foreach($request->servicio as $servicio) {

            $servicios_array[] = array(
                                  'id_serv'=> $request->id_serv[$indice],
                                  'no_serv'=> $request->no_serv[$indice],
                                  'servicio'=> $servicio,
                                  'desc_serv'=> $request->desc_serv[$indice],
                                  'total_serv'=> number_format($request->total_serv[$indice], 2),
                                );

            $indice++;
        }
    }

    $detalle_array = array('productos'=>$productos_array, 'servicios'=>$servicios_array);

    $data['detalle'] = json_encode($detalle_array);

    $data['total'] = number_format($request->total, 2);

    $model->fill($data)->save();

    $model->no = $model->no.$model->id;
    $model->save();

    $url_redirect = 'cotis/'.$model->id;

    return redirect($url_redirect)->with('success','Cotización Creada!');
  }



  public function destroy($id) {

    cotisModel::destroy($id);

    return redirect('cotis')->with('success','Cotización Borrada!');
  }



  public function update(Request $request, $id) {

    $grupo = cotisModel::find($id);

    $id_user =  Auth::user()->id;

    $data = $request->only($grupo->getFillable());

    $data['ids_miembros'] = implode(",", $request->ids_miembros);

    $grupo->fill($data)->save();


    return redirect('cotis')->with('success','Grupo Actualizado!');


  }


  public function ver_pdf($id) {

      $coti = cotisModel::find($id);

      return view('admin.cotis.pdf', compact('coti'));
  }



    public function export_pdf($id) {

      $coti = cotisModel::find($id);

      $nombre_pdf = $coti->no.".pdf";

      
      $data['empresa'] = $coti->empresa;
      $data['no'] = $coti->no;
      $data['fecha'] = $coti->fecha;
      $data['cliente'] = $coti->cliente;
      $data['sede'] = $coti->sede;
      $data['nit'] = $coti->nit;
      $data['detalle'] = $coti->detalle;
      $data['total'] = $coti->total;
      $data['firma'] = $coti->firma;

      // dd($coti->detalle);

      $pdf = PDF::loadView('admin.cotis.pdf', $data);


      // $pdf = PDF::loadHTML($data)->setPaper('legal');

      // $pdf->save('public/pdfs/'.$nombre_pdf);
      return $pdf->download($nombre_pdf);

    }



    public function export_pdf2($id) {

      $coti = cotisModel::find($id);

      $nombre_pdf = $coti->no.".pdf";

      $html = view('admin.cotis.pdf_old', compact('coti'))->render();
      $conv = new \Anam\PhantomMagick\Converter();
      $options = [
        'zoomfactor' => 1,
        'orientation' => 'portrait',
        'margin' => '2cm',
        'width' => '850',
        'height' => '1150',
      ];
      $conv->setPdfOptions($options);
      $conv->addPage($html)->download($nombre_pdf);
      
      return redirect('cotis')->with('success','Cotizacion Exportada a PDF!');

    }


}
