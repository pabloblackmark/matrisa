<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\documentos;
use App\adminModels\colaboradores;
use App\adminModels\company_colab_assocModel;
use App\adminModels\companyclientsModel;
use App\adminModels\companiesModel;
use App\adminModels\puestosModel;

use Barryvdh\DomPDF\Facade as PDF;
use \Storage;

class documentosController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'documentos';
    }

    public function index(){
        $data = documentos::get();

        return view('admin.documentos.index',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data
              ]);
    }

    public function show($id=null){

        if ($id != null){
            $data = documentos::find($id);
            $edit = true;
        }

        return view('admin.documentos.show',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'edit' => $edit
              ]);
    }

    public function create(){
        $edit = false;
        $data = [];

        return view('admin.documentos.show',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'edit' => $edit
              ]);
    }

    public function store(Request $request) {
        $model = new documentos;
        $data = $request->only($model->getFillable());
        try{

            $model->fill($data)->save();

            return redirect($this->back)->with('success','Guardado correctamente!');
        }catch(\Exception $e){
            return redirect()->back()->with('warning','No se pudieron guardar los datos: ' . $e);
        }

    }

    public function update(Request $request, $id) {
        $model = new documentos;
        $finded = $model::find($id);
        $dataMod = $request->only($model->getFillable());

        if ($request->file('foto')){
            $foto = $request->file('foto')->storePublicly('fotos', 'public');
            $dataMod['foto'] = $foto;
        }

        $finded->fill($dataMod)->save();
        return redirect($this->back)->with('info','Actualizado correctamente!');
    }

    public function destroy($id) {
        try {
            documentos::destroy($id);
            return redirect()->back()->with('warning','Borrado correctamente!');
          }catch (\Exception $e) {
           return redirect()->back()->with('error','No se puede eliminar porque se está utilizando por un usuario del módulo -Personal-. Primero debe eliminar esta configuración.'.$e->getCode());
        }
    }

    public function download(){
        $uid = isset($_GET['uid']) ? $_GET['uid'] : '';
        $did = isset($_GET['did']) ? $_GET['did'] : '';

        $docdata = documentos::find($did);
        $userdata = colaboradores::find($uid);

        $assocdata = company_colab_assocModel::where("idColab", $uid)->first();
        $idCompany = $assocdata->idCompany;
        $company = companiesModel::find($idCompany);

        if ($docdata){
            setlocale(LC_ALL,"es_ES");
            $doc = $docdata['body'];

            // $cad = preg_replace('/\$nombres\$/', $nombres, $doc);
            // $cad = preg_replace('/\$apellidos\$/', $apellidos, $cad);
            // $cad = preg_replace('/\$dpi\$/', $dpi, $cad);
            $dpi = '??';

            $nombres_empleado = $this->val($userdata, 'nombres');
            $apellidos_empleado = $this->val($userdata, 'apellidos');
            $fecha_nacimiento_empleado = $this->val($userdata, 'fecha', 'fecha');
            $edad_empleado = $this->calcularEdad($fecha_nacimiento_empleado);
            $genero_empleado = $this->val($userdata, 'genero');
            $estado_civil_empleado = $this->val($userdata, 'estado_civil');
            $dpi_empleado = $this->val($userdata, 'dpi');
            $nacionalidad_empleado = $this->val($userdata, 'nacionalidad');
            $direccion_empleado = $this->val($userdata, 'direccion');


            $fechai = " - - - ";
            $dac = " - - - ";
            $mac = " - - - ";
            $yac = " - - - ";
            if ($this->val($userdata, 'fecha_ingreso', 'fecha') != ""){
                $fechai = new \DateTime($this->val($userdata, 'fecha_ingreso', 'fecha'));

                $dac = $fechai->format('d');
                $mac = $fechai->format('F');
                $yac = $fechai->format('Y');
            }

            $pues = $this->val($userdata, 'puesto');

            $puesto_data = puestosModel::where('nombre', $pues)->first();

            $desc = $this->val($puesto_data, 'descripcion');

            $tipo = $this->val($userdata, 'tipo_pago');
            $suel = $this->val($userdata, 'sueldo', 'numero') * 30;
            $bono = $this->val($userdata, 'bono_extra', 'numero') * 30;

            $nomr = $this->val($company, 'legalrepresent');

            $fecha_nacimiento = $this->val($company, 'fecha_nacimiento', 'fecha');

            $edar = $this->calcularEdad($fecha_nacimiento);

            $genr = $this->val($company, 'genero');
            $estr = $this->val($company, 'estado_civil');
            $nacr = $this->val($company, 'nacionalidad');
            $munr = $this->val($company, 'municip');
            $dpir = $this->val($company, 'dpi');
            $depr = $this->val($company, 'depto');

            $empresa = $this->val($company, 'company');
            $nit = $this->val($company, 'nit');
            $direccion = $this->val($company, 'addess');


            //Reemplazo de datos
            $cad = preg_replace('/\$nombres_representante\$/', $nomr, $doc);
            $cad = preg_replace('/\$edad_representante\$/', $edar, $cad);
            $cad = preg_replace('/\$genero_representante\$/', $genr, $cad);
            $cad = preg_replace('/\$estado_civil_representante\$/', $estr, $cad);
            $cad = preg_replace('/\$nacionalidad_representante\$/', $nacr, $cad);
            $cad = preg_replace('/\$municipio_representante\$/', $munr, $cad);
            $cad = preg_replace('/\$dpi_representante\$/', $dpir, $cad);
            $cad = preg_replace('/\$departamento_representante\$/', $depr, $cad);


            $cad = preg_replace('/\$empresa\$/', $empresa, $cad);
            $cad = preg_replace('/\$nit_empresa\$/', $nit, $cad);
            $cad = preg_replace('/\$direccion_empresa\$/', $direccion, $cad);


            $cad = preg_replace('/\$nombres_empleado\$/', $nombres_empleado, $cad);
            $cad = preg_replace('/\$apellidos_empleado\$/', $apellidos_empleado, $cad);
            $cad = preg_replace('/\$edad_empleado\$/', $edad_empleado, $cad);
            $cad = preg_replace('/\$genero_empleado\$/', $genero_empleado, $cad);
            $cad = preg_replace('/\$estado_civil_empleado\$/', $estado_civil_empleado, $cad);
            $cad = preg_replace('/\$dpi_empleado\$/', $dpi_empleado, $cad);
            $cad = preg_replace('/\$nacionalidad_empleado\$/', $nacionalidad_empleado, $cad);
            $cad = preg_replace('/\$direccion_empleado\$/', $direccion_empleado, $cad);


            $cad = preg_replace('/\$dia_alta_colaborador\$/', $dac, $cad);
            $cad = preg_replace('/\$mes_alta_colaborador\$/', $mac, $cad);
            $cad = preg_replace('/\$year_alta_colaborador\$/', $yac, $cad);
            $cad = preg_replace('/\$puesto_de_trabajo\$/', $pues, $cad);
            $cad = preg_replace('/\$descripcion_puesto_de_trabajo\$/', $desc, $cad);
            $cad = preg_replace('/\$tipo_contrato\$/', $tipo, $cad);
            $cad = preg_replace('/\$sueldo_mensual\$/', $suel, $cad);
            $cad = preg_replace('/\$bono_ley\$/', $bono, $cad);

            $date = new \DateTime('now');
            $cad = preg_replace('/\$hoy\$/', $this->formatearFecha($date->format('l d')), $cad);
            $cad = preg_replace('/\$mes_actual\$/', $this->formatearFecha($date->format('F')), $cad);
            $cad = preg_replace('/\$year_actual\$/', $this->formatearFecha($date->format('Y')), $cad);

            //////PDF constructor-------------------------------------------------
            $pdf = PDF::loadHTML($cad)->setPaper('legal');

            // $filename = 'Carta_' . time() . '.pdf';
            // $contents = $pdf->stream();
            // Storage::disk('public')->put($filename, $contents);
            // Storage::put('Denuncia_temp.pdf', $contents);
            // $path = Storage::disk('public')->path($filename);
            // $file = Storage::get($filename);
            // $file = fopen($path, 'r');
            // $file = file_get_contents($path);

            return $pdf->stream();

        }else{
            return redirect()->back()->with('error','No se puede eliminar porque se está utilizando por un usuario del módulo -Personal-. Primero debe eliminar esta configuración.'.$e->getCode());
        }

    }

    function val($var, $campo, $tipo="normal"){
        if ($tipo == "fecha"){
            $res = isset($var[$campo]) ? $var[$campo] : "";
        }elseif ($tipo == "numero"){
            $res = isset($var[$campo]) ? $var[$campo] : 0;
        }
        else{
            $res = isset($var[$campo]) ? $var[$campo] : " - - - ";
        }

        return $res;
    }

    function formatearFecha($fecha){
        $res = preg_replace("/Sunday/", "Domingo", $fecha);
        $res = preg_replace("/Monday/", "Lunes", $res);
        $res = preg_replace("/Tuesday/", "Martes", $res);
        $res = preg_replace("/Wednesday/", "Miércoles", $res);
        $res = preg_replace("/Thursday/", "Jueves", $res);
        $res = preg_replace("/Friday/", "Viernes", $res);
        $res = preg_replace("/Saturday/", "Sábado", $res);

        $res = preg_replace("/January/", "Enero", $res);
        $res = preg_replace("/February/", "Febrero", $res);
        $res = preg_replace("/March/", "Marzo", $res);
        $res = preg_replace("/April/", "Abril", $res);
        $res = preg_replace("/May/", "Mayo", $res);
        $res = preg_replace("/June/", "Junio", $res);
        $res = preg_replace("/July/", "Julio", $res);
        $res = preg_replace("/August/", "Agosto", $res);
        $res = preg_replace("/September/", "Septiembre", $res);
        $res = preg_replace("/October/", "Octubre", $res);
        $res = preg_replace("/November/", "Noviembre", $res);
        $res = preg_replace("/December/", "Diciembre", $res);

        return $res;
    }

    function calcularEdad($fecha){
        $edad = "";
        if ($fecha != ""){
            $nacim = new \DateTime($fecha);

            $today = new \DateTime('now');

            $edad = $today->format('Y') - $nacim->format('Y');

            if ($nacim->format('md') > $today->format('md')){
                $edad -= 1;
            }
        }
        return $edad;
    }

}
