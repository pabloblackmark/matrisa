<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\productosModel;
use App\adminModels\stockIngresosModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\stockEgresosModel;
use App\adminModels\unidadesModel;

class egresosClientesController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company_id, $bodega_id)
    {
        $bodega = bodegasModel::find($bodega_id);
        $company = companiesModel::find($company_id);
        $cliente = $this->info_bodega($bodega_id)['cliente'];
        $data = stockEgresosModel::where('bodega_id', $bodega_id)->get();
        foreach($data as $temp){
            $prod = productosModel::find($temp->producto_id);
            $temp->producto = $prod;

            $medi = unidadesModel::find($temp->medida_id);
            $temp->medidas_element = $medi;

            // Se preparan los datos de a quien se cargo y se cobro el producto
            if ($temp->tipo_egreso == 'cliente'){
                $cli = companyclientsModel::find($temp->receptor_cliente_id);
                $temp->egreso_element = $cli->clientName;
            }elseif($temp->tipo_egreso == 'colaborador'){
                $col = colaboradores::find($temp->receptor_colaborador_id);
                $temp->egreso_element = $col->apellidos . ', ' . $col->nombres;
            }else{
                $temp->egreso_element = 'Sin cargo';
            }

            if ($temp->tipo_cobro == 'cliente'){
                $cli = companyclientsModel::find($temp->cobrar_cliente_id);
                $temp->cobro_element = isset($cli->clientName) ? $cli->clientName : '';
            }elseif($temp->tipo_cobro == 'colaborador'){
                $col = colaboradores::find($temp->cobrar_colaborador_id);
                $temp->cobro_element = $col->apellidos . ', ' . $col->nombres;
            }else{
                $temp->cobro_element = 'Sin cobro';
            }

        }

        return view('admin.bodegas.egresosClientes',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'company' => $company,
                'cliente' => $cliente,
                'company_id' => $company_id
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
