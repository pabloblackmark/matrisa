<?php

namespace App\Http\Controllers\admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\UserAdmin;
use App\adminModels\roles;
use App\adminModels\mensajesModel;
use App\adminModels\colaboradores;

use Illuminate\Support\Facades\Hash;

class mensajes_userController extends Controller
{
  protected $redirecUlr;
  public function __construct()
  {
      $this->middleware('auth:admin');

      $this->id_user = 1;
  }


  public function index() {

    $id_user = $this->id_user;


    $mensajes = mensajesModel::where('de_user', $id_user)->orWhere('para_user', $id_user)->orWhere('para_user', 0)->orderBy('updated_at', 'DESC')->get();

    // $mensajes = mensajesModel::where('para', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

    $mensajes_data = array();

    foreach($mensajes AS $mensaje) {

        $mensaje_data['id'] = $mensaje->id;
        $mensaje_data['de'] = $mensaje->de;
        $mensaje_data['para'] = $mensaje->para;
        $mensaje_data['titulo'] = $mensaje->titulo; 
        $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
        $mensaje_data['tipo'] = $mensaje->tipo;
        $mensaje_data['status'] = $mensaje->status;

        $mensajes_data[] = $mensaje_data;

    }
    

    return view('admin.mensajes_user.index',
          ['menubar'=> $this->list_sidebar(),
           'mensajes'=>$mensajes_data
          ]);
  }


    public function show(Request $request, $id)
    {

        $mensaje = mensajesModel::find($id);

        $mensaje_data['id'] = $mensaje->id;
        $mensaje_data['de'] = $mensaje->de;
        $mensaje_data['para'] = $mensaje->para;
        $mensaje_data['titulo'] = $mensaje->titulo; 
        $mensaje_data['mensaje'] = $mensaje->mensaje;
        $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->created_at));
        $mensaje_data['tipo'] = $mensaje->tipo;
        $mensaje_data['status'] = $mensaje->status;

        $mensaje_data['respuestas'] = json_decode($mensaje->respuestas, true);


        if (($mensaje->tipo==0)&&($mensaje->status == 0)) {
            $mensaje->status = 1;
        }
        $mensaje->save();


        return view('admin.mensajes_user.show', ['menubar'=> $this->list_sidebar(),
           'mensaje'=>$mensaje_data
          ]);
    }


/*
  public function enviados() {

    // $mensajes = mensajesModel::where('de', Auth::user()->id)->orWhere('para', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

    $mensajes = mensajesModel::where('de', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();

    $mensajes_data = array();

    foreach($mensajes AS $mensaje) {

        if ($mensaje->tipo==0) {
            $mensaje_data['de'] = UserAdmin::get_name($mensaje->de);
            $mensaje_data['para'] = colaboradores::get_name($mensaje->para);
        } else {
            $mensaje_data['de'] = colaboradores::get_name($mensaje->de);
            $mensaje_data['para'] = UserAdmin::get_name($mensaje->para);
        }
        $mensaje_data['titulo'] = $mensaje->titulo; 
        $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->created_at));

        $mensajes_data[] = $mensaje_data;

    }
    

    return view('admin.mensajes.enviados',
          ['menubar'=> $this->list_sidebar(),
           'mensajes'=>$mensajes_data
          ]);
  }
*/

    public function create(Request $request)
    {

        $user = Auth::user();

        $user->id = $this->id_user;

        $admins = userAdmin::get();

        return view('admin.mensajes_user.create', ['menubar'=> $this->list_sidebar(),
           'user'=>$user,
           'admins'=>$admins,
          ]);
    }


  public function store(Request $request) {
    /*
    $validator = $request->validate([
        'name' => 'required',
        'password' => 'required',
        'usersys' => 'required',
    ]);
    */    

    $model = new mensajesModel;
    $data = $request->only($model->getFillable());

    $data['de'] = colaboradores::get_name($request->de_user);
    $data['para'] = UserAdmin::get_name($request->para_admin);

    $model->fill($data)->save();

    return redirect('mensajes_user')->with('success','Mensaje Enviado!');
  }



  public function destroy($id) {
    UserAdmin::destroy($id);
    return redirect()->back()->with('warning','Borrado correctamente!');
  }



  public function update(Request $request, $id) {


    $mensaje = mensajesModel::find($id);
    $respuestas = json_decode($mensaje->respuestas, true);

    $id_user = $this->id_user;

    $respuesta["de"] = colaboradores::get_name($id_user);
    $respuesta["fecha"] = date('d-m-Y h:i');
    $respuesta["respuesta"] = $request->respuesta;
    $respuesta["tipo"] = 1;

    $respuestas[] = $respuesta;

    $mensaje->respuestas = json_encode($respuestas);

    if ($mensaje->tipo == 1) {
        $mensaje->status = 0; // enviado
    } else {
        $mensaje->status = 2; // respondido
    }

    $mensaje->save();

    return redirect('mensajes_user')->with('success','Mensaje Respondido!');


  }
}
