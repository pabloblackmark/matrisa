<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\productosModel;
use App\adminModels\equiposModel;
use App\adminModels\stockIngresosModel;
use App\adminModels\stockIngresosEquipoModel;
use App\adminModels\unidadesModel;
use App\adminModels\conversionesModel;
use App\adminModels\categoriesModel;
use App\adminModels\subCategoriesModel;
use App\adminModels\providers;

use App\Http\Controllers\Admin\codigosController;

class facturasController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $elem = new facturasModel;
        $data = $request->only($elem->getFillable());
        $data['bodega_id'] = $id;
        $elem->fill($data);
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = facturasModel::where('bodega_id', $id)->where('tipo', 'producto')->get();
        $bodega = bodegasModel::find($id);

        $providers = providers::get();
        $providers_arr = [['', '- - -']];
        $providers_obj = [];
        foreach($providers as $temp){
            $providers_arr[] = [$temp->id, $temp->nombre];
            $providers_obj[$temp->id] = $temp;
        }

        return view('admin.facturas.index',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'providers_arr' => $providers_arr,
                'providers_obj' => $providers_obj
            ]);
    }

    public function showFacturasEquipo($id)
    {
        $data = facturasModel::where('bodega_id', $id)->where('tipo', 'equipo')->get();
        $bodega = bodegasModel::find($id);

        $providers = providers::get();
        $providers_arr = [['', '- - -']];
        $providers_obj = [];
        foreach($providers as $temp){
            $providers_arr[] = [$temp->id, $temp->nombre];
            $providers_obj[$temp->id] = $temp;
        }

        // revisar si la factura esta cuadrada o hay que corregir datos
        foreach($data as $temp){
            $ingresos = stockIngresosModel::where('factura_id', $temp->id)->get();
            $temp->ingresos = $ingresos;
            if ($temp->estado == null){
                if (count($temp->ingresos) > 0){
                    $temp->estado_badge = 'warning';
                }else{
                    $temp->estado_badge = 'dark';
                    $temp->estado_texto = 'Pendiente ingresar productos';
                }
            }else{
                $temp->estado_badge = 'success';
                $temp->estado_texto = 'Factura cuadrada';
            }
        }

        return view('admin.facturas.indexEquipos',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'providers_arr' => $providers_arr,
                'providers_obj' => $providers_obj
            ]);
    }

    public function showProductos($bodega_id, $factura_id)
    {
        $factura = facturasModel::find($factura_id);
        $bodega = bodegasModel::find($bodega_id);
        $data = stockIngresosModel::where('factura_id', $factura_id)->get();

        $factura->proveedor = providers::find($factura->proveedor_id)->nombre;

        $productos = productosModel::get();
        $productos_arr = [['', '- - -']];
        $productos_obj = [];
        foreach($productos as $temp){
            $productos_arr[] = [$temp->id, $temp->nombre . ' ' . $temp->codigo];
            $productos_obj[$temp->id] = $temp;
        }

        $suma = 0;
        foreach($data as $temp){
            $suma += $temp->valor_total;
        }

        $unidades = unidadesModel::get();
        $unidades_arr = [['', '- - -']];
        $unidades_obj = [];
        foreach($unidades as $temp){
            $unidades_arr[] = [$temp->id, $temp->nombre];
            $unidades_obj[$temp->id] = $temp;
        }

        $conversiones = conversionesModel::get();
        $conversiones_arr = [['', '- - -']];
        foreach($conversiones as $temp){
            $conversiones_arr[] = [$temp->id, $temp->nombre];
        }


        return view('admin.facturas.productos',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'factura' => $factura,
                'productos' => $productos,
                'productos_arr' => $productos_arr,
                'productos_obj' => $productos_obj,
                'unidades' => $unidades,
                'unidades_arr' => $unidades_arr,
                'unidades_obj' => $unidades_obj,
                'conversiones' => $conversiones,
                'suma' => $suma,

            ]);
    }

    public function storeProductos(Request $request, $bodega_id, $factura_id)
    {

        $elem = new stockIngresosModel;
        $data = $request->only($elem->getFillable());
        $data['bodega_id'] = $bodega_id;
        $data['factura_id'] = $factura_id;

        $elem->fill($data)->save();
        $this->cuadrarFactura($factura_id);

        $this->calcularStock($bodega_id);
        $this->revisar_precio_producto($data['producto_id']);
        $this->revisar_stock_producto($elem->producto_id);

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProductos(Request $request, $bodega_id, $factura_id, $ingreso_id)
    {
        $elem = new stockIngresosModel;
        $elem = $elem->find($ingreso_id);
        $elem->fill($request->only($elem->getFillable()));
        $elem->save();
        $this->cuadrarFactura($factura_id);

        $this->calcularStock($bodega_id);
        $this->revisar_precio_producto($elem->producto_id);
        $this->revisar_stock_producto($elem->producto_id);

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bodega_id, $factura_id, $ingreso_id)
    {
        stockIngresosModel::destroy($ingreso_id);
        $this->cuadrarFactura($factura_id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }

    ////////---------------------------------equipos
    public function showEquipos($bodega_id, $factura_id)
    {
        //--------- catalogos ----------------
        $equipos = equiposModel::get();
        $equipos_arr = [[null, '- - -']];
        $equipos_obj = [];
        foreach($equipos as $temp){
            $equipos_arr[] = [$temp->id, $temp->nombre . ' ' . $temp->codigo];
            $equipos_obj[$temp->id] = $temp;
        }

        $unidades = unidadesModel::get();
        $unidades_arr = [['', '- - -']];
        $unidades_obj = [];
        foreach($unidades as $temp){
            $unidades_arr[] = [$temp->id, $temp->nombre];
            $unidades_obj[$temp->id] = $temp;
        }

        $conversiones = conversionesModel::get();
        $conversiones_arr = [[null, '- - -']];
        foreach($conversiones as $temp){
            $conversiones_arr[] = [$temp->id, $temp->nombre];
        }

        $categorias = [];
        $categorias_query = categoriesModel::all();
        $categorias_arr = [['', '- - -']];
        $subcategorias_arr = [['', '- - -']];
        foreach($categorias_query AS $temp){
            $subcategorias =  $temp->subcats()->get();
            $subtemp = [];
            $categorias_arr[] = [$temp->id, $temp->category];
            foreach($subcategorias AS $temp2){
                $subtemp[$temp2['id']] = $temp2['subCategory'];
                $subcategorias_arr[] = [$temp2->id, $temp2->subCategory];
            }
            $categorias[$temp["id"]] = [
                'nombre' => $temp["category"],
                'subcategoria' => $subtemp
            ];
        }

        //---- datos de la factura -------------------------------------
        $factura = facturasModel::find($factura_id);
        $bodega = bodegasModel::find($bodega_id);
        $data = stockIngresosEquipoModel::where('factura_id', $factura_id)->get();

        foreach($data as $temp){
            $equi = equiposModel::find($temp->equipo_id);
            $medi = unidadesModel::find($equi->medida_id);
            $temp->equipo = $equi;
            $temp->medida = $medi;
            $cate = categoriesModel::find($equi->categoria_id);
            $subcate = subCategoriesModel::find($equi->sub_categoria_id);
            $temp->categoria = $cate;
            $temp->sub_categoria = $subcate;
        }

        //------ calculo del total de la factura -----------
        $suma = 0;
        foreach($data as $temp){
            $suma += $temp->costo_total;
        }


        return view('admin.facturas.equipos',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'factura' => $factura,
                'equipos' => $equipos,
                'equipos_arr' => $equipos_arr,
                'equipos_obj' => $equipos_obj,
                'unidades' => $unidades,
                'unidades_arr' => $unidades_arr,
                'unidades_obj' => $unidades_obj,
                'conversiones' => $conversiones,
                'suma' => $suma,
                'categorias' => $categorias,
                'subcategorias' => $subcategorias,
                'categorias_arr' => $categorias_arr,
                'subcategorias_arr' => $subcategorias_arr

            ]);
    }

    public function storeEquipos(Request $request, $bodega_id, $factura_id)
    {
        // dd($request, $bodega_id, $factura_id);

        $equipo = new equiposModel;
        $data_equipo = $request->only($equipo->getFillable());
        $data_equipo['estado'] = 'activo';
        $equipo->fill($data_equipo)->save();

        $elem = new stockIngresosEquipoModel;
        $data = $request->only($elem->getFillable());
        $data['equipo_id'] = $equipo->id;
        $data['bodega_id'] = $bodega_id;
        $data['factura_id'] = $factura_id;

        $elem->fill($data)->save();
        $this->cuadrarFactura($factura_id, 'equipos');

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateEquipos(Request $request, $bodega_id, $factura_id, $ingreso_id)
    {



        $ingreso = new stockIngresosEquipoModel;
        $ingreso = $ingreso->find($ingreso_id);
        $ingreso->fill($request->only($ingreso->getFillable()));
        $ingreso->save();
        $this->cuadrarFactura($factura_id, 'equipos');

        $equipo = new equiposModel;
        $equipo = $equipo->find($ingreso->equipo_id);
        $equipo->fill($request->only($equipo->getFillable()));
        $equipo->save();

        return redirect()->back()->with('info', 'Actualizado correctamente');
    }





    public function cuadrarFactura($factura_id, $tipo='productos'){
        $factura = facturasModel::find($factura_id);

        if ($tipo == 'productos'){
            $ingresos = stockIngresosModel::where('factura_id', $factura_id)->get();
        }else{
            $ingresos = stockIngresosEquipoModel::where('factura_id', $factura_id)->get();
        }
        $suma = 0;
        foreach($ingresos as $temp){
            if ($tipo == 'productos'){
                $suma += $temp->valor_total;
            }else{
                $suma += $temp->costo_total;
            }
        }
        if ($suma == $factura->total){
            $factura->estado = 'cuadrada';
            $factura->save();

            // crea contraseña de pago
            codigosController::crear_codigo($factura_id);

        }else{
            $factura->estado = null;
            $factura->save();
        }
        return $factura->estado;
    }
}
