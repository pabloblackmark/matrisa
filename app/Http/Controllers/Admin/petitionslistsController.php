<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\petitionsModel;

class petitionslistsController extends Controller
{
  private $baseModel;
  public function __construct()
  {
    $this->middleware('auth:admin');
    $this->baseModel=new petitionsModel;
    $this->routeTo='admin.listpetitions.index';
  }
  public function index(){
    $listof=$this->baseModel::orderBy("created_at")->get();
    // $colorc = [];
    // foreach($listof AS $colorsm){
    //   $colorc[$colorsm["category"]][$colorsm["id"]]=$colorsm["namecolor"];
    // }
    return view('admin.petitions.show',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$listof]);
  }

  public function store(Request $request) {
    $data = $request->only($this->baseModel->getFillable());
    $this->baseModel->fill($data)->save();
    return redirect()->route($this->routeTo)->with('success','Guardado correctamente!');
  }
  public function update(Request $request, $id) {
    $model = $this->baseModel::find($id);
    $data = $request->only($this->baseModel->getFillable());
    $model->fill($data)->save();
    return redirect()->route($this->routeTo)->with('info','Actualizado correctamente!');
  }
  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }
}
