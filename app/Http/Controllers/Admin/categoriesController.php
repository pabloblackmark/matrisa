<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\categoriesModel;
use App\adminModels\subCategoriesModel;

class categoriesController extends Controller
{
  private $baseModel;
  public function __construct()
  {
    $this->middleware('auth:admin');
    $this->baseModel=new categoriesModel;
    $this->baseModel2=new subCategoriesModel;
    $this->routeTo='admin.categories.index';
  }
  public function index(){
    // $listof=$this->baseModel::orderBy("created_at")->get();
    $all = $this->baseModel->all();
    $res = [];
    foreach($all AS $temp){
        $subcats =  $temp->subcats()->get();
        $subtemp = [];
        foreach($subcats AS $temp2){
            $subtemp[$temp2['id']] = $temp2['subCategory'];
        }
        $res[$temp["id"]] = [
            'name' => $temp["category"],
            'subcategories' => $subtemp
        ];
    }
    return view(
                'admin.categories.show',
                [
                    'menubar'=> $this->list_sidebar(),
                    'data'=>$res
                ]
              );
  }

  public function store(Request $request) {
    $data = $request->only($this->baseModel->getFillable());
    $this->baseModel->fill($data)->save();
    return redirect()->route($this->routeTo)->with('success','Guardado correctamente!');
  }
  public function update(Request $request, $id) {
    $model = $this->baseModel::find($id);
    $data = $request->only($this->baseModel->getFillable());
    $model->fill($data)->save();
    return redirect()->route($this->routeTo)->with('info','Actualizado correctamente!');
  }
  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }

  public function storeSubCategory(Request $request, $id){
    $data = $request->only($this->baseModel2->getFillable());
    $data['category_id'] = $id;
    $this->baseModel2->fill($data)->save();
    return redirect()->route($this->routeTo)->with('success','Guardado correctamente!');
  }

  public function updateSubCategory(Request $request, $id){
    $elem = $this->baseModel2->find($id);
    $data = $request->only($this->baseModel2->getFillable());
    $elem->fill($data)->save();
    return redirect()->route($this->routeTo)->with('success','Guardado correctamente!');
  }

  public function deleteSubCategory(Request $request, $id){
    try {
        $this->baseModel2->destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }
}
