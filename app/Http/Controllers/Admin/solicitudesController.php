<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\providers;
class solicitudesController extends Controller
{
  protected $redirecUlr;
  public function __construct()
  {
      $this->middleware('auth:admin');
  }
  public function index(){
    $data = providers::get();

    return view('admin.providers.show',
          ['menubar'=> $this->list_sidebar(),
           'data' => $data
          ]);
  }

  public function store(Request $request) {
    $model = new providers;
    $data = $request->only($model->getFillable());

    $model->fill($data)->save();
    $model->nameroles()->sync($request->acceds);
    return redirect()->back()->with('success','Guardado correctamente!');
  }
  public function destroy($id) {
    try {
        providers::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente!');
      }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque se está utilizando por un usuario del módulo -Personal-. Primero debe eliminar esta configuración.'.$e->getCode());
    }
  }
  public function update(Request $request, $id) {
    $model = new roles;
    $finded = $model::find($id);
    $dataMod = $request->only($model->getFillable());
        $finded->fill($dataMod)->save();
    $finded->nameroles()->sync($request->acceds);
    return redirect()->back()->with('info','Actualizado correctamente!');
  }
}
