<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\productosModel;
use App\adminModels\stockIngresosModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\companyclientsBranchModel;
use App\adminModels\stockEgresosModel;
use App\adminModels\cobroColaboradorModel;
use App\adminModels\stockModel;
use App\adminModels\unidadesModel;
use App\apiModels\descuentosModel;
use App\adminModels\client_colab_assocModel;

class stockClientesController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
        $this->back = 'bancos';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $elem = new facturasModel;
        $data = $request->only($elem->getFillable());
        $data['bodega_id'] = $id;
        $elem->fill($data);
        $elem->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company_id, $bodega_id)
    {
        $bodega = bodegasModel::find($bodega_id);
        $company = companiesModel::find($company_id);
        $cliente = $this->info_bodega($bodega_id)['cliente'];
        $sede = $this->info_bodega($bodega_id)['sede'];

        $productos_query = productosModel::get();

        $productos = [];
        $productos_arr = [];
        $productos_obj = [];
        foreach($productos_query as $temp){
            $productos[$temp->id] = $temp->nombre;
            $productos_arr[] = [$temp->id, $temp->nombre];
            $productos_obj[$temp->id] = $temp;
        }
        //status = 1 asignado y activo
        $colaboradores_query = client_colab_assocModel::where('idClient', $cliente->id)->where('status', 1)->get();

        $colaboradores = [[null, '- - -']];
        foreach($colaboradores_query as $temp){
            $temp2 = $temp->getColabsInfo()->first();
            $colaboradores[] = [$temp2['id'], $temp2['apellidos'] . ', ' . $temp2['nombres']];
        }

        // $colaboradores_query = colaboradores::get();
        // $colaboradores = [[null, '- - -']];
        // foreach($colaboradores_query as $temp){
        //     $colaboradores[] = [$temp->id, $temp->apellidos . ', ' . $temp->nombres];
        // }


        $clientes[] = [$cliente->id, $cliente->clientName];
        $sedes = companyclientsBranchModel::where('idClient', $temp->id)->get();
        $clientesSedes = [[null, '- - -']];
        foreach($sedes as $temp2){
            $clientesSedes[] = [$temp2->bodega_id, $temp->clientName . ' - Sede ' . $temp2->namebranch];
        }

        $this->calcularStock($bodega_id);
        $data = stockModel::where('bodega_id', $bodega_id)->get();
        $suma = 0;
        foreach($data as $temp){
            $suma += $temp->valor_total;
            $prod = productosModel::find($temp->producto_id);
            $temp->producto_element = $prod;

            $temp->precio_cliente = $prod->precio_cliente;

            $medi = unidadesModel::find($temp->medida_id);
            $temp->medida_element = $medi;

        }

        return view('admin.stock.clienteIndex',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'productos' => $productos,
                'productos_arr' => $productos_arr,
                'productos_obj' => $productos_obj,
                'suma' => $suma,
                'colaboradores' => $colaboradores,
                'clientes' => $clientes,
                'clientesSedes' => $clientesSedes,
                'bodega_id' => $bodega_id,
                'company' => $company,
                'company_id' => $company_id,
                'cliente' => $cliente,
                'sede' => $sede
            ]);
    }


    /**
     * Egreso de producto
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function egreso(Request $request, $company_id, $bodega_id, $producto_id)
    {
        $egreso = new stockEgresosModel;
        $data_egreso = $request->only($egreso->getFillable());

        $data_egreso['producto_id'] = $producto_id;
        $data_egreso['bodega_id'] = $bodega_id;

        $data_egreso['cantidad_medida'] = $request->cantidad;
        $data_egreso['medida_id'] = $request->medida;

        $egreso->fill($data_egreso);
        $egreso->save();

        $detalle = "";
        $producto = productosModel::find($producto_id);
        if ($producto){
            $detalle = $request->cantidad. " " . $producto->nombre;
        }

        if ($request->tipo_cobro == "colaborador"){
            $cobro = new cobroColaboradorModel;
            $precio_cuotas = number_format($request->precio_total / $request->cuotas, 2);

            $cobro->stock_egresos_id = $egreso->id;
            $cobro->producto_id = $producto_id;
            $cobro->colaborador_id = $request->cobrar_colaborador_id;
            $cobro->medida_id = $request->medida;
            $cobro->cantidad = $request->cantidad;
            $cobro->precio_total = $request->precio_total;
            $cobro->precio_cuotas = $precio_cuotas;
            $cobro->cuotas_iniciales = $request->cuotas;
            $cobro->cuotas_restantes = $request->cuotas;

            $cobro->save();


            $descuento = new descuentosModel;
            $precio_cuotas = number_format($request->precio_total / $request->cuotas, 2);

            $descuento->id_empresa = $company_id;
            $descuento->id_colab = $request->cobrar_colaborador_id;
            $descuento->tipo = "productos";
            $descuento->monto = $request->precio_total;
            $descuento->no_cuotas = $request->cuotas;
            $descuento->valor_cuota = $precio_cuotas;
            $descuento->fecha_debito = "Fin de Mes";
            $descuento->detalle = $detalle;
            $descuento->estado = "Activo";

            $descuento->save();
        }

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        bancosModel::destroy($id);
        return redirect()->back()->with('warning', 'Eliminado correctamente');
    }
}
