<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\puestosModel;

class puestosController extends Controller
{
  private $baseModel;
  public function __construct()
  {
    $this->middleware('auth:admin');
    $this->baseModel = new puestosModel;
  }
  public function index(){
    // $listof=$this->baseModel::orderBy("created_at")->get();
    $all = $this->baseModel->all();
    $res = [];
    foreach($all AS $temp){
        $res[$temp["id"]] = [
            'nombre' => $temp["nombre"],
            'descripcion' => $temp["descripcion"],
            'payDT' => $temp["payDT"],
            'payNT' => $temp["payNT"],
            'payEDT' => $temp["payEDT"],
            'payENT' => $temp["payENT"],
            'extraPay' => $temp["extraPay"],
            'perDiem' => $temp["perDiem"],
            'movil' => $temp["movil"],
            'points' => $temp["points"],
            'id_horary_manager' => $temp["id_horary_manager"],
            'id_task_manager' => $temp["id_task_manager"],
            'viaticos_extra' => $temp["viaticos_extra"],
        ];
    }

    /* preparaciones para breadcrumb, task y horary manager*/
    $breadcrumb = [
        'base' => 'Puestos',
        'inicio' => 'puestos'
    ];

    session(['breadcrumb' => $breadcrumb]);

    return view(
                'admin.puestos.show',
                [
                    'menubar'=> $this->list_sidebar(),
                    'data'=>$res
                ]
              );
  }

  public function store(Request $request) {

    $data = $request->only($this->baseModel->getFillable());
    $nombre = preg_replace('/\s/', '-', $request->nombre);
    $data['id_task_manager'] = $this->generateRandomManagerId($nombre);
    $data['id_horary_manager'] = $this->generateRandomManagerId($nombre);

    $this->baseModel->fill($data)->save();

    return redirect()->back()->with('success','Guardado correctamente!');
  }
  public function update(Request $request, $id) {
    $model = $this->baseModel::find($id);
    $data = $request->only($this->baseModel->getFillable());
    $model->fill($data)->save();
    $this->changeLog([
        'area' => 'puestos',
        'type' => 'edicion',
        'request' => $request,
        'element_id' => $id,
    ]);
    return redirect()->back()->with('info','Actualizado correctamente!');
  }
  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }
}
