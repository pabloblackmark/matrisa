<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\productosModel;
use App\adminModels\bodegasModel;
use App\adminModels\categoriesModel;
use App\adminModels\subCategoriesModel;
use App\adminModels\providers;
use App\adminModels\unidadesModel;
use App\adminModels\alertasModel;
use App\adminModels\stockModel;
use Illuminate\Support\Facades\Storage;


class productosController extends Controller
{

  private $baseModel;

  public function __construct()
  {
    $this->middleware('auth:admin');
    $this->baseModel = new productosModel;
  }

  public function index(){
    $data = $this->baseModel::orderBy("created_at")->get();

    foreach($data as $temp){
        $temp->categoria_element = categoriesModel::find($temp->categoria_id);
        $temp->sub_categoria_element = subCategoriesModel::find($temp->sub_categoria_id);
    }

    $categorias_data = categoriesModel::get();
    $categorias = [['', '- - -']];
    $subcats = [];
    $sub_categorias_arr = [['', '- - -']];
    $categorias_obj = [['', '- - -']];
    foreach($categorias_data as $temp){
        $categorias_obj[$temp->id] = $temp;

        $categorias[] = [$temp->id, $temp->category];
        $subcats[$temp->id] = categoriesModel::find($temp->id)->subcats;
        $sub_categorias_temp = categoriesModel::find($temp->id)->subcats;
        foreach($sub_categorias_temp as $temp2){
            $sub_categorias_arr[] = [$temp2->id, $temp2->subCategory];
        }

    }

    $unidades = unidadesModel::get();
    $unidades_arr = [['', '- - -']];
    foreach($unidades as $temp){
        $unidades_arr[] = [$temp->id, $temp->nombre];
    }

    return view('admin.productos.show',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$data,
           'categorias' => $categorias,
           'categorias_obj' => $categorias_obj,
           'subcategorias' => $subcats,
           'sub_categorias_arr' => $sub_categorias_arr,
           'unidades_arr' => $unidades_arr
       ]);
  }

  public function store(Request $request) {
      $data = $request->only($this->baseModel->getFillable());

      $this->baseModel->fill($data)->save();

      $this->revisar_precio_producto($this->baseModel->id);

      return redirect()->back()->with('success','Guardado correctamente!');
  }

  public function update(Request $request, $id) {
      $model = $this->baseModel::find($id);
      $data = $request->only($this->baseModel->getFillable());

      $model->fill($data)->save();

      $this->revisar_precio_producto($id);

      return redirect()->back()->with('info','Actualizado correctamente!');
  }

  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }

}
