<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\puestosModel;
use App\adminModels\bancosModel;
use App\adminModels\contratosModel;
use App\adminModels\companiesModel;
use App\adminModels\company_colab_assocModel;
use Illuminate\Support\Facades\Hash;
class colaboradoresController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'colaboradores';
    }

    public function index(){
        $data = colaboradores::query();
        // $data = colaboradores::query();
        if(!empty($_GET["estado"])){
          $data = $data->where("estatus",$_GET["estado"]);
        }else{
          $data = $data->where("estatus",'Activo');
        }
        $data = $data->orderBy("nombres","ASC")->orderBy("apellidos","ASC")->get();
        // dd($data);
        $docs = documentos::get();
        $qcompanies = companiesModel::get();

        $companies = [];
        $companies_ids = [];
        foreach($qcompanies as $temp){
            $companies[] = [$temp->id, $temp->company];
            $companies_ids[] = "Empresa_" . $temp->id;
        }

        /* preparaciones para breadcrumb y file manager*/
        $breadcrumb = [
            'base' => 'Colaboradores',
            'inicio' => 'colaboradores'
        ];

        session(['breadcrumb' => $breadcrumb]);

        foreach($data as $dt){
            if ($dt->id_file_manager == null){
                $ref = preg_replace('/\s+/', '-', $dt->nombres . ' ' . $dt->apellidos);
                $dt->id_file_manager = $this->generateRandomFileManagerId($ref);
                $dt->save();
            }
        }
        /* preparaciones para breadcrumb y file manager*/

        return view('admin.colaboradores.index',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'docs' => $docs,
               'breadcrumb' => $breadcrumb,
               'companies' => $companies,
               'companies_ids' => $companies_ids
              ]);
    }

    public function show($id=null){
        $edit = false;
        $data = [];

        if ($id != null){
            $data = colaboradores::find($id);
            $edit = true;
        }

        $pdata = puestosModel::get();
        $puestos = [];
        foreach($pdata as $tpd){
            $puestos[] = $tpd->nombre;
        }

        $pdata = ContratosModel::get();
        $contratos = [];
        foreach($pdata as $tpd){
            $contratos[] = $tpd->name;
        }

        $pdata = BancosModel::get();
        $bancos = [];
        foreach($pdata as $tpd){
            $bancos[] = $tpd->nombre;
        }

        return view('admin.colaboradores.show',
              ['menubar'=> $this->list_sidebar(),
               'data' => $data,
               'edit' => $edit,
               'depmun' => $this->get_depmunic(),
               'puestos' => $puestos,
               'contratos' => $contratos,
               'bancos' => $bancos
              ]);
    }

    public function store(Request $request) {
      // dd('POST');
        $model = new colaboradores;
        $data = $request->only($model->getFillable());
        try{

            if ($request->file('foto')){
                $foto = $request->file('foto')->storePublicly('fotos', 'public');
                $data['foto'] = $foto;
            }

            if ($request->file('firma')){
                $foto = $request->file('firma')->storePublicly('firmas', 'public');
                $data['firma'] = $foto;
            }

            $data['password'] = Hash::make($data['password']);
            $model->fill($data)->save();


            return redirect($this->back)->with('success','Guardado correctamente!');
            // return $this->changeLog(redirect($this->back)->with('success','Guardado correctamente!'));
        }catch(\Exception $e){
            return redirect()->back()->with('warning','No se pudieron guardar los datos: ' . $e);
        }

    }

    public function update(Request $request, $id) {
        $model = new colaboradores;
        $finded = $model::find($id);
        $dataMod = $request->only($model->getFillable());

        if ($request->file('foto')){
            $foto = $request->file('foto')->storePublicly('fotos', 'public');
            $dataMod['foto'] = $foto;
        }

        if ($request->file('firma')){
            $firma = $request->file('firma')->storePublicly('firmas', 'public');
            $dataMod['firma'] = $firma;
        }
        if(!empty($dataMod['password']))
          {
            $dataMod['password'] = Hash::make($dataMod['password']);
          }else{
            unset($dataMod['password']);
          }

        $this->changeLog([
                          'area' => 'colaboradores',
                          'type' => 'edicion',
                          'request' => $request,
                          'element_id' => $id
                      ]);
        // dd($request->password,$dataMod['password'],$dataMod);
        $finded->fill($dataMod)->save();
        return redirect($this->back)->with('info','Actualizado correctamente!');
    }

    public function multipleAssign(Request $request){
        foreach($request->request as $k => $v){
            $exploded = explode('_', $k);
            if ($exploded[0] == 'Empresa'){
                $assoc = new company_colab_assocModel();
                $data = $request->only($assoc->getFillable());
                $data["idCompany"] = $v;
                $data["status"] = "1";

                $assoc->fill($data)->save();
            }
        }

        return redirect($this->back)->with('info','Actualizado correctamente!');
    }

    public function destroy($id) {
        try {
            colaboradores::destroy($id);
            return redirect()->back()->with('warning','Borrado correctamente!');
          }catch (\Exception $e) {
           return redirect()->back()->with('error','No se puede eliminar porque se está utilizando por un usuario del módulo -Personal-. Primero debe eliminar esta configuración.'.$e->getCode());
        }
    }

}
