<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\equiposModel;
use App\adminModels\bodegasModel;
use App\adminModels\categoriesModel;
use App\adminModels\subCategoriesModel;
use App\adminModels\providers;
use App\adminModels\unidadesModel;
use Illuminate\Support\Facades\Storage;


class equiposController extends Controller
{

  private $baseModel;

  public function __construct()
  {
    $this->middleware('auth:admin');
    $this->baseModel=new equiposModel;
  }

  public function index(){
    $data = $this->baseModel::orderBy("created_at")->get();

    foreach($data as $temp){
        $temp->sub_categoria_element = subCategoriesModel::find($temp->sub_categoria);
    }

    $categorias_data = categoriesModel::get();
    $categorias = [[null, '- - -']];
    $subcats = [[null, '- - -']];
    $categorias_obj = [];
    foreach($categorias_data as $temp){
        $categorias[] = [$temp->id, $temp->category];
        $categorias_obj[$temp->id] = $temp->category;
        $subcats[$temp->id] = categoriesModel::find($temp->id)->subcats;
    }
    $unidades = unidadesModel::get();
    $unidades_arr = [[null, '- - -']];
    foreach($unidades as $temp){
        $unidades_arr[] = [$temp->id, $temp->nombre];
    }

    foreach($data as $temp){
        $temp->categoria_name = $categorias_obj[$temp->categoria];
    }

    return view('admin.equipos.show',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$data,
           'categorias' => $categorias,
           'subcategorias' => $subcats,
           'unidades_arr' => $unidades_arr
       ]);
  }

  public function store(Request $request) {
      $data = $request->only($this->baseModel->getFillable());

      $this->baseModel->fill($data)->save();
      return redirect()->back()->with('success','Guardado correctamente!');
  }

  public function update(Request $request, $id) {
      $model = $this->baseModel::find($id);
      $data = $request->only($this->baseModel->getFillable());

      $model->fill($data)->save();
      return redirect()->back()->with('info','Actualizado correctamente!');
  }

  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }


}
