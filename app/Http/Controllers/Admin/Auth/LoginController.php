<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Auth;
use Hash;
class LoginController extends Controller
{
    public function __construct(){
      $this->middleware("guest:admin",["except"=>['logout','userlogout']]);
    }
    public function showLoginView(){
      // echo var_dump(route('home'));
      return view('admin.Auth.login');
    }
    public function Login(Request $request){
        $this->validate($request,[
          'usersys'=>'required',
          'password'=>'required',
        ]);
        $credentials=['usersys'=>$request->usersys,
                      'password'=>$request->password];
                      // dd($request->only('usersys','password'));
                      // dd(Auth::guard('admin'));
        if(Auth::guard('admin')->attempt($request->only('usersys','password'))){
          // dd(route('admin.home'));
            return redirect()
                ->intended(route('admin.home'))
                ->with('status','You are Logged in as Admin!');
        }
// dd("test");
        return $this->loginFailed();

      // $this->validate($request,[
      //   'usersys'=>'required',
      //   'pass'=>'required',
      // ]);
      // $credentials=['usersys'=>$request->usersis,
      //               'pass'=>$request->password];
      // if(Auth::guard('admin')->attempt($credentials,$request->remember)){
      //    $estl = route('home')."/";
      //   return redirect()->intended($estl);
      // }
      // $errors = new MessageBag(['password' => ['Usuario o contraseña invalidos.']]);
      // return redirect()->back()->withInput($request->only('usersis','remember'))->withErrors($errors);
    }
    public function username()
    {
        return 'usersys';
    }
    public function loginUsername()
{
    return 'usersys';
}
    public function logout(){
      Auth::guard('admin')->logout();
      $estl = route('admin.login');
      return redirect($estl);
    }
    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'usersys'    => 'required|string|exists:admins|min:5|max:191',
            'pass' => 'required|string|min:4|max:255',
        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];
        // dd($request->validate($rules,$messages));
        //validate the request.
        $request->validate($rules,$messages);
    }
    private function loginFailed(){
      return redirect()
          ->back()
          ->withInput()
          ->with('error','Login failed, please try again!');
  }
  // protected function authenticated(Request $request, $user)
  // {
  //   dd($request);
  // if ( $user->isAdmin() ) {// do your magic here
  //     return redirect()->route('home');
  // }
  //
  //  return redirect('/');
  // }
}
