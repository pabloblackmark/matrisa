<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\equiposModel;
use App\adminModels\stockEgresosEquipoModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\stockEgresosModel;
use App\adminModels\unidadesModel;
use App\adminModels\categoriesModel;
use App\adminModels\subCategoriesModel;

class egresosEquipoController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($bodega_id)
    {
        $bodega = bodegasModel::find($bodega_id);
        $data = stockEgresosEquipoModel::where('bodega_id', $bodega_id)->orderBy('id', 'desc')->get();
        $unidades = unidadesModel::get();
        $unidades_arr = [[null, '- - -']];
        $unidades_obj = [];
        foreach($unidades as $temp){
            $unidades_arr[] = [$temp->id, $temp->nombre];
            $unidades_obj[$temp->id] = $temp;
        }

        foreach($data as $temp){
            $equi = equiposModel::find($temp->equipo_id);
            $medi = unidadesModel::find($equi->medida_id);
            $temp->equipo = $equi;
            $temp->medida = $medi;
            $cate = categoriesModel::find($equi->categoria_id);
            $subcate = subCategoriesModel::find($equi->sub_categoria_id);
            $temp->categoria = $cate;
            $temp->sub_categoria = $subcate;
        }

        return view('admin.bodegas.egresosEquipo',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'unidades_obj' => $unidades_obj
            ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
