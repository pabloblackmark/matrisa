<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\adminModels\UserAdmin;

use App\adminModels\colaboradores;

use App\apiModels\descuentosModel;


class adminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
      // dd("llego");
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      //
      // dd(UserAdmin::find($request->user()->id_prson)->role->codaccus);
      // dd($this->list_sidebar());
        // session[]


        $this->current_month_birthdays = colaboradores::currentMonthBirthday();

        /*
        return view('admin.home')->
               with('menubar', $this->list_sidebar());
        */

        $modelo_colabs = new colaboradores;

        $descuentos = descuentosModel::where('estado','No Activo')->get();

        return view('admin.home',
              ['menubar'=> $this->list_sidebar(),
               'current_month_birthdays' => $this->current_month_birthdays,
               'descuentos' => $descuentos,
               'modelo_colabs' => $modelo_colabs,
              ]);

    }

}
