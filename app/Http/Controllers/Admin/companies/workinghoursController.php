<?php

namespace App\Http\Controllers\admin\companies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\colaboradores;
use App\adminModels\documentos;
use App\adminModels\company_colab_assocModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\companiesModel;
use App\apiModels\loggsModel;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
class workinghoursController extends Controller
{
    protected $redirecUlr;
    public function __construct(){
      $this->middleware('auth:admin');
      $this->back = 'admin.workdone';
    }
    public function index()
    {
      $data0 = companiesModel::orderBy('company')->get();
      $companies = [];
      foreach($data0 as $d){
          $companies[] = [$d->id,$d->company];
      }
      // dd($companies);
          return view('admin.companies.selecompshedules',
                ['menubar'=> $this->list_sidebar(),
                 'companies' => $companies,
                ]);
    }
    public function show($id=null)
    {
        $edit = false;
        $data = [];

        if ($id != null){

            $company_colab_query = company_colab_assocModel::where(["idCompany"=>$id,"status"=>1])->get();
            $edit = true;

            foreach($company_colab_query as $k=>$v){
                $client_colab_query = client_colab_assocModel::where('idColab', $v->idColab)->where('status', 1)->first();
                if ($client_colab_query){
                    $data[] = $v;
                }
            }
        }

        $data0 = companiesModel::orderBy('company')->get();
        $companies = [];
        foreach($data0 as $d){
            $companies[] = [$d->id,$d->company];
        }
        return view('admin.companies.selecompshedules',
              ['menubar'=> $this->list_sidebar(),
               'companies' => $companies,
               'colabs'=>$data,
               'idEmp'=>$id
              ]);
        // return view('admin.companies.listcolabsshedules',
        //       ['menubar'=> $this->list_sidebar(),
        //        'data' => $data,
        //        'edit' => $edit
        //       ]);
    }
    public function workReport($id,$idComp,$nWk=null,$nYr=null)
    {
      $weekNumb = (!empty($nWk)?$nWk:date("W"));
      $years = (!empty($nYr)?$nYr:date("Y"));
      $week_array = $this->getStartAndEndDate($weekNumb,$years);
      // dd($week_array);
      $septimo = false;
      $edit = false;
      $data = [];
      if ($id != null){
        $days = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
        $daysEng = ['Monday'=>'Lunes',
                 'Tuesday'=>'Martes',
                 'Wednesday'=>'Miércoles',
                 'Thursday'=>'Jueves',
                 'Friday'=>'Viernes',
                 'Saturday'=>'Sábado',
                 'Sunday'=>'Domingo'];
        // dd(date("l"),date("L"));
        $baseDay = ['dayWeek'=>'','dateDay'=>'','ontime'=>'','id'=>0,'hourEnter'=>null,
                    'hourExit'=>null,'hourEnterBetw'=>null,'hourExitBetw'=>null,'outTimeEnter'=>null,'hourDay'=>null,
                    'extraHourDay'=>null,'hourNight'=>null,'extraHourNight'=>null,'extraHourEspecial'=>null,'estado'=>'',
                    'clientName'=>'','tasks'=>'','baseTasks'=>null,'aditTask'=>0];
        $fechasReservadas = [];
        /***permiso sin recuperacion de tiempo***/
        $vacations = defaulttimepermisionsModel::where("idUser",$id)
                                                  ->where('status',1)
                                                  ->where('typos','Vacaciones')
                                                  ->get();


        // dd($vacations);
        $tiemDefault = defaulttimepermisionsModel::where("idUser",$id)
        ->where('status',1)
        ->get();
        $dateDefPerm = [];
        // echo "<pre>";
        foreach($tiemDefault AS $permis){
          $date = $permis->datePerm;
          $date = strtotime($date);
          $date = strtotime("+".($permis->days-1)." day", $date);
          // echo date('M d, Y', $date);
          // dd($permis->datePerm,$date,date('Y-m-d', $date));
          $period = new DatePeriod(
                         new DateTime($permis->datePerm),
                         new DateInterval('P1D'),
                         new DateTime(date('Y-m-d',$date).' 23:59:59')
                    );
          // dd($period);
          // $dateDefPerm = [];
          foreach ($period as $key => $value) {
            // var_dump($value->format("Y-m-d"));
            if(strtotime($value->format("Y-m-d"))>=strtotime($week_array["week_start"])&&
               strtotime($value->format("Y-m-d"))<=strtotime($week_array["week_end"])){
              $dayP = $value->format('l');
              $dateDefPerm[$daysEng[$dayP]] = [$value->format("Y-m-d"),
                                               ((strtolower($permis->typos)=='otro')?$permis->typeOther:$permis->typos),
                                              '<b>Tipo:</b>'.((strtolower($permis->typos)=='otro')?$permis->typeOther:$permis->typos).'<br>'.
                                              '<b>Descripción:</b>'.$permis->description];
            }
            $fechasReservadas[strtotime($value->format("Y-m-d"))] = $value->format("Y-m-d");
          }
        }
        // dd($dateDefPerm);
        /***permiso con recuperacion de tiempo***/
        $timeperms = timepermisionsModel::where("idUser",$id)
        ->where('status',1)
        ->get();

        foreach($timeperms AS $valorx){
          $pperiod = new DatePeriod(
                         new DateTime($valorx->dateOut),
                         new DateInterval('P1D'),
                         new DateTime($valorx->dateEnter)
                    );
          // $datePerms = [];
          foreach ($pperiod as $key => $value) {
            $fechasReservadas[strtotime($valorx->dateOut)] = date('Y-m-d', strtotime($valorx->dateOut));
            $fechasReservadas[strtotime($valorx->dateEnter)] = date('Y-m-d', strtotime($valorx->dateEnter));
            $fechasReservadas[strtotime($valorx->deteRepEnter)] = date('Y-m-d', strtotime($valorx->deteRepEnter));
            $fechasReservadas[strtotime($valorx->deteRepOut)] = date('Y-m-d', strtotime($valorx->deteRepOut));
          }
        }

        $timeperms = timepermisionsModel::where("idUser",$id)
        ->whereBetween('dateOut', [$week_array["week_start"],$week_array["week_end"]])
        ->where('status',1)
        ->get();
        // dd($timeperms);
        $datePerms = [];
        foreach($timeperms AS $valorx){
          $pperiod = new DatePeriod(
                         new DateTime($valorx->dateOut),
                         new DateInterval('P1D'),
                         new DateTime($valorx->dateEnter)
                    );
          // $datePerms = [];
          foreach ($pperiod as $key => $value) {
              $dayP = $value->format('l');
              $datePerms[$daysEng[$dayP]] = [$valorx->dateOut,$valorx->dateEnter,
                                            '<b>Descripción:</b>'.$valorx->description,$valorx->typeOther];
            $fechasReservadas[] = date('Y-m-d', strtotime($valorx->dateOut));
            $fechasReservadas[] = date('Y-m-d', strtotime($valorx->dateEnter));
          }
        }
        $pperiod = $timeperms = $permis = [];
        // dd($datePerms);
        /***fechas de los dias de la semana***/
        $dayts = [];
        $datesDays = new DatePeriod(
                       new DateTime($week_array["week_start"]),
                       new DateInterval('P1D'),
                       new DateTime($week_array["week_end"].' 23:59:59')
                  );

        foreach($datesDays AS $vals){
          $dayP = $vals->format('l');
          $dayts[$daysEng[$dayP]] = $vals->format("Y-m-d");
        }
        // dd($dayts);
        $dateDays = [];
        $data = loggsModel::whereBetween('dateDay', [$week_array["week_start"],$week_array["week_end"]])
                          ->where("userId",$id)
                          ->where('idCompany',$idComp)->get();
        $ordeDay = [];
        // dd($data);
        foreach($data AS $datPo){
          $estatus = client_colab_assocModel::where(["id"=>$datPo->idLink,"status"=>1])->get();
          if(count($estatus)>0){
            $puestoInfo = $estatus[0]->getJobs()->first();
            $ordeDay[$datPo->dayWeek][$datPo->idDay][$datPo->idLink] = ['dayWeek'=>$datPo->dayWeek,'dateDay'=>$datPo->dateDay,
                                           'ontime'=>$datPo->ontime,'id'=>$datPo->id,
                                           'viatics'=>$datPo->viatics,'viaticsAdded'=>$datPo->viaticsAdded,
                                           'viaticsMovil'=>$datPo->viaticsMovil,'viaticsDesc'=>$datPo->viaticsDesc,
                                           'hourEnter'=>$datPo->hourEnter,
                                           'hourExit'=>($datPo->hourExit!='00:00:00'?$datPo->hourExit:null),
                                           'hourEnterBetw'=>($datPo->hourEnterBetw!='00:00:00'?$datPo->hourEnterBetw:null),
                                           'hourExitBetw'=>($datPo->hourExitBetw!='00:00:00'?$datPo->hourExitBetw:null),
                                           'outTimeEnter'=>($datPo->outTimeEnter!='00:00:00'?$datPo->outTimeEnter:null),
                                           'hourDay'=>($datPo->hourDay!='00:00:00'?$datPo->hourDay:null),
                                           'extraHourDay'=>($datPo->extraHourDay!='00:00:00'?$datPo->extraHourDay:null),
                                           'hourNight'=>($datPo->hourNight!='00:00:00'?$datPo->hourNight:null),
                                           'extraHourNight'=>($datPo->extraHourNight!='00:00:00'?$datPo->extraHourNight:null),
                                           'extraHourEspecial'=>($datPo->extraHourEspecial!='00:00:00'?$datPo->extraHourEspecial:null),
                                           'extraHourMix'=>($datPo->extraHourMix!='00:00:00'?$datPo->extraHourMix:null),
                                           'estado'=>$datPo->typeday,
                                           'puesto'=>$puestoInfo->nombre,
                                           'clientName'=>$datPo->getInfoClient()->first()->clientName,
                                           'aproved'=>(!empty($datPo->aproved)?$datPo->aproved:0),
                                           'shedlId'=>$datPo->idDay,'baseTasks'=>$datPo->baseTasks,
                                           'tasks'=>$datPo->tasks,'quantityTask'=>$datPo->quantityTask,'complTask'=>$datPo->complTask,
                                           'aditTask'=>$datPo->aditTask,'lessTask'=>$datPo->lessTask,'priceAdiTask'=>$datPo->priceAdiTask,];
            if($datPo->aproved==3){
              $septimo = true;
            }
          }
        }
        // dd($ordeDay,$datePerms,$dateDefPerm);
        // dd($ordeDay);
        $colab = colaboradores::find($id);

        $workIng=client_colab_assocModel::where(["idColab"=>$id,"status"=>1])->get();
        $wokingWeek = [];
        $wokWClient = [];
        // dd($workIng);
        foreach($workIng AS $valus){
          $shed =$valus->getJobs()->first();
          // dd($valus->getInfoClient()->first());
          $client = $valus->getInfoClient()->first();
          $puestoInfo = $valus->getJobs()->first();
          if(!empty($shed)){
            $horarios = $shed->getShedules()->get();
            // dd($horarios);
            foreach($horarios AS $vlone){
              if(!isset($ordeDay[$vlone->day][$vlone->id][$valus->id])
                &&!array_key_exists($vlone->day,$datePerms)
                &&!array_key_exists($vlone->day,$dateDefPerm))
              {

                $tmp = $baseDay;
                $tmp['estado']='No Registrado';
                $tmp['dayWeek']=$vlone["day"];
                $tmp['clientName']=$client->clientName;
                $tmp['puesto']=$puestoInfo->nombre;
                $tmp['idBranch']=$valus->idBranch;
                $tmp['idClient']=$client->id;
                $tmp['idLink']=$valus->id;
                $tmp['idDay']=$vlone->id;
                $tmp['idArea']=$valus->idArea;
                $tmp['entry']=$vlone->entry;
                $tmp['exit']=$vlone->exit;
                $tmp['dateDay'] = $dayts[$vlone["day"]];
                $tmp['idCompany']=$idComp;
                $tmp['aproved']=-1;
                // $orderDays[$dasy][] = $tmp;
                $taskAssi = $shed->tasks()->get();
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"]];
                  }
                  $tmp['tasks']=json_encode($taskList);
                }
                $ordeDay[$vlone->day][$vlone->id][$valus->id] = $tmp;
              }
              else if(isset($ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks']))
              {
                $taskAssi = $shed->tasks()->get();
                // dd($taskAssi);
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  $tasksaved = (!empty($ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks'])?json_decode($ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks'],TRUE):[]);
                  foreach($tasksaved AS $valtask){
                      $taskin[$valtask["idT"]]=$valtask["quantity"];
                  }
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"],
                                   "descript"=>(isset($taskin[$vlist["id"]])?$taskin[$vlist["id"]]:[])];
                  }
                  // dd($trackTask);
                  $ordeDay[$vlone->day][$vlone->id][$valus->id]['tasks']=json_encode($taskList);
                }

                // dd($ordeDay);
              }
              else if(array_key_exists($vlone["day"],$datePerms))
              {
                $tmp = $baseDay;
                $tmp['estado']='Permiso';
                $tmp['dayWeek']=$vlone["day"];
                $tmp['clientName']=$client->clientName;
                $tmp['idBranch']=$valus->idBranch;
                $tmp['idClient']=$client->id;
                $tmp['puesto']=$puestoInfo->nombre;
                $tmp['idLink']=$valus->id;
                $tmp['idDay']=$vlone->id;
                $tmp['idArea']=$valus->idArea;
                $tmp['entry']=$vlone->entry;
                $tmp['exit']=$vlone->exit;
                $tmp['dateDay'] = $dayts[$vlone["day"]];
                $tmp['idCompany']=$valus->getCompany()->first()->idCompany;
                $tmp['descripDay']=$datePerms[$vlone["day"]][2];
                $tmp['aproved']=-1;
                $taskAssi = $shed->tasks()->get();
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"]];
                  }
                  $tmp['tasks']=json_encode($taskList);
                }
                $ordeDay[$vlone->day][$vlone->id][$valus->id] = $tmp;
              }
              else if(array_key_exists($vlone["day"],$dateDefPerm))
              {
                $tmp = $baseDay;
                $tmp['estado']='Permiso '.$dateDefPerm[$vlone["day"]][1];
                $tmp['dayWeek']=$vlone["day"];
                $tmp['clientName']=$client->clientName;
                $tmp['idBranch']=$valus->idBranch;
                $tmp['idClient']=$client->id;
                $tmp['puesto']=$puestoInfo->nombre;
                $tmp['idLink']=$valus->id;
                $tmp['idDay']=$vlone->id;
                $tmp['idArea']=$valus->idArea;
                $tmp['entry']=$vlone->entry;
                $tmp['exit']=$vlone->exit;
                $tmp['dateDay'] = $dayts[$vlone["day"]];
                $tmp['idCompany']=$idComp;
                $tmp['descripDay']=$dateDefPerm[$vlone["day"]][2];
                $tmp['aproved']=-1;
                $taskAssi = $shed->tasks()->get();
                $taskList= $taskin = $tasksaved =[];
                if(count($taskAssi)>0){
                  foreach($taskAssi AS $vlist){
                    $taskList[] = ["ids"=>$vlist["id"],
                                   "name"=>$vlist["name"],
                                   "origin"=>$vlist["count"]];
                  }
                  $tmp['tasks']=json_encode($taskList);
                }
                $ordeDay[$vlone->day][$vlone->id][$valus->id] = $tmp;
              }
            }
          }
          // dd($ordeDay,$taskin,$trackTask);
        }
        // dd($ordeDay);
        $orderDays = [];
        foreach($days AS $ky=>$dasy){
          if(isset($ordeDay[$dasy])){
            // dd($tmp);
            $orde = $ordeDay[$dasy];
            ksort($orde);
            $orderDays[$dasy] = $orde;
            // dd($wokingWeek);
          }
          else{
            // dd($workIng);
            foreach($workIng as $valuesAssoc)
            {
              // dd($valus->getClientInfo()->first());
              $cliente = $valus->getClientInfo()->first()->getClient()->first();
              $puestoInfo = $valus->getJobs()->first();
              $tmp = $baseDay;
              $tmp['estado']='Sin horario';
              $tmp['dateDay'] = $dayts[$dasy];
              $tmp['dayWeek']=$dasy;
              $tmp['idClient']=$cliente->id;
              $tmp['clientName']=$cliente->clientName;
              $tmp['puesto']=$puestoInfo->nombre;
              $tmp['idBranch']=$valuesAssoc->idBranch;
              $tmp['idLink']=$valuesAssoc->id;
              $tmp['aproved']=-1;
              $tmp['idArea']=$valuesAssoc->idArea;
              $tmp['idCompany']=$idComp;
              $orderDays[$dasy][$ky][$valuesAssoc->id] = $tmp;
            }
          }
        }
        // dd($orderDays);
        // dd($wokingWeek);


        // $ANIOS = loggsModel::select(DB::raw("EXTRACT(YEAR FROM created_at) AS year"))->
        // groupByRaw('year')->get();
        foreach(range(2017,date("Y")) as $anios){
          // $obj->$key = new stdClass();
          // array_to_obj($value, $obj->$key);
          $ANIOS[] = (object)["year"=>$anios];

        }
        // $ANIOS = $ANIOS;
        // dd((object)$ANIOS);

        // $thyeas = loggsModel::where("userId",$id)->where('idCompany',$idComp)->groupBy("ye")->get();
        // $company =
        // dd($colab);
      }
      // dd($fechasReservadas);
      return view('admin.companies.Mlaboremploy',
            ['menubar'=> $this->list_sidebar(),
             'data' => $orderDays,
             'colabInfo'=> $colab,
             'idCompany'=>$idComp,
             'idColab'=>$id,
             'years'=> $ANIOS,
             'yr' => $years,
             'wk' => $weekNumb,
             'week' => $week_array,
             'idUser' => $id,
             'vacationes' => $vacations,
             'septimo'=>$septimo,
             'fechasReservas'=>$fechasReservadas
            ]);
      // dd($id);
    }
    function workReportSave(Request $request,$id,$idComp)
    {
      $model = new loggsModel;
      $colab = colaboradores::find($id);
      $dataOrig = json_decode($request->datos,TRUE);
      // $dataOrig = $request->only($model->getFillable());
      // dd($request["idPer"],$dataOrig);
      // dd($colab->hora_extra_diurna,$colab->hora_extra_nocturna,$colab->hora_extra_especial);
      $tyops = [1=>'aprobado',2=>'descuento',3=>'descuento_septimo'];
      $dataOrig['typeday'] = $tyops[$dataOrig['aproved']];
      // var_dump(empty($dataOrig["idPer"]));
      if(!empty($dataOrig["idPer"]))
      {
        $howHours = '';
        $finded = $model::find($dataOrig["idPer"]);
        $dataMod = $dataOrig;
        if(!empty($dataMod["aproved"])&&$dataMod["aproved"]==1){
          $enterHour = $dataMod["hourEnter"];
          $exitHour =  $dataMod["hourExit"];
          // dd($this->hourSeconds($enterHour)>$this->hourSeconds($exitHour));
          if($this->hourSeconds($enterHour)>$this->hourSeconds($exitHour)){
            $exitHour =  $this->hourSeconds($exitHour);
            $exitHour = ($exitHour + 86400);
            $exitHour = $this->secondsHour($exitHour);
          }
          $outenter = $this->bettwenMin($enterHour,$exitHour);
          // echo var_dump($exitHour,$outenter);
          // die();
          // $dataMod["hourDay"] = $outenter;
          $dataMod["hourDay"] = $outenter;
          if(isset($dataMod["extraHourDay"])){
            $salida = $this->hourSeconds($dataMod["extraHourDay"]);
            $exitBefore = true;
            $dataMod["extraHourDayMiddle"] = floor($salida / 1800);
            $dataMod["extraPayDay"] = $dataMod["extraHourDayMiddle"]*($colab->hora_extra_diurna/2);
          }
          if(isset($dataMod["extraHourNight"])){
            $salida = $this->hourSeconds($dataMod["extraHourNight"]);
            // $exitBefore = true;
            $dataMod["extraHourNightMiddle"] = floor($salida / 1800);
            $dataMod["extraPayNight"] = $dataMod["extraHourNightMiddle"]*($colab->hora_extra_nocturna/2);
          }
          if(isset($dataMod["extraHourEspecial"])){
            $salida = $this->hourSeconds($dataMod["extraHourEspecial"]);
            // $exitBefore = true;
            $dataMod["extraHourEspecMiddle"] = floor($salida / 1800);
            $dataMod["extraPayEspec"] = $dataMod["extraHourEspecMiddle"]*($colab->hora_extra_especial/2);
          }
          if(isset($dataMod["extraHourMix"])){
            $salida = $this->hourSeconds($dataMod["extraHourMix"]);
            // $exitBefore = true;
            $dataMod["extraHourMixMiddle"] = floor($salida / 1800);
            $dataMod["extraPayMix"] = $dataMod["extraHourMixMiddle"]*($colab->hora_extra_mixta/2);
          }
        }
        else
        {
          unset($dataMod["tasks"]);
          unset($dataMod["hourDay"]);
          unset($dataMod["hourExit"]);
          unset($dataMod["hourEnterBetw"]);
          unset($dataMod["hourExitBetw"]);
        }
        // var_dump($dataMod);
        // die();
        $finded->fill($dataMod)->save();
      }
      else
      {
        $data = $dataOrig;
        // var_dump($dataOrig);
        // die();
        if(!empty($data["aproved"])&&$data["aproved"]==1){
          $enterHour = $data["hourEnter"];
          $exitHour =  $data["hourExit"];
          if($this->hourSeconds($enterHour)>$this->hourSeconds($exitHour)){
            $exitHour =  $this->hourSeconds($exitHour);
            $exitHour = ($exitHour + 86400);
            $exitHour = $this->secondsHour($exitHour);
          }
          // dd($exitHour);
          $outenter = $this->bettwenMin($enterHour,$exitHour);
          if(isset($data["extraHourDay"])){
            $salida = $this->hourSeconds($data["extraHourDay"]);
            $exitBefore = true;
            $data["extraHourDayMiddle"] = floor($salida / 1800);
            $data["extraPayDay"] = $data["extraHourDayMiddle"]*($colab->hora_extra_diurna/2);
          }
          if(isset($data["extraHourNight"])){
            $salida = $this->hourSeconds($data["extraHourNight"]);
            // $exitBefore = true;
            $data["extraHourNightMiddle"] = floor($salida / 1800);
            $data["extraPayNight"] = $data["extraHourNightMiddle"]*($colab->hora_extra_nocturna/2);
          }
          if(isset($data["extraHourEspecial"])){
            $salida = $this->hourSeconds($data["extraHourEspecial"]);
            // $exitBefore = true;
            $data["extraHourEspecMiddle"] = floor($salida / 1800);
            $data["extraPayEspec"] = $data["extraHourEspecMiddle"]*($colab->hora_extra_especial/2);
          }
          if(isset($data["extraHourMix"])){
            $salida = $this->hourSeconds($data["extraHourMix"]);
            // $exitBefore = true;
            $data["extraHourMixMiddle"] = floor($salida / 1800);
            $data["extraPayMix"] = $data["extraHourMixMiddle"]*($colab->hora_extra_mixta/2);
          }

          // $data["idFollower"] = $this->randem(10);
          $data["hourIdeal"] = $outenter;
          $data["hourDay"] = $outenter;
        }else{
          unset($data["tasks"]);
          unset($data["hourEnter"]);
          unset($data["hourDay"]);
          unset($data["hourExit"]);
          unset($data["hourEnterBetw"]);
          unset($data["hourExitBetw"]);
        }
        $data["dateDay"] = date("Y-m-d",strtotime($dataOrig["dateDay"]));
        // var_dump($data);
        // die();
        $data["latitude"] = '0';
        $data["longitud"] = '0';
        $model->fill($data)->save();
        $dataOrig["idPer"] = $model->id;
      }
      /**********CALCULO DE TAREAS************/
      if(!empty($dataOrig["tasks"])||!empty($dataMod["tasks"])){
        $priceTaskCol = loggsModel::find($dataOrig["idPer"]);
        $idPs = $priceTaskCol->getClientAsoc()->first()->getJobs()->first();
        $getjsbs = (isset($priceTaskCol["tasks"])?json_decode($priceTaskCol["tasks"],TRUE):[]);
        $tDone = [];
        $sumTOt = 0;
        foreach($getjsbs AS $vat){
          $tDone[$vat["idT"]] = $vat["quantity"];
          $sumTOt+= $vat["quantity"];
        }
        $atsks = $idPs->tasks()->get();
        $limitTask = [];
        $totDone = $doneAdd = $doneLess = 0;
        $tmpDone = $totDoneBase = 0;
        if(count($atsks)){
          foreach($atsks AS $taskP){
            if(isset($tDone[$taskP->id])){
              if($tDone[$taskP->id]>$taskP->count){
                $doneAdd+= ($tDone[$taskP->id]-$taskP->count);
              }else if($tDone[$taskP->id]<$taskP->count){
                $doneLess+= ($taskP->count-$tDone[$taskP->id]);
              }
              $totDoneBase+= $taskP->count;
            }

          }
          $totDone = $totDoneBase-$doneLess;
        }
        $priceTaskCol->quantityTask = $totDone;
        $priceTaskCol->baseTasks = $totDoneBase;
        $priceTaskCol->complTask = ($sumTOt>= $totDone?'true':'false' );
        $priceTaskCol->aditTask = $doneAdd;
        $priceTaskCol->lessTask = $doneLess;
        $priceTaskCol->priceAdiTask = ($doneAdd*$colab->hora_extra_especial);
        $priceTaskCol->save();

      }
      /**********FINALIZA CALCULO DE TAREAS************/

      // header('Content-Type: application/json; charset=utf-8');
      return json_encode(['status'=>true,'id'=>$dataOrig["idPer"],'typo'=> $tyops[$dataOrig['aproved']]]);
      // dd($id,$idComp,$request);
    }
    private function getStartAndEndDate($week, $year) {
      $dto = new DateTime();
      $dto->setISODate($year, $week);
      $ret['week_start'] = $dto->format('Y-m-d');
      $dto->modify('+6 days');
      $ret['week_end'] = $dto->format('Y-m-d');
      return $ret;
    }
    public function updateWorkDone(Request $request,$id,$idComp,$nYr,$nWk){
      $listas = [];
      $ideal= $totDone = $addit = $lessdone = 0;
      // dd($request);
      $finded = loggsModel::find($request->idFollower);
      if(!empty($finded)){
        foreach($request->altu AS $ky=>$valus){
          $finl = explode("_",$ky);
          $listas[] = ["idT"=>$finl[0],"quantity"=>$valus];
          $ideal+=$finl[1];
          $totDone+=$valus;
        }
        if($totDone>=$ideal){
          $addit=($totDone-$ideal);
        }else if($totDone<=$ideal){
          $lessdone = $ideal-$totDone;
        }
        // dd($ideal,$totDone,$request,$listas,$id,$idComp,$nWk,$nYr);
        $finded->quantityTask = $totDone;
        // $finded->priceTask = $sumTOt;
        $finded->baseTasks = $ideal;
        $finded->complTask = ($totDone>= $ideal?'true':'false' );
        $finded->aditTask = $addit;
        $finded->lessTask = $lessdone;
        $finded->tasks = json_encode($listas);
        $finded->save();
      }
      return redirect()->back()->with('success','Guardado correctamente!');
    }
    public function vacationSave(Request $request,$id,$idComp,$nWk=null,$nYr=null){
      $perm = new defaulttimepermisionsModel;
      $data = $request->only($perm->getFillable());
      $numdays = $this->getDayFromDates($request->dayBack,$request->datePerm);
      // dd($request);
      // dd($numdays);
      $data["typos"] = 'Vacaciones';
      $data["reponse"] = 'Vacaciones anuales.';
      $user =$this->userName();
      $data["description"] = 'Asignadas desde el administrador por: '.$user;

      $data["status"] = 1;
      $data["days"] = $numdays;
      $data["idUser"] = $id;
      $data["lastDate"] = $request->dayBack;

      // dd($data);
      $perm->fill($data)->save();
      return redirect()->back()->with('success','Guardado correctamente!');
      // $perm->save();
    }
    public function permissionsSave(Request $request,$id,$idComp,$nWk=null,$nYr=null){
      $perm = new defaulttimepermisionsModel;
      $data = $request->only($perm->getFillable());
      $numdays = $this->getDayFromDates($request->dayBack,$request->datePerm);
      // dd($request);
      // dd($numdays);
      $data["typos"] = 'Otro';
      $data["typeOther"] = $request->typeOther;
      // $data["reponse"] = 'Otro nuevo';
      // $user =$this->userName();
      // $data["description"] = 'Asignadas desde el administrador por: '.$user;

      $data["status"] = 1;
      $data["days"] = $numdays;
      $data["idUser"] = $id;
      $data["lastDate"] = $request->dayBack;

      // dd($data);
      $perm->fill($data)->save();
      return redirect()->back()->with('success','Guardado correctamente!');
    }
    public function getDayFromDates($from,$tod){
      $now = strtotime($from); // or your date as well
      $your_date = strtotime($tod);
      $datediff = $now - $your_date;

      return round($datediff / (60 * 60 * 24));
    }
}
