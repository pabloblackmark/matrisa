<?php

namespace App\Http\Controllers\Admin\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companyclientsModel;
class clientsController extends Controller
{
    private $baseModel;
    public function __construct()
    {
      $this->middleware('auth:admin');
      $this->baseModel=new companyclientsModel;
      $this->routeTo='admin.clients.index';
    }
    public function index()
    {
      $dataRes=$this->baseModel::orderBy("clientName")->get();

      /* preparaciones para breadcrumb y file manager*/
      $breadcrumb = [
          'base' => 'Clientes',
          'inicio' => 'clients'
      ];

      session(['breadcrumb' => $breadcrumb]);

      foreach($dataRes as $dt){
          if ($dt->id_file_manager == null){
              $dt->id_file_manager = $this->generateRandomFileManagerId($dt->clientName);
              $dt->save();
          }
      }


      $departamentos = $this->get_depmunic();
      $municipios = [];
      foreach($departamentos as $key => $val){
          foreach($val as $key2 => $val2){
              $municipios[] = [$val2, $val2];
          }
      }

      return view('admin.companies.showClient',
            ['menubar'=> $this->list_sidebar(),
             'data'=>$dataRes,
             'deptos'=>$departamentos,
             'municipios' => $municipios]);
    }

    public function store(Request $request)
    {
      // $nuuser = new usersadminModel;
      // $nuuser->sysuser = 'admin'.$this->randem(4);
      // $nuuser->sysuserpass = $this->randem(8);
      // $nuuser->typeuser = 'clientadmin';
      // $nuuser->save();
      $data = $request->only($this->baseModel->getFillable());
      // $data["idCompany"] = 1;
      $this->baseModel->fill($data)->save();
      return redirect()->route($this->routeTo)->with('success','Guardado correctamente!');
    }

    public function update(Request $request, $id)
    {
      $finded = $this->baseModel::find($id);
      $data = $request->only($finded->getFillable());
      if ($request->venta_sobregiro == null){
          $data["venta_sobregiro"] = false;
      }else{
          $data["venta_sobregiro"] = true;
      }
      $finded->fill($data)->save();
      $this->changeLog([
                    'area' => 'clients',
                    'type' => 'edicion',
                    'request' => $request,
                    'element_id' => $id,
                ]);
      return redirect()->route($this->routeTo)->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
          $this->baseModel::destroy($id);
          return redirect()->back()->with('warning','Borrado correctamente');
      }catch (\Exception $e) {
         return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
      }
    }
}
