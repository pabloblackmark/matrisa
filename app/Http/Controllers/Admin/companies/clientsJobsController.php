<?php

namespace App\Http\Controllers\Admin\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\puestosModel;
use App\adminModels\companyclients_jobsModel;
class clientsJobsController extends Controller
{
    private $baseModel;
    public function __construct()
    {
      $this->middleware('auth:admin');
      $this->baseModel=new companyclients_jobsModel;
      $this->routeTo='admin.clientsjobs.show';
    }
    public function show($id)
    {
      $dataRes=$this->baseModel::where("idClient",$id)->orderBy("created_at")->get();
      $jobsCat = puestosModel::orderBy("nombre")->get();
      $jobsAll[] = ["","--Seleccionar--"];
      foreach($jobsCat AS $valu){
        $jobsAll[] = [$valu->id,$valu->nombre];
      }
      return view('admin.companies.showClientJobs',
            ['menubar'=> $this->list_sidebar(),
             'data'=>$dataRes,
             'jobsCatalog'=>$jobsAll,
             'idm'=>$id
           ]);
    }

    public function store(Request $request)
    {
      // dd($request);
      $data = $request->only($this->baseModel->getFillable());
      $this->baseModel->fill($data)->save();
      return redirect()->route($this->routeTo,$request->idClient)->with('success','Guardado correctamente!');
    }
    public function update(Request $request, $id)
    {
      $finded = $this->baseModel::find($id);
      $data = $request->only($finded->getFillable());
      $finded->fill($data)->save();
      return redirect()->route($this->routeTo,$request->idClient)->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
          $this->baseModel::destroy($id);
          return redirect()->back()->with('warning','Borrado correctamente');
      }catch (\Exception $e) {
         return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
      }
    }
}
