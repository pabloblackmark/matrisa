<?php

namespace App\Http\Controllers\Admin\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\usersadminModel;
use App\adminModels\bodegasModel;
use App\adminModels\bodegasEquipoModel;

class companyController extends Controller
{
    private $baseModel;

    public function __construct()
    {
      $this->middleware('auth:admin');
      $this->baseModel=new companiesModel;
      $this->routeTo='admin.company.index';
    }

    public function index()
    {

      $dataRes=$this->baseModel::orderBy("company")->get();

      /* preparaciones para breadcrumb y file manager*/
      $breadcrumb = [
          'base' => 'Empresa',
          'inicio' => 'company'
      ];

      session(['breadcrumb' => $breadcrumb]);

      foreach($dataRes as $dt){
          if ($dt->id_file_manager == null){
              $dt->id_file_manager = $this->generateRandomFileManagerId($dt->company);
              $dt->save();
          }
      }
      /* preparaciones para breadcrumb y file manager*/

      return view('admin.companies.show',
            ['menubar'=> $this->list_sidebar(),
             'data'=>$dataRes,
             'deptos'=>$this->get_depmunic()]);
    }

    public function store(Request $request)
    {
      // $nuuser = new usersadminModel;
      // $nuuser->sysuser = 'admin'.$this->randem(3);
      // $nuuser->sysuserpass = $this->randem(8);
      // $nuuser->typeuser = 'companyadmin';
      // $nuuser->save();

      $data = $request->only($this->baseModel->getFillable());
      $data["logo"] = '';
      if($request->logo){
        $filePath = self::uploadFilesLocal($request->logo,'companylogos/');
        $data["logo"] = $filePath;
      }

      $bono_extra = $request->bono_extra != null ? 1 : 0;
      $horas_extra = $request->horas_extra != null ? 1 : 0;
      $productividad = $request->productividad != null ? 1 : 0;

      $data['bono_extra'] = $bono_extra;
      $data['horas_extra'] = $horas_extra;
      $data['productividad'] = $productividad;


      $bodega = new bodegasModel();
      $bodega_data = [
          'nombre' => $data['company'],
          'tipo' => 'empresa'
      ];
      $bodega->fill($bodega_data)->save();

      $data['bodega_id'] = $bodega->id;

      // $data["idUser"] = 1;
      $this->baseModel->fill($data)->save();
      return redirect()->route($this->routeTo)->with('success','Guardado correctamente!');
    }
    public function update(Request $request, $id)
    {

      $finded = $this->baseModel::find($id);
      $data = $request->only($finded->getFillable());
      if($request->logo){
        $filePath = self::uploadFilesLocal($request->logo,'companylogos/');
        $data["logo"] = $filePath;
      }
      $bono_extra = $request->bono_extra != null ? 1 : 0;
      $horas_extra = $request->horas_extra != null ? 1 : 0;
      $productividad = $request->productividad != null ? 1 : 0;

      $data['bono_extra'] = $bono_extra;
      $data['horas_extra'] = $horas_extra;
      $data['productividad'] = $productividad;

      $finded->fill($data)->save();

      //actualia nombre bodega
      $bodega = bodegasModel::find($finded->bodega_id);
      $bodega["nombre"] = $request->company;
      $bodega->save();

      $this->changeLog([
                        'area' => 'company',
                        'type' => 'edicion',
                        'request' => $request,
                        'element_id' => $id
                    ]);
      return redirect()->route($this->routeTo)->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = $this->baseModel::find($id);
        $bodega_id = $company->bodega_id;

        try {
          $company->bodega_id = null;
          $company->save();

          // se intenta eliminar la bodega enlazada
          bodegasModel::destroy($bodega_id);

          $this->baseModel::destroy($id);
          return redirect()->back()->with('warning','Borrado correctamente');
        }
        catch (\Exception $e)
        {
         return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
        }
        finally
        {
            $company->bodega_id = $bodega_id;
            $company->save();
        }
    }
}
