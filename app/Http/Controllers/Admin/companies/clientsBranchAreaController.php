<?php

namespace App\Http\Controllers\Admin\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\puestosModel;
use App\adminModels\companyclientsBranchAreaModel;
use App\adminModels\companyclientsModel;
use App\adminModels\companyclientsBranchModel;

class clientsBranchAreaController extends Controller
{
    private $baseModel;
    public function __construct()
    {
      $this->middleware('auth:admin');
      $this->baseModel=new companyclientsBranchAreaModel;
    }
    public function show($id)
    {

      $databra = companyclientsBranchModel::find($id);
      $datacli = companyclientsModel::find($databra->idClient);

      $dataRes=$this->baseModel::where("idBranch",$id)->orderBy("created_at")->get();
      return view('admin.companies.showClientBranchesAreas',
            ['menubar'=> $this->list_sidebar(),
             'data'=>$dataRes,
             'idm'=>$id,
             'clientName' => $datacli->clientName,
             'branchName' => $databra->namebranch,
             'clientId' => $datacli->id
           ]);
    }

    public function store(Request $request)
    {
      // dd($request);
      $data = $request->only($this->baseModel->getFillable());
      $this->baseModel->fill($data)->save();
      return redirect()->back()->with('success','Guardado correctamente!');
    }
    public function update(Request $request, $id)
    {
      $finded = $this->baseModel::find($id);
      $data = $request->only($finded->getFillable());
      $finded->fill($data)->save();
      return redirect()->back()->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idc, $id)
    {
      try {
          $this->baseModel::destroy($id);
          return redirect()->back()->with('warning','Borrado correctamente');
      }catch (\Exception $e) {
         return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
      }
    }



    public static function get_areas(Request $request)
    {

        $id_sucursal = $request->id_sucursal;

        $areas = companyclientsBranchAreaModel::where("idBranch", $id_sucursal)->orderBy("name")->get();

        if (count($areas)>0) {

            $html_options = '<option value="" selected>-- todas --</option>';

            foreach ($areas as $area) {
                if ($request->id_area==$area->id) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }

                $html_options .= '<option value="'.$area->id.'" '.$selected.'>'.$area->name.'</option>';
            }

        } else {
           $html_options = '';
        }

        return $html_options;
    }



}
