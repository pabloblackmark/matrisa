<?php

namespace App\Http\Controllers\Admin\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\puestosModel;
use App\adminModels\companyclientsBranchModel;
use App\adminModels\companyclientsModel;
use App\adminModels\bodegasModel;
class clientsBranchOfficeController extends Controller
{
    private $baseModel;
    public function __construct()
    {
      $this->middleware('auth:admin');
      $this->baseModel=new companyclientsBranchModel;
      $this->routeTo='admin.clientsbranch.show';
    }
    public function show($id)
    {
      $dataRes=$this->baseModel::where("idClient",$id)->orderBy("created_at")->get();
      $clientData = companyclientsModel::find($id);

      return view('admin.companies.showClientBranches',
            ['menubar'=> $this->list_sidebar(),
             'data'=>$dataRes,
             'idm'=>$id,
             'clientName' => $clientData->clientName
           ]);
    }

    public function store(Request $request)
    {
      $data = $request->only($this->baseModel->getFillable());

      $bodega = new bodegasModel();
      $bodega_data = [
          'nombre' => $data['namebranch'],
          'tipo' => 'cliente'
      ];
      $bodega->fill($bodega_data)->save();

      $data['bodega_id'] = $bodega->id;


      $this->baseModel->fill($data)->save();
      return redirect()->route($this->routeTo,$request->idClient)->with('success','Guardado correctamente!');
    }
    public function update(Request $request, $id)
    {
      $finded = $this->baseModel::find($id);
      $data = $request->only($finded->getFillable());
      $finded->fill($data)->save();
      return redirect()->route($this->routeTo,$request->idClient)->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idCli, $id)
    {
      try {
          $this->baseModel::destroy($id);
          return redirect()->back()->with('warning','Borrado correctamente');
      }catch (\Exception $e) {
         return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
      }
    }



    public static function get_sucursales(Request $request)
    {

        $id_cliente = $request->id_cliente;

        $sucursales = companyclientsBranchModel::where("idClient", $id_cliente)->orderBy("namebranch")->get();

        if (count($sucursales)>0) {

            $html_options = '<option value="" selected>-- todas --</option>';

            foreach ($sucursales as $sucursal) {
                if ($request->id_sucursal==$sucursal->id) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }

                $html_options .= '<option value="'.$sucursal->id.'" '.$selected.'>'.$sucursal->namebranch.'</option>';
            }

        } else {
           $html_options = '';
        }

        return $html_options;
    }



}
