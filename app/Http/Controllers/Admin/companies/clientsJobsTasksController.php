<?php

namespace App\Http\Controllers\Admin\companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companyclients_jobstodoModel;

class clientsJobsTasksController extends Controller
{
    private $baseModel;
    public function __construct()
    {
      $this->middleware('auth:admin');
      $this->baseModel=new companyclients_jobstodoModel;
      $this->routeTo='admin.clientsjobs.show';
    }


    public function store(Request $request)
    {
      $data = $request->only($this->baseModel->getFillable());
      // unset($data[""]);
      $this->baseModel->fill($data)->save();
      return redirect()->route($this->routeTo,$request->idClients)->with('success','Guardado correctamente!');
    }
    public function update(Request $request, $id)
    {
      $finded = $this->baseModel::find($id);
      $data = $request->only($finded->getFillable());
      unset($data["idJob"]);
      $finded->fill($data)->save();
      return redirect()->route($this->routeTo,$request->idClients)->with('info','Actualizado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
          $this->baseModel::destroy($id);
          return redirect()->back()->with('warning','Borrado correctamente');
      }catch (\Exception $e) {
         return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
      }
    }
}
