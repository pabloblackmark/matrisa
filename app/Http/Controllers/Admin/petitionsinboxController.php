<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\apiModels\timepermisionsModel;
use App\apiModels\defaulttimepermisionsModel;
use App\apiModels\ecopetitionModel;
use App\apiModels\simplepetitionModel;
use App\apiModels\complaimentsModel;
use App\adminModels\companiesModel;
use DB;
class petitionsinboxController extends Controller
{
  private $baseModel;
  public function __construct()
  {
    $this->middleware('auth:admin');
    // $this->baseModel=new petitionsModel;
    $this->routeTo='admin.inboxpetitions.index';
  }
  public function index($idComp=0,$inbox=null,$type=null,$id=null){
    $contTot=0;
    $indiv = null;
    $typoFilter = ["newentrys"=>0,"aproved"=>1,"denied"=>2];
    // $getimes = (!empty($type)?$type:'');
    $hours = $selfHours = $ecoPerm = $simple = $compla = $tots = [0=>0,1=>0,2=>0];

    $hourperm=DB::table('admin_petitions_restoretimepermissions')
                ->join("admin_company_colab_assoc","admin_petitions_restoretimepermissions.idUser","=","admin_company_colab_assoc.idColab")
                ->select("admin_petitions_restoretimepermissions.status")
                ->selectRaw("COUNT(*) AS tot")
                ->where("idCompany",$idComp)
                ->groupBy("admin_petitions_restoretimepermissions.status")
                ->get();
    $defhourperm=DB::table('admin_petitions_timepermissions')
                ->join("admin_company_colab_assoc","admin_company_colab_assoc.idColab","=","admin_petitions_timepermissions.idUser")
                ->select("admin_petitions_timepermissions.status")
                ->selectRaw("COUNT(*) AS tot")
                ->where("idCompany",$idComp)
                ->groupBy("admin_petitions_timepermissions.status")
                ->get();
    $ecopetit=DB::table('admin_petitions_economicpetitions')
                ->join("admin_company_colab_assoc","admin_company_colab_assoc.idColab","=","admin_petitions_economicpetitions.idUser")
                ->select("admin_petitions_economicpetitions.status")
                ->selectRaw("COUNT(*) AS tot")
                ->where("idCompany",$idComp)
                ->groupBy("admin_petitions_economicpetitions.status")
                ->get();
    $simplepetit=DB::table('admin_petitions_simplepetitions_save')
                ->join("admin_company_colab_assoc","admin_company_colab_assoc.idColab","=","admin_petitions_simplepetitions_save.idUser")
                ->select("admin_petitions_simplepetitions_save.status")
                ->selectRaw("COUNT(*) AS tot")
                ->where("idCompany",$idComp)
                ->groupBy("admin_petitions_simplepetitions_save.status")
                ->get();
    $complaint=DB::table('admin_petitions_complaintments')
                ->join("admin_company_colab_assoc","admin_company_colab_assoc.idColab","=","admin_petitions_complaintments.idUser")
                ->select("admin_petitions_complaintments.status")
                ->selectRaw("COUNT(*) AS tot")
                ->where("idCompany",$idComp)
                ->groupBy("admin_petitions_complaintments.status")
                ->get();
    // dd($hourperm);
    foreach($hourperm AS $val){
      $hours[$val->status] = $val->tot;
      $tots[$val->status]+= $val->tot;
    }
    foreach($defhourperm AS $val){
      $selfHours[$val->status] = $val->tot;
      $tots[$val->status]+= $val->tot;
    }
    foreach($ecopetit AS $val){
      $ecoPerm[$val->status] = $val->tot;
      $tots[$val->status]+= $val->tot;
    }
    foreach($simplepetit AS $val){
      $simple[$val->status] = $val->tot;
      $tots[$val->status]+= $val->tot;
    }
    foreach($complaint AS $val){
      $compla[$val->status] = $val->tot;
      $tots[$val->status]+= $val->tot;
    }
    // dd($hours,$selfHours,$ecoPerm,$simple,$compla);
    // dd($type,$typoFilter[$inbox]);
    $specific = ["hourPerm"=>$hours,"defHourPerm"=>$selfHours,
                 "ecoPetit"=>$ecoPerm,"simplePetit"=>$simple,"complAint"=>$compla,"tots"=>$tots];
    // dd($specific);
    switch($type){
      case 'timeperms':{
          $filtcont = DB::table('admin_petitions_restoretimepermissions')
                      ->join("admin_company_colab_assoc","admin_petitions_restoretimepermissions.idUser","=","admin_company_colab_assoc.idColab")
                      ->join("admin_colaboradores","admin_colaboradores.id","=","admin_company_colab_assoc.idColab")
                      ->select(["nombres","apellidos","admin_petitions_restoretimepermissions.status","foto","typos","description",
                                "reponse","responseDate","admin_petitions_restoretimepermissions.created_at",
                               "admin_petitions_restoretimepermissions.id"])
                      ->where("idCompany",$idComp)
                      ->where('admin_petitions_restoretimepermissions.status',$typoFilter[$inbox])->get();
          if(!empty($id))
            $indiv = timepermisionsModel::find($id);
      }break;
      case 'defhour':{
          $filtcont = DB::table('admin_petitions_timepermissions')
                      ->join("admin_company_colab_assoc","admin_petitions_timepermissions.idUser","=","admin_company_colab_assoc.idColab")
                      ->join("admin_colaboradores","admin_colaboradores.id","=","admin_company_colab_assoc.idColab")
                      ->select(["nombres","apellidos","admin_petitions_timepermissions.status","foto","typos","typeOther","description",
                                "reponse","responseDate","admin_petitions_timepermissions.created_at",
                               "admin_petitions_timepermissions.id"])
                      ->where("idCompany",$idComp)
                      ->where('admin_petitions_timepermissions.status',$typoFilter[$inbox])->get();
          if(!empty($id))
            $indiv = defaulttimepermisionsModel::find($id);
      }break;
      case 'economic':{
          $filtcont = DB::table('admin_petitions_economicpetitions')
                      ->join("admin_company_colab_assoc","admin_petitions_economicpetitions.idUser","=","admin_company_colab_assoc.idColab")
                      ->join("admin_colaboradores","admin_colaboradores.id","=","admin_company_colab_assoc.idColab")
                      ->select(["nombres","apellidos","admin_petitions_economicpetitions.status","foto","typos","comment","descript",
                                "reponse","responseDate","admin_petitions_economicpetitions.created_at",
                               "admin_petitions_economicpetitions.id"])
                      ->where("idCompany",$idComp)
                      ->where('admin_petitions_economicpetitions.status',$typoFilter[$inbox])->get();
          if(!empty($id))
            $indiv = ecopetitionModel::find($id);
      }break;
      case 'simple':{
          $filtcont = DB::table('admin_petitions_simplepetitions_save')
                      ->join("admin_company_colab_assoc","admin_petitions_simplepetitions_save.idUser","=","admin_company_colab_assoc.idColab")
                      ->join("admin_colaboradores","admin_colaboradores.id","=","admin_company_colab_assoc.idColab")
                      ->select(["nombres","apellidos","admin_petitions_simplepetitions_save.status","foto","typos","typeOther","description",
                                "reponse","responseDate","admin_petitions_simplepetitions_save.created_at",
                               "admin_petitions_simplepetitions_save.id"])
                      ->where("idCompany",$idComp)
                      ->where('admin_petitions_simplepetitions_save.status',$typoFilter[$inbox])->get();
          if(!empty($id))
            $indiv = simplepetitionModel::find($id);
      }break;
      case 'complaint':{
          $filtcont = DB::table('admin_petitions_complaintments')
                      ->join("admin_company_colab_assoc","admin_petitions_complaintments.idUser","=","admin_company_colab_assoc.idColab")
                      ->join("admin_colaboradores","admin_colaboradores.id","=","admin_company_colab_assoc.idColab")
                      ->select(["nombres","apellidos","admin_petitions_complaintments.status","foto","comment",
                                "reponse","responseDate","admin_petitions_complaintments.created_at",
                               "admin_petitions_complaintments.id"])
                      ->where("idCompany",$idComp)
                      ->where('admin_petitions_complaintments.status',$typoFilter[$inbox])->get();
          if(!empty($id))
            $indiv = complaimentsModel::find($id);
      }break;
      default:{
        $filtcont = [];
      }break;
    }
    // dd($filtcont);
    $listof = [];
    $companyQRY = companiesModel::orderBy('company')->get();
    $companyList = [];
    foreach($companyQRY as $d){
        $companyList[] = [$d->id,$d->company];
    }
    return view('admin.petitions.showInbox',
          ['menubar'=> $this->list_sidebar(),
           'data'=>$specific,
           'messages'=>$filtcont,
           'inbox'=>$inbox,
          'slectd'=>$type,
          'company'=>$idComp,
          'idPetition'=>$id,
          'indivu'=>$indiv,
          'companyList'=>$companyList,

          ]);
  }

  public function store(Request $request,$company,$inbox,$typos,$id) {
    switch($typos){
      case 'timeperms':{
            $indiv = timepermisionsModel::find($id);
      }break;
      case 'defhour':{
            $indiv = defaulttimepermisionsModel::find($id);
      }break;
      case 'economic':{
            $indiv = ecopetitionModel::find($id);
      }break;
      case 'simple':{
            $indiv = simplepetitionModel::find($id);
      }break;
      case 'complaint':{
            $indiv = complaimentsModel::find($id);
      }break;
    }

    $data = $request->only($indiv->getFillable());
    $data["responseDate"]=date("Y-m-d");
    $data["status"]=(isset($request->status)?1:2);
    // dd($data);
    $indiv->fill($data)->save();
    return redirect()->route($this->routeTo,[$company,$inbox,$typos])->with('success','Guardado correctamente!');
  }
  public function update(Request $request, $id) {
    $model = $this->baseModel::find($id);
    $data = $request->only($this->baseModel->getFillable());
    $model->fill($data)->save();
    return redirect()->route($this->routeTo)->with('info','Actualizado correctamente!');
  }
  public function destroy($id) {
    try {
        $this->baseModel::destroy($id);
        return redirect()->back()->with('warning','Borrado correctamente');
    }catch (\Exception $e) {
       return redirect()->back()->with('error','No se puede eliminar porque hay datos enlazados.'.$e->getCode());
    }
  }
}
