<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\equiposModel;
use App\adminModels\stockIngresosEquipoModel;
use App\adminModels\stockEgresosEquipoModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\companyclientsBranchModel;
use App\adminModels\stockEgresosModel;
use App\adminModels\stockEquiposModel;
use App\adminModels\unidadesModel;
use App\adminModels\equipoAsignadoModel;

class stockEmpresasEquipoController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
        $this->back = 'bancos';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($bodega_id)
    {
        // -------- mayor parte de catalogos -----------
        $bodega = bodegasModel::find($bodega_id);
        $equipos_query = equiposModel::get();

        $equipos = [];
        $equipos_arr = [];
        $equipos_obj = [];
        foreach($equipos_query as $temp){
            $equipos[$temp->id] = $temp->nombre;
            $equipos_arr[] = [$temp->id, $temp->nombre];
            $equipos_obj[$temp->id] = $temp;
        }

        $colaboradores_query = $this->info_bodega($bodega_id)['colaboradores'];
        $colaboradores = [['', '- - -']];
        foreach($colaboradores_query as $temp){
            $colaboradores[] = [$temp->id, $temp->apellidos . ', ' . $temp->nombres];
        }

        $clientes_query = $this->info_bodega($bodega_id)['clientes'];
        $clientes = [['', '- - -']];
        $clientesSedes = [['', '- - -']];
        foreach($clientes_query as $temp){
            $clientes[] = [$temp->id, $temp->clientName];
            $sedes = companyclientsBranchModel::where('idClient', $temp->id)->get();
            foreach($sedes as $temp2){
                $clientesSedes[] = [$temp2->bodega_id, $temp->clientName . ' - Sede ' . $temp2->namebranch];
            }
        }

        $unidades_query = unidadesModel::get();
        $unidades = [['null', '']];
        foreach($unidades_query as $temp){
            $unidades[] = [$temp->id, $temp->nombre];
        }

        //---------------- datos del stock ----------------
        $this->calcularStockEquipo($bodega_id);
        $data = stockEquiposModel::where('bodega_id', $bodega_id)->where('estado', 'activo')->get();

        foreach($data as $temp){
            $equi = equiposModel::find($temp->equipo_id);
            $temp->equipo = $equi;

            $medida = unidadesModel::find($temp->medida_id);
            $temp->medida = $medida->nombre;

            $asignado = equipoAsignadoModel::where('equipo_id', $temp->equipo_id)->orderby('id', 'desc')->first();

            $temp->estado = 'disponible'; // estado por default a menos que se encuentre otra cosa
            if ($asignado != null){
                if ($asignado->tipo_movimiento == 'egreso'){
                    $col = colaboradores::find($asignado->asignado_a_colaborador_id);
                    $cli = companyclientsModel::find($asignado->asignado_a_cliente_id);
                    $temp->estado = 'No disponible (asignado a colaborador:' . $col->nombres . ' ' . $col->apellidos . ' y cliente: ' . $cli->clientName . ')';
                }
            }
        }


        $baja = stockEquiposModel::where('bodega_id', $bodega_id)->where('estado', 'de baja')->get();
        foreach($baja as $temp){
            $equi = equiposModel::find($temp->equipo_id);
            $temp->equipo = $equi;

            $medida = unidadesModel::find($temp->medida_id);
            $temp->medida = $medida->nombre;

            $temp->estado = 'de baja';

            $asignacion = equipoAsignadoModel::where('equipo_id', $temp->equipo_id)->orderby('id', 'desc')->first();
            $temp->fecha = $asignacion->created_at;
            $temp->comentario = $asignacion->comentario;
        }


        return view('admin.stock.empresaIndex',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega_id' => $bodega_id,
                'bodega' => $bodega,
                'equipos' => $equipos,
                'equipos_arr' => $equipos_arr,
                'equipos_obj' => $equipos_obj,
                'unidades' => $unidades,
                'colaboradores' => $colaboradores,
                'clientes' => $clientes,
                'clientesSedes' => $clientesSedes,
                'baja' => $baja
            ]);
    }

    /**
     * Egreso del equipo
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function egreso(Request $request, $bodega_id, $equipo_id)
    {
        $asignacion = new equipoAsignadoModel;
        $data_asignacion = $request->only($asignacion->getFillable());
        $data_asignacion['origen_bodega_id'] = $bodega_id;
        $asignacion->fill($data_asignacion);
        $asignacion->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
    * Reingreso del equipo
    *
    *
    */

    public function ingreso(Request $request, $bodega_id, $equipo_id)
    {
        $ultimo_asignado = equipoAsignadoModel::where('equipo_id', $equipo_id)
                                            ->where('origen_bodega_id', $bodega_id)
                                            ->where('tipo_movimiento', 'egreso')
                                            ->orderby('id', 'desc')->first();

        $asignacion = new equipoAsignadoModel;
        $data_asignacion = $request->only($asignacion->getFillable());
        $data_asignacion['origen_bodega_id'] = $bodega_id;
        $data_asignacion['asignado_a_bodega_id'] = $bodega_id;
        $asignacion->fill($data_asignacion);
        $asignacion->save();

        $egreso = new stockEgresosEquipoModel;
        $data_egreso = $request->only($egreso->getFillable());
        $data_egreso['costo_total'] = $request->cantidad * $request->costo_unidad;
        $data_egreso['bodega_id'] = $bodega_id;
        $data_egreso['cargo_a_cliente_id'] = $ultimo_asignado->asignado_a_cliente_id;
        $data_egreso['cargo_a_colaborador_id'] = $ultimo_asignado->asignado_a_colaborador_id;
        $egreso->fill($data_egreso);
        $egreso->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    public function baja(Request $request, $bodega_id, $equipo_id)
    {
        $equipo = equiposModel::find($equipo_id);
        $equipo['estado'] = 'de baja';
        $equipo->save();

        $stock = stockEquiposModel::where('equipo_id', $equipo_id)->first();
        $stock['estado'] = 'de baja';
        $stock->save();

        $asignacion = new equipoAsignadoModel;
        $data_asignacion = $request->only($asignacion->getFillable());
        $data_asignacion['origen_bodega_id'] = $bodega_id;
        $asignacion->fill($data_asignacion);
        $asignacion->save();

        return redirect()->back()->with('info', 'Guardado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

}
