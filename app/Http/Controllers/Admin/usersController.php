<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\adminModels\UserAdmin;
use App\adminModels\roles;
// use App\adminModels\countries;
use Illuminate\Support\Facades\Hash;
class usersController extends Controller
{
  protected $redirecUlr;
  public function __construct()
  {
      $this->middleware('auth:admin');
  }
  public function index(){
    $en = $this->superuser();
    $count = $this->get_country();
    // if(self::get_userType()["type"]==1){
    $roleUsers = [];
    // $listof=UserAdmin::when(!$en,
    //   function ($query) use ($count) {
    //        return $query->where('country', $count);
    //    })->get();
       // dd($count);
    $listof=UserAdmin::get();
    // $usersAd=roles::where('country', $count)->orderBy('nameRole')->get();
    $usersAd=roles::orderBy('nameRole')->get();
    foreach($usersAd AS $sers){
      $roleUsers[]=array($sers['id'],$sers['nameRole']);
    }

    $paises[] = ["","--Elegir país--"];
    // $usersAd=countries::orderBy('name')->get();
    // foreach($usersAd AS $sers){
    //   $paises[]=[$sers['code'],$sers['name'].' - '.$sers['platform']];
    // }
    return view('admin.user.show',
          ['menubar'=> $this->list_sidebar(),
           'users'=>$listof,
           'roleUsers'=>$roleUsers,
           'countries'=>$paises
          ]);
  }

  public function store(Request $request) {
    $validator = $request->validate([
        'name' => 'required',
        'password' => 'required',
        'usersys' => 'required',
    ]);
    $model = new UserAdmin;
    $data = $request->only($model->getFillable());
    $data['password'] =  Hash::make($request->password);
    $data['country'] =  $this->get_country();
    $data['statusUs'] =  isset($request->statusUs)?1:0;
    // if($this->superuser()){
    //   $data['superuser'] =  1;
    // }
    $model->fill($data)->save();
    return redirect()->back()->with('success','Guardado correctamente!');
  }
  public function destroy($id) {
    UserAdmin::destroy($id);
    return redirect()->back()->with('warning','Borrado correctamente!');
  }
  public function update(Request $request, $id) {

    $model = new UserAdmin;
    $finded = $model::find($id);
    $dataMod = $request->only($model->getFillable());
    $dataMod['password'] =  Hash::make($request->password);
    $dataMod['statusUs'] =  isset($request->statusUs)?1:0;
    // dd($dataMod);
    $finded->fill($dataMod)->save();
    return redirect()->back()->with('info','Actualizado correctamente!');
  }
}
