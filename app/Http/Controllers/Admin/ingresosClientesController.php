<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\adminModels\companiesModel;
use App\adminModels\facturasModel;
use App\adminModels\bodegasModel;
use App\adminModels\productosModel;
use App\adminModels\stockIngresosModel;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\stockEgresosModel;

class ingresosClientesController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index($company_id, $bodega_id)
    {
        $bodega = bodegasModel::find($bodega_id);
        $cliente = $this->info_bodega($bodega_id)['cliente'];
        $sede = $this->info_bodega($bodega_id)['sede'];
        $company = companiesModel::find($company_id);
        $data = stockIngresosModel::where('bodega_id', $bodega_id)->get();
        foreach($data as $temp){
            $prod = productosModel::find($temp->producto_id);
            $temp->producto = $prod;
        }

        return view('admin.bodegas.ingresosClientes',
            [
                'menubar' => $this->list_sidebar(),
                'data' => $data,
                'bodega' => $bodega,
                'company' => $company,
                'cliente' => $cliente,
                'sede' => $sede,
                'company_id' => $company_id
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
