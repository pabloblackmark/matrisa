<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class warehouseModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_warehouse';
  protected $fillable = ['name','address','lat','lon'];
}
