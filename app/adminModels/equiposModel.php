<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class equiposModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_equipos';
  protected $fillable = [
                            'codigo',
                            'nombre',
                            'descripcion',
                            'categoria_id',
                            'sub_categoria_id',
                            'medida_id',
                            'precio_cliente_unidad',
                            'porcentaje_depreciacion',
                            'estado',
                        ];
}
