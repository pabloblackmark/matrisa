<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class companyclientsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company_clients';
  protected $fillable = [
                'clientName',
                'nit',

                'nombre_factura',
                'observaciones',

                'contactName',
                'email',
                'phone1',
                'phone2',
                'depto',
                'municip',
                'addess',

                'idUser',

                'credit',
                'idCompany',

                'vma',
                'venta_sobregiro',
                'id_file_manager',
                'area',
                'id_area'
            ];

  public function getUser(){
    return $this->belongsTo(usersadminModel::class, 'idUser');
  }
  public function getBranch(){
    return $this->belongsTo(companyclientsBranchModel::class, 'idClient');
  }
}
