<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class companyclientsBranchAreaModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company_clients_branch_area';
  protected $fillable = [
      'idBranch',
      'name',
      'description',
  ];
}
