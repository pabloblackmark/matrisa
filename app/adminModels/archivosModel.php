<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class archivosModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_archivos';
  protected $fillable = [
                          'id_file_manager',
                          'name',
                          'description',
                          'url',
                          'type',
                      ];

}
