<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class solicitudes extends Model
{
  protected $guard = 'admin';
  protected $table='admin_solicitudes';
  protected $fillable = ['tipo', 'descripcion', 'reposicion', 'descuento'];
}
