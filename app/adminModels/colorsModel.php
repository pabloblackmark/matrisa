<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class colorsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_colors';
  protected $fillable = ['namecolor','category'];
}
