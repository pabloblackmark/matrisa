<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class warehouseProductsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_werehouse_products';
  protected $fillable = ['id_werhouse','id_product',
                        'amount','lot','datein','whom','egress'];
  public function whoEnter(){
    return $this->belongsTo(UserAdmin::class, 'whom','id');
  }
}
