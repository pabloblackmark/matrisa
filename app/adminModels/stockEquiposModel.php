<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class stockEquiposModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_stock_equipos';
  protected $fillable = [
                            'equipo_id',
                            'cantidad',
                            'medida_id',
                            'costo_unitario',
                            'valor_total',
                            'bodega_id',
                            'estado',
                        ];
}
