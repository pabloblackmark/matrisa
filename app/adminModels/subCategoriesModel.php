<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class subCategoriesModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_subCategories';
  protected $fillable = ['category_id', 'subCategory'];

}
