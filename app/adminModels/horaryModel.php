<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class horaryModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_horary';
  protected $fillable = [
                          'id_horary_manager',
                          'day',
                          'entry',
                          'exit',
                          'type',
                          'date',
                          'description',
                          'horas_diurnas',
                          'horas_nocturnas',
                          'horas_mixtas',
                          'cobrar_receso',
                          'tiempo_receso',
                          'tipo_receso',
                      ];

}
