<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class categoriesModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_categories';
  protected $fillable = ['category'];

  public function subcats(){
    return $this->hasMany(subCategoriesModel::class, 'category_id', 'id');
  }

}
