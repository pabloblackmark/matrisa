<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class stockModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_stock';
  protected $fillable = [
                            'producto_id',
                            'cantidad',
                            'medida_id',
                            'costo_unitario',
                            'valor_total',
                            'bodega_id',
                        ];
}
