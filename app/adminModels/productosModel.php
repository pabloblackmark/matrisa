<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class productosModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_productos';
  protected $fillable = [
                            'codigo',
                            'nombre',
                            'descripcion',
                            'categoria_id',
                            'sub_categoria_id',
                            'medida_final_id',
                            'cantidad_minima_aceptable',
                            'porcentaje_ganancia',
                            'precio_cliente',
                        ];
}
