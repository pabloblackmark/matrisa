<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;
use App\apiModels\loggsModel;
use App\apiModels\loggsPlanillaModel;
class company_colab_assocModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company_colab_assoc';
  protected $fillable = [
                            'idCompany',
                            'idColab',
                            'status',

                            'idPuesto',
                        ];
  public function getColabsInfo(){
   return $this->hasOne(colaboradores::class,'id', 'idColab');
  }
  public function getLoggs(){
   return $this->hasMany(loggsModel::class,'userId','idColab');
  }
  public function getLoggsPlanilla(){
   return $this->hasMany(loggsPlanillaModel::class,'id_colab','idColab');
  }
}
