<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class colaboradores extends Model
{
  protected $guard = 'admin';
  protected $table='admin_colaboradores';
  protected $fillable = [ "id",
                          "nombres",
                          "apellidos",
                          "foto",
                          "fecha",
                          "email",
                          "dpi",
                          "nit",
                          "iggs",
                          "irtra",
                          "intecap",
                          "licencia",
                          "vence_licencia",
                          "fecha_salud",
                          "fecha_pulmones",
                          "genero",
                          "nivel_academico",
                          "nacionalidad",
                          "estado_civil",
                          "conyuge",
                          "hijos",
                          "profesion",
                          "tel1",
                          "tel2",
                          "direccion",
                          "departamento",
                          "municipio",
                          "contacto1",
                          "tel_con1",
                          "parentesco_con1",
                          "contacto2",
                          "tel_con2",
                          "parentesco_con2",
                          "tipo_sangre",
                          "contrato",
                          "puesto",
                          "sueldo",
                          "bono_extra",
                          "antiguedad",
                          "bono_ley",
                          "hora_extra_diurna",
                          "hora_extra_nocturna",
                          "hora_extra_especial",
                          "viaticos",
                          "movil",
                          "hora_normal",
                          "hora_normal_nocturna",
                          "tipo_pago",
                          "cuenta1",
                          "cuenta2",
                          "cuenta3",
                          "banco",
                          "pago_sueldo",
                          "otros_pagos",

                          "puntos1",
                          "puntos2",
                          "puntos3",
                          "puntos4",

                          "estatus",
                          "fecha_ingreso",
                          "fecha_baja",
                          "observaciones",

                          "idUser",

                          "firma",

                          "id_file_manager",

                          "password",

                          "idWRegister",
                          "sueldo_pactado",
                          "zona",
                          "colonia",
                          "hora_extra_mixta",
                      ];


    public static function currentMonthBirthday()
    {
        $birthdays = colaboradores::select('id', 'nombres', 'apellidos', 'fecha', 'foto', 'puesto')
            ->whereRaw("EXTRACT(MONTH FROM fecha) = ?", [date('m')])->orderBy('fecha', 'asc')->get();

        /* $birthdays = colaboradores::select('nombres', 'apellidos', 'fecha', 'foto')
            ->whereRaw("MONTH(fecha) = ?", [date('m')])->where('estatus', '=', 'active')
            ->orderBy('fecha', 'asc')->get(); */

        return $birthdays;
    }


    public static function get_name($id)
    {
        $colaborador = colaboradores::select('nombres', 'apellidos')
            ->where("id", $id)->first();

        if ($colaborador!='') {
            return $colaborador->nombres.' '.$colaborador->apellidos;
        } else {
            return '';
        }

    }

}
