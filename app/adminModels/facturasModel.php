<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class facturasModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_facturas';
  protected $casts = [
    'fecha' => 'date'
  ];
  protected $fillable = [
                            'proveedor_id',
                            'numero',
                            'serie',
                            'total',
                            'fecha',
                            'info',
                            'estado',
                            'tipo',
                            'bodega_id',
                        ];
}
