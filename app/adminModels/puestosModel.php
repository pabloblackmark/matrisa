<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class puestosModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_puestos';
  protected $fillable = [
      'nombre',
      'descripcion',
      'payDT',
      'payNT',
      'payEDT',
      'payENT',
      'extraPay',
      'perDiem',
      'movil',
      'points',
      'id_task_manager',
      'id_horary_manager',
      'viaticos_extra'
  ];

  public function tasks(){
      return $this->hasMany(tasksModel::class, 'id_task_manager', 'id_task_manager');
  }
  public function getShedules(){
   return $this->hasMany(horaryModel::class,'id_horary_manager', 'id_horary_manager');
  }
}
