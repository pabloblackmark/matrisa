<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class companiesModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company';
  protected $fillable = [
                            'company',
                            'nit',
                            'legalrepresent',
                            'logo',
                            'phone1',
                            'phone2',
                            'email',
                            'depto',
                            'municip',
                            'addess',
                            'usercom',
                            'passcom',
                            'idUser',
                            'igss',

                            'fecha_inicio',
                            'fecha_nacimiento',
                            'dpi',
                            'estado_civil',
                            'genero',
                            'nacionalidad',
                            'vecino',

                            'iggs_patronal',
                            'irtra',
                            'iggs_laboral',
                            'intecap',
                            'bono_extra',
                            'horas_extra',
                            'productividad',
                            'calculo_sobre',

                            'id_file_manager',

                            'bodega_id',

                        ];
  public function getUser(){
    return $this->belongsTo(usersadminModel::class, 'idUser');
  }
}
