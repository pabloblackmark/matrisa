<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class sizesModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_sizes';
  protected $fillable = ['namesize','category'];
}
