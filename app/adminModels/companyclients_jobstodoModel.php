<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class companyclients_jobstodoModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company_clients_jobs_todo';
  protected $fillable = ['title','idJob'];

}
