<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class stockIngresosEquipoModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_stock_ingresos_equipo';
  protected $fillable = [
                            'equipo_id',
                            'cantidad',
                            'costo_unidad',
                            'costo_total',
                            'bodega_id',
                            'factura_id',
                        ];
}
