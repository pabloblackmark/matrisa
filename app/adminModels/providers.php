<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class providers extends Model
{
  protected $guard = 'admin';
  protected $table='admin_providers';
  protected $fillable = ['nombre', 'direccion', 'nit', 'contacto', 'telefono', 'email'];
  public function nameroles(){
    return $this->belongsToMany(roles_names::class, 'admin_access_roles',
                                'id_role', 'id_access')
                                ->orderBy('naccess', 'ASC');

  }

}
