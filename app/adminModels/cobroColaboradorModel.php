<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class cobroColaboradorModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_cobro_colaborador';
  protected $fillable = [
                            'stock_egresos_id', //id del registro con los datos del egreso en tabla admin_stock_egresos
                            'producto_id', // id del producto del egreso en tabla admin_productos
                            'colaborador_id', // id del colaborador en tabla admin_colaboradores

                            'medida_id', // id de la medida en tabla admin_unidades
                            'cantidad', // cantidad el producto
                            'precio_total', // precio total del producto
                            'precio_cuotas', // precio de cada cuota segun cantidad de cuotas
                            'cuotas_iniciales', // cuotas iniciales
                            'cuotas_restantes', // cuotas que faltan ir descontando
                            'estado', // puede ser cualquier estado que se desee validar ej. "abierto", "cancelado", etc...
                        ];
}
