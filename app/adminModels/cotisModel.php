<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class cotisModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_cotis';
  protected $fillable = [
                            'no',
                            'fecha',
                            'id_empresa',
                            'empresa',                            
                            'id_cliente',
                            'cliente',
                            'id_sede',                            
                            'sede',
                            'nit',
                            'detalle',
                            'total',
                            'id_admin',
                            'firma',
                            'status',
                        ];
}
