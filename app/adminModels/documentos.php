<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class documentos extends Model
{
  protected $guard = 'admin';
  protected $table='admin_documentos';
  protected $fillable = [
                          'name',
                          'description',
                          'type',
                          'body',
                      ];

}
