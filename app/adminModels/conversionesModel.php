<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class conversionesModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_conversiones';
  protected $fillable = [
                            'unidad_inicial_id',
                            'unidad_final_id',
                            'operacion',
                            'valor',
                            'descripcion',
                        ];
}
