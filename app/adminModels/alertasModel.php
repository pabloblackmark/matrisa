<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class alertasModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_alertas';
  protected $fillable = [
      'elemento_id',
      'referencia',
      'tipo',
      'mensaje',
      'nombre_modulo',
      'url'
  ];
}
