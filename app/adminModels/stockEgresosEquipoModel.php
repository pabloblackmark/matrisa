<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class stockEgresosEquipoModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_stock_egresos_equipo';
  protected $fillable = [
                            'equipo_id',
                            'cantidad',
                            'costo_unidad',
                            'costo_total',
                            'precio_total',
                            'bodega_id',
                            'cargo_a_cliente_id',
                            'cargo_a_colaborador_id',
                        ];
}
