<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class mensajesModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_mensajes';
  protected $fillable = [
                            'de_admin',
                            'para_user',
                            'de_user',
                            'para_admin',  
                            'de',
                            'para',
                            'titulo',
                            'mensaje',
                            'tipo',
                            'status',
                        ];
}
