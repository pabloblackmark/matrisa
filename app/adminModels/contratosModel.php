<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class contratosModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_contratos';
  protected $fillable = [
                          'name',
                          'description',
                          'type',
                      ];

}
