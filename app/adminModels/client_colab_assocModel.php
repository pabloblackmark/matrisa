<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

use App\apiModels\loggsModel;
use App\apiModels\loggsPlanillaModel;

class client_colab_assocModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_client_colab_assoc';
  protected $fillable = [
                            'idClient',
                            'idColab',
                            'status',
                            'idPuesto',
                            'tipo',
                            'inicio',
                            'fin',
                            'idBranch',
                            'idArea'
                        ];
  public function getInfoClient(){
     return $this->hasOne(companyclientsModel::class, 'id',  'idClient');
  }
  public function getClientInfo(){
     return $this->hasOne(companyclientsAssocModel::class,'idClient','idClient');
  }
  public function getJobs(){
     return $this->belongsTo(puestosModel::class, 'idPuesto');
  }
  public function getCompany(){
     return $this->belongsTo(companyclientsAssocModel::class, 'idClient');
  }


  // agregado Fredy
  public function getColabsInfo(){
   return $this->hasOne(colaboradores::class,'id', 'idColab');
  }
  public function getLoggs(){
   return $this->hasMany(loggsModel::class,'userId','idColab');
  }
  public function getLoggsPlanilla(){
   return $this->hasMany(loggsPlanillaModel::class,'id_colab','idColab');
  }

}
