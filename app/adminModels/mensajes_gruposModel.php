<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class mensajes_gruposModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_mensajes_grupos';
  protected $fillable = [
                            'id_admin',
                            'id_empresa',
                            'nombre',
                            'ids_miembros',
                        ];
}
