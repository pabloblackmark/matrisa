<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class companyclientsBranchModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company_clients_branchof';
  protected $fillable = [
      'namebranch',
      'address',
      'latitude',
      'longitude',
      'idClient',
      'phone1',
      'bodega_id'
  ];
}
