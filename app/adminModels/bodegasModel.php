<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class bodegasModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_bodegas';
  protected $fillable = [
                            'nombre',
                            'tipo'
                        ];
}
