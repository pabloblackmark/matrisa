<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class companyclients_jobsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company_clients_jobs';
  protected $fillable = ['idJobCatalog','idClient'];
  public function getJobName(){
   return $this->belongsTo(puestosModel::class, 'idJobCatalog');
  }
  public function getTasks(){
   return $this->hasMany(companyclients_jobstodoModel::class, 'idJob');
  }
  
}
