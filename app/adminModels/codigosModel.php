<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class codigosModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_codigos';
  protected $fillable = [
                            'codigo',
                            'id_admin',
                            'id_provider',
                            'id_factura',
                            'no_factura',
                            'valor',
                            'info',
                            'fecha_pago',
                            'status',
                        ];
}
