<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class cumplesModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_cumples';
  protected $fillable = ['mensaje','imagen'];
}
