<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class tasksModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_tasks';
  protected $fillable = [
                          'id_task_manager',
                          'name',
                          'description',
                          'count'
                      ];

}
