<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class equipoAsignadoModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_equipo_asignado';
  protected $fillable = [
                            'equipo_id',
                            'asignado_a_cliente_id',
                            'asignado_a_colaborador_id',
                            'asignado_a_bodega_id',
                            'origen_bodega_id',
                            'tipo_movimiento',
                            'comentario',
                        ];
}
