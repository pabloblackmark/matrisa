<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class respuestasModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_respuestas';
  protected $fillable = [   
                            'id_mensaje',
                            'de',
                            'para',
                            'mensaje',
                            'status',
                        ];
}
