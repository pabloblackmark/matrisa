<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class stockIngresosModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_stock_ingresos';
  protected $fillable = [
                            'producto_id',
                            'cantidad_medida',
                            'medida_id',
                            'cantidad',
                            'medida',
                            'costo_unitario',
                            'valor_total',
                            'bodega_id',
                            'factura_id',
                        ];
}
