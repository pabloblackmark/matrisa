<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class stockEgresosModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_stock_egresos';
  protected $fillable = [
                            'producto_id',
                            'cantidad_medida',
                            'medida_id',
                            'cantidad',
                            'medida',
                            'costo_unitario',
                            'valor_total',
                            'precio_unitario',
                            'precio_total',
                            'tipo_egreso',
                            'tipo_cobro',
                            'cuotas',
                            'bodega_id',

                            'receptor_cliente_id',
                            'receptor_colaborador_id',
                            'receptor_bodega_id',

                            'cobrar_cliente_id',
                            'cobrar_colaborador_id',                        ];
}
