<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class unidadesModel extends Model
{
  protected $guard = 'admin';
  protected $table = 'admin_unidades';
  protected $fillable = [
                            'nombre',
                            'tipo',
                            'descripcion',
                        ];
}
