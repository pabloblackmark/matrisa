<?php
namespace App\adminModels;

use Illuminate\Database\Eloquent\Model;

class bancosModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_bancos';
  protected $fillable = [
                          'nombre',
                          'razon',
                          'numero',
                      ];

}
