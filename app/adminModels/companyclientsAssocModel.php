<?php
namespace App\adminModels;
use Illuminate\Database\Eloquent\Model;

class companyclientsAssocModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_company_client_assoc';
  protected $fillable = ['idClient','idCompany'];
  public function getClient(){
    return $this->belongsTo(companyclientsModel::class, 'idClient');
  }
  public function getCompany(){
    return $this->belongsTo(companiesModel::class, 'idCompany');
  }
}
