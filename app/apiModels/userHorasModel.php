<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class userHorasModel extends Model
{
  protected $table = 'user_horas';
  protected $fillable = ['id_empresa',
  						          'id_colab',
  						          'id_cliente',
  						          'id_sucursal',
  						          'id_area',
                        'fecha_inicio',
                        'fecha_fin',
                        'tipo',
                        'total',
                        'extra_dia',
                        'extra_noche',
                    	  'extra_especial',
                    	  'extra_mixta',
                    	  'extra_metas',
                    	  'desc_igss',
                        'dia',
                        'noche',
                        'mixta',
                        'metas',
                    	  'estado',
                        'fecha_pago'
                    	];
}
