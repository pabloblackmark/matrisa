<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class logdescuentosModel extends Model
{
  protected $table = 'log_descuentos';
  protected $fillable = ['id_empresa',
  						 'id_colab',
                         'id_desc',
  						 'fecha',
  						 'pago',
  						 'no_cuota',
                         'saldo'
                    	];
}
