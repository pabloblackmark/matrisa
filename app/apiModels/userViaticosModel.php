<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class userViaticosModel extends Model
{
  protected $table = 'user_viaticos';
  protected $fillable = ['id_empresa',
  						          'id_colab',
  						          'id_cliente',
  						          'id_sucursal',
  						          'id_area',
                        'fecha_inicio',
                        'fecha_fin',
                        'tipo',
                        'total',
                        'viaticos',
                        'movil',
                    	  'extras',
                    	  'estado',
                        'fecha_pago'
                    	];
}
