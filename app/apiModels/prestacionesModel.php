<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class prestacionesModel extends Model
{
  protected $table = 'log_prestaciones';
  protected $fillable = ['id_empresa',
  						          'id_colab',
  						          'id_cliente',
  						          'id_sucursal',
  						          'id_area',
                        'tipo',
                        'fecha_pago',
                        'total',
                    	  'estado',
                        'detalle'
                    	];
}
