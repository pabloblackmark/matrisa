<?php
namespace App\apiModels;
use Illuminate\Database\Eloquent\Model;
use App\adminModels\colaboradores;
use App\adminModels\petitionsModel;
class simplepetitionModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_petitions_simplepetitions_save';
  protected $fillable = ['comment','status','idUser','idType','reponse','responseDate'];
  public function getColaborator(){
    return $this->belongsTo(colaboradores::class, 'idUser');
  }
  public function getTypo(){
    return $this->belongsTo(petitionsModel::class, 'idType');
  }
}
