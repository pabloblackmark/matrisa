<?php
namespace App\apiModels;
use Illuminate\Database\Eloquent\Model;
use App\adminModels\colaboradores;
class complaimentsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_petitions_complaintments';
  protected $fillable = ['comment','idUser','status','reponse','responseDate'];
  public function getColaborator(){
    return $this->belongsTo(colaboradores::class, 'idUser');
  }
}
