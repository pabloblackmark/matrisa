<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class loggsPlanillaModel extends Model
{
  protected $table = 'loggs_user_planilla';
  protected $fillable = ['id_empresa',
  						'id_colab',
  						'id_cliente',
  						'id_sucursal',
  						'id_area',
                      'fecha_inicio',
                      'fecha_fin',
                      'tipo',
                      'dias',
                      'sueldo',
                      'total',
                      'bono_ley',
                      'bono_extra',
                    	'bono_antiguedad',
                    	'desc_dia',
                    	'desc_septimo',
                    	'desc_igss',
                    	'descuentos',
                    	'total_bonos',
                    	'total_descuentos',
                    	'estado',
                    	'fecha_pago'
                    	];
}
