<?php
namespace App\apiModels;
use Illuminate\Database\Eloquent\Model;
use App\adminModels\colaboradores;
class ecopetitionModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_petitions_economicpetitions';
  protected $fillable = ['typos','applyTo','otherAplyText','comment','typeOther',
                         'descript','status','idUser','authorized','reponse','responseDate'];
 public function getColaborator(){
   return $this->belongsTo(colaboradores::class, 'idUser');
 }
}
