<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class descuentosModel extends Model
{
  protected $table = 'descuentos';
  protected $fillable = ['id_empresa',
  						          'id_colab',
  						          'tipo',
  						          'monto',
  						          'no_cuotas',
                        'valor_cuota',
                        'primera_cuota',
                        'cuota_actual',
                        'fecha_debito',
                        'debitar_en',
                        'banco',
                    	  'no_credito',
                    	  'fecha_aprobado',
                        'nombre',
                        'detalle',
                        'estado',
                        'saldo'
                    	];
}
