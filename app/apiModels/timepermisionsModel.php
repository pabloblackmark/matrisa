<?php
namespace App\apiModels;
use Illuminate\Database\Eloquent\Model;
use App\adminModels\colaboradores;
class timepermisionsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_petitions_restoretimepermissions';
  protected $fillable = ['dateOut','dateEnter','deteRepEnter','deteRepOut',
                         'description','status','idUser','typos',
                         'reponse','responseDate'];
 public function getColaborator(){
   return $this->belongsTo(colaboradores::class, 'idUser');
 }
}
