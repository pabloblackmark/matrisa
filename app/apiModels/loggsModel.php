<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;
use App\adminModels\colaboradores;
use App\adminModels\companyclientsModel;
use App\adminModels\client_colab_assocModel;
use App\adminModels\horaryModel;

class loggsModel extends Model
{
  protected $table = 'loggs_user';
  protected $fillable = ['latitude','longitud','typo','userId','ipClient',
                          'refrence','ontime','onRange','statusDay','aproved',
                          'delay','userId','idArea','idBranch','idClient','idLink',
                          'hourDay','extraHourDay','extraHourNight','extraHourEspecial',
                          'idFollower','latitudeExit','longitudExit','hourEnter',
                          'hourExit','outTimeEnter','dayWeek','idDayShudle',
                          'extraHourDayMiddle','extraHourNightMiddle','extraPayNight',
                          'extraPayDay','extraPayEspec','extraPayEspec',
                          "befTimeEnter","befTimeEnterTxt","viatics","viaticsAdded","lessTask",
                        "tasks","quantity","priceTask","complTask","dateDay","idCompany","idDay",
                        "aditTask","lessTask","priceAdiTask","baseTasks","dateAproved","dayPay",
                        "extraHourEspecMiddle","viaticsDesc","viaticsMovil","hourEnterBetw","hourExitBetw",
                        "typeday","extraHourMix","extraHourMixMiddle","extraPayMix"
                      ];

   public function getInfoCola(){
     return $this->belongsTo(colaboradores::class, 'userId');
   }
   public function getInfoClient(){
     return $this->belongsTo(companyclientsModel::class, 'idClient');
   }
   public function getClientAsoc(){
     return $this->belongsTo(client_colab_assocModel::class, 'idLink');
   }
   public function getHorary(){
     return $this->belongsTo(horaryModel::class, 'idDay');
   }
}
