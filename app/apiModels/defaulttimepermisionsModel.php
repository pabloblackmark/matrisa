<?php
namespace App\apiModels;
use Illuminate\Database\Eloquent\Model;
use App\adminModels\colaboradores;
class defaulttimepermisionsModel extends Model
{
  protected $guard = 'admin';
  protected $table='admin_petitions_timepermissions';
  protected $fillable = ['datePerm','typos','typeOther','description',
                         'days','status','idUser','reponse','responseDate','lastDate'];
 public function getColaborator(){
   return $this->belongsTo(colaboradores::class, 'idUser');
 }
}
