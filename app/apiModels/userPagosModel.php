<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class userPagosModel extends Model
{
  protected $table = 'user_pagos';
  protected $fillable = ['id_empresa',
  						          'id_cliente',
                        'id_colab',
  						          'id_planilla',
  						          'id_horas',
                        'id_viaticos',
                        'total',
                        'total_planilla',
                        'total_horas',
                        'total_viaticos',
                    	  'estado',
                        'fecha_pago'
                    	];
}
