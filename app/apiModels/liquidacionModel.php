<?php
namespace App\apiModels;

use Illuminate\Database\Eloquent\Model;

class liquidacionModel extends Model
{
  protected $table = 'log_liquidacion';
  protected $fillable = ['id_empresa',
  					    'id_colab',
                        'fecha_pago',
                        'total',
                        'indemnizacion',
                        'aguinaldo',
                        'bono_14',
                        'vacaciones',
                        'salario',
                        'total_bonos',
                        'total_horas',
                        'total_viaticos',
                        'total_descuentos',                       
                        'bonos',
                        'horas',
                        'viaticos',
                        'descuentos',
                    	'estado',
                        'detalle'
                    	];
}
