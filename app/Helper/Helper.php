<?php
namespace App\Helper;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\adminModels\mensajesModel;

class Helper
{
  public static function get_files($filesTo,$disk="s3"){
    return (!empty($filesTo)?Storage::disk($disk)
    ->temporaryUrl($filesTo, now()
    ->addMinutes(60)):null);
  }
  public static function breakWords($string){
    return preg_replace("/<br\s?\/?>/",'',$string);
  }
  public static function utf8D($cad){
    return utf8_decode($cad);
  }
  public static function superAdmin(){
    return (Auth::user()->usersys=='sadmin'?true:false);
  }
  public static function get_country($coun=null ){
    $result = preg_replace("/[^a-zA-Z]+/", "",($coun===null?Auth::user()->country:$coun));
    return $result;
  }
  public static function get_fullcountry(){
    return Auth::user()->country;
  }
  public static function get_idUser(){
    return Auth::user()->id;
  }
  public static function get_platform(){
    if(!self::superAdmin()){
      $is=Auth::user()->get_platform->platform;
    }else{
      $is = null;
    }
    return $is;
  }
  public static function get_env()
  {
    $envsrv=array('horizum.dvp'=>'pro',
                  'dev.horizum.com'=>'pro',
                  'pre.horizum.com'=>'pro',
                  'horizum.com'=>'pro');
    $devnv=$envsrv[str_replace('www.', '', $_SERVER['HTTP_HOST'])];
    return $devnv;
  }
  public static function urlUps(){
    if(self::get_env()=='pro'){
      return 'https://recursos.horizum.com/';
    }else{
      return 'http://pre-recursos.horizum.com/';
    }
  }
  public static function httpVerify($cad){
    if((strpos($cad, 'http') !== false)){
      return $cad;
    }else{
      if(!empty($_SERVER['HTTPS'])
        &&$_SERVER['HTTPS'] != 'off'){
          return 'https://'.$cad;
      }else{
        return 'http://'.$cad;
      }
    }

  }
  public static function redondear_moneda($valor){
      $val = number_format($valor, 2);
      return $val;
  }



  public static function get_notis() {


    $mensajes = mensajesModel::where('de_admin', Auth::user()->id)->orWhere('para_admin', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();


    $mensajes_data = array();

    foreach($mensajes AS $mensaje) {


        if (($mensaje->tipo==0 && $mensaje->status==2) || ($mensaje->tipo==1 && $mensaje->status==0)) {

            $mensaje_data['id'] = $mensaje->id;
            $mensaje_data['de'] = $mensaje->de;
            $mensaje_data['para'] = $mensaje->para;
            $mensaje_data['titulo'] = $mensaje->titulo;
            $mensaje_data['fecha'] = date('d-m-Y h:i',strtotime($mensaje->updated_at));
            $mensaje_data['tipo'] = $mensaje->tipo;
            // $mensaje_data['status'] = $mensaje->status;

            if ( $mensaje->tipo==1 && $mensaje->status==0 && $mensaje->respuestas=='') {

                $mensaje_data['status'] = 'Nuevo';

            } else {
                $mensaje_data['status'] = 'Respuesta';
            }

            $mensajes_data[] = $mensaje_data;

        }

    }


    return $mensajes_data;
  }
  public static function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
  }


}
