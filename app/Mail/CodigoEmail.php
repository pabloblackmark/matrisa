<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CodigoEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        $address = 'sistema@matrisa.com.gt';
        
        $name = 'Matrisa';

        // dd($this->data['message']);

        return $this->view('emails.codigo_full')
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($this->data['subject'])
                    ->with([ 'mensaje' => $this->data['mensaje'],
                             'detalle' => $this->data['detalle']
                          ]);

        return $this->view('mails.codigo_full');
    }
}
