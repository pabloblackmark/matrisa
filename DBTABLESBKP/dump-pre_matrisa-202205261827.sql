--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Ubuntu 12.6-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)

-- Started on 2022-05-26 18:27:37 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3860 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 23235)
-- Name: admin_access; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_access (
    id bigint NOT NULL,
    naccess character varying(100) NOT NULL,
    archaccess character varying(100) NOT NULL,
    iconaccess character varying(100),
    publc character varying(1) DEFAULT '1'::character varying NOT NULL,
    groupacc character varying(100) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_access OWNER TO matrisapre;

--
-- TOC entry 203 (class 1259 OID 23239)
-- Name: admin_access_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_access_id_seq OWNER TO matrisapre;

--
-- TOC entry 3861 (class 0 OID 0)
-- Dependencies: 203
-- Name: admin_access_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_access_id_seq OWNED BY public.admin_access.id;


--
-- TOC entry 204 (class 1259 OID 23241)
-- Name: admin_access_roles; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_access_roles (
    id bigint NOT NULL,
    id_role bigint NOT NULL,
    id_access bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_access_roles OWNER TO matrisapre;

--
-- TOC entry 205 (class 1259 OID 23244)
-- Name: admin_access_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_access_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_access_roles_id_seq OWNER TO matrisapre;

--
-- TOC entry 3862 (class 0 OID 0)
-- Dependencies: 205
-- Name: admin_access_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_access_roles_id_seq OWNED BY public.admin_access_roles.id;


--
-- TOC entry 206 (class 1259 OID 23246)
-- Name: admin_alertas; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_alertas (
    id bigint NOT NULL,
    elemento_id bigint,
    referencia character varying(255) NOT NULL,
    tipo character varying(255),
    mensaje text,
    nombre_modulo text,
    url character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_alertas OWNER TO matrisapre;

--
-- TOC entry 207 (class 1259 OID 23252)
-- Name: admin_alertas_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_alertas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_alertas_id_seq OWNER TO matrisapre;

--
-- TOC entry 3863 (class 0 OID 0)
-- Dependencies: 207
-- Name: admin_alertas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_alertas_id_seq OWNED BY public.admin_alertas.id;


--
-- TOC entry 208 (class 1259 OID 23254)
-- Name: admin_archivos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_archivos (
    id bigint NOT NULL,
    id_file_manager character varying(200),
    name character varying(200),
    description text,
    url character varying(200),
    type character varying(200),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_archivos OWNER TO matrisapre;

--
-- TOC entry 209 (class 1259 OID 23260)
-- Name: admin_archivos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_archivos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_archivos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3864 (class 0 OID 0)
-- Dependencies: 209
-- Name: admin_archivos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_archivos_id_seq OWNED BY public.admin_archivos.id;


--
-- TOC entry 210 (class 1259 OID 23262)
-- Name: admin_bancos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_bancos (
    id bigint NOT NULL,
    nombre character varying(200),
    razon character varying(200),
    numero character varying(200),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_bancos OWNER TO matrisapre;

--
-- TOC entry 211 (class 1259 OID 23268)
-- Name: admin_bancos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_bancos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_bancos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3865 (class 0 OID 0)
-- Dependencies: 211
-- Name: admin_bancos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_bancos_id_seq OWNED BY public.admin_bancos.id;


--
-- TOC entry 212 (class 1259 OID 23270)
-- Name: admin_bodegas; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_bodegas (
    id bigint NOT NULL,
    nombre character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    tipo character varying(255)
);


ALTER TABLE public.admin_bodegas OWNER TO matrisapre;

--
-- TOC entry 213 (class 1259 OID 23276)
-- Name: admin_bodegas_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_bodegas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_bodegas_id_seq OWNER TO matrisapre;

--
-- TOC entry 3866 (class 0 OID 0)
-- Dependencies: 213
-- Name: admin_bodegas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_bodegas_id_seq OWNED BY public.admin_bodegas.id;


--
-- TOC entry 214 (class 1259 OID 23278)
-- Name: admin_categories; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_categories (
    id bigint NOT NULL,
    category character varying(100) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_categories OWNER TO matrisapre;

--
-- TOC entry 215 (class 1259 OID 23281)
-- Name: admin_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_categories_id_seq OWNER TO matrisapre;

--
-- TOC entry 3867 (class 0 OID 0)
-- Dependencies: 215
-- Name: admin_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_categories_id_seq OWNED BY public.admin_categories.id;


--
-- TOC entry 216 (class 1259 OID 23283)
-- Name: admin_changeLog; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public."admin_changeLog" (
    id bigint NOT NULL,
    "user" character varying(200),
    area character varying(200),
    type character varying(200),
    comment text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    element_id bigint
);


ALTER TABLE public."admin_changeLog" OWNER TO matrisapre;

--
-- TOC entry 217 (class 1259 OID 23289)
-- Name: admin_changeLog_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public."admin_changeLog_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."admin_changeLog_id_seq" OWNER TO matrisapre;

--
-- TOC entry 3868 (class 0 OID 0)
-- Dependencies: 217
-- Name: admin_changeLog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public."admin_changeLog_id_seq" OWNED BY public."admin_changeLog".id;


--
-- TOC entry 218 (class 1259 OID 23291)
-- Name: admin_client_colab_assoc; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_client_colab_assoc (
    id bigint NOT NULL,
    "idClient" bigint NOT NULL,
    "idColab" bigint NOT NULL,
    status character varying(10) DEFAULT '0'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "idPuesto" bigint,
    "idBranch" bigint,
    "idArea" bigint,
    tipo character varying(255),
    inicio date,
    fin date
);


ALTER TABLE public.admin_client_colab_assoc OWNER TO matrisapre;

--
-- TOC entry 219 (class 1259 OID 23295)
-- Name: admin_client_colab_assoc_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_client_colab_assoc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_client_colab_assoc_id_seq OWNER TO matrisapre;

--
-- TOC entry 3869 (class 0 OID 0)
-- Dependencies: 219
-- Name: admin_client_colab_assoc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_client_colab_assoc_id_seq OWNED BY public.admin_client_colab_assoc.id;


--
-- TOC entry 220 (class 1259 OID 23297)
-- Name: admin_cobro_colaborador; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_cobro_colaborador (
    id bigint NOT NULL,
    stock_egresos_id bigint,
    producto_id bigint,
    colaborador_id bigint,
    medida_id bigint,
    cantidad bigint,
    precio_total numeric(14,2),
    precio_cuotas numeric(14,2),
    cuotas_iniciales integer,
    cuotas_restantes integer,
    estado character varying(255) DEFAULT 'abierto'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_cobro_colaborador OWNER TO matrisapre;

--
-- TOC entry 221 (class 1259 OID 23301)
-- Name: admin_cobro_colaborador_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_cobro_colaborador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_cobro_colaborador_id_seq OWNER TO matrisapre;

--
-- TOC entry 3870 (class 0 OID 0)
-- Dependencies: 221
-- Name: admin_cobro_colaborador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_cobro_colaborador_id_seq OWNED BY public.admin_cobro_colaborador.id;


--
-- TOC entry 222 (class 1259 OID 23303)
-- Name: admin_codigos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_codigos (
    id bigint NOT NULL,
    codigo character varying(255) NOT NULL,
    id_provider character varying(255) NOT NULL,
    id_admin character varying(255) NOT NULL,
    id_factura character varying(255) NOT NULL,
    no_factura character varying(255) NOT NULL,
    valor character varying(255) NOT NULL,
    info text,
    fecha_pago date,
    status character varying(1) DEFAULT '0'::character varying,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_codigos OWNER TO matrisapre;

--
-- TOC entry 223 (class 1259 OID 23310)
-- Name: admin_codigos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_codigos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_codigos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3871 (class 0 OID 0)
-- Dependencies: 223
-- Name: admin_codigos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_codigos_id_seq OWNED BY public.admin_codigos.id;


--
-- TOC entry 224 (class 1259 OID 23312)
-- Name: admin_colaboradores; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_colaboradores (
    id bigint NOT NULL,
    nombres character varying(100),
    apellidos character varying(100),
    foto text,
    fecha date,
    email character varying(100),
    dpi character varying(50),
    nit character varying(50),
    iggs character varying(50),
    irtra character varying(50),
    intecap character varying(50),
    licencia character varying(50),
    fecha_salud date,
    fecha_pulmones date,
    genero character varying(50),
    nivel_academico character varying(100),
    nacionalidad character varying(50),
    estado_civil character varying(50),
    conyuge character varying(150),
    hijos smallint DEFAULT '0'::smallint NOT NULL,
    profesion character varying(100),
    tel1 character varying(18),
    tel2 character varying(18),
    direccion text,
    departamento character varying(100),
    municipio character varying(100),
    contacto1 character varying(200),
    tel_con1 character varying(18),
    parentesco_con1 character varying(100),
    contacto2 character varying(200),
    tel_con2 character varying(18),
    parentesco_con2 character varying(100),
    tipo_sangre character varying(50),
    contrato character varying(200),
    puesto character varying(100),
    sueldo double precision,
    bono_extra double precision,
    antiguedad double precision,
    bono_ley double precision,
    hora_extra_diurna double precision,
    hora_extra_nocturna double precision,
    hora_extra_especial double precision,
    viaticos double precision,
    movil double precision,
    hora_normal double precision,
    hora_normal_nocturna double precision,
    tipo_pago character varying(200),
    banco character varying(50),
    pago_sueldo character varying(50),
    otros_pagos character varying(50),
    puntos1 double precision,
    puntos2 double precision,
    puntos3 double precision,
    puntos4 double precision,
    estatus character varying(50),
    fecha_ingreso date,
    observaciones text,
    "idUser" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    vence_licencia date,
    cuenta1 character varying(100),
    cuenta2 character varying(100),
    cuenta3 character varying(100),
    firma character varying(255),
    id_file_manager character varying(255),
    password character varying(100),
    "idWRegister" integer,
    hora_extra_mixta double precision,
    sueldo_pactado double precision,
    zona character varying(255),
    colonia character varying(255),
    fecha_baja date
);


ALTER TABLE public.admin_colaboradores OWNER TO matrisapre;

--
-- TOC entry 225 (class 1259 OID 23319)
-- Name: admin_colaboradores_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_colaboradores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_colaboradores_id_seq OWNER TO matrisapre;

--
-- TOC entry 3872 (class 0 OID 0)
-- Dependencies: 225
-- Name: admin_colaboradores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_colaboradores_id_seq OWNED BY public.admin_colaboradores.id;


--
-- TOC entry 226 (class 1259 OID 23321)
-- Name: admin_colors; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_colors (
    id bigint NOT NULL,
    namecolor character varying(60) NOT NULL,
    category character varying(35) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_colors OWNER TO matrisapre;

--
-- TOC entry 227 (class 1259 OID 23324)
-- Name: admin_colors_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_colors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_colors_id_seq OWNER TO matrisapre;

--
-- TOC entry 3873 (class 0 OID 0)
-- Dependencies: 227
-- Name: admin_colors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_colors_id_seq OWNED BY public.admin_colors.id;


--
-- TOC entry 228 (class 1259 OID 23326)
-- Name: admin_company; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    company character varying(255) NOT NULL,
    nit character varying(25) NOT NULL,
    legalrepresent character varying(255) NOT NULL,
    logo text,
    phone1 character varying(255) NOT NULL,
    phone2 character varying(255) NOT NULL,
    depto character varying(255) NOT NULL,
    municip character varying(255) NOT NULL,
    addess character varying(255) NOT NULL,
    "idUser" bigint,
    email character varying(255) NOT NULL,
    igss character varying(255) NOT NULL,
    dpi character varying(255) NOT NULL,
    estado_civil character varying(255) NOT NULL,
    genero character varying(255) NOT NULL,
    calculo_sobre character varying(255) NOT NULL,
    fecha_inicio date,
    nacionalidad character varying(100),
    vecino character varying(200),
    iggs_patronal character varying(100),
    irtra character varying(100),
    iggs_laboral character varying(100),
    intecap character varying(200),
    bono_extra character varying(200),
    horas_extra character varying(200),
    productividad character varying(200),
    id_file_manager character varying(200),
    "idWRegister" integer,
    bodega_id bigint,
    fecha_nacimiento date
);


ALTER TABLE public.admin_company OWNER TO matrisapre;

--
-- TOC entry 229 (class 1259 OID 23332)
-- Name: admin_company_client_assoc; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company_client_assoc (
    id bigint NOT NULL,
    "idClient" bigint NOT NULL,
    "idCompany" bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_company_client_assoc OWNER TO matrisapre;

--
-- TOC entry 230 (class 1259 OID 23335)
-- Name: admin_company_client_assoc_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_client_assoc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_client_assoc_id_seq OWNER TO matrisapre;

--
-- TOC entry 3874 (class 0 OID 0)
-- Dependencies: 230
-- Name: admin_company_client_assoc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_client_assoc_id_seq OWNED BY public.admin_company_client_assoc.id;


--
-- TOC entry 231 (class 1259 OID 23337)
-- Name: admin_company_clients; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company_clients (
    id bigint NOT NULL,
    "clientName" character varying(255),
    nit character varying(25),
    "contactName" character varying(255),
    email character varying(255),
    phone1 character varying(255),
    phone2 character varying(255),
    depto character varying(255),
    municip character varying(255),
    addess character varying(255),
    credit integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    nombre_factura character varying(200),
    observaciones text,
    vma integer,
    venta_sobregiro boolean DEFAULT false NOT NULL,
    id_file_manager character varying(200),
    area character varying(200),
    id_area character varying(200)
);


ALTER TABLE public.admin_company_clients OWNER TO matrisapre;

--
-- TOC entry 232 (class 1259 OID 23344)
-- Name: admin_company_clients_branch_area; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company_clients_branch_area (
    id bigint NOT NULL,
    "idBranch" character varying(200),
    name character varying(200),
    description text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_company_clients_branch_area OWNER TO matrisapre;

--
-- TOC entry 233 (class 1259 OID 23350)
-- Name: admin_company_clients_branch_area_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_clients_branch_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_clients_branch_area_id_seq OWNER TO matrisapre;

--
-- TOC entry 3875 (class 0 OID 0)
-- Dependencies: 233
-- Name: admin_company_clients_branch_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_clients_branch_area_id_seq OWNED BY public.admin_company_clients_branch_area.id;


--
-- TOC entry 234 (class 1259 OID 23352)
-- Name: admin_company_clients_branchof; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company_clients_branchof (
    id bigint NOT NULL,
    namebranch character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    latitude character varying(255) NOT NULL,
    longitude character varying(255) NOT NULL,
    phone1 character varying(255) NOT NULL,
    "idClient" bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    bodega_id bigint NOT NULL
);


ALTER TABLE public.admin_company_clients_branchof OWNER TO matrisapre;

--
-- TOC entry 235 (class 1259 OID 23358)
-- Name: admin_company_clients_branchof_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_clients_branchof_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_clients_branchof_id_seq OWNER TO matrisapre;

--
-- TOC entry 3876 (class 0 OID 0)
-- Dependencies: 235
-- Name: admin_company_clients_branchof_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_clients_branchof_id_seq OWNED BY public.admin_company_clients_branchof.id;


--
-- TOC entry 236 (class 1259 OID 23360)
-- Name: admin_company_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_clients_id_seq OWNER TO matrisapre;

--
-- TOC entry 3877 (class 0 OID 0)
-- Dependencies: 236
-- Name: admin_company_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_clients_id_seq OWNED BY public.admin_company_clients.id;


--
-- TOC entry 237 (class 1259 OID 23362)
-- Name: admin_company_clients_jobs; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company_clients_jobs (
    id bigint NOT NULL,
    "idJobCatalog" bigint NOT NULL,
    "idClientComp" bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_company_clients_jobs OWNER TO matrisapre;

--
-- TOC entry 238 (class 1259 OID 23365)
-- Name: admin_company_clients_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_clients_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_clients_jobs_id_seq OWNER TO matrisapre;

--
-- TOC entry 3878 (class 0 OID 0)
-- Dependencies: 238
-- Name: admin_company_clients_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_clients_jobs_id_seq OWNED BY public.admin_company_clients_jobs.id;


--
-- TOC entry 239 (class 1259 OID 23367)
-- Name: admin_company_clients_jobs_todo; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company_clients_jobs_todo (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    "idJob" bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_company_clients_jobs_todo OWNER TO matrisapre;

--
-- TOC entry 240 (class 1259 OID 23370)
-- Name: admin_company_clients_jobs_todo_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_clients_jobs_todo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_clients_jobs_todo_id_seq OWNER TO matrisapre;

--
-- TOC entry 3879 (class 0 OID 0)
-- Dependencies: 240
-- Name: admin_company_clients_jobs_todo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_clients_jobs_todo_id_seq OWNED BY public.admin_company_clients_jobs_todo.id;


--
-- TOC entry 241 (class 1259 OID 23372)
-- Name: admin_company_colab_assoc; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_company_colab_assoc (
    id bigint NOT NULL,
    "idCompany" bigint NOT NULL,
    "idColab" bigint NOT NULL,
    status character varying(10) DEFAULT '0'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_company_colab_assoc OWNER TO matrisapre;

--
-- TOC entry 242 (class 1259 OID 23376)
-- Name: admin_company_colab_assoc_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_colab_assoc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_colab_assoc_id_seq OWNER TO matrisapre;

--
-- TOC entry 3880 (class 0 OID 0)
-- Dependencies: 242
-- Name: admin_company_colab_assoc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_colab_assoc_id_seq OWNED BY public.admin_company_colab_assoc.id;


--
-- TOC entry 243 (class 1259 OID 23378)
-- Name: admin_company_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_company_id_seq OWNER TO matrisapre;

--
-- TOC entry 3881 (class 0 OID 0)
-- Dependencies: 243
-- Name: admin_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_company_id_seq OWNED BY public.admin_company.id;


--
-- TOC entry 244 (class 1259 OID 23380)
-- Name: admin_contratos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_contratos (
    id bigint NOT NULL,
    name character varying(200),
    description text,
    type character varying(200),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_contratos OWNER TO matrisapre;

--
-- TOC entry 245 (class 1259 OID 23386)
-- Name: admin_contratos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_contratos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_contratos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3882 (class 0 OID 0)
-- Dependencies: 245
-- Name: admin_contratos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_contratos_id_seq OWNED BY public.admin_contratos.id;


--
-- TOC entry 246 (class 1259 OID 23388)
-- Name: admin_conversiones; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_conversiones (
    id bigint NOT NULL,
    unidad_inicial_id bigint,
    unidad_final_id bigint,
    operacion character varying(255) NOT NULL,
    valor numeric(14,2),
    descripcion text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_conversiones OWNER TO matrisapre;

--
-- TOC entry 247 (class 1259 OID 23394)
-- Name: admin_conversiones_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_conversiones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_conversiones_id_seq OWNER TO matrisapre;

--
-- TOC entry 3883 (class 0 OID 0)
-- Dependencies: 247
-- Name: admin_conversiones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_conversiones_id_seq OWNED BY public.admin_conversiones.id;


--
-- TOC entry 248 (class 1259 OID 23396)
-- Name: admin_cotis; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_cotis (
    id bigint NOT NULL,
    no character varying(255) NOT NULL,
    fecha date NOT NULL,
    id_empresa character varying(255) NOT NULL,
    empresa character varying(255) NOT NULL,
    id_cliente character varying(255) NOT NULL,
    cliente character varying(255) NOT NULL,
    id_sede character varying(255) NOT NULL,
    sede character varying(255) NOT NULL,
    nit character varying(255),
    detalle text NOT NULL,
    total character varying(255) NOT NULL,
    firma character varying(255) NOT NULL,
    id_admin character varying(255) NOT NULL,
    status character varying(1) DEFAULT '0'::character varying,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_cotis OWNER TO matrisapre;

--
-- TOC entry 249 (class 1259 OID 23403)
-- Name: admin_cotis_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_cotis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_cotis_id_seq OWNER TO matrisapre;

--
-- TOC entry 3884 (class 0 OID 0)
-- Dependencies: 249
-- Name: admin_cotis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_cotis_id_seq OWNED BY public.admin_cotis.id;


--
-- TOC entry 250 (class 1259 OID 23405)
-- Name: admin_cumples; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_cumples (
    id bigint NOT NULL,
    mensaje text NOT NULL,
    imagen character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_cumples OWNER TO matrisapre;

--
-- TOC entry 251 (class 1259 OID 23411)
-- Name: admin_cumples_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_cumples_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_cumples_id_seq OWNER TO matrisapre;

--
-- TOC entry 3885 (class 0 OID 0)
-- Dependencies: 251
-- Name: admin_cumples_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_cumples_id_seq OWNED BY public.admin_cumples.id;


--
-- TOC entry 252 (class 1259 OID 23413)
-- Name: admin_documentos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_documentos (
    id bigint NOT NULL,
    name character varying(200),
    description text,
    type character varying(200),
    body text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_documentos OWNER TO matrisapre;

--
-- TOC entry 253 (class 1259 OID 23419)
-- Name: admin_documentos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_documentos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_documentos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3886 (class 0 OID 0)
-- Dependencies: 253
-- Name: admin_documentos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_documentos_id_seq OWNED BY public.admin_documentos.id;


--
-- TOC entry 254 (class 1259 OID 23421)
-- Name: admin_equipo_asignado; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_equipo_asignado (
    id bigint NOT NULL,
    equipo_id bigint,
    asignado_a_bodega_id bigint,
    asignado_a_cliente_id bigint,
    asignado_a_colaborador_id bigint,
    origen_bodega_id bigint,
    tipo_movimiento character varying(255),
    fecha_movimiento date,
    comentario text,
    usuario character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_equipo_asignado OWNER TO matrisapre;

--
-- TOC entry 255 (class 1259 OID 23427)
-- Name: admin_equipo_asignado_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_equipo_asignado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_equipo_asignado_id_seq OWNER TO matrisapre;

--
-- TOC entry 3887 (class 0 OID 0)
-- Dependencies: 255
-- Name: admin_equipo_asignado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_equipo_asignado_id_seq OWNED BY public.admin_equipo_asignado.id;


--
-- TOC entry 256 (class 1259 OID 23429)
-- Name: admin_equipos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_equipos (
    id bigint NOT NULL,
    codigo character varying(255),
    nombre character varying(255),
    descripcion text,
    categoria_id bigint,
    sub_categoria_id bigint,
    medida_id bigint,
    precio_cliente_unidad numeric(14,2),
    porcentaje_depreciacion numeric(14,2),
    estado character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_equipos OWNER TO matrisapre;

--
-- TOC entry 257 (class 1259 OID 23435)
-- Name: admin_equipos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_equipos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_equipos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3888 (class 0 OID 0)
-- Dependencies: 257
-- Name: admin_equipos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_equipos_id_seq OWNED BY public.admin_equipos.id;


--
-- TOC entry 258 (class 1259 OID 23437)
-- Name: admin_facturas; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_facturas (
    id bigint NOT NULL,
    proveedor_id bigint,
    numero character varying(255),
    serie character varying(255),
    total numeric(14,2),
    fecha date,
    info text,
    estado character varying(255),
    bodega_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    tipo character varying(255) DEFAULT 'producto'::character varying
);


ALTER TABLE public.admin_facturas OWNER TO matrisapre;

--
-- TOC entry 259 (class 1259 OID 23444)
-- Name: admin_facturas_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_facturas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_facturas_id_seq OWNER TO matrisapre;

--
-- TOC entry 3889 (class 0 OID 0)
-- Dependencies: 259
-- Name: admin_facturas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_facturas_id_seq OWNED BY public.admin_facturas.id;


--
-- TOC entry 260 (class 1259 OID 23446)
-- Name: admin_horary; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_horary (
    id bigint NOT NULL,
    id_horary_manager character varying(200),
    day character varying(200),
    entry time(0) without time zone,
    exit time(0) without time zone,
    type character varying(200),
    date date,
    description character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_horary OWNER TO matrisapre;

--
-- TOC entry 261 (class 1259 OID 23452)
-- Name: admin_horary_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_horary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_horary_id_seq OWNER TO matrisapre;

--
-- TOC entry 3890 (class 0 OID 0)
-- Dependencies: 261
-- Name: admin_horary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_horary_id_seq OWNED BY public.admin_horary.id;


--
-- TOC entry 262 (class 1259 OID 23454)
-- Name: admin_loggs_user_paiments; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_loggs_user_paiments (
    id bigint NOT NULL,
    "datePay" character varying(255),
    "dayPay" character varying(255),
    "viaticTotal" character varying(255),
    "viaticAdd" text,
    descript text,
    status character varying(2) DEFAULT '0'::character varying,
    reponse text,
    "responseDate" date,
    authorized character varying(255),
    "idLoogs" bigint,
    "idUser" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_loggs_user_paiments OWNER TO matrisapre;

--
-- TOC entry 263 (class 1259 OID 23461)
-- Name: admin_loggs_user_paiments_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_loggs_user_paiments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_loggs_user_paiments_id_seq OWNER TO matrisapre;

--
-- TOC entry 3891 (class 0 OID 0)
-- Dependencies: 263
-- Name: admin_loggs_user_paiments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_loggs_user_paiments_id_seq OWNED BY public.admin_loggs_user_paiments.id;


--
-- TOC entry 264 (class 1259 OID 23463)
-- Name: admin_mensajes; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_mensajes (
    id bigint NOT NULL,
    de_admin character varying(255),
    para_user character varying(255),
    de_user character varying(255),
    para_admin character varying(255),
    de character varying(255) NOT NULL,
    para character varying(255) NOT NULL,
    titulo character varying(255) NOT NULL,
    mensaje text NOT NULL,
    tipo character varying(1) DEFAULT '0'::character varying,
    status character varying(1) DEFAULT '0'::character varying,
    respuestas text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_mensajes OWNER TO matrisapre;

--
-- TOC entry 265 (class 1259 OID 23471)
-- Name: admin_mensajes_grupos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_mensajes_grupos (
    id bigint NOT NULL,
    id_admin character varying(255) NOT NULL,
    id_empresa character varying(255),
    nombre character varying(255) NOT NULL,
    ids_miembros character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_mensajes_grupos OWNER TO matrisapre;

--
-- TOC entry 266 (class 1259 OID 23477)
-- Name: admin_mensajes_grupos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_mensajes_grupos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_mensajes_grupos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3892 (class 0 OID 0)
-- Dependencies: 266
-- Name: admin_mensajes_grupos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_mensajes_grupos_id_seq OWNED BY public.admin_mensajes_grupos.id;


--
-- TOC entry 267 (class 1259 OID 23479)
-- Name: admin_mensajes_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_mensajes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_mensajes_id_seq OWNER TO matrisapre;

--
-- TOC entry 3893 (class 0 OID 0)
-- Dependencies: 267
-- Name: admin_mensajes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_mensajes_id_seq OWNED BY public.admin_mensajes.id;


--
-- TOC entry 268 (class 1259 OID 23481)
-- Name: admin_petitions_complaintments; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_petitions_complaintments (
    id bigint NOT NULL,
    comment text,
    status character varying(2) DEFAULT '0'::character varying,
    reponse text,
    "responseDate" date,
    "idUser" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_petitions_complaintments OWNER TO matrisapre;

--
-- TOC entry 269 (class 1259 OID 23488)
-- Name: admin_petitions_complaintments_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_petitions_complaintments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_petitions_complaintments_id_seq OWNER TO matrisapre;

--
-- TOC entry 3894 (class 0 OID 0)
-- Dependencies: 269
-- Name: admin_petitions_complaintments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_petitions_complaintments_id_seq OWNED BY public.admin_petitions_complaintments.id;


--
-- TOC entry 270 (class 1259 OID 23490)
-- Name: admin_petitions_economicpetitions; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_petitions_economicpetitions (
    id bigint NOT NULL,
    typos character varying(255),
    "applyTo" character varying(255),
    "otherAplyText" character varying(255),
    comment text,
    descript text,
    status character varying(2) DEFAULT '0'::character varying,
    reponse text,
    "responseDate" date,
    authorized character varying(255),
    "idUser" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_petitions_economicpetitions OWNER TO matrisapre;

--
-- TOC entry 271 (class 1259 OID 23497)
-- Name: admin_petitions_economicpetitions_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_petitions_economicpetitions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_petitions_economicpetitions_id_seq OWNER TO matrisapre;

--
-- TOC entry 3895 (class 0 OID 0)
-- Dependencies: 271
-- Name: admin_petitions_economicpetitions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_petitions_economicpetitions_id_seq OWNED BY public.admin_petitions_economicpetitions.id;


--
-- TOC entry 272 (class 1259 OID 23499)
-- Name: admin_petitions_restoretimepermissions; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_petitions_restoretimepermissions (
    id bigint NOT NULL,
    "dateOut" date,
    "dateEnter" date,
    "deteRepEnter" date,
    "deteRepOut" date,
    description text,
    typos character varying(255),
    daysapply character varying(6),
    status character varying(2) DEFAULT '0'::character varying,
    reponse text,
    "responseDate" date,
    "idUser" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_petitions_restoretimepermissions OWNER TO matrisapre;

--
-- TOC entry 273 (class 1259 OID 23506)
-- Name: admin_petitions_restoretimepermissions_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_petitions_restoretimepermissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_petitions_restoretimepermissions_id_seq OWNER TO matrisapre;

--
-- TOC entry 3896 (class 0 OID 0)
-- Dependencies: 273
-- Name: admin_petitions_restoretimepermissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_petitions_restoretimepermissions_id_seq OWNED BY public.admin_petitions_restoretimepermissions.id;


--
-- TOC entry 274 (class 1259 OID 23508)
-- Name: admin_petitions_simplepetitions; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_petitions_simplepetitions (
    id bigint NOT NULL,
    name character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_petitions_simplepetitions OWNER TO matrisapre;

--
-- TOC entry 275 (class 1259 OID 23511)
-- Name: admin_petitions_simplepetitions_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_petitions_simplepetitions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_petitions_simplepetitions_id_seq OWNER TO matrisapre;

--
-- TOC entry 3897 (class 0 OID 0)
-- Dependencies: 275
-- Name: admin_petitions_simplepetitions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_petitions_simplepetitions_id_seq OWNED BY public.admin_petitions_simplepetitions.id;


--
-- TOC entry 276 (class 1259 OID 23513)
-- Name: admin_petitions_simplepetitions_save; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_petitions_simplepetitions_save (
    id bigint NOT NULL,
    comment text,
    status character varying(2) DEFAULT '0'::character varying,
    reponse text,
    "responseDate" date,
    "idUser" bigint,
    "idType" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_petitions_simplepetitions_save OWNER TO matrisapre;

--
-- TOC entry 277 (class 1259 OID 23520)
-- Name: admin_petitions_simplepetitions_save_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_petitions_simplepetitions_save_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_petitions_simplepetitions_save_id_seq OWNER TO matrisapre;

--
-- TOC entry 3898 (class 0 OID 0)
-- Dependencies: 277
-- Name: admin_petitions_simplepetitions_save_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_petitions_simplepetitions_save_id_seq OWNED BY public.admin_petitions_simplepetitions_save.id;


--
-- TOC entry 278 (class 1259 OID 23522)
-- Name: admin_petitions_timepermissions; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_petitions_timepermissions (
    id bigint NOT NULL,
    "datePerm" date,
    typos character varying(255),
    "typeOther" character varying(255),
    description text,
    days integer,
    status character varying(2) DEFAULT '0'::character varying,
    reponse text,
    "responseDate" date,
    "idUser" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "lastDate" date
);


ALTER TABLE public.admin_petitions_timepermissions OWNER TO matrisapre;

--
-- TOC entry 279 (class 1259 OID 23529)
-- Name: admin_petitions_timepermissions_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_petitions_timepermissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_petitions_timepermissions_id_seq OWNER TO matrisapre;

--
-- TOC entry 3899 (class 0 OID 0)
-- Dependencies: 279
-- Name: admin_petitions_timepermissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_petitions_timepermissions_id_seq OWNED BY public.admin_petitions_timepermissions.id;


--
-- TOC entry 280 (class 1259 OID 23531)
-- Name: admin_productos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_productos (
    id bigint NOT NULL,
    codigo character varying(255),
    nombre character varying(255),
    descripcion text,
    categoria_id bigint,
    sub_categoria_id bigint,
    medida_final_id bigint,
    cantidad_minima_aceptable bigint,
    porcentaje_ganancia numeric(14,2),
    precio_cliente numeric(14,2),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_productos OWNER TO matrisapre;

--
-- TOC entry 281 (class 1259 OID 23537)
-- Name: admin_productos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_productos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_productos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3900 (class 0 OID 0)
-- Dependencies: 281
-- Name: admin_productos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_productos_id_seq OWNED BY public.admin_productos.id;


--
-- TOC entry 282 (class 1259 OID 23539)
-- Name: admin_products; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_products (
    id bigint NOT NULL,
    title character varying(60) NOT NULL,
    code character varying(60) NOT NULL,
    lot character varying(60) NOT NULL,
    cost character varying(60) NOT NULL,
    min_dispatch integer NOT NULL,
    max_dispatch integer NOT NULL,
    min_stock integer NOT NULL,
    provider character varying(100),
    price character varying(60) NOT NULL,
    "dateIn" date NOT NULL,
    images text,
    description text,
    sizes text,
    colors text,
    category character varying(15) NOT NULL,
    subcategory character varying(15) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_products OWNER TO matrisapre;

--
-- TOC entry 283 (class 1259 OID 23545)
-- Name: admin_products_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_products_id_seq OWNER TO matrisapre;

--
-- TOC entry 3901 (class 0 OID 0)
-- Dependencies: 283
-- Name: admin_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_products_id_seq OWNED BY public.admin_products.id;


--
-- TOC entry 284 (class 1259 OID 23547)
-- Name: admin_providers; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_providers (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    direccion text NOT NULL,
    nit character varying(20) NOT NULL,
    contacto text NOT NULL,
    telefono character varying(20) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    email character varying(255)
);


ALTER TABLE public.admin_providers OWNER TO matrisapre;

--
-- TOC entry 285 (class 1259 OID 23553)
-- Name: admin_providers_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_providers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_providers_id_seq OWNER TO matrisapre;

--
-- TOC entry 3902 (class 0 OID 0)
-- Dependencies: 285
-- Name: admin_providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_providers_id_seq OWNED BY public.admin_providers.id;


--
-- TOC entry 286 (class 1259 OID 23555)
-- Name: admin_puestos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_puestos (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "payDT" integer NOT NULL,
    "payNT" integer NOT NULL,
    "payEDT" integer NOT NULL,
    "payENT" integer NOT NULL,
    "extraPay" integer NOT NULL,
    "perDiem" integer NOT NULL,
    movil integer NOT NULL,
    points integer NOT NULL,
    id_task_manager character varying(255),
    id_horary_manager character varying(255)
);


ALTER TABLE public.admin_puestos OWNER TO matrisapre;

--
-- TOC entry 287 (class 1259 OID 23561)
-- Name: admin_puestos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_puestos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_puestos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3903 (class 0 OID 0)
-- Dependencies: 287
-- Name: admin_puestos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_puestos_id_seq OWNED BY public.admin_puestos.id;


--
-- TOC entry 288 (class 1259 OID 23563)
-- Name: admin_roles; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_roles (
    id bigint NOT NULL,
    "nameRole" character varying(40) NOT NULL,
    country character varying(10) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_roles OWNER TO matrisapre;

--
-- TOC entry 289 (class 1259 OID 23566)
-- Name: admin_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_roles_id_seq OWNER TO matrisapre;

--
-- TOC entry 3904 (class 0 OID 0)
-- Dependencies: 289
-- Name: admin_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_roles_id_seq OWNED BY public.admin_roles.id;


--
-- TOC entry 290 (class 1259 OID 23568)
-- Name: admin_sizes; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_sizes (
    id bigint NOT NULL,
    namesize character varying(60) NOT NULL,
    category character varying(35) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_sizes OWNER TO matrisapre;

--
-- TOC entry 291 (class 1259 OID 23571)
-- Name: admin_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_sizes_id_seq OWNER TO matrisapre;

--
-- TOC entry 3905 (class 0 OID 0)
-- Dependencies: 291
-- Name: admin_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_sizes_id_seq OWNED BY public.admin_sizes.id;


--
-- TOC entry 292 (class 1259 OID 23573)
-- Name: admin_solicitudes; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_solicitudes (
    id bigint NOT NULL,
    tipo character varying(100) NOT NULL,
    descripcion text NOT NULL,
    reposicion character varying(100) NOT NULL,
    descuento character varying(100) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_solicitudes OWNER TO matrisapre;

--
-- TOC entry 293 (class 1259 OID 23579)
-- Name: admin_solicitudes_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_solicitudes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_solicitudes_id_seq OWNER TO matrisapre;

--
-- TOC entry 3906 (class 0 OID 0)
-- Dependencies: 293
-- Name: admin_solicitudes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_solicitudes_id_seq OWNED BY public.admin_solicitudes.id;


--
-- TOC entry 294 (class 1259 OID 23581)
-- Name: admin_stock; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_stock (
    id bigint NOT NULL,
    producto_id bigint,
    cantidad bigint,
    medida_id bigint,
    costo_unitario numeric(14,2),
    valor_total numeric(14,2),
    bodega_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_stock OWNER TO matrisapre;

--
-- TOC entry 295 (class 1259 OID 23584)
-- Name: admin_stock_egresos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_stock_egresos (
    id bigint NOT NULL,
    producto_id bigint,
    cantidad_medida bigint,
    medida_id bigint,
    cantidad bigint,
    medida character varying(255),
    costo_unitario numeric(14,2),
    valor_total numeric(14,2),
    precio_unitario numeric(14,2),
    precio_total numeric(14,2),
    tipo_egreso character varying(255),
    tipo_cobro character varying(255),
    cuotas integer,
    bodega_id bigint,
    receptor_cliente_id bigint,
    receptor_colaborador_id bigint,
    receptor_bodega_id bigint,
    cobrar_cliente_id bigint,
    cobrar_colaborador_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_stock_egresos OWNER TO matrisapre;

--
-- TOC entry 296 (class 1259 OID 23590)
-- Name: admin_stock_egresos_equipo; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_stock_egresos_equipo (
    id bigint NOT NULL,
    equipo_id bigint,
    cantidad bigint,
    costo_unidad numeric(14,2),
    costo_total numeric(14,2),
    precio_total numeric(14,2),
    bodega_id bigint,
    cargo_a_cliente_id bigint,
    cargo_a_colaborador_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_stock_egresos_equipo OWNER TO matrisapre;

--
-- TOC entry 297 (class 1259 OID 23593)
-- Name: admin_stock_egresos_equipo_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_stock_egresos_equipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_stock_egresos_equipo_id_seq OWNER TO matrisapre;

--
-- TOC entry 3907 (class 0 OID 0)
-- Dependencies: 297
-- Name: admin_stock_egresos_equipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_stock_egresos_equipo_id_seq OWNED BY public.admin_stock_egresos_equipo.id;


--
-- TOC entry 298 (class 1259 OID 23595)
-- Name: admin_stock_egresos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_stock_egresos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_stock_egresos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3908 (class 0 OID 0)
-- Dependencies: 298
-- Name: admin_stock_egresos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_stock_egresos_id_seq OWNED BY public.admin_stock_egresos.id;


--
-- TOC entry 299 (class 1259 OID 23597)
-- Name: admin_stock_equipos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_stock_equipos (
    id bigint NOT NULL,
    equipo_id bigint,
    cantidad bigint,
    medida_id bigint,
    costo_unitario numeric(14,2),
    valor_total numeric(14,2),
    bodega_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    estado character varying(255) DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.admin_stock_equipos OWNER TO matrisapre;

--
-- TOC entry 300 (class 1259 OID 23601)
-- Name: admin_stock_equipos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_stock_equipos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_stock_equipos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3909 (class 0 OID 0)
-- Dependencies: 300
-- Name: admin_stock_equipos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_stock_equipos_id_seq OWNED BY public.admin_stock_equipos.id;


--
-- TOC entry 301 (class 1259 OID 23603)
-- Name: admin_stock_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_stock_id_seq OWNER TO matrisapre;

--
-- TOC entry 3910 (class 0 OID 0)
-- Dependencies: 301
-- Name: admin_stock_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_stock_id_seq OWNED BY public.admin_stock.id;


--
-- TOC entry 302 (class 1259 OID 23605)
-- Name: admin_stock_ingresos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_stock_ingresos (
    id bigint NOT NULL,
    producto_id bigint,
    cantidad_medida bigint,
    medida_id bigint,
    cantidad bigint,
    medida character varying(255),
    costo_unitario numeric(14,2),
    valor_total numeric(14,2),
    bodega_id bigint,
    factura_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_stock_ingresos OWNER TO matrisapre;

--
-- TOC entry 303 (class 1259 OID 23608)
-- Name: admin_stock_ingresos_equipo; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_stock_ingresos_equipo (
    id bigint NOT NULL,
    equipo_id bigint,
    cantidad bigint,
    costo_unidad numeric(16,2),
    costo_total numeric(16,2),
    bodega_id bigint,
    factura_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_stock_ingresos_equipo OWNER TO matrisapre;

--
-- TOC entry 304 (class 1259 OID 23611)
-- Name: admin_stock_ingresos_equipo_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_stock_ingresos_equipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_stock_ingresos_equipo_id_seq OWNER TO matrisapre;

--
-- TOC entry 3911 (class 0 OID 0)
-- Dependencies: 304
-- Name: admin_stock_ingresos_equipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_stock_ingresos_equipo_id_seq OWNED BY public.admin_stock_ingresos_equipo.id;


--
-- TOC entry 305 (class 1259 OID 23613)
-- Name: admin_stock_ingresos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_stock_ingresos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_stock_ingresos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3912 (class 0 OID 0)
-- Dependencies: 305
-- Name: admin_stock_ingresos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_stock_ingresos_id_seq OWNED BY public.admin_stock_ingresos.id;


--
-- TOC entry 306 (class 1259 OID 23615)
-- Name: admin_subCategories; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public."admin_subCategories" (
    id bigint NOT NULL,
    "subCategory" character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    category_id bigint NOT NULL
);


ALTER TABLE public."admin_subCategories" OWNER TO matrisapre;

--
-- TOC entry 307 (class 1259 OID 23618)
-- Name: admin_subCategories_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public."admin_subCategories_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."admin_subCategories_id_seq" OWNER TO matrisapre;

--
-- TOC entry 3913 (class 0 OID 0)
-- Dependencies: 307
-- Name: admin_subCategories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public."admin_subCategories_id_seq" OWNED BY public."admin_subCategories".id;


--
-- TOC entry 308 (class 1259 OID 23620)
-- Name: admin_tasks; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_tasks (
    id bigint NOT NULL,
    id_task_manager character varying(200),
    name character varying(200),
    description text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    count integer NOT NULL
);


ALTER TABLE public.admin_tasks OWNER TO matrisapre;

--
-- TOC entry 309 (class 1259 OID 23626)
-- Name: admin_tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_tasks_id_seq OWNER TO matrisapre;

--
-- TOC entry 3914 (class 0 OID 0)
-- Dependencies: 309
-- Name: admin_tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_tasks_id_seq OWNED BY public.admin_tasks.id;


--
-- TOC entry 310 (class 1259 OID 23628)
-- Name: admin_unidades; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_unidades (
    id bigint NOT NULL,
    nombre character varying(255),
    tipo character varying(255),
    descripcion text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_unidades OWNER TO matrisapre;

--
-- TOC entry 311 (class 1259 OID 23634)
-- Name: admin_unidades_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_unidades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_unidades_id_seq OWNER TO matrisapre;

--
-- TOC entry 3915 (class 0 OID 0)
-- Dependencies: 311
-- Name: admin_unidades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_unidades_id_seq OWNED BY public.admin_unidades.id;


--
-- TOC entry 312 (class 1259 OID 23636)
-- Name: admin_users; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_users (
    id bigint NOT NULL,
    name character varying(40) NOT NULL,
    usersys character varying(25) NOT NULL,
    password character varying(60) NOT NULL,
    "statusUs" character varying(1) NOT NULL,
    mail character varying(50),
    country character varying(10) NOT NULL,
    "roleUS" bigint NOT NULL,
    superuser integer DEFAULT 1 NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    remember_token character varying(100)
);


ALTER TABLE public.admin_users OWNER TO matrisapre;

--
-- TOC entry 313 (class 1259 OID 23640)
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_users_id_seq OWNER TO matrisapre;

--
-- TOC entry 3916 (class 0 OID 0)
-- Dependencies: 313
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_users_id_seq OWNED BY public.admin_users.id;


--
-- TOC entry 314 (class 1259 OID 23642)
-- Name: admin_usersadmin; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_usersadmin (
    id bigint NOT NULL,
    sysuser character varying(255) NOT NULL,
    sysuserpass character varying(255) NOT NULL,
    typeuser character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_usersadmin OWNER TO matrisapre;

--
-- TOC entry 315 (class 1259 OID 23648)
-- Name: admin_usersadmin_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_usersadmin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_usersadmin_id_seq OWNER TO matrisapre;

--
-- TOC entry 3917 (class 0 OID 0)
-- Dependencies: 315
-- Name: admin_usersadmin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_usersadmin_id_seq OWNED BY public.admin_usersadmin.id;


--
-- TOC entry 316 (class 1259 OID 23650)
-- Name: admin_warehouse; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_warehouse (
    id bigint NOT NULL,
    name character varying(60) NOT NULL,
    address text NOT NULL,
    lat character varying(30) NOT NULL,
    lon character varying(30) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_warehouse OWNER TO matrisapre;

--
-- TOC entry 317 (class 1259 OID 23656)
-- Name: admin_warehouse_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_warehouse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_warehouse_id_seq OWNER TO matrisapre;

--
-- TOC entry 3918 (class 0 OID 0)
-- Dependencies: 317
-- Name: admin_warehouse_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_warehouse_id_seq OWNED BY public.admin_warehouse.id;


--
-- TOC entry 318 (class 1259 OID 23658)
-- Name: admin_werehouse_products; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.admin_werehouse_products (
    id bigint NOT NULL,
    id_werhouse bigint NOT NULL,
    id_product bigint NOT NULL,
    amount integer NOT NULL,
    lot character varying(25) NOT NULL,
    datein date NOT NULL,
    whom bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_werehouse_products OWNER TO matrisapre;

--
-- TOC entry 319 (class 1259 OID 23661)
-- Name: admin_werehouse_products_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.admin_werehouse_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_werehouse_products_id_seq OWNER TO matrisapre;

--
-- TOC entry 3919 (class 0 OID 0)
-- Dependencies: 319
-- Name: admin_werehouse_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.admin_werehouse_products_id_seq OWNED BY public.admin_werehouse_products.id;


--
-- TOC entry 320 (class 1259 OID 23663)
-- Name: descuentos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.descuentos (
    id bigint NOT NULL,
    id_empresa bigint,
    id_colab bigint,
    tipo character varying(255) NOT NULL,
    monto double precision NOT NULL,
    no_cuotas integer DEFAULT 0,
    valor_cuota double precision,
    primera_cuota date,
    cuota_actual integer DEFAULT 0,
    fecha_debito character varying(255),
    banco character varying(255),
    no_credito character varying(255),
    fecha_aprobado date,
    nombre character varying(255),
    detalle text,
    saldo double precision,
    estado character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.descuentos OWNER TO matrisapre;

--
-- TOC entry 321 (class 1259 OID 23671)
-- Name: descuentos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.descuentos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.descuentos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3920 (class 0 OID 0)
-- Dependencies: 321
-- Name: descuentos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.descuentos_id_seq OWNED BY public.descuentos.id;


--
-- TOC entry 322 (class 1259 OID 23673)
-- Name: log_descuentos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.log_descuentos (
    id bigint NOT NULL,
    id_empresa bigint,
    id_colab bigint,
    id_desc bigint,
    fecha date,
    pago double precision,
    no_cuota integer DEFAULT 0,
    saldo double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.log_descuentos OWNER TO matrisapre;

--
-- TOC entry 323 (class 1259 OID 23677)
-- Name: log_descuentos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.log_descuentos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_descuentos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3921 (class 0 OID 0)
-- Dependencies: 323
-- Name: log_descuentos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.log_descuentos_id_seq OWNED BY public.log_descuentos.id;


--
-- TOC entry 324 (class 1259 OID 23679)
-- Name: log_liquidacion; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.log_liquidacion (
    id bigint NOT NULL,
    id_empresa bigint,
    id_colab bigint,
    fecha_pago date,
    total double precision,
    indeminizacion double precision,
    aguinaldo double precision,
    bono_14 double precision,
    vacaciones double precision,
    salario double precision,
    bonos double precision,
    extras double precision,
    estado character varying(255),
    detalle text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.log_liquidacion OWNER TO matrisapre;

--
-- TOC entry 325 (class 1259 OID 23685)
-- Name: log_liquidacion_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.log_liquidacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_liquidacion_id_seq OWNER TO matrisapre;

--
-- TOC entry 3922 (class 0 OID 0)
-- Dependencies: 325
-- Name: log_liquidacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.log_liquidacion_id_seq OWNED BY public.log_liquidacion.id;


--
-- TOC entry 326 (class 1259 OID 23687)
-- Name: log_prestaciones; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.log_prestaciones (
    id bigint NOT NULL,
    id_empresa bigint,
    id_colab bigint,
    id_cliente bigint,
    id_sucursal bigint,
    id_area bigint,
    tipo character varying(255) NOT NULL,
    fecha_pago date,
    total double precision,
    estado character varying(255),
    detalle text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.log_prestaciones OWNER TO matrisapre;

--
-- TOC entry 327 (class 1259 OID 23693)
-- Name: log_prestaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.log_prestaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_prestaciones_id_seq OWNER TO matrisapre;

--
-- TOC entry 3923 (class 0 OID 0)
-- Dependencies: 327
-- Name: log_prestaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.log_prestaciones_id_seq OWNED BY public.log_prestaciones.id;


--
-- TOC entry 328 (class 1259 OID 23695)
-- Name: loggs_user; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.loggs_user (
    id bigint NOT NULL,
    latitude character varying(30) NOT NULL,
    longitud character varying(30) NOT NULL,
    "latitudeExit" character varying(30),
    "longitudExit" character varying(30),
    "latitudeEnterBtw" character varying(30),
    "longitudEnterBtw" character varying(30),
    "latitudeExitBtw" character varying(30),
    "longitudExitBtw" character varying(30),
    "ipClient" character varying(30),
    refrence text,
    ontime character varying(5),
    "onRange" character varying(5),
    "statusDay" character varying(45),
    "hourEnter" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "hourExit" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "hourEnterBetw" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "hourExitBetw" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "befTimeEnter" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "befTimeEnterTxt" character varying(15),
    "outTimeEnter" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "outTimeEnterTxt" character varying(15),
    "hourIdeal" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "hourDay" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "payDay" double precision,
    "extraHourDay" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "extraHourDayMiddle" double precision,
    "extraPayDay" double precision,
    "hourNight" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "extraHourNight" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "extraHourNightMiddle" double precision,
    "extraPayNight" double precision,
    "extraHourEspecial" time(0) without time zone DEFAULT '00:00:00'::time without time zone,
    "extraPayEspec" double precision,
    aproved integer,
    "dayWeek" character varying(29),
    "dateDay" date,
    delay character varying(45),
    "dayLab" character varying(15),
    "idFollower" integer,
    viatics double precision,
    "viaticsAdded" double precision,
    tasks text,
    "quantityTask" integer,
    "baseTasks" integer,
    "priceTask" integer,
    "complTask" character varying(255),
    "aditTask" integer,
    "lessTask" integer,
    "priceAdiTask" integer,
    "userId" bigint,
    "idArea" bigint,
    "idBranch" bigint,
    "idClient" bigint,
    "idLink" bigint,
    "idDay" bigint,
    "idCompany" bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    "dateAproved" date,
    "dayPay" character varying(255),
    "extraHourEspecMiddle" double precision,
    "viaticsDesc" text,
    "viaticsMovil" integer DEFAULT 0,
    typeday character varying(255) DEFAULT 'asistencia'::character varying,
    "extraHourMix" time(0) without time zone,
    "extraHourMixMiddle" double precision,
    "extraPayMix" double precision
);


ALTER TABLE public.loggs_user OWNER TO matrisapre;

--
-- TOC entry 329 (class 1259 OID 23715)
-- Name: loggs_user_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.loggs_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.loggs_user_id_seq OWNER TO matrisapre;

--
-- TOC entry 3924 (class 0 OID 0)
-- Dependencies: 329
-- Name: loggs_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.loggs_user_id_seq OWNED BY public.loggs_user.id;


--
-- TOC entry 330 (class 1259 OID 23717)
-- Name: loggs_user_planilla; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.loggs_user_planilla (
    id bigint NOT NULL,
    id_empresa bigint,
    id_colab bigint,
    id_cliente bigint,
    id_sucursal bigint,
    id_area bigint,
    fecha_inicio date,
    fecha_fin date,
    tipo character varying(255),
    sueldo double precision,
    total double precision,
    bono_ley double precision,
    bono_extra double precision,
    bono_antiguedad double precision,
    desc_dia double precision,
    desc_septimo double precision,
    desc_igss double precision,
    descuentos text,
    total_bonos double precision,
    total_descuentos double precision,
    estado character varying(255),
    fecha_pago date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.loggs_user_planilla OWNER TO matrisapre;

--
-- TOC entry 331 (class 1259 OID 23723)
-- Name: loggs_user_planilla_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.loggs_user_planilla_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.loggs_user_planilla_id_seq OWNER TO matrisapre;

--
-- TOC entry 3925 (class 0 OID 0)
-- Dependencies: 331
-- Name: loggs_user_planilla_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.loggs_user_planilla_id_seq OWNED BY public.loggs_user_planilla.id;


--
-- TOC entry 332 (class 1259 OID 23725)
-- Name: migrations; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO matrisapre;

--
-- TOC entry 333 (class 1259 OID 23728)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO matrisapre;

--
-- TOC entry 3926 (class 0 OID 0)
-- Dependencies: 333
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 334 (class 1259 OID 23730)
-- Name: personal_access_tokens; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.personal_access_tokens OWNER TO matrisapre;

--
-- TOC entry 335 (class 1259 OID 23736)
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_access_tokens_id_seq OWNER TO matrisapre;

--
-- TOC entry 3927 (class 0 OID 0)
-- Dependencies: 335
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;


--
-- TOC entry 336 (class 1259 OID 23738)
-- Name: user_horas; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.user_horas (
    id bigint NOT NULL,
    id_empresa bigint,
    id_colab bigint,
    id_cliente bigint,
    id_sucursal bigint,
    id_area bigint,
    fecha_inicio date,
    fecha_fin date,
    tipo character varying(255),
    total double precision,
    extra_dia double precision,
    extra_noche double precision,
    extra_especial double precision,
    extra_mixta double precision,
    extra_metas double precision,
    desc_igss double precision,
    metas integer DEFAULT 0,
    estado character varying(255),
    fecha_pago date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.user_horas OWNER TO matrisapre;

--
-- TOC entry 337 (class 1259 OID 23745)
-- Name: user_horas_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.user_horas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_horas_id_seq OWNER TO matrisapre;

--
-- TOC entry 3928 (class 0 OID 0)
-- Dependencies: 337
-- Name: user_horas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.user_horas_id_seq OWNED BY public.user_horas.id;


--
-- TOC entry 338 (class 1259 OID 23747)
-- Name: user_pagos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.user_pagos (
    id bigint NOT NULL,
    id_empresa bigint,
    id_cliente bigint,
    id_colab bigint,
    id_planilla bigint,
    id_horas bigint,
    id_viaticos bigint,
    total double precision,
    total_planilla double precision,
    total_horas double precision,
    total_viaticos double precision,
    estado character varying(255),
    fecha_pago date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.user_pagos OWNER TO matrisapre;

--
-- TOC entry 339 (class 1259 OID 23750)
-- Name: user_pagos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.user_pagos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_pagos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3929 (class 0 OID 0)
-- Dependencies: 339
-- Name: user_pagos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.user_pagos_id_seq OWNED BY public.user_pagos.id;


--
-- TOC entry 340 (class 1259 OID 23752)
-- Name: user_viaticos; Type: TABLE; Schema: public; Owner: matrisapre
--

CREATE TABLE public.user_viaticos (
    id bigint NOT NULL,
    id_empresa bigint,
    id_colab bigint,
    id_cliente bigint,
    id_sucursal bigint,
    id_area bigint,
    fecha_inicio date,
    fecha_fin date,
    tipo character varying(255),
    total double precision,
    viaticos double precision,
    movil double precision,
    extras double precision,
    estado character varying(255),
    fecha_pago date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.user_viaticos OWNER TO matrisapre;

--
-- TOC entry 341 (class 1259 OID 23758)
-- Name: user_viaticos_id_seq; Type: SEQUENCE; Schema: public; Owner: matrisapre
--

CREATE SEQUENCE public.user_viaticos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_viaticos_id_seq OWNER TO matrisapre;

--
-- TOC entry 3930 (class 0 OID 0)
-- Dependencies: 341
-- Name: user_viaticos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matrisapre
--

ALTER SEQUENCE public.user_viaticos_id_seq OWNED BY public.user_viaticos.id;


--
-- TOC entry 3228 (class 2604 OID 23760)
-- Name: admin_access id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_access ALTER COLUMN id SET DEFAULT nextval('public.admin_access_id_seq'::regclass);


--
-- TOC entry 3229 (class 2604 OID 23761)
-- Name: admin_access_roles id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_access_roles ALTER COLUMN id SET DEFAULT nextval('public.admin_access_roles_id_seq'::regclass);


--
-- TOC entry 3230 (class 2604 OID 23762)
-- Name: admin_alertas id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_alertas ALTER COLUMN id SET DEFAULT nextval('public.admin_alertas_id_seq'::regclass);


--
-- TOC entry 3231 (class 2604 OID 23763)
-- Name: admin_archivos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_archivos ALTER COLUMN id SET DEFAULT nextval('public.admin_archivos_id_seq'::regclass);


--
-- TOC entry 3232 (class 2604 OID 23764)
-- Name: admin_bancos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_bancos ALTER COLUMN id SET DEFAULT nextval('public.admin_bancos_id_seq'::regclass);


--
-- TOC entry 3233 (class 2604 OID 23765)
-- Name: admin_bodegas id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_bodegas ALTER COLUMN id SET DEFAULT nextval('public.admin_bodegas_id_seq'::regclass);


--
-- TOC entry 3234 (class 2604 OID 23766)
-- Name: admin_categories id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_categories ALTER COLUMN id SET DEFAULT nextval('public.admin_categories_id_seq'::regclass);


--
-- TOC entry 3235 (class 2604 OID 23767)
-- Name: admin_changeLog id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public."admin_changeLog" ALTER COLUMN id SET DEFAULT nextval('public."admin_changeLog_id_seq"'::regclass);


--
-- TOC entry 3236 (class 2604 OID 23768)
-- Name: admin_client_colab_assoc id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_client_colab_assoc ALTER COLUMN id SET DEFAULT nextval('public.admin_client_colab_assoc_id_seq'::regclass);


--
-- TOC entry 3239 (class 2604 OID 23769)
-- Name: admin_cobro_colaborador id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cobro_colaborador ALTER COLUMN id SET DEFAULT nextval('public.admin_cobro_colaborador_id_seq'::regclass);


--
-- TOC entry 3241 (class 2604 OID 23770)
-- Name: admin_codigos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_codigos ALTER COLUMN id SET DEFAULT nextval('public.admin_codigos_id_seq'::regclass);


--
-- TOC entry 3243 (class 2604 OID 23771)
-- Name: admin_colaboradores id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_colaboradores ALTER COLUMN id SET DEFAULT nextval('public.admin_colaboradores_id_seq'::regclass);


--
-- TOC entry 3244 (class 2604 OID 23772)
-- Name: admin_colors id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_colors ALTER COLUMN id SET DEFAULT nextval('public.admin_colors_id_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 23773)
-- Name: admin_company id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company ALTER COLUMN id SET DEFAULT nextval('public.admin_company_id_seq'::regclass);


--
-- TOC entry 3246 (class 2604 OID 23774)
-- Name: admin_company_client_assoc id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_client_assoc ALTER COLUMN id SET DEFAULT nextval('public.admin_company_client_assoc_id_seq'::regclass);


--
-- TOC entry 3248 (class 2604 OID 23775)
-- Name: admin_company_clients id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients ALTER COLUMN id SET DEFAULT nextval('public.admin_company_clients_id_seq'::regclass);


--
-- TOC entry 3249 (class 2604 OID 23776)
-- Name: admin_company_clients_branch_area id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_branch_area ALTER COLUMN id SET DEFAULT nextval('public.admin_company_clients_branch_area_id_seq'::regclass);


--
-- TOC entry 3250 (class 2604 OID 23777)
-- Name: admin_company_clients_branchof id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_branchof ALTER COLUMN id SET DEFAULT nextval('public.admin_company_clients_branchof_id_seq'::regclass);


--
-- TOC entry 3251 (class 2604 OID 23778)
-- Name: admin_company_clients_jobs id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_jobs ALTER COLUMN id SET DEFAULT nextval('public.admin_company_clients_jobs_id_seq'::regclass);


--
-- TOC entry 3252 (class 2604 OID 23779)
-- Name: admin_company_clients_jobs_todo id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_jobs_todo ALTER COLUMN id SET DEFAULT nextval('public.admin_company_clients_jobs_todo_id_seq'::regclass);


--
-- TOC entry 3253 (class 2604 OID 23780)
-- Name: admin_company_colab_assoc id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_colab_assoc ALTER COLUMN id SET DEFAULT nextval('public.admin_company_colab_assoc_id_seq'::regclass);


--
-- TOC entry 3255 (class 2604 OID 23781)
-- Name: admin_contratos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_contratos ALTER COLUMN id SET DEFAULT nextval('public.admin_contratos_id_seq'::regclass);


--
-- TOC entry 3256 (class 2604 OID 23782)
-- Name: admin_conversiones id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_conversiones ALTER COLUMN id SET DEFAULT nextval('public.admin_conversiones_id_seq'::regclass);


--
-- TOC entry 3258 (class 2604 OID 23783)
-- Name: admin_cotis id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cotis ALTER COLUMN id SET DEFAULT nextval('public.admin_cotis_id_seq'::regclass);


--
-- TOC entry 3259 (class 2604 OID 23784)
-- Name: admin_cumples id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cumples ALTER COLUMN id SET DEFAULT nextval('public.admin_cumples_id_seq'::regclass);


--
-- TOC entry 3260 (class 2604 OID 23785)
-- Name: admin_documentos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_documentos ALTER COLUMN id SET DEFAULT nextval('public.admin_documentos_id_seq'::regclass);


--
-- TOC entry 3261 (class 2604 OID 23786)
-- Name: admin_equipo_asignado id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipo_asignado ALTER COLUMN id SET DEFAULT nextval('public.admin_equipo_asignado_id_seq'::regclass);


--
-- TOC entry 3262 (class 2604 OID 23787)
-- Name: admin_equipos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipos ALTER COLUMN id SET DEFAULT nextval('public.admin_equipos_id_seq'::regclass);


--
-- TOC entry 3264 (class 2604 OID 23788)
-- Name: admin_facturas id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_facturas ALTER COLUMN id SET DEFAULT nextval('public.admin_facturas_id_seq'::regclass);


--
-- TOC entry 3265 (class 2604 OID 23789)
-- Name: admin_horary id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_horary ALTER COLUMN id SET DEFAULT nextval('public.admin_horary_id_seq'::regclass);


--
-- TOC entry 3267 (class 2604 OID 23790)
-- Name: admin_loggs_user_paiments id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_loggs_user_paiments ALTER COLUMN id SET DEFAULT nextval('public.admin_loggs_user_paiments_id_seq'::regclass);


--
-- TOC entry 3270 (class 2604 OID 23791)
-- Name: admin_mensajes id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_mensajes ALTER COLUMN id SET DEFAULT nextval('public.admin_mensajes_id_seq'::regclass);


--
-- TOC entry 3271 (class 2604 OID 23792)
-- Name: admin_mensajes_grupos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_mensajes_grupos ALTER COLUMN id SET DEFAULT nextval('public.admin_mensajes_grupos_id_seq'::regclass);


--
-- TOC entry 3273 (class 2604 OID 23793)
-- Name: admin_petitions_complaintments id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_complaintments ALTER COLUMN id SET DEFAULT nextval('public.admin_petitions_complaintments_id_seq'::regclass);


--
-- TOC entry 3275 (class 2604 OID 23794)
-- Name: admin_petitions_economicpetitions id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_economicpetitions ALTER COLUMN id SET DEFAULT nextval('public.admin_petitions_economicpetitions_id_seq'::regclass);


--
-- TOC entry 3277 (class 2604 OID 23795)
-- Name: admin_petitions_restoretimepermissions id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_restoretimepermissions ALTER COLUMN id SET DEFAULT nextval('public.admin_petitions_restoretimepermissions_id_seq'::regclass);


--
-- TOC entry 3278 (class 2604 OID 23796)
-- Name: admin_petitions_simplepetitions id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_simplepetitions ALTER COLUMN id SET DEFAULT nextval('public.admin_petitions_simplepetitions_id_seq'::regclass);


--
-- TOC entry 3280 (class 2604 OID 23797)
-- Name: admin_petitions_simplepetitions_save id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_simplepetitions_save ALTER COLUMN id SET DEFAULT nextval('public.admin_petitions_simplepetitions_save_id_seq'::regclass);


--
-- TOC entry 3282 (class 2604 OID 23798)
-- Name: admin_petitions_timepermissions id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_timepermissions ALTER COLUMN id SET DEFAULT nextval('public.admin_petitions_timepermissions_id_seq'::regclass);


--
-- TOC entry 3283 (class 2604 OID 23799)
-- Name: admin_productos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_productos ALTER COLUMN id SET DEFAULT nextval('public.admin_productos_id_seq'::regclass);


--
-- TOC entry 3284 (class 2604 OID 23800)
-- Name: admin_products id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_products ALTER COLUMN id SET DEFAULT nextval('public.admin_products_id_seq'::regclass);


--
-- TOC entry 3285 (class 2604 OID 23801)
-- Name: admin_providers id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_providers ALTER COLUMN id SET DEFAULT nextval('public.admin_providers_id_seq'::regclass);


--
-- TOC entry 3286 (class 2604 OID 23802)
-- Name: admin_puestos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_puestos ALTER COLUMN id SET DEFAULT nextval('public.admin_puestos_id_seq'::regclass);


--
-- TOC entry 3287 (class 2604 OID 23803)
-- Name: admin_roles id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_roles ALTER COLUMN id SET DEFAULT nextval('public.admin_roles_id_seq'::regclass);


--
-- TOC entry 3288 (class 2604 OID 23804)
-- Name: admin_sizes id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_sizes ALTER COLUMN id SET DEFAULT nextval('public.admin_sizes_id_seq'::regclass);


--
-- TOC entry 3289 (class 2604 OID 23805)
-- Name: admin_solicitudes id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_solicitudes ALTER COLUMN id SET DEFAULT nextval('public.admin_solicitudes_id_seq'::regclass);


--
-- TOC entry 3290 (class 2604 OID 23806)
-- Name: admin_stock id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock ALTER COLUMN id SET DEFAULT nextval('public.admin_stock_id_seq'::regclass);


--
-- TOC entry 3291 (class 2604 OID 23807)
-- Name: admin_stock_egresos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos ALTER COLUMN id SET DEFAULT nextval('public.admin_stock_egresos_id_seq'::regclass);


--
-- TOC entry 3292 (class 2604 OID 23808)
-- Name: admin_stock_egresos_equipo id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos_equipo ALTER COLUMN id SET DEFAULT nextval('public.admin_stock_egresos_equipo_id_seq'::regclass);


--
-- TOC entry 3294 (class 2604 OID 23809)
-- Name: admin_stock_equipos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_equipos ALTER COLUMN id SET DEFAULT nextval('public.admin_stock_equipos_id_seq'::regclass);


--
-- TOC entry 3295 (class 2604 OID 23810)
-- Name: admin_stock_ingresos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos ALTER COLUMN id SET DEFAULT nextval('public.admin_stock_ingresos_id_seq'::regclass);


--
-- TOC entry 3296 (class 2604 OID 23811)
-- Name: admin_stock_ingresos_equipo id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos_equipo ALTER COLUMN id SET DEFAULT nextval('public.admin_stock_ingresos_equipo_id_seq'::regclass);


--
-- TOC entry 3297 (class 2604 OID 23812)
-- Name: admin_subCategories id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public."admin_subCategories" ALTER COLUMN id SET DEFAULT nextval('public."admin_subCategories_id_seq"'::regclass);


--
-- TOC entry 3298 (class 2604 OID 23813)
-- Name: admin_tasks id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_tasks ALTER COLUMN id SET DEFAULT nextval('public.admin_tasks_id_seq'::regclass);


--
-- TOC entry 3299 (class 2604 OID 23814)
-- Name: admin_unidades id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_unidades ALTER COLUMN id SET DEFAULT nextval('public.admin_unidades_id_seq'::regclass);


--
-- TOC entry 3301 (class 2604 OID 23815)
-- Name: admin_users id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_users ALTER COLUMN id SET DEFAULT nextval('public.admin_users_id_seq'::regclass);


--
-- TOC entry 3302 (class 2604 OID 23816)
-- Name: admin_usersadmin id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_usersadmin ALTER COLUMN id SET DEFAULT nextval('public.admin_usersadmin_id_seq'::regclass);


--
-- TOC entry 3303 (class 2604 OID 23817)
-- Name: admin_warehouse id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_warehouse ALTER COLUMN id SET DEFAULT nextval('public.admin_warehouse_id_seq'::regclass);


--
-- TOC entry 3304 (class 2604 OID 23818)
-- Name: admin_werehouse_products id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_werehouse_products ALTER COLUMN id SET DEFAULT nextval('public.admin_werehouse_products_id_seq'::regclass);


--
-- TOC entry 3307 (class 2604 OID 23819)
-- Name: descuentos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.descuentos ALTER COLUMN id SET DEFAULT nextval('public.descuentos_id_seq'::regclass);


--
-- TOC entry 3309 (class 2604 OID 23820)
-- Name: log_descuentos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_descuentos ALTER COLUMN id SET DEFAULT nextval('public.log_descuentos_id_seq'::regclass);


--
-- TOC entry 3310 (class 2604 OID 23821)
-- Name: log_liquidacion id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_liquidacion ALTER COLUMN id SET DEFAULT nextval('public.log_liquidacion_id_seq'::regclass);


--
-- TOC entry 3311 (class 2604 OID 23822)
-- Name: log_prestaciones id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_prestaciones ALTER COLUMN id SET DEFAULT nextval('public.log_prestaciones_id_seq'::regclass);


--
-- TOC entry 3326 (class 2604 OID 23823)
-- Name: loggs_user id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user ALTER COLUMN id SET DEFAULT nextval('public.loggs_user_id_seq'::regclass);


--
-- TOC entry 3327 (class 2604 OID 23824)
-- Name: loggs_user_planilla id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user_planilla ALTER COLUMN id SET DEFAULT nextval('public.loggs_user_planilla_id_seq'::regclass);


--
-- TOC entry 3328 (class 2604 OID 23825)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 3329 (class 2604 OID 23826)
-- Name: personal_access_tokens id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);


--
-- TOC entry 3331 (class 2604 OID 23827)
-- Name: user_horas id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_horas ALTER COLUMN id SET DEFAULT nextval('public.user_horas_id_seq'::regclass);


--
-- TOC entry 3332 (class 2604 OID 23828)
-- Name: user_pagos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos ALTER COLUMN id SET DEFAULT nextval('public.user_pagos_id_seq'::regclass);


--
-- TOC entry 3333 (class 2604 OID 23829)
-- Name: user_viaticos id; Type: DEFAULT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_viaticos ALTER COLUMN id SET DEFAULT nextval('public.user_viaticos_id_seq'::regclass);


--
-- TOC entry 3715 (class 0 OID 23235)
-- Dependencies: 202
-- Data for Name: admin_access; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_access VALUES (1, 'Roles de usuario', 'roles', 'i-Gears', '1', 'Configuración', '2020-07-30 03:31:00', '2021-10-05 03:53:34');
INSERT INTO public.admin_access VALUES (2, 'Personal', 'users', 'i-Gears', '1', 'Configuración', '2020-07-30 03:31:28', '2021-10-05 03:53:46');
INSERT INTO public.admin_access VALUES (3, 'Accesos', 'permissions', 'cogs', '1', 'Configuración', '2019-11-07 05:05:02', '2021-10-02 02:04:01');
INSERT INTO public.admin_access VALUES (4, 'Puestos', 'puestos', 'i-Management', '1', 'Clientes', '2021-08-19 22:37:03', '2021-10-15 01:26:46');
INSERT INTO public.admin_access VALUES (5, 'Categorías', 'categories', 'i-Bar-Code', '1', 'Insumos', '2021-08-19 22:38:52', '2021-10-05 03:54:46');
INSERT INTO public.admin_access VALUES (6, 'Características', 'colors', 'i-Bar-Code', '1', 'Insumos', '2021-08-19 22:44:29', '2021-10-05 03:54:59');
INSERT INTO public.admin_access VALUES (7, 'Bodegas', 'bodegas', 'i-Bar-Code', '1', 'Insumos', '2021-08-19 22:47:10', '2021-10-05 03:55:10');
INSERT INTO public.admin_access VALUES (8, 'Productos', 'productos', 'i-Bar-Code', '1', 'Insumos', '2021-08-19 22:47:49', '2021-10-05 03:55:38');
INSERT INTO public.admin_access VALUES (9, 'Dimensiones', 'sizes', 'i-Bar-Code', '1', 'Insumos', '2021-08-19 22:53:24', '2021-10-08 03:30:26');
INSERT INTO public.admin_access VALUES (10, 'Proveedores', 'providers', 'i-Bar-Code', '1', 'Insumos', '2021-08-23 21:05:40', '2021-10-05 03:55:20');
INSERT INTO public.admin_access VALUES (12, 'Documentos', 'documentos', 'i-University', '1', 'Empresas', '2021-09-30 17:55:33', '2021-10-08 02:28:19');
INSERT INTO public.admin_access VALUES (13, 'Colaboradores', 'colaboradores', 'i-Business-Mens', '1', 'Personal', '2021-09-30 17:56:10', '2021-10-05 03:56:15');
INSERT INTO public.admin_access VALUES (14, 'Empresas', 'company', 'i-University', '1', 'Empresas', '2021-10-02 02:02:02', '2021-10-05 04:00:35');
INSERT INTO public.admin_access VALUES (15, 'Clientes', 'clients', 'i-Management', '1', 'Clientes', '2021-10-02 02:03:42', '2021-10-15 01:26:53');
INSERT INTO public.admin_access VALUES (16, 'Bancos', 'bancos', 'i-University', '1', 'Empresas', '2021-10-08 02:11:42', '2021-10-08 02:27:57');
INSERT INTO public.admin_access VALUES (17, 'Contratos', 'contratos', 'i-University', '1', 'Empresas', '2021-10-08 02:24:40', '2021-10-08 02:28:29');
INSERT INTO public.admin_access VALUES (11, 'Conversion de unidades (medidas)', 'conversiones', 'i-Bar-Code', '1', 'Insumos', '2022-05-09 14:06:51', '2022-05-09 14:06:51');
INSERT INTO public.admin_access VALUES (18, 'Bancos', 'bancos', 'i-University', '0', 'Contabilidad', '2022-05-09 18:20:54', '2022-05-09 18:20:54');
INSERT INTO public.admin_access VALUES (19, 'Contraseñas de pago', 'codigos', 'i-Cash-register-2', '1', 'Contabilidad', '2022-05-10 06:28:30', '2022-05-10 06:28:30');
INSERT INTO public.admin_access VALUES (20, 'Cotizaciones', 'cotis', 'i-Cash-register-2', '1', 'Contabilidad', '2022-05-10 06:29:15', '2022-05-10 06:29:15');
INSERT INTO public.admin_access VALUES (21, 'Bodegas de equipos', 'bodegasEquipo', 'i-Check', '1', 'Equipo', '2022-05-10 06:30:48', '2022-05-10 06:30:48');
INSERT INTO public.admin_access VALUES (22, 'Descuentos', 'descuentos', 'i-Business-Mens', '1', 'Personal', '2022-05-10 06:31:57', '2022-05-10 06:31:57');
INSERT INTO public.admin_access VALUES (23, 'Pago', 'pagos', 'i-Business-Mens', '1', 'Personal', '2022-05-10 06:32:22', '2022-05-10 06:32:22');
INSERT INTO public.admin_access VALUES (24, 'Planilla', 'planillas', 'i-Business-Mens', '1', 'Personal', '2022-05-10 06:32:38', '2022-05-10 06:32:38');
INSERT INTO public.admin_access VALUES (25, 'Planilla Extras', 'horas', 'i-Business-Mens', '1', 'Personal', '2022-05-10 06:32:55', '2022-05-10 06:32:55');
INSERT INTO public.admin_access VALUES (26, 'Planilla viáticos', 'viaticos', 'i-Business-Mens', '1', 'Personal', '2022-05-10 06:33:08', '2022-05-10 06:33:08');
INSERT INTO public.admin_access VALUES (27, 'Reporte de horarios', 'workdone', 'i-Business-Mens', '1', 'Personal', '2022-05-10 06:33:27', '2022-05-10 06:33:27');
INSERT INTO public.admin_access VALUES (28, 'Cumpleaños', 'cumples', 'i-Inbox-Out', '1', 'Comunicación', '2022-05-10 06:34:33', '2022-05-10 06:34:33');
INSERT INTO public.admin_access VALUES (29, 'Mensajes', 'mensajes', 'i-Inbox-Out', '1', 'Comunicación', '2022-05-10 06:34:50', '2022-05-10 06:34:50');
INSERT INTO public.admin_access VALUES (30, 'Solicitudes', 'inboxpetitions', 'i-Support', '1', 'Solicitudes', '2022-05-10 06:36:11', '2022-05-10 06:36:11');
INSERT INTO public.admin_access VALUES (31, 'Tipos de solicitudes', 'listpetitions', 'i-Support', '1', 'Solicitudes', '2022-05-10 06:36:26', '2022-05-10 06:36:26');
INSERT INTO public.admin_access VALUES (32, 'Usuarios', 'users', 'i-Gears', '0', 'Configuración', '2022-05-10 06:37:34', '2022-05-10 06:37:34');
INSERT INTO public.admin_access VALUES (33, 'Unidades (medidas)', 'unidades', 'i-Bar-Code', '1', 'Insumos', '2022-05-18 12:20:54', '2022-05-18 12:20:54');


--
-- TOC entry 3717 (class 0 OID 23241)
-- Dependencies: 204
-- Data for Name: admin_access_roles; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3719 (class 0 OID 23246)
-- Dependencies: 206
-- Data for Name: admin_alertas; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3721 (class 0 OID 23254)
-- Dependencies: 208
-- Data for Name: admin_archivos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3723 (class 0 OID 23262)
-- Dependencies: 210
-- Data for Name: admin_bancos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_bancos VALUES (1, 'BANCO INDUSTRIAL', 'BANCO INDUSTRIAL, S. A.', NULL, '2022-05-26 10:11:29', '2022-05-26 10:11:29');
INSERT INTO public.admin_bancos VALUES (2, 'BANRURAL', 'BANCO DESARROLLO RURAL', NULL, '2022-05-26 16:21:04', '2022-05-26 16:21:04');
INSERT INTO public.admin_bancos VALUES (3, 'BAM', 'BANCO AGROMERCANTIL DE GUATEMALA, S.A', NULL, '2022-05-26 16:21:48', '2022-05-26 16:21:48');
INSERT INTO public.admin_bancos VALUES (4, 'BANCO G&T', 'BANCO G&T CONTINENTAL', NULL, '2022-05-26 16:22:25', '2022-05-26 16:22:25');
INSERT INTO public.admin_bancos VALUES (5, 'BAC', 'BANCO DE AMERICA CENTRAL', NULL, '2022-05-26 16:22:45', '2022-05-26 16:22:45');
INSERT INTO public.admin_bancos VALUES (6, 'INTERBANCO', 'BANCO  INTERNACIONAL', NULL, '2022-05-26 16:23:16', '2022-05-26 16:23:16');
INSERT INTO public.admin_bancos VALUES (7, 'Vivibanco', 'Vivibanco', NULL, '2022-05-26 16:57:47', '2022-05-26 16:57:47');


--
-- TOC entry 3725 (class 0 OID 23270)
-- Dependencies: 212
-- Data for Name: admin_bodegas; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_bodegas VALUES (1, 'Sur Occidente', '2022-05-10 06:55:33', '2022-05-10 06:55:33', 'cliente');
INSERT INTO public.admin_bodegas VALUES (2, 'Sur Occidente', '2022-05-10 06:55:50', '2022-05-10 06:55:50', 'cliente');
INSERT INTO public.admin_bodegas VALUES (3, 'Sur Occidente', '2022-05-10 06:56:06', '2022-05-10 06:56:06', 'cliente');
INSERT INTO public.admin_bodegas VALUES (4, 'Sur Occidente', '2022-05-10 06:56:22', '2022-05-10 06:56:22', 'cliente');
INSERT INTO public.admin_bodegas VALUES (5, 'Central', '2022-05-10 06:57:29', '2022-05-10 06:57:29', 'cliente');
INSERT INTO public.admin_bodegas VALUES (6, 'Flota', '2022-05-10 06:57:47', '2022-05-10 06:57:47', 'cliente');
INSERT INTO public.admin_bodegas VALUES (7, 'Móvil', '2022-05-10 06:57:58', '2022-05-10 06:57:58', 'cliente');
INSERT INTO public.admin_bodegas VALUES (8, 'Aliansa', '2022-05-10 06:58:10', '2022-05-10 06:58:10', 'cliente');
INSERT INTO public.admin_bodegas VALUES (9, 'Nor Oriente', '2022-05-10 06:58:22', '2022-05-10 06:58:22', 'cliente');
INSERT INTO public.admin_bodegas VALUES (10, 'Santa Lucía', '2022-05-10 06:59:46', '2022-05-10 06:59:46', 'cliente');
INSERT INTO public.admin_bodegas VALUES (11, 'Distribuidora Central', '2022-05-10 07:00:01', '2022-05-10 07:00:01', 'cliente');
INSERT INTO public.admin_bodegas VALUES (12, 'Planta 1', '2022-05-10 07:00:25', '2022-05-10 07:00:25', 'cliente');
INSERT INTO public.admin_bodegas VALUES (13, 'Planta 2', '2022-05-10 07:00:34', '2022-05-10 07:00:34', 'cliente');
INSERT INTO public.admin_bodegas VALUES (14, 'Distribuidora Planta 2', '2022-05-10 07:00:46', '2022-05-10 07:00:46', 'cliente');
INSERT INTO public.admin_bodegas VALUES (15, 'Harinas', '2022-05-10 07:00:57', '2022-05-10 07:00:57', 'cliente');
INSERT INTO public.admin_bodegas VALUES (16, 'Distribuidora Xela', '2022-05-10 07:01:17', '2022-05-10 07:01:17', 'cliente');
INSERT INTO public.admin_bodegas VALUES (17, 'Distribuidora Zacapa', '2022-05-10 07:01:41', '2022-05-10 07:01:41', 'cliente');
INSERT INTO public.admin_bodegas VALUES (18, 'Distribuidora Suroriente', '2022-05-10 07:03:40', '2022-05-10 07:03:40', 'cliente');
INSERT INTO public.admin_bodegas VALUES (19, 'Distribuidora Petén', '2022-05-10 07:03:51', '2022-05-10 07:03:51', 'cliente');
INSERT INTO public.admin_bodegas VALUES (20, 'Distribuidora Los Encuentros', '2022-05-10 07:04:07', '2022-05-10 07:04:07', 'cliente');
INSERT INTO public.admin_bodegas VALUES (21, 'Distribuidora Chimaltenango', '2022-05-10 07:04:42', '2022-05-10 07:04:42', 'cliente');
INSERT INTO public.admin_bodegas VALUES (22, 'Distribuidora Coatepeque', '2022-05-10 07:06:25', '2022-05-10 07:06:25', 'cliente');
INSERT INTO public.admin_bodegas VALUES (23, 'Distribuidora Huehuetenango', '2022-05-10 07:06:38', '2022-05-10 07:06:38', 'cliente');
INSERT INTO public.admin_bodegas VALUES (24, 'Distribuidora Los Esclavos', '2022-05-10 07:06:49', '2022-05-10 07:06:49', 'cliente');
INSERT INTO public.admin_bodegas VALUES (25, 'Distribuidora Puerto Barrios', '2022-05-10 07:07:05', '2022-05-10 07:07:05', 'cliente');
INSERT INTO public.admin_bodegas VALUES (26, 'Distribuidora Cobán', '2022-05-10 07:07:19', '2022-05-10 07:07:19', 'cliente');
INSERT INTO public.admin_bodegas VALUES (27, 'Distribuidora Jutiapa', '2022-05-10 07:07:34', '2022-05-10 07:07:34', 'cliente');
INSERT INTO public.admin_bodegas VALUES (28, 'Distribuidora Mazatenango', '2022-05-10 07:07:46', '2022-05-10 07:07:46', 'cliente');
INSERT INTO public.admin_bodegas VALUES (29, 'Distribuidora CIA Amatitlan', '2022-05-10 07:08:03', '2022-05-10 07:08:03', 'cliente');
INSERT INTO public.admin_bodegas VALUES (30, 'Planta Central', '2022-05-10 07:08:39', '2022-05-10 07:08:39', 'cliente');
INSERT INTO public.admin_bodegas VALUES (31, 'Bodega', '2022-05-10 07:08:51', '2022-05-10 07:08:51', 'cliente');
INSERT INTO public.admin_bodegas VALUES (32, 'Lavado Flotilla', '2022-05-10 07:09:13', '2022-05-10 07:09:13', 'cliente');
INSERT INTO public.admin_bodegas VALUES (33, 'Mecánica Flotilla', '2022-05-10 07:09:27', '2022-05-10 07:09:27', 'cliente');
INSERT INTO public.admin_bodegas VALUES (34, 'Departamento de operaciones', '2022-05-10 07:12:47', '2022-05-10 07:12:47', 'cliente');
INSERT INTO public.admin_bodegas VALUES (35, 'Departamento de Recursos Humanos', '2022-05-10 07:13:02', '2022-05-10 07:13:02', 'cliente');
INSERT INTO public.admin_bodegas VALUES (36, 'Gerencia General', '2022-05-10 07:13:14', '2022-05-10 07:13:14', 'cliente');
INSERT INTO public.admin_bodegas VALUES (37, 'Gerencia Administrativa', '2022-05-10 07:13:26', '2022-05-10 07:13:26', 'cliente');
INSERT INTO public.admin_bodegas VALUES (38, 'Oficinas centrales', '2022-05-10 07:13:39', '2022-05-10 07:13:39', 'cliente');
INSERT INTO public.admin_bodegas VALUES (39, 'Oficinas Sur Occidente', '2022-05-10 07:13:51', '2022-05-10 07:13:51', 'cliente');
INSERT INTO public.admin_bodegas VALUES (40, 'DACAR', '2022-05-10 07:14:03', '2022-05-10 07:14:03', 'cliente');
INSERT INTO public.admin_bodegas VALUES (41, 'Garantías', '2022-05-10 07:14:13', '2022-05-10 07:14:13', 'cliente');
INSERT INTO public.admin_bodegas VALUES (42, 'Planta de producción', '2022-05-10 07:14:56', '2022-05-10 07:14:56', 'cliente');
INSERT INTO public.admin_bodegas VALUES (43, 'Proyectos Horno', '2022-05-10 07:15:05', '2022-05-10 07:15:05', 'cliente');
INSERT INTO public.admin_bodegas VALUES (44, 'ECA', '2022-05-10 07:20:22', '2022-05-10 07:20:22', 'cliente');
INSERT INTO public.admin_bodegas VALUES (45, 'Central', '2022-05-10 07:22:55', '2022-05-10 07:22:55', 'cliente');
INSERT INTO public.admin_bodegas VALUES (46, 'Pollolandia', '2022-05-10 07:23:14', '2022-05-10 07:23:14', 'cliente');
INSERT INTO public.admin_bodegas VALUES (47, 'Central', '2022-05-10 07:23:32', '2022-05-10 07:23:32', 'cliente');
INSERT INTO public.admin_bodegas VALUES (48, 'Central', '2022-05-10 07:24:12', '2022-05-10 07:24:12', 'cliente');
INSERT INTO public.admin_bodegas VALUES (49, 'Reciclados', '2022-05-10 07:24:25', '2022-05-10 07:24:25', 'cliente');
INSERT INTO public.admin_bodegas VALUES (50, 'ALMARSA', '2022-05-10 07:24:40', '2022-05-10 07:24:40', 'cliente');
INSERT INTO public.admin_bodegas VALUES (51, 'Empaque Flexible', '2022-05-10 07:26:24', '2022-05-10 07:26:24', 'cliente');
INSERT INTO public.admin_bodegas VALUES (52, 'Central', '2022-05-10 07:26:34', '2022-05-10 07:26:34', 'cliente');
INSERT INTO public.admin_bodegas VALUES (53, 'Mayapan', '2022-05-10 07:28:48', '2022-05-10 07:28:48', 'cliente');
INSERT INTO public.admin_bodegas VALUES (54, 'Planta procesadora', '2022-05-10 07:29:00', '2022-05-10 07:29:00', 'cliente');
INSERT INTO public.admin_bodegas VALUES (55, 'Central', '2022-05-10 07:32:01', '2022-05-10 07:32:01', 'cliente');
INSERT INTO public.admin_bodegas VALUES (56, 'Central', '2022-05-10 07:32:31', '2022-05-10 07:32:31', 'cliente');
INSERT INTO public.admin_bodegas VALUES (57, 'Central', '2022-05-10 07:34:32', '2022-05-10 07:34:32', 'cliente');
INSERT INTO public.admin_bodegas VALUES (58, 'Mary Kay', '2022-05-10 07:39:50', '2022-05-10 07:39:50', 'cliente');
INSERT INTO public.admin_bodegas VALUES (59, 'Central', '2022-05-10 07:40:18', '2022-05-10 07:40:18', 'cliente');
INSERT INTO public.admin_bodegas VALUES (60, 'Central', '2022-05-10 07:42:06', '2022-05-10 07:42:06', 'cliente');
INSERT INTO public.admin_bodegas VALUES (61, 'Central', '2022-05-10 07:42:13', '2022-05-10 07:42:13', 'cliente');
INSERT INTO public.admin_bodegas VALUES (62, 'Central', '2022-05-10 07:42:54', '2022-05-10 07:42:54', 'cliente');
INSERT INTO public.admin_bodegas VALUES (63, 'Planta 2', '2022-05-10 07:43:21', '2022-05-10 07:43:21', 'cliente');
INSERT INTO public.admin_bodegas VALUES (64, 'Puerto Barrios', '2022-05-10 07:43:56', '2022-05-10 07:43:56', 'cliente');
INSERT INTO public.admin_bodegas VALUES (65, 'Ciudad de Guatemala', '2022-05-10 07:44:20', '2022-05-10 07:44:20', 'cliente');
INSERT INTO public.admin_bodegas VALUES (66, 'Escuintla', '2022-05-10 07:44:42', '2022-05-10 07:44:42', 'cliente');
INSERT INTO public.admin_bodegas VALUES (67, 'Central', '2022-05-10 07:45:04', '2022-05-10 07:45:04', 'cliente');
INSERT INTO public.admin_bodegas VALUES (68, 'Distribuidora Retalhuleu', '2022-05-10 07:47:15', '2022-05-10 07:47:15', 'cliente');
INSERT INTO public.admin_bodegas VALUES (69, 'Distribuidora Mazatenango', '2022-05-10 07:47:25', '2022-05-10 07:47:25', 'cliente');
INSERT INTO public.admin_bodegas VALUES (70, 'Escuintla', '2022-05-10 08:15:09', '2022-05-10 08:15:09', 'cliente');
INSERT INTO public.admin_bodegas VALUES (71, 'Oficinas Centrales', '2022-05-10 08:15:19', '2022-05-10 08:15:19', 'cliente');
INSERT INTO public.admin_bodegas VALUES (72, 'Santa Rosa', '2022-05-10 08:15:32', '2022-05-10 08:15:32', 'cliente');
INSERT INTO public.admin_bodegas VALUES (73, 'Taxisco', '2022-05-10 08:15:44', '2022-05-10 08:15:44', 'cliente');
INSERT INTO public.admin_bodegas VALUES (74, 'Mensajería', '2022-05-10 08:16:08', '2022-05-10 08:16:08', 'cliente');
INSERT INTO public.admin_bodegas VALUES (75, 'Villa Nueva', '2022-05-10 08:16:37', '2022-05-10 08:16:37', 'cliente');
INSERT INTO public.admin_bodegas VALUES (76, 'Comercialización', '2022-05-10 08:17:39', '2022-05-10 08:17:39', 'cliente');
INSERT INTO public.admin_bodegas VALUES (77, 'Central', '2022-05-10 08:18:17', '2022-05-10 08:18:17', 'cliente');
INSERT INTO public.admin_bodegas VALUES (78, 'Ciudad', '2022-05-10 08:18:43', '2022-05-10 08:18:43', 'cliente');
INSERT INTO public.admin_bodegas VALUES (79, 'Cool Tshirt', '2022-05-10 08:19:11', '2022-05-10 08:19:11', 'cliente');
INSERT INTO public.admin_bodegas VALUES (80, 'Pincel Turquesa', '2022-05-10 08:19:26', '2022-05-10 08:19:26', 'cliente');
INSERT INTO public.admin_bodegas VALUES (81, 'Oficinas Centrales', '2022-05-10 08:19:36', '2022-05-10 08:19:36', 'cliente');
INSERT INTO public.admin_bodegas VALUES (82, 'Ciudad', '2022-05-10 08:20:02', '2022-05-10 08:20:02', 'cliente');
INSERT INTO public.admin_bodegas VALUES (83, 'Ciudad', '2022-05-10 08:20:08', '2022-05-10 08:20:08', 'cliente');
INSERT INTO public.admin_bodegas VALUES (84, 'Mensajería', '2022-05-10 08:20:39', '2022-05-10 08:20:39', 'cliente');
INSERT INTO public.admin_bodegas VALUES (85, 'Vistares', '2022-05-10 08:21:17', '2022-05-10 08:21:17', 'cliente');
INSERT INTO public.admin_bodegas VALUES (86, 'Naranjo', '2022-05-10 08:21:32', '2022-05-10 08:21:32', 'cliente');
INSERT INTO public.admin_bodegas VALUES (87, 'Condado Concepción', '2022-05-10 08:21:50', '2022-05-10 08:21:50', 'cliente');
INSERT INTO public.admin_bodegas VALUES (88, 'Alamos', '2022-05-10 08:22:03', '2022-05-10 08:22:03', 'cliente');
INSERT INTO public.admin_bodegas VALUES (89, 'Fontabella', '2022-05-10 08:22:14', '2022-05-10 08:22:14', 'cliente');
INSERT INTO public.admin_bodegas VALUES (90, 'Majadas', '2022-05-10 08:22:25', '2022-05-10 08:22:25', 'cliente');
INSERT INTO public.admin_bodegas VALUES (91, 'Rambla', '2022-05-10 08:22:36', '2022-05-10 08:22:36', 'cliente');
INSERT INTO public.admin_bodegas VALUES (92, 'Jealsa Escuintla', '2022-05-10 08:22:59', '2022-05-10 08:22:59', 'cliente');
INSERT INTO public.admin_bodegas VALUES (93, 'Alpasa Bodega #5', '2022-05-10 08:23:10', '2022-05-10 08:23:10', 'cliente');
INSERT INTO public.admin_bodegas VALUES (95, 'MATRISA', '2022-05-10 14:26:14', '2022-05-10 14:26:14', 'empresa');
INSERT INTO public.admin_bodegas VALUES (96, 'GRUPO MATRI', '2022-05-10 15:21:57', '2022-05-10 15:21:57', 'empresa');
INSERT INTO public.admin_bodegas VALUES (97, 'MATRISA TEMPORALES', '2022-05-10 15:23:44', '2022-05-10 15:23:44', 'empresa');
INSERT INTO public.admin_bodegas VALUES (98, 'MATRISA BPL', '2022-05-10 15:29:26', '2022-05-10 15:29:26', 'empresa');
INSERT INTO public.admin_bodegas VALUES (99, 'Zona 12', '2022-05-12 07:51:33', '2022-05-12 07:51:33', 'cliente');
INSERT INTO public.admin_bodegas VALUES (100, 'SUPERVISION', '2022-05-13 17:01:31', '2022-05-13 17:01:31', 'cliente');


--
-- TOC entry 3727 (class 0 OID 23278)
-- Dependencies: 214
-- Data for Name: admin_categories; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_categories VALUES (1, 'LIMPIEZA', '2022-05-18 14:17:24', '2022-05-18 14:17:24');


--
-- TOC entry 3729 (class 0 OID 23283)
-- Dependencies: 216
-- Data for Name: admin_changeLog; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public."admin_changeLog" VALUES (1, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-10 07:50:50', '2022-05-10 07:50:50', 1);
INSERT INTO public."admin_changeLog" VALUES (2, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', 'Actualización de información', '2022-05-10 07:58:05', '2022-05-10 07:58:05', 1);
INSERT INTO public."admin_changeLog" VALUES (3, 'Sistema Admin-sadmin', 'puestos', 'edicion', NULL, '2022-05-10 08:24:19', '2022-05-10 08:24:19', 2);
INSERT INTO public."admin_changeLog" VALUES (4, 'Sistema Admin-sadmin', 'puestos', 'edicion', NULL, '2022-05-10 08:27:26', '2022-05-10 08:27:26', 12);
INSERT INTO public."admin_changeLog" VALUES (5, 'Sistema Admin-sadmin', 'puestos', 'edicion', NULL, '2022-05-10 08:54:49', '2022-05-10 08:54:49', 29);
INSERT INTO public."admin_changeLog" VALUES (6, 'Sistema Admin-sadmin', 'company', 'edicion', NULL, '2022-05-10 15:24:07', '2022-05-10 15:24:07', 4);
INSERT INTO public."admin_changeLog" VALUES (7, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-11 15:35:33', '2022-05-11 15:35:33', 2);
INSERT INTO public."admin_changeLog" VALUES (8, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-11 15:41:36', '2022-05-11 15:41:36', 2);
INSERT INTO public."admin_changeLog" VALUES (9, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', 'Se actualizó contraseña', '2022-05-12 09:26:02', '2022-05-12 09:26:02', 8);
INSERT INTO public."admin_changeLog" VALUES (10, 'Sistema Admin-sadmin', 'clients', 'edicion', NULL, '2022-05-12 12:35:50', '2022-05-12 12:35:50', 42);
INSERT INTO public."admin_changeLog" VALUES (11, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', 'a', '2022-05-12 15:08:05', '2022-05-12 15:08:05', 8);
INSERT INTO public."admin_changeLog" VALUES (12, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-13 11:04:17', '2022-05-13 11:04:17', 8);
INSERT INTO public."admin_changeLog" VALUES (13, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', 'DPI ESTABA MALO', '2022-05-19 09:03:10', '2022-05-19 09:03:10', 9);
INSERT INTO public."admin_changeLog" VALUES (14, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 10:10:57', '2022-05-26 10:10:57', 1);
INSERT INTO public."admin_changeLog" VALUES (15, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 10:12:38', '2022-05-26 10:12:38', 1);
INSERT INTO public."admin_changeLog" VALUES (16, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:15:05', '2022-05-26 16:15:05', 6);
INSERT INTO public."admin_changeLog" VALUES (17, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:19:44', '2022-05-26 16:19:44', 9);
INSERT INTO public."admin_changeLog" VALUES (18, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:30:12', '2022-05-26 16:30:12', 5);
INSERT INTO public."admin_changeLog" VALUES (19, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:36:45', '2022-05-26 16:36:45', 9);
INSERT INTO public."admin_changeLog" VALUES (20, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:41:34', '2022-05-26 16:41:34', 2);
INSERT INTO public."admin_changeLog" VALUES (21, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:43:49', '2022-05-26 16:43:49', 2);
INSERT INTO public."admin_changeLog" VALUES (22, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:44:30', '2022-05-26 16:44:30', 2);
INSERT INTO public."admin_changeLog" VALUES (23, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:49:57', '2022-05-26 16:49:57', 4);
INSERT INTO public."admin_changeLog" VALUES (24, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:51:09', '2022-05-26 16:51:09', 7);
INSERT INTO public."admin_changeLog" VALUES (25, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:52:05', '2022-05-26 16:52:05', 6);
INSERT INTO public."admin_changeLog" VALUES (26, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:53:17', '2022-05-26 16:53:17', 9);
INSERT INTO public."admin_changeLog" VALUES (27, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:54:08', '2022-05-26 16:54:08', 6);
INSERT INTO public."admin_changeLog" VALUES (28, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:56:36', '2022-05-26 16:56:36', 8);
INSERT INTO public."admin_changeLog" VALUES (29, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 16:58:50', '2022-05-26 16:58:50', 8);
INSERT INTO public."admin_changeLog" VALUES (30, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 17:00:13', '2022-05-26 17:00:13', 3);
INSERT INTO public."admin_changeLog" VALUES (31, 'Sistema Admin-sadmin', 'colaboradores', 'edicion', NULL, '2022-05-26 17:24:00', '2022-05-26 17:24:00', 12);


--
-- TOC entry 3731 (class 0 OID 23291)
-- Dependencies: 218
-- Data for Name: admin_client_colab_assoc; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_client_colab_assoc VALUES (12, 3, 9, 'true', '2022-05-19 08:22:52', '2022-05-19 08:22:52', 2, 94, 1, 'fijo', '2022-02-01', NULL);
INSERT INTO public.admin_client_colab_assoc VALUES (13, 3, 4, 'true', '2022-05-19 08:25:49', '2022-05-19 08:25:49', 60, 94, 1, 'fijo', '2022-02-01', NULL);
INSERT INTO public.admin_client_colab_assoc VALUES (14, 3, 7, 'true', '2022-05-19 08:26:53', '2022-05-19 08:26:53', 2, 94, 2, 'fijo', '2022-02-01', NULL);
INSERT INTO public.admin_client_colab_assoc VALUES (17, 3, 8, '2', '2022-05-25 11:02:22', '2022-05-25 11:03:02', 18, 95, 4, 'fijo', '2022-05-25', NULL);
INSERT INTO public.admin_client_colab_assoc VALUES (18, 3, 8, '1', '2022-05-25 11:03:20', '2022-05-25 11:03:20', 18, 94, 3, 'fijo', '2022-05-25', NULL);


--
-- TOC entry 3733 (class 0 OID 23297)
-- Dependencies: 220
-- Data for Name: admin_cobro_colaborador; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3735 (class 0 OID 23303)
-- Dependencies: 222
-- Data for Name: admin_codigos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_codigos VALUES (1, '1-CP-2022-05-1', '1', '1', '1', '123456', '500.00', 'DETERGENTE  EN POLVO', '2022-06-18', '0', '2022-05-18 14:20:33', '2022-05-18 14:20:33');
INSERT INTO public.admin_codigos VALUES (2, '1-CP-2022-05-2', '1', '1', '1', '123456', '500.00', 'DETERGENTE  EN POLVO', '2022-06-18', '0', '2022-05-18 16:04:18', '2022-05-18 16:04:18');
INSERT INTO public.admin_codigos VALUES (3, '2-CP-2022-05-3', '2', '1', '2', '1920024650', '406.80', NULL, '2022-06-26', '0', '2022-05-26 09:12:59', '2022-05-26 09:12:59');


--
-- TOC entry 3737 (class 0 OID 23312)
-- Dependencies: 224
-- Data for Name: admin_colaboradores; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_colaboradores VALUES (4, 'KAREN MELISSA', 'PULUC VASQUEZ', NULL, '1996-11-15', NULL, '3139124080501', NULL, '3139124080501', NULL, NULL, NULL, NULL, NULL, 'Mujer', 'Diplomado', 'Guatemalteco', 'Soltero', NULL, 0, NULL, '45652572', NULL, '1ra CALLE 1-79  ANEXO LOS SAUCES', 'Escuintla', 'Escuintla', 'GUSTAVO PULUC', '45652572', 'Hermano', NULL, NULL, NULL, NULL, 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Transferencia', 'BANCO G&T', 'Quincenal', NULL, NULL, NULL, NULL, NULL, 'Activo', '2021-01-09', NULL, NULL, '2022-05-11 14:32:37', '2022-05-26 16:49:57', NULL, '29', '64783', '4', NULL, 'melissa-puluc_WVJ5VY7G', '$2y$10$05CONk4Ovxckw2oi8MXT5urIu.g9icARRDzpD8.PCXsIYkqDgEdMe', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (7, 'ANNER EMMANUEL', 'CORNEL MORALES', NULL, '1985-12-24', 'supoperaciones@matrisa.com.gt', '1919679471904', '3715001-4', '185477239', '1082901283', NULL, '1919679471904', NULL, NULL, NULL, 'Diversificado', 'Guatemalteco', 'Casado', 'NIDIA ROJAS', 2, 'MECANICO AUTOMOTRIZ', '56024709', NULL, '7 av 9-60 zona 12', 'Zacapa', 'Gualán', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-11 15:29:42', '2022-05-26 16:51:09', '2023-12-24', NULL, NULL, NULL, NULL, 'anner-cornel_9iMSY4Pf', '$2y$10$NbqXiAiTy1t3zjxdjT00L.XUpeF594H8aUjC.VXzVzOItWz4E3PZ6', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (1, 'LILIAN NINETH', 'CARRILLO SANDOVAL', NULL, '1982-03-17', 'LCARRILLO@MATRISA.COM.GT', '1849923620101', '4002899-2', '282065192', '2046714610', NULL, NULL, NULL, NULL, 'Mujer', 'Licenciatura', 'Guatemalteco', 'Casado', 'JOSE DAVID DARDÓN PEREIRA', 3, 'GERENTE ADMINISTRATIVO', '30556066', '24718880', '7A. AVENIDA 9-60 APTO A COLONIA REFORMITA ZONA 12', 'Guatemala', 'Guatemala', 'JOSE DAVID DARDÓN PEREIRA', '40829056', 'ESPOSO', NULL, NULL, NULL, 'A+', 'FIJO', 'Gerente administrativa', 200, NULL, NULL, 8.33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Transferencia', NULL, 'Quincenal', NULL, NULL, NULL, NULL, NULL, 'Activo', NULL, NULL, NULL, '2022-05-10 07:49:32', '2022-05-26 10:12:38', '2024-05-10', NULL, NULL, NULL, NULL, 'Lilian-Nineth-Carrillo-Sandoval_XP3noCdp', '$2y$10$.VhDarsRCiqqGuR5zt.l5e3Edi2NImMYiO.MbZY9vV1rhjeSV4Z.C', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (10, 'CRISTIAN JOSUE', 'CRUZ HIGUEROS', NULL, '1993-08-25', 'cricelharri@gmail.com', '2303337450101', '77379918', '201200359251', NULL, NULL, '2303337450101', NULL, NULL, 'Hombre', 'Diplomado', 'Guatemalteco', 'Casado', 'ESPOSA', 1, NULL, '41333914', NULL, 'SECTOR CENTRAL 4 LOTE 5 EL CARMEN', 'Guatemala', 'Guatemala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FIJO', 'Piloto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Transferencia', 'BANCO G&T', 'Quincenal', NULL, NULL, NULL, NULL, NULL, 'Activo', '2022-01-22', NULL, NULL, '2022-05-26 17:06:02', '2022-05-26 17:06:02', NULL, '087', '0008564', '2', NULL, 'CRISTIAN-JOSUE-CRUZ-HIGUEROS_WjMWSAB6', '$2y$10$wV8frIgC7foigajzDS/s5O3UOsIb..IpylaiUl27b3iq7bp//tP9W', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (11, 'CHRISTIAN MOISES', 'SACAN DE LEON', NULL, '1990-06-14', NULL, '1743931800502', '69283869', NULL, NULL, NULL, NULL, NULL, NULL, 'Hombre', 'Diplomado', 'Guatemalteco', 'Soltero', NULL, 0, NULL, NULL, NULL, NULL, 'Escuintla', 'Escuintla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Transferencia', 'BANCO INDUSTRIAL', 'Quincenal', NULL, NULL, NULL, NULL, NULL, 'Activo', '2022-04-21', NULL, NULL, '2022-05-26 17:09:15', '2022-05-26 17:09:16', NULL, NULL, '2680021397', NULL, NULL, 'CHRISTIAN-MOISES-SACAN-DE-LEON_ZoVx4oqh', '$2y$10$XylYfYw5nmsxDNkLXLSA3.oO8ww64v7P9ZipFzH5WhZJJDo0YvhXy', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (5, 'Elian Angel de Jesus', 'Tejeda Perez', NULL, '2000-04-05', 'angeltejeda229@gmail.com', '2011408100101', '102789584', NULL, NULL, NULL, '1-4222150', NULL, NULL, 'Hombre', 'Diplomado', 'Guatemalteco', 'Soltero', NULL, 0, NULL, '36950320', '24718233', '7AV 8CALLE COLONIA PLAZA DE TOROS ZONA 13', 'Guatemala', 'Guatemala', 'Oscar Tejeda', '42790930', NULL, NULL, NULL, NULL, NULL, 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Transferencia', 'BANCO G&T', 'Quincenal', NULL, NULL, NULL, NULL, NULL, 'Activo', '2022-03-11', NULL, NULL, '2022-05-11 14:51:30', '2022-05-26 16:30:12', '2023-04-05', '087', '0009288', '7', NULL, 'angel-tejeda_7EhRHkuL', '$2y$10$jBE7Wdr1pG2QSc6lyLKxruKWdvQSzyfDs.y5hmZcpXZxDg4zDO4aa', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (9, 'ANA GABRIELA', 'VELIZ GARCIA', NULL, '1995-10-01', 'supoperaciones@matrisa.com.gt', '2383594250101', '6197223-1', '289080392', '2383594250101', NULL, '238354250101', NULL, NULL, 'Mujer', 'Licenciatura', 'Guatemalteco', 'Casado', 'LESTER LEONEL ESPAÑA CASTELLANOS', 1, 'PENSUM CERRADO ADMON EMPRESAS', '56919837', NULL, '7 av 9-60 zona 12', 'Guatemala', 'Guatemala', 'LESTER LEONEL ESPAÑA CASTELLANOS', '58400248', 'ESPOSO', NULL, NULL, NULL, 'AB+', 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-13 17:07:37', '2022-05-26 16:53:17', '2023-12-02', NULL, NULL, NULL, NULL, 'GABRIELA-VELIZ_lkDyGBOV', '$2y$10$utTUGdy.B6i60IJVWAtZO.h6WtENPECjbrCYiTeDdDFLIz57DUgGe', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (6, 'JUAN JOSE', 'PERÉZ CRUZ', NULL, '1993-03-16', 'supoperaciones@matrisa.com.gt', '2226069160101', '8197473-6', '201200771386', '1097508042', NULL, '1-4096554', NULL, NULL, 'Hombre', 'Diversificado', 'Guatemalteco', 'Soltero', NULL, 0, 'BACHILLER EN CONSTRUCCION.', '37788033', NULL, '7 av 9-60 zona 12', 'Guatemala', 'Guatemala', 'JULIETA CRUZ', '45946791', 'MAMÁ', NULL, NULL, NULL, NULL, 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Transferencia', NULL, 'Quincenal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-11 14:58:34', '2022-05-26 16:54:08', '2023-03-16', '3510742215', NULL, NULL, NULL, 'juanjo-perez_lAlvMbuq', '$2y$10$jiox/GVvV.qPFuPJ4YLdJeaoupvm/kAjNRP710jP0898eAssOnRgW', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (2, 'Brenda Yasmin', 'Velasquez de Leon', 'fotos/MUQBXlr5netTPpR3hoeswQY7uwqFrPRIGifYZTma.jpg', '1991-09-27', 'supoperaciones@matrisa.com.gt', '2076868740101', '69634114', '201402160788', '2100750048', NULL, '3076 86874 0101', NULL, NULL, 'Mujer', 'Diplomado', 'Guatemalteco', 'Soltero', NULL, 0, 'administradora', '30145040', 'v', '25 av. 19-49 zona 5 colonia la palmita', 'Guatemala', 'Guatemala', 'Brenda de Velasqez', '46003459', 'Madre', 'Eric Velasquez', '59438180', 'padre', 'A+', 'FIJO', 'Administración de planilla', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, 'Transferencia', 'BAM', 'Quincenal', NULL, NULL, NULL, NULL, NULL, 'Activo', '2016-02-04', NULL, NULL, '2022-05-11 14:24:44', '2022-05-26 16:44:30', '2023-09-27', '3', '4056901426', '4', NULL, 'yasmin-velasquez_doORhDP3', '$2y$10$EHgp8Myohv2m23h3/bNkLOyGCSAS.8lQlqSsblLh0pBY.OdznISNu', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (8, 'ALLAN DANIEL', 'NORIEGA GUERRA', NULL, '2002-01-11', NULL, '3650548010101', NULL, NULL, NULL, NULL, NULL, '1988-01-24', '0001-01-01', 'Hombre', 'Diplomado', 'Guatemalteco', 'Soltero', NULL, 0, NULL, NULL, NULL, '37CALLE B 33-59 ZONA 7 COLONIA SAKERTY 1', 'Guatemala', 'Guatemala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FIJO', 'Mensajero', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Transferencia', 'Vivibanco', 'Quincenal', NULL, NULL, NULL, NULL, NULL, 'Activo', '2022-04-01', NULL, NULL, '2022-05-12 09:23:56', '2022-05-26 16:58:50', '1988-01-24', '44082', '59200', '590224', NULL, 'Pablo-Mejía_nhaj1T0n', '$2y$10$27c6SS4XvteKfxy29ihjauF5nBOLVmI4/zWVR6NyVqvKYaYyOwasi', 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (3, 'JOSE DAVID', 'DARDON PEREIRA', NULL, '1979-05-01', 'supoperaciones@matrisa.com.gt', '1930428000101', '946017-9', '179542477', NULL, NULL, NULL, NULL, NULL, NULL, 'Maestría', NULL, 'Casado', 'LILY CARRILLO DE DARDON', 3, 'ADMINISTRACION DE EMPRESAS', '24718880', NULL, '7 av 9-60 zona 12', 'Guatemala', 'Guatemala', 'LILY CARRILLO DE DARDON', '30556066', 'ESPOSA', NULL, NULL, NULL, NULL, 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-11 14:31:51', '2022-05-26 17:00:13', NULL, NULL, NULL, NULL, NULL, 'José-Dardón_Tdhu5oy7', '$2y$10$MdOJJYiGH7GBhJRx8qHkguoSNRtZrwxl0xZBs1YCB0kqiPjkjNGcK', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_colaboradores VALUES (12, 'BRENDA NOEMI', 'VELIZ BUENAFE', NULL, '1996-01-17', 'NOEMIVELIZ275@GMAIL.COM', '3459224680101', '8924676-4', '201501863536', NULL, NULL, NULL, NULL, NULL, NULL, 'Diversificado', 'Guatemalteco', 'Soltero', NULL, 0, '4TO SEMESTRES DE PSICOLOGIA', '57117072', NULL, NULL, 'Guatemala', 'Guatemala', 'SORAYA', '53271419', 'ABUELA', NULL, NULL, NULL, NULL, 'FIJO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-26 17:10:58', '2022-05-26 17:24:00', NULL, NULL, NULL, NULL, NULL, 'BRENDA-NOEMI-BRENDA-NOEMI_booqzmbf', '$2y$10$QdKWQnKkMRFk8aenC.7dx.S6aN9s2kIjVbfi4OtmqRawl7H4gi.ZK', NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3739 (class 0 OID 23321)
-- Dependencies: 226
-- Data for Name: admin_colors; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3741 (class 0 OID 23326)
-- Dependencies: 228
-- Data for Name: admin_company; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_company VALUES (2, '2022-05-10 14:26:14', '2022-05-10 14:26:14', 'MATRISA', '80851363', 'JOSÉ DAVID DARDÓN PEREIRA', 'companylogos/2bfaH6c4efAtajNqa0wuItcDZEHdF1n2XeV.PNG', '23193392', '23193393', 'Guatemala', 'Guatemala', '7A AV, 9-60 APTO A ZONA 12 REFORMITA', NULL, 'lcarrillo@matrisa.com.gt', 'P135194', '1930428000101', 'casado', 'masculino', 'sueldo base', '1979-05-01', 'GUATE', NULL, '10.67', '1', '4.83', '1', '0', '0', '0', 'MATRISA_QgITZ7Fq', NULL, 95, '1979-05-01');
INSERT INTO public.admin_company VALUES (3, '2022-05-10 15:21:57', '2022-05-10 15:21:58', 'GRUPO MATRI', '4958565-7', 'JOSÉ DAVID DARDÓN PEREIRA', 'companylogos/jfLXt6673aJL89EzmhN9CrsFU6P8ldjAuJR.PNG', '23193392', '23193393', 'Guatemala', 'Guatemala', '7A AV. 17-58 ZONA 12', NULL, 'lcarrillo@matrisa.com.gt', 'P115968', '1930428000101', 'casado', 'masculino', 'sueldo base', '1979-05-01', 'GUATEMALTECA', NULL, '10.67', '1', '4.83', '1', '0', '0', '0', 'GRUPO MATRI_byaeaYFj', NULL, 96, '1979-05-01');
INSERT INTO public.admin_company VALUES (4, '2022-05-10 15:23:44', '2022-05-10 15:24:07', 'MATRISA TEMPORALES', '8085136-3', 'JOSÉ DAVID DARDÓN PEREIRA', 'companylogos/pGmoftFLaJVrQc0bIecoMCwOpn7Qnya7Z73.PNG', '23193392', '23193393', 'Guatemala', 'Guatemala', '7A AV, 9-60 APTO A ZONA 12 REFORMITA', NULL, 'lcarrillo@matrisa.com.gt', 'P135194', '1930428000101', 'casado', 'masculino', 'sueldo base', '1979-05-01', 'GUATEMALTECA', 'null', NULL, NULL, NULL, NULL, '0', '0', '0', 'MATRISA TEMPORALES_c1PJ5Sq3', NULL, 97, '1979-05-01');
INSERT INTO public.admin_company VALUES (5, '2022-05-10 15:29:26', '2022-05-10 15:29:26', 'MATRISA BPL', '8085136-3', 'JOSÉ DAVID DARDÓN PEREIRA', 'companylogos/AL2uNE5FB3B63Oohl8PjG640XQldn5MIhwk.PNG', '23193392', '23193393', 'Guatemala', 'Guatemala', '7A AV. 17-58 ZONA 12', NULL, 'lcarrillo@matrisa.com.gt', 'P135194', '1930428000101', 'casado', 'masculino', 'sueldo base', '1979-05-01', 'GUATEMALTECA', NULL, NULL, NULL, NULL, NULL, '0', '1', '0', 'MATRISA BPL_A6lRrLke', NULL, 98, '1979-05-01');


--
-- TOC entry 3742 (class 0 OID 23332)
-- Dependencies: 229
-- Data for Name: admin_company_client_assoc; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_company_client_assoc VALUES (3, 42, 2, '2022-05-11 14:20:02', '2022-05-11 14:20:02');
INSERT INTO public.admin_company_client_assoc VALUES (6, 43, 3, '2022-05-19 09:06:35', '2022-05-19 09:06:35');


--
-- TOC entry 3744 (class 0 OID 23337)
-- Dependencies: 231
-- Data for Name: admin_company_clients; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_company_clients VALUES (31, 'Megacomputadoras', '1819936-4', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:53:16', '2022-05-10 06:53:17', NULL, NULL, NULL, false, 'Megacomputadoras_0cEUkVU9', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (2, 'Sistemas y equipos, S.A.', '6717-2', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:38:27', '2022-05-10 06:38:27', NULL, NULL, NULL, false, 'Sistemas y equipos, S.A._rflzwXWs', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (3, 'Avícola Villalobos, S.A.', '33621-1', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:38:51', '2022-05-10 06:38:51', NULL, NULL, NULL, false, 'Avícola Villalobos, S.A._iBI3bM8a', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (4, 'DHL, S.A.', '1252273-2', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:39:04', '2022-05-10 06:39:05', NULL, NULL, NULL, false, 'DHL, S.A._pVQmrSil', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (6, 'Ecaelectrodos, S.A.', '4306798-0', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:41:17', '2022-05-10 06:41:17', NULL, NULL, NULL, false, 'Ecaelectrodos, S.A._6wvklc6e', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (7, 'Nutek Barkley', NULL, NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:41:28', '2022-05-10 06:41:29', NULL, NULL, NULL, false, 'Nutek Barkley_h0lh7wZx', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (8, 'Equipos Especializados, S.A.', '1494379-4', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:41:45', '2022-05-10 06:41:45', NULL, NULL, NULL, false, 'Equipos Especializados, S.A._kEmrWUXJ', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (9, 'INA, S.A.', '156469-2', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:41:58', '2022-05-10 06:41:58', NULL, NULL, NULL, false, 'INA, S.A._JcytDqeV', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (10, 'Inyectores de plástico, S.A.', '107880-1', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:42:12', '2022-05-10 06:42:13', NULL, NULL, NULL, false, 'Inyectores de plástico, S.A._AWwbDHk0', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (11, 'Central de empaques, S.A.', '328702-5', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:46:27', '2022-05-10 06:46:27', NULL, NULL, NULL, false, 'Central de empaques, S.A._GOOYqO2A', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (12, 'Empacadora Toledo, S.A.', '90493-7', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:47:43', '2022-05-10 06:47:44', NULL, NULL, NULL, false, 'Empacadora Toledo, S.A._TaB0Epsc', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (13, 'Muehlstein de Guatemala, S.A.', '790672-2', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:48:00', '2022-05-10 06:48:01', NULL, NULL, NULL, false, 'Muehlstein de Guatemala, S.A._SGoKOKAX', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (14, 'GENTRAC', '1251150-1', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:48:10', '2022-05-10 06:48:10', NULL, NULL, NULL, false, 'GENTRAC_nWCBP5fh', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (15, 'Carlos Chacón', '681400-K', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:48:25', '2022-05-10 06:48:25', NULL, NULL, NULL, false, 'Carlos Chacón_jDUg0vsN', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (16, 'Inversiones Masdel, S.A.', '573937-3', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:48:57', '2022-05-10 06:49:01', NULL, NULL, NULL, false, 'Inversiones Masdel, S.A._EEm3PCpI', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (17, 'Jayor Medical Guatemala, S.A.', '8753793-1', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:49:21', '2022-05-10 06:49:21', NULL, NULL, NULL, false, 'Jayor Medical Guatemala, S.A._ym6SMKMM', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (18, 'Compubodegas', '7437868-6', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:49:57', '2022-05-10 06:49:57', NULL, NULL, NULL, false, 'Compubodegas_0xvIeGQT', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (19, 'Grupo Latinoamericano de Empaques, S.A.', '128935-7', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:50:14', '2022-05-10 06:50:15', NULL, NULL, NULL, false, 'Grupo Latinoamericano de Empaques, S.A._vxtara9F', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (20, 'Jorge Mejicanos', '430323-7', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:50:27', '2022-05-10 06:50:28', NULL, NULL, NULL, false, 'Jorge Mejicanos_ZmU4mYKD', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (21, 'Unidades Móviles Automotrices, S.A.', '9541563-7', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:50:50', '2022-05-10 06:50:50', NULL, NULL, NULL, false, 'Unidades Móviles Automotrices, S.A._X3V5ZQAf', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (22, 'DISAGRO', '2670282-7', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:50:57', '2022-05-10 06:50:57', NULL, NULL, NULL, false, 'DISAGRO_Yn0oxZDo', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (23, 'Grupo Soluciona', '9366085-5', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:51:07', '2022-05-10 06:51:07', NULL, NULL, NULL, false, 'Grupo Soluciona_1w3nWsQf', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (24, 'Hill + Knowlton Strategies Guatemala, S.A.', '1955391-9', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:51:32', '2022-05-10 06:51:32', NULL, NULL, NULL, false, 'Hill + Knowlton Strategies Guatemala, S.A._DvJAZ1Kl', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (25, 'Iron Logistics, S.A.', '10802396-6', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:51:45', '2022-05-10 06:51:45', NULL, NULL, NULL, false, 'Iron Logistics, S.A._0wPvR5il', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (26, 'Molinos Modernos', '72932-9', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:51:54', '2022-05-10 06:51:55', NULL, NULL, NULL, false, 'Molinos Modernos_CsWgrsYp', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (27, 'Compañía Agromaster, S.A.', '603614-7', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:52:12', '2022-05-10 06:52:13', NULL, NULL, NULL, false, 'Compañía Agromaster, S.A._R6sS7Th6', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (28, 'Aysa Álvarez', '2251071-0', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:52:26', '2022-05-10 06:52:26', NULL, NULL, NULL, false, 'Aysa Álvarez_dKzrznwB', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (29, 'Hospital Nacional Especializado de Villa Nueva', '9651988-6', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:52:52', '2022-05-10 06:52:53', NULL, NULL, NULL, false, 'Hospital Nacional Especializado de Villa Nueva_KEKfOByX', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (30, 'Chocolates Granada', '406517-4', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:53:04', '2022-05-10 06:53:05', NULL, NULL, NULL, false, 'Chocolates Granada_ShidFUnx', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (32, 'SPADD', '1819936-4', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:53:30', '2022-05-10 06:53:30', NULL, NULL, NULL, false, 'SPADD_afIVD9sn', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (33, 'Auto Pro', '2436829-6', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:53:41', '2022-05-10 06:53:42', NULL, NULL, NULL, false, 'Auto Pro_MzkzIgSi', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (34, 'BDI Guatemala', '10829152-9', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:53:56', '2022-05-10 06:53:56', NULL, NULL, NULL, false, 'BDI Guatemala_VIf8ZfJl', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (35, 'Open Box y/o Niclas Díaz', '7532529-2', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:54:19', '2022-05-10 06:54:19', NULL, NULL, NULL, false, 'Open Box y/o Niclas Díaz_gOWaxNnk', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (36, 'Cliente nuevo', 'CF', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:54:28', '2022-05-10 06:54:28', NULL, NULL, NULL, false, 'Cliente nuevo_xZUQjgEW', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (37, '100 Montaditos', '8510601-1', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:54:41', '2022-05-10 06:54:41', NULL, NULL, NULL, false, '100 Montaditos_w6xj75vh', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (38, 'Industria Atunera Centroamericana, S.A.', '123456-6', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:54:55', '2022-05-10 06:54:55', NULL, NULL, NULL, false, 'Industria Atunera Centroamericana, S.A._0H7tkmB5', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (39, 'Crowley Logistics de Guatemala, S.A.', 'CF', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 06:55:11', '2022-05-10 06:55:12', NULL, NULL, NULL, false, 'Crowley Logistics de Guatemala, S.A._IMamfmCt', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (40, 'Proveedora de servicios', '376281-5', NULL, NULL, NULL, NULL, NULL, 'Chahal', NULL, NULL, '2022-05-10 07:34:13', '2022-05-10 07:34:14', NULL, NULL, NULL, false, 'Proveedora de servicios_v0gQSRja', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (43, 'grupo M', '4958565-7', 'lilian Carrillo', 'supoperaciones@matrisa.com.gt', '23193392', '23193393', 'Guatemala', 'Guatemala', '7A AV. 17-58 ZONA 12', 50000, '2022-05-19 08:46:06', '2022-05-19 08:46:06', 'Grupo Matri', NULL, 50000, true, 'grupo M_Jv9bAbm8', NULL, NULL);
INSERT INTO public.admin_company_clients VALUES (42, 'MATRISA', '8085136-3', 'MATRISA', 'lcarrillo@matrisa.com.gt', '23193392', '23193393', 'Guatemala', 'Guatemala', '7A AV, 9-60 APTO A ZONA 12 REFORMITA', 150000, '2022-05-11 14:19:12', '2022-05-12 12:35:50', 'MATRISA', 'null', 150000, true, 'MATRISA_PuVDdf1D', NULL, NULL);


--
-- TOC entry 3745 (class 0 OID 23344)
-- Dependencies: 232
-- Data for Name: admin_company_clients_branch_area; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_company_clients_branch_area VALUES (1, '94', 'RRHH', 'Recursos Humanos', '2022-05-12 07:51:48', '2022-05-12 07:51:48');
INSERT INTO public.admin_company_clients_branch_area VALUES (2, '94', 'operaciones', 'encargado de procesos', '2022-05-12 12:37:13', '2022-05-12 12:37:27');
INSERT INTO public.admin_company_clients_branch_area VALUES (3, '94', 'SUPÉRVISION', 'SUPERVISAR AREAS DE  TRABAJO', '2022-05-13 17:03:05', '2022-05-13 17:03:05');
INSERT INTO public.admin_company_clients_branch_area VALUES (4, '95', 'Prueba', 'prueba', '2022-05-16 09:36:23', '2022-05-16 09:36:23');


--
-- TOC entry 3747 (class 0 OID 23352)
-- Dependencies: 234
-- Data for Name: admin_company_clients_branchof; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_company_clients_branchof VALUES (4, 'Sur Occidente', 'Sur Occidente', '000000000', '000000000', '12345678', 2, '2022-05-10 06:56:22', '2022-05-10 06:56:22', 4);
INSERT INTO public.admin_company_clients_branchof VALUES (5, 'Central', 'Central', '000000000', '000000000', '12345678', 2, '2022-05-10 06:57:29', '2022-05-10 06:57:29', 5);
INSERT INTO public.admin_company_clients_branchof VALUES (6, 'Flota', 'Flota', '000000000', '000000000', '12345678', 2, '2022-05-10 06:57:47', '2022-05-10 06:57:47', 6);
INSERT INTO public.admin_company_clients_branchof VALUES (7, 'Móvil', 'Móvil', '000000000', '000000000', '12345678', 2, '2022-05-10 06:57:58', '2022-05-10 06:57:58', 7);
INSERT INTO public.admin_company_clients_branchof VALUES (8, 'Aliansa', 'Aliansa', '000000000', '000000000', '12345678', 2, '2022-05-10 06:58:10', '2022-05-10 06:58:10', 8);
INSERT INTO public.admin_company_clients_branchof VALUES (9, 'Nor Oriente', 'Nor Oriente', '000000000', '000000000', '12345678', 2, '2022-05-10 06:58:22', '2022-05-10 06:58:22', 9);
INSERT INTO public.admin_company_clients_branchof VALUES (10, 'Santa Lucía', 'Santa Lucía', '000000000', '000000000', '12345678', 3, '2022-05-10 06:59:46', '2022-05-10 06:59:46', 10);
INSERT INTO public.admin_company_clients_branchof VALUES (11, 'Distribuidora Central', 'Distribuidora Central', '000000000', '000000000', '12345678', 3, '2022-05-10 07:00:01', '2022-05-10 07:00:01', 11);
INSERT INTO public.admin_company_clients_branchof VALUES (12, 'Planta 1', 'Planta 1', '000000000', '000000000', '12345678', 3, '2022-05-10 07:00:25', '2022-05-10 07:00:25', 12);
INSERT INTO public.admin_company_clients_branchof VALUES (13, 'Planta 2', 'Planta 2', '000000000', '000000000', '12345678', 3, '2022-05-10 07:00:34', '2022-05-10 07:00:34', 13);
INSERT INTO public.admin_company_clients_branchof VALUES (14, 'Distribuidora Planta 2', 'Distribuidora Planta 2', '000000000', '000000000', '12345678', 3, '2022-05-10 07:00:46', '2022-05-10 07:00:46', 14);
INSERT INTO public.admin_company_clients_branchof VALUES (15, 'Harinas', 'Harinas', '000000000', '000000000', '12345678', 3, '2022-05-10 07:00:57', '2022-05-10 07:00:57', 15);
INSERT INTO public.admin_company_clients_branchof VALUES (16, 'Distribuidora Xela', 'Distribuidora Xela', '000000000', '000000000', '12345678', 3, '2022-05-10 07:01:17', '2022-05-10 07:01:17', 16);
INSERT INTO public.admin_company_clients_branchof VALUES (17, 'Distribuidora Zacapa', 'Distribuidora Zacapa', '000000000', '000000000', '12345678', 3, '2022-05-10 07:01:41', '2022-05-10 07:01:41', 17);
INSERT INTO public.admin_company_clients_branchof VALUES (18, 'Distribuidora Suroriente', 'Distribuidora Zacapa', '000000000', '000000000', '12345678', 3, '2022-05-10 07:03:40', '2022-05-10 07:03:40', 18);
INSERT INTO public.admin_company_clients_branchof VALUES (19, 'Distribuidora Petén', 'Distribuidora Petén', '000000000', '000000000', '12345678', 3, '2022-05-10 07:03:51', '2022-05-10 07:03:51', 19);
INSERT INTO public.admin_company_clients_branchof VALUES (20, 'Distribuidora Los Encuentros', 'Distribuidora Los Encuentros', '000000000', '000000000', '12345678', 3, '2022-05-10 07:04:07', '2022-05-10 07:04:07', 20);
INSERT INTO public.admin_company_clients_branchof VALUES (21, 'Distribuidora Chimaltenango', 'Distribuidora Chimaltenango', '000000000', '000000000', '12345678', 3, '2022-05-10 07:04:42', '2022-05-10 07:04:42', 21);
INSERT INTO public.admin_company_clients_branchof VALUES (22, 'Distribuidora Coatepeque', 'Distribuidora Coatepeque', '000000000', '000000000', '12345678', 3, '2022-05-10 07:06:25', '2022-05-10 07:06:25', 22);
INSERT INTO public.admin_company_clients_branchof VALUES (23, 'Distribuidora Huehuetenango', 'Distribuidora Huehuetenango', '000000000', '000000000', '12345678', 3, '2022-05-10 07:06:38', '2022-05-10 07:06:38', 23);
INSERT INTO public.admin_company_clients_branchof VALUES (24, 'Distribuidora Los Esclavos', 'Distribuidora Los Esclavos', '000000000', '000000000', '12345678', 3, '2022-05-10 07:06:49', '2022-05-10 07:06:49', 24);
INSERT INTO public.admin_company_clients_branchof VALUES (25, 'Distribuidora Puerto Barrios', 'Distribuidora Puerto Barrios', '000000000', '000000000', '12345678', 3, '2022-05-10 07:07:05', '2022-05-10 07:07:05', 25);
INSERT INTO public.admin_company_clients_branchof VALUES (26, 'Distribuidora Cobán', 'Distribuidora Cobán', '000000000', '000000000', '12345678', 3, '2022-05-10 07:07:19', '2022-05-10 07:07:19', 26);
INSERT INTO public.admin_company_clients_branchof VALUES (27, 'Distribuidora Jutiapa', 'Distribuidora Jutiapa', '000000000', '000000000', '12345678', 3, '2022-05-10 07:07:34', '2022-05-10 07:07:34', 27);
INSERT INTO public.admin_company_clients_branchof VALUES (28, 'Distribuidora Mazatenango', 'Distribuidora Mazatenango', '000000000', '000000000', '12345678', 3, '2022-05-10 07:07:46', '2022-05-10 07:07:46', 28);
INSERT INTO public.admin_company_clients_branchof VALUES (29, 'Distribuidora CIA Amatitlan', 'Distribuidora CIA Amatitlan', '000000000', '000000000', '12345678', 3, '2022-05-10 07:08:03', '2022-05-10 07:08:03', 29);
INSERT INTO public.admin_company_clients_branchof VALUES (30, 'Planta Central', 'Planta Central', '000000000', '000000000', '12345678', 4, '2022-05-10 07:08:39', '2022-05-10 07:08:39', 30);
INSERT INTO public.admin_company_clients_branchof VALUES (31, 'Bodega', 'Bodega', '000000000', '000000000', '12345678', 4, '2022-05-10 07:08:51', '2022-05-10 07:08:51', 31);
INSERT INTO public.admin_company_clients_branchof VALUES (32, 'Lavado Flotilla', 'Lavado Flotilla', '000000000', '000000000', '12345678', 4, '2022-05-10 07:09:13', '2022-05-10 07:09:13', 32);
INSERT INTO public.admin_company_clients_branchof VALUES (33, 'Mecánica Flotilla', 'Mecánica Flotilla', '000000000', '000000000', '12345678', 4, '2022-05-10 07:09:27', '2022-05-10 07:09:27', 33);
INSERT INTO public.admin_company_clients_branchof VALUES (42, 'Planta de producción', 'Planta de producción', '000000000', '000000000', '12345678', 6, '2022-05-10 07:14:56', '2022-05-10 07:14:56', 42);
INSERT INTO public.admin_company_clients_branchof VALUES (43, 'Proyectos Horno', 'Proyectos Horno', '000000000', '000000000', '12345678', 6, '2022-05-10 07:15:05', '2022-05-10 07:15:05', 43);
INSERT INTO public.admin_company_clients_branchof VALUES (44, 'ECA', 'ECA', '000000000', '000000000', '12345678', 7, '2022-05-10 07:20:22', '2022-05-10 07:20:22', 44);
INSERT INTO public.admin_company_clients_branchof VALUES (45, 'Central', 'Central', '000000000', '000000000', '12345678', 8, '2022-05-10 07:22:55', '2022-05-10 07:22:55', 45);
INSERT INTO public.admin_company_clients_branchof VALUES (46, 'Pollolandia', 'Pollolandia', '000000000', '000000000', '12345678', 9, '2022-05-10 07:23:14', '2022-05-10 07:23:14', 46);
INSERT INTO public.admin_company_clients_branchof VALUES (47, 'Central', 'Central', '000000000', '000000000', '12345678', 10, '2022-05-10 07:23:32', '2022-05-10 07:23:32', 47);
INSERT INTO public.admin_company_clients_branchof VALUES (48, 'Central', 'Central', '000000000', '000000000', '12345678', 11, '2022-05-10 07:24:12', '2022-05-10 07:24:12', 48);
INSERT INTO public.admin_company_clients_branchof VALUES (49, 'Reciclados', 'Reciclados', '000000000', '000000000', '12345678', 11, '2022-05-10 07:24:25', '2022-05-10 07:24:25', 49);
INSERT INTO public.admin_company_clients_branchof VALUES (50, 'ALMARSA', 'ALMARSA', '000000000', '000000000', '12345678', 11, '2022-05-10 07:24:40', '2022-05-10 07:24:40', 50);
INSERT INTO public.admin_company_clients_branchof VALUES (55, 'Central', 'Central', '000000000', '000000000', '12345678', 14, '2022-05-10 07:32:01', '2022-05-10 07:32:01', 55);
INSERT INTO public.admin_company_clients_branchof VALUES (56, 'Central', 'Central', '000000000', '000000000', '12345678', 15, '2022-05-10 07:32:31', '2022-05-10 07:32:31', 56);
INSERT INTO public.admin_company_clients_branchof VALUES (57, 'Central', 'Central', '000000000', '000000000', '12345678', 40, '2022-05-10 07:34:32', '2022-05-10 07:34:32', 57);
INSERT INTO public.admin_company_clients_branchof VALUES (52, 'Planta procesadora', 'Planta procesadora', '000000000', '000000000', '12345678', 12, '2022-05-10 07:26:34', '2022-05-10 07:37:52', 52);
INSERT INTO public.admin_company_clients_branchof VALUES (51, 'Mayapan', 'Mayapan', '000000000', '000000000', '12345678', 12, '2022-05-10 07:26:24', '2022-05-10 07:37:40', 51);
INSERT INTO public.admin_company_clients_branchof VALUES (53, 'Central', 'Central', '000000000', '000000000', '12345678', 13, '2022-05-10 07:28:48', '2022-05-10 07:38:39', 53);
INSERT INTO public.admin_company_clients_branchof VALUES (54, 'Desactivado', 'Desactivado', '000000000', '000000000', '12345678', 13, '2022-05-10 07:29:00', '2022-05-10 07:39:06', 54);
INSERT INTO public.admin_company_clients_branchof VALUES (58, 'Mary Kay', 'Mary Kay', '000000000', '000000000', '12345678', 16, '2022-05-10 07:39:50', '2022-05-10 07:39:50', 58);
INSERT INTO public.admin_company_clients_branchof VALUES (59, 'Central', 'Central', '000000000', '000000000', '12345678', 17, '2022-05-10 07:40:18', '2022-05-10 07:40:18', 59);
INSERT INTO public.admin_company_clients_branchof VALUES (60, 'Central', 'Central', '000000000', '000000000', '12345678', 18, '2022-05-10 07:42:06', '2022-05-10 07:42:06', 60);
INSERT INTO public.admin_company_clients_branchof VALUES (61, 'Central', 'Central', '000000000', '000000000', '123', 19, '2022-05-10 07:42:13', '2022-05-10 07:42:13', 61);
INSERT INTO public.admin_company_clients_branchof VALUES (62, 'Central', 'Central', '000000000', '000000000', '12345678', 21, '2022-05-10 07:42:54', '2022-05-10 07:42:54', 62);
INSERT INTO public.admin_company_clients_branchof VALUES (63, 'Planta 2', 'Planta 2', '000000000', '000000000', '12345678', 20, '2022-05-10 07:43:21', '2022-05-10 07:43:21', 63);
INSERT INTO public.admin_company_clients_branchof VALUES (64, 'Puerto Barrios', 'Puerto Barrios', '000000000', '000000000', '12345678', 22, '2022-05-10 07:43:56', '2022-05-10 07:43:56', 64);
INSERT INTO public.admin_company_clients_branchof VALUES (65, 'Ciudad de Guatemala', 'Ciudad de Guatemala', '000000000', '000000000', '12345678', 23, '2022-05-10 07:44:20', '2022-05-10 07:44:20', 65);
INSERT INTO public.admin_company_clients_branchof VALUES (66, 'Escuintla', 'Escuintla', '000000000', '000000000', '12345678', 23, '2022-05-10 07:44:42', '2022-05-10 07:44:42', 66);
INSERT INTO public.admin_company_clients_branchof VALUES (67, 'Central', 'Central', '000000000', '000000000', '12345678', 24, '2022-05-10 07:45:04', '2022-05-10 07:45:04', 67);
INSERT INTO public.admin_company_clients_branchof VALUES (68, 'Distribuidora Retalhuleu', 'Distribuidora Retalhuleu', '000000000', '000000000', '12345678', 25, '2022-05-10 07:47:15', '2022-05-10 07:47:15', 68);
INSERT INTO public.admin_company_clients_branchof VALUES (69, 'Distribuidora Mazatenango', 'Distribuidora Mazatenango', '000000000', '000000000', '12345678', 25, '2022-05-10 07:47:25', '2022-05-10 07:47:25', 69);
INSERT INTO public.admin_company_clients_branchof VALUES (70, 'Escuintla', 'Escuintla', '000000000', '000000000', '12345678', 26, '2022-05-10 08:15:09', '2022-05-10 08:15:09', 70);
INSERT INTO public.admin_company_clients_branchof VALUES (71, 'Oficinas Centrales', 'Oficinas Centrales', '000000000', '000000000', '12345678', 26, '2022-05-10 08:15:19', '2022-05-10 08:15:19', 71);
INSERT INTO public.admin_company_clients_branchof VALUES (72, 'Santa Rosa', 'Santa Rosa', '000000000', '000000000', '12345678', 26, '2022-05-10 08:15:32', '2022-05-10 08:15:32', 72);
INSERT INTO public.admin_company_clients_branchof VALUES (73, 'Taxisco', 'Taxisco', '000000000', '000000000', '12345678', 26, '2022-05-10 08:15:44', '2022-05-10 08:15:44', 73);
INSERT INTO public.admin_company_clients_branchof VALUES (74, 'Mensajería', 'Mensajería', '000000000', '000000000', '12345678', 27, '2022-05-10 08:16:08', '2022-05-10 08:16:08', 74);
INSERT INTO public.admin_company_clients_branchof VALUES (75, 'Villa Nueva', 'Villa Nueva', '000000000', '000000000', '12345678', 28, '2022-05-10 08:16:37', '2022-05-10 08:16:37', 75);
INSERT INTO public.admin_company_clients_branchof VALUES (76, 'Comercialización', 'Comercialización', '000000000', '000000000', '12345678', 29, '2022-05-10 08:17:39', '2022-05-10 08:17:39', 76);
INSERT INTO public.admin_company_clients_branchof VALUES (77, 'Central', 'Central', '000000000', '000000000', '12345678', 30, '2022-05-10 08:18:17', '2022-05-10 08:18:17', 77);
INSERT INTO public.admin_company_clients_branchof VALUES (78, 'Ciudad', 'Ciudad', '000000000', '000000000', '12345678', 31, '2022-05-10 08:18:43', '2022-05-10 08:18:43', 78);
INSERT INTO public.admin_company_clients_branchof VALUES (79, 'Cool Tshirt', 'Cool Tshirt', '000000000', '000000000', '12345678', 32, '2022-05-10 08:19:11', '2022-05-10 08:19:11', 79);
INSERT INTO public.admin_company_clients_branchof VALUES (80, 'Pincel Turquesa', 'Pincel Turquesa', '000000000', '000000000', '12345678', 32, '2022-05-10 08:19:26', '2022-05-10 08:19:26', 80);
INSERT INTO public.admin_company_clients_branchof VALUES (81, 'Oficinas Centrales', 'Oficinas Centrales', '000000000', '000000000', '12345678', 32, '2022-05-10 08:19:36', '2022-05-10 08:19:36', 81);
INSERT INTO public.admin_company_clients_branchof VALUES (82, 'Ciudad', 'Ciudad', '000000000', '000000000', '12345678', 33, '2022-05-10 08:20:02', '2022-05-10 08:20:02', 82);
INSERT INTO public.admin_company_clients_branchof VALUES (83, 'Ciudad', 'Ciudad', '000000000', '000000000', '12345678', 34, '2022-05-10 08:20:08', '2022-05-10 08:20:08', 83);
INSERT INTO public.admin_company_clients_branchof VALUES (84, 'Mensajería', 'Mensajería', '000000000', '000000000', '12345678', 35, '2022-05-10 08:20:39', '2022-05-10 08:20:39', 84);
INSERT INTO public.admin_company_clients_branchof VALUES (85, 'Vistares', 'Vistares', '000000000', '000000000', '12345678', 37, '2022-05-10 08:21:17', '2022-05-10 08:21:17', 85);
INSERT INTO public.admin_company_clients_branchof VALUES (86, 'Naranjo', 'Naranjo', '000000000', '000000000', '12345678', 37, '2022-05-10 08:21:32', '2022-05-10 08:21:32', 86);
INSERT INTO public.admin_company_clients_branchof VALUES (87, 'Condado Concepción', 'Condado Concepción', '000000000', '000000000', '12345678', 37, '2022-05-10 08:21:50', '2022-05-10 08:21:50', 87);
INSERT INTO public.admin_company_clients_branchof VALUES (88, 'Alamos', 'Alamos', '000000000', '000000000', '12345678', 37, '2022-05-10 08:22:03', '2022-05-10 08:22:03', 88);
INSERT INTO public.admin_company_clients_branchof VALUES (89, 'Fontabella', 'Fontabella', '000000000', '000000000', '12345678', 37, '2022-05-10 08:22:14', '2022-05-10 08:22:14', 89);
INSERT INTO public.admin_company_clients_branchof VALUES (90, 'Majadas', 'Majadas', '000000000', '000000000', '12345678', 37, '2022-05-10 08:22:25', '2022-05-10 08:22:25', 90);
INSERT INTO public.admin_company_clients_branchof VALUES (91, 'Rambla', 'Rambla', '000000000', '000000000', '12345678', 37, '2022-05-10 08:22:36', '2022-05-10 08:22:36', 91);
INSERT INTO public.admin_company_clients_branchof VALUES (92, 'Jealsa Escuintla', 'Jealsa Escuintla', '000000000', '000000000', '12345678', 39, '2022-05-10 08:22:59', '2022-05-10 08:22:59', 92);
INSERT INTO public.admin_company_clients_branchof VALUES (93, 'Alpasa Bodega #5', 'Alpasa Bodega #5', '000000000', '000000000', '12345678', 39, '2022-05-10 08:23:10', '2022-05-10 08:23:10', 93);
INSERT INTO public.admin_company_clients_branchof VALUES (94, 'Zona 12', 'zona 12', '1111', '111223', '546464', 42, '2022-05-12 07:51:33', '2022-05-12 07:51:33', 99);
INSERT INTO public.admin_company_clients_branchof VALUES (95, 'SUPERVISION', '7AV 9-60 ZONA 12', '14.606915420856426', '90.54022036092088', '23193392', 42, '2022-05-13 17:01:31', '2022-05-13 17:01:31', 100);


--
-- TOC entry 3750 (class 0 OID 23362)
-- Dependencies: 237
-- Data for Name: admin_company_clients_jobs; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3752 (class 0 OID 23367)
-- Dependencies: 239
-- Data for Name: admin_company_clients_jobs_todo; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3754 (class 0 OID 23372)
-- Dependencies: 241
-- Data for Name: admin_company_colab_assoc; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_company_colab_assoc VALUES (1, 2, 2, 'true', '2022-05-11 14:28:31', '2022-05-11 14:28:31');
INSERT INTO public.admin_company_colab_assoc VALUES (2, 2, 4, 'true', '2022-05-11 14:50:06', '2022-05-11 14:50:06');
INSERT INTO public.admin_company_colab_assoc VALUES (3, 2, 3, 'true', '2022-05-11 14:50:09', '2022-05-11 14:50:09');
INSERT INTO public.admin_company_colab_assoc VALUES (4, 2, 1, 'true', '2022-05-11 14:50:14', '2022-05-11 14:50:14');
INSERT INTO public.admin_company_colab_assoc VALUES (5, 3, 5, 'true', '2022-05-11 14:51:50', '2022-05-11 14:51:50');
INSERT INTO public.admin_company_colab_assoc VALUES (6, 2, 6, 'true', '2022-05-11 15:02:48', '2022-05-11 15:02:48');
INSERT INTO public.admin_company_colab_assoc VALUES (7, 2, 7, 'true', '2022-05-12 07:49:28', '2022-05-12 07:49:28');
INSERT INTO public.admin_company_colab_assoc VALUES (8, 2, 8, 'true', '2022-05-12 09:24:18', '2022-05-12 09:24:18');
INSERT INTO public.admin_company_colab_assoc VALUES (9, 2, 9, 'true', '2022-05-13 17:08:35', '2022-05-13 17:08:35');
INSERT INTO public.admin_company_colab_assoc VALUES (10, 2, 8, '1', '2022-05-25 08:58:46', '2022-05-25 08:58:46');


--
-- TOC entry 3757 (class 0 OID 23380)
-- Dependencies: 244
-- Data for Name: admin_contratos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_contratos VALUES (1, 'FIJO', 'INDEFINIDO', NULL, '2022-05-19 08:52:37', '2022-05-19 08:52:37');
INSERT INTO public.admin_contratos VALUES (2, 'TEMPORAL', 'SEIS MESES', NULL, '2022-05-19 08:52:57', '2022-05-19 08:52:57');


--
-- TOC entry 3759 (class 0 OID 23388)
-- Dependencies: 246
-- Data for Name: admin_conversiones; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_conversiones VALUES (1, 1, 1, 'multiplicar', 1.00, 'Un(a) kilogramo tiene  libras', '2022-05-18 13:34:45', '2022-05-18 13:34:45');


--
-- TOC entry 3761 (class 0 OID 23396)
-- Dependencies: 248
-- Data for Name: admin_cotis; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3763 (class 0 OID 23405)
-- Dependencies: 250
-- Data for Name: admin_cumples; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3765 (class 0 OID 23413)
-- Dependencies: 252
-- Data for Name: admin_documentos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_documentos VALUES (1, 'CONTRATO', 'CONTRATO NUEVO TRABAJADOR', NULL, NULL, '2022-05-19 08:59:14', '2022-05-19 08:59:14');


--
-- TOC entry 3767 (class 0 OID 23421)
-- Dependencies: 254
-- Data for Name: admin_equipo_asignado; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3769 (class 0 OID 23429)
-- Dependencies: 256
-- Data for Name: admin_equipos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3771 (class 0 OID 23437)
-- Dependencies: 258
-- Data for Name: admin_facturas; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_facturas VALUES (1, 1, '123456', 'AF', 500.00, '2022-05-18', 'DETERGENTE  EN POLVO', 'cuadrada', 95, '2022-05-18 14:20:10', '2022-05-18 16:04:18', 'producto');
INSERT INTO public.admin_facturas VALUES (2, 2, '1920024650', '81E58AEA', 406.80, '2022-05-24', NULL, 'cuadrada', 95, '2022-05-26 09:12:24', '2022-05-26 09:12:59', 'producto');


--
-- TOC entry 3773 (class 0 OID 23446)
-- Dependencies: 260
-- Data for Name: admin_horary; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_horary VALUES (1, 'Asistente-de-Recursos-Humanos-1_5NT7jG1Y', 'Lunes', '08:00:00', '17:00:00', NULL, NULL, NULL, '2022-05-16 09:33:01', '2022-05-16 09:33:01');
INSERT INTO public.admin_horary VALUES (4, 'Carwashero_3vJ3E2y4', 'Lunes', '08:00:00', '17:00:00', NULL, NULL, NULL, '2022-05-25 10:40:43', '2022-05-25 10:40:43');
INSERT INTO public.admin_horary VALUES (5, 'Carwashero_3vJ3E2y4', 'Martes', '08:00:00', '17:00:00', NULL, NULL, NULL, '2022-05-25 10:40:43', '2022-05-25 10:40:43');
INSERT INTO public.admin_horary VALUES (6, 'Carwashero_3vJ3E2y4', 'Miércoles', '08:00:00', '17:00:00', NULL, NULL, NULL, '2022-05-25 10:40:43', '2022-05-25 10:40:43');
INSERT INTO public.admin_horary VALUES (7, 'Carwashero_3vJ3E2y4', 'Jueves', '08:00:00', '17:00:00', NULL, NULL, NULL, '2022-05-25 10:40:43', '2022-05-25 10:40:43');
INSERT INTO public.admin_horary VALUES (8, 'Carwashero_3vJ3E2y4', 'Viernes', '08:00:00', '17:00:00', NULL, NULL, NULL, '2022-05-25 10:40:43', '2022-05-25 10:40:43');


--
-- TOC entry 3775 (class 0 OID 23454)
-- Dependencies: 262
-- Data for Name: admin_loggs_user_paiments; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3777 (class 0 OID 23463)
-- Dependencies: 264
-- Data for Name: admin_mensajes; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_mensajes VALUES (1, '1', '6', NULL, NULL, 'Sistema Admin', 'juanjo perez', 'jajaja', 'hablale a tu jefa', '0', '0', NULL, '2022-05-11 15:05:48', '2022-05-11 15:05:48');


--
-- TOC entry 3778 (class 0 OID 23471)
-- Dependencies: 265
-- Data for Name: admin_mensajes_grupos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3781 (class 0 OID 23481)
-- Dependencies: 268
-- Data for Name: admin_petitions_complaintments; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_petitions_complaintments VALUES (1, 'esto es una queja', '0', NULL, NULL, 1, '2022-05-10 07:58:07', '2022-05-10 07:58:07');
INSERT INTO public.admin_petitions_complaintments VALUES (2, 'Jajekdwjaloqnwjejsnakks', '0', NULL, NULL, 6, '2022-05-11 15:01:50', '2022-05-11 15:01:50');
INSERT INTO public.admin_petitions_complaintments VALUES (3, 'Jajekdwjaloqnwjejsnakks', '0', NULL, NULL, 6, '2022-05-11 15:01:58', '2022-05-11 15:01:58');
INSERT INTO public.admin_petitions_complaintments VALUES (4, 'Jajekdwjaloqnwjejsnakks', '0', NULL, NULL, 6, '2022-05-11 15:02:00', '2022-05-11 15:02:00');
INSERT INTO public.admin_petitions_complaintments VALUES (5, 'Jajekdwjaloqnwjejsnakks', '0', NULL, NULL, 6, '2022-05-11 15:02:02', '2022-05-11 15:02:02');
INSERT INTO public.admin_petitions_complaintments VALUES (6, 'Jajekdwjaloqnwjejsnakks', '0', NULL, NULL, 6, '2022-05-11 15:02:04', '2022-05-11 15:02:04');
INSERT INTO public.admin_petitions_complaintments VALUES (7, 'Jajekdwjaloqnwjejsnakks', '0', NULL, NULL, 6, '2022-05-11 15:02:14', '2022-05-11 15:02:14');
INSERT INTO public.admin_petitions_complaintments VALUES (8, 'He tenido muchos inconvenientes con mi jefa', '0', NULL, NULL, 6, '2022-05-11 15:02:41', '2022-05-11 15:02:41');


--
-- TOC entry 3783 (class 0 OID 23490)
-- Dependencies: 270
-- Data for Name: admin_petitions_economicpetitions; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_petitions_economicpetitions VALUES (1, 'Prestamo', 'Aguinaldo', NULL, 'mensaje de prueba', NULL, '0', NULL, NULL, NULL, 1, '2022-05-10 07:57:21', '2022-05-10 07:57:21');


--
-- TOC entry 3785 (class 0 OID 23499)
-- Dependencies: 272
-- Data for Name: admin_petitions_restoretimepermissions; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_petitions_restoretimepermissions VALUES (1, '2022-05-10', '2022-05-14', '2022-05-17', NULL, 'mensaje de prueba', NULL, NULL, '0', NULL, NULL, 1, '2022-05-10 07:56:31', '2022-05-10 07:56:31');


--
-- TOC entry 3787 (class 0 OID 23508)
-- Dependencies: 274
-- Data for Name: admin_petitions_simplepetitions; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3789 (class 0 OID 23513)
-- Dependencies: 276
-- Data for Name: admin_petitions_simplepetitions_save; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3791 (class 0 OID 23522)
-- Dependencies: 278
-- Data for Name: admin_petitions_timepermissions; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3793 (class 0 OID 23531)
-- Dependencies: 280
-- Data for Name: admin_productos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_productos VALUES (1, 'DET1', 'DETERGENTE', 'DETERGENTE EN POLVO 5KG', 1, 1, 1, 5, 50.00, 100.00, '2022-05-18 14:18:30', '2022-05-18 14:18:30');
INSERT INTO public.admin_productos VALUES (2, 'MEC2', 'MECHAS DE TRAPEADOR', 'MECHAS PARA TRAPEADOR DE LIMPIEZA', 1, 2, 3, 5, 50.00, 50.00, '2022-05-26 09:11:33', '2022-05-26 09:11:33');


--
-- TOC entry 3795 (class 0 OID 23539)
-- Dependencies: 282
-- Data for Name: admin_products; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3797 (class 0 OID 23547)
-- Dependencies: 284
-- Data for Name: admin_providers; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_providers VALUES (1, 'CLEMATIS', '8 CALLE ZONA 13', '4958565-7', 'GERARDO', '30950320', '2022-05-18 14:19:31', '2022-05-18 14:19:31', 'supoperaciones@matrisa.com.gt');
INSERT INTO public.admin_providers VALUES (2, 'disdel', '27 calle 1-41 zona 3', '4630629-3', 'Julissa Guzmán', '24226120', '2022-05-26 09:09:22', '2022-05-26 09:09:22', NULL);


--
-- TOC entry 3799 (class 0 OID 23555)
-- Dependencies: 286
-- Data for Name: admin_puestos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_puestos VALUES (2, 'Administración de planilla', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:23:59', '2022-05-10 08:24:19', 0, 0, 0, 0, 0, 0, 0, 0, 'Administración-de-planilla_NXlurZye', 'Administración-de-planilla_8mtkJHfe');
INSERT INTO public.admin_puestos VALUES (3, 'Albañil', 'ENCARGADO DE VELAR POR EL MANTENIMIENTO DE OBRAS CIVILES', '2022-05-10 08:24:26', '2022-05-10 08:24:26', 0, 0, 0, 0, 0, 0, 0, 0, 'Albañil_312jT1jN', 'Albañil_7goemPOk');
INSERT INTO public.admin_puestos VALUES (4, 'Asistente de operaciones', 'La posición se hará cargo de la parte administrativa del área de operaciones y logística, será responsable de supervisar y controlar la recepción de órdenes de trabajo, así como de la correcta y puntual digitación. Llevará control sobre la asistencia del personal técnico y operativo. Emitirá facturas y llevará el control de las cuentas por pagar y salvaguardar las contraseñas de pago. Será encargada de gestionar reportería semanal y mensual por cliente, área, empleado. Brindar soporte y seguimiento a los proyectos de soldadura y unidades moviles.', '2022-05-10 08:24:36', '2022-05-10 08:24:36', 0, 0, 0, 0, 0, 0, 0, 0, 'Asistente-de-operaciones_YWca253n', 'Asistente-de-operaciones_hVx33nbg');
INSERT INTO public.admin_puestos VALUES (5, 'Asistente de Recursos Humanos 1', 'Analizar actividades técnicas y/o administrativas relacionadas con el recurso humano, desarrollando y ejecutando planes y programas pertinentes a la administración de personal, a fin de lograr y mantener un buen clima laboral.', '2022-05-10 08:25:00', '2022-05-10 08:25:00', 0, 0, 0, 0, 0, 0, 0, 0, 'Asistente-de-Recursos-Humanos-1_81uErrY7', 'Asistente-de-Recursos-Humanos-1_5NT7jG1Y');
INSERT INTO public.admin_puestos VALUES (6, 'Asistente de Recursos Humanos 2', 'Encargado de realizar procesos de Reclutamiento, Selección, contratatación, capacitación, inducción y desvinculación. Brindará apoyo a la gestión administrativa y operativa del depto. para su óptimo desarrollo. Realizará jornadas de reclutamiento en el interior de la República, dara apoyo a la atención de candidatos via redes sociales. Así mismo, Analizara actividades técnicas y/o administrativas relacionadas con el departamento de recursos humanos, desarrollará y ejecutará planes y programas pertinentes a la administración de personal, a fin de lograr y mantener un buen clima laboral.', '2022-05-10 08:25:11', '2022-05-10 08:25:11', 0, 0, 0, 0, 0, 0, 0, 0, 'Asistente-de-Recursos-Humanos-2_bEYGpbCq', 'Asistente-de-Recursos-Humanos-2_7POd6b4f');
INSERT INTO public.admin_puestos VALUES (7, 'Auxiliar de bodega', 'Llevar al día el inventario de los productos que la empresa comercializa y que almacena para su posterior distribución. Enargado de la recepción, Almacenamiento y preparación para la distribución de productos. Por otro lado, es el encargado de garantizar las óptimas condiciones, en cuanto a limpieza y seguridad, del lugar de almacenamiento.', '2022-05-10 08:25:20', '2022-05-10 08:25:20', 0, 0, 0, 0, 0, 0, 0, 0, 'Auxiliar-de-bodega_FmQPbjQP', 'Auxiliar-de-bodega_uf8MwtCQ');
INSERT INTO public.admin_puestos VALUES (8, 'Auxiliar de empaque', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:25:31', '2022-05-10 08:25:31', 0, 0, 0, 0, 0, 0, 0, 0, 'Auxiliar-de-empaque_4wSIbH5r', 'Auxiliar-de-empaque_2sX7WG8R');
INSERT INTO public.admin_puestos VALUES (9, 'Auxiliar de mecánico', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:26:28', '2022-05-10 08:26:28', 0, 0, 0, 0, 0, 0, 0, 0, 'Auxiliar-de-mecánico_QbdT9JtS', 'Auxiliar-de-mecánico_Qd0guH3J');
INSERT INTO public.admin_puestos VALUES (10, 'Auxiliar de mecánico industrial', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:26:42', '2022-05-10 08:26:42', 0, 0, 0, 0, 0, 0, 0, 0, 'Auxiliar-de-mecánico-industrial_9BirTkZB', 'Auxiliar-de-mecánico-industrial_JJb4cUH6');
INSERT INTO public.admin_puestos VALUES (11, 'Auxiliar de refrigeración', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:26:57', '2022-05-10 08:26:57', 0, 0, 0, 0, 0, 0, 0, 0, 'Auxiliar-de-refrigeración_m2pU4FC2', 'Auxiliar-de-refrigeración_87igVteL');
INSERT INTO public.admin_puestos VALUES (12, 'Auxiliar de soldador', 'Auxiliar en la reparación de piezas y fabricacion de estructuras metálicas, utilizando distrintos procesos de soldadura, de acuerdo a normas y parametros establecidos. asi como en areas segun sean requeridos', '2022-05-10 08:27:05', '2022-05-10 08:27:26', 0, 0, 0, 0, 0, 0, 0, 0, 'Auxiliar-de-soldador_bVeCVJXR', 'Auxiliar-de-soldador_YFTB1eCR');
INSERT INTO public.admin_puestos VALUES (13, 'Auxiliar general', 'Auxiliar en distintas áreas como producción, soldadura, mantenimiento Industrial, automotriz y/o áreas comunes entre otras áreas en distintas tareas  teniendo en cuenta el apoyo indispensable en las áreas antes mencionada así como el seguimiento de normas y parámetros establecidos en cada una de estas como apoyo principal para la mejor ejecución de las tareas asignadas tanto individuales como grupales', '2022-05-10 08:27:44', '2022-05-10 08:27:44', 0, 0, 0, 0, 0, 0, 0, 0, 'Auxiliar-general_2ob7Oa80', 'Auxiliar-general_NbTvnHUN');
INSERT INTO public.admin_puestos VALUES (14, 'Ayudante técnico industrial 1', 'Auxiliar en distintas áreas como producción, soldadura, mantenimiento Industrial, automotriz y/o áreas comunes entre otras áreas en distintas tareas  teniendo en cuenta el apoyo indispensable en las áreas antes mencionada así como el seguimiento de normas y parámetros establecidos en cada una de estas como apoyo principal para la mejor ejecución de las tareas asignadas tanto individuales como grupales', '2022-05-10 08:28:01', '2022-05-10 08:28:01', 0, 0, 0, 0, 0, 0, 0, 0, 'Ayudante-técnico-industrial-1_5lyeTHTj', 'Ayudante-técnico-industrial-1_o5rHhEKA');
INSERT INTO public.admin_puestos VALUES (15, 'Ayudante técnico industrial 2', 'Auxiliar en distintas áreas como producción, soldadura, mantenimiento Industrial, automotriz y/o áreas comunes entre otras áreas en distintas tareas  teniendo en cuenta el apoyo indispensable en las áreas antes mencionada así como el seguimiento de normas y parámetros establecidos en cada una de estas como apoyo principal para la mejor ejecución de las tareas asignadas tanto individuales como grupales', '2022-05-10 08:28:11', '2022-05-10 08:28:11', 0, 0, 0, 0, 0, 0, 0, 0, 'Ayudante-técnico-industrial-2_bMfn723R', 'Ayudante-técnico-industrial-2_FLNWLC1r');
INSERT INTO public.admin_puestos VALUES (16, 'Bodega de llantas', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:28:22', '2022-05-10 08:28:22', 0, 0, 0, 0, 0, 0, 0, 0, 'Bodega-de-llantas_SsiGAjaX', 'Bodega-de-llantas_D567ETbC');
INSERT INTO public.admin_puestos VALUES (17, 'Bodeguero', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:28:28', '2022-05-10 08:28:28', 0, 0, 0, 0, 0, 0, 0, 0, 'Bodeguero_9sh2gXjg', 'Bodeguero_eeOsZJk1');
INSERT INTO public.admin_puestos VALUES (18, 'Carwashero', 'Recibe instrucciones sobre los diferentes tipos de servicios de limpieza interior y exterior para cada diferente tipo de vehículo. Es responsable del adecuado uso de los útiles y enseres de limpieza, así como encargado del manejo, cuidado y mantenimiento de los equipos eléctricos como son aspiradora, hidro lavadora entre otros. Es responsable de dar aviso oportuno a su supervisor sobre el mantenimiento preventivo y correctivo de las máquinas que tiene a su cargo.', '2022-05-10 08:28:39', '2022-05-10 08:28:39', 0, 0, 0, 0, 0, 0, 0, 0, 'Carwashero_CHo9SvyI', 'Carwashero_3vJ3E2y4');
INSERT INTO public.admin_puestos VALUES (19, 'Conserje', 'Ser el encargado de la limpieza en áreas administrativas, operativas o en donde le sea requerido. Velando por la limpieza de pisos, muebles, ventaneria, manteniendo el orden y limpieza de cada área diariamente, así como, apoyar en actividades de servicio al cliente para los invitados a reuniones, proveedores y/o personal administrativo. Será responsable del buen manejo de los enseres e insumos de limpieza que le sean entregados, así como de pedir al supervisor de operaciones de MATRISA el re abastecimiento oportuno de los mismos.', '2022-05-10 08:49:57', '2022-05-10 08:49:57', 0, 0, 0, 0, 0, 0, 0, 0, 'Conserje_hQseBPl1', 'Conserje_qlVlNjpi');
INSERT INTO public.admin_puestos VALUES (20, 'Control de calidad', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:51:31', '2022-05-10 08:51:31', 0, 0, 0, 0, 0, 0, 0, 0, 'Control-de-calidad_6pdCxbXN', 'Control-de-calidad_I2tAXVw8');
INSERT INTO public.admin_puestos VALUES (21, 'Cotizador', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:51:39', '2022-05-10 08:51:39', 0, 0, 0, 0, 0, 0, 0, 0, 'Cotizador_rAdXbDjs', 'Cotizador_jFW8Rw7q');
INSERT INTO public.admin_puestos VALUES (22, 'Dibujante Autocad', 'El levantamiento de diseño de maquinaria, equipo e inmueble y todos los levantamientos adicionales que se le solicite,  encargado de plasma las ideas en dibujos y esquemas para ser interpretadas posteriormente en obra. El desarrollo de planos según sean necesarios con sus respectivas mediciones', '2022-05-10 08:51:52', '2022-05-10 08:51:52', 0, 0, 0, 0, 0, 0, 0, 0, 'Dibujante-Autocad_zJdCiHMb', 'Dibujante-Autocad_Pu5896hU');
INSERT INTO public.admin_puestos VALUES (23, 'Dibujante Autocad C/E', 'El levantamiento de diseño de maquinaria, equipo e inmueble y todos los levantamientos adicionales que se le solicite,  encargado de plasma las ideas en dibujos y esquemas para ser interpretadas posteriormente en obra. El desarrollo de planos según sean necesarios con sus respectivas mediciones', '2022-05-10 08:52:04', '2022-05-10 08:52:04', 0, 0, 0, 0, 0, 0, 0, 0, 'Dibujante-Autocad-C/E_cPYcrKMj', 'Dibujante-Autocad-C/E_e1mkFO3B');
INSERT INTO public.admin_puestos VALUES (24, 'Dibujante Autocad S/E', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:52:19', '2022-05-10 08:52:19', 0, 0, 0, 0, 0, 0, 0, 0, 'Dibujante-Autocad-S/E_GFTI30ps', 'Dibujante-Autocad-S/E_Fhn5nNZp');
INSERT INTO public.admin_puestos VALUES (25, 'Digitador de datos', 'Digitación y tabulación de guías, órdenes de servicio, emisión de reportes varios, llevar el control de los ingresos y egresos al inventario de bodega, elaboración de cotizaciones y compra de herramientas e insumos para la empresa. Introducir y actualizar documentos y bases de datos en formato físico a digital, asi mismo, transcribir de datos. Realizar reportes dinámicos, a través de herramientas digitales.', '2022-05-10 08:52:32', '2022-05-10 08:52:32', 0, 0, 0, 0, 0, 0, 0, 0, 'Digitador-de-datos_frRrGQqN', 'Digitador-de-datos_UFXIv26f');
INSERT INTO public.admin_puestos VALUES (26, 'Electricista Industrial Tipo A', 'Instalar y proveer mantenimiento a equipo y máquinas eléctricas, asi como circuitos eléctricos de mando, alumbrado, fuerza y señalizacion, de acuerdo a especificaciones tecnicas solicitadas en areas industriales, asi como realizar diagnostivos preventivos y reparaciones correctivas realizando los reportes correspondientes.', '2022-05-10 08:52:48', '2022-05-10 08:52:48', 0, 0, 0, 0, 0, 0, 0, 0, 'Electricista-Industrial-Tipo-A_KXBkQ38p', 'Electricista-Industrial-Tipo-A_OtoLXQWI');
INSERT INTO public.admin_puestos VALUES (27, 'Electricista Industrial Tipo B', 'Instalar y proveer mantenimiento a equipo y máquinas eléctricas, asi como circuitos eléctricos de mando, alumbrado, fuerza y señalizacion, de acuerdo a especificaciones tecnicas solicitadas en areas industriales, asi como realizar diagnostivos preventivos y reparaciones correctivas realizando los reportes correspondientes.', '2022-05-10 08:52:58', '2022-05-10 08:52:58', 0, 0, 0, 0, 0, 0, 0, 0, 'Electricista-Industrial-Tipo-B_e44K1A9s', 'Electricista-Industrial-Tipo-B_z4C3Bkku');
INSERT INTO public.admin_puestos VALUES (28, 'Electricista Industrial Tipo C', 'Instalar y proveer mantenimiento a equipo y máquinas eléctricas, asi como circuitos eléctricos de mando, alumbrado, fuerza y señalizacion, de acuerdo a especificaciones tecnicas solicitadas en areas industriales, asi como realizar diagnostivos preventivos y reparaciones correctivas realizando los reportes correspondientes.', '2022-05-10 08:53:07', '2022-05-10 08:53:07', 0, 0, 0, 0, 0, 0, 0, 0, 'Electricista-Industrial-Tipo-C_erzbhPw1', 'Electricista-Industrial-Tipo-C_jqSecInZ');
INSERT INTO public.admin_puestos VALUES (29, 'Electromecánico Automotriz', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:53:17', '2022-05-10 08:54:49', 0, 0, 0, 0, 0, 0, 0, 0, 'Electrónico-Automotriz_rLaAgdnw', 'Electrónico-Automotriz_Nh6RZKU2');
INSERT INTO public.admin_puestos VALUES (30, 'Electromecánico por productividad', 'Diagnosticar, reparar y/o mantener circuitos eléctricos automotrices. Así como organizar y ejecutar el proceso de diagnóstico y reparación de circuitos eléctricos, acondicionar el vehículo y estar capacitado para verificar el estado funcional del sistema. Entre sus funciones se pueden destacar las tres siguientes: Localización de averías: detección de averías en el sistema mecánico, hidráulico, neumático y eléctrico de los diferentes vehículos para su posterior reparación. Tanto livianos como pesados de Gasolina o Diesel. Realizar reparación de motores, sistemas auxiliares y todos los elementos que forman parte de un vehículo como la caja de cambios, frenos, luces, sensores, inyección, etc. Así como la sustitución y ajuste en la suspensión y dirección.', '2022-05-10 08:55:05', '2022-05-10 08:55:05', 0, 0, 0, 0, 0, 0, 0, 0, 'Electromecánico-por-productividad_k8Fxj8rl', 'Electromecánico-por-productividad_GWOy66lK');
INSERT INTO public.admin_puestos VALUES (31, 'Electromecánico tipo A', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:55:16', '2022-05-10 08:55:16', 0, 0, 0, 0, 0, 0, 0, 0, 'Electromecánico-tipo-A_k5jx8zeq', 'Electromecánico-tipo-A_XKNy5dGK');
INSERT INTO public.admin_puestos VALUES (32, 'Electromecánico tipo B', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:55:25', '2022-05-10 08:55:25', 0, 0, 0, 0, 0, 0, 0, 0, 'Electromecánico-tipo-B_jPIRwJ6S', 'Electromecánico-tipo-B_RhK3Xr47');
INSERT INTO public.admin_puestos VALUES (33, 'Encargado de bodega', 'mantener el orden y control como encargado, velando por el correcto manejo de los insumos entregado reportes solicitados asi como realizar los cuadres respectivos manejando de manera ordenada y responsable cada uno de los valores que sean puestos a disposicion, asi como velar por que no se manejen faltantes en los controles que se manejen,', '2022-05-10 08:55:35', '2022-05-10 08:55:35', 0, 0, 0, 0, 0, 0, 0, 0, 'Encargado-de-bodega_QqhmuAcH', 'Encargado-de-bodega_ByXmX6Zs');
INSERT INTO public.admin_puestos VALUES (34, 'Gerente administrativa', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:55:47', '2022-05-10 08:55:47', 0, 0, 0, 0, 0, 0, 0, 0, 'Gerente-administrativa_EvNhQelB', 'Gerente-administrativa_bIry2F5e');
INSERT INTO public.admin_puestos VALUES (35, 'Gerente general', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:55:55', '2022-05-10 08:55:55', 0, 0, 0, 0, 0, 0, 0, 0, 'Gerente-general_Ul3vT5SX', 'Gerente-general_NaCCJ1OU');
INSERT INTO public.admin_puestos VALUES (36, 'Herrería industrial', 'Programar, distribuir y supervisar el trabajo del personal de la Sección de Soldadura. Es responsable de la calidad de los trabajos que efectúa la unidad, Participar en la ejecución de trabajos de soldadura y realiza trabajos especiales, así como participar diariamente en la ejecución de labores de soldadura dentro y fuera del taller, asi como efectuar trabajos de herrería en general. Entre sus tareas específicas se encuntran: 1.Programar diariamente las actividades de los trabajos dentro y fuera del área designada. Recibe y atiende la requisición(OT), Verifica la petición del trabajo solicitado con la requisición y los materiales existentes, evalúa y programa el día para observar el área donde se efectuará el trabajo, presenta el informe al Supervisor de mantenimiento para que tome la decisión respecto al trabajo. 2. Elabora diariamente el presupuesto de materiales y equipo de soldadura, Recibir la orden de servicio, Verificar el área y toma las medidas, Confeccionar el presupuesto según las especificaciones del trabajo requerido. 3. Distribuir diariamente las órdenes de trabajo al personal y las entrega al equipo y herramientas de soldadura y materiales. 4. Designar al personal según la complejidad del trabajo. 5. Supervisar diariamente  al personal de soldadura de menor jerarquía. 6. Verificar y corregir el trabajo que se realiza dentro y  fuera del taller. 7. Orientar al personal a que ejecuten sus labores en forma rápida y práctica. 8. Verificar y llevar el control quincenalmente  de la existencia de materiales de uso en la sección de soldadura. 9. Revisar y seleccionar los materiales que se van a utilizar en los trabajos solicitados en las distintas áreas.10. Solicitar el abastecimiento de materiales y equipos de soldaduras a servicios administrativos. 11. Controlar la asistencia del personal a su cargo.', '2022-05-10 08:56:07', '2022-05-10 08:56:07', 0, 0, 0, 0, 0, 0, 0, 0, 'Herrería-industrial_BI8i32U7', 'Herrería-industrial_TEIzBfqo');
INSERT INTO public.admin_puestos VALUES (37, 'Herrería industrial 2', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:56:22', '2022-05-10 08:56:22', 0, 0, 0, 0, 0, 0, 0, 0, 'Herrería-industrial-2_J0PSdN9e', 'Herrería-industrial-2_tzTMpaal');
INSERT INTO public.admin_puestos VALUES (38, 'Limpia concensadores', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:56:37', '2022-05-10 08:56:37', 0, 0, 0, 0, 0, 0, 0, 0, 'Limpia-concensadores_7y7aZlii', 'Limpia-concensadores_eLke9TEQ');
INSERT INTO public.admin_puestos VALUES (39, 'Mantenimiento', 'Encargado del mantenimiento y limpieza de areas de trabajo dentro de la sede asignada velando por la inocuidad de las areas en las cuales sean de produccion, y demas areas que sean asignadas o requeridas', '2022-05-10 08:56:46', '2022-05-10 08:56:46', 0, 0, 0, 0, 0, 0, 0, 0, 'Mantenimiento_LHT5yglV', 'Mantenimiento_ZTHZdcX2');
INSERT INTO public.admin_puestos VALUES (40, 'Mantenimiento áreas comunes', 'Encargado del mantenimiento y limpieza de areas de trabajo dentro de la sede asignada velando por la inocuidad de las areas en las cuales sean de produccion, y demas areas que sean asignadas o requeridas', '2022-05-10 08:57:15', '2022-05-10 08:57:15', 0, 0, 0, 0, 0, 0, 0, 0, 'Mantenimiento-áreas-comunes_hNmRS8Yl', 'Mantenimiento-áreas-comunes_yd4824NG');
INSERT INTO public.admin_puestos VALUES (41, 'Mecánico automotriz', 'Servicios mecánicos automotrices para transporte liviano y pesado en las actividades de mantenimiento preventivo y correctivo que se requieren, estas pueden ser mecánicas o eléctricas según correspondan y programadas segun requierimiento de taller asi como el seguimiento a los requierimientos que las unidades  tengan tanto administrativas como tecnicas mecanicas.', '2022-05-10 08:57:23', '2022-05-10 08:57:23', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-automotriz_b5UKTY8y', 'Mecánico-automotriz_B1nsgwgz');
INSERT INTO public.admin_puestos VALUES (42, 'Mecánico automotriz por productividad', 'Servicios mecánicos automotrices para transporte liviano y pesado en las actividades de mantenimiento preventivo y correctivo que se requieren, estas pueden ser mecánicas o eléctricas según correspondan y programadas segun requierimiento de taller asi como el seguimiento a los requierimientos que las unidades  tengan tanto administrativas como tecnicas mecanicas.', '2022-05-10 08:57:33', '2022-05-10 08:57:33', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-automotriz-por-productividad_ZEWws7qV', 'Mecánico-automotriz-por-productividad_2C6epBWr');
INSERT INTO public.admin_puestos VALUES (43, 'Mecánico automotriz por productividad (+)', 'Servicios mecánicos automotrices para transporte liviano y pesado en las actividades de mantenimiento preventivo y correctivo que se requieren, estas pueden ser mecánicas o eléctricas según correspondan y programadas segun requierimiento de taller asi como el seguimiento a los requierimientos que las unidades  tengan tanto administrativas como tecnicas mecanicas.', '2022-05-10 08:58:28', '2022-05-10 08:58:28', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-automotriz-por-productividad-(+)_nWyMhIpm', 'Mecánico-automotriz-por-productividad-(+)_LkZO3eBo');
INSERT INTO public.admin_puestos VALUES (44, 'Mecánico automotriz tipo A', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:58:39', '2022-05-10 08:58:39', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-automotriz-tipo-A_JLDfs5mO', 'Mecánico-automotriz-tipo-A_98kfEhPn');
INSERT INTO public.admin_puestos VALUES (45, 'Mecánico automotriz tipo B', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:58:47', '2022-05-10 08:58:47', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-automotriz-tipo-B_oiQrXqCb', 'Mecánico-automotriz-tipo-B_Cu6B61S9');
INSERT INTO public.admin_puestos VALUES (46, 'Mecánico automotriz tipo C', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:58:55', '2022-05-10 08:58:55', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-automotriz-tipo-C_3dXliuS9', 'Mecánico-automotriz-tipo-C_LpikfyPd');
INSERT INTO public.admin_puestos VALUES (47, 'Mecánico auxiliar', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:59:03', '2022-05-10 08:59:03', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-auxiliar_QeNbpgJQ', 'Mecánico-auxiliar_QrHylX9r');
INSERT INTO public.admin_puestos VALUES (48, 'Mecánico de flota', 'Servicios mecánicos automotrices para transporte liviano y pesado en las actividades de mantenimiento preventivo y correctivo que se requieren, estas pueden ser mecánicas o eléctricas según correspondan y programadas segun requierimiento de taller asi como el seguimiento a los requierimientos que las unidades  tengan tanto administrativas como tecnicas mecanicas.', '2022-05-10 08:59:13', '2022-05-10 08:59:13', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-de-flota_PkYJbkiS', 'Mecánico-de-flota_U7UwAH0g');
INSERT INTO public.admin_puestos VALUES (49, 'Mecánico industrial tipo A', 'Encargado de desempeñar con eficiencia funciones de instalación, montaje, operación, mantenimiento y reparación de máquinas, equipos mecánicos, hidráulicos y neumáticos asi como realizar trabajos de electricidad industrial como tambien realizar manteminientos electricos, realizar mecanica de ajustes, procesos de soldadura, teniendo en cuenta la programacion de los mantenimientos', '2022-05-10 08:59:22', '2022-05-10 08:59:22', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-industrial-tipo-A_s7QK8sYI', 'Mecánico-industrial-tipo-A_aQPIVSO5');
INSERT INTO public.admin_puestos VALUES (50, 'Mecánico industrial tipo B', 'Encargado de desempeñar con eficiencia funciones de instalación, montaje, operación, mantenimiento y reparación de máquinas, equipos mecánicos, hidráulicos y neumáticos asi como realizar trabajos de electricidad industrial como tambien realizar manteminientos electricos, realizar mecanica de ajustes, procesos de soldadura, teniendo en cuenta la programacion de los mantenimientos y diagnosticos asi como realizar los reportes y notificaciones correspondientes.', '2022-05-10 08:59:37', '2022-05-10 08:59:37', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-industrial-tipo-B_83wYYfxY', 'Mecánico-industrial-tipo-B_7Xa8mv5u');
INSERT INTO public.admin_puestos VALUES (51, 'Mecánico industrial tipo C', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:59:46', '2022-05-10 08:59:46', 0, 0, 0, 0, 0, 0, 0, 0, 'Mecánico-industrial-tipo-C_I8EJhPwI', 'Mecánico-industrial-tipo-C_5mmHekL8');
INSERT INTO public.admin_puestos VALUES (52, 'Mensajero', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 08:59:53', '2022-05-10 08:59:53', 0, 0, 0, 0, 0, 0, 0, 0, 'Mensajero_YLpsmnLT', 'Mensajero_d4ufd7My');
INSERT INTO public.admin_puestos VALUES (53, 'Mensajero con moto', 'Revisión y limpieza del vehículo y la lonchera en los días asignados. Abastecer de combustible el vehículo. Mantener saldo de redes en su dispositivo móvil. Recibir y revisar la ruta asignada con su respectiva documentación. (Despacho) Revisar cantidades, tipo, empaque y estado del producto detallado en cada factura, liquidar diariamente ruta con albaranes y cobros. Realizar depósitos monetarios en agencias bancarias y realizar gestiones administrativas.', '2022-05-10 09:00:20', '2022-05-10 09:00:20', 0, 0, 0, 0, 0, 0, 0, 0, 'Mensajero-con-moto_m8GFIAtI', 'Mensajero-con-moto_HaaOu03P');
INSERT INTO public.admin_puestos VALUES (54, 'OP asistente 1', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 09:00:29', '2022-05-10 09:00:29', 0, 0, 0, 0, 0, 0, 0, 0, 'OP-asistente-1_ETI2taiP', 'OP-asistente-1_tzcEFDmM');
INSERT INTO public.admin_puestos VALUES (55, 'OP Supervisor 1', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 09:00:42', '2022-05-10 09:00:42', 0, 0, 0, 0, 0, 0, 0, 0, 'OP-Supervisor-1_aUZ5w3hO', 'OP-Supervisor-1_hr7JxXFX');
INSERT INTO public.admin_puestos VALUES (56, 'Operario de limpieza', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 09:00:51', '2022-05-10 09:00:51', 0, 0, 0, 0, 0, 0, 0, 0, 'Operario-de-limpieza_jIOX28y6', 'Operario-de-limpieza_M3HKpger');
INSERT INTO public.admin_puestos VALUES (57, 'Operario de producción', 'controlar y realizar los procesos de recepción, manipulación, transformación y elaboración de productos alimenticios para su posterior almacenamiento, expedición, transporte y comercialización', '2022-05-10 09:01:01', '2022-05-10 09:01:01', 0, 0, 0, 0, 0, 0, 0, 0, 'Operario-de-producción_EuEJkFTu', 'Operario-de-producción_vfuKE6ux');
INSERT INTO public.admin_puestos VALUES (58, 'Piloto', 'Responsable de conducir diversos vehículos (livianos como camiones de 1 a 12 toneladas, así como Cabezales con sistema de arrastre llevando mercadería y/o productos dentro de la ciudad capital y al interior de la República o movimientos dentro de la sede asignada), de combustión diesel y gasolina. Deberá apoyar en cargar y descargar el producto de los camiones, llevar el control del mismo, velar por el buen estado de la unidad así como reportar las fallas considerables para evitar contratiempos en los traslados. Es responsabilidad del Piloto asignado responsable, conducir el vehículo de acuerdo a lo indicado en las leyes y reglamentos de tránsito y a los preceptos de educación vial. Es responsabilidad del Piloto, si se produciera un accidente comunicarlo de inmediato con la empresa corredora de seguros automovilísticos e informar la situación. Si se produjeran daños a los medios de transporte o responsabildiad civil, por negligencia, descuido o irresponsabilidad en la utilización, y deliberadamente se hubiere ocultado, será responsabilidad absoluta del piloto asignado del vehículo; según corresponda al responsable de la omisión de información y los gastos que originen las reparaciones serán cubiertas por el pilotor. sin prejuicio de la aplicación de las sanciones disciplinarias si el caso lo amerita, previo al trámite administrativo correspondiente.', '2022-05-10 09:01:10', '2022-05-10 09:01:10', 0, 0, 0, 0, 0, 0, 0, 0, 'Piloto_NPHngTB2', 'Piloto_HfLgRl4h');
INSERT INTO public.admin_puestos VALUES (59, 'Piloto administrativo', 'Recopilación de papelería, cobros, tramites de facturas, digitación de horas del personal de cada unidad de negocio', '2022-05-10 09:01:23', '2022-05-10 09:01:23', 0, 0, 0, 0, 0, 0, 0, 0, 'Piloto-administrativo_wBAObg4g', 'Piloto-administrativo_fZtBpitB');
INSERT INTO public.admin_puestos VALUES (60, 'Recepcionista', 'Atención de planta telefónica central, trato y atención con proveedores, manejo y control de atención a personal interno, en la sede asignada, controles de ordenes de trabajo asi como el seguimiento a los procesos solicitados.', '2022-05-10 09:01:35', '2022-05-10 09:01:35', 0, 0, 0, 0, 0, 0, 0, 0, 'Recepcionista_mekgc1hx', 'Recepcionista_ddrzk2kF');
INSERT INTO public.admin_puestos VALUES (61, 'Receptor de vehículos', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 09:01:49', '2022-05-10 09:01:49', 0, 0, 0, 0, 0, 0, 0, 0, 'Receptor-de-vehículos_Oe1jgmf8', 'Receptor-de-vehículos_sc5RFg4i');
INSERT INTO public.admin_puestos VALUES (62, 'Receptor de vehículos 2', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 09:01:57', '2022-05-10 09:01:57', 0, 0, 0, 0, 0, 0, 0, 0, 'Receptor-de-vehículos-2_Hip67KPs', 'Receptor-de-vehículos-2_YXxm7tv2');
INSERT INTO public.admin_puestos VALUES (63, 'RH Asistente 1', 'XXXXXXXX---ACTUALIZAR FUNCIONES DEL PUESTO---XXXXXXX', '2022-05-10 09:02:09', '2022-05-10 09:02:09', 0, 0, 0, 0, 0, 0, 0, 0, 'RH-Asistente-1_G1ATP5vY', 'RH-Asistente-1_XOKiqDI3');


--
-- TOC entry 3801 (class 0 OID 23563)
-- Dependencies: 288
-- Data for Name: admin_roles; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_roles VALUES (1, 'Sadmin', 'su00', '2020-09-11 07:04:39', '2021-02-13 10:45:44');


--
-- TOC entry 3803 (class 0 OID 23568)
-- Dependencies: 290
-- Data for Name: admin_sizes; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3805 (class 0 OID 23573)
-- Dependencies: 292
-- Data for Name: admin_solicitudes; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3807 (class 0 OID 23581)
-- Dependencies: 294
-- Data for Name: admin_stock; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_stock VALUES (2, 1, 3, 1, 5.00, 15.00, 99, '2022-05-19 09:19:42', '2022-05-19 09:19:58');
INSERT INTO public.admin_stock VALUES (1, 1, 7, 1, 5.00, 35.00, 95, '2022-05-18 16:03:49', '2022-05-26 09:28:40');
INSERT INTO public.admin_stock VALUES (3, 2, 18, 3, 22.00, 396.00, 95, '2022-05-26 09:27:58', '2022-05-26 09:28:40');


--
-- TOC entry 3808 (class 0 OID 23584)
-- Dependencies: 295
-- Data for Name: admin_stock_egresos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_stock_egresos VALUES (1, 1, 3, 1, 3, '1', 5.00, 15.00, NULL, 300.00, 'bodega', NULL, NULL, 95, NULL, NULL, 99, NULL, NULL, '2022-05-19 09:18:29', '2022-05-19 09:18:29');


--
-- TOC entry 3809 (class 0 OID 23590)
-- Dependencies: 296
-- Data for Name: admin_stock_egresos_equipo; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3812 (class 0 OID 23597)
-- Dependencies: 299
-- Data for Name: admin_stock_equipos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3815 (class 0 OID 23605)
-- Dependencies: 302
-- Data for Name: admin_stock_ingresos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_stock_ingresos VALUES (1, 1, 10, 1, 10, '1', 5.00, 500.00, 95, 1, '2022-05-18 14:20:33', '2022-05-18 14:20:33');
INSERT INTO public.admin_stock_ingresos VALUES (3, 1, 3, 1, 3, '1', 5.00, 15.00, 99, NULL, '2022-05-19 09:18:29', '2022-05-19 09:18:29');
INSERT INTO public.admin_stock_ingresos VALUES (4, 2, 18, 3, 18, '3', 22.00, 406.80, 95, 2, '2022-05-26 09:12:59', '2022-05-26 09:12:59');


--
-- TOC entry 3816 (class 0 OID 23608)
-- Dependencies: 303
-- Data for Name: admin_stock_ingresos_equipo; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3819 (class 0 OID 23615)
-- Dependencies: 306
-- Data for Name: admin_subCategories; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public."admin_subCategories" VALUES (1, 'LAVANDERIA', '2022-05-18 14:17:38', '2022-05-18 14:17:38', 1);
INSERT INTO public."admin_subCategories" VALUES (2, 'CONSERJERIA', '2022-05-26 09:10:26', '2022-05-26 09:10:26', 1);


--
-- TOC entry 3821 (class 0 OID 23620)
-- Dependencies: 308
-- Data for Name: admin_tasks; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3823 (class 0 OID 23628)
-- Dependencies: 310
-- Data for Name: admin_unidades; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_unidades VALUES (1, 'kilogramo', 'peso', 'kilogramo', '2022-05-18 12:23:35', '2022-05-18 12:23:35');
INSERT INTO public.admin_unidades VALUES (2, 'libras', 'peso', 'libras', '2022-05-18 12:24:41', '2022-05-18 12:24:41');
INSERT INTO public.admin_unidades VALUES (3, 'paquete', 'otro', 'unidad', '2022-05-26 09:06:57', '2022-05-26 09:06:57');
INSERT INTO public.admin_unidades VALUES (4, 'UNIDAD', 'otro', NULL, '2022-05-26 09:13:53', '2022-05-26 09:13:53');


--
-- TOC entry 3825 (class 0 OID 23636)
-- Dependencies: 312
-- Data for Name: admin_users; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.admin_users VALUES (1, 'Sistema Admin', 'sadmin', '$2y$10$3aCx1cm1Sriq4ImSZrQKdO6Ms4cMJxiCOA3XzrwCRj7Fu0ztas0Lu', '1', NULL, 'su00', 1, 1, '2020-09-11 07:04:39', '2021-02-13 11:09:34', NULL);


--
-- TOC entry 3827 (class 0 OID 23642)
-- Dependencies: 314
-- Data for Name: admin_usersadmin; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3829 (class 0 OID 23650)
-- Dependencies: 316
-- Data for Name: admin_warehouse; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3831 (class 0 OID 23658)
-- Dependencies: 318
-- Data for Name: admin_werehouse_products; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3833 (class 0 OID 23663)
-- Dependencies: 320
-- Data for Name: descuentos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3835 (class 0 OID 23673)
-- Dependencies: 322
-- Data for Name: log_descuentos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3837 (class 0 OID 23679)
-- Dependencies: 324
-- Data for Name: log_liquidacion; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3839 (class 0 OID 23687)
-- Dependencies: 326
-- Data for Name: log_prestaciones; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3841 (class 0 OID 23695)
-- Dependencies: 328
-- Data for Name: loggs_user; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.loggs_user VALUES (4, 'sin latitud', 'sin longitud', NULL, NULL, NULL, NULL, 'sin latitud', 'sin longitud', NULL, NULL, 'true', NULL, NULL, '06:29:43', '00:00:00', '00:00:00', '12:08:43', '06:29:43', 'true', '00:00:00', 'true', '00:00:00', '00:00:00', NULL, '01:31:00', 3, NULL, '00:00:00', '00:00:00', NULL, NULL, '00:00:00', NULL, NULL, 'Jueves', '2022-05-26', NULL, NULL, 87394318, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 3, 94, 42, 18, 7, 2, '2022-05-26 06:29:42', '2022-05-26 12:08:44', NULL, NULL, NULL, NULL, 0, 'asistencia', NULL, NULL, NULL);
INSERT INTO public.loggs_user VALUES (5, 'sin latitud', 'sin longitud', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, '14:25:32', '00:00:00', '00:00:00', '00:00:00', '14:25:32', 'false', '06:25:00', 'true', '00:00:00', '00:00:00', NULL, '00:00:00', 0, NULL, '00:00:00', '00:00:00', NULL, NULL, '00:00:00', NULL, NULL, 'Jueves', '2022-05-26', NULL, NULL, 87394318, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 3, 94, 42, 18, 7, 2, '2022-05-26 14:25:33', '2022-05-26 14:25:33', NULL, NULL, NULL, NULL, 0, 'asistencia', NULL, NULL, NULL);


--
-- TOC entry 3843 (class 0 OID 23717)
-- Dependencies: 330
-- Data for Name: loggs_user_planilla; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3845 (class 0 OID 23725)
-- Dependencies: 332
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.migrations VALUES (1, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO public.migrations VALUES (2, '2021_02_10_004756_create_admin_roles_table', 1);
INSERT INTO public.migrations VALUES (3, '2021_02_10_005223_create_admin_users_table', 1);
INSERT INTO public.migrations VALUES (4, '2021_02_10_230758_create_admin_access_table', 1);
INSERT INTO public.migrations VALUES (5, '2021_02_10_233048_create_admin_access_roles_table', 1);
INSERT INTO public.migrations VALUES (6, '2021_02_20_010744_create_admin_products_table', 1);
INSERT INTO public.migrations VALUES (7, '2021_02_20_165149_create_admin_warehouse_table', 1);
INSERT INTO public.migrations VALUES (8, '2021_02_22_192957_create_admin_sizes_table', 1);
INSERT INTO public.migrations VALUES (9, '2021_02_22_193311_create_admin_colors_table', 1);
INSERT INTO public.migrations VALUES (10, '2021_02_22_194656_create_admin_werehouse_products_table', 1);
INSERT INTO public.migrations VALUES (11, '2021_05_27_120000_create_admin_categories_table', 1);
INSERT INTO public.migrations VALUES (12, '2021_05_27_120000_create_admin_subCategories_table', 1);
INSERT INTO public.migrations VALUES (13, '2021_08_10_162200_create_admin_puestos_table', 1);
INSERT INTO public.migrations VALUES (14, '2021_08_23_120000_create_admin_providers_table', 1);
INSERT INTO public.migrations VALUES (15, '2021_08_23_120000_create_admin_solicitudes_table', 1);
INSERT INTO public.migrations VALUES (16, '2021_09_27_120000_create_admin_colaboradores_table', 1);
INSERT INTO public.migrations VALUES (17, '2021_09_27_191338_create_admin_usersadmin_table', 1);
INSERT INTO public.migrations VALUES (18, '2021_09_28_014335_create_admin_comanies_table', 1);
INSERT INTO public.migrations VALUES (19, '2021_09_28_065247_create_admin_company_clients_table', 1);
INSERT INTO public.migrations VALUES (20, '2021_09_28_065347_create_admin_company_client_assoc_table', 1);
INSERT INTO public.migrations VALUES (21, '2021_09_28_164934_create_admin_company_clients_jobs_table', 1);
INSERT INTO public.migrations VALUES (22, '2021_09_28_170026_create_admin_company_clients_jobs_todo_table', 1);
INSERT INTO public.migrations VALUES (23, '2021_09_29_025533_create_admin_company_clients_branchof_table', 1);
INSERT INTO public.migrations VALUES (24, '2021_09_29_030059_update_admin_puestos__table', 1);
INSERT INTO public.migrations VALUES (25, '2021_09_29_120000_create_admin_client_colab_assoc_table', 1);
INSERT INTO public.migrations VALUES (26, '2021_09_29_120000_create_admin_company_colab_assoc_table', 1);
INSERT INTO public.migrations VALUES (27, '2021_09_29_120000_create_admin_documentos_table', 1);
INSERT INTO public.migrations VALUES (28, '2021_10_04_194253_update_admin_company_table', 1);
INSERT INTO public.migrations VALUES (29, '2021_10_07_120000_create_admin_bancos_table', 1);
INSERT INTO public.migrations VALUES (30, '2021_10_07_130000_create_admin_contratos_table', 1);
INSERT INTO public.migrations VALUES (31, '2021_10_08_150000_update_admin_company2_table', 1);
INSERT INTO public.migrations VALUES (32, '2021_10_26_120000_update_admin_colaboradores_table', 1);
INSERT INTO public.migrations VALUES (33, '2021_10_26_130000_update_admin_company3_table', 1);
INSERT INTO public.migrations VALUES (34, '2021_10_27_120000_create_admin_archivos_table', 1);
INSERT INTO public.migrations VALUES (35, '2021_10_27_120000_update_admin_company_clients_table', 1);
INSERT INTO public.migrations VALUES (36, '2021_10_29_120000_create_admin_horary_table', 1);
INSERT INTO public.migrations VALUES (37, '2021_10_29_120000_create_admin_tasks_table', 1);
INSERT INTO public.migrations VALUES (38, '2021_10_29_130000_update_admin_puestos_table2', 1);
INSERT INTO public.migrations VALUES (39, '2021_11_02_090000_create_admin_company_clients_branch_area_table', 1);
INSERT INTO public.migrations VALUES (40, '2021_11_02_100000_update_admin_tasks_table', 1);
INSERT INTO public.migrations VALUES (41, '2021_11_02_120000_create_admin_changeLog_table', 1);
INSERT INTO public.migrations VALUES (42, '2021_11_04_234548_update_admin_colaboradores2_table', 1);
INSERT INTO public.migrations VALUES (43, '2021_11_05_100000_update_admin_company4_table', 1);
INSERT INTO public.migrations VALUES (44, '2021_11_05_235008_create_loggs_user_table', 2);
INSERT INTO public.migrations VALUES (45, '2021_11_08_120000_update_admin_client_colab_assoc_table', 2);
INSERT INTO public.migrations VALUES (46, '2021_11_10_110000_create_admin_unidades_table', 2);
INSERT INTO public.migrations VALUES (47, '2021_11_10_120000_create_admin_bodegas_table', 2);
INSERT INTO public.migrations VALUES (48, '2021_11_10_120000_create_admin_facturas_table', 2);
INSERT INTO public.migrations VALUES (49, '2021_11_10_120000_create_admin_productos_table', 2);
INSERT INTO public.migrations VALUES (50, '2021_11_10_120000_update_admin_bodegas_table', 2);
INSERT INTO public.migrations VALUES (51, '2021_11_10_120000_update_admin_company5_table', 2);
INSERT INTO public.migrations VALUES (52, '2021_11_10_130000_create_admin_stock_table', 2);
INSERT INTO public.migrations VALUES (53, '2021_11_10_130000_update_admin_company_clients_branchof_table', 2);
INSERT INTO public.migrations VALUES (54, '2021_11_10_140000_create_admin_stock_egresos_table', 2);
INSERT INTO public.migrations VALUES (55, '2021_11_10_140000_create_admin_stock_ingresos_table', 2);
INSERT INTO public.migrations VALUES (56, '2021_11_11_120000_create_admin_conversiones_table', 2);
INSERT INTO public.migrations VALUES (57, '2021_11_14_032010_update_admin_colaboradores3_table', 2);
INSERT INTO public.migrations VALUES (58, '2021_11_19_110000_create_admin_equipos_table', 2);
INSERT INTO public.migrations VALUES (59, '2021_11_19_120000_create_admin_equipo_asignado_table', 2);
INSERT INTO public.migrations VALUES (60, '2021_11_19_120000_create_admin_stock_egresos_equipo_table', 2);
INSERT INTO public.migrations VALUES (61, '2021_11_19_120000_create_admin_stock_equipos_table', 2);
INSERT INTO public.migrations VALUES (62, '2021_11_19_120000_create_admin_stock_ingresos_equipo_table', 2);
INSERT INTO public.migrations VALUES (63, '2021_11_19_120000_update_admin_facturas_table', 2);
INSERT INTO public.migrations VALUES (64, '2021_11_26_153618_create_admin_restoretimepermissions_table', 2);
INSERT INTO public.migrations VALUES (65, '2021_11_26_154948_create_admin_timepermissions_table', 2);
INSERT INTO public.migrations VALUES (66, '2021_11_26_160332_create_admin_economicpetitions_table', 2);
INSERT INTO public.migrations VALUES (67, '2021_11_26_160815_create_admin_simplepetitions_table', 2);
INSERT INTO public.migrations VALUES (68, '2021_11_26_161046_create_admin_simplepetitions_save_table', 2);
INSERT INTO public.migrations VALUES (69, '2021_11_26_161500_create_admin_complaintments_table', 2);
INSERT INTO public.migrations VALUES (70, '2021_11_29_120000_update_admin_changeLog_table', 2);
INSERT INTO public.migrations VALUES (71, '2021_11_30_120000_update_admin_stock_equipos_table', 2);
INSERT INTO public.migrations VALUES (72, '2021_12_08_120000_create_admin_alertas_table', 2);
INSERT INTO public.migrations VALUES (73, '2021_12_09_181446_create_admin_planilla_totales_table', 2);
INSERT INTO public.migrations VALUES (74, '2021_12_11_122350_create_admin_cumples_table', 2);
INSERT INTO public.migrations VALUES (75, '2021_12_11_122438_create_admin_mensajes_table', 2);
INSERT INTO public.migrations VALUES (76, '2021_12_14_132546_update_loggs_user_table', 2);
INSERT INTO public.migrations VALUES (77, '2021_12_14_165351_create_loggs_user_planilla_table', 2);
INSERT INTO public.migrations VALUES (78, '2021_12_20_182325_create_admin_mensajes_grupos_table', 2);
INSERT INTO public.migrations VALUES (79, '2021_12_21_192758_update_admin_colaboradores4_table', 2);
INSERT INTO public.migrations VALUES (80, '2021_12_23_064120_create_admin_codigos_table', 2);
INSERT INTO public.migrations VALUES (81, '2021_12_29_072634_update_admin_providers_table', 2);
INSERT INTO public.migrations VALUES (82, '2021_12_31_073217_create_admin_cotis_table', 2);
INSERT INTO public.migrations VALUES (83, '2022_01_11_160955_update_loggs_user4_table', 2);
INSERT INTO public.migrations VALUES (84, '2022_01_25_170150_update_admin_petitions_timepermissions_table', 2);
INSERT INTO public.migrations VALUES (85, '2022_03_23_120000_update_admin_colaboradores5_table', 2);
INSERT INTO public.migrations VALUES (86, '2022_03_25_054715_create_user_horas_table', 2);
INSERT INTO public.migrations VALUES (87, '2022_03_25_054818_create_user_viaticos_table', 2);
INSERT INTO public.migrations VALUES (88, '2022_04_01_062749_create_user_pagos_table', 2);
INSERT INTO public.migrations VALUES (89, '2022_04_05_120000_create_admin_cobro_colaborador_table', 2);
INSERT INTO public.migrations VALUES (90, '2022_04_06_180000_update_admin_company6_table', 2);
INSERT INTO public.migrations VALUES (91, '2022_04_12_094209_create_descuentos_table', 2);
INSERT INTO public.migrations VALUES (92, '2022_04_12_102655_create_log_descuentos_table', 2);
INSERT INTO public.migrations VALUES (93, '2022_05_03_064739_create_log_prestaciones_table', 3);
INSERT INTO public.migrations VALUES (95, '2021_10_27_120000_update_admin_company_clients2_table', 4);
INSERT INTO public.migrations VALUES (96, '2022_05_03_064814_create_log_liquidacion_table', 5);
INSERT INTO public.migrations VALUES (97, '2022_05_23_120000_update_admin_client_colab_assoc_table', 6);
INSERT INTO public.migrations VALUES (98, '2022_05_23_120000_update_admin_company_colab_assoc_table', 6);
INSERT INTO public.migrations VALUES (99, '2022_05_23_130000_update_admin_company_clients_table', 6);
INSERT INTO public.migrations VALUES (100, '2022_05_23_230000_update_admin_company7_table', 6);


--
-- TOC entry 3847 (class 0 OID 23730)
-- Dependencies: 334
-- Data for Name: personal_access_tokens; Type: TABLE DATA; Schema: public; Owner: matrisapre
--

INSERT INTO public.personal_access_tokens VALUES (2, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', '822459dfc6d69a40c5396afa4e2b526ad84a6cd24e323b85de9a99539ff8aee8', '["*"]', '2022-05-10 10:34:09', '2022-05-10 09:39:47', '2022-05-10 10:34:09');
INSERT INTO public.personal_access_tokens VALUES (28, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '674081c65b04b07b987ce691850067117c9d169cb8745874455c5d338d641277', '["*"]', '2022-05-12 15:20:02', '2022-05-12 15:20:01', '2022-05-12 15:20:02');
INSERT INTO public.personal_access_tokens VALUES (24, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'a199f9bb72520034ea8a1133cae1722eaa25989fae57cc3a909bd9d5797a169b', '["*"]', '2022-05-12 10:08:44', '2022-05-12 10:08:40', '2022-05-12 10:08:44');
INSERT INTO public.personal_access_tokens VALUES (26, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'e581e0cb1850c7ac16819d50e68ad3e033f2d996ce00168a611977d4507b9070', '["*"]', NULL, '2022-05-12 15:15:02', '2022-05-12 15:15:02');
INSERT INTO public.personal_access_tokens VALUES (15, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'ca4296cd870b0593aa1536466f6183aff6aaa54703126a15dee9a0826d83eb1e', '["*"]', '2022-05-12 09:50:09', '2022-05-12 09:40:40', '2022-05-12 09:50:09');
INSERT INTO public.personal_access_tokens VALUES (19, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', '1a0cbe2d0d7de481fa49261a745a998f7c9435bfb02cb6e219ac680da1c70755', '["*"]', '2022-05-12 09:53:30', '2022-05-12 09:53:01', '2022-05-12 09:53:30');
INSERT INTO public.personal_access_tokens VALUES (12, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '881b514981d02a8fbe00111ef97c8e0d5387583e71aa381af8ec20740a0d622d', '["*"]', '2022-05-12 09:33:59', '2022-05-12 09:33:58', '2022-05-12 09:33:59');
INSERT INTO public.personal_access_tokens VALUES (20, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'a58eeaa17b68c6c79e0e324749f94c8d708ed9036afaee28a230888b2fbadb4e', '["*"]', '2022-05-12 09:53:42', '2022-05-12 09:53:41', '2022-05-12 09:53:42');
INSERT INTO public.personal_access_tokens VALUES (7, 'App\apiModels\colaboradoresTokenModel', 6, 'API Token', 'ff6c36dcf12ef7bbd842bc2ab25b634237c15b3921bd18ec3557759912e10529', '["*"]', '2022-05-11 15:06:17', '2022-05-11 14:59:30', '2022-05-11 15:06:17');
INSERT INTO public.personal_access_tokens VALUES (3, 'App\apiModels\colaboradoresTokenModel', 2, 'API Token', '47702860a991308b6207ff68dbe30bc58cca5adb158807ff1261c5d5bbb4b901', '["*"]', '2022-05-11 14:27:37', '2022-05-11 14:25:54', '2022-05-11 14:27:37');
INSERT INTO public.personal_access_tokens VALUES (34, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', 'f9586f2575de5d67ca2d0f229d5c45540f5183c6d2686d5c2f1327aa67769d6a', '["*"]', '2022-05-12 16:10:14', '2022-05-12 16:10:13', '2022-05-12 16:10:14');
INSERT INTO public.personal_access_tokens VALUES (4, 'App\apiModels\colaboradoresTokenModel', 2, 'API Token', 'a5e9a8dde0b2ff99c4570793ece63d34f8cc8a92268818c074f441686f7bcff5', '["*"]', '2022-05-11 14:27:46', '2022-05-11 14:25:56', '2022-05-11 14:27:46');
INSERT INTO public.personal_access_tokens VALUES (16, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', '283f86b1b1cb615bc54d946569b786a50972e2d6b406ae3addcfe74116290568', '["*"]', '2022-05-12 09:51:02', '2022-05-12 09:50:34', '2022-05-12 09:51:02');
INSERT INTO public.personal_access_tokens VALUES (30, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '375472a9f04294bd6a1ee7341a14d6e3a8bb0b791c5c82842839b17850597b53', '["*"]', '2022-05-12 15:43:49', '2022-05-12 15:43:42', '2022-05-12 15:43:49');
INSERT INTO public.personal_access_tokens VALUES (14, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', '9d95443b0e3f296a7096ed75790c2b41186bc36a16ca7e14d92292e072946ded', '["*"]', '2022-05-12 09:39:42', '2022-05-12 09:39:41', '2022-05-12 09:39:42');
INSERT INTO public.personal_access_tokens VALUES (6, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', 'a6bf1dd714c2cc174977cb701342def5007a02b8479f509abbe0d65969bde0dc', '["*"]', '2022-05-11 15:31:18', '2022-05-11 14:39:34', '2022-05-11 15:31:18');
INSERT INTO public.personal_access_tokens VALUES (1, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', 'ccf2fde17577ad854043beab4ae02ea3f52a82801eb751f64511b2456d47ab00', '["*"]', '2022-05-10 07:58:53', '2022-05-10 07:54:26', '2022-05-10 07:58:53');
INSERT INTO public.personal_access_tokens VALUES (9, 'App\apiModels\colaboradoresTokenModel', 7, 'API Token', '4a8729614f8f515dbad690b84e3f3b711ffca12675272f20d6b5e0ee737c4ce5', '["*"]', '2022-05-11 15:31:32', '2022-05-11 15:31:31', '2022-05-11 15:31:32');
INSERT INTO public.personal_access_tokens VALUES (8, 'App\apiModels\colaboradoresTokenModel', 6, 'API Token', '3479fa967aba4354065bf34aaf86db5c5f2178110cd1f020d6e5196f41f999c5', '["*"]', '2022-05-11 21:36:57', '2022-05-11 15:06:29', '2022-05-11 21:36:57');
INSERT INTO public.personal_access_tokens VALUES (27, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '23b60d46f61e9eab7120d45cc2fe1b761fd3df4f01c27313dcae0bdabc93d241', '["*"]', '2022-05-12 15:18:21', '2022-05-12 15:18:18', '2022-05-12 15:18:21');
INSERT INTO public.personal_access_tokens VALUES (10, 'App\apiModels\colaboradoresTokenModel', 2, 'API Token', '3008c001bf4544f6b28a7a07477c215b24e1cbc1bde88cff26b2d096ca3eaa1a', '["*"]', '2022-05-12 09:22:07', '2022-05-11 15:35:52', '2022-05-12 09:22:07');
INSERT INTO public.personal_access_tokens VALUES (17, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '4d473b0700a3d7a64ade5179be2bdd5c80a2d51d57c46878d5d777ac81dc755e', '["*"]', '2022-05-12 09:51:17', '2022-05-12 09:51:16', '2022-05-12 09:51:17');
INSERT INTO public.personal_access_tokens VALUES (21, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '7f8e48cb2424c1febd318883d7db7ccbd5489ddae0d97fffb70824d9f8f2772a', '["*"]', '2022-05-12 09:54:45', '2022-05-12 09:54:37', '2022-05-12 09:54:45');
INSERT INTO public.personal_access_tokens VALUES (25, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '09e3ed24f1d84c1d435ce1876b408bdb70950b911281bca8e883fb9edaa0a0dd', '["*"]', '2022-05-12 15:19:42', '2022-05-12 15:04:14', '2022-05-12 15:19:42');
INSERT INTO public.personal_access_tokens VALUES (22, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '532dc4197a6c6b817a14c46608743f5140389afddf73949f9a07a4d5cdceec69', '["*"]', '2022-05-12 09:56:26', '2022-05-12 09:56:24', '2022-05-12 09:56:26');
INSERT INTO public.personal_access_tokens VALUES (18, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '1e6bc50ab55097f899d73a97c19755434500b4023244fe8b421a8d998ca1e64c', '["*"]', '2022-05-12 09:52:51', '2022-05-12 09:51:30', '2022-05-12 09:52:51');
INSERT INTO public.personal_access_tokens VALUES (29, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '6db96ba7b809feb467ae28903c9b284d4fe50108cd83c60de238ecdfa7e6765b', '["*"]', '2022-05-12 15:43:00', '2022-05-12 15:42:59', '2022-05-12 15:43:00');
INSERT INTO public.personal_access_tokens VALUES (23, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'f3a495569285f9a0a89446a99071c9a431e72ed333afab0c74eeb30566d29551', '["*"]', '2022-05-12 10:07:41', '2022-05-12 10:07:34', '2022-05-12 10:07:41');
INSERT INTO public.personal_access_tokens VALUES (13, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'aa6248590a06906d42a7cdc5eec969574dd9f84b15e1e46d5ad0a272040d3cdd', '["*"]', '2022-05-12 16:08:44', '2022-05-12 09:37:12', '2022-05-12 16:08:44');
INSERT INTO public.personal_access_tokens VALUES (32, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', '859caa633bd2cefd298630903d6f9f789e9c86442b1645fb90ef1e09f71fcbf5', '["*"]', '2022-05-12 16:09:42', '2022-05-12 16:09:35', '2022-05-12 16:09:42');
INSERT INTO public.personal_access_tokens VALUES (5, 'App\apiModels\colaboradoresTokenModel', 4, 'API Token', '29cd9c44aa908f41078a4aa749026a754554271ae669f7d0568b268ee25a4f9f', '["*"]', '2022-05-13 17:05:43', '2022-05-11 14:34:04', '2022-05-13 17:05:43');
INSERT INTO public.personal_access_tokens VALUES (31, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '1c99b35b0b5de81502db21deb85bb3edaa0dff7193dc0d9225239ab25c3a2f82', '["*"]', '2022-05-12 16:08:58', '2022-05-12 16:08:57', '2022-05-12 16:08:58');
INSERT INTO public.personal_access_tokens VALUES (33, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '23215f972cffcaef79260420d71248eedf65fa182f5efba9edf0b0cfea0ecef5', '["*"]', '2022-05-12 16:09:52', '2022-05-12 16:09:51', '2022-05-12 16:09:52');
INSERT INTO public.personal_access_tokens VALUES (35, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '8263ffdfdb284ac3f6f0e174c4f95760503968fa4f79936d67ca20169aa05f1a', '["*"]', '2022-05-12 16:10:25', '2022-05-12 16:10:24', '2022-05-12 16:10:25');
INSERT INTO public.personal_access_tokens VALUES (36, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', 'a4dfc6161cbad7e93a0d2ad48fcdcefd7231ddc1f000aec554a256a9ded0516b', '["*"]', '2022-05-12 16:10:48', '2022-05-12 16:10:47', '2022-05-12 16:10:48');
INSERT INTO public.personal_access_tokens VALUES (37, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'c1d0e3868de9ae9442be689aa16055a8d6d898ddf03d27437bbdb6085c761536', '["*"]', '2022-05-12 16:10:57', '2022-05-12 16:10:57', '2022-05-12 16:10:57');
INSERT INTO public.personal_access_tokens VALUES (38, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', '0ab5bc86f863092501a3286bc4fbb5ed6219f2ea6513b0ddc977e51d2194bd37', '["*"]', '2022-05-12 16:35:22', '2022-05-12 16:35:21', '2022-05-12 16:35:22');
INSERT INTO public.personal_access_tokens VALUES (39, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'd65f5906dedfaef929e5ad1051a90515589552f1bb8dfcdb2d762dcc76033658', '["*"]', '2022-05-12 18:28:52', '2022-05-12 18:28:34', '2022-05-12 18:28:52');
INSERT INTO public.personal_access_tokens VALUES (40, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'bcba2de54461150aac8f1a89626817398c0758aef57f9db23d79f8c22f362466', '["*"]', '2022-05-12 18:31:22', '2022-05-12 18:31:21', '2022-05-12 18:31:22');
INSERT INTO public.personal_access_tokens VALUES (55, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'fe93f6fe686d876652320d3817ce29c227620fb07df2e391f94224a831924b5e', '["*"]', '2022-05-16 09:29:52', '2022-05-16 09:29:51', '2022-05-16 09:29:52');
INSERT INTO public.personal_access_tokens VALUES (41, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '4292aebf92452cbb891a13eb36bf1bd31bab158dc533dd62f5aeba2aba1e8872', '["*"]', '2022-05-13 09:56:53', '2022-05-13 09:56:52', '2022-05-13 09:56:53');
INSERT INTO public.personal_access_tokens VALUES (42, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '0022fb50f4cbaf849f2765e5b268834d32a902845c40dd2cca35b5484f038acb', '["*"]', '2022-05-13 10:04:14', '2022-05-13 10:04:13', '2022-05-13 10:04:14');
INSERT INTO public.personal_access_tokens VALUES (43, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'e775a4cd083eb7d37f19d243873e3d245ccd7e6512301416846c26ba311786d7', '["*"]', '2022-05-13 10:54:39', '2022-05-13 10:54:38', '2022-05-13 10:54:39');
INSERT INTO public.personal_access_tokens VALUES (44, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'e2d039cf90d3fc19a2dbbf43bd7c225c9bdd40e5d43515ba4239ce192aaadd3c', '["*"]', '2022-05-13 10:56:03', '2022-05-13 10:56:02', '2022-05-13 10:56:03');
INSERT INTO public.personal_access_tokens VALUES (45, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', 'ede304a4313eedaeb6675d680d03ab03c52c7494bb49e356218282d276862e9b', '["*"]', '2022-05-13 10:57:05', '2022-05-13 10:57:04', '2022-05-13 10:57:05');
INSERT INTO public.personal_access_tokens VALUES (56, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'c9e1278ad6e724342f4bf3b4606f3cce8a9c4059a3b1fe3b1b400bbb57183c85', '["*"]', '2022-05-16 09:31:26', '2022-05-16 09:31:25', '2022-05-16 09:31:26');
INSERT INTO public.personal_access_tokens VALUES (46, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '60d5f46e7e53416d5cda55a02493fe071a5a7acc83be109bdd19774d9991bad6', '["*"]', '2022-05-13 11:00:54', '2022-05-13 11:00:53', '2022-05-13 11:00:54');
INSERT INTO public.personal_access_tokens VALUES (11, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '8871dbb4abc15f08894e3567831f2fb57b74643769bfd0b0768854677490771e', '["*"]', '2022-05-13 11:02:17', '2022-05-12 09:26:13', '2022-05-13 11:02:17');
INSERT INTO public.personal_access_tokens VALUES (47, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '29b4005c6db049e233a2372adde3d63711015b010e1f257a4b974bea1fa45445', '["*"]', '2022-05-13 11:04:22', '2022-05-13 11:04:22', '2022-05-13 11:04:22');
INSERT INTO public.personal_access_tokens VALUES (48, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', 'dc158ac7629e316ba30af635a2aad6c707c822eace2a24e6c793c0feee4bab19', '["*"]', '2022-05-13 11:04:32', '2022-05-13 11:04:31', '2022-05-13 11:04:32');
INSERT INTO public.personal_access_tokens VALUES (49, 'App\apiModels\colaboradoresTokenModel', 1, 'API Token', 'f04b278f80d8a7bbd5c564c0a940308eef2a701474c550a4b4278081dcac4c19', '["*"]', '2022-05-13 11:05:19', '2022-05-13 11:05:18', '2022-05-13 11:05:19');
INSERT INTO public.personal_access_tokens VALUES (50, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '72a4b63c775792e4c575cc5de89a1e2ca9245029067839680a8b53ae8acb894f', '["*"]', '2022-05-13 11:05:21', '2022-05-13 11:05:19', '2022-05-13 11:05:21');
INSERT INTO public.personal_access_tokens VALUES (52, 'App\apiModels\colaboradoresTokenModel', 9, 'API Token', '0ae4fa37d3a3df634c4516f6758acd1cdd9ed74a842f7514fdc6d791a05dc9b9', '["*"]', NULL, '2022-05-13 17:18:08', '2022-05-13 17:18:08');
INSERT INTO public.personal_access_tokens VALUES (51, 'App\apiModels\colaboradoresTokenModel', 9, 'API Token', 'b49afd28add6894719901dbaef21c3cc4f96c9e9e3b7a496010ae9fa5bbdb10e', '["*"]', '2022-05-13 17:18:09', '2022-05-13 17:18:07', '2022-05-13 17:18:09');
INSERT INTO public.personal_access_tokens VALUES (57, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '259cb6f69af64a20f3b79dd58a17ae941a3c689d81837008ce515b432a244249', '["*"]', '2022-05-16 09:33:17', '2022-05-16 09:33:16', '2022-05-16 09:33:17');
INSERT INTO public.personal_access_tokens VALUES (58, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '7c02506d527301b47b7cbc4dbc0fb4299002fab4977d96435d18a93e65a1672f', '["*"]', '2022-05-16 09:35:17', '2022-05-16 09:34:39', '2022-05-16 09:35:17');
INSERT INTO public.personal_access_tokens VALUES (54, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '8e3ed021ea9522ab99bf6a43d9f0fe12717259e8aa3f86c9f820afa41cc3345d', '["*"]', '2022-05-16 08:46:57', '2022-05-16 08:46:56', '2022-05-16 08:46:57');
INSERT INTO public.personal_access_tokens VALUES (62, 'App\apiModels\colaboradoresTokenModel', 9, 'API Token', 'ceddcd4ed812243d8ae23186bbd84f12b9885ec1fd3c7c4cc29845fc037c80fd', '["*"]', '2022-05-19 17:23:16', '2022-05-19 09:22:19', '2022-05-19 17:23:16');
INSERT INTO public.personal_access_tokens VALUES (61, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '80856c426713ce23b85e74822e9fa69cf6df02629beeeb49fb54467402b24ce5', '["*"]', '2022-05-19 09:22:21', '2022-05-17 06:31:21', '2022-05-19 09:22:21');
INSERT INTO public.personal_access_tokens VALUES (60, 'App\apiModels\colaboradoresTokenModel', 9, 'API Token', 'f0d19e04d341ab54dfed3b90a83aaedd98dcf67dc4fef313361d3dfea28bfb09', '["*"]', '2022-05-16 16:40:55', '2022-05-16 16:40:06', '2022-05-16 16:40:55');
INSERT INTO public.personal_access_tokens VALUES (59, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '7e34a3cb93db36c7f009f59dd8fa05942ac11cfd8eb7419ecb2f88b6f01a4725', '["*"]', '2022-05-17 06:31:07', '2022-05-16 09:39:03', '2022-05-17 06:31:07');
INSERT INTO public.personal_access_tokens VALUES (53, 'App\apiModels\colaboradoresTokenModel', 9, 'API Token', '2278847817e1443f2263340f3a35148f99303521b6212ce9308936f69c37f36c', '["*"]', '2022-05-19 08:28:53', '2022-05-14 10:08:31', '2022-05-19 08:28:53');
INSERT INTO public.personal_access_tokens VALUES (63, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '10e04b39321357dca81563a7b167c1be848a380e231b6e8315437eb64bfc938f', '["*"]', '2022-05-24 13:55:54', '2022-05-19 09:22:48', '2022-05-24 13:55:54');
INSERT INTO public.personal_access_tokens VALUES (66, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '1ea3936a6b3335082c004df1c8a68293759a7dcfd073ec188855250b2510697f', '["*"]', '2022-05-25 08:57:28', '2022-05-25 08:57:23', '2022-05-25 08:57:28');
INSERT INTO public.personal_access_tokens VALUES (69, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '30f43660f374953ab81cfc3afe87a5b69542e7b5b2cabb8717691f304f0e2bfd', '["*"]', '2022-05-26 14:43:38', '2022-05-26 06:29:08', '2022-05-26 14:43:38');
INSERT INTO public.personal_access_tokens VALUES (64, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '136031b98eea4c8983e22d6b37ffd329aa9249ad91c688ee74e756afe1bf88a4', '["*"]', '2022-05-25 08:27:12', '2022-05-24 13:56:06', '2022-05-25 08:27:12');
INSERT INTO public.personal_access_tokens VALUES (67, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '69c4273f57baaf18b11253d6ce32e4461719a7e2a132eaf748bef508eca909b0', '["*"]', '2022-05-25 10:40:52', '2022-05-25 08:59:29', '2022-05-25 10:40:52');
INSERT INTO public.personal_access_tokens VALUES (65, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '04475143d4c4898b67178e4c16d4c3e96ea596f5b1a21ee9867b34c6c5ba5ff7', '["*"]', '2022-05-25 08:57:10', '2022-05-25 08:27:29', '2022-05-25 08:57:10');
INSERT INTO public.personal_access_tokens VALUES (68, 'App\apiModels\colaboradoresTokenModel', 8, 'API Token', '53eb798ab49b4eff025733d75fa4a0b6b8d57bb2e15bcd8cf0f0b5588b53a1f2', '["*"]', '2022-05-26 06:28:55', '2022-05-25 10:41:05', '2022-05-26 06:28:55');


--
-- TOC entry 3849 (class 0 OID 23738)
-- Dependencies: 336
-- Data for Name: user_horas; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3851 (class 0 OID 23747)
-- Dependencies: 338
-- Data for Name: user_pagos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3853 (class 0 OID 23752)
-- Dependencies: 340
-- Data for Name: user_viaticos; Type: TABLE DATA; Schema: public; Owner: matrisapre
--



--
-- TOC entry 3931 (class 0 OID 0)
-- Dependencies: 203
-- Name: admin_access_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_access_id_seq', 33, true);


--
-- TOC entry 3932 (class 0 OID 0)
-- Dependencies: 205
-- Name: admin_access_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_access_roles_id_seq', 1, false);


--
-- TOC entry 3933 (class 0 OID 0)
-- Dependencies: 207
-- Name: admin_alertas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_alertas_id_seq', 1, false);


--
-- TOC entry 3934 (class 0 OID 0)
-- Dependencies: 209
-- Name: admin_archivos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_archivos_id_seq', 1, false);


--
-- TOC entry 3935 (class 0 OID 0)
-- Dependencies: 211
-- Name: admin_bancos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_bancos_id_seq', 7, true);


--
-- TOC entry 3936 (class 0 OID 0)
-- Dependencies: 213
-- Name: admin_bodegas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_bodegas_id_seq', 100, true);


--
-- TOC entry 3937 (class 0 OID 0)
-- Dependencies: 215
-- Name: admin_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_categories_id_seq', 1, true);


--
-- TOC entry 3938 (class 0 OID 0)
-- Dependencies: 217
-- Name: admin_changeLog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public."admin_changeLog_id_seq"', 31, true);


--
-- TOC entry 3939 (class 0 OID 0)
-- Dependencies: 219
-- Name: admin_client_colab_assoc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_client_colab_assoc_id_seq', 18, true);


--
-- TOC entry 3940 (class 0 OID 0)
-- Dependencies: 221
-- Name: admin_cobro_colaborador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_cobro_colaborador_id_seq', 1, false);


--
-- TOC entry 3941 (class 0 OID 0)
-- Dependencies: 223
-- Name: admin_codigos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_codigos_id_seq', 3, true);


--
-- TOC entry 3942 (class 0 OID 0)
-- Dependencies: 225
-- Name: admin_colaboradores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_colaboradores_id_seq', 12, true);


--
-- TOC entry 3943 (class 0 OID 0)
-- Dependencies: 227
-- Name: admin_colors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_colors_id_seq', 1, false);


--
-- TOC entry 3944 (class 0 OID 0)
-- Dependencies: 230
-- Name: admin_company_client_assoc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_client_assoc_id_seq', 6, true);


--
-- TOC entry 3945 (class 0 OID 0)
-- Dependencies: 233
-- Name: admin_company_clients_branch_area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_clients_branch_area_id_seq', 4, true);


--
-- TOC entry 3946 (class 0 OID 0)
-- Dependencies: 235
-- Name: admin_company_clients_branchof_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_clients_branchof_id_seq', 95, true);


--
-- TOC entry 3947 (class 0 OID 0)
-- Dependencies: 236
-- Name: admin_company_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_clients_id_seq', 43, true);


--
-- TOC entry 3948 (class 0 OID 0)
-- Dependencies: 238
-- Name: admin_company_clients_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_clients_jobs_id_seq', 1, false);


--
-- TOC entry 3949 (class 0 OID 0)
-- Dependencies: 240
-- Name: admin_company_clients_jobs_todo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_clients_jobs_todo_id_seq', 1, false);


--
-- TOC entry 3950 (class 0 OID 0)
-- Dependencies: 242
-- Name: admin_company_colab_assoc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_colab_assoc_id_seq', 10, true);


--
-- TOC entry 3951 (class 0 OID 0)
-- Dependencies: 243
-- Name: admin_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_company_id_seq', 5, true);


--
-- TOC entry 3952 (class 0 OID 0)
-- Dependencies: 245
-- Name: admin_contratos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_contratos_id_seq', 2, true);


--
-- TOC entry 3953 (class 0 OID 0)
-- Dependencies: 247
-- Name: admin_conversiones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_conversiones_id_seq', 1, true);


--
-- TOC entry 3954 (class 0 OID 0)
-- Dependencies: 249
-- Name: admin_cotis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_cotis_id_seq', 1, false);


--
-- TOC entry 3955 (class 0 OID 0)
-- Dependencies: 251
-- Name: admin_cumples_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_cumples_id_seq', 1, false);


--
-- TOC entry 3956 (class 0 OID 0)
-- Dependencies: 253
-- Name: admin_documentos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_documentos_id_seq', 1, true);


--
-- TOC entry 3957 (class 0 OID 0)
-- Dependencies: 255
-- Name: admin_equipo_asignado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_equipo_asignado_id_seq', 1, false);


--
-- TOC entry 3958 (class 0 OID 0)
-- Dependencies: 257
-- Name: admin_equipos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_equipos_id_seq', 1, false);


--
-- TOC entry 3959 (class 0 OID 0)
-- Dependencies: 259
-- Name: admin_facturas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_facturas_id_seq', 2, true);


--
-- TOC entry 3960 (class 0 OID 0)
-- Dependencies: 261
-- Name: admin_horary_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_horary_id_seq', 8, true);


--
-- TOC entry 3961 (class 0 OID 0)
-- Dependencies: 263
-- Name: admin_loggs_user_paiments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_loggs_user_paiments_id_seq', 1, false);


--
-- TOC entry 3962 (class 0 OID 0)
-- Dependencies: 266
-- Name: admin_mensajes_grupos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_mensajes_grupos_id_seq', 1, false);


--
-- TOC entry 3963 (class 0 OID 0)
-- Dependencies: 267
-- Name: admin_mensajes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_mensajes_id_seq', 1, true);


--
-- TOC entry 3964 (class 0 OID 0)
-- Dependencies: 269
-- Name: admin_petitions_complaintments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_petitions_complaintments_id_seq', 8, true);


--
-- TOC entry 3965 (class 0 OID 0)
-- Dependencies: 271
-- Name: admin_petitions_economicpetitions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_petitions_economicpetitions_id_seq', 1, true);


--
-- TOC entry 3966 (class 0 OID 0)
-- Dependencies: 273
-- Name: admin_petitions_restoretimepermissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_petitions_restoretimepermissions_id_seq', 1, true);


--
-- TOC entry 3967 (class 0 OID 0)
-- Dependencies: 275
-- Name: admin_petitions_simplepetitions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_petitions_simplepetitions_id_seq', 1, false);


--
-- TOC entry 3968 (class 0 OID 0)
-- Dependencies: 277
-- Name: admin_petitions_simplepetitions_save_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_petitions_simplepetitions_save_id_seq', 1, false);


--
-- TOC entry 3969 (class 0 OID 0)
-- Dependencies: 279
-- Name: admin_petitions_timepermissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_petitions_timepermissions_id_seq', 1, false);


--
-- TOC entry 3970 (class 0 OID 0)
-- Dependencies: 281
-- Name: admin_productos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_productos_id_seq', 2, true);


--
-- TOC entry 3971 (class 0 OID 0)
-- Dependencies: 283
-- Name: admin_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_products_id_seq', 1, false);


--
-- TOC entry 3972 (class 0 OID 0)
-- Dependencies: 285
-- Name: admin_providers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_providers_id_seq', 2, true);


--
-- TOC entry 3973 (class 0 OID 0)
-- Dependencies: 287
-- Name: admin_puestos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_puestos_id_seq', 63, true);


--
-- TOC entry 3974 (class 0 OID 0)
-- Dependencies: 289
-- Name: admin_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_roles_id_seq', 1, false);


--
-- TOC entry 3975 (class 0 OID 0)
-- Dependencies: 291
-- Name: admin_sizes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_sizes_id_seq', 1, false);


--
-- TOC entry 3976 (class 0 OID 0)
-- Dependencies: 293
-- Name: admin_solicitudes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_solicitudes_id_seq', 1, false);


--
-- TOC entry 3977 (class 0 OID 0)
-- Dependencies: 297
-- Name: admin_stock_egresos_equipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_stock_egresos_equipo_id_seq', 1, false);


--
-- TOC entry 3978 (class 0 OID 0)
-- Dependencies: 298
-- Name: admin_stock_egresos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_stock_egresos_id_seq', 1, true);


--
-- TOC entry 3979 (class 0 OID 0)
-- Dependencies: 300
-- Name: admin_stock_equipos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_stock_equipos_id_seq', 1, false);


--
-- TOC entry 3980 (class 0 OID 0)
-- Dependencies: 301
-- Name: admin_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_stock_id_seq', 3, true);


--
-- TOC entry 3981 (class 0 OID 0)
-- Dependencies: 304
-- Name: admin_stock_ingresos_equipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_stock_ingresos_equipo_id_seq', 1, false);


--
-- TOC entry 3982 (class 0 OID 0)
-- Dependencies: 305
-- Name: admin_stock_ingresos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_stock_ingresos_id_seq', 4, true);


--
-- TOC entry 3983 (class 0 OID 0)
-- Dependencies: 307
-- Name: admin_subCategories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public."admin_subCategories_id_seq"', 2, true);


--
-- TOC entry 3984 (class 0 OID 0)
-- Dependencies: 309
-- Name: admin_tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_tasks_id_seq', 1, false);


--
-- TOC entry 3985 (class 0 OID 0)
-- Dependencies: 311
-- Name: admin_unidades_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_unidades_id_seq', 4, true);


--
-- TOC entry 3986 (class 0 OID 0)
-- Dependencies: 313
-- Name: admin_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_users_id_seq', 1, false);


--
-- TOC entry 3987 (class 0 OID 0)
-- Dependencies: 315
-- Name: admin_usersadmin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_usersadmin_id_seq', 1, false);


--
-- TOC entry 3988 (class 0 OID 0)
-- Dependencies: 317
-- Name: admin_warehouse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_warehouse_id_seq', 1, false);


--
-- TOC entry 3989 (class 0 OID 0)
-- Dependencies: 319
-- Name: admin_werehouse_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.admin_werehouse_products_id_seq', 1, false);


--
-- TOC entry 3990 (class 0 OID 0)
-- Dependencies: 321
-- Name: descuentos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.descuentos_id_seq', 1, false);


--
-- TOC entry 3991 (class 0 OID 0)
-- Dependencies: 323
-- Name: log_descuentos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.log_descuentos_id_seq', 1, false);


--
-- TOC entry 3992 (class 0 OID 0)
-- Dependencies: 325
-- Name: log_liquidacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.log_liquidacion_id_seq', 1, false);


--
-- TOC entry 3993 (class 0 OID 0)
-- Dependencies: 327
-- Name: log_prestaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.log_prestaciones_id_seq', 1, false);


--
-- TOC entry 3994 (class 0 OID 0)
-- Dependencies: 329
-- Name: loggs_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.loggs_user_id_seq', 5, true);


--
-- TOC entry 3995 (class 0 OID 0)
-- Dependencies: 331
-- Name: loggs_user_planilla_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.loggs_user_planilla_id_seq', 1, false);


--
-- TOC entry 3996 (class 0 OID 0)
-- Dependencies: 333
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.migrations_id_seq', 100, true);


--
-- TOC entry 3997 (class 0 OID 0)
-- Dependencies: 335
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 69, true);


--
-- TOC entry 3998 (class 0 OID 0)
-- Dependencies: 337
-- Name: user_horas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.user_horas_id_seq', 1, false);


--
-- TOC entry 3999 (class 0 OID 0)
-- Dependencies: 339
-- Name: user_pagos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.user_pagos_id_seq', 1, false);


--
-- TOC entry 4000 (class 0 OID 0)
-- Dependencies: 341
-- Name: user_viaticos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matrisapre
--

SELECT pg_catalog.setval('public.user_viaticos_id_seq', 1, false);


--
-- TOC entry 3335 (class 2606 OID 23831)
-- Name: admin_access admin_access_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_access
    ADD CONSTRAINT admin_access_pkey PRIMARY KEY (id);


--
-- TOC entry 3337 (class 2606 OID 23833)
-- Name: admin_access_roles admin_access_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_access_roles
    ADD CONSTRAINT admin_access_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 3339 (class 2606 OID 23835)
-- Name: admin_alertas admin_alertas_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_alertas
    ADD CONSTRAINT admin_alertas_pkey PRIMARY KEY (id);


--
-- TOC entry 3341 (class 2606 OID 23837)
-- Name: admin_alertas admin_alertas_referencia_unique; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_alertas
    ADD CONSTRAINT admin_alertas_referencia_unique UNIQUE (referencia);


--
-- TOC entry 3343 (class 2606 OID 23839)
-- Name: admin_archivos admin_archivos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_archivos
    ADD CONSTRAINT admin_archivos_pkey PRIMARY KEY (id);


--
-- TOC entry 3345 (class 2606 OID 23841)
-- Name: admin_bancos admin_bancos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_bancos
    ADD CONSTRAINT admin_bancos_pkey PRIMARY KEY (id);


--
-- TOC entry 3347 (class 2606 OID 23843)
-- Name: admin_bodegas admin_bodegas_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_bodegas
    ADD CONSTRAINT admin_bodegas_pkey PRIMARY KEY (id);


--
-- TOC entry 3349 (class 2606 OID 23845)
-- Name: admin_categories admin_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_categories
    ADD CONSTRAINT admin_categories_pkey PRIMARY KEY (id);


--
-- TOC entry 3351 (class 2606 OID 23847)
-- Name: admin_changeLog admin_changeLog_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public."admin_changeLog"
    ADD CONSTRAINT "admin_changeLog_pkey" PRIMARY KEY (id);


--
-- TOC entry 3353 (class 2606 OID 23849)
-- Name: admin_client_colab_assoc admin_client_colab_assoc_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_client_colab_assoc
    ADD CONSTRAINT admin_client_colab_assoc_pkey PRIMARY KEY (id);


--
-- TOC entry 3355 (class 2606 OID 23851)
-- Name: admin_cobro_colaborador admin_cobro_colaborador_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cobro_colaborador
    ADD CONSTRAINT admin_cobro_colaborador_pkey PRIMARY KEY (id);


--
-- TOC entry 3357 (class 2606 OID 23853)
-- Name: admin_codigos admin_codigos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_codigos
    ADD CONSTRAINT admin_codigos_pkey PRIMARY KEY (id);


--
-- TOC entry 3359 (class 2606 OID 23856)
-- Name: admin_colaboradores admin_colaboradores_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_colaboradores
    ADD CONSTRAINT admin_colaboradores_pkey PRIMARY KEY (id);


--
-- TOC entry 3361 (class 2606 OID 23858)
-- Name: admin_colors admin_colors_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_colors
    ADD CONSTRAINT admin_colors_pkey PRIMARY KEY (id);


--
-- TOC entry 3365 (class 2606 OID 23860)
-- Name: admin_company_client_assoc admin_company_client_assoc_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_client_assoc
    ADD CONSTRAINT admin_company_client_assoc_pkey PRIMARY KEY (id);


--
-- TOC entry 3369 (class 2606 OID 23862)
-- Name: admin_company_clients_branch_area admin_company_clients_branch_area_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_branch_area
    ADD CONSTRAINT admin_company_clients_branch_area_pkey PRIMARY KEY (id);


--
-- TOC entry 3371 (class 2606 OID 23864)
-- Name: admin_company_clients_branchof admin_company_clients_branchof_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_branchof
    ADD CONSTRAINT admin_company_clients_branchof_pkey PRIMARY KEY (id);


--
-- TOC entry 3373 (class 2606 OID 23866)
-- Name: admin_company_clients_jobs admin_company_clients_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_jobs
    ADD CONSTRAINT admin_company_clients_jobs_pkey PRIMARY KEY (id);


--
-- TOC entry 3375 (class 2606 OID 23868)
-- Name: admin_company_clients_jobs_todo admin_company_clients_jobs_todo_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_jobs_todo
    ADD CONSTRAINT admin_company_clients_jobs_todo_pkey PRIMARY KEY (id);


--
-- TOC entry 3367 (class 2606 OID 23870)
-- Name: admin_company_clients admin_company_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients
    ADD CONSTRAINT admin_company_clients_pkey PRIMARY KEY (id);


--
-- TOC entry 3377 (class 2606 OID 23872)
-- Name: admin_company_colab_assoc admin_company_colab_assoc_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_colab_assoc
    ADD CONSTRAINT admin_company_colab_assoc_pkey PRIMARY KEY (id);


--
-- TOC entry 3363 (class 2606 OID 23874)
-- Name: admin_company admin_company_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company
    ADD CONSTRAINT admin_company_pkey PRIMARY KEY (id);


--
-- TOC entry 3379 (class 2606 OID 23876)
-- Name: admin_contratos admin_contratos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_contratos
    ADD CONSTRAINT admin_contratos_pkey PRIMARY KEY (id);


--
-- TOC entry 3381 (class 2606 OID 23878)
-- Name: admin_conversiones admin_conversiones_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_conversiones
    ADD CONSTRAINT admin_conversiones_pkey PRIMARY KEY (id);


--
-- TOC entry 3383 (class 2606 OID 23880)
-- Name: admin_cotis admin_cotis_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cotis
    ADD CONSTRAINT admin_cotis_pkey PRIMARY KEY (id);


--
-- TOC entry 3385 (class 2606 OID 23882)
-- Name: admin_cumples admin_cumples_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cumples
    ADD CONSTRAINT admin_cumples_pkey PRIMARY KEY (id);


--
-- TOC entry 3387 (class 2606 OID 23884)
-- Name: admin_documentos admin_documentos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_documentos
    ADD CONSTRAINT admin_documentos_pkey PRIMARY KEY (id);


--
-- TOC entry 3389 (class 2606 OID 23886)
-- Name: admin_equipo_asignado admin_equipo_asignado_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipo_asignado
    ADD CONSTRAINT admin_equipo_asignado_pkey PRIMARY KEY (id);


--
-- TOC entry 3391 (class 2606 OID 23888)
-- Name: admin_equipos admin_equipos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipos
    ADD CONSTRAINT admin_equipos_pkey PRIMARY KEY (id);


--
-- TOC entry 3393 (class 2606 OID 23890)
-- Name: admin_facturas admin_facturas_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_facturas
    ADD CONSTRAINT admin_facturas_pkey PRIMARY KEY (id);


--
-- TOC entry 3395 (class 2606 OID 23892)
-- Name: admin_horary admin_horary_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_horary
    ADD CONSTRAINT admin_horary_pkey PRIMARY KEY (id);


--
-- TOC entry 3397 (class 2606 OID 23894)
-- Name: admin_loggs_user_paiments admin_loggs_user_paiments_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_loggs_user_paiments
    ADD CONSTRAINT admin_loggs_user_paiments_pkey PRIMARY KEY (id);


--
-- TOC entry 3401 (class 2606 OID 23896)
-- Name: admin_mensajes_grupos admin_mensajes_grupos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_mensajes_grupos
    ADD CONSTRAINT admin_mensajes_grupos_pkey PRIMARY KEY (id);


--
-- TOC entry 3399 (class 2606 OID 23898)
-- Name: admin_mensajes admin_mensajes_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_mensajes
    ADD CONSTRAINT admin_mensajes_pkey PRIMARY KEY (id);


--
-- TOC entry 3403 (class 2606 OID 23900)
-- Name: admin_petitions_complaintments admin_petitions_complaintments_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_complaintments
    ADD CONSTRAINT admin_petitions_complaintments_pkey PRIMARY KEY (id);


--
-- TOC entry 3405 (class 2606 OID 23902)
-- Name: admin_petitions_economicpetitions admin_petitions_economicpetitions_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_economicpetitions
    ADD CONSTRAINT admin_petitions_economicpetitions_pkey PRIMARY KEY (id);


--
-- TOC entry 3407 (class 2606 OID 23904)
-- Name: admin_petitions_restoretimepermissions admin_petitions_restoretimepermissions_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_restoretimepermissions
    ADD CONSTRAINT admin_petitions_restoretimepermissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3409 (class 2606 OID 23906)
-- Name: admin_petitions_simplepetitions admin_petitions_simplepetitions_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_simplepetitions
    ADD CONSTRAINT admin_petitions_simplepetitions_pkey PRIMARY KEY (id);


--
-- TOC entry 3411 (class 2606 OID 23908)
-- Name: admin_petitions_simplepetitions_save admin_petitions_simplepetitions_save_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_simplepetitions_save
    ADD CONSTRAINT admin_petitions_simplepetitions_save_pkey PRIMARY KEY (id);


--
-- TOC entry 3413 (class 2606 OID 23910)
-- Name: admin_petitions_timepermissions admin_petitions_timepermissions_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_timepermissions
    ADD CONSTRAINT admin_petitions_timepermissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3415 (class 2606 OID 23912)
-- Name: admin_productos admin_productos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_productos
    ADD CONSTRAINT admin_productos_pkey PRIMARY KEY (id);


--
-- TOC entry 3417 (class 2606 OID 23914)
-- Name: admin_products admin_products_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_products
    ADD CONSTRAINT admin_products_pkey PRIMARY KEY (id);


--
-- TOC entry 3419 (class 2606 OID 23916)
-- Name: admin_providers admin_providers_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_providers
    ADD CONSTRAINT admin_providers_pkey PRIMARY KEY (id);


--
-- TOC entry 3421 (class 2606 OID 23918)
-- Name: admin_puestos admin_puestos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_puestos
    ADD CONSTRAINT admin_puestos_pkey PRIMARY KEY (id);


--
-- TOC entry 3423 (class 2606 OID 23920)
-- Name: admin_roles admin_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_roles
    ADD CONSTRAINT admin_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 3425 (class 2606 OID 23922)
-- Name: admin_sizes admin_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_sizes
    ADD CONSTRAINT admin_sizes_pkey PRIMARY KEY (id);


--
-- TOC entry 3427 (class 2606 OID 23924)
-- Name: admin_solicitudes admin_solicitudes_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_solicitudes
    ADD CONSTRAINT admin_solicitudes_pkey PRIMARY KEY (id);


--
-- TOC entry 3433 (class 2606 OID 23926)
-- Name: admin_stock_egresos_equipo admin_stock_egresos_equipo_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos_equipo
    ADD CONSTRAINT admin_stock_egresos_equipo_pkey PRIMARY KEY (id);


--
-- TOC entry 3431 (class 2606 OID 23928)
-- Name: admin_stock_egresos admin_stock_egresos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_pkey PRIMARY KEY (id);


--
-- TOC entry 3435 (class 2606 OID 23930)
-- Name: admin_stock_equipos admin_stock_equipos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_equipos
    ADD CONSTRAINT admin_stock_equipos_pkey PRIMARY KEY (id);


--
-- TOC entry 3439 (class 2606 OID 23932)
-- Name: admin_stock_ingresos_equipo admin_stock_ingresos_equipo_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos_equipo
    ADD CONSTRAINT admin_stock_ingresos_equipo_pkey PRIMARY KEY (id);


--
-- TOC entry 3437 (class 2606 OID 23934)
-- Name: admin_stock_ingresos admin_stock_ingresos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos
    ADD CONSTRAINT admin_stock_ingresos_pkey PRIMARY KEY (id);


--
-- TOC entry 3429 (class 2606 OID 23936)
-- Name: admin_stock admin_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock
    ADD CONSTRAINT admin_stock_pkey PRIMARY KEY (id);


--
-- TOC entry 3441 (class 2606 OID 23938)
-- Name: admin_subCategories admin_subCategories_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public."admin_subCategories"
    ADD CONSTRAINT "admin_subCategories_pkey" PRIMARY KEY (id);


--
-- TOC entry 3443 (class 2606 OID 23940)
-- Name: admin_tasks admin_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_tasks
    ADD CONSTRAINT admin_tasks_pkey PRIMARY KEY (id);


--
-- TOC entry 3445 (class 2606 OID 23942)
-- Name: admin_unidades admin_unidades_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_unidades
    ADD CONSTRAINT admin_unidades_pkey PRIMARY KEY (id);


--
-- TOC entry 3447 (class 2606 OID 23944)
-- Name: admin_users admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3449 (class 2606 OID 23946)
-- Name: admin_users admin_users_usersys_unique; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_usersys_unique UNIQUE (usersys);


--
-- TOC entry 3451 (class 2606 OID 23948)
-- Name: admin_usersadmin admin_usersadmin_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_usersadmin
    ADD CONSTRAINT admin_usersadmin_pkey PRIMARY KEY (id);


--
-- TOC entry 3453 (class 2606 OID 23950)
-- Name: admin_warehouse admin_warehouse_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_warehouse
    ADD CONSTRAINT admin_warehouse_pkey PRIMARY KEY (id);


--
-- TOC entry 3455 (class 2606 OID 23952)
-- Name: admin_werehouse_products admin_werehouse_products_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_werehouse_products
    ADD CONSTRAINT admin_werehouse_products_pkey PRIMARY KEY (id);


--
-- TOC entry 3457 (class 2606 OID 23954)
-- Name: descuentos descuentos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.descuentos
    ADD CONSTRAINT descuentos_pkey PRIMARY KEY (id);


--
-- TOC entry 3459 (class 2606 OID 23956)
-- Name: log_descuentos log_descuentos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_descuentos
    ADD CONSTRAINT log_descuentos_pkey PRIMARY KEY (id);


--
-- TOC entry 3461 (class 2606 OID 23958)
-- Name: log_liquidacion log_liquidacion_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_liquidacion
    ADD CONSTRAINT log_liquidacion_pkey PRIMARY KEY (id);


--
-- TOC entry 3463 (class 2606 OID 23960)
-- Name: log_prestaciones log_prestaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_prestaciones
    ADD CONSTRAINT log_prestaciones_pkey PRIMARY KEY (id);


--
-- TOC entry 3465 (class 2606 OID 23962)
-- Name: loggs_user loggs_user_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3467 (class 2606 OID 23964)
-- Name: loggs_user_planilla loggs_user_planilla_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user_planilla
    ADD CONSTRAINT loggs_user_planilla_pkey PRIMARY KEY (id);


--
-- TOC entry 3469 (class 2606 OID 23966)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3471 (class 2606 OID 23968)
-- Name: personal_access_tokens personal_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);


--
-- TOC entry 3473 (class 2606 OID 23970)
-- Name: personal_access_tokens personal_access_tokens_token_unique; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);


--
-- TOC entry 3476 (class 2606 OID 23972)
-- Name: user_horas user_horas_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_horas
    ADD CONSTRAINT user_horas_pkey PRIMARY KEY (id);


--
-- TOC entry 3478 (class 2606 OID 23974)
-- Name: user_pagos user_pagos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos
    ADD CONSTRAINT user_pagos_pkey PRIMARY KEY (id);


--
-- TOC entry 3480 (class 2606 OID 23976)
-- Name: user_viaticos user_viaticos_pkey; Type: CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_viaticos
    ADD CONSTRAINT user_viaticos_pkey PRIMARY KEY (id);


--
-- TOC entry 3474 (class 1259 OID 23977)
-- Name: personal_access_tokens_tokenable_type_tokenable_id_index; Type: INDEX; Schema: public; Owner: matrisapre
--

CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);


--
-- TOC entry 3481 (class 2606 OID 23978)
-- Name: admin_access_roles admin_access_roles_id_access_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_access_roles
    ADD CONSTRAINT admin_access_roles_id_access_foreign FOREIGN KEY (id_access) REFERENCES public.admin_access(id) ON DELETE CASCADE;


--
-- TOC entry 3482 (class 2606 OID 23983)
-- Name: admin_access_roles admin_access_roles_id_role_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_access_roles
    ADD CONSTRAINT admin_access_roles_id_role_foreign FOREIGN KEY (id_role) REFERENCES public.admin_roles(id) ON DELETE CASCADE;


--
-- TOC entry 3483 (class 2606 OID 23988)
-- Name: admin_cobro_colaborador admin_cobro_colaborador_colaborador_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cobro_colaborador
    ADD CONSTRAINT admin_cobro_colaborador_colaborador_id_foreign FOREIGN KEY (colaborador_id) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3484 (class 2606 OID 23993)
-- Name: admin_cobro_colaborador admin_cobro_colaborador_medida_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cobro_colaborador
    ADD CONSTRAINT admin_cobro_colaborador_medida_id_foreign FOREIGN KEY (medida_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3485 (class 2606 OID 23998)
-- Name: admin_cobro_colaborador admin_cobro_colaborador_producto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cobro_colaborador
    ADD CONSTRAINT admin_cobro_colaborador_producto_id_foreign FOREIGN KEY (producto_id) REFERENCES public.admin_productos(id);


--
-- TOC entry 3486 (class 2606 OID 24003)
-- Name: admin_cobro_colaborador admin_cobro_colaborador_stock_egresos_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_cobro_colaborador
    ADD CONSTRAINT admin_cobro_colaborador_stock_egresos_id_foreign FOREIGN KEY (stock_egresos_id) REFERENCES public.admin_stock_egresos(id);


--
-- TOC entry 3487 (class 2606 OID 24008)
-- Name: admin_company admin_company_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company
    ADD CONSTRAINT admin_company_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3489 (class 2606 OID 24013)
-- Name: admin_company_client_assoc admin_company_client_assoc_idclient_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_client_assoc
    ADD CONSTRAINT admin_company_client_assoc_idclient_foreign FOREIGN KEY ("idClient") REFERENCES public.admin_company_clients(id) ON DELETE CASCADE;


--
-- TOC entry 3490 (class 2606 OID 24018)
-- Name: admin_company_client_assoc admin_company_client_assoc_idcompany_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_client_assoc
    ADD CONSTRAINT admin_company_client_assoc_idcompany_foreign FOREIGN KEY ("idCompany") REFERENCES public.admin_company(id) ON DELETE CASCADE;


--
-- TOC entry 3491 (class 2606 OID 24023)
-- Name: admin_company_clients_branchof admin_company_clients_branchof_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_branchof
    ADD CONSTRAINT admin_company_clients_branchof_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3492 (class 2606 OID 24028)
-- Name: admin_company_clients_branchof admin_company_clients_branchof_idclient_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_branchof
    ADD CONSTRAINT admin_company_clients_branchof_idclient_foreign FOREIGN KEY ("idClient") REFERENCES public.admin_company_clients(id) ON DELETE CASCADE;


--
-- TOC entry 3493 (class 2606 OID 24033)
-- Name: admin_company_clients_jobs admin_company_clients_jobs_idclientcomp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_jobs
    ADD CONSTRAINT admin_company_clients_jobs_idclientcomp_foreign FOREIGN KEY ("idClientComp") REFERENCES public.admin_company_client_assoc(id) ON DELETE CASCADE;


--
-- TOC entry 3494 (class 2606 OID 24038)
-- Name: admin_company_clients_jobs admin_company_clients_jobs_idjobcatalog_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_jobs
    ADD CONSTRAINT admin_company_clients_jobs_idjobcatalog_foreign FOREIGN KEY ("idJobCatalog") REFERENCES public.admin_puestos(id) ON DELETE CASCADE;


--
-- TOC entry 3495 (class 2606 OID 24043)
-- Name: admin_company_clients_jobs_todo admin_company_clients_jobs_todo_idjob_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company_clients_jobs_todo
    ADD CONSTRAINT admin_company_clients_jobs_todo_idjob_foreign FOREIGN KEY ("idJob") REFERENCES public.admin_company_clients_jobs(id) ON DELETE CASCADE;


--
-- TOC entry 3488 (class 2606 OID 24048)
-- Name: admin_company admin_company_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_company
    ADD CONSTRAINT admin_company_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.admin_usersadmin(id) ON DELETE CASCADE;


--
-- TOC entry 3496 (class 2606 OID 24053)
-- Name: admin_conversiones admin_conversiones_unidad_final_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_conversiones
    ADD CONSTRAINT admin_conversiones_unidad_final_id_foreign FOREIGN KEY (unidad_final_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3497 (class 2606 OID 24058)
-- Name: admin_conversiones admin_conversiones_unidad_inicial_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_conversiones
    ADD CONSTRAINT admin_conversiones_unidad_inicial_id_foreign FOREIGN KEY (unidad_inicial_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3498 (class 2606 OID 24063)
-- Name: admin_equipo_asignado admin_equipo_asignado_asignado_a_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipo_asignado
    ADD CONSTRAINT admin_equipo_asignado_asignado_a_bodega_id_foreign FOREIGN KEY (asignado_a_bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3499 (class 2606 OID 24068)
-- Name: admin_equipo_asignado admin_equipo_asignado_asignado_a_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipo_asignado
    ADD CONSTRAINT admin_equipo_asignado_asignado_a_cliente_id_foreign FOREIGN KEY (asignado_a_cliente_id) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3500 (class 2606 OID 24073)
-- Name: admin_equipo_asignado admin_equipo_asignado_asignado_a_colaborador_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipo_asignado
    ADD CONSTRAINT admin_equipo_asignado_asignado_a_colaborador_id_foreign FOREIGN KEY (asignado_a_colaborador_id) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3501 (class 2606 OID 24078)
-- Name: admin_equipo_asignado admin_equipo_asignado_equipo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipo_asignado
    ADD CONSTRAINT admin_equipo_asignado_equipo_id_foreign FOREIGN KEY (equipo_id) REFERENCES public.admin_equipos(id);


--
-- TOC entry 3502 (class 2606 OID 24083)
-- Name: admin_equipo_asignado admin_equipo_asignado_origen_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipo_asignado
    ADD CONSTRAINT admin_equipo_asignado_origen_bodega_id_foreign FOREIGN KEY (origen_bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3503 (class 2606 OID 24088)
-- Name: admin_equipos admin_equipos_categoria_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipos
    ADD CONSTRAINT admin_equipos_categoria_id_foreign FOREIGN KEY (categoria_id) REFERENCES public.admin_categories(id);


--
-- TOC entry 3504 (class 2606 OID 24093)
-- Name: admin_equipos admin_equipos_medida_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipos
    ADD CONSTRAINT admin_equipos_medida_id_foreign FOREIGN KEY (medida_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3505 (class 2606 OID 24098)
-- Name: admin_equipos admin_equipos_sub_categoria_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_equipos
    ADD CONSTRAINT admin_equipos_sub_categoria_id_foreign FOREIGN KEY (sub_categoria_id) REFERENCES public."admin_subCategories"(id);


--
-- TOC entry 3506 (class 2606 OID 24103)
-- Name: admin_facturas admin_facturas_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_facturas
    ADD CONSTRAINT admin_facturas_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3507 (class 2606 OID 24108)
-- Name: admin_facturas admin_facturas_proveedor_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_facturas
    ADD CONSTRAINT admin_facturas_proveedor_id_foreign FOREIGN KEY (proveedor_id) REFERENCES public.admin_providers(id);


--
-- TOC entry 3508 (class 2606 OID 24113)
-- Name: admin_loggs_user_paiments admin_loggs_user_paiments_idloogs_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_loggs_user_paiments
    ADD CONSTRAINT admin_loggs_user_paiments_idloogs_foreign FOREIGN KEY ("idLoogs") REFERENCES public.loggs_user(id);


--
-- TOC entry 3509 (class 2606 OID 24118)
-- Name: admin_loggs_user_paiments admin_loggs_user_paiments_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_loggs_user_paiments
    ADD CONSTRAINT admin_loggs_user_paiments_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3510 (class 2606 OID 24123)
-- Name: admin_petitions_complaintments admin_petitions_complaintments_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_complaintments
    ADD CONSTRAINT admin_petitions_complaintments_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3511 (class 2606 OID 24128)
-- Name: admin_petitions_economicpetitions admin_petitions_economicpetitions_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_economicpetitions
    ADD CONSTRAINT admin_petitions_economicpetitions_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3512 (class 2606 OID 24133)
-- Name: admin_petitions_restoretimepermissions admin_petitions_restoretimepermissions_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_restoretimepermissions
    ADD CONSTRAINT admin_petitions_restoretimepermissions_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3513 (class 2606 OID 24138)
-- Name: admin_petitions_simplepetitions_save admin_petitions_simplepetitions_save_idtype_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_simplepetitions_save
    ADD CONSTRAINT admin_petitions_simplepetitions_save_idtype_foreign FOREIGN KEY ("idType") REFERENCES public.admin_petitions_simplepetitions(id);


--
-- TOC entry 3514 (class 2606 OID 24143)
-- Name: admin_petitions_simplepetitions_save admin_petitions_simplepetitions_save_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_simplepetitions_save
    ADD CONSTRAINT admin_petitions_simplepetitions_save_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3515 (class 2606 OID 24148)
-- Name: admin_petitions_timepermissions admin_petitions_timepermissions_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_petitions_timepermissions
    ADD CONSTRAINT admin_petitions_timepermissions_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3516 (class 2606 OID 24153)
-- Name: admin_productos admin_productos_categoria_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_productos
    ADD CONSTRAINT admin_productos_categoria_id_foreign FOREIGN KEY (categoria_id) REFERENCES public.admin_categories(id);


--
-- TOC entry 3517 (class 2606 OID 24158)
-- Name: admin_productos admin_productos_medida_final_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_productos
    ADD CONSTRAINT admin_productos_medida_final_id_foreign FOREIGN KEY (medida_final_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3518 (class 2606 OID 24163)
-- Name: admin_productos admin_productos_sub_categoria_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_productos
    ADD CONSTRAINT admin_productos_sub_categoria_id_foreign FOREIGN KEY (sub_categoria_id) REFERENCES public."admin_subCategories"(id);


--
-- TOC entry 3519 (class 2606 OID 24168)
-- Name: admin_stock admin_stock_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock
    ADD CONSTRAINT admin_stock_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3522 (class 2606 OID 24173)
-- Name: admin_stock_egresos admin_stock_egresos_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3523 (class 2606 OID 24178)
-- Name: admin_stock_egresos admin_stock_egresos_cobrar_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_cobrar_cliente_id_foreign FOREIGN KEY (cobrar_cliente_id) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3524 (class 2606 OID 24183)
-- Name: admin_stock_egresos admin_stock_egresos_cobrar_colaborador_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_cobrar_colaborador_id_foreign FOREIGN KEY (cobrar_colaborador_id) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3530 (class 2606 OID 24188)
-- Name: admin_stock_egresos_equipo admin_stock_egresos_equipo_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos_equipo
    ADD CONSTRAINT admin_stock_egresos_equipo_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3531 (class 2606 OID 24193)
-- Name: admin_stock_egresos_equipo admin_stock_egresos_equipo_cargo_a_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos_equipo
    ADD CONSTRAINT admin_stock_egresos_equipo_cargo_a_cliente_id_foreign FOREIGN KEY (cargo_a_cliente_id) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3532 (class 2606 OID 24198)
-- Name: admin_stock_egresos_equipo admin_stock_egresos_equipo_cargo_a_colaborador_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos_equipo
    ADD CONSTRAINT admin_stock_egresos_equipo_cargo_a_colaborador_id_foreign FOREIGN KEY (cargo_a_colaborador_id) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3533 (class 2606 OID 24203)
-- Name: admin_stock_egresos_equipo admin_stock_egresos_equipo_equipo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos_equipo
    ADD CONSTRAINT admin_stock_egresos_equipo_equipo_id_foreign FOREIGN KEY (equipo_id) REFERENCES public.admin_equipos(id);


--
-- TOC entry 3525 (class 2606 OID 24208)
-- Name: admin_stock_egresos admin_stock_egresos_medida_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_medida_id_foreign FOREIGN KEY (medida_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3526 (class 2606 OID 24213)
-- Name: admin_stock_egresos admin_stock_egresos_producto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_producto_id_foreign FOREIGN KEY (producto_id) REFERENCES public.admin_productos(id);


--
-- TOC entry 3527 (class 2606 OID 24218)
-- Name: admin_stock_egresos admin_stock_egresos_receptor_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_receptor_bodega_id_foreign FOREIGN KEY (receptor_bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3528 (class 2606 OID 24223)
-- Name: admin_stock_egresos admin_stock_egresos_receptor_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_receptor_cliente_id_foreign FOREIGN KEY (receptor_cliente_id) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3529 (class 2606 OID 24228)
-- Name: admin_stock_egresos admin_stock_egresos_receptor_colaborador_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_egresos
    ADD CONSTRAINT admin_stock_egresos_receptor_colaborador_id_foreign FOREIGN KEY (receptor_colaborador_id) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3534 (class 2606 OID 24233)
-- Name: admin_stock_equipos admin_stock_equipos_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_equipos
    ADD CONSTRAINT admin_stock_equipos_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3535 (class 2606 OID 24238)
-- Name: admin_stock_equipos admin_stock_equipos_equipo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_equipos
    ADD CONSTRAINT admin_stock_equipos_equipo_id_foreign FOREIGN KEY (equipo_id) REFERENCES public.admin_equipos(id);


--
-- TOC entry 3536 (class 2606 OID 24243)
-- Name: admin_stock_equipos admin_stock_equipos_medida_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_equipos
    ADD CONSTRAINT admin_stock_equipos_medida_id_foreign FOREIGN KEY (medida_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3537 (class 2606 OID 24248)
-- Name: admin_stock_ingresos admin_stock_ingresos_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos
    ADD CONSTRAINT admin_stock_ingresos_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3541 (class 2606 OID 24253)
-- Name: admin_stock_ingresos_equipo admin_stock_ingresos_equipo_bodega_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos_equipo
    ADD CONSTRAINT admin_stock_ingresos_equipo_bodega_id_foreign FOREIGN KEY (bodega_id) REFERENCES public.admin_bodegas(id);


--
-- TOC entry 3542 (class 2606 OID 24258)
-- Name: admin_stock_ingresos_equipo admin_stock_ingresos_equipo_equipo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos_equipo
    ADD CONSTRAINT admin_stock_ingresos_equipo_equipo_id_foreign FOREIGN KEY (equipo_id) REFERENCES public.admin_equipos(id);


--
-- TOC entry 3543 (class 2606 OID 24263)
-- Name: admin_stock_ingresos_equipo admin_stock_ingresos_equipo_factura_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos_equipo
    ADD CONSTRAINT admin_stock_ingresos_equipo_factura_id_foreign FOREIGN KEY (factura_id) REFERENCES public.admin_facturas(id);


--
-- TOC entry 3538 (class 2606 OID 24268)
-- Name: admin_stock_ingresos admin_stock_ingresos_factura_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos
    ADD CONSTRAINT admin_stock_ingresos_factura_id_foreign FOREIGN KEY (factura_id) REFERENCES public.admin_facturas(id);


--
-- TOC entry 3539 (class 2606 OID 24273)
-- Name: admin_stock_ingresos admin_stock_ingresos_medida_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos
    ADD CONSTRAINT admin_stock_ingresos_medida_id_foreign FOREIGN KEY (medida_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3540 (class 2606 OID 24278)
-- Name: admin_stock_ingresos admin_stock_ingresos_producto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock_ingresos
    ADD CONSTRAINT admin_stock_ingresos_producto_id_foreign FOREIGN KEY (producto_id) REFERENCES public.admin_productos(id);


--
-- TOC entry 3520 (class 2606 OID 24283)
-- Name: admin_stock admin_stock_medida_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock
    ADD CONSTRAINT admin_stock_medida_id_foreign FOREIGN KEY (medida_id) REFERENCES public.admin_unidades(id);


--
-- TOC entry 3521 (class 2606 OID 24288)
-- Name: admin_stock admin_stock_producto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_stock
    ADD CONSTRAINT admin_stock_producto_id_foreign FOREIGN KEY (producto_id) REFERENCES public.admin_productos(id);


--
-- TOC entry 3544 (class 2606 OID 24293)
-- Name: admin_subCategories admin_subcategories_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public."admin_subCategories"
    ADD CONSTRAINT admin_subcategories_category_id_foreign FOREIGN KEY (category_id) REFERENCES public.admin_categories(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3545 (class 2606 OID 24298)
-- Name: admin_users admin_users_roleus_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_roleus_foreign FOREIGN KEY ("roleUS") REFERENCES public.admin_roles(id) ON DELETE CASCADE;


--
-- TOC entry 3546 (class 2606 OID 24303)
-- Name: admin_werehouse_products admin_werehouse_products_id_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_werehouse_products
    ADD CONSTRAINT admin_werehouse_products_id_product_foreign FOREIGN KEY (id_product) REFERENCES public.admin_products(id) ON DELETE CASCADE;


--
-- TOC entry 3547 (class 2606 OID 24308)
-- Name: admin_werehouse_products admin_werehouse_products_id_werhouse_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_werehouse_products
    ADD CONSTRAINT admin_werehouse_products_id_werhouse_foreign FOREIGN KEY (id_werhouse) REFERENCES public.admin_warehouse(id) ON DELETE CASCADE;


--
-- TOC entry 3548 (class 2606 OID 24313)
-- Name: admin_werehouse_products admin_werehouse_products_whom_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.admin_werehouse_products
    ADD CONSTRAINT admin_werehouse_products_whom_foreign FOREIGN KEY (whom) REFERENCES public.admin_users(id) ON DELETE CASCADE;


--
-- TOC entry 3549 (class 2606 OID 24318)
-- Name: descuentos descuentos_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.descuentos
    ADD CONSTRAINT descuentos_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3550 (class 2606 OID 24323)
-- Name: descuentos descuentos_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.descuentos
    ADD CONSTRAINT descuentos_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3551 (class 2606 OID 24328)
-- Name: log_descuentos log_descuentos_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_descuentos
    ADD CONSTRAINT log_descuentos_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3552 (class 2606 OID 24333)
-- Name: log_descuentos log_descuentos_id_desc_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_descuentos
    ADD CONSTRAINT log_descuentos_id_desc_foreign FOREIGN KEY (id_desc) REFERENCES public.descuentos(id);


--
-- TOC entry 3553 (class 2606 OID 24338)
-- Name: log_descuentos log_descuentos_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_descuentos
    ADD CONSTRAINT log_descuentos_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3554 (class 2606 OID 24343)
-- Name: log_liquidacion log_liquidacion_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_liquidacion
    ADD CONSTRAINT log_liquidacion_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3555 (class 2606 OID 24348)
-- Name: log_liquidacion log_liquidacion_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_liquidacion
    ADD CONSTRAINT log_liquidacion_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3556 (class 2606 OID 24353)
-- Name: log_prestaciones log_prestaciones_id_area_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_prestaciones
    ADD CONSTRAINT log_prestaciones_id_area_foreign FOREIGN KEY (id_area) REFERENCES public.admin_company_clients_branch_area(id);


--
-- TOC entry 3557 (class 2606 OID 24358)
-- Name: log_prestaciones log_prestaciones_id_cliente_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_prestaciones
    ADD CONSTRAINT log_prestaciones_id_cliente_foreign FOREIGN KEY (id_cliente) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3558 (class 2606 OID 24363)
-- Name: log_prestaciones log_prestaciones_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_prestaciones
    ADD CONSTRAINT log_prestaciones_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3559 (class 2606 OID 24368)
-- Name: log_prestaciones log_prestaciones_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_prestaciones
    ADD CONSTRAINT log_prestaciones_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3560 (class 2606 OID 24373)
-- Name: log_prestaciones log_prestaciones_id_sucursal_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.log_prestaciones
    ADD CONSTRAINT log_prestaciones_id_sucursal_foreign FOREIGN KEY (id_sucursal) REFERENCES public.admin_company_clients_branchof(id);


--
-- TOC entry 3561 (class 2606 OID 24378)
-- Name: loggs_user loggs_user_idarea_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_idarea_foreign FOREIGN KEY ("idArea") REFERENCES public.admin_company_clients_branch_area(id) ON DELETE RESTRICT;


--
-- TOC entry 3562 (class 2606 OID 24383)
-- Name: loggs_user loggs_user_idbranch_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_idbranch_foreign FOREIGN KEY ("idBranch") REFERENCES public.admin_company_clients_branchof(id) ON DELETE RESTRICT;


--
-- TOC entry 3563 (class 2606 OID 24388)
-- Name: loggs_user loggs_user_idclient_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_idclient_foreign FOREIGN KEY ("idClient") REFERENCES public.admin_company_clients(id) ON DELETE RESTRICT;


--
-- TOC entry 3564 (class 2606 OID 24393)
-- Name: loggs_user loggs_user_idcompany_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_idcompany_foreign FOREIGN KEY ("idCompany") REFERENCES public.admin_company(id) ON DELETE RESTRICT;


--
-- TOC entry 3565 (class 2606 OID 24398)
-- Name: loggs_user loggs_user_idday_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_idday_foreign FOREIGN KEY ("idDay") REFERENCES public.admin_horary(id) ON DELETE RESTRICT;


--
-- TOC entry 3566 (class 2606 OID 24403)
-- Name: loggs_user loggs_user_idlink_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_idlink_foreign FOREIGN KEY ("idLink") REFERENCES public.admin_client_colab_assoc(id) ON DELETE RESTRICT;


--
-- TOC entry 3568 (class 2606 OID 24408)
-- Name: loggs_user_planilla loggs_user_planilla_id_area_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user_planilla
    ADD CONSTRAINT loggs_user_planilla_id_area_foreign FOREIGN KEY (id_area) REFERENCES public.admin_company_clients_branch_area(id);


--
-- TOC entry 3569 (class 2606 OID 24413)
-- Name: loggs_user_planilla loggs_user_planilla_id_cliente_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user_planilla
    ADD CONSTRAINT loggs_user_planilla_id_cliente_foreign FOREIGN KEY (id_cliente) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3570 (class 2606 OID 24418)
-- Name: loggs_user_planilla loggs_user_planilla_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user_planilla
    ADD CONSTRAINT loggs_user_planilla_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3571 (class 2606 OID 24423)
-- Name: loggs_user_planilla loggs_user_planilla_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user_planilla
    ADD CONSTRAINT loggs_user_planilla_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3572 (class 2606 OID 24428)
-- Name: loggs_user_planilla loggs_user_planilla_id_sucursal_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user_planilla
    ADD CONSTRAINT loggs_user_planilla_id_sucursal_foreign FOREIGN KEY (id_sucursal) REFERENCES public.admin_company_clients_branchof(id);


--
-- TOC entry 3567 (class 2606 OID 24433)
-- Name: loggs_user loggs_user_userid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.loggs_user
    ADD CONSTRAINT loggs_user_userid_foreign FOREIGN KEY ("userId") REFERENCES public.admin_colaboradores(id) ON DELETE RESTRICT;


--
-- TOC entry 3573 (class 2606 OID 24438)
-- Name: user_horas user_horas_id_area_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_horas
    ADD CONSTRAINT user_horas_id_area_foreign FOREIGN KEY (id_area) REFERENCES public.admin_company_clients_branch_area(id);


--
-- TOC entry 3574 (class 2606 OID 24443)
-- Name: user_horas user_horas_id_cliente_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_horas
    ADD CONSTRAINT user_horas_id_cliente_foreign FOREIGN KEY (id_cliente) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3575 (class 2606 OID 24448)
-- Name: user_horas user_horas_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_horas
    ADD CONSTRAINT user_horas_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3576 (class 2606 OID 24453)
-- Name: user_horas user_horas_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_horas
    ADD CONSTRAINT user_horas_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3577 (class 2606 OID 24458)
-- Name: user_horas user_horas_id_sucursal_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_horas
    ADD CONSTRAINT user_horas_id_sucursal_foreign FOREIGN KEY (id_sucursal) REFERENCES public.admin_company_clients_branchof(id);


--
-- TOC entry 3578 (class 2606 OID 24463)
-- Name: user_pagos user_pagos_id_cliente_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos
    ADD CONSTRAINT user_pagos_id_cliente_foreign FOREIGN KEY (id_cliente) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3579 (class 2606 OID 24468)
-- Name: user_pagos user_pagos_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos
    ADD CONSTRAINT user_pagos_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3580 (class 2606 OID 24473)
-- Name: user_pagos user_pagos_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos
    ADD CONSTRAINT user_pagos_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3581 (class 2606 OID 24478)
-- Name: user_pagos user_pagos_id_horas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos
    ADD CONSTRAINT user_pagos_id_horas_foreign FOREIGN KEY (id_horas) REFERENCES public.user_horas(id);


--
-- TOC entry 3582 (class 2606 OID 24483)
-- Name: user_pagos user_pagos_id_planilla_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos
    ADD CONSTRAINT user_pagos_id_planilla_foreign FOREIGN KEY (id_planilla) REFERENCES public.loggs_user_planilla(id);


--
-- TOC entry 3583 (class 2606 OID 24488)
-- Name: user_pagos user_pagos_id_viaticos_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_pagos
    ADD CONSTRAINT user_pagos_id_viaticos_foreign FOREIGN KEY (id_viaticos) REFERENCES public.user_viaticos(id);


--
-- TOC entry 3584 (class 2606 OID 24493)
-- Name: user_viaticos user_viaticos_id_area_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_viaticos
    ADD CONSTRAINT user_viaticos_id_area_foreign FOREIGN KEY (id_area) REFERENCES public.admin_company_clients_branch_area(id);


--
-- TOC entry 3585 (class 2606 OID 24498)
-- Name: user_viaticos user_viaticos_id_cliente_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_viaticos
    ADD CONSTRAINT user_viaticos_id_cliente_foreign FOREIGN KEY (id_cliente) REFERENCES public.admin_company_clients(id);


--
-- TOC entry 3586 (class 2606 OID 24503)
-- Name: user_viaticos user_viaticos_id_colab_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_viaticos
    ADD CONSTRAINT user_viaticos_id_colab_foreign FOREIGN KEY (id_colab) REFERENCES public.admin_colaboradores(id);


--
-- TOC entry 3587 (class 2606 OID 24508)
-- Name: user_viaticos user_viaticos_id_empresa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_viaticos
    ADD CONSTRAINT user_viaticos_id_empresa_foreign FOREIGN KEY (id_empresa) REFERENCES public.admin_company(id);


--
-- TOC entry 3588 (class 2606 OID 24513)
-- Name: user_viaticos user_viaticos_id_sucursal_foreign; Type: FK CONSTRAINT; Schema: public; Owner: matrisapre
--

ALTER TABLE ONLY public.user_viaticos
    ADD CONSTRAINT user_viaticos_id_sucursal_foreign FOREIGN KEY (id_sucursal) REFERENCES public.admin_company_clients_branchof(id);


-- Completed on 2022-05-26 18:29:11 CST

--
-- PostgreSQL database dump complete
--

