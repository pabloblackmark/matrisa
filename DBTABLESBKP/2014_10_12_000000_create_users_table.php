<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admusers', function (Blueprint $table) {
            $table->id();
            $table->string('name',40);
            $table->string('usersis',25)->unique();
            $table->string('pass');
            $table->string('statusUs',1);
            $table->string('mail');
            $table->string('country',10);
            $table->string('role',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
