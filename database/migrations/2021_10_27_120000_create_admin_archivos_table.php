<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_archivos', function (Blueprint $table) {
            $table->id();

            $table->string("id_file_manager", 200)->nullable();
            $table->string("name", 200)->nullable();
            $table->text("description")->nullable();
            $table->string("url", 200)->nullable();
            $table->string("type", 200)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_archivos');
    }
}
