<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_facturas', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('proveedor_id')->nullable();
          $table->string('numero')->nullable();
          $table->string('serie')->nullable();
          $table->decimal('total', 14, 2)->nullable();
          $table->date('fecha')->nullable();
          $table->text('info')->nullable();
          $table->string('estado')->nullable();
          $table->unsignedBigInteger('bodega_id');

          $table->foreign('proveedor_id')->references('id')->on('admin_providers');
          $table->foreign('bodega_id')->references('id')->on('admin_bodegas');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_facturas');
    }
}
