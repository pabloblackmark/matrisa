<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminWerehouseProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_werehouse_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_werhouse");
            $table->unsignedBigInteger("id_product");
            $table->integer("amount");
            $table->string("lot",25);
            $table->date("datein");
            $table->unsignedBigInteger("whom");
            $table->foreign('whom')
                  ->references('id')->on('admin_users')
                  ->onDelete('cascade');
            $table->foreign('id_werhouse')
                  ->references('id')->on('admin_warehouse')
                  ->onDelete('cascade');
            $table->foreign('id_product')
                  ->references('id')->on('admin_products')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_werehouse_products');
    }
}
