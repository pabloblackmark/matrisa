<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCotisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_cotis', function (Blueprint $table) {
            $table->id();
            $table->string("no");
            $table->date("fecha");
            $table->string("id_empresa");
            $table->string("empresa");
            $table->string("id_cliente");
            $table->string("cliente");
            $table->string("id_sede");
            $table->string("sede");
            $table->string("nit")->nullable();
            $table->text("detalle");
            $table->string("total");
            $table->string("firma");
            $table->string("id_admin");
            $table->string('status',1)->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_cotis');
    }
}
