<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminPlanillaTotalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_loggs_user_paiments', function (Blueprint $table) {
            $table->id();
            $table->string('datePay')->nullable();
            $table->string('dayPay')->nullable();
            $table->string('viaticTotal')->nullable();
            $table->text('viaticAdd')->nullable();
            $table->text('descript')->nullable();
            $table->string('status',2)->nullable()->default('0');
            $table->text('reponse')->nullable();
            $table->date('responseDate')->nullable();
            $table->string('authorized')->nullable();
            $table->unsignedBigInteger('idLoogs')->nullable();
            $table->foreign('idLoogs')->references('id')->on('loggs_user');
            $table->unsignedBigInteger('idUser')->nullable();
            $table->foreign('idUser')->references('id')->on('admin_colaboradores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_planilla_totales');
    }
}
