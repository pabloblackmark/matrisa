<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminColaboradores6Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('admin_colaboradores', function (Blueprint $table) {

        $table->float("sueldo", 10, 4)->nullable()->change();
        $table->float("bono_extra", 10, 4)->nullable()->change();
        $table->float("antiguedad", 10, 4)->nullable()->change();
        $table->float("bono_ley", 10, 4)->nullable()->change();
        $table->float("hora_extra_diurna", 10, 4)->nullable()->change();
        $table->float("hora_extra_nocturna", 10, 4)->nullable()->change();
        $table->float("hora_extra_especial", 10, 4)->nullable()->change();
        $table->float("viaticos", 10, 4)->nullable()->change();
        $table->float("movil", 10, 4)->nullable()->change();
        $table->float("hora_normal", 10, 4)->nullable()->change();
        $table->float("hora_normal_nocturna", 10, 4)->nullable()->change();

        $table->float("puntos1", 10, 4)->nullable()->change();
        $table->float("puntos2", 10, 4)->nullable()->change();
        $table->float("puntos3", 10, 4)->nullable()->change();
        $table->float("puntos4", 10, 4)->nullable()->change();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
