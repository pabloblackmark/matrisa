<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCompanyClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company_clients', function (Blueprint $table) {
            $table->id();
            $table->string("clientName");
            $table->string("nit",25);
            $table->string("contactName");
            $table->string("email");
            $table->integer("phone1");
            $table->integer("phone2");
            $table->string("depto");
            $table->string("municip");
            $table->string("addess");
            $table->integer("credit");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_clients');
    }
}
