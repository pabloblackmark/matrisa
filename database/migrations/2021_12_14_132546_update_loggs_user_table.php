<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLoggsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('loggs_user', function (Blueprint $table) {
        $table->date('dateAproved')->nullable();
        $table->string('dayPay')->nullable();
        $table->float('extraHourEspecMiddle',10,4)->nullable();
        $table->text('viaticsDesc')->nullable();
        $table->integer("viaticsMovil")->default(0)->nullable()->add();
        $table->string("typeDay")->default('asistencia')->nullable()->add();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
