<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserViaticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_viaticos', function (Blueprint $table) {
            //
            $table->float('total',10,4)->nullable()->change();

            $table->float('viaticos',10,4)->nullable()->change();
            $table->float('movil',10,4)->nullable()->change();
            $table->float('extras',10,4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_viaticos', function (Blueprint $table) {
            //
        });
    }
}
