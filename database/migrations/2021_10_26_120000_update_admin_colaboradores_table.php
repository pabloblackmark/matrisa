<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminColaboradoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_colaboradores', function (Blueprint $table) {
            $table->date("vence_licencia")->nullable()->add();
            $table->dropColumn('cuenta');
            $table->string('cuenta1', 100)->nullable()->add();
            $table->string('cuenta2', 100)->nullable()->add();
            $table->string('cuenta3', 100)->nullable()->add();
            $table->string('firma')->nullable()->add();
            $table->string('id_file_manager')->nullable()->add();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
