<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_pagos', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_empresa')->nullable();
            $table->foreign('id_empresa')->references('id')->on('admin_company');
            // $table->unsignedBigInteger("id_cliente")->nullable();
            // $table->foreign('id_cliente')->references('id')->on('admin_company_clients');
            $table->unsignedBigInteger('id_colab')->nullable();
            $table->foreign('id_colab')->references('id')->on('admin_colaboradores');

            $table->unsignedBigInteger("id_planilla")->nullable();
            $table->foreign('id_planilla')->references('id')->on('loggs_user_planilla');

            $table->unsignedBigInteger("id_horas")->nullable();
            $table->foreign('id_horas')->references('id')->on('user_horas');

            $table->unsignedBigInteger("id_viaticos")->nullable();
            $table->foreign('id_viaticos')->references('id')->on('user_viaticos');

            $table->float('total',10,4)->nullable();

            $table->float('total_planilla',10,4)->nullable();
            $table->float('total_horas',10,4)->nullable();
            $table->float('total_viaticos',10,4)->nullable();

            $table->string('estado')->nullable();
            $table->date('fecha_pago')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_pagos');
    }
}
