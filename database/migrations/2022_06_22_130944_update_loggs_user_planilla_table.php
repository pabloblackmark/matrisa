<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLoggsUserPlanillaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loggs_user_planilla', function (Blueprint $table) {
            //
            $table->integer("dias")->default(0)->nullable()->add();
            $table->float('sueldo',10,4)->nullable()->change();
            $table->float('total',10,4)->nullable()->change();

            $table->float('bono_ley',10,4)->nullable()->change();
            $table->float('bono_extra',10,4)->nullable()->change();
            $table->float('bono_antiguedad',10,4)->nullable()->change();

            $table->float('desc_dia',10,4)->nullable()->change();
            $table->float('desc_septimo',10,4)->nullable()->change();
            $table->float('desc_igss',10,4)->nullable()->change();
            $table->float('total_bonos',10,4)->nullable()->change();
            $table->float('total_descuentos',10,4)->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loggs_user_planilla', function (Blueprint $table) {
            //
        });
    }
}
