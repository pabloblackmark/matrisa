<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminAlertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_alertas', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger("elemento_id")->nullable();
            $table->string("referencia")->unique();
            $table->string("tipo")->nullable();
            $table->text("mensaje")->nullable();
            $table->text('nombre_modulo')->nullable();
            $table->string('url')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_alertas');
    }
}
