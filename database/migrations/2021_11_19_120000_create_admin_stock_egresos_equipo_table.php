<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminStockEgresosEquipoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_stock_egresos_equipo', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('equipo_id')->nullable();
          $table->bigInteger('cantidad')->nullable();
          $table->decimal('costo_unidad', 14, 2)->nullable();
          $table->decimal('costo_total', 14, 2)->nullable();
          $table->decimal('precio_total', 14, 2)->nullable();

          $table->unsignedBigInteger('bodega_id')->nullable();
          $table->unsignedBigInteger('cargo_a_cliente_id')->nullable();
          $table->unsignedBigInteger('cargo_a_colaborador_id')->nullable();

          $table->foreign('equipo_id')->references('id')->on('admin_equipos');
          $table->foreign('bodega_id')->references('id')->on('admin_bodegas');
          $table->foreign('cargo_a_cliente_id')->references('id')->on('admin_company_clients');
          $table->foreign('cargo_a_colaborador_id')->references('id')->on('admin_colaboradores');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_stock_egresos_equipo');
    }
}
