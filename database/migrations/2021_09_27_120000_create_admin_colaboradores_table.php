<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminColaboradoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_colaboradores', function (Blueprint $table) {
            $table->id();

            $table->string("nombres", 100)->nullable();
            $table->string("apellidos", 100)->nullable();
            $table->text("foto")->nullable();
            $table->date("fecha")->nullable();
            $table->string("email", 100)->nullable();
            $table->string("dpi", 50)->nullable();
            $table->string("nit", 50)->nullable();
            $table->string("iggs", 50)->nullable();
            $table->string("irtra", 50)->nullable();
            $table->string("intecap", 50)->nullable();
            $table->string("licencia", 50)->nullable();
            $table->date("fecha_salud")->nullable();
            $table->date("fecha_pulmones")->nullable();
            $table->string("genero", 50)->nullable();
            $table->string("nivel_academico", 100)->nullable();
            $table->string("nacionalidad", 50)->nullable();
            $table->string("estado_civil", 50)->nullable();
            $table->string("conyuge", 150)->nullable();
            $table->unsignedSmallInteger("hijos")->default(0);
            $table->string("profesion", 100)->nullable();
            $table->string("tel1", 18)->nullable();
            $table->string("tel2", 18)->nullable();
            $table->text("direccion", 100)->nullable();
            $table->string("departamento", 100)->nullable();
            $table->string("municipio", 100)->nullable();
            $table->string("contacto1", 200)->nullable();
            $table->string("tel_con1", 18)->nullable();
            $table->string("parentesco_con1", 100)->nullable();
            $table->string("contacto2", 200)->nullable();
            $table->string("tel_con2", 18)->nullable();
            $table->string("parentesco_con2", 100)->nullable();
            $table->string("tipo_sangre", 50)->nullable();
            $table->string("contrato", 200)->nullable();
            $table->string("puesto", 100)->nullable();
            $table->double("sueldo", 6, 2)->nullable();
            $table->double("bono_extra", 6, 2)->nullable();
            $table->double("antiguedad", 6, 2)->nullable();
            $table->double("bono_ley", 6, 2)->nullable();
            $table->double("hora_extra_diurna", 6, 2)->nullable();
            $table->double("hora_extra_nocturna", 6, 2)->nullable();
            $table->double("hora_extra_especial", 6, 2)->nullable();
            $table->double("viaticos", 6, 2)->nullable();
            $table->double("movil", 6, 2)->nullable();
            $table->double("hora_normal", 6, 2)->nullable();
            $table->double("hora_normal_nocturna", 6, 2)->nullable();
            $table->string("tipo_pago", 200)->nullable();
            $table->string("cuenta", 50)->nullable();
            $table->string("banco", 50)->nullable();
            $table->string("pago_sueldo", 50)->nullable();
            $table->string("otros_pagos", 50)->nullable();

            $table->double("puntos1", 6, 2)->nullable();
            $table->double("puntos2", 6, 2)->nullable();
            $table->double("puntos3", 6, 2)->nullable();
            $table->double("puntos4", 6, 2)->nullable();

            $table->string("estatus", 50)->nullable();
            $table->date("fecha_ingreso")->nullable();
            $table->text("observaciones")->nullable();


            $table->unsignedBigInteger("idUser")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_colaboradores');
    }
}
