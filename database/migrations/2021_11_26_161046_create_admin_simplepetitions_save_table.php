<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminSimplepetitionsSaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_petitions_simplepetitions_save', function (Blueprint $table) {
            $table->id();
            $table->text('comment')->nullable();
            $table->string('status',2)->nullable()->default('0');
            $table->text('reponse')->nullable();
            $table->date('responseDate')->nullable();
            $table->unsignedBigInteger('idUser')->nullable();
            $table->foreign('idUser')->references('id')->on('admin_colaboradores');
            $table->unsignedBigInteger('idType')->nullable();
            $table->foreign('idType')->references('id')->on('admin_petitions_simplepetitions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_petitions_simplepetitions_save');
    }
}
