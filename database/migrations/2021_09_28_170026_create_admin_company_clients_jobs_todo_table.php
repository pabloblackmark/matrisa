<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCompanyClientsJobsTodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company_clients_jobs_todo', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->unsignedBigInteger("idJob");
            $table->foreign('idJob')
                  ->references('id')->on('admin_company_clients_jobs')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_clients_jobs_todo');
    }
}
