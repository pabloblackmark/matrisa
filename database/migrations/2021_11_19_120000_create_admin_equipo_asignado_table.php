<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminEquipoAsignadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_equipo_asignado', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('equipo_id')->nullable();
          $table->unsignedBigInteger('asignado_a_bodega_id')->nullable();
          $table->unsignedBigInteger('asignado_a_cliente_id')->nullable();
          $table->unsignedBigInteger('asignado_a_colaborador_id')->nullable();
          $table->unsignedBigInteger('origen_bodega_id')->nullable();
          $table->string('tipo_movimiento')->nullable();
          $table->date('fecha_movimiento')->nullable();
          $table->text('comentario')->nullable();
          $table->string('usuario')->nullable();

          $table->foreign('equipo_id')->references('id')->on('admin_equipos');
          $table->foreign('asignado_a_bodega_id')->references('id')->on('admin_bodegas');
          $table->foreign('asignado_a_cliente_id')->references('id')->on('admin_company_clients');
          $table->foreign('asignado_a_colaborador_id')->references('id')->on('admin_colaboradores');
          $table->foreign('origen_bodega_id')->references('id')->on('admin_bodegas');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_equipo_asignado');
    }
}
