<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminStockEgresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_stock_egresos', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('producto_id')->nullable();
          $table->bigInteger('cantidad_medida')->nullable();
          $table->unsignedBigInteger('medida_id')->nullable();
          $table->bigInteger('cantidad')->nullable();
          $table->string('medida')->nullable();
          $table->decimal('costo_unitario',14,2)->nullable();
          $table->decimal('valor_total',14,2)->nullable();
          $table->decimal('precio_unitario',14,2)->nullable();
          $table->decimal('precio_total',14,2)->nullable();
          $table->string('tipo_egreso')->nullable();
          $table->string('tipo_cobro')->nullable();
          $table->integer('cuotas')->nullable();
          $table->unsignedBigInteger('bodega_id')->nullable();

          $table->unsignedBigInteger('receptor_cliente_id')->nullable();
          $table->unsignedBigInteger('receptor_colaborador_id')->nullable();
          $table->unsignedBigInteger('receptor_bodega_id')->nullable();

          $table->unsignedBigInteger('cobrar_cliente_id')->nullable();
          $table->unsignedBigInteger('cobrar_colaborador_id')->nullable();

          $table->foreign('producto_id')->references('id')->on('admin_productos');
          $table->foreign('medida_id')->references('id')->on('admin_unidades');
          $table->foreign('bodega_id')->references('id')->on('admin_bodegas');

          $table->foreign('receptor_bodega_id')->references('id')->on('admin_bodegas');
          $table->foreign('receptor_cliente_id')->references('id')->on('admin_company_clients');
          $table->foreign('receptor_colaborador_id')->references('id')->on('admin_colaboradores');

          $table->foreign('cobrar_cliente_id')->references('id')->on('admin_company_clients');
          $table->foreign('cobrar_colaborador_id')->references('id')->on('admin_colaboradores');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_stock_egresos');
    }
}
