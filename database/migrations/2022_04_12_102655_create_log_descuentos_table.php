<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_descuentos', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_empresa')->nullable();
            $table->foreign('id_empresa')->references('id')->on('admin_company');
            $table->unsignedBigInteger('id_colab')->nullable();
            $table->foreign('id_colab')->references('id')->on('admin_colaboradores');
            $table->unsignedBigInteger('id_desc')->nullable();
            $table->foreign('id_desc')->references('id')->on('descuentos');

            $table->date('fecha')->nullable();
            $table->float('pago',10,4)->nullable();
            $table->integer("no_cuota")->default(0)->nullable();
            $table->float('saldo',10,4)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_descuentos');
    }
}
