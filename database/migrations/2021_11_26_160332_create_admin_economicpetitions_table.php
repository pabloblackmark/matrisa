<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminEconomicpetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_petitions_economicpetitions', function (Blueprint $table) {
            $table->id();
            $table->string('typos')->nullable();
            $table->string('applyTo')->nullable();
            $table->string('otherAplyText')->nullable();
            $table->text('comment')->nullable();
            $table->text('descript')->nullable();
            $table->string('status',2)->nullable()->default('0');
            $table->text('reponse')->nullable();
            $table->date('responseDate')->nullable();
            $table->string('authorized')->nullable();
            $table->unsignedBigInteger('idUser')->nullable();
            $table->foreign('idUser')->references('id')->on('admin_colaboradores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_petitions_economicpetitions');
    }
}
