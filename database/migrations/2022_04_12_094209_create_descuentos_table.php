<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descuentos', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_empresa')->nullable();
            $table->foreign('id_empresa')->references('id')->on('admin_company');
            $table->unsignedBigInteger('id_colab')->nullable();
            $table->foreign('id_colab')->references('id')->on('admin_colaboradores');

            $table->string('tipo');
            $table->float('monto',10,4);
            $table->integer("no_cuotas")->default(0)->nullable();
            $table->float('valor_cuota',10,4)->nullable();
            $table->date('primera_cuota')->nullable();
            $table->integer("cuota_actual")->default(0)->nullable();
            $table->string('fecha_debito')->nullable();
            $table->string('banco')->nullable();
            $table->string('no_credito')->nullable();
            $table->date('fecha_aprobado')->nullable();
            $table->string('nombre')->nullable();
            $table->text("detalle")->nullable();
            $table->float('saldo',10,4)->nullable();
            $table->string('estado')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descuentos');
    }
}
