<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCompanyClientAssocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company_client_assoc', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("idClient")->add();
            $table->foreign('idClient')
            ->references('id')->on('admin_company_clients')
            ->onDelete('cascade');
            $table->unsignedBigInteger("idCompany")->add();
            $table->foreign('idCompany')
            ->references('id')->on('admin_company')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_client_assoc');
    }
}
