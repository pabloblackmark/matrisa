<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminComaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("company");
            $table->string("nit",25);
            $table->string("legalrepresent");
            $table->text("logo")->nullable();
            $table->integer("phone1");
            $table->integer("phone2");
            $table->string("depto");
            $table->string("municip");
            $table->string("addess");
            $table->unsignedBigInteger("idUser")->nullable();
            $table->foreign('idUser')
                  ->references('id')->on('admin_usersadmin')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company');
    }
}
