<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoggsUserPlanillaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::create('loggs_user_planilla', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('id_empresa')->nullable();
          $table->foreign('id_empresa')->references('id')->on('admin_company');
          $table->unsignedBigInteger('id_colab')->nullable();
          $table->foreign('id_colab')->references('id')->on('admin_colaboradores');

          /*
          $table->unsignedBigInteger("id_cliente")->nullable();
          $table->foreign('id_cliente')->references('id')->on('admin_company_clients');
          $table->unsignedBigInteger("id_sucursal")->nullable();
          $table->foreign('id_sucursal')->references('id')->on('admin_company_clients_branchof');
          $table->unsignedBigInteger("id_area")->nullable();
          $table->foreign('id_area')->references('id')->on('admin_company_clients_branch_area');
          */

          $table->date('fecha_inicio')->nullable();
          $table->date('fecha_fin')->nullable();

          $table->integer("dias")->default(0)->nullable();

          $table->string('tipo')->nullable();

          $table->float('sueldo',10,4)->nullable();
          $table->float('total',10,4)->nullable();

          $table->float('bono_ley',10,4)->nullable();
          $table->float('bono_extra',10,4)->nullable();
          $table->float('bono_antiguedad',10,4)->nullable();

          $table->float('desc_dia',10,4)->nullable();
          $table->float('desc_septimo',10,4)->nullable();
          $table->float('desc_igss',10,4)->nullable();

          $table->text('descuentos')->nullable();

          $table->float('total_bonos',10,4)->nullable();
          $table->float('total_descuentos',10,4)->nullable();

          $table->string('estado')->nullable();
          $table->date('fecha_pago')->nullable();

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('loggs_user_planilla');

    }
}
