<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPrestacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_prestaciones', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('id_empresa')->nullable();
            $table->foreign('id_empresa')->references('id')->on('admin_company');
            $table->unsignedBigInteger('id_colab')->nullable();
            $table->foreign('id_colab')->references('id')->on('admin_colaboradores');

            $table->string('tipo');
            $table->date('fecha_pago')->nullable();
            $table->float('total',10,4)->nullable();
            $table->string('estado')->nullable();
            $table->text("detalle")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_prestaciones');
    }
}
