<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_productos', function (Blueprint $table) {
          $table->id();

          $table->string('codigo')->nullable();
          $table->string('nombre')->nullable();
          $table->text('descripcion')->nullable();
          $table->unsignedBigInteger('categoria_id')->nullable();
          $table->unsignedBigInteger('sub_categoria_id')->nullable();
          $table->unsignedBigInteger('medida_final_id')->nullable();
          $table->bigInteger('cantidad_minima_aceptable')->nullable();
          $table->decimal('porcentaje_ganancia', 14, 2)->nullable();
          $table->decimal('precio_cliente', 14, 2)->nullable();

          $table->foreign('categoria_id')->references('id')->on('admin_categories');
          $table->foreign('sub_categoria_id')->references('id')->on('admin_subCategories');
          $table->foreign('medida_final_id')->references('id')->on('admin_unidades');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_productos');
    }
}
