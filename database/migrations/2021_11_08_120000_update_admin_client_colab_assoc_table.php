<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminClientColabAssocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_client_colab_assoc', function (Blueprint $table) {
            $table->unsignedBigInteger('idPuesto')->nullable();
            $table->unsignedBigInteger('idBranch')->nullable();
            $table->unsignedBigInteger('idArea')->nullable();
            $table->string('tipo')->nullable();
            $table->date('inicio')->nullable();
            $table->date('fin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
