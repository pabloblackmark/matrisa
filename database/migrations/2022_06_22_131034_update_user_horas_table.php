<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserHorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_horas', function (Blueprint $table) {
            //
            $table->float('total',10,4)->nullable()->change();

            $table->float('extra_dia',10,4)->nullable()->change();
            $table->float('extra_noche',10,4)->nullable()->change();
            $table->float('extra_mixta',10,4)->nullable()->change();
            $table->float('extra_especial',10,4)->nullable()->change();
            $table->float('extra_metas',10,4)->nullable()->change();

            $table->integer("dia")->default(0)->nullable()->add();
            $table->integer("noche")->default(0)->nullable()->add();
            $table->integer("mixta")->default(0)->nullable()->add();
            $table->integer("especial")->default(0)->nullable()->add();
            $table->integer("metas")->default(0)->nullable()->change();

            $table->float('desc_igss',10,4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_horas', function (Blueprint $table) {
            //
        });
    }
}
