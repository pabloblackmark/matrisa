<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminHorary2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('admin_horary', function (Blueprint $table) {
        $table->string("horas_mixtas")->change();
        $table->string("horas_diurnas")->change();
        $table->string("horas_nocturnas")->change();
        $table->string("tiempo_receso")->default("")->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
