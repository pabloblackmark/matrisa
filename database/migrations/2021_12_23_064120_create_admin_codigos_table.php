<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCodigosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_codigos', function (Blueprint $table) {
            $table->id();
            $table->string("codigo");
            $table->string("id_provider");
            $table->string("id_admin");
            $table->string("id_factura");
            $table->string("no_factura");
            $table->string("valor");
            $table->text("info")->nullable();
            $table->date("fecha_pago")->nullable();
            $table->string('status',1)->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_codigos');
    }
}
