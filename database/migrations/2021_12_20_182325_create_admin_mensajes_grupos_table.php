<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminMensajesGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_mensajes_grupos', function (Blueprint $table) {
            $table->id();
            $table->string("id_admin");
            $table->string("id_empresa")->nullable();
            $table->string("nombre");
            $table->string("ids_miembros");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_mensajes_grupos');
    }
}
