<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminPuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('admin_puestos', function (Blueprint $table) {
        $table->integer("payDT")->add();
        $table->integer("payNT")->add();
        $table->integer("payEDT")->add();
        $table->integer("payENT")->add();
        $table->integer("extraPay")->add();
        $table->integer("perDiem")->add();
        $table->integer("movil")->add();
        $table->integer("points")->add();
        // $table->unsignedBigInteger("idCompany")->add();
        // // $table->foreign('idCompany')
        // // ->references('id')->on('admin_company')
        // // ->onDelete('cascade');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
