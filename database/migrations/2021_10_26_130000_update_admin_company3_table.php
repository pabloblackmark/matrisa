<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminCompany3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_company', function (Blueprint $table) {
            $table->date('fecha_inicio')->nullable()->add();
            $table->string('nacionalidad', 100)->nullable()->add();
            $table->string('vecino', 200)->nullable()->add();
            $table->string('iggs_patronal', 100)->nullable()->add();
            $table->string('irtra', 100)->nullable()->add();
            $table->string('iggs_laboral', 100)->nullable()->add();
            $table->string('intecap', 200)->nullable()->add();
            $table->string('bono_extra', 200)->nullable()->add();
            $table->string('horas_extra', 200)->nullable()->add();
            $table->string('productividad', 200)->nullable()->add();
            $table->string('id_file_manager', 200)->nullable()->add();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
