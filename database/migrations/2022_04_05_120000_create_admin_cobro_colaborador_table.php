<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCobroColaboradorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_cobro_colaborador', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('stock_egresos_id')->nullable();
          $table->unsignedBigInteger('producto_id')->nullable();
          $table->unsignedBigInteger('colaborador_id')->nullable();

          $table->unsignedBigInteger('medida_id')->nullable();
          $table->bigInteger('cantidad')->nullable();
          $table->decimal('precio_total',14,2)->nullable();
          $table->decimal('precio_cuotas',14,2)->nullable();
          $table->integer('cuotas_iniciales')->nullable();
          $table->integer('cuotas_restantes')->nullable();
          $table->string('estado')->default("abierto");

          $table->foreign('stock_egresos_id')->references('id')->on('admin_stock_egresos');
          $table->foreign('producto_id')->references('id')->on('admin_productos');
          $table->foreign('colaborador_id')->references('id')->on('admin_colaboradores');

          $table->foreign('medida_id')->references('id')->on('admin_unidades');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_cobro_colaborador');
    }
}
