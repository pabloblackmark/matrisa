<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminStockIngresosEquipoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_stock_ingresos_equipo', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('equipo_id')->nullable();
          $table->bigInteger('cantidad')->nullable();
          $table->decimal('costo_unidad', 16, 2)->nullable();
          $table->decimal('costo_total', 16, 2)->nullable();

          $table->unsignedBigInteger('bodega_id')->nullable();
          $table->unsignedBigInteger('factura_id')->nullable();

          $table->foreign('equipo_id')->references('id')->on('admin_equipos');
          $table->foreign('bodega_id')->references('id')->on('admin_bodegas');
          $table->foreign('factura_id')->references('id')->on('admin_facturas');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_stock_ingresos_equipo');
    }
}
