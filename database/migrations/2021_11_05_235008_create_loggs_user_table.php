<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoggsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loggs_user', function (Blueprint $table) {
            $table->id();
            $table->string("latitude", 30);
            $table->string("longitud", 30);
            $table->string("latitudeExit", 30)->nullable();
            $table->string("longitudExit", 30)->nullable();
            $table->string("latitudeEnterBtw", 30)->nullable();
            $table->string("longitudEnterBtw", 30)->nullable();
            $table->string("latitudeExitBtw", 30)->nullable();
            $table->string("longitudExitBtw", 30)->nullable();
            $table->string("ipClient", 30)->nullable();
            $table->text("refrence")->nullable();


            $table->string("ontime",5)->nullable();
            $table->string("onRange", 5)->nullable();
            $table->string("statusDay", 45)->nullable();
            $table->time("hourEnter")->default('00:00:00')->nullable();
            $table->time("hourExit")->default('00:00:00')->nullable();
            $table->time("hourEnterBetw")->default('00:00:00')->nullable();
            $table->time("hourExitBetw")->default('00:00:00')->nullable();
            $table->time("befTimeEnter")->default('00:00:00')->nullable();
            $table->string("befTimeEnterTxt",15)->nullable();
            $table->time("outTimeEnter")->default('00:00:00')->nullable();
            $table->string("outTimeEnterTxt",15)->nullable();
            $table->time("hourIdeal")->default('00:00:00')->nullable();
            $table->time("hourDay")->default('00:00:00')->nullable();
            $table->float("payDay",10,4)->nullable();
            $table->time("extraHourDay")->default('00:00:00')->nullable();
            $table->float("extraHourDayMiddle",10,4)->nullable();
            $table->float("extraPayDay",10,4)->nullable();
            $table->time("hourNight")->default('00:00:00')->nullable();
            $table->time("extraHourNight")->default('00:00:00')->nullable();
            $table->float("extraHourNightMiddle",10,4)->nullable();
            $table->float("extraPayNight",10,4)->nullable();
            $table->time("extraHourEspecial")->default('00:00:00')->nullable();
            $table->float("extraPayEspec",10,4)->nullable();
            $table->integer("aproved")->nullable();
            $table->string("dayWeek", 29)->nullable();
            $table->date("dateDay")->nullable();
            $table->string("delay", 45)->nullable();
            $table->string("dayLab", 15)->nullable();
            $table->integer("idFollower", 45);
            $table->float("viatics",10,4)->nullable();
            $table->float("viaticsAdded",10,4)->nullable();

            $table->text("tasks")->nullable();
            $table->integer("quantityTask")->nullable();
            $table->integer("baseTasks")->nullable();
            $table->integer("priceTask")->nullable();
            $table->string("complTask")->nullable();
            $table->integer("aditTask")->nullable();
            $table->integer("lessTask")->nullable();
            $table->integer("priceAdiTask")->nullable();

            $table->unsignedBigInteger("userId")->nullable();
            $table->foreign('userId')
                  ->references('id')->on('admin_colaboradores')
                  ->onDelete('restrict');
            $table->unsignedBigInteger("idArea")->nullable();
            $table->foreign('idArea')
            ->references('id')->on('admin_company_clients_branch_area')
            ->onDelete('restrict');
            $table->unsignedBigInteger("idBranch")->nullable();
            $table->foreign('idBranch')
                  ->references('id')->on('admin_company_clients_branchof')
                  ->onDelete('restrict');
            $table->unsignedBigInteger("idClient")->nullable();
            $table->foreign('idClient')
                  ->references('id')->on('admin_company_clients')
                  ->onDelete('restrict');
            $table->unsignedBigInteger("idLink")->nullable();
            $table->foreign('idLink')
                  ->references('id')->on('admin_client_colab_assoc')
                  ->onDelete('restrict');
            $table->unsignedBigInteger("idDay")->nullable();
            $table->foreign('idDay')
                  ->references('id')->on('admin_horary')
                  ->onDelete('restrict');
            $table->unsignedBigInteger("idCompany")->nullable();
            $table->foreign('idCompany')
                  ->references('id')->on('admin_company')
                  ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loggs_user');
    }
}
