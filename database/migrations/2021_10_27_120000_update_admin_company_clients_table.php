<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminCompanyClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_company_clients', function (Blueprint $table) {
            $table->string('nombre_factura', 200)->nullable()->add();
            $table->text('observaciones')->nullable()->add();
            $table->integer('vma')->nullable()->add();
            $table->boolean('venta_sobregiro')->default(false)->add();
            $table->string('id_file_manager', 200)->nullable()->add();
            $table->string('area', 200)->nullable()->add();
            $table->string('id_area', 200)->nullable()->add();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_clients');
    }
}
