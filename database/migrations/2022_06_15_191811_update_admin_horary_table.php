<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminHoraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('admin_horary', function (Blueprint $table) {
        $table->time("horas_diurnas")->default('00:00')->nullable()->add();
        $table->time("horas_nocturnas")->default('00:00')->nullable()->add();
        $table->string("cobrar_receso",2)->default('no')->nullable()->add();
        $table->time("tiempo_receso",2)->default('01:00')->nullable()->add();
        $table->string("tipo_receso",8)->nullable()->add();
      });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
