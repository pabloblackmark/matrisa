<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCompanyClientsBranchofTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company_clients_branchof', function (Blueprint $table) {
            $table->id();
            $table->string("namebranch");
            $table->string("address");
            $table->string("latitude");
            $table->string("longitude");
            $table->string("phone1");
            $table->unsignedBigInteger("idClient");
            $table->foreign('idClient')
                  ->references('id')->on('admin_company_clients')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_clients_branchof');
    }
}
