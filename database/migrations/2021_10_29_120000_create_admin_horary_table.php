<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminHoraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_horary', function (Blueprint $table) {
            $table->id();

            $table->string("id_horary_manager", 200)->nullable();
            $table->string("day", 200)->nullable();
            $table->time("entry")->nullable();
            $table->time("exit")->nullable();
            $table->string("type", 200)->nullable();
            $table->date("date", 200)->nullable();
            $table->string("description")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_horary');
    }
}
