<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_pagos', function (Blueprint $table) {
            //
            $table->float('total',10,4)->nullable()->change();

            $table->float('total_planilla',10,4)->nullable()->change();
            $table->float('total_horas',10,4)->nullable()->change();
            $table->float('total_viaticos',10,4)->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_pagos', function (Blueprint $table) {
            //
        });
    }
}
