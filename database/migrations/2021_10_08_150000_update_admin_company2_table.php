<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminCompany2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('admin_company', function (Blueprint $table) {
        $table->string("igss")->add();
        $table->string("dpi")->add();
        $table->string("estado_civil")->add();
        $table->string("genero")->add();
        $table->string('calculo_sobre')->add();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
