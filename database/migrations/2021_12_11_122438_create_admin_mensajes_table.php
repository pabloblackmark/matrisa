<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_mensajes', function (Blueprint $table) {
            $table->id();
            $table->string("de_admin")->nullable();
            $table->string("para_user")->nullable();
            $table->string("de_user")->nullable();
            $table->string("para_admin")->nullable();
            $table->string("de");
            $table->string("para");
            $table->string("titulo");
            $table->text('mensaje');
            $table->string('tipo',1)->nullable()->default('0');
            $table->string('status',1)->nullable()->default('0');
            $table->text('respuestas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_mensajes');
    }
}
