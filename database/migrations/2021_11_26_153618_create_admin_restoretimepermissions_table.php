<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminRestoretimepermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_petitions_restoretimepermissions', function (Blueprint $table) {
            $table->id();
            $table->date('dateOut')->nullable();
            $table->date('dateEnter')->nullable();
            $table->date('deteRepEnter')->nullable();
            $table->date('deteRepOut')->nullable();
            $table->text('description')->nullable();
            $table->string('typos')->nullable();
            $table->string('daysapply',6)->nullable();
            $table->string('status',2)->nullable()->default('0');
            $table->text('reponse')->nullable();
            $table->date('responseDate')->nullable();
            $table->unsignedBigInteger('idUser')->nullable();
            $table->foreign('idUser')->references('id')->on('admin_colaboradores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_petitions_restoretimepermissions');
    }
}
