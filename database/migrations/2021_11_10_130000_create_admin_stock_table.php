<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_stock', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('producto_id')->nullable();
          $table->bigInteger('cantidad')->nullable();
          $table->unsignedBigInteger('medida_id')->nullable();
          $table->decimal('costo_unitario',14,2)->nullable();
          $table->decimal('valor_total',14,2)->nullable();
          $table->unsignedBigInteger('bodega_id')->nullable();

          $table->foreign('producto_id')->references('id')->on('admin_productos');
          $table->foreign('medida_id')->references('id')->on('admin_unidades');
          $table->foreign('bodega_id')->references('id')->on('admin_bodegas');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_stock');
    }
}
