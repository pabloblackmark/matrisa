<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLogLiquidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_liquidacion', function (Blueprint $table) {
            //
            $table->float('total',10,4)->nullable()->change();

            $table->float('indemnizacion',10,4)->nullable()->add();
            $table->float('aguinaldo',10,4)->nullable()->change();
            $table->float('bono_14',10,4)->nullable()->change();
            $table->float('vacaciones',10,4)->nullable()->change();
            $table->float('salario',10,4)->nullable()->change();
            $table->float('total_bonos',10,4)->nullable()->add();
            $table->float('total_horas',10,4)->nullable()->add();
            $table->float('total_viaticos',10,4)->nullable()->add();
            $table->float('total_descuentos',10,4)->nullable()->add();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_liquidacion', function (Blueprint $table) {
            //
        });
    }
}
