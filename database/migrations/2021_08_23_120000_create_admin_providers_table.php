<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_providers', function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 100);
            $table->text("direccion");
            $table->string("nit", 20);
            $table->text("contacto");
            $table->string("telefono", 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_providers');
    }
}
