<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCompanyClientsJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company_clients_jobs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("idJobCatalog");
            $table->foreign('idJobCatalog')
                  ->references('id')->on('admin_puestos')
                  ->onDelete('cascade');
            $table->unsignedBigInteger("idClientComp");
            $table->foreign('idClientComp')
                  ->references('id')->on('admin_company_client_assoc')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_clients_jobs');
    }
}
