<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_products', function (Blueprint $table) {
          $table->id();
          $table->string('title', 60);
          $table->string('code', 60);
          $table->string('lot', 60);
          $table->string('cost', 60);
          $table->integer('min_dispatch');
          $table->integer('max_dispatch');
          $table->integer('min_stock');
          $table->string('provider', 100)->nullable();
          $table->string('price', 60);
          $table->date('dateIn');
          $table->text('images')->nullable();
          $table->text('description')->nullable();
          $table->text('sizes')->nullable();
          $table->text('colors')->nullable();
          $table->string('category', 15);
          $table->string('subcategory', 15);
          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_products');
    }
}
