<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminPuestosTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('admin_puestos', function (Blueprint $table) {
          $table->float("payDT", 10, 4)->nullable()->change();
          $table->float("payNT", 10, 4)->nullable()->change();
          $table->float("payEDT", 10, 4)->nullable()->change();
          $table->float("payENT", 10, 4)->nullable()->change();
          $table->float("extraPay", 10, 4)->nullable()->change();
          $table->float("perDiem", 10, 4)->nullable()->change();
          $table->float("movil", 10, 4)->nullable()->change();
          $table->float("points", 10, 4)->nullable()->change();

          $table->float("viaticos_extra", 10, 4)->nullable()->add();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
