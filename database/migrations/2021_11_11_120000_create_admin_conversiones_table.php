<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminConversionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_conversiones', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('unidad_inicial_id')->nullable();
          $table->unsignedBigInteger('unidad_final_id')->nullable();
          $table->string('operacion');
          $table->decimal('valor', 14, 2)->nullable();
          $table->text('descripcion')->nullable();

          $table->foreign('unidad_inicial_id')->references('id')->on('admin_unidades');
          $table->foreign('unidad_final_id')->references('id')->on('admin_unidades');

          $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_conversiones');
    }
}
