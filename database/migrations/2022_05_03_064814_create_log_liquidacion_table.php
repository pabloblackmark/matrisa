<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogLiquidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_liquidacion', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_empresa')->nullable();
            $table->foreign('id_empresa')->references('id')->on('admin_company');
            $table->unsignedBigInteger('id_colab')->nullable();
            $table->foreign('id_colab')->references('id')->on('admin_colaboradores');

            $table->date('fecha_pago')->nullable();
            $table->float('total',10,4)->nullable();

            $table->float('indemnizacion',10,4)->nullable();
            $table->float('aguinaldo',10,4)->nullable();
            $table->float('bono_14',10,4)->nullable();
            $table->float('vacaciones',10,4)->nullable();
            $table->float('salario',10,4)->nullable();
            $table->float('total_bonos',10,4)->nullable();
            $table->float('total_horas',10,4)->nullable();
            $table->float('total_viaticos',10,4)->nullable();
            $table->float('total_descuentos',10,4)->nullable();

            $table->text("bonos")->nullable();
            $table->text("horas")->nullable();
            $table->text("viaticos")->nullable();
            $table->text("descuentos")->nullable();

            $table->string('estado')->nullable();
            $table->text("detalle")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_liquidacion');
    }
}
