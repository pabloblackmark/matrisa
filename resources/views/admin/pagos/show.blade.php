@extends('admin.layouts.master')

@section('main-content')

@php  $creat=array();
  $colors = ["ausencia"=>'#f14848',"sin horario"=>'#a99944',"asistencia"=>'#38b6f5',
            "Permiso"=>'#ecffef',"Vacaciones"=>'#f3fbff',"Iggs"=>'#f3fbff'];
@endphp
<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{ url('pagos') }}?id_empresa={{ $request->id_empresa }}&id_cliente={{ $request->id_cliente }}&fecha_pago={{ $request->fecha_pago }}">Listado de Pagos</a> </h1>
    <ul>
        <li>Detalle de Pago</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Datos del empleado</div>
      </div>
      <div class="card-body">
          <div class="row">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

@php
            $planilla = 0;
            $horas = 0;
            $viaticos = 0;
                    
            if ($log_planilla!='') {
                $planilla = $log_planilla['total'];
            }

            if ($log_horas!='') {
                $horas = $log_horas['total'];
            }

            if ($log_viaticos!='') {
                $viaticos = $log_viaticos['total'];
            }

            $total = $planilla + $horas + $viaticos;


            $planilla = number_format($planilla,2,'.',',');
            $horas = number_format($horas,2,'.',',');
            $viaticos = number_format($viaticos,2,'.',',');

            $total = number_format($total,2,'.',',');

@endphp
            <div class="col-3">
                <label  style="font-weight:bold;">Código:</label>
                {{ $colabInfo->id }}<br>
                <label  style="font-weight:bold;">Nombre: </label>
                {{ $colabInfo->nombres.' '.$colabInfo->apellidos }}<br>
                <label  style="font-weight:bold;">Estado: </label>
                {{ $colabInfo->estatus}}<br>
                <label  style="font-weight:bold;">Puesto: </label>
                {{ $colabInfo->puesto }}<br>                  
                <label  style="font-weight:bold;">Fecha de Pago: </label>
                {{ date("d-m-Y", strtotime($request->fecha_pago)) }}
                <br>
                <label  style="font-weight:bold;">Estado: </label>
                          @if(!empty($log_pago->estado))
                            <span class="badge badge-pill {{($log_pago->estado=='Rechazado'?'badge-danger':'badge-success')}} m-2">
                            {{ $log_pago->estado }}</span>
                          @else
                            No procesado
                          @endif
                <br>
            </div>

            <div class="col-2">

              <h5>Planilla</h5>
              <small>({{ date("d-m-Y", strtotime($log_planilla['fecha_inicio'])) }} a {{ date("d-m-Y", strtotime($log_planilla['fecha_fin'])) }})</small>
              <br><br>

              &nbsp;&nbsp;&nbsp;<label>Salario:</label>
              Q. {{ $log_planilla['sueldo'] }}
              <br>
              <br>
              <strong>Bonos</strong><br>
              &nbsp;&nbsp;&nbsp;<label>Bono de ley:</label>
              Q. {{ $log_planilla['bono_ley'] }}
              <br>
              
              &nbsp;&nbsp;&nbsp;<label>Extra:</label>
              Q. {{ $log_planilla['bono_extra'] }}
              <br>
              &nbsp;&nbsp;&nbsp;<label>Antiguedad:</label>
              Q. {{ $log_planilla['bono_antiguedad'] }}
              <br>
              <br>

              <strong>Descuentos</strong><br>
              &nbsp;&nbsp;&nbsp;<label>Descuentos dia:</label>
              Q. {{ $log_planilla['desc_dia'] }}
              <br>
              &nbsp;&nbsp;&nbsp;<label>Descuento septimo: </label>
              Q. {{ $log_planilla['desc_septimo'] }}
              <br>
              &nbsp;&nbsp;&nbsp;<label>IGSS: </label>
              Q. {{ $log_planilla['desc_igss'] }}

@php

  $array_descuentos = json_decode($log_planilla['descuentos'], true);

@endphp

              @foreach ($array_descuentos as $descuento)
              <br>
              &nbsp;&nbsp;&nbsp;<label style="text-transform: capitalize;">{{ $descuento['tipo'] }}: </label>
              Q. {{ $descuento['pago'] }}


              @endforeach

              <br>
              <br>
              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">TOTAL:</label>
              <strong>Q. {{ $planilla }}</strong>
              <br>


            </div>

            <div class="col-2">

              <h5>Horas Extras</h5>
              <small>({{ date("d-m-Y", strtotime($log_horas['fecha_inicio'])) }} a {{ date("d-m-Y", strtotime($log_horas['fecha_fin'])) }})</small>
              <br><br>

              &nbsp;&nbsp;&nbsp;<label>Diurna:</label>
              Q. {{ $log_horas['extra_dia'] }} ({{ $log_horas['dia'] }} horas extras)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Nocturna:</label>
              Q. {{ $log_horas['extra_noche'] }} ({{ $log_horas['noche'] }} horas extras)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Mixta:</label>
              Q. {{ $log_horas['extra_mixta'] }} ({{ $log_horas['mixta'] }} horas extras)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Especial:</label>
              Q. {{ $log_horas['extra_metas'] }} ({{ $log_horas['metas'] }} horas extras)
              <br>

              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">TOTAL:</label>
              <strong>Q. {{ $horas }}</strong>


            </div>

            <div class="col-2">

              <h5>Viáticos</h5>
              <small>({{ date("d-m-Y", strtotime($log_viaticos['fecha_inicio'])) }} a {{ date("d-m-Y", strtotime($log_viaticos['fecha_fin'])) }})</small>
              <br><br>

              &nbsp;&nbsp;&nbsp;<label>Normal:</label>
              Q. {{ $log_viaticos['viaticos'] }}
              <br>
              &nbsp;&nbsp;&nbsp;<label>Movil:</label>
              Q. {{ $log_viaticos['movil'] }}
              <br>
              &nbsp;&nbsp;&nbsp;<label>Extras:</label>
              Q. {{ $log_viaticos['extras'] }}
              <br>
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">TOTAL:</label>
              <strong>Q. {{ $viaticos }}</strong>              

            </div>


            <div class="col-2">
              <h4  style="font-weight:bold;">Total:
              Q. {{ $total }}
              </h4>
            </div>
 
          </div>
      </div>
  </div>

@if(@$log_pago->estado=='Aceptado')
            <div class="col-md-12">
                <a href="{{ url('pagos/export_pdf')}}?id_pago={{ @$log_pago->id }}" target="_blank">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Descargar PDF
                </button>
                </a>
            </div>
@endif

 </div>
</div>




<script type="text/javascript">
  function calcViatics(des){
    let viatics = {{!empty($colabInfo->viaticos)?$colabInfo->viaticos:1}};
    $(des).parent().find("span").html('Q.'+viatics*des.value);
    // console.log(viatics*des.value);
  }
  function calcViaticsMovil(des){
    let viatics = {{!empty($colabInfo->movil)?$colabInfo->movil:1}};
    $(des).parent().find("span").html('Q.'+viatics*des.value);
    // console.log(viatics*des.value);
  }
  function notifyViatExt(text){
    swal.fire({title:'<small>Agregar comentario viaticos extras</small>',
               html:'<div class="row"><div class="col-12"><textarea id="viaticExText" class="form-control">'+text+'</textarea></div><br>\
               <input type="button"  class="btn btn-primary btn-icon btn-sm m-1" value="Guardar" onclick="addCommentViatExtra()">\
               </div>',
              showCloseButton: false,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function addCommentViatExtra(){
    $('#txtViaticExt').val(  $('#viaticExText').val());
    Swal.close();
  }
  function filter(){
    if($('#week').val()!=''){
      window.location= '{{ url('admin.colabCompanyWork') }}'+'/'+$('#week').val()+'/'+$('#year').val();
    }else{
      alert('Por favor eliga una semana');
    }
  }

  function showDesc(messa){
    swal.fire({title:'Descripción de permiso',
               html:'<div class="row"><div class="col-12">'+messa+'</div></div>',
              showCloseButton: true,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function changColor(estos){
    $(estos).css("color","black");
  }
  function checkTime(t){
    var v = t.value;
    if (v.match(/^\d{2}$/) !== null) {
        t.value = v + ':';
        t.value=t.value;
        return ;
    }
    // else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
    //     t = v + '/';
    //     t.value=t;
    //     return ;
    // }
  }
</script>
<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
h5{
  font-weight: bold;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
hr{

}
</style>
@endsection

@section('page-js')



@endsection

@section('bottom-js')

{{-- <script src="{{asset('assets/js/quill.script.js')}}"></script> --}}
<script>
$(document).ready(function () {
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });

});



  function changeLog(){
      Swal.fire({
          title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                    <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                </div>`,
          html:`
               <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                   <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                       <div class="modal-body">
                           <div class="row">
                               <label class="col-form-label">Comentario</label>
                               <textarea id="changeLogComment" class="form-control"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               `,
           preConfirm: function(){
            $('#bt_actualizar').hide();

               let val = $('#changeLogComment').val(),
                   cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`;
             $('#form_viaticos').append(cad).attr('onsubmit', '').submit();
             return true
         }
      })
  }



</script>


@endsection
