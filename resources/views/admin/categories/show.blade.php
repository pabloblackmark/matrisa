@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Título",nm:"category",elv:"0"}
        };
    ked={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Título",nm:"subCategory",elv:"0"}
        };
    valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Categorías</h1>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <a href="javascript:"class="" onclick="newfloatv2(kad);">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nueva categoría
                </a>
            </h3>

        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="categorias_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Categoría</th>
                  <th>Subcategoría</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>

                @php
                    $data1 = array();
                    $data2 = array();
                @endphp

                @foreach ($data as $ky1=>$dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td> {{$dataRes['name']}}</td>
                  <td></td>
                  <td>

                    <button type="button" name="button"
                        onclick="evaluadores('l{{$ky1}}k');"
                        class="btn btn-sm btn-primary"> <i class="fa fa-eye"></i></button>

                    <button type="button" name="button"
                        onclick="modifyfloat('{{$ky1}}', kad, data1);$('.card-title')[1].innerHTML = 'Editar categoria';"
                        class="btn btn-sm btn-primary" tooltip="nuevo"> <i class="fa fa-edit"></i></button>

                    <button type="button" name="button"
                        onclick="newfloatv2(ked,null,'storeSubCategory/{{$ky1}}');$('.card-title')[1].innerHTML = 'Agregar subcategoria';"
                        class="btn btn-sm btn-primary" tooltip="nuevo"> <i class="fa fa-plus-square"></i></button>

                    <button type="button" name="button"
                        onclick="deleteD('{{$ky1}}','{{ csrf_token() }}');"
                        class="btn btn-sm btn-primary" tooltip="nuevo"> <i class="fa fa-trash"></i></button>

                  </td>
                  </tr>
                  @foreach($dataRes['subcategories'] AS $ky2=>$valus)
                    <tr class="subparone l{{$ky1}}k table-subtitle" style="display:none;">
                      <td></td>
                      <td></td>
                      <td >{{$valus}}</td>
                      <td class="td-actions text-left table-subtitle">
                            <a  href="javascript:"
                              onclick="modifyfloat('{{$ky2}}',ked, data2, 'literalUrl', 'updateSubCategory/{{$ky2}}');">
                              <i class="text-12" data-feather="edit"></i>
                            </a>

                            <a  href="javascript:"
                                onclick="deleteD('{{$ky2}}','{{ csrf_token() }}', 'deleteSubCategory');">
                                <i class="text-12" data-feather="trash"></i>
                            </a>
                      </td>
                    </tr>
                    @php
                        $data2[$ky2] = [$valus, $ky1];
                    @endphp
                  @endforeach

                  @php
                    $data1[$ky1] = [$dataRes['name']];
                  @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var data1 = @json($data1);
      var data2 = @json($data2);
    </script>

@endsection
