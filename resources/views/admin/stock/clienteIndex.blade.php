@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    let cuotas = [['', '- - -']]
    for (let i = 1; i <= 12; i++){
        cuotas.push([i, i])
    }
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"slct",tl:"Producto",id:"producto_id",nm:"producto_id",vl:@json($productos_arr),elv:"0",add:'disabled'},
                2:{type:"nbr",tl:"Cantidad",id:"cantidad",nm:"cantidad",nxt:6, add:'oninput="cambiarCantidad(this)" min="0"'},

                3:{type:"slct",tl:"Cargo a",id:"tipo_egreso",nm:"tipo_egreso",vl:[[null, 'No cargo'],['cliente', 'Cliente'],['colaborador', 'Colaborador']],add:'onchange="selectCargo(this)"'},
                4:{type:"slct",tl:"Cargar a cliente",id:"receptor_cliente_id", nm:"receptor_cliente_id",vl:@json($clientes),nxt:6,add:'disabled omit="T"'},
                5:{type:"slct",tl:"Cargar a colaborador",id:"receptor_colaborador_id", nm:"receptor_colaborador_id",vl:@json($colaboradores),nxt:6,add:'disabled omit="T"'},

                6:{type:"slct",tl:"Cobro a",id:"tipo_cobro",nm:"tipo_cobro",vl:[[null, 'No cobro'],['cliente', 'Cliente'],['colaborador', 'Colaborador']],add:'onchange="selectCobro(this)"'},
                7:{type:"slct",tl:"Cobrar a cliente",id:"cobrar_cliente_id", nm:"cobrar_cliente_id",vl:@json($clientes),nxt:6,add:'disabled omit="T"'},
                8:{type:"slct",tl:"Cobrar a colaborador",id:"cobrar_colaborador_id", nm:"cobrar_colaborador_id",vl:@json($colaboradores),nxt:6,add:'disabled omit="T"'},

                9:{type:"slct",tl:"Cuotas",id:"cuotas",nm:"cuotas",vl:cuotas,add:'omit="T"'},

                10:{type:"nbr",tl:"Costo unitario",id:"costo_unitario",nm:"costo_unitario",nxt:6, add:'readonly'},
                11:{type:"nbr",tl:"Valor total",id:"valor_total",nm:"valor_total",nxt:6, add:'readonly'},

                12:{type:"nbr",tl:"Precio unitario",id:"precio_unitario",nm:"precio_unitario",nxt:6, add:'readonly'},
                13:{type:"nbr",tl:"Precio total",id:"precio_total",nm:"precio_total",nxt:6, add:'readonly'},
                14:{type:"hddn",nm:"medida",elv:"1"},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegas')}}">Inicio</a></li>
        <li><a href="{{url('/bodegasCliente', $company_id)}}">Bodegas clientes</a></li>
        <li>Stock</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Empresa: {{$company->company;}}
                </h3>
                <h3 class="card-title">
                    Cliente: {{$cliente->clientName;}}
                </h3>
                <h3 class="card-title">
                    Bodega / sede: {{$bodega->nombre;}}
                </h3>
                <h3 class="card-title">
                    <a href="{{url('/bodegasCliente', $company_id)}}"><i class="text-20" data-feather="skip-back"></i>Regresar</a>
                </h3>

                <h3 class="card-title">
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="stock_productos_clientes_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Medida</th>
                                <th>Costo</th>
                                <th>Importe inventario</th>
                                <th>Precio cliente</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $productos[$value->producto_id] }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>{{ $value->medida_element->nombre }}</td>
                                    <td>Q. {{ number_format($value->costo_unitario, 2) }}</td>
                                    <td>Q. {{number_format($value->valor_total, 2)}}</td>
                                    <td>Q. {{number_format($value->producto_element->precio_cliente, 2)}}</td>
                                    <td>
                                        {{-- <a href="javascript:" onclick="modifyfloat('{{$value->id}}',kad,criteria,undefined,undefined);$('.card-title')[1].innerHTML = 'Editar producto';" title="Editar">
                                            <i class="text-20"data-feather="edit-3"></i>
                                        </a>
                                        <a href="jsavascript:" onclick="deleteD('{{$value->id}}','{{ csrf_token() }}', 'destroy');" title="Eliminar">
                                            <i class="text-20"data-feather="trash"></i>
                                        </a> --}}

                                        {{-- <a href="javascript:" onclick="modifyfloat('{{$value->producto_id}}',kad,criteria,'literalUrl','{{$bodega_id}}/{{$value->producto_id}}');$('#verifyModalContent_title').html('Egreso de producto');" title="Egreso"> --}}
                                        <a href="javascript:" onclick="modifyfloat('{{$value->producto_id}}',kad,criteria);$('#verifyModalContent_title').html('Egreso de producto');" title="Egreso">
                                            <i class="text-20"data-feather="trending-down"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php
                                $criti[$value->producto_id] = [
                                        $value->producto_id,
                                        $value->medida_id,
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        productos = @json($data),
        cliente = @json($clientes);

    function selectCargo(elm){
      let val = $(elm).val()
      if (val == 'cliente'){
          $('#receptor_cliente_id').attr('disabled', false).val(cliente[0][0])
          $('#receptor_colaborador_id').attr('disabled', true).val('')
          $('#cuotas').attr('disabled', true).val('')
      }else if (val == 'colaborador'){
          $('#receptor_cliente_id').attr('disabled', true).val('')
          $('#receptor_colaborador_id').attr('disabled', false)
          $('#cuotas').attr('disabled', false)
      }else{
          $('#receptor_cliente_id').attr('disabled', true).val('')
          $('#receptor_colaborador_id').attr('disabled', true).val('')
          $('#cuotas').attr('disabled', true).val('')
      }
    }

    function selectCobro(elm){
      let val = $(elm).val()
      if (val == 'cliente'){
          $('#cobrar_cliente_id').attr('disabled', false)
          $('#cobrar_colaborador_id').attr('disabled', true).val('')
          $('#cuotas').attr('disabled', true).val('')
      }else if (val == 'colaborador'){
          $('#cobrar_cliente_id').attr('disabled', true).val('')
          $('#cobrar_colaborador_id').attr('disabled', false)
          $('#cuotas').attr('disabled', false)
      }else{
          $('#cobrar_cliente_id').attr('disabled', true).val('')
          $('#cobrar_colaborador_id').attr('disabled', true).val('')
          $('#cuotas').attr('disabled', true).val('')
      }
    }

    function cambiarCantidad(elm){
        let factor = $(elm).val(),
            idprod = $('#producto_id').val(),
            valor = 0,
            precio_cliente = 0,
            medida = '',
            medida_id = null,
            total = 0;


        for(let i = 0; i < productos.length; i++){
            if (productos[i].producto_id == idprod){
                valor = Number(productos[i].costo_unitario)
                precio_cliente = Number(productos[i].precio_cliente)
                medida = productos[i].medida
                medida_id = productos[i].medida_id
                let max = productos[i].cantidad

                if (factor > max){
                    $(elm).val(max)
                }
            }
        }
        total = Math.round(valor * factor * 100) / 100
        precio_total = Math.round(precio_cliente * factor * 100) / 100
        $('#costo_unitario').val(valor.toFixed(2))
        $('#valor_total').val(total.toFixed(2))

        $('#precio_unitario').val(precio_cliente.toFixed(2))
        $('#precio_total').val(precio_total.toFixed(2))

        $('#medida_title').val(medida)
        $('#medida').val(medida_id)
    }
</script>

@endsection

@section('bottom-js')
    <script>
        $('#stock_productos_clientes_table').DataTable();
    </script>
@endsection
