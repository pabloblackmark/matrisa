@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Producto",id:"producto_title",nm:"producto_title",elv:"0",add:'readonly'},
                2:{type:"hddn",id:"producto_id",nm:"producto_id",elv:"1"},
                3:{type:"nbr",tl:"Cantidad",nm:"cantidad",nxt:6, add:'oninput="calcularTotal(this)" min="0"'},
                4:{type:"txt",tl:"Medida",nm:"medida_select",add:"readonly",elv:2, nxt:6},
                5:{type:"hddn",nm:"medida",elv:3},
                6:{type:"hddn",nm:"medida_id",elv:3},
                7:{type:"slct",tl:"Cliente responsable",nm:"receptor_bodega_id",vl:@json($clientesSedes)},
                8:{type:"formato_moneda",tl:"Precio unidad",id:"precio_unidad",nm:"precio_unidad", nxt:6, elv:4},
                9:{type:"formato_moneda",tl:"Precio total",id:"precio_total",nm:"precio_total", nxt:6},
                10:{type:"hddn",nm:"tipo_egreso",vl:"bodega"},
                11:{type:"hddn",nm:"costo_unitario",elv:5}
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegas')}}">Inicio</a></li>
        <li>Stock</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: {{$bodega->nombre;}}
                </h3>
                <h3 class="card-title">
                    <a href="{{url('/bodegas')}}"><i class="text-20" data-feather="skip-back"></i>Regresar</a>
                </h3>

                <h3 class="card-title">
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="stock_productos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código</th>
                                <th>Producto</th>
                                <th>Stock</th>
                                <th>Medida</th>
                                <th>Costo</th>
                                <th>Importe inventario</th>
                                <th>Precio cliente</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ isset($value->producto_element->codigo) ? $value->producto_element->codigo : '' }}</td>
                                    <td>{{ isset($value->producto_element->nombre) ? $value->producto_element->nombre : '' }}</td>
                                    <td>
                                        @if ($value->cantidad < $value->producto_element->cantidad_minima_aceptable)
                                            <span class="text-danger">{{$value->cantidad}}</span><br>
                                            <span class="badge badge-danger">Cantidad menor a la mínima aceptable</span>
                                        @else
                                            {{$value->cantidad}}
                                        @endif
                                    </td>
                                    <td>{{ isset($value->medida_element->nombre) ? $value->medida_element->nombre : '' }}</td>
                                    <td>Q.{{ number_format($value->costo_unitario, 2) }}</td>
                                    <td>Q.{{ number_format($value->valor_total, 2) }}</td>
                                    <td>
                                        @php $ganancia_minima = ($value->producto_element->porcentaje_ganancia * $value->costo_unitario / 100) + $value->costo_unitario; @endphp
                                        @if ($ganancia_minima > $value->producto_element->precio_cliente)
                                            Q.{{ number_format($value->producto_element->precio_cliente, 2) }}
                                            <br>
                                            <span class="badge badge-warning">Precio por debajo del valor mínimo de ganancia configurado</span>
                                            <br>
                                            <span class="badge badge-warning">Precio recomendado: Q.{{Helper::redondear_moneda($ganancia_minima)}}</span>
                                        @else
                                            Q.{{ number_format($value->producto_element->precio_cliente, 2) }}
                                        @endif
                                    </td>
                                    <td>
                                        {{-- <a href="javascript:" onclick="modifyfloat('{{$value->producto_element->id}}',kad2,criteria2,'literalUrl','/stockEmpresasEquipoIngreso/{{$bodega_id}}/{{$value->id}}');$('#verifyModalContent_title').html('Egreso de producto');" title="Egreso de producto"> --}}
                                        <a href="javascript:" onclick="modifyfloat('{{$value->producto_element->id}}',kad,criteria);$('#verifyModalContent_title').html('Egreso de producto');" title="Egreso de producto">
                                            <i class="text-20"data-feather="trending-down"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php
                                $criti[$value->producto_element->id] = [
                                        $value->producto_element->nombre,
                                        $value->producto_element->id,
                                        $value->medida_element->nombre,
                                        $value->producto_element->medida_final_id,
                                        $value->producto_element->precio_cliente,
                                        $value->costo_unitario,
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        productos = @json($data);

    function calcularTotal(elm){
        let cantidad = elm.value,
            costo = $('#precio_unidad').val(),
            producto_id = $('#producto_id').val(),
            producto_actual = null,
            total = 0;

        for (let i = 0; i < productos.length; i++){
            producto_actual = productos[i]
            break;
        }
        max = producto_actual.cantidad

        if (cantidad > max){
            cantidad = max
            elm.value = cantidad
        }
        total = redondear_moneda(cantidad * costo)
        $('#precio_total').val(total.toFixed(2))
    }
</script>

@endsection

@section('bottom-js')
    <script>
        $('#stock_productos_table').DataTable();
    </script>
@endsection
