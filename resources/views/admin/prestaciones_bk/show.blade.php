@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Calculo de Prestaciones</h1>
</div>
<div class="separator-breadcrumb border-top"></div>

<form id="cumples_form" method="POST" action="{{ url('cumples_update') }}" enctype="multipart/form-data" >

    @csrf
    @if(@$edit)
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-md-12">


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Prestaciones</h4>
                </div>
                <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

                    <div class="row">

                    <div class="col-3">
                        <label  style="font-weight:bold;">Código:</label>
                        {{ $colab->id }}<br>
                        <label  style="font-weight:bold;">Nombre: </label>
                        {{ $colab->nombres.' '.$colab->apellidos }}<br>
                        <label  style="font-weight:bold;">Estado: </label>
                        {{ $colab->estatus}}<br>
                        <label  style="font-weight:bold;">Puesto: </label>
                        {{ $colab->puesto }}<br>

                        <label><strong>Fecha de Inicio:</strong></label> {{ $data['inicio'] }}<br>
                        <label><strong>Fecha de Baja:</strong></label> {{ $data['fin'] }}<br>
                        <label><strong>Sueldo:</strong><label> <strong style="color:green"> Q {{ $data['sueldo'] }}</strong><br>

                    </div>

                    <div class="col-md-3 form-group mb-3">
                             Tiempo laborado:<br>

                             Días {{ $data['dias'] }}<br>
                             Meses {{ $data['meses'] }}<br>
                             Años {{ $data['anos'] }}<br>
                             <br>
                             Año Actual:<br>
                            Días {{ $data['dias_actual'] }}<br>
                            <br>
                             Mes Actual:<br>
                            Días {{ $data['dias_mes'] }}<br>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-3 form-group mb-3">
                            <h3>Aguinaldo</h3>
                            Días {{ $data['dias_aguinaldo'] }}<br>
                            PAGO <strong style="color:green">Q {{ $data['aguinaldo'] }}</strong>
                        </div>

                        <div class="col-md-3 form-group mb-3">
                            <h3>Bono 14</h3>
                            Días {{ $data['dias_bono_14'] }}<br>
                            PAGO <strong style="color:green">Q {{ $data['bono_14'] }}</strong>
                        </div>

                        <div class="col-md-3 form-group mb-3">
                            <h3>Vacaciones</h3>
                            PAGO <strong style="color:green">Q {{ $data['vacas'] }}</strong>
                        </div>


                        <div class="col-md-3 form-group mb-3">
                            <h3>TOTAL A PAGAR</h3>
                            <h3 style="color:green">Q {{ $data['total'] }}</h3>
                        </div>
                    </div>    
 

                </div>
            </div>


        </div>

    </div>

</form>


@endsection
