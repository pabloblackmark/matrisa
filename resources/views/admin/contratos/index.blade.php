@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Nombre",nm:"name",elv:"0"},
                2:{type:"txt",tl:"Descripción",nm:"description",elv:"1"},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Contratos</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    <a href="javascript:"class="" onclick="newfloatv2(kad);">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nuevo contrato
                    </a>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="contratos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td>
                                        <a href="javascript:" onclick="modifyfloat('{{$value->id}}', kad, criteria);">
                                            <i class="text-20"data-feather="edit-3"></i>
                                        </a>
                                        <a href="javascript:" onclick="deleteD('{{$value->id}}', '{{ csrf_token() }}');">
                                            <i class="text-12" data-feather="trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php $criti[$value->id] = [$value->name, $value->description]; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var criteria = @json($criti);
</script>

@endsection


@section('bottom-js')
    <script>
        $('#contratos_table').DataTable();
    </script>
@endsection
