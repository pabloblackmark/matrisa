@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
        1:{type:"txt",tl:"Nombre del puesto",nm:"nombre",elv:"0"},
        2:{type:"txt",tl:"Descripción",nm:"descripcion",elv:"1"},
        3:{type:"nbr",tl:"Hora Laboral Diurna Q.",nm:"payDT",elv:"2",nxt:3,vl:'0'},
        4:{type:"nbr",tl:"Hora Laboral Nocturna Q.",nm:"payNT",elv:"3",nxt:3,vl:'0'},
        5:{type:"nbr",tl:"Hora Extra Diurna Q.",nm:"payEDT",elv:"4",nxt:3,vl:'0'},
        6:{type:"nbr",tl:"Hora Extra Nocturna Q.",nm:"payENT",elv:"5",nxt:3,vl:'0'},
        7:{type:"nbr",tl:"Hora Extra Mixta Q.",nm:"extraPay",elv:"6",nxt:3,vl:'0'},
        8:{type:"nbr",tl:"Metas Q.",nm:"points",elv:"7",nxt:3,vl:'0'},
        9:{type:"nbr",tl:"Viáticos Q.",nm:"perDiem",elv:"8",nxt:3,vl:'0'},
        10:{type:"nbr",tl:"Viáticos Movil Q.",nm:"movil",elv:"9",nxt:3,vl:'0'},
        11:{type:"nbr",tl:"Viáticos Extra %",nm:"viaticos_extra",elv:"12",nxt:3,vl:'0'},
    },
    valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Puestos</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">

        <div class="card-header">
            <h3 class="card-title">
                <a href="javascript:"class="" onclick="newfloatv2(kad);">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nuevo puesto
                </a>
            </h3>

            {{-- <h3 class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{route("admin.upcsv.store", ["typo" => "puestos"])}}" enctype="multipart/form-data"  aria-label="{{ __('Upload') }}">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="archivo" name="archivo"/>
                                        <label class="custom-file-label" for="archivo" style="font-size: 16px;" aria-describedby="{{ __('Upload') }}">Seleccionar archivo csv</label>
                                    </div>
                                </div>

                                <button class="btn btn-primary">Subir archivo</button>
                            </form>
                        </div>
                    </div>
                </div>
            </h3> --}}

		</div>

        <div class="card-body">
          <div class="table-responsive">
            <table id="puestos_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Puesto</th>
                  <th>Descripción</th>
                  <th>Hora Laboral Diurna</th>
                  <th>Hora Laboral Nocturna</th>
                  <th>Hora Extra Diurna</th>
                  <th>Hora Extra Nocturna</th>
                  <th>Hora Extra Mixta</th>
                  <th>Metas</th>
                  <th>Viáticos</th>
                  <th>Viáticos Móvil</th>
                  <th>Viáticos Extra</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti=array(); @endphp
                @foreach ($data as $ky1=>$dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td> {{$dataRes['nombre']}}</td>
                  <td> {{$dataRes['descripcion']}}</td>
                  <td> Q {{$dataRes['payDT']}}</td>
                  <td> Q {{$dataRes['payNT']}}</td>
                  <td> Q {{$dataRes['payEDT']}}</td>
                  <td> Q {{$dataRes['payENT']}}</td>
                  <td> Q {{$dataRes['extraPay']}}</td>
                  <td> Q {{$dataRes['points']}}</td>
                  <td> Q {{$dataRes['perDiem']}}</td>
                  <td> Q {{$dataRes['movil']}}</td>
                  <td> {{ $dataRes['viaticos_extra']}} %</td>
                  <td >

                    <a href="javascript:" onclick="modifyfloat('{{$ky1}}',kad,criteria,undefined,undefined,true);$('.card-title')[1].innerHTML = 'Editar catalogo';" title="Editar">
                        <i class="text-20"data-feather="edit-3"></i>
                    </a>

                    <a href="{{url('changeLog', ['puestos', $ky1])}}" title="Registro de cambios">
                        <i class="text-20"data-feather="clock"></i>
                    </a>

                    <a href="{{url('horary', ['id' => $dataRes['id_horary_manager']])}}" title="Horarios">
                        <i class="text-20"data-feather="watch"></i>
                    </a>

                    <a href="{{url('tasks', ['id' => $dataRes['id_task_manager']])}}" title="Metas">
                        <i class="text-20"data-feather="award"></i>
                    </a>

                    <a href="javascript:" onclick="deleteD('{{$ky1}}','{{ csrf_token() }}', 'destroy');" title="Eliminar">
                        <i class="text-12" data-feather="trash"></i>
                    </a>

                  </td>
                  </tr>
                    @php $criti[$ky1] = array_values($dataRes); @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
    </script>

@endsection

@section('bottom-js')
    <script>
        $('#puestos_table').DataTable();
    </script>
@endsection
