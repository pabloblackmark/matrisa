@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Listado de Horas Extras</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">
            <div class="card-body">

              <form id="form_filtro" action="" method="get">
                @csrf

              <div class="row">
                  <div class="col-md-3">
                    Empresa:<br>
                      <select class="form-control" id="id_empresa" name="id_empresa" required>
                        <option value="" selected disabled>-- seleccionar empresa --</option>
                        @foreach ($companies as $k => $v)
                          <option value="{{$v[0]}}" {{(isset($request->id_empresa)&&$v[0]==$request->id_empresa?'selected':'')}}>{{$v[1]}}</option>
                        @endforeach
                      </select>
                  </div>


                  <div class="col-md-3">
                    Cliente:<br>
                      <select class="form-control" id="id_cliente" name="id_cliente">
                        {!! $clientes !!}
                      </select>
                  </div>

                  <div class="col-md-3">
                    Sucursal:<br>
                      <select class="form-control" id="id_sucursal" name="id_sucursal">
                          {!! $sucursales !!}
                      </select>
                  </div>

                  <div class="col-md-3">
                    Area:<br>
                      <select class="form-control" id="id_area" name="id_area">
                          {!! $areas !!}
                      </select>
                  </div>



              </div>
              <br>
              <div class="row">
    <div class="col-lg-3">
      Fecha Inicio
      <input type="date" name="inicio" id="inicio" class="w-100 form-control" value="{{ @$request->inicio }}" required>
    </div>
    <div class="col-lg-3">
      Fecha Fin
      <input type="date" name="fin" id="fin" class="w-100 form-control" value="{{ @$request->fin }}" required>
    </div>
    <div class="col-lg-3">
        <button type="submit" form="form_filtro" value="filtrar" class="btn btn-primary text-white btn-rounded" style="margin-top: 20px;">Filtrar</button>
    </div>

              </div>

              </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
    		  <h3 class="card-title  mb-0">
            Colaboradores en esta empresa
          </h3>
    		</div>
        <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


          <form id="form_pago" action="{{ url('aprobar_horas') }}" method="post" onsubmit="return validate_data()">
          @csrf

          <div class="table-responsive">
@php
    if ($request->id_empresa!='') {

                    // por los dias que hay en el filtro, todo el mes 30 dias

                    $inicio = date("Y-m-d", strtotime($request->inicio));
                    $fin = date("Y-m-d", strtotime($request->fin));

                    $fecha_inicio = date_create($inicio);;
                    $fecha_fin = date_create($fin);

                    $diff_dias = date_diff($fecha_inicio, $fecha_fin);

                    $dias_laborados = $diff_dias->format('%d')+1;



        $dia_inicio = date_format($fecha_inicio, 'd');
        $dia_fin = date_format($fecha_fin, 'd');
        $mes_fecha = date_format($fecha_fin, 'm');


        if ($dia_inicio==1&&$dia_fin==31) {
            $dias_laborados = 30;
        } elseif ($dia_inicio==1&&$dia_fin==28&&$mes_fecha==2) {
            $dias_laborados = 30;
        } elseif ($dia_inicio==1&&$dia_fin==29&&$mes_fecha==2) {
            $dias_laborados = 30;
        } elseif ($dia_inicio==16&&$dia_fin==31) {
            $dias_laborados = 15;
        } elseif ($dia_inicio==16&&$dia_fin==28&&$mes_fecha==2) {
            $dias_laborados = 15;
        } elseif ($dia_inicio==16&&$dia_fin==29&&$mes_fecha==2) {
            $dias_laborados = 15;
        }



                    echo 'Días laborados: <strong>'.$dias_laborados.'</strong><br><br><br>';
    }

@endphp
            <table id="asignar_colaboradores_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>&nbsp;</th>
                  <th>Código</th>
                  <th>Colaborador</th>
<!--                  
                  <th>Viaticos</th>
-->               
                  <th>Horas Extras</th>   
                  <th>Descuentos</th>
                  <th>Total</th>
                  <th>Detalle</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($colabs))
                  @foreach ($colabs as $value)
                    @php

                    $colabInfo = $value->getColabsInfo()->first();

                    // aplicar filtros
                    $status = $model::select('id','estado')
                                                ->where("fecha_inicio",$request->inicio)
                                                ->where("fecha_fin",$request->fin)
                                                ->where("id_colab",$value->idColab)
                                                ->where('id_empresa',$request->id_empresa)
                                                // ->where('id_cliente',$request->id_cliente)
                                                // ->where('id_sucursal',$request->id_sucursal)
                                                // ->where('id_area',$request->id_area)
                                                ->first();                             

                    $request->id_colab = $value->idColab;


                    // filtro por fechas de inicio y fin
                    $from = date('Y-m-d',strtotime($request->inicio));
                    $to = date('Y-m-d',strtotime($request->fin));


                    // trae horas extras
                    /*
                    $extra_dia = $value->getLoggs()
                                ->whereBetween("dateDay", [$from, $to])
                                ->where('idCompany',$request->id_empresa)
                                // ->where('idClient',$request->id_cliente)
                                // ->where('idBranch',$request->id_sucursal)
                                // ->where('idArea',$request->id_area)                                
                                ->sum("extraPayDay");

                    $extra_noche = $value->getLoggs()
                                  ->whereBetween("dateDay", [$from, $to])
                                  ->where('idCompany',$request->id_empresa)
                                  // ->where('idClient',$request->id_cliente)
                                  // ->where('idBranch',$request->id_sucursal)
                                  // ->where('idArea',$request->id_area)
                                  ->sum("extraPayNight");

                    $extra_especial = $value->getLoggs()
                                  ->whereBetween("dateDay", [$from, $to])
                                  ->where('idCompany',$request->id_empresa)
                                  // ->where('idClient',$request->id_cliente)
                                  // ->where('idBranch',$request->id_sucursal)
                                  // ->where('idArea',$request->id_area)
                                  ->sum("extraPayEspec");

                    $extra_mixta = $value->getLoggs()
                                ->whereBetween("dateDay", [$from, $to])
                                ->where('idCompany',$request->id_empresa)
                                // ->where('idClient',$request->id_cliente)
                                // ->where('idBranch',$request->id_sucursal)
                                // ->where('idArea',$request->id_area)
                                ->sum("extraPayMix");

                    $extra_metas = $value->getLoggs()
                                ->whereBetween("dateDay", [$from, $to])
                                ->where('idCompany',$request->id_empresa)
                                // ->where('idClient',$request->id_cliente)
                                // ->where('idBranch',$request->id_sucursal)
                                // ->where('idArea',$request->id_area)
                                ->sum("priceAdiTask");                                        

                    $conteo_metas = $value->getLoggs()
                          ->whereBetween('dateDay', [$from, $to])
                          ->where('idCompany',$request->id_empresa)
                          // ->where('idClient',$request->id_cliente)
                          // ->where('idBranch',$request->id_sucursal)
                          // ->where('idArea',$request->id_area)
                          ->sum("aditTask");

                    */


                    $horas_extras = App\Http\Controllers\Admin\accounting\horasController::get_horas($request);


                    $extra_dia = $horas_extras['extra_dia'];
                    $extra_noche = $horas_extras['extra_noche'];
                    $extra_mixta = $horas_extras['extra_mixta'];
                    $extra_metas = $horas_extras['extra_metas'];

                    $dia = $horas_extras['dia'];
                    $noche = $horas_extras['noche'];
                    $mixta = $horas_extras['mixta'];
                    $metas = $horas_extras['metas'];

                    $total_horas = $extra_dia + $extra_noche + $extra_mixta + $extra_metas;


                    // calculo igss
                    if ($config_empresa->calculo_sobre=='sueldo base') {

                        $igss = 0;

                    } elseif ($config_empresa->calculo_sobre=='sueldo total') {

                        if ($config_empresa->bono_extra==1) {
                            $total_calcular = $total_horas;
                        } else {
                            $total_calcular = 0;
                        }

                        $igss = $total_calcular * ($config_empresa->iggs_laboral/100);
                    }


                    $total = $total_horas - $igss;


                    if ($dias_laborados==7) {
                        $tipo = 'semanal';
                    } elseif ($dias_laborados==15) {
                        $tipo = 'quincenal';
                    } elseif ($dias_laborados==30) {
                        $tipo = 'mensual';
                    } else {
                        $tipo = 'días';
                    }


                    $array_horas = array(
                                          'id_empresa'=>$request->id_empresa,
                                          'id_colab'=>$value->idColab,
                                          'id_cliente'=>$request->id_cliente,
                                          'id_sucursal'=>$request->id_sucursal,
                                          'id_area'=>$request->id_area,
                                          'fecha_inicio'=>$request->inicio,
                                          'fecha_fin'=>$request->fin,
                                          'tipo'=>$tipo,
                                          'total'=> $total,
                                          'extra_dia'=>$extra_dia,
                                          'extra_noche'=>$extra_noche,
                                          'extra_mixta'=>$extra_mixta,
                                          'extra_metas'=>$extra_metas,
                                          'desc_igss'=>$igss,
                                          'dia'=>$dia,
                                          'noche'=>$noche,
                                          'mixta'=>$mixta,
                                          'metas'=>$metas
                    );


                    $json_horas = json_encode($array_horas);

                    $extra_dia = number_format($extra_dia, 2, '.', ',');
                    $extra_noche = number_format($extra_noche, 2, '.', ',');
                    $extra_mixta = number_format($extra_mixta, 2, '.', ',');
                    $extra_metas = number_format($extra_metas, 2, '.', ',');

                    $igss = number_format($igss, 2, '.', ',');  
                    $total = number_format($total, 2, '.', ',');

                    @endphp

                      <tr>
                        <td >
                          @if(empty($status->estado))

                          <input type="checkbox" id="{{ $value->idColab }}" name="pago[]" value="{{ $json_horas }}">

                          @endif

                        </td>
                        <td >{{ $value->idColab }}</td>
                        <td >{{ $colabInfo->nombres }} {{ $colabInfo->apellidos }}
                          <br><small><i>{{ $colabInfo->puesto }}</i></small></td>

                        <td>
                            <small>Diurna</small>: Q.{{ $extra_dia }} ({{ $dia }} extras)<br>
                            <hr class="mb-0 mt-1">
                            <small>Nocturna</small>: Q.{{ $extra_noche }} ({{ $noche }} extras)<br>
                            <hr class="mb-0 mt-1">
                            <small>Mixta</small>: Q.{{ $extra_mixta }} ({{ $mixta }} extras)<br>
                            <hr class="mb-0 mt-1">
                            <small>Especial</small>: Q.{{ $extra_metas }} ({{ $metas }} metas)
                        </td>

                        <td>
                            <small>IGSS</small>: Q.{{ $igss }} <br>
                        </td>
                        <td>Q.{{ $total }}
                      </td>
                        <td class="td-actions  text-left" style="font-size: 20px;">
                          <a  href="{{ url('horas_colab') }}?id_empresa={{ $request->id_empresa }}&id_colab={{$value->idColab}}&id_cliente={{ $request->id_cliente }}&id_sucursal={{ $request->id_sucursal }}&id_area={{ $request->id_area }}&inicio={{ $request->inicio }}&fin={{ $request->fin }}" title="Ver detalle" target="_blank">
                              <i data-feather="eye"></i>
                          </a>
                        </td>
                        <td>
                          @if(!empty($status->estado))
                            <span class="badge badge-pill {{($status->estado=='Denegado'?'badge-danger':'badge-success')}} m-2">
                            {{ $status->estado }}</span> <br>
                            {{ $status->nota }}
                          </td>
                          @else
                            No procesado
                          @endif
                      </tr>

                  @endforeach
                @endif
					    </tbody>
           </table>


            

          </div>


@if ($request->id_empresa!='')

          <input type="hidden" id="id_empresa" name="id_empresa" value="{{ $request->id_empresa }}">
          <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $request->id_cliente }}">
          <input type="hidden" id="id_sucursal" name="id_sucursal" value="{{ $request->id_sucursal }}">
          <input type="hidden" id="id_area" name="id_area" value="{{ $request->id_area }}">
          <input type="hidden" id="inicio" name="inicio" value="{{ $request->inicio }}">
          <input type="hidden" id="fin" name="fin" value="{{ $request->fin }}">

          <div class="text-left">
              <input type="checkbox" id="pago_todos" name="pago_todos" value="todos"> SELECCIONAR TODOS
          </div>
          <div class="text-center">
              <div style="width: 200px; margin-left: auto; margin-right: auto;">
                  Fecha de Pago
                  <input type="date" name="fecha_pago" id="fecha_pago" class="w-100 form-control" value="" required>
              </div>
              <br>
              <button type="submit" form="form_pago" value="pago" class="btn btn-primary text-white btn-rounded">Procesar PAGO</button>
          </div>

@endif
        </form>

        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">


$(function() {

    $('#id_empresa').on('change', function() {


        $('#id_cliente').html('');
        $('#id_sucursal').html('');

        id_empresa = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_clientes')}}',
           data:{
             id_empresa: id_empresa,
           },
           success:function(data) {

                $('#id_cliente').html(data);     
           }
        });

    });


    $('#id_cliente').on('change', function() {


        $('#id_sucursal').html('');

        id_cliente = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_sucursales')}}',
           data:{
             id_cliente: id_cliente,
           },
           success:function(data) {

                $('#id_sucursal').html(data);
           }
        });

    });



    $('#id_sucursal').on('change', function() {


        $('#id_area').html('');

        id_sucursal = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_areas')}}',
           data:{
             id_sucursal: id_sucursal,
           },
           success:function(data) {

                $('#id_area').html(data);
           }
        });

    });


    $("#pago_todos").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });


});


    function validate_data() {

        var selected = [];

         $('#asignar_colaboradores_table input:checked').each(function() {
             selected.push($(this).attr('value'));
         });


        if(selected.length == 0){
            alert('Debes Selecciona al menos un Colaborador para continuar.');
            return false;
        } else {
            return true;
        }

    }


</script>
<style media="screen">
  small{
    font-weight:bold;
  }
</style>
@endsection

@section('bottom-js')
    <script>
        $('#asignar_colaboradores_table').DataTable();
    </script>
@endsection
