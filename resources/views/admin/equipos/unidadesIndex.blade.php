@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
        1:{type:"txt",tl:"Nombre",nm:"nombre",elv:"0"},
        2:{type:"slct",tl:"Tipo",nm:"tipo",elv:"1",vl:@json($tipos), add:'style="text-transform:capitalize;"'},
        3:{type:"txtarea",tl:"Descripción",nm:"descripcion",elv:"2"},
    },
    valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Unidades</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    <a href="javascript:" onclick="newfloatv2(kad)">
                        <i class="ion-ios7-plus-outline "></i>
                        &nbsp;&nbsp;Nueva unidad
                    </a>
                </h3>

			</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table i class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre de la unidad</th>
                                <th>Tipo</th>
                                <th>Descripción</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $criti=array(); @endphp
                            @foreach ($data as $dataRes)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td > {{$dataRes->nombre}}</td>
                                    <td > {{$dataRes->tipo}}</td>
                                    <td > {{$dataRes->descripcion}}</td>

                                    <td class="td-actions  text-left">
                                      <a href="javascript:" onclick="modifyfloat('{{$dataRes->id}}',kad,criteria)">
                                        <i class="text-20"data-feather="edit-3"></i>
                                      </a>
                                      <a href="javascript:"
                                      onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                                      <i class="text-12" data-feather="trash"></i></a>
                                    </td>
                                    {{-- <td > <a href="{{route("admin.products.edit",$dataRes->id)}}"class="btn btn-sm btn-primary">
                                      <i class="fa fa-eye"></i></a> </td> --}}
                                </tr>
                                @php $criti[$dataRes->id] = [
                                        $dataRes->nombre,
                                        $dataRes->tipo,
                                        $dataRes->descripcion
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var criteria = @json($criti);
</script>

@endsection
