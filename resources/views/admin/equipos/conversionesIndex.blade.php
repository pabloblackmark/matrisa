@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
        1:{type:"slct",tl:"Unidad inicial",id:"unidad_inicial",nm:"unidad_inicial_id",vl:@json($unidades_arr),elv:"0", add:'onchange="changeConversion()"'},
        2:{type:"slct",tl:"Unidad final",id:"unidad_final",nm:"unidad_final_id",vl:@json($unidades_arr),elv:"1", add:'onchange="changeConversion()"'},
        3:{type:"slct",tl:"Operación",id:"operacion",nm:"operacion",elv:"2",vl:@json($operaciones), add:'style="text-transform:capitalize;" onchange="changeConversion()"'},
        4:{type:"nbr",tl:"Valor",id:"valor",nm:"valor",elv:"3", add:'oninput="changeConversion()"'},
        5:{type:"txtarea",tl:"Descripción",id:"descripcion",nm:"descripcion",elv:"4"},
    },
    valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Unidades</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    <a href="javascript:" onclick="newfloatv2(kad)">
                        <i class="ion-ios7-plus-outline "></i>
                        &nbsp;&nbsp;Nueva conversion
                    </a>
                </h3>

			</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table i class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Unidad inicial</th>
                                <th>Unidad final</th>
                                <th>Operación</th>
                                <th>Valor</th>
                                <th>Descripción</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $criti=array(); @endphp
                            @foreach ($data as $dataRes)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $dataRes->unidad_inicial->nombre }}</td>
                                    <td>{{ $dataRes->unidad_final->nombre }}</td>
                                    <td style="text-transform:capitalize">{{ $dataRes->operacion }}</td>
                                    <td>{{ $dataRes->valor }}</td>
                                    <td>{{ $dataRes->descripcion }}</td>

                                    <td class="td-actions  text-left">
                                      <a href="javascript:" onclick="modifyfloat('{{$dataRes->id}}',kad,criteria)">
                                        <i class="text-20"data-feather="edit-3"></i>
                                      </a>
                                      <a href="javascript:"
                                      onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                                      <i class="text-12" data-feather="trash"></i></a>
                                    </td>
                                    {{-- <td > <a href="{{route("admin.products.edit",$dataRes->id)}}"class="btn btn-sm btn-primary">
                                      <i class="fa fa-eye"></i></a> </td> --}}
                                </tr>
                                @php $criti[$dataRes->id] = [
                                        $dataRes->unidad_inicial_id,
                                        $dataRes->unidad_final_id,
                                        $dataRes->operacion,
                                        $dataRes->valor,
                                        $dataRes->descripcion,
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        unidades = @json($unidades_obj);

    function changeConversion(){
        let ui = $('#unidad_inicial').val(),
            uf = $('#unidad_final').val(),
            op = $('#operacion').val(),
            va = $('#valor').val(),
            ui_nom = unidades[ui] != undefined ? unidades[ui].nombre : '',
            uf_nom = unidades[uf] != undefined ? unidades[uf].nombre : '';

        console.log(unidades, ui, uf, op, va)
        let cad = '', val = 0
        if (op == 'multiplicar'){
            cad = `Un(a) ${uf_nom} tiene ${va} ${ui_nom}`
        }else{
            cad = `Un(a) ${ui_nom} tiene ${va} ${uf_nom}`
        }

        $('#descripcion').html(cad)
    }
</script>

@endsection
