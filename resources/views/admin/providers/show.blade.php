@extends('admin.layouts.master')
@section('main-content')
@php $criti = [];@endphp
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
      1:{type:"txt",tl:"Nombre",nm:"nombre",elv:"0"},
      2:{type:"txt",tl:"Dirección",nm:"direccion",elv:"1"},
      3:{type:"txt",tl:"Nit",nm:"nit",nxt:6,elv:"2"},
      4:{type:"txt",tl:"Teléfono",nm:"telefono",nxt:6,elv:"3"},
      5:{type:"txt",tl:"Email",nm:"email",nxt:6,elv:"4"},
      6:{type:"txtarea",tl:"Contacto",nm:"contacto",elv:"5"},

  };
    // valdis={clase:"red",text:1};
</script>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <a href="javascript:"class="" onclick="newfloatv2(kad);">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nuevo proveedor
                </a>
            </h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="proveedores_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Dirección</th>
                  <th>Nit</th>
                  <th>Teléfono</th>
                  <th>Email</th>
                  <th>Contacto</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($data as $value)
                  @php $perms=array(); @endphp
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td >{{$value->nombre}}</td>
                    <td >{{$value->direccion}}</td>
                    <td >{{$value->nit}}</td>
                    <td >{{$value->telefono}}</td>
                    <td >{{$value->email}}</td>
                    <td >{{$value->contacto}}</td>
                    <td class="td-actions  text-left">
                      <a  href="javascript:"
                          onclick="modifyfloat('{{$value->id}}',kad,criteria,undefined,undefined,true);">
                          <i class="text-20"data-feather="edit-3"></i>
                      </a>

                      <a href="{{url('changeLog', ['providers', $value->id])}}" title="Registro de cambios">
                          <i class="text-20"data-feather="clock"></i>
                      </a>

                      <a  href="javascript:"
                          onclick="deleteD('{{$value->id}}','{{ csrf_token() }}');">
                          <i class="text-12" data-feather="trash"></i>
                      </a>
                    </td>
                  </tr>
                  @php
                  $criti[$value->id]=array($value->nombre, $value->direccion, $value->nit, $value->telefono, $value->email, $value->contacto);
                  @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
    </script>

@endsection

@section('bottom-js')
    <script>
        $('#proveedores_table').DataTable();
    </script>
@endsection
