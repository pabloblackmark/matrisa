@extends('admin.layouts.master')
@section('main-content')
@php $criti = [];@endphp
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
      1:{type:"txt",tl:"Nombre",nm:"nameRole",elv:"0"},
        2:{type:"chckbx",tl:"Rutas permitidas",nm:"acceds[]",vl:@json($permission),elv:"1"},
  };
    valdis={clase:"red",text:1};
</script>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
					<h3 class="card-title">
            <a href="javascript:"class="" onclick="newfloatv2(kad);">
            <i class="ion-ios7-plus-outline "></i>
            &nbsp;&nbsp;Nuevo rol de usuario
          </a>
          </h3>
					<div class="card-options">
						<a href="#" class="card-options-collapse" data-toggle="card-collapse">
              <i class="fe fe-chevron-up"></i>
            </a>
						<a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen">
              <i class="fe fe-maximize"></i>
            </a>
						<a href="#" class="card-options-remove" data-toggle="card-remove">
              <i class="fe fe-x"></i>
            </a>
					</div>
				</div>
        <div class="card-body">
          <div class="table-responsive">
            <table i class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Permisos</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($roles as $role)
                  @php $perms=array(); @endphp
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td >{{$role->nameRole}}</td>
                    <td >
                      @foreach($role->nameroles AS $acced)
                        {{$acced->naccess}}<br>
                  @php $perms[]=$acced->id; @endphp
                      @endforeach
                    </td>
                    <td class="td-actions  text-left">
                      <a  href="javascript:"
                          onclick="modifyfloat('{{$role->id}}',kad,criteria);">
                          <i class="text-20"data-feather="edit-3"></i>
                      </a>
                      <a  href="javascript:"
                          onclick="deleteD('{{$role->id}}','{{ csrf_token() }}');">
                          <i class="text-12" data-feather="trash"></i>
                      </a>
                    </td>
                  </tr>
                  @php
                  $criti[$role->id]=array($role->nameRole, $perms);
                  @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
    </script>

@endsection
