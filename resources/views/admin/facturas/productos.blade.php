@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"slct",tl:"Producto",id:"producto_id",nm:"producto_id",vl:@json($productos_arr),elv:"0", add:'onchange="calcularCantidad()"'},
                2:{type:"nbr",tl:"Cantidad",id:"cantidad_medida",nm:"cantidad_medida",elv:"1", nxt:6, add:'onchange="calcularCantidad()"'},
                3:{type:"slct",tl:"Medida inicial",id:"medida_id",nm:"medida_id",vl:@json($unidades_arr),elv:"2", nxt:6, add:'onchange="calcularCantidad()"'},
                4:{type:"nbr",tl:"Cantidad Final",id:"cantidad",nm:"cantidad",elv:"3", nxt:6, add:"readonly"},
                5:{type:"slct",tl:"Medida Final",id:"medida_texto",nm:"medida_texto",vl:@json($unidades_arr),elv:"4", nxt:6, add:'disabled'},
                6:{type:"hddn",id:"medida",nm:"medida",elv:"4"},
                7:{type:"formato_moneda",tl:"Costo unitario en medida inicial",id:"costo_unitario_inicial",nm:"costo_unitario_inicial",elv:"5", nxt:6, add:`oninput="calcularCantidad('unitario')"`},
                8:{type:"hddn",id:"costo_unitario",nm:"costo_unitario", elv:"6"},
                9:{type:"formato_moneda",tl:"Valor total",id:"valor_total",nm:"valor_total",elv:"7", nxt:6, add:`oninput="calcularCantidad('total')"`},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegas')}}">Inicio</a></li>
        <li><a href="{{url('/facturas', ['bodega_id' => $bodega->id])}}">Facturas</a></li>
        <li>Productos de esta factura</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: <b>{{$bodega->nombre;}}</b>
                </h3>
                <h3 class="card-title">
                    <a href="{{url('/facturas', ['bodega_id' => $bodega->id])}}"><i class="text-20" data-feather="skip-back"></i>Regresar</a>
                </h3>
                <div style="width:100%; height: 35px;">
                    <h4 class="float-right">Factura No.{{$factura->numero;}} Serie: {{$factura->serie;}}</h4>
                </div>
                <div style="width:100%; height: 35px;">
                    <h4 class="float-right">Proveedor: {{$factura->proveedor;}}</h4>
                </div>
                <div style="width:100%; height: 35px;">
                    <h4 class="float-right">Fecha: {{$factura->fecha->format('d/m/Y')}}</h4>
                </div>
                <div style="width:100%; height: 35px;">
                    <h3 class="float-right">Total: Q.{{$factura->total;}}</h3>
                </div>

                <h3 class="card-title">
                    <a href="javascript:"class="" onclick="newfloatv2(kad,undefined,undefined,undefined,'/productosFactura/{{$bodega->id}}/{{$factura->id}}');">
                        <i class="ion-ios7-plus-outline "></i>
                        &nbsp;&nbsp; Agregar producto a esta factura
                    </a>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="productos_factura_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Medida</th>
                                <th>Cantidad final</th>
                                <th>Medida final</th>
                                <th>Costo unitario (medida final)</th>
                                <th>Valor total</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $productos_obj[$value->producto_id]->nombre }}</td>
                                    <td>{{ $value->cantidad_medida }}</td>
                                    <td>{{ $unidades_obj[$value->medida_id]->nombre }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>{{ $unidades_obj[$value->medida]->nombre }}</td>
                                    <td>Q. {{ number_format($value->costo_unitario, 2) }}</td>
                                    <td><h5>Q. {{ number_format($value->valor_total, 2) }}</h5></td>
                                    <td>
                                        <a href="javascript:" onclick="modifyfloat('{{$value->id}}',kad,criteria,undefined,undefined);$('.card-title')[1].innerHTML = 'Editar producto';" title="Editar">
                                            <i class="text-20"data-feather="edit-3"></i>
                                        </a>
                                        <a href="javascript:" onclick="deleteD('{{$value->id}}','{{ csrf_token() }}', 'destroy');" title="Eliminar">
                                            <i class="text-20"data-feather="trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php
                                $criti[$value->id] = [
                                        $value->producto_id,
                                        $value->cantidad_medida,
                                        $value->medida_id,
                                        $value->cantidad,
                                        $value->medida,
                                        Helper::redondear_moneda($value->valor_total / $value->cantidad_medida, 2),//costo_unitario_inicial
                                        $value->costo_unitario,
                                        $value->valor_total
                                    ];
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            <h4 class="float-right" style="width:300px;"></h4>
                            <h3 class="float-right"> {{number_format($suma, 2)}} </h3>
                            <h4 class="float-right">Total Q. </h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right" style="margin-top:10px;">
                            @if ($suma == $factura->total)
                                <h5 class="w-badge badge-success">Valor de la factura cuadrado correctamente</h5>
                            @elseif ($suma > $factura->total)
                                <h5 class="w-badge badge-danger">La suma de los productos supera el valor declarado en la factura</h5>
                            @else
                                <h5 class="w-badge badge-warning">Faltan valores para cuadrar el valor de la factura</h5>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var criteria = @json($criti),
        productos = @json($productos_obj),
        unidades = @json($unidades_obj),
        conversiones = @json($conversiones);

    function calcularCantidad(tipo_costo){
        let unidad_inicial = $('#medida_id').val(),
            cantidad_inicial = $('#cantidad_medida').val(),
            producto_id = $('#producto_id').val(),
            unidad_final = productos[producto_id].medida_final_id,
            factor_de_conversion = 1;
        $('#medida_texto').val(unidad_final)
        $('#medida').val(unidad_final)

        let cantidad_final = 1,
            operacion = null,
            valor = 1;

        for(let i = 0; i < conversiones.length; i++){
            let temp = conversiones[i];

            if (unidad_inicial == temp.unidad_inicial_id && unidad_final == temp.unidad_final_id){
                operacion = temp.operacion
                factor_de_conversion = temp.valor
                break
            }
        }

        if (operacion == 'dividir'){
            cantidad_final = cantidad_inicial / factor_de_conversion
        }else{
            cantidad_final = cantidad_inicial * factor_de_conversion
        }

        $('#cantidad').val(cantidad_final)

        ///////// calculo del valor total y unitario con base en el ingresado por usuario
        let tot = $('#valor_total').val(),
            uni = $('#costo_unitario_inicial').val();

        if (tipo_costo == 'unitario'){
            let res = redondear_moneda(uni * cantidad_inicial)
            $('#valor_total').val(res)
        }else{
            let res = redondear_moneda(tot / cantidad_inicial)
            $('#costo_unitario_inicial').val(res)
        }
        let costo_unitario = redondear_moneda(tot / cantidad_final)
        $('#costo_unitario').val(costo_unitario)
    }

</script>

@endsection

@section('bottom-js')
    <script>
        $('#productos_factura_table').DataTable();
    </script>
@endsection
