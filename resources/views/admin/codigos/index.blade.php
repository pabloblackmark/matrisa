@extends('admin.layouts.master')
@section('main-content')
@php $criti = [];@endphp
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
      1:{type:"txt",tl:"Codigo",nm:"codigo",elv:"0"},
      2:{type:"txt",tl:"Id Proveedor",nm:"id_provider",elv:"1"},
      3:{type:"txt",tl:"Id Admin",nm:"id_admin",nxt:6,elv:"2"},
      4:{type:"txt",tl:"Id Factura",nm:"id_factura",nxt:6,elv:"3"},
      5:{type:"txt",tl:"No Factura",nm:"no_factura",nxt:6,elv:"4"},
      6:{type:"txt",tl:"Valor",nm:"valor",nxt:6,elv:"5"},
      7:{type:"txtarea",tl:"Detalle",nm:"info",nxt:6,elv:"6"},
      // 8:{type:"txt",tl:"Status",nm:"status",elv:"7"},
      8:{type:"slct",tl:"Estado",nm:"status",elv:"7",vl:[['0', 'No Pagado'],['1', 'Parcialmente Pagada'],['2', 'Pagado']],nxt:6},
  };
    // valdis={clase:"red",text:1};
</script>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <a href="javascript:"class="" onclick="newfloatv2(kad);">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nueva Contraseña
                </a>
            </h3>

            <div class="col-md-4">
                             <label for="proveedor">Proveedor:</label>
                             <select class="form-control" id="proveedor" name="proveedor">
                                 <option value=""> --- Todos --- </option>
                                 @foreach($proveedores as $proveedor)
                                      @php
                                        $selected = "";

                                        if ($request->id_provider==$proveedor->id) {
                                            $selected = "selected";
                                        }

                                      @endphp
                                     <option value="{{ $proveedor->id }}" {{ $selected }} >{{ $proveedor->nombre }}</option>
                                 @endforeach
                             </select>
            </div>
        </div>


        <div class="card-body">
          <div class="table-responsive">
            <table id="codigos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>Contraseña</th>
                  <th>Proveedor</th>
                  <th>No. Factura</th>
                  <th>Valor</th>
                  <th>Fecha de Pago</th>
                  <th>Estado</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($data as $value)
                  @php $perms=array(); @endphp
                  <tr>
                    <td >{{$value->codigo}}</td>
                    <td >{{ @$proveedor_nombre[$value->id_provider] }}</td>
                    <td >{{ $value->no_factura }}</td>
                    <td >Q. {{ $value->valor }}</td>
                    <td >{{ date("d-m-Y", strtotime($value->fecha_pago)) }}</td>
                    <td >

                      @if ($value->status==0)
                          <span class="badge w-badge badge-danger">No Pagada</span>
                      @elseif ($value->status==1)
                          <span class="badge w-badge badge-warning">Parcialmente Pagada</span>
                      @elseif ($value->status==2)
                          <span class="badge w-badge badge-success">Pagada</span>
                      @endif
                    

                    </td>
                    <td class="td-actions  text-left">
                      <a  href="javascript:"
                          onclick="modifyfloat('{{$value->id}}',kad,criteria,undefined,undefined,true);" title="Editar">
                          <i class="text-20"data-feather="edit-3"></i>
                      </a>

                      <a  href="javascript:"
                          onclick="deleteD('{{$value->id}}','{{ csrf_token() }}');" title="Borrar">
                          <i class="text-12" data-feather="trash"></i>
                      </a>
                    </td>
                  </tr>
                  @php
                  $criti[$value->id]=array($value->codigo, $value->id_provider, $value->id_admin, $value->id_factura, $value->no_factura, $value->valor, $value->info, $value->status);
                  @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
<script type="text/javascript">
      var criteria = @json($criti);



    $(function () {

        $("#proveedor").change(function(e) {
            window.location.href = '{{ url("codigos") }}?id_provider=' + $(this).val();
        });

    });


</script>

@endsection

@section('bottom-js')
    <script>
        $('#codigos_table').DataTable();
    </script>
@endsection
