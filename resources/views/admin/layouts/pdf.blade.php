<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Matrisa</title>

<link rel="stylesheet" href="{{url('/')}}/assets/styles/css/themes/lite-blue.min.css">

<link rel="stylesheet" href="{{ asset('assets/styles/vendor/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.bubble.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.snow.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}" />

<link rel="stylesheet" href="{{ asset('assets/js/chosen_v1.8.7/docsupport/style.css') }}">

<link rel="stylesheet" href="{{ asset('assets/js/chosen_v1.8.7/chosen.css') }}">

<link rel="stylesheet" href="{{ asset('css/admin/fixes.css') }}">



<script src="{{ asset('assets/js/vendors/jquery-3.6.0.min.js')}}"></script>

</head>
<body style="background-color: #FFF;">

@section('main-content')
@show

@include('admin.layouts.common.scriptsfooter')
</body>

</html>
