<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  @include('admin.layouts.common.scripts')
</head>
<body class="text-left">

<div class="app-admin-wrap layout-sidebar-large">
    @include('admin.layouts.common.header')
    @include('admin.layouts.common.sidebar')
    <div class="main-content-wrap sidenav-open d-flex flex-column">
      <div class="main-content">
         @section('main-content')
         @show
       </div>
       <div class="flex-grow-1"></div>
       @include('admin.layouts.common.footer')
    </div>
</div>
@include('admin.layouts.common.scriptsfooter')

<!-- <div class='loadscreen' id="preloader">
<div class="loader spinner-bubble spinner-bubble-primary"></div>
</div>
<div id="loading">
<img src="{{asset('assets/images/other/loader.svg')}}" class="loader-img" alt="Loader">
</div> -->

@section('bottom-js')
@show



</body>

</html>
