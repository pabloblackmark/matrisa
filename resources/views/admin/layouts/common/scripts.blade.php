<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Matrisa -</title>
<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
@yield('before-css')
{{-- theme css --}}

<link  href="{{asset('assets/css/icons.css')}}" rel="stylesheet">

<link id="gull-theme" rel="stylesheet" href="{{ asset('assets/fonts/iconsmind/iconsmind.css') }}">
<link id="gull-theme" rel="stylesheet" href="{{ asset('assets/styles/css/themes/lite-blue.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.bubble.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.snow.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}" />

<link rel="stylesheet" href="{{ asset('assets/js/chosen_v1.8.7/docsupport/style.css') }}">

<link rel="stylesheet" href="{{ asset('assets/js/chosen_v1.8.7/chosen.css') }}">

<link rel="stylesheet" href="{{ asset('css/admin/fixes.css') }}">



<script src="{{ asset('assets/js/vendors/jquery-3.6.0.min.js')}}"></script>




{{-- page specific css --}}
@yield('page-css')



<!-- <meta http-equiv="x-ua-compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="icon" type="image/png" href="{{ asset('img_admin/favicon.ico')}}" />
<title>Matrisa - </title> -->
<!-- Sweet-alert css -->
<!-- <link href="{{asset('assets/plugins/sweet-alert/sweetalert.css')}}" rel="stylesheet" /> -->
<!-- owl-carousel css-->
<!--Bootstrap-daterangepicker css-->
<!-- <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}"> -->
<!--Bootstrap-datepicker css-->
<!-- <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}"> -->
<!--Font icons css-->
<!-- <link  href="{{asset('assets/css/icons.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/notify/css/notifIt.css')}}" rel="stylesheet" /> -->
<!-- Nice-select css  -->
<!-- <link href="{{asset('assets/plugins/jquery-nice-select/css/nice-select.css')}}" rel="stylesheet"/> -->
<!--Mutipleselect css-->
<!-- <link rel="stylesheet" href="{{asset('assets/plugins/multipleselect/multiple-select.css')}}"> -->
<!--multi css-->
<!-- <link rel="stylesheet" href="{{asset('assets/plugins/multi/multi.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/plugins/fancyuploder/fancy_fileupload.css')}}"> -->

<!-- File Upload css-->
<!-- <link href="{{asset('assets/plugins/fileuploads/css/dropify.css')}}" rel="stylesheet" /> -->
