<script src="{{ asset('assets/js/vendor/perfect-scrollbar.min.js')}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-4.1.3/popper.min.js')}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/vendors/jquery.sparkline.min.js')}}"></script>
<script src="{{ asset('assets/js/vendors/circle-progress.min.js')}}"></script>
<script src="{{ asset('assets/plugins/rating/jquery.rating-stars.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--Moment js-->
<script src="{{ asset('assets/plugins/moment/moment.min.js')}}"></script>
<!-- Custom scroll bar js-->
<script src="{{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!--owl-carousel js-->
{{-- <script src="{{ asset('assets/plugins/owl-carousel/owl.carousel.js')}}"></script> --}}
<!-- Bootstrap-daterangepicker js -->
<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- Bootstrap-datepicker js -->
<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<!--Counters -->
{{-- <script src="{{ asset('assets/plugins/jquery-countdown/countdown.js')}}"></script> --}}
{{-- <script src="{{ asset('assets/plugins/jquery-countdown/jquery.plugin.min.js')}}"></script> --}}
{{-- <script src="{{ asset('assets/plugins/jquery-countdown/jquery.countdown.js')}}"></script> --}}

@if (!Request::routeIs('login'))
{{--
<!-- Bootstrap-scripts js -->
<script src="{{ asset('assets/js/vendors/bootstrap.bundle.min.js')}}"></script>
<!-- Sparkline JS-->
<script src="{{ asset('assets/js/vendors/jquery.sparkline.min.js')}}"></script>
<!-- Chart-circle js -->
<script src="{{ asset('assets/js/vendors/circle-progress.min.js')}}"></script>
<!-- Rating-star js -->
<script src="{{ asset('assets/plugins/rating/jquery.rating-stars.js')}}"></script>
<!-- Clipboard js -->
<script src="{{ asset('assets/plugins/clipboard/clipboard.min.js')}}"></script>
<script src="{{ asset('assets/plugins/clipboard/clipboard.js')}}"></script>
<!-- Prism js -->
<script src="{{ asset('assets/plugins/prism/prism.js')}}"></script>
<!-- Nice-select js-->
<script src="{{ asset('assets/plugins/jquery-nice-select/js/jquery.nice-select.js')}}"></script>
<script src="{{ asset('assets/plugins/jquery-nice-select/js/nice-select.js')}}"></script>
<!-- P-scroll js -->
<script src="{{ asset('assets/plugins/p-scroll/p-scroll.js')}}"></script>
<script src="{{ asset('assets/plugins/p-scroll/p-scroll-leftmenu.js')}}"></script>
<!-- Sidemenu js-->
<script src="{{ asset('assets/plugins/sidemenu/sidemenu.js')}}"></script>
<!-- Sidemenu-respoansive-tabs js -->
<script src="{{ asset('assets/plugins/sidemenu-responsive-tabs/js/sidemenu-responsive-tabs.js')}}"></script>
--}}
<!-- Sweet-alert js  -->
<!--<script src="{{ asset('assets/plugins/sweet-alert/sweetalert.min.js')}}"></script>-->
<!--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>-->
<script src="{{ asset('assets/plugins/sweet-alert2.9/sweetAlert.js')}}"></script>
<!-- JQVMap -->
<script src="{{ asset('assets/plugins/jqvmap/jquery.vmap.js')}}"></script>
<script src="{{ asset('assets/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<script src="{{ asset('assets/plugins/jqvmap/jquery.vmap.sampledata.js')}}"></script>
<!-- Apexchart js-->
<script src="{{ asset('assets/js/apexcharts.js')}}"></script>
<!-- Chart js-->
{{-- <script src="{{ asset('assets/plugins/chart/chart.min.js')}}"></script> --}}
<!-- Index js -->
{{-- <script src="{{ asset('assets/js/index.js')}}"></script> --}}
{{-- <script src="{{ asset('assets/js/index-map.js')}}"></script> --}}
<!-- Leftmenu js -->
{{-- <script src="{{ asset('assets/js/left-menu.js')}}"></script> --}}


<!-- Rightsidebar js -->
{{-- <script src="{{ asset('assets/plugins/sidebar/sidebar.js')}}"></script> --}}
<script src="{{asset('js/admin/js_own/abcscriptv2.js')}}"></script>
<script src="{{asset('js/admin/js_own/functions.js')}}"></script>
<script src="{{asset('js/admin/js_own/validador.js')}}"></script>
@endif
<!-- Custom js-->
<script src="{{ asset('assets/js/custom.js')}}"></script>
<!-- Notifications js -->
<script src="{{ asset('assets/plugins/notify/js/sample.js')}}"></script>
<script src="{{ asset('assets/plugins/notify/js/jquery.growl.js')}}"></script>
<script src="{{ asset('assets/plugins/notify/js/notifIt.js')}}"></script>
<!-- Data tables -->
<script src="{{ asset('assets/plugins/datatable/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/pdfmake.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/vfs_fonts.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>

<!-- Counters js -->
<script src="{{ asset('assets/plugins/counters/counterup.min.js')}}"></script>
<script src="{{ asset('assets/plugins/counters/waypoints.min.js')}}"></script>
{{-- <script src="{{ asset('assets/plugins/counters/counters-1.js')}}"></script> --}}
<!-- Nice-select js-->
<script src="{{ asset('assets/plugins/jquery-nice-select/js/jquery.nice-select.js')}}"></script>
<script src="{{ asset('assets/plugins/jquery-nice-select/js/nice-select.js')}}"></script>
<!-- data table js -->
{{-- <script src="{{ asset('assets/js/datatable.js')}}"></script> --}}

<!--MutipleSelect js-->
<script src="{{ asset('assets/plugins/multipleselect/multiple-select.js')}}"></script>
<script src="{{ asset('assets/plugins/multipleselect/multi-select.js')}}"></script>

<!--multi js-->
<script src="{{ asset('assets/plugins/multi/multi.min.js')}}"></script>
<!-- Summernote js  -->
<script src="{{ asset('assets/plugins/summernote/summernote-bs4.js')}}"></script>
<!-- File uploads js -->
<script src="{{ asset('assets/plugins/fileuploads/js/dropify.js')}}"></script>
<script src="{{ asset('js/admin/js_own/jquery.fileuploader.min.js')}}"></script>

<script src="{{ asset('assets/js/feather.min.js')}}"></script>
<script src="{{ asset('assets/js/script.js') }}"></script>
<script src="{{ asset('assets/js/sidebar.large.script.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script src="{{asset('assets/js/vendor/quill.min.js')}}"></script>
{{-- <script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script> --}}
<script src="{{ asset('assets/js/chosen_v1.8.7/docsupport/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/chosen_v1.8.7/chosen.jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/chosen_v1.8.7/docsupport/prism.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('assets/js/chosen_v1.8.7/docsupport/init.js') }}" type="text/javascript" charset="utf-8"></script>

<script src="{{ asset('assets/plugins/datatable1/datatables.min.js')}}"></script>

<script type="text/javascript">
feather.replace({ width: '20px', 'height': '20px','stroke-width':'1px' })
if($('#myTab').length>0){
  $( "#myTab" ).sortable({
    update: function( event, ui ) {
      if (typeof hasChange === "function") {
        hasChange(event, ui);
      }
    }
  });
}
if($('.sortable').length>0){
  $( ".sortable" ).sortable({
    update: function( event, ui ) {
      if (typeof hasChange === "function") {
        hasChange(event, ui);
      }

    }
  });
}
if (typeof setTable === "function") {
  setTable();
}
var select = document.getElementById('fruit_select1');
if(select != undefined){
	multi(select, {
		enable_search: false
	} );
}
if($('.datepicker').length>0){
	$('.datepicker').datepicker({format: "dd/mm/yyyy"});
	 // $('.datepicker').datepicker("option", "dateFormat","dd/mm/yy");
}



if($('#fruit_select2').length>0){
var select = document.getElementById('fruit_select2');
  	multi(select, {
			enable_search: true,
            search_placeholder: 'Buscar...'
		} );
}
if($('#fruit_select3').length>0){
var select = document.getElementById('fruit_select3');
  	multi(select, {
			enable_search: true,
            search_placeholder: 'Buscar...'
		} );
}
if($('#fruit_select4').length>0){
var select = document.getElementById('fruit_select4');
  	multi(select, {
			enable_search: true,
            search_placeholder: 'Buscar...'
		} );
}
function dateBeLoad(){
  $('.datepicker').datepicker({format: "dd/mm/yyyy"});
}
function elem(){
  var select = document.getElementById('fruit_select');
  if(select!=undefined&&select.length>0){
    multi(select, {
      non_selected_header: 'Literaturas',
      selected_header: 'Para agregar'
    });
  }
}
elem();



if($(".metatx").length>0){
  $(".metatx").hover(function(){
    console.log($(this).position().left,$(this).position().top);
  });
}
if($('.editorSummer').length>0){
  $('.editorSummer').summernote({
		placeholder: 'Escribe aquí el texto',
		tabsize: 3,
		height: 250,
        toolbar: [
          ['font', ['bold','italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph','fontsize']],
          // ['fontsize', ['fontsize']],
          // ['insert', ['link', 'picture', 'video']],
          // ['view', ['fullscreen']]
        ]
  });
}

$('body').on('focus',".daterange", function(){
    $(this).daterangepicker({  drops: 'up',locale: {format: 'DD/MM/YYYY'}});


})
if($('#tableDown').length>0){
  var table = $('#tableDown').DataTable( {
  		lengthChange: false,
  		buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
  	} );
}

function formato_moneda(elm){
    let val = elm.value
    elm.value = Number.parseFloat(val).toFixed(2)
}
if($('#toast-success').length>0){
$("#toast-success").on("click", function () {
    toastr.success("toastr is a Javascript library for non-blocking notifications. jQuery is required!", "Miracle Max Says", {
      timeOut: "50000"
    });
  })
}


</script>
@component('components.messagesForm')
@endcomponent

<!-- Footer Start -->

<!-- fotter end -->
