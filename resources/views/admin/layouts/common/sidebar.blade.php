<script>
    const ACCESOS = @json($menubar)["access"]
    let pathname = window.location.pathname,
        path = pathname.split("/");

    function find_acceso_actual(){
        for (i = 0; i < path.length; i++){
            let temp_path = path[i]
            if (path[i] != ""){
                for (key in ACCESOS){
                    let temp =ACCESOS[key]
                    for (k = 0; k < temp.length; k++){
                        if (temp_path == temp[k]['perm']){
                            return key;
                        }
                    }
                }
            }
        }
    }
</script>

<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            {{-- @if(count($menubar["groups"])>0)
             @foreach ($menubar["groups"] as $permissions)
               <li class="{{(\Request::is($permissions['perm'])?'resp-tab-active active':'')}}">
                  <span class="side-menu__icon"
                  data-toggle="tooltip" data-placement="right"
                  title="" data-original-title="{{$permissions['name']}}">
                    <i class="text-20 {{$permissions['icon']}}"></i>
                  </span>
               </li>
             @endforeach
            @endif --}}
            @if(count($menubar["groups"])>0)
             @foreach ($menubar["groups"] as $permissions)

               <li class="nav-item "
                 data-item="{{$permissions['name']}}">
                   <a class="nav-item-hold" href="#">
                       <i class="text-20 {{$permissions['icon']}}"></i>
                       <span class="nav-text">{{$permissions['name']}}</span>
                   </a>
                   <div class="triangle"></div>
               </li>
             @endforeach
            @endif




        </ul>
    </div>

    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        @if(count($menubar["access"])>0)
         @foreach ($menubar["access"] as $k => $accedes)

        <ul class="childNav" data-parent="{{$k}}">

            @foreach ($accedes as $key => $page)
            <li class="nav-item ">
                <a class="{{ Route::currentRouteName()=='admin.'.$page["perm"].'.'.'index' ? 'open' : '' }}"
                    href="{{route('admin.'.$page["perm"].'.'.'index')}}">
                    <!-- <i class="nav-icon i-Receipt"></i> -->
                    <span class="item-name">{{$page['name']}}</span>
                </a>
            </li>
            @endforeach

        </ul>
        @endforeach
        @endif
        {{-- <ul class="childNav" data-parent="forms">

            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='forms-basic' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                    <span class="item-name">Basic Elements</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='basic-action-bar' ? 'open' : '' }}"
                    href="">
                    <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                    <span class="item-name">Basic action bar </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='form-layouts' ? 'open' : '' }}"
                    href="">
                    <i class="nav-icon i-Split-Vertical"></i>
                    <span class="item-name">Form Layouts</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='multi-column-forms' ? 'open' : '' }}"
                    href="">
                    <i class="nav-icon i-Split-Vertical"></i>
                    <span class="item-name">Multi column forms</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='form-input-group' ? 'open' : '' }}"
                    href="">
                    <i class="nav-icon i-Receipt-4"></i>
                    <span class="item-name">Input Groups</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='form-validation' ? 'open' : '' }}"
                    href="">
                    <i class="nav-icon i-Close-Window"></i>
                    <span class="item-name">Form Validation</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='smartWizard' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-Width-Window"></i>
                    <span class="item-name">Smart Wizard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='tagInput' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-Tag-2"></i>
                    <span class="item-name">Tag Input</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='form-editor' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-Pen-2"></i>
                    <span class="item-name">Rich Editor</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="widgets">
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='widget-card' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-Receipt-4"></i>
                    <span class="item-name">widget card</span>
                </a>
            </li>
            <li class="nav-item">


                <a class="{{ Route::currentRouteName()=='widget-statistics' ? 'open' : '' }}"
                    href="">
                    <i class="nav-icon i-Receipt-4"></i>
                    <span class="item-name">widget statistics</span>
                </a>
            </li>

            <li class="nav-item">


                <a class="{{ Route::currentRouteName()=='widget-list' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-Receipt-4"></i>
                    <span class="item-name">Widget List <span class="ml-2 badge badge-pill badge-danger">
                            New</span></span>
                </a>
            </li>

            <li class="nav-item">


                <a class="{{ Route::currentRouteName()=='widget-app' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-Receipt-4"></i>
                    <span class="item-name">Widget App <span class="ml-2 badge badge-pill badge-danger"> New</span>
                    </span>
                </a>
            </li>
            <li class="nav-item">


                <a class="{{ Route::currentRouteName()=='widget-weather-app' ? 'open' : '' }}"
                    href="">
                    <i class="nav-icon i-Receipt-4"></i>
                    <span class="item-name"> Weather App <span class="ml-2 badge badge-pill badge-danger"> New</span>
                    </span>
                </a>
            </li>

        </ul>

        <ul class="childNav" data-parent="charts">
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='echarts' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                    <span class="item-name">echarts</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='chartjs' ? 'open' : '' }}" href="">
                    <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                    <span class="item-name">ChartJs</span>
                </a>
            </li>
            <li class="nav-item dropdown-sidemenu">
                <a>
                    <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                    <span class="item-name">Apex Charts</span>
                    <i class="dd-arrow i-Arrow-Down"></i>
                </a>
                <ul class="submenu">
                    <li><a class="{{ Route::currentRouteName()=='apexAreaCharts' ? 'open' : '' }}"
                            href="{{route('apexAreaCharts')}}">Area Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexBarCharts' ? 'open' : '' }}"
                            href="{{route('apexBarCharts')}}">Bar Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexBubbleCharts' ? 'open' : '' }}"
                            href="{{route('apexBubbleCharts')}}">Bubble Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexColumnCharts' ? 'open' : '' }}"
                            href="{{route('apexColumnCharts')}}">Column Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexCandleStickCharts' ? 'open' : '' }}"
                            href="{{route('apexCandleStickCharts')}}">CandleStick Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexLineCharts' ? 'open' : '' }}"
                            href="{{route('apexLineCharts')}}">Line Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexMixCharts' ? 'open' : '' }}"
                            href="{{route('apexMixCharts')}}">Mix Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexPieDonutCharts' ? 'open' : '' }}"
                            href="{{route('apexPieDonutCharts')}}">PieDonut Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexRadarCharts' ? 'open' : '' }}"
                            href="{{route('apexRadarCharts')}}">Radar Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexRadialBarCharts' ? 'open' : '' }}"
                            href="{{route('apexRadialBarCharts')}}">RadialBar Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexScatterCharts' ? 'open' : '' }}"
                            href="{{route('apexScatterCharts')}}">Scatter Charts</a></li>
                    <li><a class="{{ Route::currentRouteName()=='apexSparklineCharts' ? 'open' : '' }}"
                            href="{{route('apexSparklineCharts')}}">Sparkline Charts</a></li>

                </ul>
            </li>





        </ul> --}}


    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->
