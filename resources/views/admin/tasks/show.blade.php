@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
  var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
          1:{type:"txt",tl:"Nombre",nm:"name",elv:"0",nxt:6},
          2:{type:"txtarea",tl:"Descripción",nm:"description",elv:"1",nxt:12},
          3:{type:"nbr",tl:"Cantidad",nm:"count",elv:"2",nxt:12},
      }
  var conform={action:"{{route('admin.permissions.store')}}"}
    valdis={clase:"red",text:1,checkbox:1,radio:1};
</script>
@php
    $criti = [];
@endphp

<div class="breadcrumb">
    <h1 class="mr-2">{{$breadcrumb['base']}}</h1>
    <ul>
        <li> <a href="{{url($breadcrumb['inicio'])}}"> Inicio</a> </li>
        <li>Administrar metas</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
		  <h3 class="card-title">
            Metas de: <b>{{$name}}</b>
          </h3>
          <h5><a href="javascript:"class="" onclick="newfloatv2(kad);"> <i class="ion-ios7-plus-outline "></i>&nbsp;&nbsp;Agregar meta </a></h5>
				</div>

        <div class="card-body">
          <div class="table-responsive">
            <table id="tareas_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Cantidad</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($files as $f)
                      <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td class="hidden-xs">{{$f->name}}</td>
                        <td class="hidden-xs">{{$f->description}}</td>
                        <td class="hidden-xs">{{$f->count}}</td>

                        <td>

                          <a href="javascript:" class="" onclick="modifyfloat('{{$f->id}}',kad,criteria);" title="Editar archivo">
                              <i class="text-20"data-feather="edit-3"></i>
                          </a>

                          <a href="javascript:"onclick="deleteD('{{$f->id}}','{{ csrf_token() }}');" title="Eliminar archivo">
                              <i class="text-12" data-feather="trash"></i>
                          </a>
                        </td>
                      </tr>
                      @php
                        $criti[$f->id] = [$f->name, $f->description, $f->count];
                      @endphp

                    @endforeach
    		       </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = <?=json_encode($criti)?>;
      function fillimg(sto)
      {
        $("#imag").val(sto);
        flotantecloseblank();
      }
    </script>
    @component('components.messagesForm')
    @endcomponent
@endsection

@section('bottom-js')
    <script>
        $('#tareas_table').DataTable();
    </script>
@endsection
