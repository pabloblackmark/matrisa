@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    
function valida_form() {

    tipo = $('#tipo').val();

    switch (tipo) {
      case 'bancario':

            banco = $('#bancario #banco').val();
            no_credito = $('#bancario #no_credito').val();
            monto = $('#bancario #monto').val();
            no_cuotas = $('#bancario #no_cuotas').val();
            valor_cuota = $('#bancario #valor_cuota').val();
            primera_cuota = $('#bancario #primera_cuota').val();
            fecha_debito = $('#bancario #fecha_debito').val();
            fecha_aprobado = $('#bancario #fecha_aprobado').val();

            if (banco=="" || no_credito=="" || monto==0.00 || no_cuotas==0 || valor_cuota==0.00 || primera_cuota=="" || fecha_debito==null || fecha_aprobado=="") {
                alert('Debes llenar todos los datos del formualio.');
                return false;
            } else {
                $('#adelanto, #manutencion, #embargo, #otros').remove();
                return true;
            }
            
        break;
      case 'adelanto':
            
            monto = $('#adelanto #monto').val();
            fecha_debito = $('#adelanto #fecha_debito').val();
            detalle = $('#adelanto #detalle').val();

            if (monto==0.00 || fecha_debito==null || detalle=="") {
                alert('Debes llenar todos los datos del formualio.');
                return false;
            } else {
                $('#bancario, #manutencion, #embargo, #otros').remove();
                return true;
            }

        break;
      case 'manutencion':

            monto = $('#manutencion #monto').val();
            fecha_debito = $('#manutencion #fecha_debito').val();
            detalle = $('#manutencion #detalle').val();

            if (monto==0.00 || fecha_debito==null || detalle=="") {
                alert('Debes llenar todos los datos del formualio.');
                return false;
            } else {
                $('#bancario, #adelanto, #embargo, #otros').remove();
                return true;
            }

        break;
      case 'embargo':

            no_credito = $('#embargo #no_credito').val();

            monto = $('#embargo #monto').val();
            no_cuotas = $('#embargo #no_cuotas').val();
            valor_cuota = $('#embargo #valor_cuota').val();
            primera_cuota = $('#embargo #primera_cuota').val();
            fecha_debito = $('#embargo #fecha_debito').val();
            detalle = $('#embargo #detalle').val();

            if (monto==0.00 || no_credito=="" || no_cuotas==0 || valor_cuota==0.00 || primera_cuota=="" || fecha_debito==null || detalle=="") {
                alert('Debes llenar todos los datos del formualio.');
                return false;
            } else {
                $('#bancario, #adelanto, #manutencion, #otros').remove();
                return true;
            }
 
        break;
      case 'otros':

            nombre = $('#otros #nombre').val();
            monto = $('#otros #monto').val();
            no_cuotas = $('#otros #no_cuotas').val();
            valor_cuota = $('#otros #valor_cuota').val();
            primera_cuota = $('#otros #primera_cuota').val();
            fecha_debito = $('#otros #fecha_debito').val();
            detalle = $('#otros #detalle').val();

            if (nombre=="" || monto==0.00 || no_cuotas==0 || valor_cuota==0.00 || primera_cuota=="" || fecha_debito==null || detalle=="") {
                alert('Debes llenar todos los datos del formualio.');
                return false;
            } else {
                $('#bancario, #adelanto, #manutencion, #embargo').remove();
                return true;
            }
 
        break;
    }

    return true;

}

</script>

<div class="breadcrumb">

    <h1 class="mr-2">Descuentos</h1>

                        <ul>
                            <li><a href="{{url('/descuentos')}}">Listado</a></li>
                        </ul>    
    
   
</div>
<div class="separator-breadcrumb border-top"></div>


<form method="POST" action="{{ url('descuentos') }}" onsubmit="return valida_form();">

    @csrf

    <div class="row">
        <div class="col-md-12">


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Crear Descuento</h4>
                </div>
                <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
                    <div class="row">
                        <div class="col-md-4 form-group mb-3">
                             <label for="id_empresa">Empresa:</label>
                              <select class="form-control" id="id_empresa" name="id_empresa" required>
                                <option value="" selected disabled>-- seleccionar empresa --</option>
                                @foreach ($empresas as $k => $v)
                                  <option value="{{$v[0]}}" {{(isset($request->id_empresa)&&$v[0]==$request->id_empresa?'selected':'')}}>{{$v[1]}}</option>
                                @endforeach
                              </select>

                        </div>
                        <div class="col-md-4 form-group mb-3">
                            <label for="id_cliente">Colaborador:</label>
                            <select class="form-control" id="id_colab" name="id_colab" required>
                                
                            </select>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-4 form-group mb-3">
                             <label for="tipo">Descuento:</label>
                             <select class="form-control" id="tipo" name="tipo" required>
                                 <option selected disabled value=""> --- tipo de descuento --- </option>
                                     <option value="bancario">Préstamo Bancario</option>
                                     <option value="adelanto">Adelanto de Salario</option>
                                     <option value="manutencion">Manutención</option>
                                     <option value="embargo">Embargo</option>
                                     <option value="otros">Otros</option>
                             </select>
                        </div>

                    </div> 

                    <br>        

                    <!-- bancario -->
                    <div id="bancario" class="box_descuento" style="display: none;">

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <strong>Prestamo Bancario:</strong> 
                            </div>
                        </div>     
                        
                        <div class="row">

                        <div class="col-md-3 form-group mb-3">
                             <label for="banco">banco:</label>
                              <select class="form-control" id="banco" name="banco">
                                <option value="" selected disabled>-- seleccionar banco --</option>
                                @foreach ($bancos as $banco)
                                  <option value="{{ $banco->id }}">{{ $banco->nombre }}</option>
                                @endforeach
                              </select>

                        </div>


                            <div class="col-md-3 form-group mb-3">
                                <label for="no_credito">Número de crédito del banco</label>
                                <input class="form-control" id="no_credito" name="no_credito" type="text" value="" placeholder="No."  />
                            </div>

                        </div>
                        
                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="monto">Monto Q.</label>
                                <input class="form-control" id="monto" name="monto" type="number" value="0.00" placeholder="Total"  />
                            </div>
                        </div>                    


                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="no_cuotas">Cantidad de cuotas</label>
                                <input class="form-control" id="no_cuotas" name="no_cuotas" type="number" value="0" placeholder="Total"  />
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="valor_cuota">Valor de cuota Q.</label>
                                <input class="form-control" id="valor_cuota" name="valor_cuota" type="number" value="0.00" placeholder="Total"  />
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="primera_cuota">Fecha de primer cuota</label>
                                <input type="date" name="primera_cuota" id="primera_cuota" class="w-100 form-control" value="" >
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="fecha_aprobado">Fecha de aprobación</label>
                                <input type="date" name="fecha_aprobado" id="fecha_aprobado" class="w-100 form-control" value="" >
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="fecha_debito">Fecha de débito</label>
                                 <select class="form-control" id="fecha_debito" name="fecha_debito" >
                                     <option selected disabled value=""> --- seleccionar --- </option>
                                         <option value="Quincena">Quincena</option>
                                         <option value="Fin de Mes">Fin de Mes</option>
                                         <option value="Quincena y Fin de Mes">Quincena y Fin de Mes</option>
                                 </select>
                            </div>

                        </div>

                    </div>




                    <!-- adelanto -->
                    <div id="adelanto" class="box_descuento" style="display: none;">

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <strong>Adelanto de Salario:</strong> 
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="monto">Monto Q.</label>
                                <input class="form-control" id="monto" name="monto" type="number" value="0.00" placeholder="Total"  />
                            </div>
                        </div>                    

                        <div class="row">

                            <div class="col-md-3 form-group mb-3">
                                <label for="fecha_debito">Fecha de débito</label>
                                 <select class="form-control" id="fecha_debito" name="fecha_debito" >
                                     <option selected disabled value=""> --- seleccionar --- </option>
                                         <option value="Quincena">Quincena</option>
                                         <option value="Fin de Mes">Fin de Mes</option>
                                         <option value="Quincena y Fin de Mes">Quincena y Fin de Mes</option>
                                         <option value="Bono 14">Bono 14</option>
                                         <option value="Aguinaldo">Aguinaldo</option>
                                 </select>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="detalle">Detalle Legal</label>
                                <textarea class="form-control" style="height: 150px;" id="detalle" name="detalle" placeholder="Detalle..."  ></textarea>
                            </div>
                        </div>


                    </div>




                    <!-- manutencion -->
                    <div id="manutencion" class="box_descuento" style="display: none;">

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <strong>Manutención:</strong> 
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="monto">Monto Mensual Q.</label>
                                <input class="form-control" id="monto" name="monto" type="number" value="0.00" placeholder="Total"  />
                            </div>


                            <div class="col-md-3 form-group mb-3">
                                <label for="fecha_debito">Fecha de débito</label>
                                 <select class="form-control" id="fecha_debito" name="fecha_debito" >
                                     <option selected disabled value=""> --- seleccionar --- </option>
                                         <option value="Quincena">Quincena</option>
                                         <option value="Fin de Mes">Fin de Mes</option>
                                         <option value="Quincena y Fin de Mes">Quincena y Fin de Mes</option>
                                 </select>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="detalle">Detalle Legal</label>
                                <textarea class="form-control" style="height: 150px;" id="detalle" name="detalle" placeholder="Detalle..."  ></textarea>
                            </div>
                        </div>

                    </div>



                    <!-- embargo -->
                    <div id="embargo" class="box_descuento" style="display: none;">

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <strong>Embargo:</strong> 
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="no_credito">Número de embargo</label>
                                <input class="form-control" id="no_credito" name="no_credito" type="text" value="" placeholder="No."  />
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="monto">Monto Q.</label>
                                <input class="form-control" id="monto" name="monto" type="number" value="0.00" placeholder="Total"  />
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="no_cuotas">Cantidad de cuotas</label>
                                <input class="form-control" id="no_cuotas" name="no_cuotas" type="number" value="0" placeholder="Total"  />
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="valor_cuota">Valor de cuota Q.</label>
                                <input class="form-control" id="valor_cuota" name="valor_cuota" type="number" value="0.00" placeholder="Total"  />
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="primera_cuota">Fecha de primer cuota</label>
                                <input type="date" name="primera_cuota" id="primera_cuota" class="w-100 form-control" value="" >
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="fecha_debito">Fecha de débito</label>
                                 <select class="form-control" id="fecha_debito" name="fecha_debito" >
                                     <option selected disabled value=""> --- seleccionar --- </option>
                                         <option value="Quincena">Quincena</option>
                                         <option value="Fin de Mes">Fin de Mes</option>
                                         <option value="Quincena y Fin de Mes">Quincena y Fin de Mes</option>
                                 </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="detalle">Detalle Legal</label>
                                <textarea class="form-control" style="height: 150px;" id="detalle" name="detalle" placeholder="Detalle..."  ></textarea>
                            </div>
                        </div>

                    </div>



                    <!-- otros -->
                    <div id="otros" class="box_descuento" style="display: none;">

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <strong>Otros:</strong> 
                            </div>
                        </div>     
                        

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="nombre">Nombre de Descuento</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" value="" placeholder="Nombre"  />
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="monto">Monto Q.</label>
                                <input class="form-control" id="monto" name="monto" type="number" value="0.00" placeholder="Total"  />
                            </div>
                        </div>                    


                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="no_cuotas">Cantidad de cuotas</label>
                                <input class="form-control" id="no_cuotas" name="no_cuotas" type="number" value="0" placeholder="Total"  />
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="valor_cuota">Valor de cuota Q.</label>
                                <input class="form-control" id="valor_cuota" name="valor_cuota" type="number" value="0.00" placeholder="Total"  />
                            </div>

                            <div class="col-md-3 form-group mb-3">
                                <label for="primera_cuota">Fecha de primer cuota</label>
                                <input type="date" name="primera_cuota" id="primera_cuota" class="w-100 form-control" value="" >
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="fecha_debito">Fecha de débito</label>
                                 <select class="form-control" id="fecha_debito" name="fecha_debito" >
                                     <option selected disabled value=""> --- seleccionar --- </option>
                                         <option value="Quincena">Quincena</option>
                                         <option value="Fin de Mes">Fin de Mes</option>
                                         <option value="Quincena y Fin de Mes">Quincena y Fin de Mes</option>
                                 </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="detalle">Detalle Legal</label>
                                <textarea class="form-control" style="height: 150px;" id="detalle" name="detalle" placeholder="Detalle..."  ></textarea>
                            </div>

                        </div>

                    </div>



                </div>
            </div>

            <div class="col-md-12">
                <input type="hidden" id="estado" name="estado" value="No Activo">

                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Crear Descuento
                </button>
            </div>

        </div>

    </div>

</form>


<script type="text/javascript">


$(document).ready(function() {

    $('#id_empresa').on('change', function() {


        $('#id_colab').html('');

        id_empresa = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_colaboradores')}}',
           data:{
             id_empresa: id_empresa,
           },
           success:function(data) {

                $('#id_colab').html(data);     
           }
        });

    });



    $('#tipo').change(function() {

        val = $(this).val();

        elemento = "#"+val;

        $(".box_descuento").hide();

        // $(".box_descuento input").removeAttr("required");

        $(".box_descuento input").attr("required", false);

        $( elemento ).fadeIn();

        inputs = elemento+' input';

        $(inputs).attr("required", true);

    });



});


</script>

@endsection
