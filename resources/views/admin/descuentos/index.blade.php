@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Listado de Descuentos</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">

        <div class="card-header">
            <h3 class="card-title">
                <a href="{{ url('/descuentos/create') }}">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Crear Descuento
                </a>
            </h3>


        </div>

            <div class="card-body">


              <form id="form_filtro" action="" method="get">
                @csrf

              <div class="row">
                  <div class="col-md-3">
                    Empresa:<br>
                      <select class="form-control" id="id_empresa" name="id_empresa" required>
                        <option value="" selected disabled>-- seleccionar empresa --</option>
                        @foreach ($empresas as $k => $v)
                          <option value="{{$v[0]}}" {{(isset($request->id_empresa)&&$v[0]==$request->id_empresa?'selected':'')}}>{{$v[1]}}</option>
                        @endforeach
                      </select>
                  </div>


                  <div class="col-md-3">
                    Colaborador:<br>
                      <select class="form-control" id="id_colab" name="id_colab">
                        {!! $colaboradores !!}
                      </select>
                  </div>


                  <div class="col-md-3">
                      Descuento:<br>
                      <select class="form-control" id="tipo" name="tipo">
                          <option selected value=""> --- todos --- </option>
                          <option value="bancario" {{ ($request->tipo=='bancario'?'selected':'') }} >Préstamo Bancario</option>
                          <option value="adelanto" {{ ($request->tipo=='adelanto'?'selected':'') }} >Adelanto de Salario</option>
                          <option value="manutencion" {{ ($request->tipo=='manutencion'?'selected':'') }} >Manutención</option>
                          <option value="embargo" {{ ($request->tipo=='embargo'?'selected':'') }} >Embargo</option>
                          <option value="otros" {{ ($request->tipo=='otros'?'selected':'') }} >Otros</option>
                      </select>
                  </div>

                  <div class="col-lg-3">
                      <button type="submit" form="form_filtro" value="filtrar" class="btn btn-primary text-white btn-rounded" style="margin-top: 20px;">Filtrar</button>
                  </div>

              </div>

              </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
    		  <h3 class="card-title  mb-0">
            Colaboradores
          </h3>
    		</div>
        <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


          <div class="table-responsive">

            <table id="asignar_colaboradores_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>Código</th>
                  <th>Colaborador</th>                  
                  <th>Descuento</th>
                  <th>Monto</th>
                  <th>Saldo</th>
                  <th>Estado</th>
                  <th>Detalle</th>
                  
                </tr>
              </thead>
              <tbody>
                @if(!empty($descuentos))
                  @foreach ($descuentos as $descuento)
                    @php

                        $colabInfo = $modelo::find($descuento->id_colab);;

                    @endphp
                    
                      <tr>

                        <td >{{ $colabInfo->id }}</td>
                        <td >{{ $colabInfo->nombres }} {{ $colabInfo->apellidos }}
                          <br><small><i>{{ $colabInfo->puesto }}</i></small>
                        </td>
                        <td>
                            {{ $descuento->tipo }}
                        </td>

                        <td>
                            Q.{{ $descuento->monto }}
                        </td>
                        <td>
@if ($descuento->tipo!=='manutencion')
                            Q.{{ $descuento->saldo }}
@endif
                        </td>
                        <td>
                          @if(!empty($descuento->estado))
                            <span class="badge badge-pill {{($descuento->estado=='Activo'?'badge-success':'badge-danger')}} m-2">
                            {{ $descuento->estado }}</span>
                          @else
                            No procesado
                          @endif
                        </td>

                        <td class="td-actions text-left" style="font-size: 20px;">
                          <a  href="{{ url('descuentos') }}/{{ $descuento->id }}" title="Ver detalle" target="_blank">
                              <i data-feather="eye"></i>
                          </a>
                        </td>

                      </tr>
                  @endforeach
                @endif
					    </tbody>
           </table>


          </div>


        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">


$(function() {

    $('#id_empresa').on('change', function() {


        $('#id_colab').html('');

        id_empresa = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_colaboradores')}}',
           data:{
             id_empresa: id_empresa,
           },
           success:function(data) {

                $('#id_colab').html(data);     
           }
        });

    });


});


</script>
<style media="screen">
  small{
    font-weight:bold;
  }
</style>
@endsection

@section('bottom-js')
    <script>
        $('#asignar_colaboradores_table').DataTable();
    </script>
@endsection
