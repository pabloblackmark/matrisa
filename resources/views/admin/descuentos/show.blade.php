@extends('admin.layouts.master')

@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{ url('descuentos') }}?id_empresa={{ $request->id_empresa }}&id_colab={{ $request->id_colab }}">Listado descuentos</a> </h1>
    <ul>
        <li>Detalle de Descuento</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Datos del empleado</div>
      </div>
      <div class="card-body">
          <div class="row">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

            @php

            $total = 0;

            $total = number_format($total,2,'.',',');

            @endphp
            <div class="col-3">
                <label  style="font-weight:bold;">Código:</label>
                {{ $colabInfo->id }}<br>
                <label  style="font-weight:bold;">Nombre: </label>
                {{ $colabInfo->nombres.' '.$colabInfo->apellidos }}<br>
                <label  style="font-weight:bold;">Estado: </label>
                {{ $colabInfo->estatus}}<br>
                <label  style="font-weight:bold;">Puesto: </label>
                {{ $colabInfo->puesto }}<br> 

                <label  style="font-weight:bold;">Descuento: </label>
                <spam style="text-transform: capitalize;">{{ $descuento->tipo }}</spam><br>
                <label  style="font-weight:bold;">Detalle: </label>
                <spam style="text-transform: capitalize;">{{ $descuento->detalle }}</spam><br>


                <label  style="font-weight:bold;">Debiar en: </label>
                {{ $descuento->fecha_debito }}<br>


                <label  style="font-weight:bold;">Estado Descuento: </label>
                <span class="badge badge-pill {{($descuento->estado=='Pagado'?'badge-success':'badge-danger')}} m-2">
                            {{ $descuento->estado }}</span><br>                              

            </div>
            <div class="col-3">

@if ($descuento->tipo=='embargo')

              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">
              No. de Embargo: </label>
              Q.{{ $descuento->no_credito }}
              <br>

@endif


              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">
              Monto: </label>
              Q.{{ $descuento->monto }}
              <br>

@if ($descuento->tipo!=='manutencion')         
              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">
              Cuota: </label>
              Q.{{ $descuento->valor_cuota }}
              <br>

              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">
              No. de Cuotas: </label>
              {{ $descuento->no_cuotas }}
              <br>
              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">
              Cuota Actual: </label>
@if ($descuento->fecha_debito == 'Quincena y Fin de Mes')              
              {{ $descuento->cuota_actual/2 }}
@else
              {{ $descuento->cuota_actual }}
@endif
              <br>

              @if ($descuento->cuota_actual==0)

              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">
              Primer Cuota: </label>
              {{ date_format(date_create($descuento->primera_cuota), 'd-m-Y') }}
              <br>

              @endif
@endif

            </div>

            <div class="col-3">

@php
   if ($descuento->saldo==0) {
        $color_saldo = 'gray';
   } else {
        $color_saldo = 'red';
   }

@endphp

@if($descuento->tipo!=='manutencion')
              <h4  style="font-weight:bold; color: {{ $color_saldo }}">Saldo:
              Q.{{ $descuento->saldo }}
              </h4>
@endif

            </div>


            <div class="col-4">
              <br><br>

              <form id="form_descuento" action="{{ url('update_descuento')}}" method="post" onsubmit="event.preventDefault(); return changeLog();">
                <input type="hidden" name="id_descuento" value="{{ @$descuento->id }}">

                @csrf

                <label for="" style="color: green;">Aprobado</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Activo"
                {{(!empty(@$descuento->estado)&&@$descuento->estado=='Activo'||!empty(@$descuento->estado)&&@$descuento->estado==''?'checked':'')}}>
                <label for="" style="color: red;">Denegado</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Denegado"
                {{(!empty(@$descuento->estado)&&@$descuento->estado=='Denegado'||!empty(@$descuento->estado)&&@$descuento->estado==''?'checked':'')}}>

              <br>

                <input type="submit" id="bt_actualizar" class="btn btn-success m-1" value="Actualizar">

                <br>

              </form>

            </div>



          </div>
      </div>
  </div>




 </div>
</div>



<style>
.ul-widget-s7::before{
    left: 20.3% !important;
}
</style>



  <div class="card o-hidden mb-4">
    <div class="card-header bg-transparent">
        <div class="card-title mb-0">Detalle</div>
    </div>

    <div class="card-body">
      <div class="row">
        <div class="col-12" style="overflow-x:scroll">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Pago</th>
@if ($descuento->tipo!=='manutencion')               
                <th>Cuota</th>
                <th>Saldo</th>
@endif
              </tr>
            </thead>
            <tbody>

          @foreach($log_descuento AS $desc)


                <tr >
                  <td>
                      {{ date_format(date_create($desc->fecha), 'd-m-Y') }}
                  </td>


                  <td>
                      {{ $desc->pago }}
                  </td>

@if ($descuento->tipo!=='manutencion') 
                  <td>
                  @if ($descuento->fecha_debito == 'Quincena y Fin de Mes')   
                      {{ $desc->no_cuota/2 }}
                  @else
                      {{ $desc->no_cuota }}
                  @endif
                  </td>
                  <td>
                      {{ $desc->saldo }}
                  </td>
@endif
                </tr>

          @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>


  </div>



<div class="row">

    <div class="col-lg-8 col-xl-8 mb-4 offset-2">
        <div class="card">
            <div class="card-body">

                <div class="ul-widget__head __g-support v-margin">
                    <div class="ul-widget__head-label">
                        <h3 class="ul-widget__head-title">Últimos cambios</h3>
                    </div>
                </div>

                <div class="ul-widget__body">

                    @foreach($data_changelog as $d)
                        <div class="ul-widget-s7">
                            <div class="ul-widget-s7__items" style="width:100%;">
                                <span class="ul-widget-s7__item-time" style="width:19%;">{{$d->created_at->format('d/m/Y H:i')}}</span>

                                <div class="ul-widget-s7__item-circle" style="width:11%;">
                                    <p class="badge-dot-warning ul-widget7__big-dot"></p>
                                </div>

                                <div class="ul-widget-s7__item-text" style="width:70%;">
                                    Edición:
                                    {{$d->comment}}
                                    en
                                    <span class="badge badge-pill badge-primary m-2">{{$d->element}}</span>
                                    por
                                    <span class="badge badge-pill badge-success m-2">{{$d->user}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <br>
            </div>
        </div>
    </div>

</div>



<script type="text/javascript">
  

  function changeLog(){
      Swal.fire({
          title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                    <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                </div>`,
          html:`
               <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                   <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                       <div class="modal-body">
                           <div class="row">
                               <label class="col-form-label">Comentario</label>
                               <textarea id="changeLogComment" class="form-control"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               `,
           preConfirm: function(){
            $('#bt_actualizar').hide();

               let val = $('#changeLogComment').val(),
                   cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`;
             $('#form_descuento').append(cad).attr('onsubmit', '').submit();
             return true
         }
      })
  }
  
</script>


<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
h5{
  font-weight: bold;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
hr{

}
</style>
@endsection

@section('page-js')

@endsection

@section('bottom-js')


@endsection
