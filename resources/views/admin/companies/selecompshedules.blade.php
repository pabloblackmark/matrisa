@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Reporte colaboradores por empresas</h1>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">
            <div class="card-body">
              <div class="row">
                  <div class="col-md-12">
                      <select data-placeholder="Colaborador" class="chosen-select" tabindex="6" onchange="selectMe(this)" name="idColab">
                        <option value="">--elegir empresa--</option>
                        @foreach ($companies as $k => $v)
                          <option value="{{$v[0]}}" {{(isset($idEmp)&&$v[0]==$idEmp?'selected':'')}}>{{$v[1]}}</option>
                        @endforeach
                      </select>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
    		  <h3 class="card-title  mb-0">
            Colaboradores en esta empresa
          </h3>
    		</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="asignar_colaboradores_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th width="15%">Codigo empleado</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Puesto</th>
                  <th>Ver reporte</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($colabs))
                  @foreach ($colabs as $value)
                    @php $perms=array();
                    // dd($value  );
                    $colabInfo = $value->getColabsInfo()->first();
                    //$colabInfoPuesto = $value->getColabsInfoPuesto()->first();
                    @endphp
                    @if(!empty($colabInfo))
                      <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td >{{$value->idColab}}</td>
                        <td >{{$colabInfo->nombres}}</td>
                        <td >{{$colabInfo->apellidos}}</td>
                        <td >{{$colabInfo->puesto}}</td>
                        <td class="td-actions  text-left" style="font-size: 20px;">
                          <a  href="{{ route('admin.colabCompanyWork', ['id' => $value->idColab,'idComp'=>$value->idCompany]) }}"  title="">
                              <i data-feather="eye"></i>
                          </a>
                        </td>
                      </tr>
                    @endif
                  @endforeach
                @endif
					    </tbody>
           </table>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  function selectMe(estu){
    let wind = '{{route('admin.workdone.show','IDS')}}';
      window.location=wind.replace('IDS',estu.value);
  }
</script>

@endsection

@section('bottom-js')
    <script>
        $('#asignar_colaboradores_table').DataTable();
    </script>
@endsection
