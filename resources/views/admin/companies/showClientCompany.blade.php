@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"slct",tl:"Cliente",nm:"idClient",vl:@json($clients),elv:"0"},
         2:{type:"hddn",vl:"{{$idm}}",nm:"idCompany"},
       };
    valdis={clase:"red",text:1};

</script>

<div class="breadcrumb">
    <h1 class="mr-2">Empresas</h1>
    <ul>
        <li><a href="{{url('/company')}}">Inicio</a></li>
        <li>Clientes</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12 mb-2">
        <h1>{{$nameCompany}}</h1>
    </div>
</div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
					<h3 class="card-title">
              <a class="btn btn-outline-danger btn-icon m-1"
                      href="{{route('admin.company.index')}}">
                 <i class="text-20 i-Back1"></i> </a>
              <a class="btn btn-outline-info m-1"
                  onclick="newfloatv2(kad,null,'{{route('admin.clientscompany.store')}}');">
                <i class="ion-ios7-plus-outline "></i>
                &nbsp;&nbsp;Agregar cliente
              </a>
          </h3>
				</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="asignar_clientes_table" class="display table table-striped table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Cliente</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti = $crite = []; @endphp
                @foreach ($data as $dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td > {{$dataRes->getClient()->first()->clientName}}</td>
                  <td class="td-actions text-left table-subtitle">
                    <a  href="{{ route('admin.colaboradoresClient', ['com_id' => $idm, 'cli_id' => $dataRes->idClient, 'assoc_id'=>$dataRes->id]) }}"
                        title="Colaboradores">
                        <i class="text-20 i-Business-Mens"></i>
                    </a>
                    <a  href="javascript:"
                        onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}','{{route('admin.clientscompany.destroy',$dataRes->id)}}');"
                        title="Borrar">
                        <i class="text-12" data-feather="trash"></i>
                    </a>

                  </td>
                  </tr>
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
          criterie = @json($crite);

    </script>

@endsection

@section('bottom-js')
    <script>
        $('#asignar_clientes_table').DataTable();
    </script>
@endsection
