@extends('admin.layouts.master')

@section('main-content')

@php  $creat=array();
  $colors = ["no registrado"=>"#a8cfff;","ausencia"=>'#f14848',"sin horario"=>'#a99944',"asistencia"=>'#38b6f5',
            "Permiso"=>'#ecffef',"Vacaciones"=>'#f3fbff',"Iggs"=>'#f3fbff',"descuento"=>'#b74949',"descuento_septimo"=>'#cc5c4a'];
@endphp
<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{route('admin.workdone.index')}}">Reporte colaboradores por empresas</a> </h1>
    <ul>
        <li>Reporte colaborador</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="">
        <a href="{{route('admin.workdone.show',$idCompany)}}">
          <i class="i-Arrow-Left"></i>
          Regresar</a>
    </div>
    <div class="card o-hidden mb-4">
      {{-- <div class="card-header bg-transparent">
          <div class="card-title mb-0">Datos del empleado</div>
      </div> --}}
      <div class="card-body">
          <ul class="nav nav-tabs" id="myTab2" role="tablist">
            <li class="nav-item"><a class="nav-link active" id="home-basic-tab" data-toggle="tab" href="#report" role="tab"
              aria-controls="homeBasic" aria-selected="true">Reporte de horas</a></li>
            <li class="nav-item"><a class="nav-link" id="profile-basic-tab" data-toggle="tab" href="#homeBasic"
                role="tab" aria-controls="profileBasic" aria-selected="false">Datos generales</a></li>
            <li class="nav-item"><a class="nav-link" id="profile-basic-tab" data-toggle="tab" href="#profileBasic"
              role="tab" aria-controls="profileBasic" aria-selected="false">Vacaciones</a></li>
            <li class="nav-item"><a class="nav-link" id="contact-basic-tab" data-toggle="tab" href="#contactBasic"
              role="tab" aria-controls="contactBasic" aria-selected="false">Ausencias</a></li>
          </ul>
          <div class="tab-content" id="myTab2Content">
            <div class="tab-pane fade active show" id="report" role="tabpanel" aria-labelledby="home-basic-tab">
              <div class="col-md-1 form-group mb-3" style="float:left;">
                
                <select class="form-control" onchange="filter();" id="year" >

                  @if(count($years)>0)
                    @foreach($years AS $vls)
                      <option value="{{$vls->year}}" {{($yr==$vls->year?'selected':'')}}>{{$vls->year}}</option>
                    @endforeach
                  @else
                    <option value="{{date('Y')}}" selected>{{date('Y')}}</option>
                  @endif
                </select>
              </div>
              <div class="col-md-1 form-group mb-3" style="float:left;">

                @php $yearTop = (date("Y")==$yr?date("W"):52); @endphp
                <select class="form-control" onchange="filter();" id="week">
                  @if(count($years)>0)
                    @foreach(range($yearTop,1) AS $vls)
                      <option value="{{$vls}}" {{($wk==$vls?'selected':'')}}>{{$vls}}</option>
                    @endforeach

                  @else
                    <option value="{{date("W")}}">{{date("W")}}</option>
                  @endif
                </select>
              </div>
              <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Dia laborado</th>
                    <th>Intermedio</th>
                    <th>Extras</th>
                    <th>Viáticos</th>
                    <th>Metas</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                    $counter=0;
                  @endphp
                  @foreach($data AS $valuesAssocs)
                    @foreach($valuesAssocs AS $valuesDays)
                        @foreach($valuesDays AS $ky=>$elems)
                          @php
                            $counter++ ;
                            // dd($elems);
                          @endphp
                        <form class="" action="{{route('admin.colabCompanyWorkSave',[$idColab,$idCompany])}}" method="post">
                        <tr class="form">
                          <td>{{$elems["dayWeek"]}}<br>
                            @php
                            $mes =  ["1"=>"Enero","2"=>"Febrero",
                                    "3"=>"Marzo","4"=>"Abril",
                                    "5"=>"Mayo","6"=>"Junio","7"=>"Julio",
                                    "8"=>"Agosto","9"=>"Septiembre","10"=>"Octubre",
                                    "11"=>"Noviembre","12"=>"Diciembre"
                                  ];
                                $fech = date("d/m/Y",strtotime($elems["dateDay"]));
                                  $fech = explode("/",$fech);
                            @endphp
                                   {{$fech[0]." ".$mes[(INT)$fech[1]]." ".$fech[2]}} <br>
                                     {{$elems["clientName"]}}<br>
                                     <i>{{(!empty($elems["puesto"])?$elems["puesto"]:'')}}</i>
                                      <br>
                                     <span class="badge badge-pill badge-info statusDay"
                                     style="background-color:{{isset($colors[strtolower($elems['estado'])])?$colors[strtolower($elems['estado'])]:'#47404f!important'}}">
                                     {{$elems['estado']}}
                                     @if(!empty($elems['descripDay']))
                                       <a href="javascript:" onclick="showDesc('{{$elems['descripDay']}}');"
                                       class="text-white"
                                       >(ver)</a>
                                     @endif
                                   </span> <br>
                                   {{--1 aprobado, 2 descontar, 3 descontar con séptimo--}}
                                   <label class="radio radio-outline-primary">
                                     <input type="radio" name="aproved" value="1" onclick="cleanForms(this,'false');"
                                     {{(!empty($elems['aproved'])&&$elems['aproved']==1?'checked':'')}}
                                     /><span>{{(!empty($elems['aproved'])&&$elems['aproved']==1?'Aprobado':'Aprobar')}}</span><span class="checkmark"></span>
                                   </label>
                                   <label class="radio radio-outline-primary">
                                     <input type="radio" name="aproved" value="2" onclick="cleanForms(this,'disabled');"
                                     {{(!empty($elems['aproved'])&&$elems['aproved']==2?'checked':'')}}
                                     /><span>{{(!empty($elems['aproved'])&&$elems['aproved']==2?'Descontado':'Descontar')}}</span><span class="checkmark"></span>
                                   </label>

                                   <label class="radio radio-outline-primary">
                                     <input type="radio" name="aproved"  value="3" onclick="cleanForms(this,'disabled');"
                                     {{(!empty($elems['aproved'])&&$elems['aproved']==3?'checked':'')}} class="septim"
                                     {{($septimo=='true'?'disabled':'')}}
                                     /><span>
                                       {{(!empty($elems['aproved'])&&$elems['aproved']==3?'Descontado con séptimo':'Descontar con séptimo')}}</span><span class="checkmark"></span>
                                   </label>
                                   @csrf
                                   @if($elems["estado"]!='no trabaja')
                                     <button type="button"
                                             class="btn btn-icon button"
                                             id="toast-success"
                                             onclick="formSubmit(this);"
                                     > <i data-feather="save"></i> </button>
                                   @endif
                            @if(empty($elems['shedlId']))
                              @if(strtolower($elems["estado"])=='ausencia'||strtolower($elems["estado"])!='sin horario')
                                <input type="hidden" name="userId" Omit="true" value="{{$idUser}}">
                                <input type="hidden" name="dayWeek" Omit="true" value="{{$elems["dayWeek"]}}">
                                <input type="hidden" name="clientName"Omit="true" value="{{$elems["clientName"]}}">
                                <input type="hidden" name="idBranch" Omit="true" value="{{$elems["idBranch"]}}">
                                <input type="hidden" name="idClient" Omit="true" value="{{$elems["idClient"]}}">
                                <input type="hidden" name="idLink" Omit="true" value="{{$elems["idLink"]}}">
                                <input type="hidden" name="idDay" Omit="true" value="{{$elems["idDay"]}}">
                                <input type="hidden" name="idArea" Omit="true" value="{{$elems["idArea"]}}">
                                <input type="hidden" name="idCompany" Omit="true" value="{{$elems["idCompany"]}}">
                                <input type="hidden" name="entry"  value="{{$elems["entry"]}}">
                                <input type="hidden" name="exit"  value="{{$elems["exit"]}}">
                                <input type="hidden" name="dateDay" Omit="true" class="dateDay" value="{{$elems["dateDay"]}}">

                              @elseif($elems["estado"]=='Sin horario')
                                <input type="hidden" name="clientName" Omit="true" value="{{$elems["clientName"]}}">
                                <input type="hidden" name="idBranch" Omit="true" value="{{$elems["idBranch"]}}">
                                <input type="hidden" name="idClient" Omit="true" value="{{$elems["idClient"]}}">
                                <input type="hidden" name="idLink" Omit="true" value="{{$elems["idLink"]}}">
                                <input type="hidden" name="idArea" Omit="true" value="{{$elems["idArea"]}}">
                                <input type="hidden" name="idCompany" Omit="true" value="{{$elems["idCompany"]}}">
                                <input type="hidden" name="dateDay" Omit="true" class="dateDay" value="{{$elems["dateDay"]}}">
                              @endif
                             @endif
                             <input type="hidden" name="tasks" id="tasks{{$counter}}" value="">
                          </td>
                          <td>
                              <input type="hidden" name="idPer" class="idPer" value="{{$elems["id"]}}">
                              <span style="font-size:10px;">Entrada</span> <br>
                              <input type="time" name="hourEnter" onchange="changColor(this)"
                              class="hourEnter"
                              {{(empty($elems["hourEnter"])?'style=color:#c5c5c5':'')}}
                                value="{{!empty($elems["hourEnter"])&&$elems["hourEnter"]!='00:00:00'?date("H:i",strtotime($elems["hourEnter"])):''}}"
                                placeholder="00:00">
                              <br>
                              <span style="font-size:10px;">Atraso:
                              {{!empty($elems["outTimeEnter"])?date("H:i",strtotime($elems["outTimeEnter"])):''}}</span>
                              <br>
                              <span style="font-size:10px;">Salida</span> <br>
                              <input type="time" name="hourExit"  onchange="changColor(this)"
                              class="hourExit"
                                {{(empty($elems["hourExit"])?'style=color:#c5c5c5':'')}}
                                value="{{!empty($elems["hourExit"])?date("H:i",strtotime($elems["hourExit"])):''}}"
                                placeholder="00:00">
                              <br>
                              <span style="font-size:10px;">Horas laboradas:
                              {{!empty($elems["hourDay"])?date("H:i",strtotime($elems["hourDay"])):''}}</span>
                          </td>

                          <td>
                            <span style="font-size:10px;">Salida</span> <br>
                            <input type="time" name="hourExitBetw" onchange="changColor(this)"
                            {{(empty($elems["hourExitBetw"])?'style=color:#c5c5c5':'')}}
                            placeholder="00:00"
                            value="{{!empty($elems["hourExitBetw"])?date("H:i",strtotime($elems["hourExitBetw"])):''}}">
                           <br>
                           <span style="font-size:10px;">Entrada</span> <br>
                           <input type="time" name="hourEnterBetw"  onchange="changColor(this)"
                           {{(empty($elems["hourEnterBetw"])?'style=color:#c5c5c5':'')}}
                           placeholder="00:00"
                           value="{{!empty($elems["hourEnterBetw"])?date("H:i",strtotime($elems["hourEnterBetw"])):''}}">
                          </td>
                          <td>
                            <span style="color:black;font-size:10px;">Diurna</span> <br>
                            <input type="text" name="extraHourDay" {{(empty($elems["extraHourDay"])?'style=color:#c5c5c5':'')}}
                            onchange="changColor(this)"
                            value="{{!empty($elems["extraHourDay"])?date("H:i",strtotime($elems["extraHourDay"])):''}}"
                            onkeyup="checkTime(this);" placeholder="00:00" class="w-30"
                            >
                            <br>
                            <span style="color:black;font-size:10px;">Nocturna</span><br>
                            <input type="text" name="extraHourNight"  {{(empty($elems["extraHourNight"])?'style=color:#c5c5c5':'')}}
                            onchange="changColor(this)"
                            value="{{(!empty($elems["extraHourNight"])?date("H:i",strtotime($elems["extraHourNight"])):'')}}"
                            onkeyup="checkTime(this);" placeholder="00:00" class="w-30"
                            >
                            <br>
                            <span style="color:black;font-size:10px;">Mixta</span><br>
                            <input type="text" name="extraHourMix"  {{(empty($elems["extraHourMix"])?'style=color:#c5c5c5':'')}}
                            onchange="changColor(this)"
                            value="{{(!empty($elems["extraHourMix"])?date("H:i",strtotime($elems["extraHourMix"])):'')}}"
                            onkeyup="checkTime(this);" placeholder="00:00" class="w-30"
                            >
                            <br>
                            <span style="color:black;font-size:10px;">Especial</span><br>
                            <input type="text" name="extraHourEspecial"  {{(empty($elems["extraHourEspecial"])?'style=color:#c5c5c5':'')}}
                            onchange="changColor(this)"
                            value="{{(!empty($elems["extraHourEspecial"])?date("H:i",strtotime($elems["extraHourEspecial"])):'')}}"
                            onkeyup="checkTime(this);" placeholder="00:00" class="w-30"
                            >
                          </td>
                          <td>
                            <label style="font-weight:bold">Normales</label>
                            <input type="number" name="viatics" id="viatics"
                                   value="{{!empty($elems["viatics"])?$elems["viatics"]:0}}" style="width:25%"
                                   onchange="calcViatics(this);" >
                            <span style="color:black;font-size:10px;" class="viaticsSpan">Total: {{!empty($elems["viatics"])?'Q.'.($elems["viatics"]*(!empty($colabInfo->viaticos)?$colabInfo->viaticos:1)):0}}</span>
                            <br>
                            <hr class="mb-2 mt-2">
                              <label style="font-weight:bold">Moviles</label>
                              <input type="number" name="viaticsMovil" id="viaticsMovil"
                                     value="{{!empty($elems["viaticsMovil"])?$elems["viaticsMovil"]:0}}" style="width:25%"
                                     onchange="calcViaticsMovil(this);">
                            <span style="color:black;font-size:10px;" class="viaticsMovilSpan">Total: {{!empty($elems["viaticsMovil"])?'Q.'.($elems["viaticsMovil"]*(!empty($colabInfo->movil)?$colabInfo->movil:1)):0}}</span>
                            <hr class="mb-2 mt-2">
                              <label style="font-weight:bold">Extras</label>
                              Q.<input type="number" name="viaticsAdded"  id="viaticsAdded"
                                       value="{{!empty($elems["viaticsAdded"])?$elems["viaticsAdded"]:0}}" style="width:25%">
                            <input type="hidden" name="viaticsDesc" id="txtViaticExt{{$counter}}" class="txtViaticExt" value="">
                            <input type="button"  class="btn btn-linkedin btn-icon btn-sm m-1" value="Razón" onclick="notifyViatExt('txtViaticExt{{$counter}}','{{!empty($elems["viaticsDesc"])?$elems["viaticsDesc"]:''}}')">
                            <br>
                          </td>
                          <td>
                            @if(!empty($elems["id"]))
                              <span style="color:black;font-size:10px;">Completado: <span class="completTask">{{(isset($elems["quantityTask"])?$elems["quantityTask"]:0)}} / {{$elems["baseTasks"]}}</span></span> <br>
                              <span style="color:black;font-size:10px;">Adicional: <span class="addedTask">{{$elems["aditTask"]}}</span></span> <br>
                            @endif
                            {{-- @if(!isset($elems["baseTasks"])||strtolower($elems["estado"])=='ausencia'||strtolower($elems["estado"])=='permiso') --}}
                            @if(!empty($elems["tasks"]))
                              <a href="javascript:" title="Productividad"
                              onclick = "showProductiv({{$elems["tasks"]}},{{$elems["id"]}},'{{$elems["clientName"]}}','{{$elems["dayWeek"]}}',this,'tasks{{$counter}}');"
                              > <i data-feather="eye"></i></a>
                            @endif
                              {{-- {{$elems["tasks"]}} --}}
                            {{-- @endif --}}
                          </td>
                          {{-- <a href="javascript:"title="Referencia"> <i data-feather="book-open"></i></a> --}}
                          {{-- <td>
                          </td> --}}
                        </tr>
                        </form>
                      @endforeach
                    @endforeach
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Total</th>
                  </tr>
                </tfoot>
              </table>

            </div>
            <div class="tab-pane fade" id="homeBasic" role="tabpanel" aria-labelledby="home-basic-tab">
              <div class="float-left col-6">
                  <label  style="font-weight:bold;">Código:</label>
                  {{$colabInfo->id }}<br>
                  <label  style="font-weight:bold;">Nombre: </label>
                  {{$colabInfo->nombres.' '.$colabInfo->apellidos }}<br>
                  <label  style="font-weight:bold;">Estado: </label>
                  {{$colabInfo->estatus}}<br>
                <label  style="font-weight:bold;">Puesto: </label>
                {{$colabInfo->puesto }}<br>
              </div>
            </div>

            <div class="tab-pane fade" id="profileBasic" role="tabpanel" aria-labelledby="profile-basic-tab">
              <form class="" action="{{route('admin.saveVacations',[$idColab,$idCompany,$wk,$yr])}}" method="post">
                @csrf
                <div class="row">
                  <div class="col-3">
                    Dia de salida: <input type="date" name="datePerm" id="datePerm" class="form-control">
                  </div>
                  <div class="col-3">
                    Dia de entrada: <input type="date" name="dayBack" id="dayBack" class="form-control"><br>
                  </div>
                  <div class="col-4"> <br>
                    <input type="submit" onclick="return verficarFecha('vacaciones');" class="btn btn-info m-1" value="Guardar">
                  </div>
                </div>
              </form>
                <div class="table-responsive">

                <table class="table table-bordered ">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">Dia de salida</th>
                      <th scope="col">Dia de entrada</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(count($vacationes))
                      @foreach($vacationes AS $values)
                        <tr>

                          <td>{{date('d-m-Y', strtotime($values->datePerm))}}</td>
                          <td>{{date('d-m-Y', strtotime($values->datePerm. ' + '.($values->days-1).' days'))}}</td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane fade" id="contactBasic" role="tabpanel" aria-labelledby="contact-basic-tab">
              <form class="" action="{{route('admin.savePermission',[$idColab,$idCompany,$wk,$yr])}}" method="post">
                @csrf
                <div class="row">
                  <div class="col-8">
                    Titulo: <input type="text" name="typeOther" value="" class="form-control">
                  </div>
                  <div class="col-8">
                    Dia de salida: <input type="date" name="datePerm" id="dateOut" value="" class="form-control">
                  </div>
                  <div class="col-8">
                    Dia de entrada: <input type="date" name="dayBack" id="dateBack" value="" class="form-control"><br>
                  </div>
                  <div class="col-8">
                    Razon:
                    <textarea name="description" rows="8" cols="80" class="form-control"></textarea>
                  </div>
                  <div class="col-8"> <br>
                    <input type="submit" name="" onclick="return verficarFecha();"class="btn btn-info m-1" value="Guardar">
                  </div>
                </div>
              </form>
            </div>
          </div>

    </div>
  </div>

 </div>
</div>
<script type="text/javascript">
function verficarFecha(typos){
  let fecha1,fecha2;
  if (typos=='vacaciones'){
    fecha1 = '#datePerm';
    fecha2 = '#dayBack';
  }else{
    fecha1 = '#dateOut';
    fecha2 = '#dateBack';
  }
  let datesVerify = @json($fechasReservas);
  var dateFrom = $(fecha1).val().toString();
  var dateTo = $(fecha2).val().toString();
  var d1 = dateFrom.split("-");
  var d2 = dateTo.split("-");
  var from = new Date(d1[0], parseInt(d1[1]-1), d1[2]);  // -1 because months are from 0 to 11
  var to   = new Date(d2[0], parseInt(d2[1]-1), d2[2]);
  var estatus = true;
  for(var indix in datesVerify){
    var date1 = datesVerify[indix].split("-");
    var check1 = new Date(date1[0], parseInt(date1[1]-1), date1[2]);
    if(Date.parse(datesVerify[indix]) >= Date.parse(dateFrom) && Date.parse(datesVerify[indix]) <= Date.parse(dateTo)){
      // alert('d');
      estatus = false;
    }
  }
  if(estatus==false){
    alert("No es posible asignar esta fecha. Hay un evento en esa fecha");
  }
    // console.log(datesVerify);
  return estatus;
}
  function cleanForms(esto,enable){
    let formArr = [];
    let formD = $(esto).parent().parent().parent();
    let times = formD.find('input[type="time"]');
    let txts = formD.find('input[type="text"]');
    let hiddens = formD.find('input[type="hidden"]');
    let radios = formD.find('input[type="radio"]');
    let tempid= $('.idPer').val();
    let tempday= $('.dateDay').val();
    // console.log(numbrs);
    if(enable=='false'){
      $(times).each(function(){
        if($(this).attr("Omit")!='true')
          $(this).removeAttr('disabled');
      });
      $(txts).each(function(){
        if($(this).attr("Omit")!='true')
          $(this).removeAttr('disabled');
      });
      $(hiddens).each(function(){
        if($(this).attr("Omit")!='true')
          $(this).removeAttr('disabled');
      });
      formD.find('#viatics').removeAttr('disabled');
      formD.find('#viaticsMovil').removeAttr('disabled');
      formD.find('#viaticsAdded').removeAttr('disabled');
      formD.find('.txtViaticExt').removeAttr('disabled');
    }else{
      $(times).each(function(){
        if($(this).attr("Omit")!='true')
          $(this).attr('disabled','disabled');
      });
      $(txts).each(function(){
        if($(this).attr("Omit")!='true')
          $(this).attr('disabled','disabled');
      });
      $(hiddens).each(function(){
        if($(this).attr("Omit")!='true')
          $(this).attr('disabled','disabled');
      });
      formD.find('#viatics').attr('disabled','disabled');
      formD.find('#viaticsMovil').attr('disabled','disabled');
      formD.find('#viaticsAdded').attr('disabled','disabled');
      formD.find('.txtViaticExt').attr('disabled','disabled');
    }
    console.log(enable);

    formD.find('.idPer').attr(tempid);
    formD.find('.dateDay').attr(tempday);

    // formD.find('.addedTask').html('0');
    // formD.find('.completTask').html('0/0');
    // formD.find('.viaticsSpan').html('0');
    // formD.find('.viaticsMovilSpan').html('0');


  }
  function formSubmit(esto){
    let formArr = [];
    let formD = $(esto).parent().parent();
    let times = formD.find('input[type="time"]');
    let txts = formD.find('input[type="text"]');
    let hiddens = formD.find('input[type="hidden"]');
    let radios = formD.find('input[type="radio"]');
    let radTest = 0;
    // console.log(numbrs);
    $(radios).each(function(){
      if($(this).is(":checked")){
        let formObj = {};
          formArr[$(this).attr("name")] = $(this).val();
          radTest = $(this).val();
      }
    });
    // console.log(radTest,radios);
    $(times).each(function(){
      let formObj = {};
      if($(this).val()!=''){
        formArr[$(this).attr("name")] = $(this).val();
      }
    });
    $(txts).each(function(){
      let formObj = {};
      if($(this).val()!=''){
        formArr[$(this).attr("name")] = $(this).val();
      }
    });
    $(hiddens).each(function(){
      let formObj = {};
      if($(this).val()!=''){
        formArr[$(this).attr("name")] = $(this).val();
      }
    });
    if(radTest==1){

      formArr["viatics"] = formD.find('#viatics').val();
      formArr["viaticsMovil"] = formD.find('#viaticsMovil').val();
      formArr["viaticsAdded"] = formD.find('#viaticsAdded').val();
      formArr["txtViaticExt"] = formD.find('.txtViaticExt').val();
    }
    // $(numbrs).each(function(){
    //   if($(this).is(":checked")){
    //     let formObj = {};
    //     console.log($(this).val());
    //     formArr[$(this).attr("name")] = ($(this).val()==''?'0':$(this).val());
    //     // formArr.push(formObj);
    //   }
    // });
    // console.log(formArr);
    // console.log(JSON.stringify(Object.assign({}, formArr)));
    // toastr.success('Guardado');
    // console.log(formArr,$('.aproved').val());
    // console.log(radTest,$('.hourEnter').val(),''&&$('#hourExit').val());
    if(radTest==1&&formD.find('.hourEnter').val()!=''&&formD.find('.hourExit').val()!=''
      ||radTest==2||radTest==3){
        $.post('{{route('admin.colabCompanyWorkSave',[$idColab,$idCompany])}}',
              {"datos":JSON.stringify(Object.assign({},formArr)),"_token": "{{csrf_token()}}"},
            function(data){
              let tps = @json($colors);
              const datJson = JSON.parse(data);
              // const datJson = data;
              if(datJson.status=='true' || datJson.status==true){
                formD.find('.statusDay').html(datJson.typo).attr("style","background-color:"+tps[datJson.typo]+"!important");
                formD.find('.idPer').val(datJson.id);
                if(radTest==3){
                  $('.septim').attr("disable","disable");
                  location.reload();
                }
              }
              console.log(datJson);
        })
        .fail(function(data) {
          console.log(data);
          alert( "error" );
        });
    }else{
      alert('Debe ingresar una hora de entrada y una hora de salida');
    }

    // console.log(JSON.stringify(Object.assign({},formArr)));
  }
  function calcViatics(des){
    let viatics = {{!empty($colabInfo->viaticos)?$colabInfo->viaticos:1}};
    $(des).parent().find(".viaticsSpan").html('Q.'+viatics*des.value);
    // console.log(viatics*des.value);
  }
  function calcViaticsMovil(des){
    let viatics = {{!empty($colabInfo->movil)?$colabInfo->movil:1}};
    $(des).parent().find(".viaticsMovilSpan").html('Q.'+viatics*des.value);
    // console.log(viatics*des.value);
  }
  function notifyViatExt(esto,text){
    swal.fire({title:'<small>Agregar comentario viáticos extras</small>',
               html:'<div class="row"><div class="col-12"><textarea id="viaticExText" class="form-control">'+text+'</textarea></div><br>\
               <input type="button"  class="btn btn-primary btn-icon btn-sm m-1" value="Guardar" onclick="addCommentViatExtra(\''+esto+'\')">\
               </div>',
              showCloseButton: false,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function addCommentViatExtra(idapli){
    let idTodes = '#'+idapli;
    console.log(idTodes,$('#viaticExText').val());
    $(idTodes).val($('#viaticExText').val());
    Swal.close();
  }
  function filter(){
    if($('#week').val()!=''){
      window.location= '{{route('admin.colabCompanyWork',[$idColab,$idCompany])}}'+'/'+$('#week').val()+'/'+$('#year').val();
    }else{
      alert('Por favor eliga una semana');
    }
  }
  function showProductiv(produ,idTas,nmCl,day,estus,idto){
    console.log(produ);
    let listof =''
    for(let ll in produ){
      listof+='<tr><td>'+produ[ll].name+'</td><td> <input type="number" value="'+produ[ll].descript+'" name="altu['+produ[ll].ids+'_'+produ[ll].origin+']" style="width:25%" class="canti" idTask="'+produ[ll].ids+'"> / '+produ[ll].origin+'\
      </td></tr>';

    }
    swal.fire({title:'<small>Cliente:</small> '+nmCl+ '<small> Día: </small>'+day,
               html:'<div class="row"><div class="col-12">\
               <table class="table">\
                <tbody>'+listof+'<tr><td colspan="2">'+
                '<input type="button" value="Guardar" class="btn btn-raised btn-raised-primary m-1" onclick="getTsk(\''+idto+'\');">'
                +'</td></tr>\
                </tbody>\
               </table></div></div>',
              showCloseButton: false,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function getTsk(idTo){
    let finalT = [];
    $('.canti').each(function(){
      let arrk = {};
      arrk["quantity"] =$(this).val();
      arrk["idT"] = $(this).attr("idTask");
      finalT.push(arrk);
    });
    Swal.close();
    $('#'+idTo).val(JSON.stringify(finalT));
  }
  function showDesc(messa){
    swal.fire({title:'Descripción de permiso',
               html:'<div class="row"><div class="col-12">'+messa+'</div></div>',
              showCloseButton: true,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function changColor(estos){
    $(estos).css("color","black");
  }
  function checkTime(t){
    var v = t.value;
    if (v.match(/^\d{2}$/) !== null) {
        t.value = v + ':';
        t.value=t.value;
        return ;
    }
    // else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
    //     t = v + '/';
    //     t.value=t;
    //     return ;
    // }
  }
</script>
<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
hr{

}
</style>
@endsection

@section('page-js')



@endsection

@section('bottom-js')

@endsection
