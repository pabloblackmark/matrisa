@extends('admin.layouts.master')
@section('main-content')
@php
$depato[] = ['','--Seleccionar--'];
foreach(array_keys($deptos) AS $valde){
  $depato[] = [$valde,$valde];
}
@endphp
<script type="text/javascript">
var kad={
         0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Nombre cliente",nm:"clientName",elv:"0",nxt:6},
         2:{type:"txt",tl:"Nombre contacto",nm:"contactName",elv:"1",nxt:6},
         3:{type:"txt",tl:"Nombre factura",nm:"nombre_factura",elv:"2",nxt:6},
         4:{type:"txt",tl:"Nit",nm:"nit",elv:"3",nxt:6},
         5:{type:"txt",tl:"Observaciones",nm:"observaciones",elv:"4",nxt:12},

         6:{type:"txt",tl:"Correo",nm:"email",elv:"5",nxt:6},
         7:{type:"nbr",tl:"Credito",nm:"credit",elv:"6",nxt:6},
         8:{type:"txt",tl:"Telefono 1",nm:"phone1",elv:"7",nxt:6},
         9:{type:"txt",tl:"Telefono 2",nm:"phone2",elv:"8",nxt:6},
         10:{type:"slct",tl:"Departamento",nm:"depto",vl:@json($depato),elv:"9",nxt:6,add:"onchange=\"municipf(this.value)\""},
         11:{type:"slct",tl:"Municipio",nm:"municip",id:"munic",vl:@json($municipios),nxt:6,elv:"10"},
         12:{type:"txt",tl:"Direccion",nm:"addess",elv:"11"},

         13:{type:"nbr",tl:"VMA",nm:"vma",elv:"12",nxt:6},
         14:{type:"chkbxstyl",tl:"Venta ante sobregiro",nm:"venta_sobregiro",elv:"13",nxt:6, vl:[[1,""]]}
        };

    valdis={clase:"red",text:1};
function municipf(estu,elo){
  let dps = @json($deptos),ljo = '<option value="">--Elegir--</option>';
  console.log(elo);
  for(let lp of dps[estu]){
    ljo +='<option value="'+lp+'" '+(elo!==undefined&&elo===lp?'selected':'')+'>'+lp+'</option>';
  }
  $('#munic').html(ljo);
}
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Clientes</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
		    <h3 class="card-title">
                <a href="javascript:"class="" onclick="newfloatv2(kad);">
                  <i class="ion-ios7-plus-outline "></i>
                  &nbsp;&nbsp;Nuevo cliente
                </a>
            </h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="clientes_table" class="display table table-striped table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Cliente</th>
                  <th>Nit</th>
                  <th>Telefono 1</th>
                  <th>Telefono 2</th>
                  <th>Departamento</th>
                  <th>Municipio</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti=array(); @endphp
                @foreach ($data as $kyCol=>$dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td > {{$dataRes->clientName}}</td>
                  <td > {{$dataRes->nit}}</td>
                  <td > {{$dataRes->phone1}}</td>
                  <td > {{$dataRes->phone2}}</td>
                  <td > {{$dataRes->depto}}</td>
                  <td > {{$dataRes->municip}}</td>
                  <td class="td-actions text-left table-subtitle">
                    <a href="{{route('admin.clientsbranch.show',$dataRes->id)}}"class=""
                      title="Administrar sedes">
                      <i class="text-20 i-Clothing-Store"></i>
                    </a>

                    <a  href="javascript:"
                        title="Editar"
                        onclick="modifyfloat('{{$dataRes->id}}',kad,criteria,undefined,undefined,true);"
                        >
                        <i class="text-20"data-feather="edit-3"></i>
                    </a>

                    <a href="{{url('changeLog', ['clients', $dataRes->id])}}" title="Registro de cambios">
                        <i class="text-20"data-feather="clock"></i>
                    </a>

                    <a  href="{{url('archivos', ['id' => $dataRes->id_file_manager])}}"
                        title="Administar archivos">
                        <i class="text-20" data-feather="file"></i>
                    </a>

                    <a  href="javascript:"
                        title="Borrar"
                        onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                        <i class="text-12" data-feather="trash"></i>
                    </a>
                  </td>
                  </tr>
                  @php $criti[$dataRes->id] = [$dataRes->clientName,
                                               $dataRes->contactName,
                                               $dataRes->nombre_factura,
                                               $dataRes->nit,
                                               $dataRes->observaciones,
                                               $dataRes->email,
                                               $dataRes->credit,
                                               $dataRes->phone1,
                                               $dataRes->phone2,
                                               $dataRes->depto,
                                               $dataRes->municip,
                                               $dataRes->addess,
                                               $dataRes->vma,
                                               $dataRes->venta_sobregiro,
                                           ];
                                        @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
    </script>

@endsection

@section('bottom-js')
    <script>
        $('#clientes_table').DataTable();
    </script>
@endsection
