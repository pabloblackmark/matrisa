@extends('admin.layouts.master')
@section('main-content')
@php
$depato[] = ['','- - -'];
foreach($deptos AS $key => $val){
  $depato[] = [$key,$key];
  foreach($val as $key2 => $val2){
      $munic[] = [$val2, $val2];
  }
}
@endphp
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Nombre empresa",nm:"company",elv:"0",nxt:6},
         2:{type:"txt",tl:"Nombre representante legal",nm:"legalrepresent",elv:"1",nxt:6},
         4:{type:"txt",tl:"Nit",nm:"nit",elv:"2",nxt:6},
         5:{type:"txt",tl:"No. Patronal de IGSS",nm:"igss",elv:"3",nxt:6},
         6:{type:"txt",tl:"Telefono 1",nm:"phone1",elv:"4",nxt:6},
         7:{type:"txt",tl:"Telefono 2",nm:"phone2",elv:"5",nxt:6},
         8:{type:"slct",tl:"Departamento",nm:"depto",vl:@json($depato),elv:"6",nxt:6,add:"onchange=\"municipf(this.value)\""},
         9:{type:"slct",tl:"Municipio",nm:"municip",id:"munic",vl:@json($munic),elv:"7",nxt:6},
         10:{type:"txt",tl:"Correo",nm:"email",elv:"8"},
         11:{type:"txt",tl:"Direccion",nm:"addess",elv:"9",add:" rows=\"3\""},
         12:{type:"fl",tl:"Logotipo",nm:"logo",nxt:6},
         13:{type:"img",tl:"Imagen",elv:"10",add:" width=\"100\"",nxt:12},

         14:{type:"dtmpckr",tl:"Fecha de inicio",nm:"fecha_inicio",elv:"11",nxt:6},
         15:{type:"dtmpckr",tl:"Fecha de nacimiento",nm:"fecha_nacimiento",elv:"12",nxt:6},
         16:{type:"nbr",tl:"DPI",nm:"dpi",elv:"13",nxt:6},
         17:{type:"slct",tl:"Estado civil",nm:"estado_civil",elv:"14",nxt:6,vl:[[null, '- - -'], ["soltero", "Soltero"], ["casado", "Casado"], ["unido", "Unido"], ["viudo", "Viudo"], ["divorciado", "Divorciado"]]},
         18:{type:"slct",tl:"Género",nm:"genero",elv:"15",vl:[[null, '- - -'],['masculino', 'Masculino'],['femenino', 'Femenino']],nxt:4},
         19:{type:"txt",tl:"Nacionalidad",nm:"nacionalidad",elv:"16",nxt:4},
         20:{type:"txt",tl:"Vecino",nm:"vecino",elv:"17",nxt:4},

         21:{type:"nbr",tl:"Iggs patronal (%)",nm:"iggs_patronal",elv:"18",nxt:4},
         22:{type:"nbr",tl:"IRTRA (%)",nm:"irtra",elv:"19",nxt:4},
         23:{type:"nbr",tl:"Iggs laboral (%)",nm:"iggs_laboral",elv:"20",nxt:4},
         24:{type:"nbr",tl:"Intecap (%)",nm:"intecap",elv:"21",nxt:4},
         25:{type:"slct",tl:"Cálculo sobre",nm:"calculo_sobre",elv:"22",vl:[[null, '- - -'],['sueldo base', 'Sueldo base'],['sueldo total', 'Sueldo total']],nxt:8,add:"onchange=\"selectMe(this)\""},
         26:{type:"chckbx",tl:"",nm:"bono_extra",id:"bono_extra",elv:"23",vl:[[1, 'Bono extra']],add:'style="margin-top:10px;"',nxt:4},
         27:{type:"chckbx",tl:"",nm:"horas_extra",elv:"24",vl:[[1, 'Horas extra']],add:'style="margin-top:10px;"',nxt:4},
         28:{type:"chckbx",tl:"",nm:"productividad",elv:"25",vl:[[1, 'Productividad']],add:'style="margin-top:10px;"',nxt:4},


        };
        // 8:{type:"txt",tl:"Usuario",nm:"",elv:"6",add:" placeholder=\"Generado por el sistema\" disabled",nxt:6},
        // 9:{type:"txt",tl:"Contrasena",nm:"",elv:"7",add:" placeholder=\"Generado por el sistema\" disabled",nxt:6},
    valdis={clase:"red",text:1};
function municipf(estu,elo){
  let dps = @json($deptos),ljo = '<option value="">- - -</option>';
  // console.log(elo);
  for(let lp of dps[estu]){
    ljo +='<option value="'+lp+'" '+(elo!==undefined&&elo===lp?'selectedcompany':'')+'>'+lp+'</option>';
  }
  $('#munic').html(ljo);
}
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Empresas</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
					<h3 class="card-title">
            <a href="javascript:"class="" onclick="newfloatv2(kad);">
            <i class="ion-ios7-plus-outline "></i>
            &nbsp;&nbsp;Nueva empresa
          </a>
          </h3>
				</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="empresas_table" class="display table table-striped table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Empresa</th>
                  <th>Nit</th>
                  <th>Telefono 1</th>
                  <th>Telefono 2</th>
                  <th>Departamento</th>
                  <th>Correo</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti=array(); @endphp
                @foreach ($data as $kyCol=>$dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td > {{$dataRes->company}}</td>
                  <td > {{$dataRes->nit}}</td>
                  <td > {{$dataRes->phone1}}</td>
                  <td > {{$dataRes->phone2}}</td>
                  <td > {{$dataRes->depto}} <br> {{$dataRes->municip}}</td>
                  <td > {{$dataRes->email}}</td>
                  <td class="td-actions text-left table-subtitle">
                    <a href="{{route('admin.colaboradoresCompany',$dataRes->id)}}"title="Colaboradores" >
                      <i class="text-20 i-Business-Mens"></i>
                    </a>
                    {{-- <a href="{{route('admin.clientsjobs.show',$dataRes->id)}}"title="Colaboradores" >
                      <i class="text-20 i-Business-Mens"></i>
                    </a> --}}
                    <a href="{{route('admin.clientscompany.show',$dataRes->id)}}"title="Clientes" >
                      <i class="text-20 i-Address-Book"></i>
                    </a>
                    <a  href="javascript:"
                        onclick="modifyfloat('{{$dataRes->id}}',kad,criteria,undefined,undefined,true);" title="Editar">
                        <i class="text-20"data-feather="edit-3"></i>
                    </a>
                    {{-- <a href="{{url('workdone', ['id' => $dataRes->id])}}" title="Reporte de labores">
                        <i class="text-20"data-feather="clock"></i>
                    </a> --}}
                    <a href="{{url('changeLog', ['company', $dataRes->id])}}" title="Registro de cambios">
                        <i class="text-20"data-feather="clock"></i>
                    </a>
                    <a  href="{{url('archivos', ['id' => $dataRes->id_file_manager])}}"
                        title="Administar archivos">
                        <i class="text-20"data-feather="file"></i>
                    </a>
                    <a  href="javascript:"
                        onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');" title="Borrar">
                        <i class="text-12" data-feather="trash"></i>
                    </a>
                  </td>
                  </tr>
                  @php $criti[$dataRes->id] = [
                                   $dataRes->company,
                                   $dataRes->legalrepresent,
                                   $dataRes->nit,
                                   $dataRes->igss,
                                   $dataRes->phone1,
                                   $dataRes->phone2,
                                   $dataRes->depto,
                                   $dataRes->municip,
                                   $dataRes->email,
                                   $dataRes->addess,
                                   asset('storage/'.$dataRes->logo),

                                   $dataRes->fecha_inicio,
                                   $dataRes->fecha_nacimiento,
                                   $dataRes->dpi,
                                   $dataRes->estado_civil,
                                   $dataRes->genero,
                                   $dataRes->nacionalidad,
                                   $dataRes->vecino,

                                   $dataRes->iggs_patronal,
                                   $dataRes->irtra,
                                   $dataRes->iggs_laboral,
                                   $dataRes->intecap,
                                   $dataRes->calculo_sobre,
                                   $dataRes->bono_extra,
                                   $dataRes->horas_extra,
                                   $dataRes->productividad,

                               ]; @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
    </script>

@endsection

@section('bottom-js')
    <script>
        $('#empresas_table').DataTable();
        function selectMe(elm){
            let val = elm.value
            let target = document.getElementById("bono_extra")

            if (val == "sueldo base"){
                target.disabled = true;
                target.checked = false;
            }else{
                target.disabled = false;
                target.checked = true;
            }

            console.log()
        }
    </script>
@endsection
