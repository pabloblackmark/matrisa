@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Área",nm:"name",elv:"0"},
         2:{type:"txt",tl:"Descripción",nm:"description",elv:"1"},
         6:{type:"hddn",vl:"{{$idm}}",nm:"idBranch"},
       };
    valdis={clase:"red",text:1};

</script>

<div class="breadcrumb">
    <h1 class="mr-2">Clientes</h1>
    <ul>
        <li><a href="{{url("clients")}}">Inicio</a></li>
        <li><a href="{{url("clientsbranch", ['id' => $clientId])}}">Sede</a></li>
        <li>Área</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
          <div class="card-header">
  		    <h4>
                Cliente: <b>{{$clientName}}</b>
            </h4>
            <h3 style="margin: 10px;">
                Sede: <b>{{$branchName}}</b>
            </h3>
            <h3 class="card-title">
                <a href="javascript:"class="" onclick="newfloatv2(kad,null,'{{route('admin.clientsbrancharea.store')}}');">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nueva área
                </a>
            </h3>
  		</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="clientes_sedes_areas_table" class="table" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Área</th>
                  <th>Descripción</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti = $crite = []; @endphp
                @foreach ($data as $dataRes)
                  <tr>
                      <td>{{ $loop->index + 1 }}</td>

                  <td > {{$dataRes->name}}</td>
                  <td > {{$dataRes->description}}</td>

                  <td class="td-actions text-left table-subtitle">
                    <a  href="javascript:"
                        onclick="modifyfloat('{{$dataRes->id}}',kad,criteria,'literalUrl','{{route('admin.clientsbrancharea.update',$dataRes->id)}}');">
                        <i class="text-20"data-feather="edit-3"></i>
                    </a>
                    <a  href="javascript:"
                        onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                        <i class="text-12" data-feather="trash"></i>
                    </a>

                  </td>
                  </tr>
                  @php $criti[$dataRes->id] = [$dataRes->name,
                                               $dataRes->description]; @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
          criterie = @json($crite);

    </script>

@endsection


@section('bottom-js')
    <script>
        $('#clientes_sedes_areas_table').DataTable();
    </script>
@endsection
