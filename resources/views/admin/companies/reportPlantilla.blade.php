@extends('admin.layouts.master')

@section('main-content')

@php  $creat=array();
  $colors = ["ausencia"=>'#f14848',"sin horario"=>'#a99944',"asistencia"=>'#38b6f5',
            "Permiso"=>'#ecffef',"Vacaciones"=>'#f3fbff',"Iggs"=>'#f3fbff'];
@endphp
<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{ url('planillas') }}?id_empresa={{ $request->id_empresa }}&id_cliente={{ $request->id_cliente }}&id_sucursal={{ $request->id_sucursal }}&id_area={{ $request->id_area }}&inicio={{ $request->inicio }}&fin={{ $request->fin }}">Listado planillas</a> </h1>
    <ul>
        <li>Detalle de plantilla</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Datos del empleado</div>
      </div>
      <div class="card-body">
          <div class="row">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>

            @php

            $desde = date_create($request->inicio);
            $hasta = date_create($request->fin);

            $desde = date_format($desde, 'd-m-Y');
            $hasta = date_format($hasta, 'd-m-Y');


            $salario = $colabInfo->sueldo * $dias_laborados;

            // $igss = $salario*0.0483;
            // $igss =  number_format($igss, 2, '.', ',');

            $bono_extra = $colabInfo->bono_extra * $dias_laborados;
            $antiguedad = $colabInfo->antiguedad * $dias_laborados;
            $bono_ley = $colabInfo->bono_ley * $dias_laborados;



            // $igss = $salario * 0.0483;

            // calculo igss
            if ($config_empresa->calculo_sobre=='sueldo base') {

                $igss = $salario * ($config_empresa->iggs_laboral/100);

            } elseif ($config_empresa->calculo_sobre=='sueldo total') {

                if ($config_empresa->bono_extra==1) {
                    $total_calcular = $salario + $bono_extra + $antiguedad;
                } else {
                    $total_calcular = $salario + $antiguedad;
                }

                $igss = $total_calcular * ($config_empresa->iggs_laboral/100);
            }


            $descuentos = App\Http\Controllers\Admin\accounting\descuentosController::get_descuentos($request);

            // dd($descuentos);

            $desc_modulo = 0;

            foreach ($descuentos as $desc) {
                $desc_modulo += $desc['pago'];
            }



            $total = $salario + $bono_extra + $antiguedad + $bono_ley;


            if ($dias_laborados==7) {
                $tipo = 'semanal';
            } elseif ($dias_laborados==15) {
                $tipo = 'quincenal';
            } elseif ($dias_laborados==30) {
                $tipo = 'mensual';
            } else {
                $tipo = 'días';
            }


            $desc_dia = $discounts["disCount"] * $colabInfo->sueldo;
            $desc_septimo = $discounts["septCount"] * ($colabInfo->sueldo*2);

            $total_bonos = $bono_ley + $bono_extra + $antiguedad;
            $total_descuentos = $desc_dia + $desc_septimo + $igss + $desc_modulo;


            $total = $total - $total_descuentos;

            $array_planilla = array(
                                  'id_empresa'=>$request->id_empresa,
                                  'id_colab'=>$colabInfo->id,
                                  'id_cliente'=>$request->id_cliente,
                                  'id_sucursal'=>$request->id_sucursal,
                                  'id_area'=>$request->id_area,
                                  'fecha_inicio'=>$request->inicio,
                                  'fecha_fin'=>$request->fin,
                                  'tipo'=>$tipo,
                                  'dias'=>$dias_laborados,
                                  'sueldo'=>$salario,
                                  'total'=> $total,
                                  'bono_ley'=>$bono_ley,
                                  'bono_extra'=>$bono_extra,
                                  'bono_antiguedad'=>$antiguedad,
                                  'desc_dia'=>$desc_dia,
                                  'desc_septimo'=>$desc_septimo,
                                  'desc_igss'=>$igss,
                                  'descuentos'=>$descuentos,
                                  'total_bonos'=>$total_bonos,
                                  'total_descuentos'=>$total_descuentos
            );


            $json_planilla = json_encode($array_planilla);


            $bono_ley = number_format($bono_ley,2,'.',',');
            $bono_extra = number_format($bono_extra,2,'.',',');
            $antiguedad = number_format($antiguedad,2,'.',',');

            $total = number_format($total,2,'.',',');

            $desc_dia = number_format($desc_dia,2,'.',',');
            $desc_septimo = number_format($desc_septimo,2,'.',',');

            $salario = number_format($salario,2,'.',',');

            @endphp
            <div class="col-3">
                <label  style="font-weight:bold;">Código:</label>
                {{ $colabInfo->id }}<br>
                <label  style="font-weight:bold;">Nombre: </label>
                {{ $colabInfo->nombres.' '.$colabInfo->apellidos }}<br>
                <label  style="font-weight:bold;">Estado: </label>
                {{ $colabInfo->estatus}}<br>
                  <label  style="font-weight:bold;">Puesto: </label>
                  {{ $colabInfo->puesto }}<br>
                  <label  style="font-weight:bold;">Días laborados: </label>
                  {{ $dias_laborados }}
                  <br>
                  <label  style="font-weight:bold;">Tipo de Pago: </label>
                  {{ $tipo }}
                  <br>

                  <label  style="font-weight:bold;">Desde: </label>
                  {{ $desde }}
                  <br>
                  <label  style="font-weight:bold;">Hasta: </label>
                  {{ $hasta }}
                  <br>


            </div>
            <div class="col-3">
              <h5  style="font-weight:bold;">Salario: </h5>
              Q.{{ $salario }}
              <br><br>

              <h5>Bonos</h5>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Bono de ley: </label>
              Q.{{ $bono_ley }}
              <br>

              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Extra: </label>
              Q.{{ $bono_extra }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Antiguedad: </label>
              Q.{{ $antiguedad }}
              <br>

            </div>

            <div class="col-3">
              <h5>Descuentos</h5>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Descuentos dia: </label>
              Q.{{ $desc_dia }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Descuento septimo: </label>
              Q.{{ $desc_septimo }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">IGSS: </label>
              Q.{{ $igss }}
              <br>

@foreach ($descuentos as $descuento)
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold; text-transform: capitalize;">{{ $descuento['tipo'] }}:</label>
              Q.{{ $descuento['pago'] }}
              <br>
@endforeach
              <br>
                  <h4  style="font-weight:bold;">Total:
                  Q.{{ $total }}
                  </h4>


            </div>
            <div class="col-4">
              <br><br>

@if (@$payment->estado=='')
              <form id="form_planilla" action="{{route('admin.aproveplanilla')}}" method="post" onsubmit="event.preventDefault(); return changeLog();">
@else
              <form id="form_planilla" action="{{ url('update_planilla')}}" method="post" onsubmit="event.preventDefault(); return changeLog();">
                <input type="hidden" name="id_planilla" value="{{ $payment->id }}">
@endif

                @csrf
                <input type="hidden" name="datos_planilla" value="{{ $json_planilla }}">


                <label for="" style="color: green;">Procesar pago</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Procesado"
                {{(!empty($payment->estado)&&$payment->estado=='Procesado'||!empty($payment->estado)&&$payment->estado==''?'checked':'')}}>
                <label for="" style="color: red;">Denegar pago</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Denegado"
                {{(!empty($payment->estado)&&$payment->estado=='Denegado'||!empty($payment->estado)&&$payment->estado==''?'checked':'')}}>

              <div style="width: 200px;">
                  Fecha de Pago
                  <input type="date" name="fecha_pago" id="fecha_pago" class="w-100 form-control" value="{{ @$payment->fecha_pago }}" required>
              </div>
              <br>

@if (@$payment->estado=='')
                <input type="submit" class="btn btn-success m-1" value="Guardar">
@else
                <input type="submit" id="bt_actualizar" class="btn btn-success m-1" value="Actualizar">
@endif
                <br>


<!--
                <label for="" class="razon">Nota:</label>
                <textarea name="nota" class="form-control razon" rows="8" cols="80">{{(!empty($payment->nota)?$payment->nota:'')}}</textarea>

-->

              </form>

            </div>
          </div>
      </div>
  </div>




 </div>
</div>



<style>
.ul-widget-s7::before{
    left: 20.3% !important;
}
</style>


<div class="row">

    <div class="col-lg-8 col-xl-8 mb-4 offset-2">
        <div class="card">
            <div class="card-body">

                <div class="ul-widget__head __g-support v-margin">
                    <div class="ul-widget__head-label">
                        <h3 class="ul-widget__head-title">Últimos cambios</h3>
                    </div>
                </div>

                <div class="ul-widget__body">

                    @foreach($data_chagelog as $d)
                        <div class="ul-widget-s7">
                            <div class="ul-widget-s7__items" style="width:100%;">
                                <span class="ul-widget-s7__item-time" style="width:19%;">{{$d->created_at->format('d/m/Y H:i')}}</span>

                                <div class="ul-widget-s7__item-circle" style="width:11%;">
                                    <p class="badge-dot-warning ul-widget7__big-dot"></p>
                                </div>

                                <div class="ul-widget-s7__item-text" style="width:70%;">
                                    Edición:
                                    {{$d->comment}}
                                    en
                                    <span class="badge badge-pill badge-primary m-2">{{$d->element}}</span>
                                    por
                                    <span class="badge badge-pill badge-success m-2">{{$d->user}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <br>
            </div>
        </div>
    </div>

</div>



<script type="text/javascript">
  function calcViatics(des){
    let viatics = {{!empty($colabInfo->viaticos)?$colabInfo->viaticos:1}};
    $(des).parent().find("span").html('Q.'+viatics*des.value);
    // console.log(viatics*des.value);
  }
  function calcViaticsMovil(des){
    let viatics = {{!empty($colabInfo->movil)?$colabInfo->movil:1}};
    $(des).parent().find("span").html('Q.'+viatics*des.value);
    // console.log(viatics*des.value);
  }
  function notifyViatExt(text){
    swal.fire({title:'<small>Agregar comentario viaticos extras</small>',
               html:'<div class="row"><div class="col-12"><textarea id="viaticExText" class="form-control">'+text+'</textarea></div><br>\
               <input type="button"  class="btn btn-primary btn-icon btn-sm m-1" value="Guardar" onclick="addCommentViatExtra()">\
               </div>',
              showCloseButton: false,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function addCommentViatExtra(){
    $('#txtViaticExt').val(  $('#viaticExText').val());
    Swal.close();
  }
  function filter(){
    if($('#week').val()!=''){
      window.location= '{{ url('admin.colabCompanyWork') }}'+'/'+$('#week').val()+'/'+$('#year').val();
    }else{
      alert('Por favor eliga una semana');
    }
  }

  function showDesc(messa){
    swal.fire({title:'Descripción de permiso',
               html:'<div class="row"><div class="col-12">'+messa+'</div></div>',
              showCloseButton: true,
              showCancelButton: false,
              showConfirmButton: false
          });
  }
  function changColor(estos){
    $(estos).css("color","black");
  }
  function checkTime(t){
    var v = t.value;
    if (v.match(/^\d{2}$/) !== null) {
        t.value = v + ':';
        t.value=t.value;
        return ;
    }
    // else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
    //     t = v + '/';
    //     t.value=t;
    //     return ;
    // }
  }
</script>
<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
h5{
  font-weight: bold;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
hr{

}
</style>
@endsection

@section('page-js')



@endsection

@section('bottom-js')

{{-- <script src="{{asset('assets/js/quill.script.js')}}"></script> --}}
<script>
$(document).ready(function () {
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });

});



  function changeLog(){
      Swal.fire({
          title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                    <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                </div>`,
          html:`
               <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                   <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                       <div class="modal-body">
                           <div class="row">
                               <label class="col-form-label">Comentario</label>
                               <textarea id="changeLogComment" class="form-control"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               `,
           preConfirm: function(){
            $('#bt_actualizar').hide();

               let val = $('#changeLogComment').val(),
                   cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`;
             $('#form_planilla').append(cad).attr('onsubmit', '').submit();
             return true
         }
      })
  }



</script>


@endsection
