@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Prestaciones</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">
            <div class="card-body">


              <form id="form_filtro" action="" method="get">
                @csrf

              <div class="row">
                  <div class="col-md-3">
                    Empresa:<br>
                      <select class="form-control" id="id_empresa" name="id_empresa" required>
                        <option value="" selected disabled>-- seleccionar empresa --</option>
                        @foreach ($companies as $k => $v)
                          <option value="{{$v[0]}}" {{(isset($request->id_empresa)&&$v[0]==$request->id_empresa?'selected':'')}}>{{$v[1]}}</option>
                        @endforeach
                      </select>
                  </div>


                  <div class="col-md-3">
                    Cliente:<br>
                      <select class="form-control" id="id_cliente" name="id_cliente">
                        {!! $clientes !!}
                      </select>
                  </div>

                  <div class="col-md-3">
                    Sucursal:<br>
                      <select class="form-control" id="id_sucursal" name="id_sucursal">
                          {!! $sucursales !!}
                      </select>
                  </div>

                  <div class="col-md-3">
                    Area:<br>
                      <select class="form-control" id="id_area" name="id_area">
                          {!! $areas !!}
                      </select>
                  </div>



              </div>
              <br>
              <div class="row">

                  <div class="col-md-3">
                     Prestacion:<br>
                      <select class="form-control" id="tipo" name="tipo" required>
                          <option selected value="" disabled> --- todos --- </option>
                          <option value="aguinaldo" {{ ($request->tipo=='aguinaldo'?'selected':'') }} >Aguinaldo</option>
                          <option value="bono 14" {{ ($request->tipo=='bono 14'?'selected':'') }} >Bono 14</option>
                          <option value="vacaciones" {{ ($request->tipo=='vacaciones'?'selected':'') }} >Vacaciones</option>
                      </select>
                  </div>


    <div class="col-lg-3">
      Año
      <input type="text" name="ano" id="ano" class="w-100 form-control" value="{{ @$request->ano }}">
    </div>

    <div class="col-lg-3">
        <button type="submit" form="form_filtro" value="filtrar" class="btn btn-primary text-white btn-rounded" style="margin-top: 20px;">Filtrar</button>
    </div>

              </div>

              </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
    		  <h3 class="card-title  mb-0">
            Colaboradores en esta empresa
          </h3>
    		</div>
        <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


          <form id="form_pago" action="{{ url('aprobar_prestaciones') }}" method="post" onsubmit="return validate_data()">
          @csrf

          <div class="table-responsive">

            <table id="asignar_colaboradores_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>&nbsp;</th>
                  <th>Código</th>
                  <th>Colaborador</th>                  
                  <th>Prestacion</th>
                  <th>Total</th>
                  <th>Detalle</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($colabs))
                  @foreach ($colabs as $value)
                    @php

                    $colab = $value->getColabsInfo()->first();
                    $sueldo = $colab->sueldo*30;
                    $fecha_ingreso = date_create($colab->fecha_ingreso);

                    $ano_pasado = date("Y", strtotime ('-1 year'));
                    $ano_presente = date("Y");

                    if ($request->tipo=='aguinaldo') {

                        // aguinaldo

                        $fecha_aguinaldo = $ano_pasado."-11-30";
                        $ano_aguinaldo = date_create($fecha_aguinaldo);

                        $fecha_fin = $ano_presente."-11-30";
                        $fin = date_create($fecha_fin);

                        // validacion de fecha de ingreso
                        if ($fecha_ingreso<$ano_aguinaldo) {
                            $cuando_entro = "entro antes";

                            $fecha_inicio = $fecha_aguinaldo;
                            $inicio = $ano_aguinaldo;
                        } else {
                            $cuando_entro = "entro despues";

                            $fecha_inicio = $colab->fecha_ingreso;
                            $inicio = date_create($fecha_inicio);
                        }

                        $diff_aguinaldo = date_diff($inicio, $fin);

                        $dias_aguinaldo = $diff_aguinaldo->format('%a')+1;

                        $aguinaldo = ($sueldo/365)*$dias_aguinaldo;
                        // $aguinaldo = number_format($aguinaldo, 2, '.', ',');

                        $total = $aguinaldo;

                        $total_dias = $dias_aguinaldo;

                    } elseif ($request->tipo=='bono 14') {

                        // bono 14
                        $ano_bono_14 = $ano_pasado."-06-30";
                        $ano_bono_14 = date_create($ano_bono_14);

                        $fin = date_create($ano_presente."-06-30");

                        // validacion de fecha de ingreso
                        if ($fecha_inicio<$ano_bono_14) {
                            $cuando_entro = "entro antes";

                            $inicio = $ano_bono_14;

                            $diff_bono_14 = date_diff($ano_bono_14, $fin);
                        } else {
                            $cuando_entro = "entro despues";

                            $diff_bono_14 = date_diff($inicio, $fin);
                        }

                        // $diff_bono_14 = date_diff($ano_bono_14, $fin);

                        $dias_bono_14 = $diff_bono_14->format('%a')+1;

                        $bono_14 = ($sueldo/365)*$dias_bono_14;
                        // $bono_14 = number_format($bono_14, 2, '.', ',');

                        $total = $bono_14;

                        $total_dias = $dias_bono_14;

                    }


                    // aplicar filtros
                    $log_prestaciones = $model::whereYear("fecha_pago",$request->ano)
                                                ->where("id_colab",$value->idColab)
                                                ->where('id_empresa',$request->id_empresa)
                                                ->first();
                                                          


                    $array_prestaciones = array(
                                          'id_empresa'=>$request->id_empresa,
                                          'id_colab'=>$value->idColab,
                                          'id_cliente'=>$request->id_cliente,
                                          'id_sucursal'=>$request->id_sucursal,
                                          'id_area'=>$request->id_area,
                                          'tipo'=>$request->tipo,
                                          'total'=> $total,
                                          'detalle'=>'Desde '.date_format($inicio, 'd-m-Y').' hasta '.date_format($fin, 'd-m-Y')
                    );


                    $json_prestaciones = json_encode($array_prestaciones);


                    $total = number_format($total, 2, '.', ',');


                    @endphp
                    @if(!empty($colab))
                      <tr>
                        <td >
                          @if(empty($log_prestaciones->estado))

                          <input type="checkbox" id="{{ $value->idColab }}" name="pago[]" value="{{ $json_prestaciones }}">

                          @endif

                        </td>
                        <td >{{ $value->idColab }}</td>
                        <td >{{ $colab->nombres }} {{ $colab->apellidos }}
                          <br><small><i>{{ $colab->puesto }}</i></small>
                          <br><small><i>( {{ $colab->fecha_ingreso }} )</i></small>
                        </td>
                        <td style="text-transform: capitalize;">

                          {{ $request->tipo }}
                        </td>

                        <td>
                          Q. 
                          @if(!empty($log_prestaciones->total))
                              {{ number_format($log_prestaciones->total, 2, '.', ',') }}
                          @else
                              {{ $total }}
                          @endif
                      </td>
                        <td class="td-actions  text-left" style="font-size: 20px;">
                          <a  href="{{ url('prestaciones_colab') }}?id_empresa={{ $request->id_empresa }}&id_colab={{$value->idColab}}&id_cliente={{ $request->id_cliente }}&id_sucursal={{ $request->id_sucursal }}&id_area={{ $request->id_area }}&tipo={{ $request->tipo }}&ano={{ $request->ano }}" title="Ver detalle" target="_blank">
                              <i data-feather="eye"></i>
                          </a>
                        </td>
                        <td>
                          @if(!empty($log_prestaciones->estado))
                            <span class="badge badge-pill {{($log_prestaciones->estado=='Denegado'?'badge-danger':'badge-success')}} m-2">
                            {{ $log_prestaciones->estado }}</span>
                          </td>
                          @else
                            No procesado
                          @endif
                      </tr>
                    @endif
                  @endforeach
                @endif
					    </tbody>
           </table>


          </div>


@if ($request->id_empresa!='')

          <input type="hidden" id="id_empresa" name="id_empresa" value="{{ $request->id_empresa }}">
          <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $request->id_cliente }}">
          <input type="hidden" id="id_sucursal" name="id_sucursal" value="{{ $request->id_sucursal }}">
          <input type="hidden" id="id_area" name="id_area" value="{{ $request->id_area }}">
          <input type="hidden" id="inicio" name="inicio" value="{{ $request->inicio }}">
          <input type="hidden" id="fin" name="fin" value="{{ $request->fin }}">

          <div class="text-left">
              <input type="checkbox" id="pago_todos" name="pago_todos" value="todos"> SELECCIONAR TODOS
          </div>
          <div class="text-center">
            
              <div style="width: 200px; margin-left: auto; margin-right: auto;">
                  Fecha de Pago
                  <input type="date" name="fecha_pago" id="fecha_pago" class="w-100 form-control" value="" required>
              </div>
              <br>

              <button type="submit" form="form_pago" value="pago" class="btn btn-primary text-white btn-rounded">Procesar PAGO</button>
          </div>

@endif
        </form>

        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">


$(function() {


    $('#id_empresa').on('change', function() {


        $('#id_cliente').html('');
        $('#id_sucursal').html('');

        id_empresa = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_clientes')}}',
           data:{
             id_empresa: id_empresa,
           },
           success:function(data) {

                $('#id_cliente').html(data);     
           }
        });

    });


    $('#id_cliente').on('change', function() {


        $('#id_sucursal').html('');

        id_cliente = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_sucursales')}}',
           data:{
             id_cliente: id_cliente,
           },
           success:function(data) {

                $('#id_sucursal').html(data);
           }
        });

    });



    $('#id_sucursal').on('change', function() {


        $('#id_area').html('');

        id_sucursal = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_areas')}}',
           data:{
             id_sucursal: id_sucursal,
           },
           success:function(data) {

                $('#id_area').html(data);
           }
        });

    });


    $("#pago_todos").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });


});


    function validate_data() {

        var selected = [];

         $('#asignar_colaboradores_table input:checked').each(function() {
             selected.push($(this).attr('value'));
         });


        if(selected.length == 0){
            alert('Debes Selecciona al menos un Colaborador para continuar.');
            return false;
        } else {
            return true;
        }

    }


</script>
<style media="screen">
  small{
    font-weight:bold;
  }
</style>
@endsection

@section('bottom-js')
<script>
        $('#asignar_colaboradores_table').DataTable();
</script>


@endsection
