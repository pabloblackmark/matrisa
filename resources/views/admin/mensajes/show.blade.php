@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')
@php 

$mensajes_noleidos = Helper::get_notis();

$mensaje_id_now = @$mensaje["id"];

// echo $mensaje_id_now;

@endphp
<div class="breadcrumb">
    <h1 class="mr-2">Mensajes</h1>
    <!--
                        <ul>
                            <li>Buzón de Entrada</li>
                            <li><a href="{{url('/mensajes_enviados')}}">Enviados</a></li>
                        </ul>
    -->
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


   <div class="row">

@include('admin.mensajes.inc_empresas')

    <div class="col-md-12">
      <div class="card">
                <div class="inbox-main-sidebar-container" data-sidebar-container="main">
                    <div class="inbox-main-content" data-sidebar-content="main" style="width: 85%; float: right;">
                        <!-- SECONDARY SIDEBAR CONTAINER-->
                        <div class="inbox-secondary-sidebar-container box-shadow-1" data-sidebar-container="secondary">

                            <div data-sidebar-content="secondary" style="width: 70%;  float: right;">
                                <div class="inbox-secondary-sidebar-content position-relative" style="min-height: 500px">
                                    <div class="inbox-topbar box-shadow-1 perfect-scrollbar rtl-ps-none pl-3" data-suppress-scroll-y="true">
                                        <!-- <span class="d-sm-none">Test</span>--><a class="link-icon d-md-none" data-sidebar-toggle="main"><i class="icon-regular i-Arrow-Turn-Left"></i></a><a class="link-icon mr-3 d-md-none" data-sidebar-toggle="secondary"><i class="icon-regular mr-1 i-Left-3"></i> Inbox</a>
                                        <div class="d-flex">
                                        <!--
                                        <a class="link-icon mr-3" href=""><i class="icon-regular i-Mail-Reply"></i> Reply</a><a class="link-icon mr-3" href=""><i class="icon-regular i-Mail-Reply-All"></i> Forward</a>
                                        
                                        <a class="link-icon mr-3" href="">
                                        -->
                                        <a href="javascript:" onclick="deleteD('{{ @$mensaje["id"] }}','{{ csrf_token() }}', 'destroy');" title="Borrar" class="link-icon mr-3">
                                                <i class="icon-regular i-Mail-Reply-All"></i> Borrar</a></div>
                                    </div>
                                    <!-- EMAIL DETAILS-->
                                    <div class="inbox-details perfect-scrollbar rtl-ps-none" data-suppress-scroll-x="true">
                                        <div class="row no-gutters">
                                            <!--
                                            <div class="mr-2" style="width: 36px"><img class="rounded-circle" src="../../dist-assets/images/faces/1.jpg" alt="" /></div>
                                            -->
                                            <div class="col-xs-12">
                                                <p class="m-0">de: {{ @$mensaje['de'] }}</p>
                                                <p class="m-0">para: {{ @$mensaje['para'] }}</p>
                                                <p class="text-12 text-muted">{{ @$mensaje['fecha'] }}</p>
                                                <br>
                                            </div>
                                        </div>
                                        <h4 class="mb-3">{{ @$mensaje['titulo'] }}</h4>
                                        <div>
                                            <p>
                                                {{ @$mensaje['mensaje'] }}
                                            </p>
                                        </div>

                                        <hr>

@if (@$mensaje["respuestas"]!='')
    @foreach($mensaje["respuestas"] as $respuesta)
    <div class="row">
        @if ($respuesta["tipo"]==0)
            <div class="col-lg-6 col-xl-6 mt-3">

            </div>
            <div class="col-lg-6 col-xl-6 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <!--
                                    <div class="card-title mb-2">Re: {{ $respuesta["de"] }}</div>
                                -->
                                    <small>Re: {{ $respuesta["de"] }}</small><br>
                                    <small>{{ $respuesta["fecha"] }}</small><br><br>
                                    {{ $respuesta["respuesta"] }}
                                </div>
                            </div>
            </div>
        @else
            <div class="col-lg-6 col-xl-6 mt-3">
                            <div class="card" style="background-color: #F0F0F0;">
                                <div class="card-body">
                                    <small>Re: {{ $respuesta["de"] }}</small><br>
                                    <small>{{ $respuesta["fecha"] }}</small><br><br>
                                    {{ $respuesta["respuesta"] }}
                                </div>
                            </div>
            </div>
        @endif
    </div>
    @endforeach

<br>
<br>
@endif

<form method="POST" action="{{ url('mensajes/') }}/{{ @$mensaje['id'] }}" >

    @csrf
    @method('PUT')

                    <div class="row">
                        <div class="col-md-12 form-group mb-3">
                             <label for="respuesta">Mensaje de Respuesta:</label>
                             <textarea class="form-control" style="height: 150px;" id="respuesta" name="respuesta" placeholder="Mensaje de Respuesta..." ></textarea>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                                Responder Mensaje
                            </button>
                        </div>                        
                    </div> 

</form>

                                    </div>
                                </div>
                            </div>
                            <!-- Secondary Inbox sidebar-->
                            <div class="inbox-secondary-sidebar rtl-ps-none" data-sidebar="secondary" style="width: 30%;  float: right; overflow: auto; height: 600px;">
                                <!--
                                <i class="sidebar-close i-Close" data-sidebar-toggle="secondary" ></i>
                                -->

@forelse($mensajes as $mensaje)

<a href="{{ url('mensajes') }}/{{ $mensaje['id'] }}">
    @php
        if ($mensaje['id']==$mensaje_id_now) {
            $bg_color = 'style="background-color: #F0F0F0;"';
        } else {
            $bg_color = '';
        }
    @endphp
<div class="mail-item" @if (($mensaje['tipo']==1 AND $mensaje['status']==0) OR ($mensaje['tipo']==0 AND $mensaje['status']==2)) style="font-weight: bold;" @endif  {!! $bg_color !!} >
                                    <!--
                                    <div class="avatar"><img src="../../dist-assets/images/faces/1.jpg" alt="" /></div>
                                    -->
                                    <div class="col-xs-6 details"><span class="name text-muted">
                                        <p class="m-0" style="color: black;"><strong>{{ $mensaje['titulo'] }}</strong></p>
                                        de: {{ $mensaje['de'] }}<br>
                                        para: {{ $mensaje['para'] }}</span>
                                    </div>
                                    <div class="col-xs-3 date"><span class="text-muted">{{ $mensaje['fecha'] }}<br>
                                    {{ $mensaje['status_texto'] }}</span></div>
                                </div>
</a>                                

                @empty
                  <div class="alert alert-success">
                    No hay mensajes en el buzón.
                  </div>

                @endforelse


                            </div>
                        </div>
                    </div>
                    <!-- MAIN INBOX SIDEBAR-->
                    <div class="inbox-main-sidebar" data-sidebar="main" data-sidebar-position="left" style="width: 15%;  float: right;">
                        <div class="pt-3 pr-3 pb-3">
                        <!--    
                            <i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
                        -->
                            <a href="{{ url('/mensajes/create') }}">
                            <button class="btn btn-rounded btn-primary btn-block mb-4">Nuevo Mensaje</button>
                            </a>
                            <div class="pl-3">
                                <p class="text-muted mb-2"></p>
                                <ul class="inbox-main-nav">
                                     <li><a class="active" href="{{ url('mensajes') }}"><i class="icon-regular i-Mail-2"></i> <strong>Mensajes ({{ count($mensajes_noleidos) }})</strong></a></li>
                                    <!--
                                   
                                    <li><a href=""><i class="icon-regular i-Mail-Outbox"></i> Sent</a></li>
                                
                                    <li><a href=""><i class="icon-regular i-Mail-Favorite"></i> Starred</a></li>
                                    <li><a href=""><i class="icon-regular i-Folder-Trash"></i> Trash</a></li>
                                    -->
                                    <li><a href="{{ url('mensajes_grupos') }}"><i class="icon-regular i-Spam-Mail"></i> Grupos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>



                </div>


          </div>
        </div>
      </div>


    @component('components.messagesForm')
    @endcomponent
@endsection
