
                        <div class="col-md-4 form-group mb-3">
                             <label for="id_empresa">Empresa:</label>
                             <select class="form-control" id="id_empresa" name="id_empresa" required>
                                 <option selected readonly value="---"> --- Todas --- </option>
                                 @foreach($empresas as $empresa)

                                      @php
                                        // echo session('id_empresa');

                                        $selected = "";

                                        if ($empresa->id==session('id_empresa')) {
                                            $selected = "selected";
                                        }

                                      @endphp

                                     <option value="{{ $empresa->id }}" {{ $selected }} >{{ $empresa->company }}</option>
                                 @endforeach
                             </select>
                        </div>

<script type="text/javascript">

    $(function () {

        $("#id_empresa").change(function(e) {
            window.location.href = '?id_empresa=' + $(this).val();
        });

    });


</script>