@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')
@php 

$mensajes_noleidos = Helper::get_notis();

@endphp
<div class="breadcrumb">
    <h1 class="mr-2">Mensajes</h1>
    <!--
                        <ul>
                            <li>Buzón de Entrada</li>
                            <li><a href="{{url('/mensajes_enviados')}}">Enviados</a></li>
                        </ul>
    -->
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


   <div class="row">

@include('admin.mensajes.inc_empresas')

    <div class="col-md-12">
      <div class="card">
                <div class="inbox-main-sidebar-container" data-sidebar-container="main">
                    <div class="inbox-main-content" data-sidebar-content="main" style="width: 80%; float: right;">
                        <!-- SECONDARY SIDEBAR CONTAINER-->

                        <div class="inbox-secondary-sidebar-container box-shadow-1" data-sidebar-container="secondary">
                            <div data-sidebar-content="secondary" style="width: 100%;  float: right;">

                                <div class="inbox-secondary-sidebar-content position-relative" style="min-height: 500px">
                                    <div class="inbox-topbar box-shadow-1 perfect-scrollbar rtl-ps-none pl-3" data-suppress-scroll-y="true">
                                        <!-- <span class="d-sm-none">Test</span>--><a class="link-icon d-md-none" data-sidebar-toggle="main"><i class="icon-regular i-Arrow-Turn-Left"></i></a><a class="link-icon mr-3 d-md-none" data-sidebar-toggle="secondary"><i class="icon-regular mr-1 i-Left-3"></i> Inbox</a>
                                        <div class="d-flex">
                                        <!--
                                        <a class="link-icon mr-3" href=""><i class="icon-regular i-Mail-Reply"></i> Reply</a>>
                                        
                                        <a class="link-icon mr-3" href="">
                                        -->
                                        <h4 class="link-icon mr-3">
                                        <i class="icon-regular i-Mail-Reply-All"></i> Nuevo Mensaje</h4>
                                        </div>
                                    </div>

                               <div class="inbox-details perfect-scrollbar rtl-ps-none" data-suppress-scroll-x="true">
<form id="form_mensaje" method="POST" action="{{ url('mensajes') }}" onsubmit="return validate_data()" >

    @csrf

                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                             <label for="para_user">Para:</label>
                                             <select class="form-control" id="para_user" name="para_user" required>
                                                 <option selected disabled value=""></option>
                                                     <option value="0">------ EN GENERAL ------</option>
                                                     <option value="g">------ Un Grupo ------</option>
                                                     <option value="v">------ Seleccionar Varios------</option>
                                                 @foreach($colaboradores as $colaborador)
                                                     <option value="{{ $colaborador->id }}">{{ $colaborador->nombres }} {{ $colaborador->apellidos }}</option>
                                                 @endforeach
                                             </select>
                                        </div>
                                    </div> 


                                    <div id="grupos" class="row" style="display: none;">
                                        <div class="col-md-6 form-group mb-3">
                                             <label for="para_user">Grupo:</label>
                                             <select class="form-control" id="para_grupo" name="para_grupo">
                                                 <option selected disabled value="">------ Seleccionar Grupo ------</option>
                                                 @foreach($grupos as $grupo)
                                                     <option value="{{ $grupo->id }}">{{ $grupo->nombre }}</option>
                                                 @endforeach
                                             </select>
                                        </div>
                                    </div>

                                    <div id="varios" class="row" style="display: none;">
                                        <div class="col-md-6 form-group mb-3">
                                             <label for="para_user">Varios:</label>
                                        </div>                                      
                                        <div class="col-md-12 form-group mb-3" style="column-count: 3;">
                                             @foreach($colaboradores as $colaborador)

                                                 <input type="checkbox" id="{{ $colaborador->id }}" name="ids[]" class="ids_varios" value="{{ $colaborador->id }}" > 
      &nbsp;<strong>{{ $colaborador->nombres }} {{ $colaborador->apellidos }}</strong> <br>
                                             @endforeach

                                        </div>
                                    </div>

                                             <input type="hidden" id="de_admin" name="de_admin" value="{{ $user->id }}">
                                             <input type="hidden" id="tipo" name="tipo" value="0">

                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="asunto">Asunto</label>
                                            <input class="form-control" id="titulo" name="titulo" type="text" placeholder="Asunto" required />
                                        </div>

                                    </div>



                                    <div class="row">
                                        <div class="col-md-12 form-group mb-3">
                                             <label for="observaciones">Mensaje:</label>
                                             <textarea class="form-control" style="height: 150px;" id="mensaje" name="mensaje" placeholder="Mensaje..." required >{{ isset($data['mensaje']) ? $data['mensaje'] : '' }}</textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                                                Enviar Mensaje
                                            </button>
                                        </div>  
                                    </div>
</form>


                                </div>

                            </div>



                                </div>
                        </div>
                    </div>

                    <!-- MAIN INBOX SIDEBAR-->
                    <div class="inbox-main-sidebar" data-sidebar="main" data-sidebar-position="left" style="width: 20%;  float: right;">
                        <div class="pt-3 pr-3 pb-3">
                        <!--    
                            <i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
                        -->
                            <a href="{{ url('/mensajes/create') }}">
                            <button class="btn btn-rounded btn-primary btn-block mb-4">Nuevo Mensaje</button>
                            </a>
                            <div class="pl-3">
                                <p class="text-muted mb-2"></p>
                                <ul class="inbox-main-nav">
                                     <li><a class="active" href="{{ url('mensajes') }}"><i class="icon-regular i-Mail-2"></i> Mensajes ({{ count($mensajes_noleidos) }})</a></li>
                                    <!--
                                   
                                    <li><a href=""><i class="icon-regular i-Mail-Outbox"></i> Sent</a></li>
                                
                                    <li><a href=""><i class="icon-regular i-Mail-Favorite"></i> Starred</a></li>
                                    <li><a href=""><i class="icon-regular i-Folder-Trash"></i> Trash</a></li>
                                    -->
                                    <li><a href="{{ url('mensajes_grupos') }}"><i class="icon-regular i-Spam-Mail"></i> Grupos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>



                </div>


          </div>
        </div>
      </div>


<script type="text/javascript">


$(document).ready(function() {
});


    $('#para_user').change(function() {

        val = $(this).val();


        if (val=="g") {

            $("#varios").hide();
            $("#grupos").show();

            $("#para_grupo").attr("required",true);

        } else if (val=="v") {
            $("#varios").show();
            $("#grupos").hide();

            $("#para_grupo").attr("required",false);

        } else {
            $("#varios").hide();
            $("#grupos").hide();

            $("#para_grupo").attr("required",false);
        }

     });
  

      function validate_data() {
        
        // event.preventDefault();
        var validate = 0;
        var selected = [];

           $('#varios input:checked').each(function() {
               selected.push($(this).attr('value'));
           });

         if ($('#para_user').val()=='v') {

             if(selected.length == 0){
               alert('Selecciona al menos un Colaborador para enviar el mensaje.');
               return false;
             } else {
                return true;
             }
         }

      }

</script>

    @component('components.messagesForm')
    @endcomponent
@endsection
