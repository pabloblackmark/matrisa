@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')
@php 

$mensajes_noleidos = Helper::get_notis();

@endphp
<div class="breadcrumb">
    <h1 class="mr-2">Mensajes</h1>
    <!--
                        <ul>
                            <li>Buzón de Entrada</li>
                            <li><a href="{{url('/mensajes_enviados')}}">Enviados</a></li>
                        </ul>
    -->
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


   <div class="row">

@include('admin.mensajes.inc_empresas')

    <div class="col-md-12">
      <div class="card">
                <div class="inbox-main-sidebar-container" data-sidebar-container="main">
                    <div class="inbox-main-content" data-sidebar-content="main" style="width: 80%; float: right;">
                        <!-- SECONDARY SIDEBAR CONTAINER-->
                        <div class="inbox-secondary-sidebar-container box-shadow-1" data-sidebar-container="secondary">

                            <!-- Secondary Inbox sidebar-->
                            <div class="inbox-secondary-sidebar perfect-scrollbar rtl-ps-none" data-sidebar="secondary" style="width: 100%;  float: right;">
                                <!--
                                <i class="sidebar-close i-Close" data-sidebar-toggle="secondary" ></i>
                                -->

@forelse($mensajes as $mensaje)

<a href="{{ url('mensajes') }}/{{ $mensaje['id'] }}">
                                <div class="mail-item" @if (($mensaje['tipo']==1 AND $mensaje['status']==0) OR ($mensaje['tipo']==0 AND $mensaje['status']==2)) style="font-weight: bold;" @endif >
                                    <!--
                                    <div class="avatar"><img src="../../dist-assets/images/faces/1.jpg" alt="" /></div>
                                    -->
                                    <div class="col-xs-6 details"><span class="name text-muted">
                                        <p class="m-0" style="color: black;"><strong>{{ $mensaje['titulo'] }}</strong></p>
                                        de: {{ $mensaje['de'] }}<br>
                                        para: {{ $mensaje['para'] }}</span>
                                        
                                    </div>
                                    <div class="col-xs-3 date"><span class="text-muted">{{ $mensaje['fecha'] }}<br>
                                    {{ $mensaje['status_texto'] }}</span></div>
                                </div>
</a>                                

                @empty
                  <div class="alert alert-success">
                    No hay mensajes en el buzón.
                  </div>

                @endforelse


                            </div>
                        </div>
                    </div>
                    <!-- MAIN INBOX SIDEBAR-->
                    <div class="inbox-main-sidebar" data-sidebar="main" data-sidebar-position="left" style="width: 20%;  float: right;">
                        <div class="pt-3 pr-3 pb-3">
                        <!--    
                            <i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
                        -->
                            <a href="{{ url('/mensajes/create') }}">
                            <button class="btn btn-rounded btn-primary btn-block mb-4">Nuevo Mensaje</button>
                            </a>
                            <div class="pl-3">
                                <p class="text-muted mb-2"></p>
                                <ul class="inbox-main-nav">
                                     <li><a class="active" href="{{ url('mensajes') }}"><i class="icon-regular i-Mail-2"></i> <strong>Mensajes ({{ count($mensajes_noleidos) }})</strong></a></li>
                                    <!--
                                   
                                    <li><a href=""><i class="icon-regular i-Mail-Outbox"></i> Sent</a></li>
                                
                                    <li><a href=""><i class="icon-regular i-Mail-Favorite"></i> Starred</a></li>
                                    <li><a href=""><i class="icon-regular i-Folder-Trash"></i> Trash</a></li>
                                    -->
                                    <li><a href="{{ url('mensajes_grupos') }}"><i class="icon-regular i-Spam-Mail"></i> Grupos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>



                </div>


          </div>
        </div>
      </div>

<script type="text/javascript">

    $(function () {

        $("#id_empresa").change(function(e) {
            window.location.href = '{{ url("mensajes") }}?id_empresa=' + $(this).val();
        });

    });


</script>

    @component('components.messagesForm')
    @endcomponent
@endsection
