@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Cumpleaños</h1>
</div>
<div class="separator-breadcrumb border-top"></div>

<form id="cumples_form" method="POST" action="{{ url('cumples_update') }}" enctype="multipart/form-data" >

    @csrf
    @if(@$edit)
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-md-12">


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Mensaje de cumpleaños</h4>
                </div>
                <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

                    <div class="row">

                                    @if(isset($data->imagen))
                                        <div class="col-lg-6 offset-3 col-md-12 form-group mb-1">
                                            <img src="{{ asset('storage/' . $data->imagen) }}"/ width="100%" height="100%" style="margin: auto;">
                                        </div>
                                    @endif()

                                    <div class="col-lg-6 col-md-6 form-group mb-1">
                                        <label for="imagen">Imagen</label>
                                        <input class="form-control" id="imagen" name="imagen" type="file" placeholder="Archivo de imagen" />
                                        <br>
                                    </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 form-group mb-3">
                             <label for="observaciones">Mensaje</label>
                             <textarea class="form-control" style="height: 150px;" id="mensaje" name="mensaje" placeholder="Mensaje de cumpleaños..." >{{ isset($data['mensaje']) ? $data['mensaje'] : '' }}</textarea>
                        </div>
                    </div>     
                    
                </div>
            </div>



            <div class="col-md-12">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Actualizar datos
                </button>
            </div>

        </div>

    </div>

</form>


@endsection
