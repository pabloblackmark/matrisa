@extends('admin.layouts.master')
@section('main-content')


<style type="text/css">
    
.coti {
    font-size: 18px;
}

</style>

<div class="breadcrumb">

    <h1 class="mr-2">Cotizaciones</h1>

                        <ul>
                            <li><a href="{{url('/cotis')}}">Listado</a></li>
                        </ul>    
    

   
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

    <div class="row">
        <div class="col-md-12">


            <div class="card mb-3">

                <div class="card-header">
                    <h4>Cotización no. {{ $coti['no'] }}</h4>
                </div>
                
                <div class="card-body" id='coti'>


                    <div class="row">
                        <div class="col-md-4 form-group mb-3">
                             <label for="id_empresa">Empresa:</label><br>
                            <span class="coti">{{ $coti['empresa'] }}</span>
                        </div>
                        <div class="col-md-4 form-group mb-3">

                        </div>

                        <div class="col-md-4 form-group mb-3">
                            <label for="fecha">Fecha</label><br>
                            <span class="coti">{{ date("d/m/Y", strtotime($coti['fecha'])) }}</span>
                        </div>                                              
                    </div>

                    <div class="row">

                        <div class="col-md-4 form-group mb-3">
                             <label for="id_cliente">Cliente:</label><br>
                             <span class="coti">{{ $coti['cliente'] }}</span>
                        </div>

                        <div class="col-md-4 form-group mb-3">
                             <label for="id_sede">Sucursal:</label><br>
                             <span class="coti">{{ $coti['sede'] }}</span>
                        </div>



                        <div class="col-md-3 form-group mb-3">
                             <label for="detalle">Nit:</label><br>
                             <span class="coti">{{ $coti['nit'] }}</span>
                        </div>                        

                    </div>         


                    <div class="row">
                        <div class="col-md-12 form-group mb-3">
                            <br>
@php
    $detalle_array = json_decode($coti['detalle'],true);
@endphp


@if ($detalle_array['productos'])
Productos:

<div class="col-md-12 row">
    <div class="col-md-1 form-group">
        
    </div>
    <div class="col-md-2 form-group">
        cantidad
    </div>
    <div class="col-md-3 form-group">
        producto
    </div>
    <div class="col-md-1 form-group">
        desc.
    </div>
    <div class="col-md-2 form-group">
        total
    </div>
</div>

@foreach($detalle_array['productos'] as $producto)

<div id="servicio_'+count_serv+'" class="servicio col-md-12 row">
    <div class="col-md-1 form-group">
        
    </div>
    <div class="col-md-2 form-group">
        {{ $producto['no_prod'] }}
    </div>
    <div class="col-md-3 form-group">
        {{ $producto['producto'] }}
    </div>
    <div class="col-md-1 form-group">
        {{ $producto['desc_prod'] }}%
    </div>    
    <div class="col-md-2 form-group">
        Q {{ $producto['total_prod'] }}
    </div>
</div>
@endforeach
@endif


@if ($detalle_array['servicios'])
Servicios:

<div class="col-md-12 row">
    <div class="col-md-1 form-group">
        
    </div>
    <div class="col-md-2 form-group">
        horas
    </div>
    <div class="col-md-3 form-group">
        servicio
    </div>
    <div class="col-md-1 form-group">
        desc.
    </div>
    <div class="col-md-2 form-group">
        total
    </div>
</div>

@foreach($detalle_array['servicios'] as $servicio)
<div id="servicio_'+count_serv+'" class="servicio col-md-12 row">
    <div class="col-md-1 form-group">
        
    </div>
    <div class="col-md-2 form-group">
        {{ $servicio['no_serv'] }}
    </div>
    <div class="col-md-3 form-group">
        {{ $servicio['servicio'] }}
    </div>
    <div class="col-md-1 form-group">
        {{ $servicio['desc_serv'] }}%
    </div>
    <div class="col-md-2 form-group">
        Q {{ $servicio['total_serv'] }}
    </div>
</div>
@endforeach
@endif


                        </div>
                    </div>     
                    
                    <div class="row">
                        <div class="col-md-8 form-group mb-3">
                        </div>
                        <div class="col-md-4 form-group mb-3">
                            <label for="total">TOTAL</label>
                            <span class="coti"><strong>Q {{ $coti['total'] }}</strong></span><br>
                            
                        </div>
                    </div>                    

                    <div class="row">
                        <div class="col-md-12 text-center">
                        <br><br>
<small>*nuestros precios estan sujetos a 30 dias de vigencia</small><br>
Sin más, quedo en espera de una pronta y favorable respuesta. Atentamente,
<br><br><br><br><br><br>
<strong>{{ $coti['firma'] }}</strong>
<br><br><br>
Dirección 7 Av. 9 - 60 La Reformita Zona 12 Guatemala C.A.<br>
Telefono : (502) 2471-8880 / 2308-4548<br>
Email: supoperaciones@matrisa.com.gt<br>
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-md-12">
                <a href="{{ url('cotis/export_pdf')}}" target="_blank">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Descargar PDF
                </button>
                </a>
            </div>

        </div>

    </div>

</form>


@endsection
