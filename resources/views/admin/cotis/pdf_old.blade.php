@extends('admin.layouts.pdf')
@section('main-content')


<style type="text/css">
    
.coti {
    font-size: 18px;
}

</style>

    <div class="row" style="width: 775px;">
        <div class="col-md-12">


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Cotización</h4>
                </div>
                <div class="card-body" id='coti'>

                    <div class="row">
                        <div class="col-md-4 form-group mb-3">
                             <label for="id_empresa">Empresa:</label>
                            <span class="coti">{{ $coti['empresa'] }}</span>
                        </div>
                        <div class="col-md-4 form-group mb-3">
                             <label for="id_empresa">Cotización No:</label>
                            <span class="coti">{{ $coti['no'] }}</span>
                        </div>

                        <div class="col-md-4 form-group mb-3">
                            <label for="fecha">Fecha</label>
                            <span class="coti">{{ date("d/m/Y", strtotime($coti['fecha'])) }}</span>
                        </div>                                              
                    </div>

                    <div class="row">

                        <div class="col-md-4 form-group mb-3">
                             <label for="id_cliente">Cliente:</label>
                             <span class="coti">{{ $coti['cliente'] }}</span>
                        </div>

                        <div class="col-md-4 form-group mb-3">
                             <label for="id_sede">Sucursal:</label>
                             <span class="coti">{{ $coti['sede'] }}</span>
                        </div>



                        <div class="col-md-3 form-group mb-3">
                             <label for="detalle">Nit:</label>
                             <span class="coti">{{ $coti['nit'] }}</span>
                        </div>                        

                    </div>         


                    <div class="row">
                        <div class="col-md-12 form-group mb-3">
                             <label for="detalle">Detalle:</label><br>
                             <span class="coti">{{ $coti['detalle'] }}</span>
                        </div>
                    </div>     
                    
                    <div class="row">
                        <div class="col-md-8 form-group mb-3">
                        </div>
                        <div class="col-md-4 form-group mb-3">
                            <label for="total">Total</label>
                            <span class="coti">Q {{ $coti['total'] }}</span><br>
                            <small>*nuestros precios estan sujetos a 30 dias de vigencia</small><br>
                        </div>
                    </div>                    

                    <div class="row">
                        <div class="col-md-12 text-center">
                        <br><br><br>
Sin más, quedo en espera de una pronta y favorable respuesta, Atentamente.
<br><br><br><br><br><br>
<strong>{{ $coti['firma'] }}</strong><br>
GERENTE GENERAL
<br><br><br>
Dirección 7 Av. 9 - 60 La Reformita Zona 12 Guatemala C.A.<br>
Telefono : (502) 2471-8880 / 2308-4548<br>
Email: supoperaciones@matrisa.com.gt<br>
                        </div>
                    </div>


                </div>
            </div>


        </div>

    </div>


@endsection
