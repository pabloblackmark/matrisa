@extends('admin.layouts.pdf')
@section('main-content')


<style type="text/css">

body, html {
    margin: 0;
    padding: 0;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

h4 {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

.coti_titulo {
    font-size: 20px;
    font-weight: bold;
}

.coti {
    font-size: 18px;
}

</style>

    <div class="row" style="width: 730px; margin-right: auto; margin-left: auto;">
        <div class="col-md-12" style="padding-top: 40px;">


    <div style="width: 50%; float: left;">
        <img src="{{ asset('images/matrisa.jpg') }}" />
    </div>
    <div style="width: 50%; float: left; text-align: right;">
            <span class="coti_titulo">Cotización No. {{ $no }}</span>

    </div>

    <div style="clear: both;">
    </div>
    <br>
<table width="100%">
    <tr>
        <td width="40%">
                            Empresa:<br>
                            <span class="coti">{{ $empresa }}</span>
        </td>
        <td width="35%">
                            &nbsp;
        </td>
        <td width="25%">
                            Fecha<br>
                            <span class="coti">{{ date("d/m/Y", strtotime($fecha)) }}</span>
        </td>
    </tr>
    <tr>    
        <td>
                             Cliente:<br>
                             <span class="coti">{{ $cliente }}</span>  
        </td>
        <td>
                             Sucursal:<br>
                             <span class="coti">{{ $sede }}</span>
        </td>
        <td>
                             Nit:<br>
                             <span class="coti">{{ $nit }}</span>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br>
@php
    $detalle_array = json_decode($detalle,true);
@endphp

@if ($detalle_array['productos'])
Productos:
            <table width="100%">

                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="10%">cantidad</td>
                    <td width="50%">producto</td>
                    <td width="10%">desc</td>
                    <td width="20%">total</td>
                </tr>

@foreach($detalle_array['productos'] as $producto)

                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="10%">{{ $producto['no_prod'] }}</td>
                    <td width="50%">{{ $producto['producto'] }}</td>
                    <td width="10%">{{ $producto['desc_prod'] }}%</td>
                    <td width="20%">Q {{ $producto['total_prod'] }}</td>
                </tr>    
@endforeach
            </table>
@endif


@if ($detalle_array['servicios'])
Servicios:
            <table width="100%">
                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="10%">horas</td>
                    <td width="50%">servicio</td>
                    <td width="10%">desc</td>
                    <td width="20%">total</td>
                </tr>
@foreach($detalle_array['servicios'] as $servicio)

                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="10%">{{ $servicio['no_serv'] }}</td>
                    <td width="50%">{{ $servicio['servicio'] }}</td>
                    <td width="10%">{{ $servicio['desc_serv'] }}%</td>
                    <td width="20%">Q {{ $servicio['total_serv'] }}</td>
                </tr>    
@endforeach
            </table>
@endif

        </td>
    </tr>
    <tr>    
        <td>
&nbsp;
        </td>
        <td>
&nbsp;
        </td>
        <td>
                            Total
                            <span class="coti">Q {{ $total }}</span><br>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center;">
                        <br><br>
<small>*nuestros precios estan sujetos a 30 dias de vigencia</small><br>                        
Sin más, quedo en espera de una pronta y favorable respuesta. Atentamente,
<br><br><br><br><br><br>
<strong>{{ $firma }}</strong>
<br><br>
Dirección 7 Av. 9 - 60 La Reformita Zona 12 Guatemala C.A.<br>
Telefono : (502) 2471-8880 / 2308-4548<br>
Email: supoperaciones@matrisa.com.gt<br>
        </td>
    </tr> 

</table>


        </div>

    </div>


@endsection
