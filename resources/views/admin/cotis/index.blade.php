@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">

    <h1 class="mr-2">Cotizaciones</h1>   
    
</div>
<div class="separator-breadcrumb border-top"></div>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <a href="{{ url('/cotis/create') }}">
                    <i class="ion-ios7-plus-outline "></i>
                    &nbsp;&nbsp;Nueva Cotización
                </a>
            </h3>


        </div>


        <div class="card-body">


            <div class="col-md-4">
                             <label for="cliente">Cliente:</label>
                             <select class="form-control" id="cliente" name="cliente">
                                 <option value=""> --- Todos --- </option>
                                 @foreach($clientes as $cliente)
                                      @php
                                        $selected = "";

                                        if ($request->id_cliente==$cliente->id) {
                                            $selected = "selected";
                                        }

                                      @endphp
                                     <option value="{{ $cliente->id }}" {{ $selected }} >{{ $cliente->clientName }}</option>
                                 @endforeach
                             </select>
                             <br>
            </div>


          <div class="table-responsive">
            <table id="cotis_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>No. Cotización</th>
                  <th>Fecha</th>
                  <th>Cliente</th>
                  <th>Sucursal</th>
                  <th>NIT</th>
                  <th>Valor</th>
                  <th>Status</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($data as $value)
                  @php $perms=array(); @endphp
                  <tr>
                    <td >{{$value->no}}</td>
                    <td >{{ date("d/m/Y", strtotime($value->fecha)) }}</td>
                    <td >{{ $value->cliente }}</td>
                    <td >{{ $value->sede }}</td>
                    <td >{{ $value->nit }}</td>
                    <td >Q. {{ $value->total }}</td>
                    <td >

                      @if ($value->status==0)
                          <span class="badge w-badge badge-danger">Creada</span>
                      @elseif ($value->status==1)
                          <span class="badge w-badge badge-warning">Enviada</span>
                      @elseif ($value->status==2)
                          <span class="badge w-badge badge-success">Aprovada</span>
                      @endif
                    

                    </td>
                    <td class="td-actions  text-left">
                      <a  href="{{ url('cotis') }}/{{$value->id}}" title="Ver Cotización">
                          <i class="text-20" data-feather="file"></i>
                      </a>

                      <a  href="javascript:"
                          onclick="deleteD('{{$value->id}}','{{ csrf_token() }}');" title="Borrar">
                          <i class="text-12" data-feather="trash"></i>
                      </a>
                    </td>
                  </tr>

                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
<script type="text/javascript">

    $(function () {

        $("#cliente").change(function(e) {
            window.location.href = '{{ url("cotis") }}?id_cliente=' + $(this).val();
        });

    });


</script>

@endsection

@section('bottom-js')
    <script>
        $('#cotis_table').DataTable();
    </script>
@endsection
