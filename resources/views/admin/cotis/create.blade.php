@extends('admin.layouts.master')
@section('main-content')

<style type="text/css">
    
.bt_borrar {
     margin-top: 10px;
}

.bt_borrar a {
    color: red;
}

</style>

<script type="text/javascript">

    count_prod = 0;
    count_serv = 0;

    function suma_total() {

        var sum_prod = 0;
        var sum_serv = 0;

        $('.total_prod').each(function() {

            sum_prod += Number($(this).val());
        });

        $('.total_serv').each(function() {
            sum_serv += Number($(this).val());
        });

        gran_total = sum_prod+sum_serv;
        gran_total = gran_total.toFixed(2);

        $("#total").val(gran_total);
    }

    
    function add_prod() {

        no_prod = $("#add_no_prod").val();
        id_prod = $("#add_producto").val();
        producto = $("#add_producto option:selected").text();
        desc_prod = $("#add_desc_prod").val();
        total_prod = $("#add_total_prod").val();


        if (no_prod=='') {
            alert("Agregar una cantidad");
            return false;
        }

        if (id_prod=='') {
            alert("Agregar un producto");
            return false;
        }

        count_prod++;

        $( "#productos" ).append( '<div id="producto_'+count_prod+'" class="producto col-md-12 row"><div class="col-md-1 form-group"></div><div class="col-md-2 form-group"><input class="form-control" id="no_prod[]" name="no_prod[]" type="number" value="'+no_prod+'" readonly /></div><div class="col-md-3 form-group"><input class="form-control" id="producto[]" name="producto[]" type="text" value="'+producto+'" readonly /><input id="id_prod[]" name="id_prod[]" type="hidden" value="'+id_prod+'"></div><div class="col-md-1 form-group"><input class="form-control" id="desc_prod[]" name="desc_prod[]" type="number" value="'+desc_prod+'" readonly /></div><div class="col-md-2 form-group"><input class="form-control total_prod" id="total_prod[]" name="total_prod[]" type="text" value="'+total_prod+'" readonly /></div><div class="col-md-2 form-group"><div class="bt_borrar"><a href="javascript:borrar_prod('+count_prod+')" title="Borrar"><strong>X</strong></a></div></div></div>' );


        $("#add_no_prod").val('');
        $("#add_producto").val('');
        $("#add_desc_prod").val('');
        $("#add_total_prod").val('');


        suma_total();

    }

    function borrar_prod(id) {
        $("#producto_"+id).remove();

        suma_total();
    }



    function add_serv() {

        no_serv = $("#add_no_serv").val();
        id_serv = $("#add_servicio").val();
        servicio = $("#add_servicio option:selected").text();
        desc_serv = $("#add_desc_serv").val();
        total_serv = $("#add_total_serv").val();


        if (no_serv=='') {
            alert("Agregar horas");
            return false;
        }

        if (id_serv=='') {
            alert("Agregar un servicio");
            return false;
        }

        count_serv++;

        $( "#servicios" ).append( '<div id="servicio_'+count_serv+'" class="servicio col-md-12 row"><div class="col-md-1 form-group"></div><div class="col-md-2 form-group"><input class="form-control" id="no_serv[]" name="no_serv[]" type="number" value="'+no_serv+'" readonly /></div><div class="col-md-3 form-group"><input class="form-control" id="servicio[]" name="servicio[]" type="text" value="'+servicio+'" readonly /><input id="id_serv[]" name="id_serv[]" type="hidden" value="'+id_serv+'"></div><div class="col-md-1 form-group"><input class="form-control" id="desc_serv[]" name="desc_serv[]" type="number" value="'+desc_serv+'" readonly /></div><div class="col-md-2 form-group"><input class="form-control total_serv" id="total_serv[]" name="total_serv[]" type="text" value="'+total_serv+'" readonly /></div><div class="col-md-2 form-group"><div class="bt_borrar"><a href="javascript:borrar_serv('+count_serv+')" title="Borrar"><strong>X</strong></a></div></div></div>' );


        $("#add_no_serv").val('');
        $("#add_servicio").val('');
        $("#add_desc_serv").val('');
        $("#add_total_serv").val('');


        suma_total();

    }

    function borrar_serv(id) {
        $("#servicio_"+id).remove();

        suma_total();
    }




    function valida_form() {

        if (($('.producto').length!=0) || ($('.servicio').length!=0)) {
            return true;
        } else {
            alert('Debes ingresar un producto o servicio.');
            return false;
        }

    }


</script>


<div class="breadcrumb">

    <h1 class="mr-2">Cotizaciones</h1>

                        <ul>
                            <li><a href="{{url('/cotis')}}">Listado</a></li>
                        </ul>    
    
   
</div>
<div class="separator-breadcrumb border-top"></div>


<form method="POST" action="{{ url('cotis') }}" onsubmit="return valida_form();" >

    @csrf

    <div class="row">
        <div class="col-md-12">


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Crear Cotización</h4>
                </div>
                <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
                    <div class="row">
                        <div class="col-md-4 form-group mb-3">
                             <label for="id_empresa">Empresa:</label>
                             <select class="form-control" id="id_empresa" name="id_empresa" required>
                                 <option selected readonly value=""> --- Seleccionar Empresa --- </option>
                                 @foreach($empresas as $empresa)
                                     <option value="{{ $empresa->id }}">{{ $empresa->company }}</option>
                                 @endforeach
                             </select>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4 form-group mb-3">
                             <label for="id_cliente">Cliente:</label>
                             <select class="form-control" id="id_cliente" name="id_cliente" required>
                                 <option selected readonly value=""> --- Seleccionar Cliente --- </option>
                                 @foreach($clientes as $cliente)
                                     <option value="{{ $cliente->id }}">{{ $cliente->clientName }}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-4 form-group mb-3">
                             <label for="id_sede">Sucursal:</label>
                             <select class="form-control" id="id_sede" name="id_sede" required>
                                 <option selected readonly value=""> --- Seleccionar Sucursal --- </option>
                                 @foreach($sedes as $sede)
                                     <option value="{{ $sede->id }}">{{ $sede->namebranch }}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-4 form-group mb-3">
                            <label for="fecha">Fecha</label>
                            <input class="form-control" id="fecha" name="fecha" type="date" placeholder="Fecha" value="{{ date('Y-m-d') }}" required />
                        </div>

                    </div>         




                    <div class="row">


                        <div id="productos" class="col-md-12 row">

                            <div class="col-md-12 form-group">
                                <br>
                                 <label>Productos:</label> 
                            </div>


                            <div class="col-md-12 row">
                                <div class="col-md-1 form-group">
                                </div>

                                <div class="col-md-2 form-group">
                                        <label>cantidad:</label>
                                        <input class="form-control" id="add_no_prod" name="add_no_prod" type="number" value="" />
                                </div>
                                <div class="col-md-3 form-group">
                                        <label>producto:</label>
                                     <select class="form-control" id="add_producto" name="add_producto">
                                         <option selected readonly value=""> --- Seleccionar Producto --- </option>
                                         @foreach($productos as $producto)
                                             <option value="{{ $producto->id }}" data-precio="{{ $producto->precio_cliente }}" >{{ $producto->nombre }} - Q {{ $producto->precio_cliente }} cu</option>
                                         @endforeach
                                     </select>
                                </div>
                                <div class="col-md-1 form-group">
                                        <label>desc. %</label>
                                        <input class="form-control" id="add_desc_prod" name="add_desc_prod" type="number" value="0" size="3" />
                                </div>
                                <div class="col-md-2 form-group">
                                        <label>Q.</label>
                                        <input class="form-control" id="add_total_prod" name="add_total_prod" type="number" value="" readonly />
                                </div>
                                <div class="col-md-2 form-group">
                                    <label>&nbsp;</label>
                                    <button type="button" class="btn btn-primary" onclick="javascript:add_prod();" style="width:70%; margin-top: 25px;">
                                        Agragar
                                    </button>
                                </div>
                            </div>


                        </div>




                        <div id="servicios" class="col-md-12 mt-1 row">

                            <div class="col-md-12 form-group">
                                 <label>Servicios:</label> 
                            </div>

                            <div class="col-md-12 row">
                                <div class="col-md-1 form-group">
                                </div>

                                <div class="col-md-2 form-group">
                                        <label>horas:</label>
                                        <input class="form-control" id="add_no_serv" name="add_no_serv" type="number" value="" />
                                </div>
                                <div class="col-md-3 form-group">
                                        <label>servicio:</label>
                                     <select class="form-control" id="add_servicio" name="add_servicio">
                                         <option selected readonly value=""> --- Seleccionar Servicio --- </option>
                                         @foreach($servicios as $servicio)

                                            @php

                                                if ($servicio->payDT!='' || $servicio->payDT!=0) {
                                                    $servicio_precio = $servicio->payDT;
                                                } else {
                                                    $servicio_precio = $servicio->payNT;
                                                }

                                            @endphp

                                             <option value="{{ $servicio->id }}" data-precio="{{ $servicio_precio }}" >{{ $servicio->nombre }} - Q {{ $servicio_precio }} hr</option>
                                         @endforeach
                                     </select>
                                </div>

                                <div class="col-md-1 form-group">
                                        <label>desc. %</label>
                                        <input class="form-control" id="add_desc_serv" name="add_desc_serv" type="number" value="0" size="3" />
                                </div>

                                <div class="col-md-2 form-group">
                                        <label>Q.</label>
                                        <input class="form-control" id="add_total_serv" name="add_total_serv" type="number" value="" readonly />
                                </div>
                                <div class="col-md-2 form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-primary" type="button" style="width:70%; margin-top: 25px;" onclick="javascript:add_serv();">
                                        Agragar
                                    </button>
                                </div>
                            </div>


                        </div>


                    </div>     
                    
                    <div class="row">
                        <div class="col-md-6 form-group">

                        </div>
                        <div class="col-md-3 form-group mb-3">
                            <label for="total">Total</label>
                            <input class="form-control" id="total" name="total" type="number" value="0.00" placeholder="Total" required readonly />
                        </div>
                    </div>                    

                    <div class="row">
                        <div class="col-md-3 form-group mb-3">
                            <label for="firma">Firma</label>
                            <input class="form-control" id="firma" name="firma" type="text" placeholder="Firma" value="{{ $user->name }}" required />
                        </div>
                    </div>


                    <input type="hidden" id="id_admin" name="id_admin" value="{{ $user->id }}">
                    <input type="hidden" id="status" name="status" value="0">


                </div>
            </div>

            <div class="col-md-12">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Crear Cotización
                </button>
            </div>

        </div>

    </div>

</form>


<script type="text/javascript">


$(document).ready(function() {

    $("#add_producto, #add_no_prod, #add_desc_prod").change(function() {

        if (($("#add_no_prod").val()!='')&&($("#add_producto").val()!='')) {
            temp_total_prod = ($("#add_no_prod").val()) * ($("#add_producto option:selected").attr("data-precio"));

            temp_desc_prod = (100-($("#add_desc_prod").val()))/100;
            temp_total_prod = temp_total_prod*temp_desc_prod;

            temp_total_prod = temp_total_prod.toFixed(2);
            $("#add_total_prod").val(temp_total_prod);
        }
    });


    $("#add_servicio, #add_no_serv, #add_desc_serv").change(function() {

        if (($("#add_no_serv").val()!='')&&($("#add_servicio").val()!='')) {
            temp_total_serv = ($("#add_no_serv").val()) * ($("#add_servicio option:selected").attr("data-precio"));

            temp_desc_serv = (100-($("#add_desc_serv").val()))/100;
            temp_total_serv = temp_total_serv*temp_desc_serv;

            temp_total_serv = temp_total_serv.toFixed(2);
            $("#add_total_serv").val(temp_total_serv);
        }
    });


});


</script>

@endsection
