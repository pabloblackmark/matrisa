@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')
@php 

$mensajes_noleidos = Helper::get_notis();

@endphp
<div class="breadcrumb">
    <h1 class="mr-2">Mensajes</h1>
                        <ul>
                            <li><a href="{{url('/mensajes_grupos')}}">Grupos</a></li>
                        </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


   <div class="row">

@include('admin.mensajes.inc_empresas')

    <div class="col-md-12">
      <div class="card">
                <div class="inbox-main-sidebar-container" data-sidebar-container="main">
                    <div class="inbox-main-content" data-sidebar-content="main" style="width: 80%; float: right;">
                        <!-- SECONDARY SIDEBAR CONTAINER-->

                        <div class="inbox-secondary-sidebar-container box-shadow-1" data-sidebar-container="secondary">
                            <div data-sidebar-content="secondary" style="width: 100%;  float: right;">

                                <div class="inbox-secondary-sidebar-content position-relative" style="min-height: 500px">
                                    <div class="inbox-topbar box-shadow-1 perfect-scrollbar rtl-ps-none pl-3" data-suppress-scroll-y="true">
                                        <!-- <span class="d-sm-none">Test</span>--><a class="link-icon d-md-none" data-sidebar-toggle="main"><i class="icon-regular i-Arrow-Turn-Left"></i></a><a class="link-icon mr-3 d-md-none" data-sidebar-toggle="secondary"><i class="icon-regular mr-1 i-Left-3"></i> Inbox</a>
                                        <div class="d-flex">
                                        <!--
                                        <a class="link-icon mr-3" href=""><i class="icon-regular i-Mail-Reply"></i> Reply</a>>
                                        
                                        <a class="link-icon mr-3" href="">
                                        -->
                                        <h4 class="link-icon mr-3">Nuevo Grupo</h4>
                                        </div>
                                    </div>

                               <div class="inbox-details perfect-scrollbar rtl-ps-none" data-suppress-scroll-x="true">
<form method="POST" action="{{ url('mensajes_grupos') }}" onsubmit="return validate_data()" >

    @csrf
         

                                    <input type="hidden" id="id_admin" name="id_admin" value="{{ $user->id }}">

                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="asunto">Nombre:</label>
                                            <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre del Grupo" required />
                                        </div>

                                    </div>



                                    <div class="row">
                                        <div class="col-md-12 form-group mb-3">
                                             <label for="observaciones">Miembros:</label><br>
                                        </div>
                                        <div id="ids_varios" class="col-md-12 form-group mb-3" style="column-count: 3;">
                                             @foreach($colaboradores as $colaborador)

                                                 <input type="checkbox" id="{{ $colaborador->id }}" name="ids_miembros[]" value="{{ $colaborador->id }}" > 
      &nbsp;<strong>{{ $colaborador->nombres }} {{ $colaborador->apellidos }}</strong> <br>

                                             @endforeach


                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                                                Crear Grupo
                                            </button>
                                        </div>  
                                    </div>
</form>


                                </div>

                            </div>



                                </div>
                        </div>
                    </div>

                    <!-- MAIN INBOX SIDEBAR-->
                    <div class="inbox-main-sidebar" data-sidebar="main" data-sidebar-position="left" style="width: 20%;  float: right;">
                        <div class="pt-3 pr-3 pb-3">
                        <!--    
                            <i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
                        -->
                            <a href="{{ url('/mensajes_grupos/create') }}">
                            <button class="btn btn-rounded btn-primary btn-block mb-4">Crear Grupo</button>
                            </a>
                            <div class="pl-3">
                                <p class="text-muted mb-2"></p>
                                <ul class="inbox-main-nav">
                                     <li><a  href="{{ url('mensajes') }}"><i class="icon-regular i-Mail-2"></i> Mensajes ({{ count($mensajes_noleidos) }})</a></li>

                                    <li><a class="active" href="{{ url('mensajes_grupos') }}"><i class="icon-regular i-Spam-Mail"></i> <strong>Grupos</strong></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>



                </div>


          </div>
        </div>
      </div>

<script type="text/javascript">

      function validate_data() {
        
        // event.preventDefault();
        var validate = 0;
        var selected = [];

           $('#ids_varios input:checked').each(function() {
               selected.push($(this).attr('value'));
           });

             if(selected.length == 0){
               alert('Selecciona al menos un Colaborador para crear el Grupo.');
               return false;
             } else {
                return true;
             }

      }

</script>

    @component('components.messagesForm')
    @endcomponent
@endsection
