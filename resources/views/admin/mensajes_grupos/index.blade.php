@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')
@php 

$mensajes_noleidos = Helper::get_notis();

@endphp
<div class="breadcrumb">
    <h1 class="mr-2">Mensajes</h1>
                        <ul>
                            <li><a href="{{url('/mensajes_grupos')}}">Grupos</a></li>
                        </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


   <div class="row">

@include('admin.mensajes.inc_empresas')

    
    <div class="col-md-12">
      <div class="card">
                <div class="inbox-main-sidebar-container" data-sidebar-container="main">
                    <div class="inbox-main-content" data-sidebar-content="main" style="width: 80%; float: right;">
                        <!-- SECONDARY SIDEBAR CONTAINER-->
                        <div class="inbox-secondary-sidebar-container box-shadow-1" data-sidebar-container="secondary">

                            <!-- Secondary Inbox sidebar-->
                            <div class="inbox-secondary-sidebar perfect-scrollbar rtl-ps-none" data-sidebar="secondary" style="width: 100%;  float: right;">
                                <!--
                                <i class="sidebar-close i-Close" data-sidebar-toggle="secondary" ></i>
                                -->

@forelse($grupos as $grupo)

                                <a href="{{ url('mensajes_grupos') }}/{{ $grupo['id'] }}">
                                <div class="mail-item" >
                                    
                                    <div class="col-xs-6 details">
                                        
                                            <p class="m-0" style="color: black;">{{ $grupo['nombre'] }}</p>
                                        
                                    </div>
                                    <div class="col-xs-3">
                                             <i class="pe-7s-note icon-regular"></i>
                                    </div>
                                    
                                </div>
                                </a>
                              

                @empty
                  <div class="alert alert-success">
                    No hay grupos.
                  </div>

                @endforelse


                            </div>
                        </div>
                    </div>
                    <!-- MAIN INBOX SIDEBAR-->
                    <div class="inbox-main-sidebar" data-sidebar="main" data-sidebar-position="left" style="width: 20%;  float: right;">
                        <div class="pt-3 pr-3 pb-3">
                        <!--    
                            <i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
                        -->
                            <a href="{{ url('/mensajes_grupos/create') }}">
                            <button class="btn btn-rounded btn-primary btn-block mb-4">Crear Grupo</button>
                            </a>
                            <div class="pl-3">
                                <p class="text-muted mb-2"></p>
                                <ul class="inbox-main-nav">
                                     <li><a href="{{ url('mensajes') }}"><i class="icon-regular i-Mail-2"></i> Mensajes ({{ count($mensajes_noleidos) }})</a></li>

                                    <li><a class="active" href="{{ url('mensajes_grupos') }}"><i class="icon-regular i-Spam-Mail"></i> <strong>Grupos</strong></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>



                </div>


          </div>
        </div>
      </div>


    @component('components.messagesForm')
    @endcomponent
@endsection
