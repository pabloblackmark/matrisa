@extends('admin.layouts.master')
@section('main-content')

<style>
.ul-widget-s7::before{
    left: 20.3% !important;
}
</style>

<div class="breadcrumb">
    <h1 class="mr-2">Registro de cambios</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row mb-5">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <a href="{{url()->previous()}}"><h3><< Regresar</h3></a>
            </div>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-lg-8 col-xl-8 mb-4 offset-2">
        <div class="card">
            <div class="card-body">

                <div class="ul-widget__head __g-support v-margin">
                    <div class="ul-widget__head-label">
                        <h3 class="ul-widget__head-title">Últimos cambios</h3>
                    </div>

                    {{-- <button class="btn bg-white _r_btn border-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="_dot _inline-dot bg-primary"></span><span class="_dot _inline-dot bg-primary"></span><span class="_dot _inline-dot bg-primary"></span></button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Separated link</a>
                    </div> --}}
                </div>

                <div class="ul-widget__body">

                    @foreach($data as $d)
                        <div class="ul-widget-s7">
                            <div class="ul-widget-s7__items" style="width:100%;">
                                <span class="ul-widget-s7__item-time" style="width:19%;">{{$d->created_at->format('d/m/Y H:i')}}</span>

                                <div class="ul-widget-s7__item-circle" style="width:11%;">
                                    <p class="badge-dot-warning ul-widget7__big-dot"></p>
                                </div>

                                <div class="ul-widget-s7__item-text" style="width:70%;">
                                    Edición:
                                    {{$d->comment}}
                                    en
                                    <span class="badge badge-pill badge-primary m-2">{{$d->element}}</span>
                                    por
                                    <span class="badge badge-pill badge-success m-2">{{$d->user}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <br>
            </div>
        </div>
    </div>

</div>

    @component('components.messagesForm')
    @endcomponent
@endsection
