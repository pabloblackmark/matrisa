@extends('admin.layouts.master')

@section('main-content')

<style>
    .title_description{
        margin: 10px 0 0 0;
        font-weight: bold;
    }
</style>

<div class="breadcrumb">
    <h1 class="mr-2">Documentos</h1>
    <ul>
        <li><a href="{{url('/documentos')}}">Inicio</a></li>
        <li>Crear documento</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<form method="POST" action="/documentos{{isset($data['id']) ? '/'.$data['id'] : ''}}" enctype="multipart/form-data">

    @csrf
    @if($edit)
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-md-12">

            <h4></h4>
            <div class="card mb-1 col-lg-12">
                <div class="card-body">
                    <div class="row">

                        <div class="col-lg-6 col-md-12 form-group mb-4">
                            <label for="name">Nombre</label>
                            <input class="form-control" id="name" name="name" type="text" placeholder="Nombre del documento" value="{{isset($data['name']) ? $data['name'] : ''}}"/>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-lg-12 col-md-12 form-group mb-4">
                            <label for="description">Descripción</label>
                            <input class="form-control" id="description" name="description" type="text" placeholder="Descripción del documento" value="{{isset($data['description']) ? $data['description'] : ''}}"/>
                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col-12">
                            <h2 for="full-editor" style="text-align: center;">Cuerpo del documento</h2>
                            <p><b>Indicaciones:</b> la plataforma puede sustituír los datos del colaborador en las partes del documento en donde se encuentren las siguientes claves.</p>
                        </div>
                    </div>


                    <div class="row mt-3">

                        <div class="col-3">
                            <ul>
                                {{-- <li><b>$nombres$</b> = Ambos nombres</li>
                                <li><b>$apellidos$</b> = Ambos apellidos</li>
                                <li><b>$dpi$</b> = DPI completo</li> --}}
                                <h5 class="title_description">Representante</h5>
                                <li><b>$nombres_representante$</b> = Nombres del representante</li>
                                <li><b>$edad_representante$</b> = Edad del representante</li>
                                <li><b>$genero_representante$</b> = Género del representante</li>
                                <li><b>$estado_civil_representante$</b> = Estado civil del representante</li>
                                <li><b>$nacionalidad_representante$</b> = Nacionalidad del representante</li>
                                <li><b>$municipio_representante$</b> = Municipio del representante</li>
                                <li><b>$dpi_representante$</b> = DPI del representante</li>
                                <li><b>$municipio_representante$</b> = Municipio del representante</li>
                                <li><b>$departamento_representante$</b> = Departamento del representante</li>

                                <h5 class="title_description">Empresa</h5>
                                <li><b>$empresa$</b> = Empresa en donde está el colaborador</li>
                                <li><b>$nit_empresa$</b> = Nit de la empresa</li>
                                <li><b>$direccion_empresa$</b> = Dirección de la empresa</li>

                                <h5 class="title_description">Empleado</h5>
                                <li><b>$nombres_empleado$</b> = Nombres del empleado</li>
                                <li><b>$apellidos_empleado$</b> = Aapellidos del empleado</li>
                                <li><b>$edad_empleado$</b> = Edad del empleado</li>
                                <li><b>$genero_empleado$</b> = Género del empleado</li>
                                <li><b>$estado_civil_empleado$</b> = Estado civil del empleado</li>
                                <li><b>$dpi_empleado$</b> = DPI del empleado</li>
                                <li><b>$nacionalidad_empleado$</b> = Nacionalidad del empleado</li>
                                <li><b>$direccion_empleado$</b> = Dirección del empleado</li>

                                <h5 class="title_description">Colaborador</h5>
                                <li><b>$dia_alta_colaborador$</b> = campo fecha de ingreso en módulo colaborador</li>
                                <li><b>$mes_alta_colaborador$</b> = campo fecha de ingreso en módulo colaborador</li>
                                <li><b>$year_alta_colaborador$</b> = campo fecha de ingreso en módulo colaborador</li>
                                <li><b>$puesto_de_trabajo$</b> = puesto ingresado en módulo colaboradores</li>
                                <li><b>$descripcion_puesto_de_trabajo$</b> = descripción de puesto ingresado en módulo colaboradores</li>
                                <li><b>$tipo_contrato$</b> = tipo de contrato en módulo colaboradores</li>
                                <li><b>$sueldo_mensual$</b> = campo base diaria x 30 en módulo colaboradores</li>
                                <li><b>$bono_ley$</b> = campo bono de ley en módulo colaboradores</li>

                                <h5 class="title_description">Fechas</h5>
                                <li><b>$hoy$</b> = el día de hoy</li>
                                <li><b>$mes_actual$</b> = el mes actual</li>
                                <li><b>$year_actual$</b> = el año actual</li>
                            </ul>
                        </div>


                        <div class="col-9">
                            <div class="mx-auto col-md-12 mb-5" style="height: 900px;">
                                <div id="full-editor" onchange="changeMe(this);">
                                    {!! isset($data['body']) ? $data['body'] : '' !!}
                                </div>
                                <input id="full-editor-data" name="body" type="hidden">
                            </div>
                        </div>



                    </div>
                </div>
            </div>




            <div class="col-md-12">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    {{ $edit ? 'Guardar cambios' : 'Guardar'}}
                </button>
            </div>

        </div>

    </div>

</form>

@endsection

@section('bottom-js')
<script>
$(document).ready(function () {
    $('#full-editor-data').val(`{!! isset($data['body']) ? $data['body'] : '' !!}`)
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });
});

</script>
@endsection
