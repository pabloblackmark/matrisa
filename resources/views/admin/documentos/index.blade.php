@extends('admin.layouts.master')
@section('main-content')


<div class="breadcrumb">
    <h1 class="mr-2">Documentos</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
<div class="col-md-12">
  <div class="card">
    <div class="card-header">
		<h3 class="card-title">
            <a href="{{url("documentos/create")}}">
            <i class="ion-ios7-plus-outline "></i>
            &nbsp;&nbsp;Nuevo documento</a>
        </h3>
	</div>
    <div class="card-body">
      <div class="table-responsive">
        <table id="documentos_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre del documento</th>
              <th>Descripción</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($data as $value)
              @php $perms=array(); @endphp
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td >{{$value->name}}</td>
                <td >{{$value->description}}</td>
                <td class="td-actions  text-left" style="font-size: 20px;">
                  <a  href="{{ url('documentos/' . $value->id) }}">
                      <i class="pe-7s-note "></i>
                  </a>
                  <a  href="javascript:"
                      onclick="deleteD('{{$value->id}}','{{ csrf_token() }}');">
                      <i class="pe-7s-trash "></i>
                  </a>
                </td>
              </tr>
              @php
              $criti[$value->id]=array($value->nombres);
              @endphp
            @endforeach
					    </tbody>
           </table>
          </div>
        </div>
      </div>
    </div>
</div>

@endsection


@section('bottom-js')
    <script>
        $('#documentos_table').DataTable();
    </script>
@endsection
