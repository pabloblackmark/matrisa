@extends('admin.layouts.master')

@section('main-content')

@php  $creat=array();
  $colors = ["ausencia"=>'#f14848',"sin horario"=>'#a99944',"asistencia"=>'#38b6f5',
            "Permiso"=>'#ecffef',"Vacaciones"=>'#f3fbff',"Iggs"=>'#f3fbff'];
@endphp
<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{ url('reporte_clientes') }}?id_empresa={{ $request->id_empresa }}&id_cliente={{ $request->id_cliente }}&inicio={{ $request->inicio }}&fin={{ $request->fin }}">Reporte Clientes</a> </h1>
    <ul>
        <li>Detalle de Pago</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Reporte del Colaborador</div>
      </div>
      <div class="card-body">
          <div class="row">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

@php
                    $horas_dia = $reporte_colab["horas_dia"];
                    $horas_noche = $reporte_colab["horas_noche"];
                    $horas_mixta = $reporte_colab["horas_mixta"];
                    $pago_horas_dia = $reporte_colab["pago_horas_dia"];
                    $pago_horas_noche = $reporte_colab["pago_horas_noche"];
                    $pago_horas_mixta = $reporte_colab["pago_horas_mixta"];
                    $pago_total_horas = $reporte_colab["pago_total_horas"];
                                                
                    $extras_dia = $reporte_colab["extras_dia"];
                    $extras_noche = $reporte_colab["extras_noche"];
                    $extras_mixta = $reporte_colab["extras_mixta"];
                    $extras_especial = $reporte_colab["extras_especial"];
                    $pago_extras_dia = $reporte_colab["pago_extras_dia"];
                    $pago_extras_noche = $reporte_colab["pago_extras_noche"];
                    $pago_extras_mixta = $reporte_colab["pago_extras_mixta"];
                    $pago_extras_especial = $reporte_colab["pago_extras_especial"];
                    $pago_total_extras = $reporte_colab["pago_total_extras"];

                    $viaticos = $reporte_colab["viaticos"];
                    $viaticos_movil = $reporte_colab["viaticos_movil"];
                    $viaticos_extra = $reporte_colab["viaticos_extra"];
                    $pago_viaticos = $reporte_colab["pago_viaticos"];
                    $pago_viaticos_movil = $reporte_colab["pago_viaticos_movil"];
                    $pago_viaticos_extra = $reporte_colab["pago_viaticos_extra"];
                    $pago_total_viaticos = $reporte_colab["pago_total_viaticos"];

                    $total = $reporte_colab["total"];

                    $pago_total_horas = number_format($pago_total_horas,2,'.',',');
                    $pago_total_extras = number_format($pago_total_extras,2,'.',',');
                    $pago_total_viaticos = number_format($pago_total_viaticos,2,'.',',');

                    $total = number_format($total,2,'.',',');

                    $productos = number_format(0,2,'.',',');
                    $equipo = number_format(0,2,'.',',');

@endphp
            <div class="col-3">
                <label  style="font-weight:bold;">Código:</label>
                {{ $colabInfo->id }}<br>
                <label  style="font-weight:bold;">Nombre: </label>
                {{ $colabInfo->nombres.' '.$colabInfo->apellidos }}<br>
                <label  style="font-weight:bold;">Estado: </label>
                {{ $colabInfo->estatus}}<br>
                <label  style="font-weight:bold;">Puesto: </label>
                {{ $colabInfo->puesto }}<br>                  
                <label  style="font-weight:bold;">Fecha de Inicio: </label>
                {{ date("d-m-Y", strtotime($request->inicio)) }}<br>                  
                <label  style="font-weight:bold;">Fecha de Fin: </label>
                {{ date("d-m-Y", strtotime($request->fin)) }}
                <br>
                <br>
            </div>

            <div class="col-2">

              <h5>Horas Laborales</h5><br>

              &nbsp;&nbsp;&nbsp;<label>Diurna:</label>
              Q. {{ $pago_horas_dia }} ({{ $horas_dia }} horas)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Nocturna:</label>
              Q. {{ $pago_horas_noche }} ({{ $horas_noche }} horas)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Mixta:</label>
              Q. {{ $pago_horas_mixta }} ({{ $horas_mixta }} horas)
              <br>

              <br>
              &nbsp;&nbsp;&nbsp;<label style="font-weight:bold;">TOTAL:</label>
              <strong>Q. {{ $pago_total_horas }}</strong>
              <br>


            </div>

            <div class="col-2">

              <h5>Horas Extras</h5><br>

              &nbsp;&nbsp;&nbsp;<label>Diurna:</label>
              Q. {{ $pago_extras_dia }} ({{ $extras_dia }} extras)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Nocturna:</label>
              Q. {{ $pago_extras_noche }} ({{ $extras_noche }} extras)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Mixta:</label>
              Q. {{ $pago_extras_mixta }} ({{ $extras_mixta }} extras)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Especial:</label>
              Q. {{ $pago_extras_especial }} ({{ $extras_especial }} extras)
              <br>

              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">TOTAL:</label>
              <strong>Q. {{ $pago_total_extras }}</strong>


            </div>

            <div class="col-2">

              <h5>Viáticos</h5><br>

              &nbsp;&nbsp;&nbsp;<label>Normal:</label>
              Q. {{ $pago_viaticos }} ({{ $viaticos }} viáticos)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Movil:</label>
              Q. {{ $pago_viaticos_movil }} ({{ $viaticos_movil }} viáticos)
              <br>
              &nbsp;&nbsp;&nbsp;<label>Extras:</label>
              Q. {{ $pago_viaticos_extra }}
              <br>
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">TOTAL:</label>
              <strong>Q. {{ $pago_total_viaticos }}</strong>              

            </div>


            <div class="col-2">
              <h4  style="font-weight:bold;">Total:
              Q. {{ $total }}
              </h4>
            </div>
 
          </div>
      </div>
  </div>


 </div>
</div>



  <div class="card o-hidden mb-4">
    <div class="card-header bg-transparent">
        <div class="card-title mb-0">Detalle</div>
        <a href="{{ url('/') }}/colaboradores/companyReports/{{ $request->id_colab }}/{{ $request->id_empresa }}/" target="_blanck"><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1px" stroke-linecap="round" stroke-linejoin="round" class="feather feather-watch text-20"><circle cx="12" cy="12" r="7"></circle><polyline points="12 9 12 12 13.5 13.5"></polyline><path d="M16.51 17.35l-.35 3.83a2 2 0 0 1-2 1.82H9.83a2 2 0 0 1-2-1.82l-.35-3.83m.01-10.7l.35-3.83A2 2 0 0 1 9.83 1h4.35a2 2 0 0 1 2 1.82l.35 3.83"></path></svg> Editar Horarios</a>
    </div>

    <div class="card-body">
      <div class="row">
        <div class="col-12" style="overflow-x:scroll">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Horas Laboradas</th>
                <th>Horas Extras</th>
                <th>Viáticos</th>
              </tr>
            </thead>
            <tbody>

          @foreach($log AS $elems)

                <tr >
                  <td>{{$elems->dayWeek}}<br>
                    @php
                    $mes =  ["1"=>"Enero","2"=>"Febrero",
                            "3"=>"Marzo","4"=>"Abril",
                            "5"=>"Mayo","6"=>"Junio","7"=>"Julio",
                            "8"=>"Agosto","9"=>"Septiembre","10"=>"Octubre",
                            "11"=>"Noviembre","12"=>"Diciembre"
                          ];
                        $fech = date("d/m/Y",strtotime($elems["dateDay"]));
                          $fech = explode("/",$fech);
                    @endphp
                     {{$fech[0]." ".$mes[(INT)$fech[1]]." ".$fech[2]}} <br>
                     {{$elems->getInfoClient()->first()->clientName}}<br>

                  </td>
@php

    $horarioInfo = $elems->getHorary()->first();

    $horas_dia = (int)$horarioInfo->horas_diurnas/60;
    $horas_noche = (int)$horarioInfo->horas_nocturnas/60;
    $horas_mixta = (int)$horarioInfo->horas_mixtas/60;


    $pago_horas_dia = $horas_dia * $puesto->payDT;
    $pago_horas_noche = $horas_noche * $puesto->payNT;
    $pago_horas_mixta = $horas_mixta * $puesto->payDT;


@endphp

                  <td>
                      <span style="color:black;font-weight:bold;">Diurna</span>:
                      Q.{{ number_format( $pago_horas_dia ,2,'.',',') }} ({{ $horas_dia }} horas)
                      <hr class="mb-1 mt-1">
                      <span style="color:black;font-weight:bold;">Nocturna</span>:
                      Q.{{ number_format( $pago_horas_noche ,2,'.',',') }} ({{ $horas_noche }} horas)
                      <hr class="mb-1 mt-1">
                      <span style="color:black;font-weight:bold;">Mixta</span>:
                      Q.{{ number_format( $pago_horas_mixta ,2,'.',',') }} ({{ $horas_mixta }} horas)

                  </td>


@php
  
    $extras_dia = $elems->extraHourDayMiddle/2;
    $extras_noche = $elems->extraHourNightMiddle/2;
    $extras_mixta = $elems->extraHourMixMiddle/2;
    $extras_especial = $elems->aditTask;

    $pago_extras_dia = $extras_dia * $puesto->payEDT;
    $pago_extras_noche = $extras_noche * $puesto->payENT;
    $pago_extras_mixta = $extras_mixta * $puesto->extraPay;
    $pago_extras_especial = $extras_especial * $puesto->points;

@endphp
                  <td>
                      <span style="color:black;font-weight:bold;">Diurna</span>:
                      Q.{{ number_format( $pago_extras_dia ,2,'.',',') }} ({{ $extras_dia }} extras)
                      <hr class="mb-1 mt-1">
                      <span style="color:black;font-weight:bold;">Nocturna</span>:
                      Q.{{ number_format( $pago_extras_noche ,2,'.',',') }} ({{ $extras_noche }} extras)
                      <hr class="mb-1 mt-1">
                      <span style="color:black;font-weight:bold;">Mixta</span>:
                      Q.{{ number_format( $pago_extras_mixta ,2,'.',',') }} ({{ $extras_mixta }} extras)
                      <hr class="mb-1 mt-1">
                      <span style="color:black;font-weight:bold;">Especial</span>:
                      Q.{{ number_format( $pago_extras_especial ,2,'.',',') }} ({{ $extras_especial }} metas)

                  </td>

@php
  
    $viaticos = $elems->viatics;
    $viaticos_movil = $elems->viaticsMovil;
    $viaticos_extra = $elems->viaticsAdded;

    $pago_viaticos = $viaticos * $puesto->perDiem;
    $pago_viaticos_movil = $viaticos_movil * $puesto->movil;
    $pago_viaticos_extra = $viaticos_extra + ( $viaticos_extra * ($puesto->viaticos_extra/100) );;

@endphp

                  <td>
                      <span style="color:black;font-weight:bold;">Viaticos:</span>
                      Q.{{ number_format( $pago_viaticos ,2,'.',',') }}</span> ({{ $viaticos }} viático)
                      <hr class="mb-1 mt-1">
                      <span style="color:black;font-weight:bold;">Moviles:</span>
                      Q.{{ number_format( $pago_viaticos_movil ,2,'.',',') }} ({{ $viaticos_movil }} viático)
                      <hr class="mb-1 mt-1">
                      <span style="color:black;font-weight:bold;">Extras:</span>
                      Q.{{ number_format( $pago_viaticos_extra ,2,'.',',') }} <br>

                      @if ($elems->viaticsDesc!='')
                      <span style="color:black;font-weight:bold;">  Descripción: </span>{{!empty($elems->viaticsDesc)?$elems->viaticsDesc:''}}
                      @endif
                  </td>

                </tr>

          @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>


  </div>




<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
h5{
  font-weight: bold;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
hr{

}
</style>
@endsection

@section('page-js')



@endsection

@section('bottom-js')

{{-- <script src="{{asset('assets/js/quill.script.js')}}"></script> --}}
<script>
$(document).ready(function () {
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });

});



  function changeLog(){
      Swal.fire({
          title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                    <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                </div>`,
          html:`
               <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                   <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                       <div class="modal-body">
                           <div class="row">
                               <label class="col-form-label">Comentario</label>
                               <textarea id="changeLogComment" class="form-control"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               `,
           preConfirm: function(){
            $('#bt_actualizar').hide();

               let val = $('#changeLogComment').val(),
                   cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`;
             $('#form_viaticos').append(cad).attr('onsubmit', '').submit();
             return true
         }
      })
  }



</script>


@endsection
