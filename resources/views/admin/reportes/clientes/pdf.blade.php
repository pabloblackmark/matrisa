<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Boleta de Pago</title>

<style type="text/css">

body, html {
    margin: 0;
    padding: 0;
}


</head>

<body style="width: 540px;">
<table width="520" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-right: auto; margin-left: auto; padding: 40px;">
  <tr>
    <td><table width="520" border="0">
      <tr style="font-size:18px;">
        <td align="center">Boleta de pago - {{ $nombre_empresa }}</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left"><hr /></td>
  </tr>
  <tr>
    <td><table width="520" border="0">
      <tr>
<!--      
        <td align="center">--- Del: {{ $desde }} al {{ $hasta }} ---</td>
-->
        <td align="center">--- Fecha de Pago: {{ $fecha_pago }} ---</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><hr /></td>
  </tr>
  <tr>
    <td><table width="520" border="0">
      <tr>
        <td>Nombre: {{ $nombre_colab }}</td>
        <td>ID: {{ $id_colab }}</td>
        <td>{{ $tipo_pago }}</td>
        <td>{{ $estatus }}</td>
      </tr>
    </table>
      <table width="520" border="0">
        <tr>
          <td>Puesto: {{ $puesto }}</td>
<!--          
          <td align="right">Horas extras y productividad correspondientes a las semanas 38, 39 del año 2021</td>
-->
          <td align="right">Tipo de Pago: {{ $pago_sueldo }}</td>
        </tr>
      </table>
      <table width="520" border="0">
        <tr>
          <td>Recibí de: {{ $nombre_empresa }}</td>
          <td align="right">La cantidad de: *****Q {{ $total }}*****</td>
        </tr>
    </table>      <hr />    <br /></td>
  </tr>
  <tr>
    <td><table width="520" border="0">
      <tr>
        <td align="center">( + ) Desglose</td>
        <td align="center">( - ) Descuentos</td>
      </tr>
      <tr>
        <td><table width="306" border="0">
          <tr>
            <td width="151" align="right">Días laborados:</td>
            <td width="145" align="left">{{ $desde }} al {{ $hasta }}</td>
          </tr>
          <tr>
            <td align="right">Base devengado:</td>
            <td align="left">Q {{ $sueldo }}</td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td align="right">Bono de Ley:</td>
            <td align="left">Q {{ $planilla['bono_ley'] }}</td>
          </tr>
          <tr>
            <td align="right">Bono Extra</td>
            <td align="left">Q {{ $planilla['bono_extra'] }}</td>
          </tr>
          <tr>
            <td align="right">Bono de Antiguedad:</td>
            <td align="left">Q {{ $planilla['bono_antiguedad'] }}</td>
          </tr>

<!--          
          <tr>
            <td align="right">Total bonificación 78-79:</td>
            <td align="left">Q {{ $bono_ley }}</td>
          </tr>
          <tr>
            <td align="right">Vacaciones gozadas:</td>
            <td align="left">Q 0.00</td>
          </tr>
-->
          <tr>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td align="right">Horas Extras:</td>
            <td align="left">{{ date("d-m-Y", strtotime($horas['fecha_inicio'])) }} al {{ date("d-m-Y", strtotime($horas['fecha_fin'])) }}</td>
          </tr>
          <tr>
            <td align="right">Diurna:</td>
            <td align="left">Q {{ $horas['extra_dia'] }}</td>
          </tr>
          <tr>
            <td align="right">Nocturna:</td>
            <td align="left">Q {{ $horas['extra_noche'] }}</td>
          </tr>
          <tr>
            <td align="right">Especial:</td>
            <td align="left">Q {{ $horas['extra_especial'] }}</td>
          </tr>
          <tr>
            <td align="right">Mixta:</td>
            <td align="left">Q {{ $horas['extra_mixta'] }}</td>
          </tr>
          <tr>
            <td align="right">Metas:</td>
            <td align="left">Q {{ $horas['extra_metas'] }}</td>
          </tr>


          <tr>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>          
          <tr>
            <td align="right">Viáticos:</td>
            <td align="left">{{ date("d-m-Y", strtotime($viaticos['fecha_inicio'])) }} al {{ date("d-m-Y", strtotime($viaticos['fecha_fin'])) }}</td>
          </tr>
          <tr>
            <td align="right">Normal:</td>
            <td align="left">Q {{ $viaticos['viaticos'] }}</td>
          </tr>
          <tr>
            <td align="right">Movil:</td>
            <td align="left">Q {{ $viaticos['movil'] }}</td>
          </tr>
          <tr>
            <td align="right">Extras:</td>
            <td align="left">Q {{ $viaticos['extras'] }}</td>
          </tr>

        </table></td>
        <td valign="top"><table width="306" border="0">
          <tr>
            <td width="150" align="right">Seguro Social:</td>
            <td width="146" align="left">Q {{ $desc_igss }}</td>
          </tr>

          <tr>
            <td align="right">Días:</td>
            <td align="left">Q {{ $planilla['desc_dia'] }}</td>
          </tr>
          <tr>
            <td align="right">Septimo:</td>
            <td align="left">Q {{ $planilla['desc_septimo'] }}</td>
          </tr>

@foreach ($planilla['descuentos'] as $descuento)
          <tr>
            <td align="right" style="text-transform: capitalize;">{{ $descuento['tipo'] }}:</td>
            <td align="left">Q {{ $descuento['pago'] }}</td>
          </tr>
@endforeach

<!--
          <tr>
            <td align="right">Equipo, herramients<br />
              y/o uniforme</td>
            <td align="left">Q 0.00</td>
          </tr>
          <tr>
            <td align="right">Préstamo bancario:</td>
            <td align="left">Q 0.00</td>
          </tr>
          <tr>
            <td align="right">Otros:</td>
            <td align="left">Q 0.00</td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
-->
          </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><hr /></td>
  </tr>
  <tr>
    <td><table width="520" border="0" style="font-size:16px">
      <tr>
        <td align="center">Total ingresos: Q {{ $total_ingresos }}</td>
        <td align="center">Total descuentos: Q {{ $total_descuentos }}</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><hr /></td>
  </tr>
  <tr>
    <td><table width="520" border="0" style="font-size:16px">
      <tr>
        <td align="center"><table width="306" border="0" style="font-size:12px;">
          <tr>
            <td align="center">
              <!-- <img src="firma.jpg" width="200" alt="firma" /> -->
              <img src="data:image/jpg;base64,{{ base64_encode(file_get_contents(public_path('images/firma.jpg'))) }}" />
            </td>
          </tr>
          <tr>
            <td align="center">Recibí Conforme</td>
          </tr>
        </table></td>
        <td align="center"><strong>Líquido a recibir: ***Q {{ $total }}***</strong></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="520" border="0">
      <tr>
        <td align="center">Pago realizado a través de transferencia 029 0070001 7</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
