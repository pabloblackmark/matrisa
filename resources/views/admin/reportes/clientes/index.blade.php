@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Reporte de Clientes</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">
            <div class="card-body">


              <form id="form_filtro" action="" method="get">
                @csrf

              <div class="row">
                  <div class="col-md-3">
                    Empresa:<br>
                      <select class="form-control" id="id_empresa" name="id_empresa" required>
                        <option value="" selected disabled>-- seleccionar empresa --</option>
                        @foreach ($companies as $k => $v)
                          <option value="{{$v[0]}}" {{(isset($request->id_empresa)&&$v[0]==$request->id_empresa?'selected':'')}}>{{$v[1]}}</option>
                        @endforeach
                      </select>
                  </div>


                  <div class="col-md-3">
                    Cliente:<br>
                      <select class="form-control" id="id_cliente" name="id_cliente" required>
                        {!! $clientes !!}
                      </select>
                  </div>

    <div class="col-lg-2">
      Fecha Inicio
      <input type="date" name="inicio" id="inicio" class="w-100 form-control" value="{{ @$request->inicio }}" required>
    </div>
    <div class="col-lg-2">
      Fecha Fin
      <input type="date" name="fin" id="fin" class="w-100 form-control" value="{{ @$request->fin }}" required>
    </div>

    <div class="col-lg-2">
        <button type="submit" form="form_filtro" value="filtrar" class="btn btn-primary text-white btn-rounded" style="margin-top: 20px;">Filtrar</button>
    </div>


              </div>

              </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
    		  <h3 class="card-title  mb-0">
            Colaboradores
          </h3>
    		</div>
        <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


          <form id="form_pago" action="{{ url('aprobar_viaticos') }}" method="post" onsubmit="return validate_data()">
          @csrf

          <div class="table-responsive">
            <table id="asignar_colaboradores_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <!--
                  <th >&nbsp;</th>
                -->
                  <th>Código</th>
                  <th>Colaborador</th>                  
                  <th>Horas Laboradas</th>
                  <th>Horas Extras</th>
                  <th>Viáticos</th>
                  <th>Producto</th>
                  <th>Equipo</th>
                  <th>TOTAL</th>
<!--                  
                  <th>Estado</th>
-->
                  <th>Detalle</th>
                </tr>
              </thead>
              <tbody>
@php

$gran_total = 0;

@endphp

                @if(isset($colabs))
                  @foreach ($colabs as $value)
                    @php

                    // info colaborador
                    $colabInfo = $value->getColabsInfo()->first();

                    $request->id_colab = $value->idColab;

                    $reporte_colab = App\Http\Controllers\Admin\reportes\clientesController::get_reporte_colab($request);

                    $horas_dia = $reporte_colab["horas_dia"];
                    $horas_noche = $reporte_colab["horas_noche"];
                    $horas_mixta = $reporte_colab["horas_mixta"];
                    $pago_horas_dia = $reporte_colab["pago_horas_dia"];
                    $pago_horas_noche = $reporte_colab["pago_horas_noche"];
                    $pago_horas_mixta = $reporte_colab["pago_horas_mixta"];
                    $pago_total_horas = $reporte_colab["pago_total_horas"];
                                                
                    $extras_dia = $reporte_colab["extras_dia"];
                    $extras_noche = $reporte_colab["extras_noche"];
                    $extras_mixta = $reporte_colab["extras_mixta"];
                    $extras_especial = $reporte_colab["extras_especial"];
                    $pago_extras_dia = $reporte_colab["pago_extras_dia"];
                    $pago_extras_noche = $reporte_colab["pago_extras_noche"];
                    $pago_extras_mixta = $reporte_colab["pago_extras_mixta"];
                    $pago_extras_especial = $reporte_colab["pago_extras_especial"];
                    $pago_total_extras = $reporte_colab["pago_total_extras"];

                    $viaticos = $reporte_colab["viaticos"];
                    $viaticos_movil = $reporte_colab["viaticos_movil"];
                    $viaticos_extra = $reporte_colab["viaticos_extra"];
                    $pago_viaticos = $reporte_colab["pago_viaticos"];
                    $pago_viaticos_movil = $reporte_colab["pago_viaticos_movil"];
                    $pago_viaticos_extra = $reporte_colab["pago_viaticos_extra"];
                    $pago_total_viaticos = $reporte_colab["pago_total_viaticos"];

                    $total = $reporte_colab["total"];



                    $gran_total = $gran_total + $total;

                    $pago_total_horas = number_format($pago_total_horas,2,'.',',');
                    $pago_total_extras = number_format($pago_total_extras,2,'.',',');
                    $pago_total_viaticos = number_format($pago_total_viaticos,2,'.',',');

                    $total = number_format($total,2,'.',',');

                    $productos = number_format(0,2,'.',',');
                    $equipo = number_format(0,2,'.',',');

@endphp


                      <tr>


                        <td >{{ $value->idColab }}</td>
                        <td >{{ $colabInfo->nombres }} {{ $colabInfo->apellidos }}
                          <br><small><i>{{ $colabInfo->puesto }}</i></small></td>

                        <td>Q.{{ $pago_total_horas }}<br>

                          <br>
                          día Q.{{ $pago_horas_dia }} ({{ $horas_dia }} horas)<br>
                          noche Q.{{ $pago_horas_noche }} ({{ $horas_noche }} horas)<br>
                          mixta Q.{{ $pago_horas_mixta }} ({{ $horas_mixta }} horas)<br>

                        </td>

                        <td>Q.{{ $pago_total_extras }}<br>

                          <br>
                          día Q.{{ $pago_extras_dia }} ({{ $extras_dia }} extras)<br>
                          noche Q.{{ $pago_extras_noche }} ({{ $extras_noche }} extras)<br>
                          mixta Q.{{ $pago_extras_mixta }} ({{ $extras_mixta }} extras)<br>
                          especial Q.{{ $pago_extras_especial }} ({{ $extras_especial }} metas)<br>


                        </td>

                        <td>Q.{{ $pago_total_viaticos }}<br>

                          <br>
                          viáticos Q.{{ $pago_viaticos }} ({{ $viaticos }} viáticos)<br>
                          movil Q.{{ $pago_viaticos_movil }} ({{ $viaticos_movil }} viáticos)<br>
                          extra Q.{{ $pago_viaticos_extra }} <br>
                        </td>

                        <td>Q.{{ $productos }}</td>

                        <td>Q.{{ $equipo }}</td>


                        <td><strong>Q.{{ $total }}</strong></td>

<!--
                        <td>
                          @if(!empty($estado_pago->estado))
                            <span class="badge badge-pill {{($estado_pago->estado=='Rechazado'?'badge-danger':'badge-success')}} m-2">
                            {{ $estado_pago->estado }}</span>
                          @else
                            No procesado
                          @endif
                        </td>
-->

                        <td class="td-actions  text-left" style="font-size: 20px;">
                          <a  href="{{ url('reporte_colab') }}?id_empresa={{ $request->id_empresa }}&id_colab={{$value->idColab}}&id_cliente={{ $request->id_cliente }}&inicio={{ $request->inicio }}&fin={{ $request->fin }}" title="Ver detalle" target="_blank">
                              <i data-feather="eye"></i>
                          </a>
                        </td>
                        


                      </tr>

                  @endforeach
                @endif
					    </tbody>
           </table>

@if ($gran_total>0)
@php

$gran_total = number_format($gran_total,2,'.',',');

@endphp
<h1 style='text-align: right;'>Total: Q {{ $gran_total }}</h1>

@endif

          </div>

            <div class="col-md-12">
                <a href="{{ url('reporte_clientes/export_pdf')}}?id_pago={{ @$log_pago->id }}" target="_blank">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Descargar PDF
                </button>
                </a>
            </div>


<!--
@if ($request->id_empresa!='')

          <input type="hidden" id="id_empresa" name="id_empresa" value="{{ $request->id_empresa }}">
          <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $request->id_cliente }}">
          <input type="hidden" id="id_sucursal" name="id_sucursal" value="{{ $request->id_sucursal }}">
          <input type="hidden" id="id_area" name="id_area" value="{{ $request->id_area }}">
          <input type="hidden" id="inicio" name="inicio" value="{{ $request->inicio }}">
          <input type="hidden" id="fin" name="fin" value="{{ $request->fin }}">

          <div class="text-left">
              <input type="checkbox" id="pago_todos" name="pago_todos" value="todos"> SELECCIONAR TODOS
          </div>
          <div class="text-center">
            
              <div style="width: 200px; margin-left: auto; margin-right: auto;">
                  Fecha de Pago
                  <input type="date" name="fecha_pago" id="fecha_pago" class="w-100 form-control" value="" required>
              </div>
              <br>

              <button type="submit" form="form_pago" value="pago" class="btn btn-primary text-white btn-rounded">Procesar PAGO</button>
          </div>

@endif
-->
        </form>

        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">


$(function() {

    $('#id_empresa').on('change', function() {


        $('#id_cliente').html('');
        $('#id_sucursal').html('');

        id_empresa = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_clientes')}}',
           data:{
             id_empresa: id_empresa,
           },
           success:function(data) {

                $('#id_cliente').html(data);     
           }
        });

    });




    $("#pago_todos").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });


});


    function validate_data() {

        var selected = [];

         $('#asignar_colaboradores_table input:checked').each(function() {
             selected.push($(this).attr('value'));
         });


        if(selected.length == 0){
            alert('Debes Selecciona al menos un Colaborador para continuar.');
            return false;
        } else {
            return true;
        }

    }


</script>
<style media="screen">
  small{
    font-weight:bold;
  }
</style>
@endsection

@section('bottom-js')
    <script>
        $('#asignar_colaboradores_table').DataTable();
    </script>
@endsection
