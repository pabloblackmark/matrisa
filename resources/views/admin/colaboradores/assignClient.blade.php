@extends('admin.layouts.master')
@section('main-content')


<script type="text/javascript">
    var kad={
             0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
             1:{type:"txt",tl:"Colaborador",nm:"idColabName",id:"idColabName",nxt:6},
             2:{type:"hddn",tl:"Colaborador",nm:"idColab",id:"idColab",elv:"0",nxt:6},
             3:{type:"slct",tl:"Puesto con horarios",nm:"idPuesto",id:"idPuesto",elv:"1",vl:@json($puestos),nxt:6},
             4:{type:"slct",tl:"Tipo",nm:"tipo",id:"tipo",elv:"2",vl:[['', '- - -'],['fijo', 'Fijo'],['temporal', 'Temporal']],add:'onchange="selectTipo(this)"',nxt:12},
             5:{type:"label",tl:"Periodo",nm:"periodo",id:"periodo",elv:"3",nxt:12},
             6:{type:"dtmpckr",tl:"Inicio",nm:"inicio",id:"inicio",elv:"3",nxt:6},
             7:{type:"dtmpckr",tl:"Fin",nm:"fin",id:"fin",elv:"3",nxt:6},
             8:{type:"slct",tl:"Sede",nm:"idBranch",id:"sede",elv:"4",vl:@json($sede),add:'onchange="changeSelectSede(this)"',nxt:6},
             9:{type:"slct",tl:"Área",nm:"idArea",id:"area",elv:"5",vl:[['', '- - -']],nxt:6},
             10:{type:"hddn",nm:"idAssocColab",elv:"6",vl:"{{$idAssocCLient}}"  },
            };
            // 8:{type:"txt",tl:"Usuario",nm:"",elv:"6",add:" placeholder=\"Generado por el sistema\" disabled",nxt:6},
            // 9:{type:"txt",tl:"Contrasena",nm:"",elv:"7",add:" placeholder=\"Generado por el sistema\" disabled",nxt:6},
    valdis={clase:"red",text:1, select:1};
    var vis = [
            {type:"text", tl:"Puesto", elv:"puesto", nxt:4},
            {type:"text", tl:"Colaborador", elv:"colaborador", nxt:4},
            {type:"text", tl:"Tipo", elv:"tipo", nxt:4},
            {type:"text", tl:"Inicia", elv:"inicio", nxt:6},
            {type:"text", tl:"Termina", elv:"fin", nxt:6},
            {type:"label", tl:"Metas"},
            {type:"array", elv:"tareas", elv2:"name"},
            {type:"label", tl:"Valor cobro al cliente"},
            {type:"text", tl:"Hora diurna", elv:"hora_diurna", nxt:4},
            {type:"text", tl:"Hora nocturna", elv:"hora_nocturna", nxt:4},
            {type:"text", tl:"Extra diurna", elv:"extra_diurna", nxt:4},
            {type:"text", tl:"Extra nocturna", elv:"extra_nocturna", nxt:4},
            {type:"text", tl:"Extra especial", elv:"extra_especial", nxt:4},
            {type:"text", tl:"Viáticos", elv:"viaticos", nxt:4},
            {type:"text", tl:"Viáticos móvil", elv:"viaticos_movil", nxt:4},
    ]
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Empresas</h1>
    <ul>
        <li><a href="{{url('/company')}}">Inicio</a></li>
        <li><a href="{{url('/clientscompany/' . $idCompany)}}">Clientes</a></li>
        <li>Asignación de colaboradores a cliente</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12 mb-2">
        <h1>{{$companyName}}</h1>
        <h2>{{$clientName}}</h2>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">
            <div class="card-header">
                <h3 class="card-title">
                    Colaboradores disponibles
                </h3>
            </div>

            <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <em>Colaboradores disponibles: </em>
                            <select data-placeholder="Colaborador" class="chosen-select" tabindex="6" id="idColabSelect" onchange="changeSelectAgregar(this)" >
                              <option value=""></option>

                              @foreach ($disp as $k => $v)
                                  <optgroup label="{{$k}}">
                                      @foreach ($v as $k2 => $v2)
                                          <option value="{{$k2}}">{{$v2}}</option>
                                      @endforeach
                                  </optgroup>
                              @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                                @csrf
                                <input type="hidden" name="idClient" value="{{$idAssocCLient}}"></input>
                                <a href="javascript:"class="" onclick="Agregar_colaborador()">
                                    <button id="buttonAgregar" type="submit" class="btn btn-primary" disabled>Agregar</button>
                                </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                  Colaboradores en esta empresa
                </h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display table table-striped table-bordered">
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Sede</th>
                          <th>Puesto</th>
                          <th>Área</th>
                          <th>Colaborador</th>
                          <th>Tipo</th>
                          <th>Inicio</th>
                          <th>Termina</th>
                          <th>Acciones</th>
                        </tr>
                        </thead>

                        <tbody>
                            @foreach ($assi as $key => $value)
                              @php $perms=array(); @endphp
                              <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td >{{$value['sede']}}</td>
                                <td >{{$value['puesto']}}</td>
                                <td >{{$value['area']}}</td>
                                <td >{{$value['colaborador']}}</td>
                                <td >{{$value['tipo']}}</td>
                                <td >{{$value['inicio']}}</td>
                                <td >{{$value['fin'] != null ? $value['fin'] : 'Indefinido'}}</td>
                                <td class="td-actions  text-left" style="font-size: 20px;">

                                  <a  href="javascript:" onclick="viewFloat({{$key}}, vis, assi)" title="Ver">
                                      <i class="text-20" data-feather="eye"></i>
                                  </a>

                                  <a  href="{{route('admin.colaboradoresClientUnAssign', ['com_id' => $idCompany, 'cli_id' => $idClient, 'id' => $key])}}" title="Quitar">
                                      <i class="pe-7s-trash "></i>
                                  </a>

                                </td>
                              </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

      <div class="modal fade" id="descargar_documento" tabindex="-1" role="dialog" aria-labelledby="descargar_documento" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="descargar_documento">Descargar documentos</h5>
                      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">
                      <div class="col-lg-12 col-md-12 form-group mb-4">

                      <form id="form-doc-down" method="GET" action="{{route('admin.documentos_download')}}">

                          <input id="uid" name="uid" type="hidden" value=""></input>

                          <select name="did" class="form-control" style="cursor:pointer;" onclick="putdid(this)">
                              <option selected disabled value=""> - - - </option>

                          </select>

                          </div>

                          <div class="col-lg-12 col-md-12 form-group mb-4">
                              <button class="btn btn-primary">Descargar</button>
                          </div>
                      </form>

                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                      {{-- <button class="btn btn-primary ml-2" type="button">Save changes</button> --}}
                  </div>
              </div>
          </div>
      </div>

    <script type="text/javascript">
    var areas = @json($areas);
    var assi = @json($assi);

    function Agregar_colaborador(){
        let colabs = @json($disp),
            selected = $('#idColabSelect').val(),
            name = '';

        for(let i in colabs){
            for(let j in colabs[i]){
                if (j == selected){
                    name = colabs[i][j]
                    break
                }
            }
        }

        newfloatv2(kad, undefined,  '','literalUrl','{{route('admin.colaboradoresClientAssign',[$idCompany,$idClient,$idAssocCLient])}}');
        $('#idColabName').val(name).attr('disabled', true)
        $('#idColab').val(selected)

    }

    function selectTipo(elm){
        let val = $(elm).val()
        if (val == 'fijo'){
            console.log($('#fin'))
            $('#fin').attr('disabled', true)
        }
        if (val == 'temporal'){
            console.log($('#fin'))
            $('#fin').attr('disabled', false)
        }
    }

    function changeSelectAgregar(){
        $('#buttonAgregar').attr('disabled', false)
    }

    function changeSelectSede(elm){
        elm[0].disabled = true

        let cad = ``,
            val = $(elm).val();
        for(let i in areas[val]){
            cad += `<option value="${i}">${areas[val][i]}</option>`
        }
        console.log(areas)
        $('#area').html(cad)
    }

    </script>

@endsection
