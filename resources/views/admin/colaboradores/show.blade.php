@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Colaboradores</h1>
    <ul>
        <li><a href="{{url('/colaboradores')}}">Inicio</a></li>
        <li>Datos del colaborador</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<form id="colaborador_form" method="POST" action="/colaboradores{{isset($data['id']) ? '/'.$data['id'] : ''}}" enctype="multipart/form-data" onsubmit="event.preventDefault(); return changeLog();">

    @csrf
    @if($edit)
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-md-12">

            {{-- <div class="card mb-1 col-lg-3">
                <div class="card-header">
                    <h4>Fotografía</h4>
                </div>
                <div class="card-body">
                    <div class="row">

                        @if(isset($data->foto))
                            <div class="col-lg-6 offset-3 col-md-12 form-group mb-1">
                                <img src="{{ asset('storage/' . $data->foto) }}"/ width="100%" height="100%" style="margin: auto;">
                            </div>
                        @endif()

                        <div class="col-lg-12 col-md-12 form-group mb-1">
                            <label for="foto">Fotografía</label>
                            <input class="form-control" id="foto" name="foto" type="file" placeholder="Archivo de foto" />
                        </div>

                    </div>
                </div>
            </div> --}}


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Datos del colaborador</h4>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-3">
                            <div class="card mb-5">
                                <div class="card-header">
                                    <h4>Fotografía</h4>
                                </div>
                                <div class="card-body">
                                    @if(isset($data->foto))
                                        <div class="col-lg-6 offset-3 col-md-12 form-group mb-1">
                                            <img src="{{ asset('storage/' . $data->foto) }}"/ width="100%" height="100%" style="margin: auto;">
                                        </div>
                                    @endif()

                                    <div class="col-lg-12 col-md-12 form-group mb-1">
                                        <label for="foto">Fotografía</label>
                                        <input class="form-control" id="foto" name="foto" type="file" placeholder="Archivo de foto" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6 form-group mb-3">
                            <label for="nombres">Nombres</label>
                            <input class="form-control" id="nombres" name="nombres" type="text" placeholder="Nombres" value="{{ isset($data['nombres']) ? $data['nombres'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="apellidos">Apellidos</label>
                             <input class="form-control" id="apellidos" name="apellidos" type="text" placeholder="Apellidos" value="{{ isset($data['apellidos']) ? $data['apellidos'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="fecha">Fecha de nacimiento</label>
                             <input type="date" class="form-control" id="fecha" name="fecha" placeholder="dd-mm-aaaa" value="{{ isset($data['fecha']) ? $data['fecha'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="email">Email</label>
                             <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ isset($data['email']) ? $data['email'] : '' }}"/>
                             <!--  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="dpi">DPI</label>
                            <input type="number" class="form-control" id="dpi" name="dpi" placeholder="DPI" value="{{ isset($data['dpi']) ? $data['dpi'] : '' }}"/>
                       </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="nit">NIT</label>
                             <input class="form-control" id="nit" name="nit" placeholder="NIT" value="{{ isset($data['nit']) ? $data['nit'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="iggs">No. IGGS</label>
                             <input class="form-control" id="iggs" name="iggs" placeholder="No. IGGS" value="{{ isset($data['iggs']) ? $data['iggs'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="irtra">No. IRTRA</label>
                             <input class="form-control" id="irtra" name="irtra" placeholder="No. IRTRA" value="{{ isset($data['irtra']) ? $data['irtra'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="intecap">No. INTECAP</label>
                             <input class="form-control" id="intecap" name="intecap" placeholder="No. INTECAP" value="{{ isset($data['intecap']) ? $data['intecap'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="intecap">No. Licencia</label>
                             <input class="form-control" id="licencia" name="licencia" placeholder="No. Licencia" value="{{ isset($data['licencia']) ? $data['licencia'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="vence_licencia">Fecha de vencimiento de licencia</label>
                             <input type="date" class="form-control" id="vence_licencia" name="vence_licencia" placeholder="Fecha de vencimiento de licencia" value="{{ isset($data['vence_licencia']) ? $data['vence_licencia'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="fecha_salud">Fecha de tarjeta de salud</label>
                             <input type="date" class="form-control" id="fecha_salud" name="fecha_salud" placeholder="Fecha de tarjeta de salud" value="{{ isset($data['fecha_salud']) ? $data['fecha_salud'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="fecha_pulmones">Fecha de tarjeta de pulmones</label>
                             <input type="date" class="form-control" id="fecha_pulmones" name="fecha_pulmones" placeholder="Fecha de tarjeta de pulmones" value="{{ isset($data['fecha_pulmones']) ? $data['fecha_pulmones'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="genero">Género</label>
                             <select class="form-control" id="genero" name="genero">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = ['Hombre', 'Mujer']; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['genero']) ? $data['genero'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="nivel_academico">Nivel académico</label>
                             <select class="form-control" id="nivel_academico" name="nivel_academico">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = ['Primaria', 'Secundaria', 'Diversificado', 'Diplomado', 'Licenciatura', 'Maestría', 'Doctorado']; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['nivel_academico']) ? $data['nivel_academico'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="nacionalidad">Nacionalidad</label>
                             <select class="form-control" id="nacionalidad" name="nacionalidad">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = ['Guatemalteco', 'Extranjero']; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['nacionalidad']) ? $data['nacionalidad'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="estado_civil">Estado civil</label>
                             <select class="form-control" id="estado_civil" name="estado_civil">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = ['Soltero', 'Casado', 'Unido', 'Viudo', 'Divorciado']; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['estado_civil']) ? $data['estado_civil'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="tipo_sangre">Conyuge</label>
                            <input class="form-control" id="conyuge" name="conyuge" placeholder="Conyuge" value="{{ isset($data['conyuge']) ? $data['conyuge'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="tipo_sangre">Tipo de sangre</label>
                            <input class="form-control" id="tipo_sangre" name="tipo_sangre" placeholder="Tipo de sangre" value="{{ isset($data['tipo_sangre']) ? $data['tipo_sangre'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="hijos">Hijos</label>
                             <input type="number" class="form-control" id="hijos" name="hijos" placeholder="Hijos" value="{{ isset($data['hijos']) ? $data['hijos'] : '0' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="profesion">Profesión</label>
                             <input class="form-control" id="profesion" name="profesion" placeholder="Profesión" value="{{ isset($data['profesion']) ? $data['profesion'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="tel1">Teléfono 1</label>
                             <input type="tel" class="form-control" id="tel1" name="tel1" placeholder="Teléfono" value="{{ isset($data['tel1']) ? $data['tel1'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="tel2">Teléfono 2</label>
                             <input type="tel" class="form-control" id="tel2" name="tel2" placeholder="Teléfono" value="{{ isset($data['tel2']) ? $data['tel2'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="direccion">Dirección</label>
                             <input class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="{{ isset($data['direccion']) ? $data['direccion'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="departamento">Departamento</label>
                             <select class="form-control" id="departamento" name="departamento" onchange="changeMun(this);">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = $depmun @endphp
                                 @foreach($options as $opt => $val)
                                     <option {{$opt == (isset($data['departamento']) ? $data['departamento'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                             <label for="password">Cambiar contraseña</label>
                             <input  type="password" omit="T"class="form-control" id="password" name="password" placeholder="Contraseña nueva" />
                        </div>
                        <div class="col-md-6 form-group mb-3">
                             <label for="municipio">Municipio</label>
                             <select class="form-control" id="municipio" name="municipio">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options0 = $depmun @endphp
                                 @foreach($options0 as $opt0 => $options)
                                     @foreach($options as $opt)
                                         <option {{$opt == (isset($data['municipio']) ? $data['municipio'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                     @endforeach
                                @endforeach
                             </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mb-5">
                <div class="card-header">
                    <h4>Datos de contacto</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                             <label for="contacto1">Contacto</label>
                             <input class="form-control" id="contacto1" name="contacto1" placeholder="Nombre del contacto" value="{{ isset($data['contacto1']) ? $data['contacto1'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="tel_con1">Teléfono del contacto</label>
                             <input class="form-control" id="tel_con1" name="tel_con1" placeholder="Teléfono del contacto" value="{{ isset($data['tel_con1']) ? $data['tel_con1'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="parentesco_con1">Parentesco del contacto</label>
                             <input class="form-control" id="parentesco_con1" name="parentesco_con1" placeholder="Parentesco del contacto" value="{{ isset($data['parentesco_con1']) ? $data['parentesco_con1'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="contacto2">Contacto 2</label>
                             <input class="form-control" id="contacto2" name="contacto2" placeholder="Nombre del contacto 2" value="{{ isset($data['contacto2']) ? $data['contacto2'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="tel_con2">Teléfono del contacto 2</label>
                             <input class="form-control" id="tel_con2" name="tel_con2" placeholder="Teléfono del contacto 2" value="{{ isset($data['tel_con2']) ? $data['tel_con2'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="parentesco_con2">Parentesco del contacto 2</label>
                             <input class="form-control" id="parentesco_con2" name="parentesco_con2" placeholder="Parentesco del contacto 2" value="{{ isset($data['parentesco_con2']) ? $data['parentesco_con2'] : '' }}"/>
                        </div>

                    </div>
                </div>
            </div>


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Datos de contratación</h4>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 form-group mb-3">
                             <label for="contrato">Tipo de contrato</label>
                             <select class="form-control" id="contrato" name="contrato" >
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = $contratos; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['contrato']) ? $data['contrato'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="puesto">Puesto</label>
                             <select class="form-control" id="puesto" name="puesto">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = $puestos @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['puesto']) ? $data['puesto'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Pago diario</h4>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 form-group mb-3">
                             <label for="sueldo">Base diaria</label>
                             <input type="number" step="0.0001" class="form-control" id="sueldo" name="sueldo" placeholder="Base diaria" value="{{ isset($data['sueldo']) ? $data['sueldo'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="bono_extra">Bono extra</label>
                             <input type="number" step="0.0001" class="form-control" id="bono_extra" name="bono_extra" placeholder="Bono extra" value="{{ isset($data['bono_extra']) ? $data['bono_extra'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="antiguedad">Antigüedad</label>
                             <input type="number" step="0.0001" class="form-control" id="antiguedad" name="antiguedad" placeholder="Antigüedad" value="{{ isset($data['antiguedad']) ? $data['antiguedad'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="bono_ley">Bono de ley</label>
                             <input type="number" step="0.0001" class="form-control" id="bono_ley" name="bono_ley" placeholder="Bono extra" value="{{ isset($data['bono_ley']) ? $data['bono_ley'] : '' }}"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mb-5">
                <div class="card-header">
                    <h4>Horas extras</h4>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 form-group mb-3">
                             <label for="hora_extra_diurna">Hora extra diurna</label>
                             <input type="number" step="0.0001" class="form-control" id="hora_extra_diurna" name="hora_extra_diurna" placeholder="Hora extra diurna" value="{{ isset($data['hora_extra_diurna']) ? $data['hora_extra_diurna'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="hora_extra_nocturna">Hora extra nocturna</label>
                             <input type="number" step="0.0001" class="form-control" id="hora_extra_nocturna" name="hora_extra_nocturna" placeholder="Hora extra nocturna" value="{{ isset($data['hora_extra_nocturna']) ? $data['hora_extra_nocturna'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="hora_extra_especial">Hora extra especial</label>
                             <input type="number" step="0.0001" class="form-control" id="hora_extra_especial" name="hora_extra_especial" placeholder="Hora extra especial" value="{{ isset($data['hora_extra_especial']) ? $data['hora_extra_especial'] : '' }}"/>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                             <label for="hora_extra_mixta">Hora extra mixta</label>
                             <input type="number" step="0.0001" class="form-control" id="hora_extra_mixta" name="hora_extra_mixta" placeholder="Hora extra mixta" value="{{ isset($data['hora_extra_mixta']) ? $data['hora_extra_mixta'] : '' }}"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mb-5">
                <div class="card-header">
                    <h4>Viáticos móvil</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                             <label for="viaticos">Viáticos</label>
                             <input type="number" step="0.0001" class="form-control" id="viaticos" name="viaticos" placeholder="Viáticos" value="{{ isset($data['viaticos']) ? $data['viaticos'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="movil">Movil</label>
                             <input type="number" step="0.0001" class="form-control" id="movil" name="movil" placeholder="Movil" value="{{ isset($data['movil']) ? $data['movil'] : '' }}"/>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Horas normales</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                             <label for="hora_normal">Hora normal nocturna</label>
                             <input type="number" step="0.0001" class="form-control" id="hora_normal" name="hora_normal" placeholder="Hora normal nocturna" value="{{ isset($data['hora_normal']) ? $data['hora_normal'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="hora_normal_nocturna">Horal normal nocturna</label>
                             <input type="number" step="0.0001" class="form-control" id="hora_normal_nocturna" name="hora_normal_nocturna" placeholder="Horal normal nocturna" value="{{ isset($data['hora_normal_nocturna']) ? $data['hora_normal_nocturna'] : '' }}"/>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Pagos</h4>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 form-group mb-3">
                            <label for="tipo_pago">Tipo de pago</label>
                            <select class="form-control" id="tipo_pago" name="tipo_pago">
                                <option selected disabled value=""> - - - </option>
                                @php $options = ['Transferencia', 'Cheque', 'Efectivo']; @endphp
                                @foreach($options as $opt)
                                    <option {{$opt == (isset($data['tipo_pago']) ? $data['tipo_pago'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-2 form-group mb-3">
                             <label for="cuenta1">Cuenta</label>
                             <input type="number" class="form-control" id="cuenta1" name="cuenta1" placeholder="---" value="{{ isset($data['cuenta1']) ? $data['cuenta1'] : '' }}"/>
                        </div>

                        <div class="col-md-2 form-group mb-3">
                             <label for="cuenta2" style="color:white;">Cuenta</label>
                             <input type="number" class="form-control" id="cuenta2" name="cuenta2" placeholder="---" value="{{ isset($data['cuenta2']) ? $data['cuenta2'] : '' }}"/>
                        </div>

                        <div class="col-md-2 form-group mb-3">
                             <label for="cuenta3" style="color:white;">Cuenta</label>
                             <input type="number" class="form-control" id="cuenta3" name="cuenta3" placeholder="---" value="{{ isset($data['cuenta3']) ? $data['cuenta3'] : '' }}"/>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="banco">Banco</label>
                             <select class="form-control" id="banco" name="banco"/>
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = $bancos; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['banco']) ? $data['banco'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="pago_sueldo">Pago de sueldo</label>
                             <select class="form-control" id="pago_sueldo" name="pago_sueldo"/>
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = ['Semanal', 'Quincenal', 'Mensual']; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['pago_sueldo']) ? $data['pago_sueldo'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                             <label for="otros_pagos">Otros pagos</label>
                             <input type="number" step="0.0001" class="form-control" id="otros_pagos" name="otros_pagos" placeholder="Otros pagos" value="{{ isset($data['otros_pagos']) ? $data['otros_pagos'] : '' }}"/>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Productividad</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                             <label for="puntos1">A</label>
                             <input type="number" step="0.0001" class="form-control" id="puntos1" name="puntos1" placeholder="A" value="{{ isset($data['puntos1']) ? $data['puntos1'] : '' }}"/>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                             <label for="puntos2">B</label>
                             <input type="number" step="0.0001" class="form-control" id="puntos2" name="puntos2" placeholder="B" value="{{ isset($data['puntos2']) ? $data['puntos2'] : '' }}"/>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                             <label for="puntos3">C</label>
                             <input type="number" step="0.0001" class="form-control" id="puntos3" name="puntos3" placeholder="C" value="{{ isset($data['puntos3']) ? $data['puntos3'] : '' }}"/>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                             <label for="puntos4">D</label>
                             <input type="number" step="0.0001" class="form-control" id="puntos4" name="puntos4" placeholder="D" value="{{ isset($data['puntos4']) ? $data['puntos4'] : '' }}"/>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Otra información</h4>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-4 form-group mb-3">
                             <label for="estatus">Estatus</label>
                             <select class="form-control" id="estatus" name="estatus" placeholder="Estatus">
                                 <option selected disabled value=""> - - - </option>
                                 @php $options = ['Activo', 'Inactivo', 'De baja', 'Vacaciones', 'Suspendido']; @endphp
                                 @foreach($options as $opt)
                                     <option {{$opt == (isset($data['estatus']) ? $data['estatus'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="col-md-4 form-group mb-3">
                             <label for="fecha_ingreso">Fecha de ingreso</label>
                             <input type="date" class="form-control" id="fecha_ingreso" name="fecha_ingreso" placeholder="fecha de ingreso" value="{{ isset($data['fecha_ingreso']) ? $data['fecha_ingreso'] : '' }}"/>
                        </div>

                        <div class="col-md-4 form-group mb-3">
                             <label for="fecha_baja">Fecha de baja</label>
                             <input type="date" class="form-control" id="fecha_baja" name="fecha_baja" placeholder="fecha de ingreso" value="{{ isset($data['fecha_baja']) ? $data['fecha_baja'] : '' }}"/>
                        </div>

                        <div class="col-md-12 form-group mb-3">
                             <label for="observaciones">Observaciones</label>
                             <textarea class="form-control" style="height: 150px;" id="observaciones" name="observaciones" placeholder="Observaciones" >{{ isset($data['observaciones']) ? $data['observaciones'] : '' }}</textarea>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-3">
                    <div class="card mb-5">
                        <div class="card-header">
                            <h4>Firma</h4>
                        </div>
                        <div class="card-body">
                            @if(isset($data->firma))
                                <div class="col-lg-6 offset-3 col-md-12 form-group mb-1">
                                    <img src="{{ asset('storage/' . $data->firma) }}"/ width="100%" height="100%" style="margin: auto;">
                                </div>
                            @endif()

                            <div class="col-lg-12 col-md-12 form-group mb-1">
                                <label for="firma">Firma</label>
                                <input class="form-control" id="firma" name="firma" type="file" placeholder="Archivo de firma" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    {{ $edit ? 'Actualizar datos' : 'Guardar'}}
                </button>
            </div>

        </div>

    </div>

</form>

<script type="text/javascript">
  let depmun = @json($depmun);

  function changeMun(elm){
      let val = $(elm).val(),
          cad = ``;
      for(a in depmun[val]){
          cad += `<option>${depmun[val][a]}</option>`
      }
      $('#municipio').html(cad)

  }

  function changeLog(){
      Swal.fire({
          title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                    <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                </div>`,
          html:`
               <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                   <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                       <div class="modal-body">
                           <div class="row">
                               <label class="col-form-label">Comentario</label>
                               <textarea id="changeLogComment" class="form-control"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               `,
           preConfirm: function(){
               let val = $('#changeLogComment').val(),
                   cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`;
             $('#colaborador_form').append(cad).attr('onsubmit', '').submit();
             return true
         }
      })
  }
</script>

@endsection
