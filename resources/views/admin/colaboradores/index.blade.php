@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
var kad={
         0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Colaborador",nm:"idColabName",id:"idColabName",add:"readonly"},
         2:{type:"hddn",tl:"Colaborador",nm:"idColab",id:"idColab",elv:"0"},
         3:{type:"chckbx",tl:"Empresas",nm:@json($companies_ids),id:"companies",elv:"1",vl:@json($companies)}
        };
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Colaboradores</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
		  <h3 class="card-title">
        <a href="{{route("admin.editar_colaboradores")}}">
          <i class="ion-ios7-plus-outline "></i>
            &nbsp;&nbsp;Nuevo colaborador
        </a>
      </h3>

		</div>
        <div class="card-body">
          <div class="col-md-4 form-group mb-3">
               <label for="estatus">Estado</label>
               <select class="form-control" id="estatus" onchange="filtro(this);" placeholder="Estatus">
                   <option selected disabled value=""> - - - </option>
                   @php $options = ['Activo', 'Inactivo', 'De baja', 'Vacaciones', 'Suspendido']; @endphp
                   @foreach($options as $opt)
                       <option {{$opt == (isset($data['estatus']) ? $data['estatus'] : '') ? 'selected' : ''}}>{{$opt}}</option>
                   @endforeach
               </select>
          </div>
          <div class="table-responsive">
            <table id="colaboradores_table" class="display table table-striped table-bordered">
              <thead>
                <tr>
                  {{-- <th>#</th> --}}
                  <th>Codigo</th>
                  <th>Fotografía</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Teléfono</th>
                  <th>Direccion</th>
                  <th>Puesto</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($data as $value)
                  @php $perms=array(); @endphp
                  <tr>
                    {{-- <td>{{ $loop->index + 1 }}</td> --}}
                    <td >{{$value->id}}</td>
                    <td class="d-flex justify-content-center"><img src="{{ asset('storage/' . $value->foto) }}" height="50px"></td>
                    <td >{{$value->nombres}}</td>
                    <td >{{$value->apellidos}}</td>
                    <td >{{$value->tel1}}</td>
                    <td >{{$value->direccion}}</td>
                    <td >{{$value->puesto}}</td>
                    <td class="td-actions  text-left" style="font-size: 20px;">


                      <a href="javascript:" title="Descargar documentos">
                          <span data-toggle="modal" data-target="#descargar_documento" onclick="putid('{{$value->id}}')">
                              <i class="text-20" data-feather="download"></i>
                          </span>
                      </a>

                      <a  href="{{ route('admin.editar_colaboradores', ['id' => $value->id]) }}" title="Editar colaborador">
                          <i class="pe-7s-note "></i>
                      </a>

                      <a href="{{url('changeLog', ['colaboradores', $value->id])}}" title="Registro de cambios">
                          <i class="text-20"data-feather="clock"></i>
                      </a>

                      <a  href="{{url('archivos', ['id' => $value->id_file_manager])}}" title="Administar archivos">
                          <i class="text-20" data-feather="file"></i>
                      </a>

                      <a  href="javascript:" onclick="newfloatv2(kad,undefined,'{{route("admin.asignar_colaboradores")}}'); clickMe('{{$value->id}}', '{{$value->nombres}}')"
                           title="Asignar a empresa">
                          <i class="text-20" data-feather="monitor"></i>
                      </a>

                      <a  href="javascript:" title="Eliminar colaborador"
                          onclick="deleteD('{{$value->id}}','{{ csrf_token() }}');">
                          <i class="pe-7s-trash "></i>
                      </a>

                    </td>
                  </tr>
                  @php
                  $criti[$value->id]=array($value->nombres);
                  @endphp
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="descargar_documento" tabindex="-1" role="dialog" aria-labelledby="descargar_documento" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="descargar_documento">Descargar documentos</h5>
                      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">
                      <div class="col-lg-12 col-md-12 form-group mb-4">

                      <form id="form-doc-down" method="GET" action="{{route('admin.documentos_download')}}" target="_blank">

                          <input id="uid" name="uid" type="hidden" value=""></input>

                          <select name="did" class="form-control" style="cursor:pointer;" onclick="putdid(this)">
                              <option selected disabled value=""> - - - </option>
                              @foreach($docs as $t)
                                  <option value="{{$t['id']}}">{{$t['name']}}</option>
                              @endforeach
                          </select>

                          </div>

                          <div class="col-lg-12 col-md-12 form-group mb-4">
                              <button class="btn btn-primary">Descargar</button>
                          </div>
                      </form>

                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                      {{-- <button class="btn btn-primary ml-2" type="button">Save changes</button> --}}
                  </div>
              </div>
          </div>
      </div>

    <script type="text/javascript">

      function putid(id){
          $('#uid').val(id)
      }

      function clickMe(id, nombres){
          $('#idColabName').val(nombres)
          $('#idColab').val(id)
          $('#verifyModalContent_title').html("Asignar colaborador a empresa")
      }

    </script>

@endsection

@section('bottom-js')
    <script>
        $('#colaboradores_table').DataTable();
        function filtro(esto){
          window.location = '{{route('admin.colaboradores.index')}}?estado='+esto.value;
        }
    </script>
@endsection
