@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Empresas</h1>
    <ul>
        <li><a href="{{url('/company')}}">Inicio</a></li>
        <li>Asignación de colaboradores a empresa</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12 mb-2">
        <h1>{{$nameCompany}}</h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">
            <div class="card-header">
                <h3 class="card-title">
                    Colaboradores disponibles
                </h3>
            </div>

            <div class="card-body">

                <form action="{{ route('admin.colaboradoresCompanyAssign') }}" method="post">

                    <div class="row">
                        <div class="col-md-12">
                            <em>Colaboradores disponibles: </em>
                            <select data-placeholder="Colaborador" class="chosen-select" tabindex="6" onchange="selectMe(this)" name="idColab">
                              <option value=""></option>

                              @foreach ($disp as $k => $v)
                                  <optgroup label="{{$k}}">
                                      @foreach ($v as $k2 => $v2)
                                          <option value="{{$k2}}">{{$v2}}</option>
                                      @endforeach
                                  </optgroup>
                              @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                                @csrf
                                <input type="hidden" name="idCompany" value="{{$idCompany}}"></input>
                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </form>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
		  <h3 class="card-title">
              Colaboradores en esta empresa
          </h3>
		</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="asignar_colaboradores_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Codigo empleado</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  {{-- <th>Teléfono</th>
                  <th>Direccion</th> --}}
                  <th>Puesto</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($assi as $key => $value)
                  @php $perms=array(); @endphp
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td >{{$value['id']}}</td>
                    <td >{{$value['nombres']}}</td>
                    <td >{{$value['apellidos']}}</td>
                    <td >{{$value['puesto']}}</td>

                    <td class="td-actions  text-left" style="font-size: 20px;">

                      {{-- <a >
                          <button class="btn" type="button" data-toggle="modal" data-target="#descargar_documento" onclick="putid('{{$value->id}}')">
                              <i class="i-Arrow-Down"></i>
                          </button>
                      </a>
                      --}}

                      <a  href="{{ route('admin.colabCompanyWork', ['id' => $value['id'],'idComp'=>$idCompany]) }}"  title="">
                          <i data-feather="archive"></i>
                      </a>
                      <a  href="{{route('admin.colaboradoresCompanyUnAssign', ['cid' => $idCompany, 'id' => $key])}}">
                          <i class="pe-7s-trash "></i>
                      </a>

                    </td>
                  </tr>
                @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="descargar_documento" tabindex="-1" role="dialog" aria-labelledby="descargar_documento" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="descargar_documento">Descargar documentos</h5>
                      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">
                      <div class="col-lg-12 col-md-12 form-group mb-4">

                      <form id="form-doc-down" method="GET" action="{{route('admin.documentos_download')}}">

                          <input id="uid" name="uid" type="hidden" value=""></input>

                          <select name="did" class="form-control" style="cursor:pointer;" onclick="putdid(this)">
                              <option selected disabled value=""> - - - </option>

                          </select>

                          </div>

                          <div class="col-lg-12 col-md-12 form-group mb-4">
                              <button class="btn btn-primary">Descargar</button>
                          </div>
                      </form>

                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                      {{-- <button class="btn btn-primary ml-2" type="button">Save changes</button> --}}
                  </div>
              </div>
          </div>
      </div>

    <script type="text/javascript">

      function putid(id){
          $('#uid').val(id)
      }

    </script>

@endsection

@section('bottom-js')
    <script>
        $('#asignar_colaboradores_table').DataTable();
    </script>
@endsection
