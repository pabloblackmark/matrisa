@inject('helper', 'App\Http\Helpers\helpers')
@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Mensajes User</h1>
    <!--
                        <ul>
                            <li><a href="{{url('/mensajes')}}">Buzón de Entrada</a></li>
                            <li>Enviados</li>
                        </ul>
    -->
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
			     <h3 class="card-title">
              <a href="{{ url('/mensajes/create') }}" class="" >
                <i class="ion-ios7-plus-outline "></i>
                &nbsp;&nbsp;
                Nuevo Mensaje
              </a>
           </h3>
           <div class="card-options">
             <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
             <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
             <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
           </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table i class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  
                  <th>Para</th>
                  <th>Asunto</th>
                  <th>Fecha</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>

                @forelse($mensajes as $mensaje)
                  <tr>
                    
                    <td >para: {{ $mensaje['para'] }}</td>
                    <td >{{ $mensaje['titulo'] }}</td>
                    <td>{{ $mensaje['fecha'] }}</td>

                    <td class="td-actions  text-left">
                      <a  href="javascript:"
                          onclick="">
                          <i class="text-20"data-feather="edit-3"></i>
                      </a>
                      <a  href="javascript:"
                          onclick="">
                          <i class="text-12" data-feather="trash"></i>
                      </a>
                    </td>
                  </tr>
                @empty
                  <div class="alert alert-success">
                    No hay mensajes en el buzón de entrada.
                  </div>

                @endforelse
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    @component('components.messagesForm')
    @endcomponent
@endsection
