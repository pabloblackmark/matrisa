@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Mensajes User</h1>

                        <ul>
                            <li><a href="{{url('/mensajes_user')}}">Buzón de Mensajes</a></li>
                        </ul>

    <!--
                        <ul>
                            <li><a href="{{url('/mensajes')}}">Buzón de Entrada</a></li>
                            <li><a href="{{url('/mensajes_enviados')}}">Enviados</a></li>
                        </ul>
    -->
</div>
<div class="separator-breadcrumb border-top"></div>

<form method="POST" action="{{ url('mensajes_user/') }}/{{ $mensaje['id'] }}" >

    @csrf
    @method('PUT')

    <div class="row">
        <div class="col-md-12">


            <div class="card mb-5">
                <div class="card-header">
                    <h4>Mensaje</h4>
                </div>
                <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>



<div class="row">
        <div class="col-md-6 form-group mb-3" style="font-size: 16px;">
            De: {{ $mensaje["de"] }}<br>
            Para: {{ $mensaje["para"] }}<br>
            <small>Fecha: {{ $mensaje["fecha"] }}</small><br><br>
            Menesaje: <br>
            {{ $mensaje["mensaje"] }}
            <br>
            <br>
        </div>
</div>


@if ($mensaje["respuestas"]!='')
    @foreach($mensaje["respuestas"] as $respuesta)
    <div class="row">
        @if ($respuesta["tipo"]==0)
            <div class="col-lg-6 col-xl-6 mt-3">

            </div>
        @endif
            <div class="col-lg-6 col-xl-6 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title mb-2">Re: {{ $respuesta["de"] }}</div>
                                    <small>{{ $respuesta["fecha"] }}</small><br><br>
                                    {{ $respuesta["respuesta"] }}
                                </div>
                            </div>
            </div>
    </div>
    @endforeach

<br>
<br>
@endif


                    <div class="row">
                        <div class="col-md-12 form-group mb-3">
                             <label for="respuesta">Mensaje de Respuesta:</label>
                             <textarea class="form-control" style="height: 150px;" id="respuesta" name="respuesta" placeholder="Mensaje de Respuesta..." ></textarea>
                        </div>
                    </div>   
                    
                </div>
            </div>

            <div class="col-md-12">
                <button class="btn btn-primary" style="width:50%; margin: 0 25%;">
                    Responder Mensaje
                </button>
            </div>

        </div>

    </div>

</form>


@endsection
