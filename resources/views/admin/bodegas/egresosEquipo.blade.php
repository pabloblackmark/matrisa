@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas de equipo</h1>
    <ul>
        <li><a href="{{url('/bodegasEquipo')}}">Inicio</a></li>
        <li>Egresos</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: {{$bodega->nombre}}
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="egresos_equipos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo</th>
                                <th>Equipo</th>
                                <th>Medida</th>
                                <th>Cantidad</th>
                                <th>Costo por unidad</th>
                                <th>Costo total</th>
                                <th>Precio al cliente por unidad</th>
                                <th>Precio al cliente total</th>
                                <th>Categoria</th>
                                <th>Subcategoria</th>
                                <th>Porcentaje de depreciacion</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->equipo->codigo }}</td>
                                    <td>{{ $value->equipo->nombre }}</td>
                                    <td>{{ $value->medida->nombre }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>Q. {{ number_format($value->costo_unidad, 2) }}</td>
                                    <td>Q. {{ number_format($value->costo_total, 2) }}</td>
                                    <td>Q. {{ number_format($value->equipo->precio_cliente_unidad, 2) }}</td>
                                    <td>Q. {{ number_format($value->equipo->precio_cliente_unidad, 2) * $value->cantidad }}</td>
                                    <td>{{ $value->categoria->category }}</td>
                                    <td>{{ $value->sub_categoria->subCategory }}</td>
                                    <td>{{ $value->equipo->porcentaje_depreciacion }} %</td>
                                    <td>{{ $value->created_at->format('d/m/Y H:m') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('bottom-js')
    <script>
        $('#egresos_equipos_table').DataTable();
    </script>
@endsection
