@extends('admin.layouts.master')
@section('main-content')

<script type="text/javascript">
    var kad={
                0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
                1:{type:"txt",tl:"Nombre del banco",nm:"nombre",elv:"0"},
                2:{type:"txt",tl:"Razón social del banco",nm:"razon",elv:"1"},
            },
        valdis={clase:"red",text:1};
</script>

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li>Inicio</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="bodegas_general" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre empresa</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->nombre }}</td>
                                    <td>
                                        <a href="{{url('/ingresos', ['id' => $value->id])}}" title="Ingresos">
                                            <i class="text-20"data-feather="trending-up"></i>
                                        </a>
                                        <a href="{{url('/egresos', ['id' => $value->id])}}" title="Egresos">
                                            <i class="text-20"data-feather="trending-down"></i>
                                        </a>
                                        <a href="{{url('/stockEmpresas', ['company_id' => $value->empresa->id, 'bodega_id' => $value->id])}}" title="Stock">
                                            <i class="text-20" data-feather="layers"></i>
                                        </a>
                                        <a href="{{url('/bodegasCliente', ['id' => $value->empresa->id])}}" title="Ver bodegas de clientes">
                                            <i class="text-20 i-Clothing-Store"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php $criti[$value->id] = [$value->nombre, $value->razon]; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var criteria = @json($criti);
</script>

@endsection


@section('bottom-js')
    <script>
        $('#bodegas_general').DataTable();
    </script>
@endsection
