@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegas')}}">Inicio</a></li>
        <li>Egresos</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodega: {{$bodega->nombre}}
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="egresos_productos_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo de producto</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Medida</th>
                                <th>Costo unitario</th>
                                <th>Costo total</th>
                                <th>Fecha</th>
                                <th>Se carga a</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value->producto->codigo }}</td>
                                    <td>{{ $value->producto->nombre }}</td>
                                    <td>{{ $value->cantidad }}</td>
                                    <td>{{ $value->medida_final_element->nombre }}</td>
                                    <td>Q.{{ number_format($value->costo_unitario,2) }}</td>
                                    <td>Q.{{ number_format($value->valor_total,2) }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>
                                        {{ $value->tipo_egreso }}:
                                        <br>
                                        {{ $value->bodega_element['cliente']->clientName }}
                                        <br>
                                        {{ $value->bodega_element['bodega']->nombre }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('bottom-js')
    <script>
        $('#egresos_productos_table').DataTable();
    </script>
@endsection
