@extends('admin.layouts.master')
@section('main-content')


<div class="breadcrumb">
    <h1 class="mr-2">Bodegas</h1>
    <ul>
        <li><a href="{{url('/bodegas')}}">Inicio</a></li>
        <li>Bodegas clientes</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Bodegas de los clientes de <b>{{$empresa->company}}</b>
                </h3>
    		</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="bodegas_clientes_table" class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre del cliente</th>
                                <th>Nombre de la sede</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $criti = array(); @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $value['cliente']->clientName }}</td>
                                    <td>{{ $value['bodega']->nombre }}</td>
                                    <td>
                                        <a href="{{url('/ingresosClientes', ['company_id' => $company_id, 'bodega_id' => $value['bodega']->id])}}" title="Ingresos">
                                            <i class="text-20"data-feather="trending-up"></i>
                                        </a>
                                        <a href="{{url('/egresosClientes', ['company_id' => $company_id,'id' => $value['bodega']->id])}}" title="Egresos">
                                            <i class="text-20"data-feather="trending-down"></i>
                                        </a>
                                        {{-- <a href="{{url('/stockClientes', ['company_id' => $company_id,'id' => $value['bodega']->id])}}" title="Stock"> --}}
                                        <a href="{{url('/stockClientes', ['company_id' => $company_id, 'id' => $value['bodega']->id])}}" title="Stock">
                                            <i class="text-20"data-feather="layers"></i>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>

@endsection


@section('bottom-js')
    <script>
        $('#bodegas_clientes_table').DataTable();
    </script>
@endsection
