@extends('admin.layouts.master')
@section('main-content')

<div class="breadcrumb">
    <h1 class="mr-2">Liquidaciones</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-5">
            <div class="card-body">


              <form id="form_filtro" action="" method="get">
                @csrf

              <div class="row">
                  <div class="col-md-3">
                    Empresa:<br>
                      <select class="form-control" id="id_empresa" name="id_empresa" required>
                        <option value="" selected disabled>-- seleccionar empresa --</option>
                        @foreach ($companies as $k => $v)
                          <option value="{{$v[0]}}" {{(isset($request->id_empresa)&&$v[0]==$request->id_empresa?'selected':'')}}>{{$v[1]}}</option>
                        @endforeach
                      </select>
                  </div>


                  <div class="col-md-3">
                    Cliente:<br>
                      <select class="form-control" id="id_cliente" name="id_cliente">
                        {!! $clientes !!}
                      </select>
                  </div>

                  <div class="col-md-3">
                    Sucursal:<br>
                      <select class="form-control" id="id_sucursal" name="id_sucursal">
                          {!! $sucursales !!}
                      </select>
                  </div>

                  <div class="col-md-3">
                    Area:<br>
                      <select class="form-control" id="id_area" name="id_area">
                          {!! $areas !!}
                      </select>
                  </div>



              </div>
              <br>
              <div class="row">


    <div class="col-lg-2">
      Año
      <input type="text" name="ano" id="ano" class="w-100 form-control" value="{{ date('Y') }}">
    </div>

    <div class="col-lg-3">
        <button type="submit" form="form_filtro" value="filtrar" class="btn btn-primary text-white btn-rounded" style="margin-top: 20px;">Filtrar</button>
    </div>

              </div>

              </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
    		  <h3 class="card-title  mb-0">
            Colaboradores en esta empresa
          </h3>
    		</div>
        <div class="card-body">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>


          <form id="form_pago" action="{{ url('aprobar_liquidacion') }}" method="post" onsubmit="return validate_data()">
          @csrf

          <div class="table-responsive">

            <table id="asignar_colaboradores_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>&nbsp;</th>
                  <th>Código</th>
                  <th>Colaborador</th>                  
                  <th>Liquidación</th>
                  <th>Pendientes del mes</th>
                  <th>Descuentos</th>
                  <th>Total</th>
                  <th>Detalle</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($colabs))
                  @foreach ($colabs as $value)
                    @php

                    $colab = $value->getColabsInfo()->first();
                    $sueldo = $colab->sueldo*30;
                    $fecha_ingreso = date_create($colab->fecha_ingreso);



                    // indemizacion

                    $fecha_inicio = $fecha_ingreso;
                    

                    if ($colab->fecha_baja!='') {
                        $fecha_fin = date_create($colab->fecha_baja);

                        $fecha_fin_string = $colab->fecha_baja;
                    } else {
                        $fecha_fin = date_create(date('Y-m-d'));

                        $fecha_fin_string = date('Y-m-d');
                    }


                    $diff = date_diff($fecha_inicio,$fecha_fin);


                    $dias = $diff->format('%a')+1;
                    $meses = ($diff->format('%y')*12) + $diff->format('%m');
                    $anos = $diff->format('%y');

                    $indemnizacion = ($sueldo/365)*$dias;
                    



                    // vacaciones

                    $dias_vacas = ($dias*15)/365;
                    $dias_vacas = round($dias_vacas, 0, PHP_ROUND_HALF_DOWN);


                    // $vacas = ($sueldo/365)*($dias_vacas/30)*$dias;
                    $vacas = ($sueldo/365)*(15/30)*$dias;
                    


                    $ano_pasado = date("Y", strtotime ('-1 year'));
                    $ano_presente = date("Y");


                        // aguinaldo

                        $fecha_aguinaldo = $ano_pasado."-11-30";
                        $ano_aguinaldo = date_create($fecha_aguinaldo);

                        // validacion fecha completa
                        // $fecha_fin = $ano_presente."-11-30";
                        // $fin = date_create($fecha_fin);

                        $fin_aguinaldo = $fecha_fin;

                        // validacion de fecha de ingreso
                        if ($fecha_ingreso<$ano_aguinaldo) {
                            $cuando_entro_agui = "entro antes";

                            $inicio_aguinaldo = $ano_aguinaldo;
                        } else {
                            $cuando_entro_agui = "entro despues";

                            $inicio_aguinaldo = date_create($colab->fecha_ingreso);
                        }

                        $diff_aguinaldo = date_diff($inicio_aguinaldo, $fin_aguinaldo);

                        $dias_aguinaldo = $diff_aguinaldo->format('%a')+1;

                        $aguinaldo = ($sueldo/365)*$dias_aguinaldo;
                        // $aguinaldo = number_format($aguinaldo, 2, '.', ',');




                        // bono 14
                        $ano_bono_14 = $ano_pasado."-06-30";
                        $ano_bono_14 = date_create($ano_bono_14);

                        // validacion fecha completa
                        // $fin = date_create($ano_presente."-06-30");

                        $fin_bono = $fecha_fin;

                        // validacion de fecha de ingreso
                        if ($fecha_ingreso<$ano_bono_14) {
                            $cuando_entro_bono = "entro antes";

                            $inicio_bono = $ano_bono_14;

                        } else {
                            $cuando_entro_bono = "entro despues";

                            $inicio_bono = date_create($colab->fecha_ingreso);

                        }

                        $diff_bono_14 = date_diff($inicio_bono, $fin_bono);

                        $dias_bono_14 = $diff_bono_14->format('%a')+1;

                        $bono_14 = ($sueldo/365)*$dias_bono_14;
                        // $bono_14 = number_format($bono_14, 2, '.', ',');



                         
                    $request->inicio = date('Y-m-01');
                    $request->fin = $fecha_fin_string;
                    $request->id_colab = $value->idColab;


                    // planilla
                    $planilla = App\Http\Controllers\Admin\accounting\payrollController::get_planilla($request);


                    $salario = $planilla['salario'];
                    $bono_ley = $planilla['bono_ley'];
                    $bono_extra = $planilla['bono_extra'];
                    $bono_antiguedad = $planilla['bono_antiguedad'];
                    $desc_dia = $planilla['desc_dia'];
                    $desc_septimo = $planilla['desc_septimo'];
                    $desc_igss = $planilla['desc_igss'];
                    $total_bonos = $planilla['total_bonos'];


                    // horas extras
                    $horas_extras = App\Http\Controllers\Admin\accounting\horasController::get_horas($request);

                    $total_horas = $horas_extras['total_horas'];


                    // viaticos
                    $viaticos = App\Http\Controllers\Admin\accounting\viaticosController::get_viaticos($request);

                    $total_viaticos = $viaticos['total_viaticos'];



                    // descuentos
                    $descuentos = App\Http\Controllers\Admin\accounting\descuentosController::get_saldos($request);
                    
                    $desc_modulo = 0;                    

                    foreach ($descuentos as $desc) {
                        $desc_modulo += $desc['pago'];                        
                    }




                    // GRAN TOTAL

                    $total_descuentos = $desc_dia + $desc_septimo + $desc_igss + $desc_modulo;


                    $total = ($indemnizacion + $bono_14 + $aguinaldo + $vacas + $salario + $total_bonos + $total_horas + $total_viaticos) - $total_descuentos;




                    // aplicar filtros
                    $log_liquidacion = $model::whereYear("fecha_pago",$request->ano)
                                                ->where("id_colab",$value->idColab)
                                                ->where('id_empresa',$request->id_empresa)
                                                ->first();
                                                          


                    $array_liquidacion = array(
                                          'id_empresa'=>$request->id_empresa,
                                          'id_colab'=>$value->idColab,
                                          'indemnizacion'=>$indemnizacion,
                                          'aguinaldo'=>$aguinaldo,
                                          'bono_14'=>$bono_14,
                                          'vacaciones'=>$vacas,
                                          'salario'=>$salario,
                                          'total_bonos'=>$total_bonos,
                                          'total_horas'=>$total_horas,
                                          'total_viaticos'=>$total_viaticos,
                                          'total_descuentos'=>$total_descuentos,
                                          'total'=> $total,
                                          'detalle'=>''
                    );


                    $json_liquidacion = json_encode($array_liquidacion);


                    $indemnizacion = number_format($indemnizacion, 2, '.', ',');
                    $vacas = number_format($vacas, 2, '.', ',');

                    $aguinaldo = number_format($aguinaldo, 2, '.', ',');
                    $bono_14 = number_format($bono_14, 2, '.', ',');



                    $salario = number_format($salario, 2, '.', ',');
                    $bono_ley = number_format($bono_ley, 2, '.', ',');
                    $bono_extra = number_format($bono_extra, 2, '.', ',');
                    $bono_antiguedad = number_format($bono_antiguedad, 2, '.', ',');
                    $desc_dia = number_format($desc_dia, 2, '.', ',');
                    $desc_septimo = number_format($desc_septimo, 2, '.', ',');
                    $desc_igss = number_format($desc_igss, 2, '.', ',');
                    $total_bonos = number_format($total_bonos, 2, '.', ',');


                    $total_horas = number_format($total_horas, 2, '.', ',');
                    $total_viaticos = number_format($total_viaticos, 2, '.', ',');

                    $total = number_format($total, 2, '.', ',');


                    @endphp
                    @if(!empty($colab))
                      <tr>
                        <td >
                          @if(empty($log_liquidacion->estado))

                          <input type="checkbox" id="{{ $value->idColab }}" name="pago[]" value="{{ $json_liquidacion }}">

                          @endif

                        </td>
                        <td >{{ $value->idColab }}</td>
                        <td >{{ $colab->nombres }} {{ $colab->apellidos }}
                          <br><small><i>{{ $colab->puesto }}</i></small>
                          <br><small><i>( {{ $colab->fecha_ingreso }} )</i></small>
                        </td>
                        <td>

                            <small>Indemnización</small>: Q. {{ $indemnizacion }}<br>
                            <hr class="mb-0 mt-1">
                            <small>Aguinaldo</small>: Q. {{ $aguinaldo }}<br>
                            <hr class="mb-0 mt-1">
                            <small>Bono 14</small>: Q. {{ $bono_14 }}
                            <hr class="mb-0 mt-1">
                            <small>Vacaciones</small>: Q. {{ $vacas }}
                            <hr class="mb-0 mt-1">

                        </td>
                        <td>

                            <small>Salario</small>: Q. {{ $salario }}<br>
                            <hr class="mb-0 mt-1">
                            <small>Bonos</small>: Q. {{ $total_bonos }}<br>
                            <hr class="mb-0 mt-1">
                            <small>Horas Extras</small>: Q. {{ $total_horas }}
                             <hr class="mb-0 mt-1">
                            <small>Viáticos</small>: Q. {{ $total_viaticos }}

                        </td>

                        <td>
                            <small>Días</small>: Q. {{ $desc_dia }}<br>
                            <hr class="mb-0 mt-1">
                            <small>Septimo</small>: Q. {{ $desc_septimo }}<br>
                            <hr class="mb-0 mt-1">
                            <small>IGSS</small>: Q. {{ $desc_igss }}<br>

                        @foreach ($descuentos as $descuento)
                            <hr class="mb-0 mt-1">
                            <small style="text-transform: capitalize;">{{ $descuento['tipo'] }}</small>: 
                            Q.{{ $descuento['pago'] }}<br>
                        @endforeach
                        </td>

@php 

    if ($total>0) {
        $color = 'green';
    } else {
        $color = 'red';
    }

@endphp
                        <td style="color: {{ $color }}; font-weight: bold;">
                          Q. 
                            {{ $total }}

                          {{-- 
                          @if(!empty($log_liquidacion->total))
                              {{ number_format($log_liquidacion->total, 2, '.', ',') }}
                          @else
                              {{ $total }}
                          @endif

                          --}}
                      </td>
                        <td class="td-actions  text-left" style="font-size: 20px;">
                          <a  href="{{ url('liquidacion_colab') }}?id_empresa={{ $request->id_empresa }}&id_colab={{$value->idColab}}&id_cliente={{ $request->id_cliente }}&id_sucursal={{ $request->id_sucursal }}&id_area={{ $request->id_area }}&tipo={{ $request->tipo }}&ano={{ $request->ano }}" title="Ver detalle" target="_blank">
                              <i data-feather="eye"></i>
                          </a>
                        </td>
                        <td>
                          @if(!empty($log_liquidacion->estado))
                            <span class="badge badge-pill {{($log_liquidacion->estado=='Denegado'?'badge-danger':'badge-success')}} m-2">
                            {{ $log_liquidacion->estado }}</span>
                          </td>
                          @else
                            No procesado
                          @endif
                      </tr>
                    @endif
                  @endforeach
                @endif
					    </tbody>
           </table>


          </div>


@if ($request->id_empresa!='')

          <input type="hidden" id="id_empresa" name="id_empresa" value="{{ $request->id_empresa }}">
          <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $request->id_cliente }}">
          <input type="hidden" id="id_sucursal" name="id_sucursal" value="{{ $request->id_sucursal }}">
          <input type="hidden" id="id_area" name="id_area" value="{{ $request->id_area }}">
          <input type="hidden" id="inicio" name="inicio" value="{{ $request->inicio }}">
          <input type="hidden" id="fin" name="fin" value="{{ $request->fin }}">

          <div class="text-left">
              <input type="checkbox" id="pago_todos" name="pago_todos" value="todos"> SELECCIONAR TODOS
          </div>
          <div class="text-center">
            
              <div style="width: 200px; margin-left: auto; margin-right: auto;">
                  Fecha de Pago
                  <input type="date" name="fecha_pago" id="fecha_pago" class="w-100 form-control" value="" required>
              </div>
              <br>

              <button type="submit" form="form_pago" value="pago" class="btn btn-primary text-white btn-rounded">Procesar PAGO</button>
          </div>

@endif
        </form>

        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">


$(function() {


    $('#id_empresa').on('change', function() {


        $('#id_cliente').html('');
        $('#id_sucursal').html('');

        id_empresa = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_clientes')}}',
           data:{
             id_empresa: id_empresa,
           },
           success:function(data) {

                $('#id_cliente').html(data);     
           }
        });

    });


    $('#id_cliente').on('change', function() {


        $('#id_sucursal').html('');

        id_cliente = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_sucursales')}}',
           data:{
             id_cliente: id_cliente,
           },
           success:function(data) {

                $('#id_sucursal').html(data);
           }
        });

    });



    $('#id_sucursal').on('change', function() {


        $('#id_area').html('');

        id_sucursal = $(this).find(":selected").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'post',
           url:'{{url('get_areas')}}',
           data:{
             id_sucursal: id_sucursal,
           },
           success:function(data) {

                $('#id_area').html(data);
           }
        });

    });


    $("#pago_todos").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });


});


    function validate_data() {

        var selected = [];

         $('#asignar_colaboradores_table input:checked').each(function() {
             selected.push($(this).attr('value'));
         });


        if(selected.length == 0){
            alert('Debes Selecciona al menos un Colaborador para continuar.');
            return false;
        } else {
            return true;
        }

    }


</script>
<style media="screen">
  small{
    font-weight:bold;
  }
</style>
@endsection

@section('bottom-js')
<script>
        $('#asignar_colaboradores_table').DataTable();
</script>


@endsection
