@extends('admin.layouts.master')

@section('main-content')

@php  $creat=array();
  $colors = ["ausencia"=>'#f14848',"sin horario"=>'#a99944',"asistencia"=>'#38b6f5',
            "Permiso"=>'#ecffef',"Vacaciones"=>'#f3fbff',"Iggs"=>'#f3fbff'];
@endphp
<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{ url('liquidacion') }}?id_empresa={{ $request->id_empresa }}&id_cliente={{ $request->id_cliente }}&id_sucursal={{ $request->id_sucursal }}&id_area={{ $request->id_area }}&inicio={{ $request->inicio }}&fin={{ $request->fin }}">Listado liquidaciones</a> </h1>
    <ul>
        <li>Detalle de liquidación</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Datos del empleado</div>
      </div>
      <div class="card-body">
          <div class="row">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

            @php

                    $sueldo = $colab->sueldo*30;
                    $fecha_ingreso = date_create($colab->fecha_ingreso);



                    // indemizacion

                    $fecha_inicio = $fecha_ingreso;
                    

                    if ($colab->fecha_baja!='') {
                        $fecha_fin = date_create($colab->fecha_baja);

                        $fecha_fin_string = $colab->fecha_baja;
                    } else {
                        $fecha_fin = date_create(date('Y-m-d'));

                        $fecha_fin_string = date('Y-m-d');
                    }


                    $diff = date_diff($fecha_inicio,$fecha_fin);


                    $dias = $diff->format('%a')+1;
                    $meses = ($diff->format('%y')*12) + $diff->format('%m');
                    $anos = $diff->format('%y');

                    $indemnizacion = ($sueldo/365)*$dias;
                    



                    // vacaciones

                    $dias_vacas = ($dias*15)/365;
                    $dias_vacas = round($dias_vacas, 0, PHP_ROUND_HALF_DOWN);


                    // $vacas = ($sueldo/365)*($dias_vacas/30)*$dias;
                    $vacas = ($sueldo/365)*(15/30)*$dias;
                    


                    $ano_pasado = date("Y", strtotime ('-1 year'));
                    $ano_presente = date("Y");


                        // aguinaldo

                        $fecha_aguinaldo = $ano_pasado."-11-30";
                        $ano_aguinaldo = date_create($fecha_aguinaldo);

                        // validacion fecha completa
                        // $fecha_fin = $ano_presente."-11-30";
                        // $fin = date_create($fecha_fin);

                        $fin_aguinaldo = $fecha_fin;

                        // validacion de fecha de ingreso
                        if ($fecha_ingreso<$ano_aguinaldo) {
                            $cuando_entro_agui = "entro antes";

                            $inicio_aguinaldo = $ano_aguinaldo;
                        } else {
                            $cuando_entro_agui = "entro despues";

                            $inicio_aguinaldo = date_create($colab->fecha_ingreso);
                        }

                        $diff_aguinaldo = date_diff($inicio_aguinaldo, $fin_aguinaldo);

                        $dias_aguinaldo = $diff_aguinaldo->format('%a')+1;

                        $aguinaldo = ($sueldo/365)*$dias_aguinaldo;
                        // $aguinaldo = number_format($aguinaldo, 2, '.', ',');




                        // bono 14
                        $ano_bono_14 = $ano_pasado."-06-30";
                        $ano_bono_14 = date_create($ano_bono_14);

                        // validacion fecha completa
                        // $fin = date_create($ano_presente."-06-30");

                        $fin_bono = $fecha_fin;

                        // validacion de fecha de ingreso
                        if ($fecha_ingreso<$ano_bono_14) {
                            $cuando_entro_bono = "entro antes";

                            $inicio_bono = $ano_bono_14;

                        } else {
                            $cuando_entro_bono = "entro despues";

                            $inicio_bono = date_create($colab->fecha_ingreso);

                        }

                        $diff_bono_14 = date_diff($inicio_bono, $fin_bono);

                        $dias_bono_14 = $diff_bono_14->format('%a')+1;

                        $bono_14 = ($sueldo/365)*$dias_bono_14;
                        // $bono_14 = number_format($bono_14, 2, '.', ',');



                         
                    $request->inicio = date('Y-m-01');
                    $request->fin = $fecha_fin_string;
                    $request->id_colab = $request->id_colab;


                    // planilla
                    $planilla = App\Http\Controllers\Admin\accounting\payrollController::get_planilla($request);


                    $salario = $planilla['salario'];
                    $bono_ley = $planilla['bono_ley'];
                    $bono_extra = $planilla['bono_extra'];
                    $bono_antiguedad = $planilla['bono_antiguedad'];
                    $desc_dia = $planilla['desc_dia'];
                    $desc_septimo = $planilla['desc_septimo'];
                    $desc_igss = $planilla['desc_igss'];
                    $total_bonos = $planilla['total_bonos'];


                    // horas extras
                    $horas_extras = App\Http\Controllers\Admin\accounting\horasController::get_horas($request);

                    $total_horas = $horas_extras['total_horas'];

                    $extra_dia = $horas_extras['extra_dia'];
                    $extra_noche = $horas_extras['extra_noche'];
                    $extra_mixta = $horas_extras['extra_mixta'];
                    $extra_especial = $horas_extras['extra_especial'];
                    $extra_metas = $horas_extras['extra_metas'];
                    $conteo_metas = $horas_extras['conteo_metas'];

                    // viaticos
                    $viaticos_array = App\Http\Controllers\Admin\accounting\viaticosController::get_viaticos($request);

                    $total_viaticos = $viaticos_array['total_viaticos'];

                    $viaticos = $viaticos_array['viaticos'];
                    $movil = $viaticos_array['movil'];
                    $extras = $viaticos_array['extras'];


                    // descuentos
                    $descuentos = App\Http\Controllers\Admin\accounting\descuentosController::get_saldos($request);
                    
                    $desc_modulo = 0;                    

                    foreach ($descuentos as $desc) {
                        $desc_modulo += $desc['pago'];                        
                    }




                    // GRAN TOTAL
                    $total_descuentos = $desc_dia + $desc_septimo + $desc_igss + $desc_modulo;

                    $total = ($indemnizacion + $bono_14 + $aguinaldo + $vacas + $salario + $total_bonos + $total_horas + $total_viaticos) - $total_descuentos;




                    // aplicar filtros
                    $log_liquidacion = $model::whereYear("fecha_pago",$request->ano)
                                                ->where("id_colab",$request->id_colab)
                                                ->where('id_empresa',$request->id_empresa)
                                                ->first();
                                                          


                    $array_liquidacion = array(
                                          'id_empresa'=>$request->id_empresa,
                                          'id_colab'=>$request->id_colab,
                                          'indemnizacion'=>$indemnizacion,
                                          'aguinaldo'=>$aguinaldo,
                                          'bono_14'=>$bono_14,
                                          'vacaciones'=>$vacas,
                                          'salario'=>$salario,
                                          'total_bonos'=>$total_bonos,
                                          'total_horas'=>$total_horas,
                                          'total_viaticos'=>$total_viaticos,
                                          'total_descuentos'=>$total_descuentos,
                                          'total'=> $total,
                                          'detalle'=>''
                    );


                    $json_liquidacion = json_encode($array_liquidacion);


                    $indemnizacion = number_format($indemnizacion, 2, '.', ',');
                    $vacas = number_format($vacas, 2, '.', ',');

                    $aguinaldo = number_format($aguinaldo, 2, '.', ',');
                    $bono_14 = number_format($bono_14, 2, '.', ',');



                    $salario = number_format($salario, 2, '.', ',');
                    $bono_ley = number_format($bono_ley, 2, '.', ',');
                    $bono_extra = number_format($bono_extra, 2, '.', ',');
                    $bono_antiguedad = number_format($bono_antiguedad, 2, '.', ',');
                    $desc_dia = number_format($desc_dia, 2, '.', ',');
                    $desc_septimo = number_format($desc_septimo, 2, '.', ',');
                    $desc_igss = number_format($desc_igss, 2, '.', ',');
                    $total_bonos = number_format($total_bonos, 2, '.', ',');


                    $total_horas = number_format($total_horas, 2, '.', ',');
                    $total_viaticos = number_format($total_viaticos, 2, '.', ',');

                    $total = number_format($total, 2, '.', ',');

            @endphp
            <div class="col-3">
                <label  style="font-weight:bold;">Código:</label>
                {{ $colab->id }}<br>
                <label  style="font-weight:bold;">Nombre: </label>
                {{ $colab->nombres.' '.$colab->apellidos }}<br>
                <label  style="font-weight:bold;">Estado: </label>
                {{ $colab->estatus}}<br>
                <label  style="font-weight:bold;">Puesto: </label>
                {{ $colab->puesto }}<br>                  

                <label  style="font-weight:bold;">Ingreso: </label>
                {{ $colab->fecha_ingreso }}
                <br>

                <label  style="font-weight:bold;">Baja: </label>
                {{ $colab->fecha_baja }}
                <br>

            </div>


            <div class="col-3">

              <h5 style="font-weight:bold;">Liquidación:</h5>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Indemnización: </label>
              Q.{{ $indemnizacion }}
              <br>
              
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Aguinaldo: </label>
              Q.{{ $aguinaldo }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Bono 14: </label>
              Q.{{ $bono_14 }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Vacaciones: </label>
              Q.{{ $vacas }}
              <br>
              <br>


              <h5  style="font-weight:bold;">Salario: </h5>
              &nbsp;&nbsp;&nbsp; Q.{{ $salario }}
              <br><br>

              <h5>Bonos</h5>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Bono de ley: </label>
              Q.{{ $bono_ley }}
              <br>
              
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Extra: </label>
              Q.{{ $bono_extra }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Antiguedad: </label>
              Q.{{ $bono_antiguedad }}
              <br>
              <br>

            </div>


            <div class="col-3">


              <h5>Horas Extras</h5>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Diurna: </label>
              Q.{{ $extra_dia }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Nocturna: </label>
              Q.{{ $extra_noche }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Especial: </label>
              Q.{{ $extra_especial }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Mixta: </label>
              Q.{{ $extra_mixta }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Metas: </label>
              Q.{{ $extra_metas }} ({{ $conteo_metas }} metas extras)
              <br>
              <br>


              <h5>Viaticos</h5>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Normal: </label>
              Q.{{ $viaticos }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Movil: </label>
              Q.{{ $movil }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Extras: </label>
              Q.{{ $extras }}
              <br>



            </div>

            <div class="col-3">
              <h5>Descuentos</h5>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Descuentos dia: </label>
              Q.{{ $desc_dia }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">Descuento septimo: </label>
              Q.{{ $desc_septimo }}
              <br>
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold;">IGSS: </label>
              Q.{{ $desc_igss }}
              <br>

@foreach ($descuentos as $descuento)
              &nbsp;&nbsp;&nbsp;<label  style="font-weight:bold; text-transform: capitalize;">{{ $descuento['tipo'] }}:</label>
              Q.{{ $descuento['pago'] }}
              <br>
@endforeach
              <br>

@php 

    if ($total>0) {
        $color = 'green';
    } else {
        $color = 'red';
    }

@endphp

                  <h4  style="font-weight:bold; color: {{ $color }};">Total:
                  Q.{{ $total }}
                  </h4>


            </div>


<!--
            <div class="col-3">
              <h4  style="font-weight:bold;">Total:
                          Q. 
                          @if(!empty($log_liquidacion->total))
                              {{ number_format($log_liquidacion->total, 2, '.', ',') }}
                          @else
                              {{ $total }}
                          @endif
              </h4>
            </div>

            <div class="col-3">

            </div>
-->

            <div class="col-4">
              <br><br>

@if (@$log_liquidacion->estado=='')              
              <form id="form_liquidacion" action="{{ url('aprobar_colab_liquidacion') }}" method="post" onsubmit="event.preventDefault(); return changeLog();">
@else
              <form id="form_liquidacion" action="{{ url('update_liquidacion')}}" method="post" onsubmit="event.preventDefault(); return changeLog();">
                <input type="hidden" name="id_liquidacion" value="{{ @$log_liquidacion->id }}">
@endif

                @csrf
                <input type="hidden" name="datos_liquidacion" value="{{ $json_liquidacion }}">


                <label for="" style="color: green;">Procesar pago</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Procesado"
                {{(!empty(@$log_liquidacion->estado)&&@$log_liquidacion->estado=='Procesado'||!empty(@$log_liquidacion->estado)&&@$log_liquidacion->estado==''?'checked':'')}}>
                <label for="" style="color: red;">Denegar pago</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Denegado"
                {{(!empty(@$log_liquidacion->estado)&&@$log_liquidacion->estado=='Denegado'||!empty(@$log_liquidacion->estado)&&@$log_liquidacion->estado==''?'checked':'')}}>

              <div style="width: 200px;">
                  Fecha de Pago
                  <input type="date" name="fecha_pago" id="fecha_pago" class="w-100 form-control" value="{{ @$log_liquidacion->fecha_pago }}" required>
              </div>
              <br>
                
@if (@$log_liquidacion->estado=='') 
                <input type="submit" class="btn btn-success m-1" value="Guardar">
@else
                <input type="submit" id="bt_actualizar" class="btn btn-success m-1" value="Actualizar">
@endif
                <br>

              </form>

            </div>
          </div>
      </div>
  </div>




 </div>
</div>



<style>
.ul-widget-s7::before{
    left: 20.3% !important;
}
</style>


  </div>




<div class="row">

    <div class="col-lg-8 col-xl-8 mb-4 offset-2">
        <div class="card">
            <div class="card-body">

                <div class="ul-widget__head __g-support v-margin">
                    <div class="ul-widget__head-label">
                        <h3 class="ul-widget__head-title">Últimos cambios</h3>
                    </div>
                </div>

                <div class="ul-widget__body">

                    @foreach($data_changelog as $d)
                        <div class="ul-widget-s7">
                            <div class="ul-widget-s7__items" style="width:100%;">
                                <span class="ul-widget-s7__item-time" style="width:19%;">{{$d->created_at->format('d/m/Y H:i')}}</span>

                                <div class="ul-widget-s7__item-circle" style="width:11%;">
                                    <p class="badge-dot-warning ul-widget7__big-dot"></p>
                                </div>

                                <div class="ul-widget-s7__item-text" style="width:70%;">
                                    Edición:
                                    {{$d->comment}}
                                    en
                                    <span class="badge badge-pill badge-primary m-2">{{$d->element}}</span>
                                    por
                                    <span class="badge badge-pill badge-success m-2">{{$d->user}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <br>
            </div>
        </div>
    </div>

</div>

<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
h5{
  font-weight: bold;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
hr{

}
</style>
@endsection

@section('page-js')



@endsection

@section('bottom-js')

{{-- <script src="{{asset('assets/js/quill.script.js')}}"></script> --}}
<script>
$(document).ready(function () {
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });

});



  function changeLog(){
      Swal.fire({
          title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                    <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                </div>`,
          html:`
               <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                   <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                       <div class="modal-body">
                           <div class="row">
                               <label class="col-form-label">Comentario</label>
                               <textarea id="changeLogComment" class="form-control"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               `,
           preConfirm: function(){
            $('#bt_actualizar').hide();

               let val = $('#changeLogComment').val(),
                   cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`;
             $('#form_liquidacion').append(cad).attr('onsubmit', '').submit();
             return true
         }
      })
  }



</script>


@endsection
