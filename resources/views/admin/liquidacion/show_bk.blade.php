@extends('admin.layouts.master')

@section('main-content')

@php  $creat=array();
  $colors = ["ausencia"=>'#f14848',"sin horario"=>'#a99944',"asistencia"=>'#38b6f5',
            "Permiso"=>'#ecffef',"Vacaciones"=>'#f3fbff',"Iggs"=>'#f3fbff'];
@endphp
<div class="breadcrumb">
    <h1 class="mr-2"> <a href="{{ url('prestaciones') }}?id_empresa={{ $request->id_empresa }}&id_cliente={{ $request->id_cliente }}&id_sucursal={{ $request->id_sucursal }}&id_area={{ $request->id_area }}&inicio={{ $request->inicio }}&fin={{ $request->fin }}">Listado prestaciones</a> </h1>
    <ul>
        <li>Detalle de prestaciones</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
  <div class="col-12">
    <div class="card o-hidden mb-4">
      <div class="card-header bg-transparent">
          <div class="card-title mb-0">Datos del empleado</div>
      </div>
      <div class="card-body">
          <div class="row">

<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

            @php

                    $sueldo = $colab->sueldo*30;

                    $fecha_ingreso = date_create($colab->fecha_ingreso);
                    
                    $ano_pasado = date("Y", strtotime ('-1 year'));
                    $ano_presente = date("Y");

                    if ($request->tipo=='aguinaldo') {

                        // aguinaldo

                        $fecha_aguinaldo = $ano_pasado."-11-30";
                        $ano_aguinaldo = date_create($fecha_aguinaldo);

                        $fecha_fin = $ano_presente."-11-30";
                        $fin = date_create($fecha_fin);

                        // validacion de fecha de ingreso
                        if ($fecha_ingreso>$ano_aguinaldo) {
                            $cuando_entro = "entro antes";

                            $fecha_inicio = $fecha_aguinaldo;
                            $inicio = $ano_aguinaldo;
                        } else {
                            $cuando_entro = "entro despues";

                            $fecha_inicio = $colab->fecha_ingreso;
                            $inicio = date_create($fecha_inicio);
                        }

                        $diff_aguinaldo = date_diff($inicio, $fin);

                        $dias_aguinaldo = $diff_aguinaldo->format('%a')+1;

                        $aguinaldo = ($sueldo/365)*$dias_aguinaldo;
                        // $aguinaldo = number_format($aguinaldo, 2, '.', ',');

                        $total = $aguinaldo;

                        $total_dias = $dias_aguinaldo;

                    } elseif ($request->tipo=='bono 14') {

                        // bono 14
                        $ano_bono_14 = $ano_pasado."-06-30";
                        $ano_bono_14 = date_create($ano_bono_14);

                        $fin = date_create($ano_presente."-06-30");

                        // validacion de fecha de ingreso
                        if ($fecha_inicio>$ano_bono_14) {
                            $cuando_entro = "entro antes";

                            $inicio = $ano_bono_14;

                            $diff_bono_14 = date_diff($ano_bono_14, $fin);
                        } else {
                            $cuando_entro = "entro despues";

                            $diff_bono_14 = date_diff($inicio, $fin);
                        }

                        // $diff_bono_14 = date_diff($ano_bono_14, $fin);

                        $dias_bono_14 = $diff_bono_14->format('%a')+1;

                        $bono_14 = ($sueldo/365)*$dias_bono_14;
                        // $bono_14 = number_format($bono_14, 2, '.', ',');

                        $total = $bono_14;

                        $total_dias = $dias_bono_14;

                    }
                                                          


                    $array_prestaciones = array(
                                          'id_empresa'=>$request->id_empresa,
                                          'id_colab'=>$colab->id,
                                          'id_cliente'=>$request->id_cliente,
                                          'id_sucursal'=>$request->id_sucursal,
                                          'id_area'=>$request->id_area,
                                          'tipo'=>$request->tipo,
                                          'total'=> $total,
                                          'detalle'=>'Desde '.date_format($inicio, 'd-m-Y').' hasta '.date_format($fin, 'd-m-Y')
                    );


                    $json_prestaciones = json_encode($array_prestaciones);


                    $total = number_format($total, 2, '.', ',');

            @endphp
            <div class="col-3">
                <label  style="font-weight:bold;">Código:</label>
                {{ $colab->id }}<br>
                <label  style="font-weight:bold;">Nombre: </label>
                {{ $colab->nombres.' '.$colab->apellidos }}<br>
                <label  style="font-weight:bold;">Estado: </label>
                {{ $colab->estatus}}<br>
                <label  style="font-weight:bold;">Puesto: </label>
                {{ $colab->puesto }}<br>                  

                <label  style="font-weight:bold;">Tipo de Prestacion: </label>
                <span style="text-transform: capitalize;">{{ $request->tipo }}</span>
                <br>

                  <label  style="font-weight:bold;">Desde: </label>
                  {{ date_format($inicio, 'd-m-Y') }}
                  <br>
                  <label  style="font-weight:bold;">Hasta: </label>
                  {{ date_format($fin, 'd-m-Y') }}
                  <br>

            </div>

            <div class="col-3">
              <h4  style="font-weight:bold;">Total:
                          Q. 
                          @if(!empty($log_prestaciones->total))
                              {{ number_format($log_prestaciones->total, 2, '.', ',') }}
                          @else
                              {{ $total }}
                          @endif
              </h4>
            </div>

            <div class="col-3">

            </div>

            <div class="col-4">
              <br><br>

@if (@$log_prestaciones->estado=='')              
              <form id="form_prestaciones" action="{{ url('aprobar_colab_prestaciones') }}" method="post" onsubmit="event.preventDefault(); return changeLog();">
@else
              <form id="form_prestaciones" action="{{ url('update_prestaciones')}}" method="post" onsubmit="event.preventDefault(); return changeLog();">
                <input type="hidden" name="id_prestaciones" value="{{ @$log_prestaciones->id }}">
@endif

                @csrf
                <input type="hidden" name="datos_prestaciones" value="{{ $json_prestaciones }}">


                <label for="" style="color: green;">Procesar pago</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Procesado"
                {{(!empty(@$log_prestaciones->estado)&&@$log_prestaciones->estado=='Procesado'||!empty(@$log_prestaciones->estado)&&@$log_prestaciones->estado==''?'checked':'')}}>
                <label for="" style="color: red;">Denegar pago</label><input type="radio"  class="btn btn-warning m-1"
                name="estado" value="Denegado"
                {{(!empty(@$log_prestaciones->estado)&&@$log_prestaciones->estado=='Denegado'||!empty(@$log_prestaciones->estado)&&@$log_prestaciones->estado==''?'checked':'')}}>

              <div style="width: 200px;">
                  Fecha de Pago
                  <input type="date" name="fecha_pago" id="fecha_pago" class="w-100 form-control" value="{{ @$log_prestaciones->fecha_pago }}" required>
              </div>
              <br>
                
@if (@$log_prestaciones->estado=='') 
                <input type="submit" class="btn btn-success m-1" value="Guardar">
@else
                <input type="submit" id="bt_actualizar" class="btn btn-success m-1" value="Actualizar">
@endif
                <br>

              </form>

            </div>
          </div>
      </div>
  </div>




 </div>
</div>



<style>
.ul-widget-s7::before{
    left: 20.3% !important;
}
</style>


  </div>




<div class="row">

    <div class="col-lg-8 col-xl-8 mb-4 offset-2">
        <div class="card">
            <div class="card-body">

                <div class="ul-widget__head __g-support v-margin">
                    <div class="ul-widget__head-label">
                        <h3 class="ul-widget__head-title">Últimos cambios</h3>
                    </div>
                </div>

                <div class="ul-widget__body">

                    @foreach($data_changelog as $d)
                        <div class="ul-widget-s7">
                            <div class="ul-widget-s7__items" style="width:100%;">
                                <span class="ul-widget-s7__item-time" style="width:19%;">{{$d->created_at->format('d/m/Y H:i')}}</span>

                                <div class="ul-widget-s7__item-circle" style="width:11%;">
                                    <p class="badge-dot-warning ul-widget7__big-dot"></p>
                                </div>

                                <div class="ul-widget-s7__item-text" style="width:70%;">
                                    Edición:
                                    {{$d->comment}}
                                    en
                                    <span class="badge badge-pill badge-primary m-2">{{$d->element}}</span>
                                    por
                                    <span class="badge badge-pill badge-success m-2">{{$d->user}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <br>
            </div>
        </div>
    </div>

</div>

<style media="screen">
input[type=text],input[type=time],input[type=number]{
  width: 100%;
}
h5{
  font-weight: bold;
}
.button{
  display: inline;
  margin: 0;
  padding: 0;
  height: 26px;
  margin-top: -14px;
  border: 0;
  background: none;
  color: #093982;
}
.escrit .form-group{
  float: left;
  width: auto;
}
.escrit .form-group.numbr{
  float: left;
  width: auto;
}
.rubrica td{
    vertical-align: top;
}
.rubrica tr{
    border-bottom: 3px solid #d2d2d2;
}
.numbr{
      width: 72px;
}
.red{
  border:1px solid red;
}
td{
  white-space:nowrap;
}
hr{

}
</style>
@endsection

@section('page-js')



@endsection

@section('bottom-js')

{{-- <script src="{{asset('assets/js/quill.script.js')}}"></script> --}}
<script>
$(document).ready(function () {
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    font: []
                }, {
                    size: []
                }],
                ["bold", "italic", "underline", "strike"],
                [{
                    color: []
                }, {
                    background: []
                }],
                [{
                    script: "super"
                }, {
                    script: "sub"
                }],
                [{
                    header: "1"
                }, {
                    header: "2"
                }, "blockquote", "code-block"],
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }, {
                    indent: "-1"
                }, {
                    indent: "+1"
                }],
                ["direction", {
                    align: []
                }],
                ["link", "image", "video", "formula"],
                ["clean"]
            ]
        },
        theme: 'snow'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      if (source == 'api') {
        console.log("An API call triggered this change.");
      } else if (source == 'user') {
        let val = $('#full-editor').children()[0].innerHTML
        $('#full-editor-data').val(val)
      }
    });

});



  function changeLog(){
      Swal.fire({
          title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                    <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                </div>`,
          html:`
               <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                   <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                       <div class="modal-body">
                           <div class="row">
                               <label class="col-form-label">Comentario</label>
                               <textarea id="changeLogComment" class="form-control"></textarea>
                           </div>
                       </div>
                   </div>
               </div>
               `,
           preConfirm: function(){
            $('#bt_actualizar').hide();

               let val = $('#changeLogComment').val(),
                   cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`;
             $('#form_prestaciones').append(cad).attr('onsubmit', '').submit();
             return true
         }
      })
  }



</script>


@endsection
