@extends('admin.layouts.master')
@section('main-content')
  <!-- row opened -->
  <div class="row mt-5">
    <div class="col-xl-12 product">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Reportes</h3>
          <div class="card-options ">
            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
            <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            {{-- <div class="col-lg-12 col-xl-8">
              <div class="chart-wrapper">
                <canvas id="line-chart" class=" chartjs-render-monitor chart-dropshadow2 overflow-hidden"></canvas>
              </div>
            </div> --}}
            <div class="col-lg-12 col-xl-4 lg-mt-5">
              <div class="row">
                <div class="col-6">
                  <div class="card box-shadow-0 overflow-hidden">
                    <div class="card-body p-4">
                      <div class="text-center">
                         <i class="fa fa-tasks fa-2x text-primary text-primary-shadow"></i>
                         <h3 class="mt-3 mb-0 ">07</h3>
                         <small class="text-muted">Contacto</small>
                      </div>
                    </div>
                  </div>
                </div>
                 <div class="col-6">
                    <div class="card box-shadow-0 overflow-hidden">
                    <div class="card-body p-4">
                      <div class="text-center">
                         <i class="fa fa-ticket fa-2x text-secondary text-secondary-shadow"></i>
                          <h3 class="mt-3 mb-0 ">03</h3>
                         <small class="text-muted">Tablas</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="card box-shadow-0 mb-0 overflow-hidden">
                    <div class="card-body p-4">
                      <div class="text-center">
                         <i class="fa fa-bug fa-2x text-success text-success-shadow"></i>
                         <h3 class="mt-3 mb-0 ">02</h3>
                         <small class="text-muted">Datos</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6 ">
                  <div class="card box-shadow-0 mb-0 overflow-hidden">
                    <div class="card-body p-4">
                      <div class="text-center">
                         <i class="fa fa-folder-open-o fa-2x text-info text-info-shadow"></i>
                         <h3 class="mt-3 mb-0 ">04</h3>
                         <small class="text-muted">Estadísticas</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>



@if ($descuentos!='')
                  <div class="col-lg-4 col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="card-title"><i class="fa fa-file font-dark"></i> Descuentos por Aprobar</div>

                                  <div style="height: 350px; overflow: auto;">
@forelse($descuentos as $descuento)

@php
    
    $colab = $modelo_colabs::find($descuento->id_colab);

@endphp
                                <div class="d-flex align-items-center border-bottom-dotted-dim pb-3 mb-3 mr-3">
                                    <div class="flex-grow-1">
                                        <h6 class="m-0">{{ $colab->nombres}} {{ $colab->apellidos}}</h6>
                                        <small>{{ $colab->puesto }}</small><br>
                                        <p class="m-0 text-small text-muted">Tipo: <span style="text-transform: capitalize;">{{ $descuento->tipo }}</span></p>
                                    </div>
                                    <div>
                                      <a  href="{{ url('descuentos') }}/{{$descuento->id }}" target="_blank">
                                        <button class="btn btn-outline-primary btn-rounded btn-sm">Ver</button>
                                      </a>
                                    </div>
                                </div>
@empty
                                    <p class="text-center"
                                       style="padding: 4px; margin-top: 26%;">No hay descuentos por aprobar</p>
@endforelse
                                </div>

                            </div>
                        </div>
                    </div>
@endif



                  <div class="col-lg-4 col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="card-title"><i class="fa fa-birthday-cake font-dark"></i> Cumpleañeros del mes</div>

                                  <div style="height: 350px; overflow: auto;">
@forelse($current_month_birthdays as $birthday)


                                <div class="d-flex align-items-center border-bottom-dotted-dim pb-3 mb-3 mr-3"><img class="avatar-md rounded mr-3" src="{{ asset('storage/' . $birthday->foto) }}" alt="">
                                    <div class="flex-grow-1">
                                        <h6 class="m-0">{{$birthday->nombres}} {{$birthday->apellidos}}</h6>
                                        <small>{{ $birthday->puesto }}</small><br>
                                        <p class="m-0 text-small text-muted">{{date('j M ',strtotime($birthday->fecha)) }}</p>
                                    </div>
                                    <div>
                                      <a  href="{{ route('admin.editar_colaboradores', ['id' => $birthday->id]) }}">
                                        <button class="btn btn-outline-primary btn-rounded btn-sm">Ver</button>
                                      </a>
                                    </div>
                                </div>
@empty
                                    <p class="text-center"
                                       style="padding: 4px; margin-top: 26%;">No hay cumpleañeros este mes</p>
@endforelse
                                </div>

                            </div>
                        </div>
                    </div>


          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- row closed -->
@endsection
