@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
    var k0d=[{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
            {type:"chkbxstyl",tl:"Seleccionar todos",nm:"semana",elv:"0",vl:[['todos', '']],nxt:4, add:'style="margin-bottom: 20px;" onclick="selectMecheck(this)"'},
            {type:"stub",tl:"",nm:"",elv:"",vl:"",nxt:8,add:'style="color:white;"'},

            {type:"chkbxstyl",tl:"Lunes",id:"check_lun",nm:"lunes",elv:"0",vl:[['lunes', '']],nxt:4},
            {type:"chkbxstyl",tl:"Martes",id:"check_mar",nm:"martes",elv:"0",vl:[['martes', '']],nxt:4},
            {type:"chkbxstyl",tl:"Miércoles",id:"check_mie",nm:"miercoles",elv:"0",vl:[['miercoles', '']],nxt:4},
            {type:"chkbxstyl",tl:"Jueves",id:"check_jue",nm:"jueves",elv:"0",vl:[['jueves', '']],nxt:4},
            {type:"chkbxstyl",tl:"Viernes",id:"check_vie",nm:"viernes",elv:"0",vl:[['viernes', '']],nxt:4},
            {type:"chkbxstyl",tl:"Sábado",id:"check_sab",nm:"sabado",elv:"0",vl:[['sabado', '']],nxt:4},
            {type:"chkbxstyl",tl:"Domingo",id:"check_dom",nm:"domingo",elv:"0",vl:[['domingo', '']],nxt:4, add:'style="margin-bottom: 20px;"'},
            {type:"stub",tl:"",nm:"",elv:"",vl:"",nxt:8,add:'style="color:white;"'},

            {type:"time",tl:"Hora de entrada",nm:"entry",elv:"1",nxt:6},
            {type:"time",tl:"Hora de salida",nm:"exit",elv:"2",nxt:6},
            {type:"hddn",nm:"tipo",vl:"semana"},

            {type:"txt",tl:"Total diurnas",nm:"horas_diurnas",elv:"3",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
            {type:"txt",tl:"Total nocturnas",nm:"horas_nocturnas",elv:"4",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
            {type:"txt",tl:"Total mixtas",nm:"horas_mixtas",elv:"8",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
            {type:"chkbxstyl",tl:"Cobrar receso",nm:"cobrar_receso",vl:[["si","si"],["no","no"]],elv:"5",nxt:6},
            {type:"slct",tl:"Tiempo de receso",nm:"tiempo_receso",vl:[["00:00","--seleccionar--"],["00:30","00:30"],["01:00","01:00"],["01:30","01:30"],["02:00","02:00"]],elv:"6",nxt:6,add:"omit=T"},
            {type:"rd",tl:"Tipo de receso",nm:"tipo_receso",vl:[["Diurno","diurno"],["Nocturno","nocturno"]],elv:"7",nxt:6}
          ];

    var kad=[{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
             {type:"slct",tl:"Día de la semana",nm:"day",elv:"0",vl:[['Lunes', 'Lunes'],['Martes','Martes'],['Miércoles','Miércoles'],['Jueves','Jueves'],['Viernes','Viernes'],['Sábado','Sábado'],['Domingo','Domingo']],nxt:12},
             {type:"time",tl:"Hora de entrada",nm:"entry",elv:"1",nxt:6},
             {type:"time",tl:"Hora de salida",nm:"exit",elv:"2",nxt:6},

             {type:"txt",tl:"Total diurnas",nm:"horas_diurnas",elv:"3",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
             {type:"txt",tl:"Total nocturnas",nm:"horas_nocturnas",elv:"4",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
             {type:"txt",tl:"Total mixtas",nm:"horas_mixtas",elv:"8",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
             {type:"chkbxstyl",tl:"Cobrar receso",nm:"cobrar_receso",vl:[["si","si"],["no","no"]],elv:"5",nxt:6},
             {type:"slct",tl:"Tiempo de receso",nm:"tiempo_receso",vl:[["00:00","--seleccionar--"],["00:30","00:30"],["01:00","01:00"],["01:30","01:30"],["02:00","02:00"]],elv:"6",nxt:6,add:"omit=T"},
             {type:"rd",tl:"Tipo de receso",nm:"tipo_receso",vl:[["Diurno","diurno"],["Nocturno","nocturno"]],elv:"7",nxt:6}
           ];
    var ked=[{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
             {type:"time",tl:"Hora de entrada",nm:"entry",elv:"0",nxt:6},
             {type:"time",tl:"Hora de salida",nm:"exit",elv:"1",nxt:6},

             {type:"txt",tl:"Total diurnas",nm:"horas_diurnas",elv:"2",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
             {type:"txt",tl:"Total nocturnas",nm:"horas_nocturnas",elv:"3",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
             {type:"txt",tl:"Total mixtas",nm:"horas_mixtas",elv:"4",nxt:6,add:"placeholder=\"00:00\" onkeyup=\"checkTime(this);\""},
             {type:"chkbxstyl",tl:"Cobrar receso",nm:"cobrar_receso",vl:[["si","si"],["no","no"]],elv:"5",nxt:4},
             {type:"slct",tl:"Tiempo de receso",nm:"tiempo_receso",vl:[["00:00","--seleccionar--"],["00:30","00:30"],["01:00","01:00"],["01:30","01:30"],["02:00","02:00"]],elv:"6",nxt:4,add:"omit=T"},
             {type:"rd",tl:"Tipo de receso",nm:"tipo_receso",vl:[["Diurno","diurno"],["Nocturno","nocturno"]],elv:"7",nxt:4}
            ];
    var conform={action:"{{route('admin.permissions.store')}}"}
        valdis={clase:"red",text:1,checkbox:1,radio:1};
        function checkTime(t){
           var v = t.value;
           if (v.match(/^\d{2}$/) !== null) {
               t.value = v + ':';
               t.value=t.value;
               return ;
           }
       }
</script>
@php
    $criti = [];
@endphp

<div class="breadcrumb">
    <h1 class="mr-2">{{$breadcrumb['base']}}</h1>
    <ul>
        <li> <a href="{{url($breadcrumb['inicio'])}}"> Inicio</a> </li>
        <li>Horarios</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
		  <h3 class="card-title">
            Horarios de: <b>{{$name}}</b>
          </h3>
          <br>
          <h4>Agregar horario</h4>
          <h5><a href="javascript:"class="" onclick="newfloatv2(k0d);"> <i class="ion-ios7-plus-outline "></i>&nbsp;&nbsp;Agregar semana </a></h5>
          <h5><a href="javascript:"class="" onclick="newfloatv2(kad);"> <i class="ion-ios7-plus-outline "></i>&nbsp;&nbsp;Agregar día </a></h5>
				</div>

        <div class="card-body">
          <div class="table-responsive">
            <table id="horarios_table" class="display table table-striped table-bordered" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Día</th>
                      <th>Hora de entrada</th>
                      <th>Hora de salida</th>
                      <th>Total diurnas / Total nocturnas / Mixtas</th>
                      <th>Cobrar receso</th>
                      <th>Tiempo receso</th>
                      <th>Tipo receso</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($files as $f)
                      <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td class="hidden-xs">{{$f->day}}</td>
                        <td class="hidden-xs">{{$f->entry}}</td>
                        <td class="hidden-xs">{{$f->exit}}</td>
                        <td class="hidden-xs">{{$f->horas_diurnas=='00:00:00'?'00:00':
                          date("H:i",strtotime(Helper::convertToHoursMins($f->horas_diurnas)))}} /
                          {{$f->horas_nocturnas=='00:00:00'?'00:00':date("H:i",strtotime(Helper::convertToHoursMins($f->horas_nocturnas)))}} /
                          {{$f->horas_mixtas=='00:00:00'?'00:00':date("H:i",strtotime(Helper::convertToHoursMins($f->horas_mixtas)))}}</td>
                        <td class="hidden-xs">{{$f->cobrar_receso}}</td>
                        <td class="hidden-xs">{{$f->tiempo_receso=='00:00:00'||$f->tiempo_receso=='00:00'?'00:00':date("H:i",strtotime(Helper::convertToHoursMins($f->tiempo_receso)))}}</td>
                        <td class="hidden-xs">{{$f->tipo_receso}}</td>

                        <td>

                          <a href="javascript:" class="" onclick="modifyfloat('{{$f->id}}',ked,criteria);" title="Editar archivo">
                              <i class="text-20"data-feather="edit-3"></i>
                          </a>

                          <a href="javascript:"onclick="deleteD('{{$f->id}}','{{ csrf_token() }}');" title="Eliminar archivo">
                              <i class="text-12" data-feather="trash"></i>
                          </a>
                        </td>
                      </tr>
                      @php
                        $criti[$f->id] = [$f->entry,
                                          $f->exit,
                                         ($f->horas_diurnas=='00:00:00'?'00:00':date("H:i",strtotime(Helper::convertToHoursMins($f->horas_diurnas)))),
                                         ($f->horas_nocturnas=='00:00:00'?'00:00':date("H:i",strtotime(Helper::convertToHoursMins($f->horas_nocturnas)))),
                                         ($f->horas_mixtas=='00:00:00'?'00:00':date("H:i",strtotime(Helper::convertToHoursMins($f->horas_mixtas)))),
                                          $f->cobrar_receso,
                                        [($f->tiempo_receso=='00:00:00'||$f->tiempo_receso=='00:00'?'00:00':date("H:i",strtotime(Helper::convertToHoursMins($f->tiempo_receso))))],
                                          $f->tipo_receso];
                      @endphp

                    @endforeach
    		       </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = <?=json_encode($criti)?>;
      function fillimg(sto)
      {
        $("#imag").val(sto);
        flotantecloseblank();
      }
    </script>
    @component('components.messagesForm')
    @endcomponent
@endsection

@section('bottom-js')
    <script>
        $('#horarios_table').DataTable();
        function checkTime(t){
          var v = t.value;
          if (v.match(/^\d{2}$/) !== null) {
              t.value = v + ':';
              t.value=t.value;
              return ;
          }
        }
        function selectMecheck(elm){
            let val = elm.value,
                check = elm.checked,
                clun = document.getElementById("check_lun"),
                cmar = document.getElementById("check_mar"),
                cmie = document.getElementById("check_mie"),
                cjue = document.getElementById("check_jue"),
                cvie = document.getElementById("check_vie"),
                csab = document.getElementById("check_sab"),
                cdom = document.getElementById("check_dom");


            if (check){
                clun.checked = true;
                cmar.checked = true;
                cmie.checked = true;
                cjue.checked = true;
                cvie.checked = true;
                csab.checked = true;
                cdom.checked = true;
            }else{
                clun.checked = false;
                cmar.checked = false;
                cmie.checked = false;
                cjue.checked = false;
                cvie.checked = false;
                csab.checked = false;
                cdom.checked = false;
            }

        }
    </script>
@endsection
