@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Título",nm:"name",elv:"0"}
        };
    valdis={clase:"red",text:1};
</script>
@php
$monts = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
@endphp

<div class="main-content" id="buzon">
  <select data-placeholder="Colaborador" class="chosen-select" onchange="selectMe(this)" id="company">
    <option value="">--elegir empresa--</option>
    @foreach ($companyList as $k => $v)
      <option value="{{$v[0]}}" {{(isset($company)&&$v[0]==$company?'selected':'')}}>{{$v[1]}}</option>
    @endforeach
  </select>
                <!-- MAIN SIDEBAR CONTAINER-->
  <div class="inbox-main-sidebar-container sidebar-container" id="inbox" data-sidebar-container="main" style="height: 660px;">
      <div class="inbox-main-content sidebar-content" data-sidebar-content="main" style="margin-left: 180px;">
          <!-- SECONDARY SIDEBAR CONTAINER-->

        <div class="inbox-secondary-sidebar-container box-shadow-1 sidebar-container" data-sidebar-container="secondary">

            <div data-sidebar-content="secondary" id="readmessage"class="sidebar-content" style="margin-left: 360px;">
                <div class="inbox-secondary-sidebar-content position-relative" style="min-height: 500px">
                    <div class="inbox-topbar box-shadow-1 perfect-scrollbar rtl-ps-none pl-3 ps" data-suppress-scroll-y="true">
                        <!-- <span class="d-sm-none">Test</span>-->
                        {{-- <a class="link-icon d-md-none" data-sidebar-toggle="main">
                          <i class="icon-regular i-Arrow-Turn-Left"></i>
                        </a>
                        <a class="link-icon mr-3 d-md-none" data-sidebar-toggle="secondary">
                          <i class="icon-regular mr-1 i-Left-3"></i> Inbox
                        </a> --}}

                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                          <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps__rail-y" style="top: 0px; right: 0px;">
                          <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                        </div>
                     </div>
                    <!-- EMAIL DETAILS-->
                    @if(!empty($indivu))
                      @php
                        $dtat = $indivu->getColaborator()->first();
                        $date = explode('-',date('Y-m-d',strtotime($indivu->created_at)));
                      @endphp
                      <div class="inbox-details perfect-scrollbar rtl-ps-none ps" data-suppress-scroll-x="true">
                          <div class="row no-gutters">
                              <div class="mr-2" style="width: 36px"><img class="rounded-circle" src="{{ (!empty($dtat->foto)?asset('storage/' . $dtat->foto):'') }}" alt=""></div>
                              <div class="col-xs-12">
                                  <p class="m-0">  {{$dtat->nombres.' '.$dtat->apellidos}}</p>
                                  <p class="text-12 text-muted">{{$date[2].' '.$monts[((INT)$date[1]-1)].' '.$date[0] }}</p>
                              </div>
                          </div>
                          @switch($slectd)
                            @case('timeperms')
                              <h4 class="mb-3">Permiso con reposisión de tiempo</h4>
                              <div>
                                <p> <b>Fecha de salida:</b> {{date("d - M - Y",strtotime($indivu->dateOut))}}</p>
                                <p> <b>Fecha de entrada:</b> {{date("d - M - Y",strtotime($indivu->dateEnter))}}</p>
                                <p> <b>Fecha de reposisión entrada:</b> {{date("d - M - Y",strtotime($indivu->deteRepEnter))}}</p>
                                <p> <b>Fecha de reposisión salida:</b> {{date("d - M - Y",strtotime($indivu->deteRepOut))}}</p>

                                <form class="" action="{{route("admin.inboxpetitions.store",[$company,$inbox,$slectd,$indivu->id])}}" method="post">
                                  @csrf
                                  <p>
                                      {{$indivu->description}}
                                  </p>
                                  <p>
                                    <div class="col-md-12 form-group ml-0 mb-3 pl-0">
                                      <label for="credit1">Respuesta</label>
                                      <textarea name="reponse" rows="8" cols="80" class="form-control"
                                      >{{($indivu->status>0?$indivu->reponse:'')}}</textarea>
                                    </div>
                                  </p>

                                  <p>
                                    <label class="switch pr-5 switch-primary ml-0  mr-3 ">
                                      <span>{{(!empty($indivu->status)&&$indivu->status==1||$indivu->status==0?'Aprobado':'Denegado')}}</span>
                                      <input type="checkbox" name="status" value="1"
                                      {{($indivu->status==1?'checked="true"':'')}}
                                      /><span class="slider"></span>
                                    </label>
                                    <input type="submit" name="" value="Guardar" class="btn btn-primary ripple m-1 float-right">
                                  </p>
                              </form>

                             </div>
                            @break;
                            @case('defhour')
                              <h4 class="mb-3">Permiso sin reposisión de tiempo</h4>
                              <div>
                                <p> <b>Fecha de permisos:</b> {{date("d - M - Y",strtotime($indivu->datePerm))}}</p>
                                <p> <b>Tipo:</b> {{$indivu->typos}}</p>
                                @if(strtolower($indivu->typos)=='otro')
                                  <p> <b>Razon:</b> {{$indivu->typeOther}}</p>
                                @endif
                                <p> <b>Descripción:</b> {{$indivu->description}}</p>
                                <form class="" action="{{route("admin.inboxpetitions.store",[$company,$inbox,$slectd,$indivu->id])}}" method="post">
                                  @csrf
                                  <p>
                                      {{$indivu->description}}
                                  </p>
                                  <p>
                                    <div class="col-md-12 form-group mb-3 ml-0 pl-0">
                                      <label for="credit1">Respuesta</label>
                                      <textarea name="reponse" rows="8" cols="80" class="form-control"
                                      >{{($indivu->status>0?$indivu->reponse:'')}}</textarea>
                                    </div>
                                  </p>

                                  <p>
                                    <label class="switch pr-5 switch-primary mr-3 ml-0 ">
                                      <span>{{(!empty($indivu->status)&&$indivu->status==1||$indivu->status==0?'Aprobado':'Denegado')}}</span>
                                      <input type="checkbox" name="status" value="1"
                                      {{($indivu->status==1?'checked="true"':'')}}
                                      /><span class="slider"></span>
                                    </label>
                                    <input type="submit" name="" value="Guardar" class="btn btn-primary ripple m-1 float-right">
                                  </p>
                              </form>

                             </div>
                            @break;
                            @case('economic')
                              <h4 class="mb-3">Peticion económica</h4>
                              <div>
                                <p> <b>Tipo:</b> {{$indivu->typos}}</p>
                                <p> <b>Aplicar a:</b> {{$indivu->applyTo}}</p>
                                @if(strtolower($indivu->typos)=='otro')
                                  <p> <b>Razón:</b> {{$indivu->otherAplyText}}</p>
                                @endif
                                <p> <b>Comentario:</b> {{$indivu->comment}}</p>
                                <p> <b>Descripción:</b> {{$indivu->descript}}</p>
                                <form class="" action="{{route("admin.inboxpetitions.store",[$company,$inbox,$slectd,$indivu->id])}}" method="post">
                                  @csrf
                                  <p>
                                      {{$indivu->description}}
                                  </p>
                                    <div class="col-md-4 form-group mb-3 ml-0 pl-0">
                                      <label for="credit1">Cantidad autorizada</label>
                                      <input type="number" name="authorized"
                                      value="{{(!empty($indivu->authorized)?$indivu->authorized:'')}}" class="form-control">
                                    </div>
                                    <p>
                                      <div class="col-md-12 form-group mb-3 ml-0 pl-0">
                                        <label for="credit1">Respuesta</label>
                                        <textarea name="reponse" rows="8" cols="80" class="form-control"
                                        >{{($indivu->status>0?$indivu->reponse:'')}}</textarea>
                                      </div>
                                    </p>

                                    <p>
                                      <label class="switch pr-5 switch-primary mr-3 ">
                                        <span>{{(!empty($indivu->status)&&$indivu->status==1||$indivu->status==0?'Aprobado':'Denegado')}}</span>
                                        <input type="checkbox" name="status" value="1"
                                        {{($indivu->status==1?'checked="true"':'')}}
                                        /><span class="slider"></span>
                                      </label>
                                      <input type="submit" name="" value="Guardar" class="btn btn-primary ripple m-1 float-right">
                                    </p>
                              </form>
                             </div>
                            @break;
                            @case('simple')
                              <h4 class="mb-3">Peticion general</h4>
                              <div>
                                <p> <b>Tipo:</b> {{$indivu->getTypo()->first()->name}}</p>
                                <p> <b>Comentario:</b> {{$indivu->comment}}</p>
                                <form class="" action="{{route("admin.inboxpetitions.store",[$company,$inbox,$slectd,$indivu->id])}}" method="post">
                                  @csrf
                                    <p>
                                      <div class="col-md-12 form-group mb-3 ml-0 pl-0">
                                        <label for="credit1">Respuesta</label>
                                        <textarea name="reponse" rows="8" cols="80" class="form-control"
                                        >{{($indivu->status>0?$indivu->reponse:'')}}</textarea>
                                      </div>
                                    </p>

                                    <p>
                                      <label class="switch pr-5 switch-primary mr-3 ">
                                        <span>{{(!empty($indivu->status)&&$indivu->status==1||$indivu->status==0?'Aprobado':'Denegado')}}</span>
                                        <input type="checkbox" name="status" value="1"
                                        {{($indivu->status==1?'checked="true"':'')}}
                                        /><span class="slider"></span>
                                      </label>
                                      <input type="submit" name="" value="Guardar" class="btn btn-primary ripple m-1 float-right">
                                    </p>
                              </form>
                             </div>
                            @break;
                            @case('complaint')
                              <h4 class="mb-3">Quejas</h4>
                              <div>
                                <p> <b>Comentario:</b> {{$indivu->comment}}</p>
                                <form class="" action="{{route("admin.inboxpetitions.store",[$company,$inbox,$slectd,$indivu->id])}}" method="post">
                                  @csrf
                                    <p>
                                      <div class="col-md-12 form-group mb-3 ml-0 pl-0">
                                        <label for="credit1">Respuesta</label>
                                        <textarea name="reponse" rows="8" cols="80" class="form-control"
                                        >{{$indivu->reponse}}</textarea>
                                      </div>
                                    </p>

                                    <p>
                                      <label class="switch pr-5 switch-primary mr-3 ">
                                        <span>{{(!empty($indivu->status)&&$indivu->status==1||$indivu->status==0?'Aprobado':'Denegado')}}</span>
                                        <input type="checkbox" name="status" value="1"
                                        {{($indivu->status==1?'checked="true"':'')}}
                                        /><span class="slider"></span>
                                      </label>
                                      <input type="submit" name="" value="Guardar" class="btn btn-primary ripple m-1 float-right">
                                    </p>
                              </form>
                             </div>
                            @break;
                          @endswitch



                        {{-- <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                          <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps__rail-y" style="top: 0px; right: 0px;">
                          <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                        </div> --}}
                      </div>
                  @endif
                </div>
            </div>
            <!-- Secondary Inbox sidebar-->
            <div class="inbox-secondary-sidebar perfect-scrollbar rtl-ps-none ps sidebar ps--active-y" data-sidebar="secondary" style="left: 0px;"><i class="sidebar-close i-Close" data-sidebar-toggle="secondary">ddd</i>
              <h1 style="font-size: 22px;color: #867d93;padding: 10px;margin: 0;">
                @php
                  $titls = ["timeperms"=>'Permisos con reposisión',
                            "defhour"=>'Permisos sin reposisión',
                            "economic"=>'Peticiones económicas',
                            "simple"=>'Peticiones generales',
                            "complaint"=>'Quejas'
                          ];
                @endphp
                {{(!empty($titls[$slectd])?$titls[$slectd]:'')}}
              </h1>
              @if(!empty($slectd))
                @foreach($messages AS $messa)
                  @php  //$dtat = $messa->getColaborator()->first();
                    $date = explode('-',date('Y-m-d',strtotime($messa->created_at)));
                    switch($slectd){
                      case'timeperms':
                          {$desc=substr($messa->description,0,60);}
                      break;
                      case'defhour':
                          {$desc=substr($messa->description,0,60);}
                      break;
                      case'economic':
                          {$desc=substr($messa->comment,0,60);}
                      break;
                      case'simple':
                          {$desc=substr($messa->comment,0,60);}
                      break;
                      case'complaint':
                          {$desc=substr($messa->comment,0,60);}
                      break;
                    }
                  @endphp
                <div class="mail-item" onclick="showMsg('{{route('admin.inboxpetitions.index',[$company,$inbox,$slectd,$messa->id])}}');"
                  {{($messa->id==$idPetition?'style="background-color:#f8f9fa;':'')}}
                  >
                    <div class="avatar"><img src="{{ (!empty($messa->foto)?asset('storage/' . $messa->foto):'') }}" alt=""></div>
                    <div class="col-xs-6 details">
                      <span class="name text-muted">
                        {{$messa->nombres.' '.$messa->apellidos}}
                      </span>
                      <p class="m-0">
                        {{$desc}}
                      </p>
                    </div>
                    <div class="col-xs-3 date"><span class="text-muted">{{$date[2].' '.$monts[((INT)$date[1]-1)].' '.$date[0] }} </span></div>
                </div>
              @endforeach
              @else
                {{-- <h1>Revisar bandejas</h1> --}}
              @endif
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px; height: 634px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 620px;"></div></div></div>
        </div>
      </div>
      <!-- MAIN INBOX SIDEBAR-->

    <div class="inbox-main-sidebar sidebar" data-sidebar="main" data-sidebar-position="left" style="left: 0px;">

          <div class="pt-3 pr-3 pb-3"><i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
              {{-- <button class="btn btn-rounded btn-primary btn-block mb-4">Compose</button> --}}

              <div class="">

                  <p class="text-muted mb-2">Busqueda</p>
                  <ul class="inbox-main-nav pl-0">
                      <li>
                        {{-- {{route('admin.inboxpetitions.index',[$company,'newentrys'])}} --}}
                        <a class="" href="javascript:"><i class="icon-regular i-Mail-2"></i> Entradas nuevas ({{$data["tots"][0]}})</a>
                        <ul class="inbox-main-nav">
                            <li><a class="{{($slectd=='timeperms'&&$inbox=='newentrys'?'active':'')}}" href="{{((INT)$data["hourPerm"][0]>0?route('admin.inboxpetitions.index',[$company,'newentrys','timeperms']):"javascript:")}}">Permisos RT ({{$data["hourPerm"][0]}})</a></li>
                            <li><a class="{{($slectd=='defhour'&&$inbox=='newentrys'?'active':'')}}" href="{{((INT)$data["defHourPerm"][0]>0?route('admin.inboxpetitions.index',[$company,'newentrys','defhour']):"javascript:")}}">Permisos SRT ({{$data["defHourPerm"][0]}})</a></li>
                            <li><a class="{{($slectd=='economic'&&$inbox=='newentrys'?'active':'')}}" href="{{((INT)$data["ecoPetit"][0]>0?route('admin.inboxpetitions.index',[$company,'newentrys','economic']):"javascript:")}}">Economicas ({{$data["ecoPetit"][0]}})</a></li>
                            <li><a class="{{($slectd=='simple'&&$inbox=='newentrys'?'active':'')}}" href="{{((INT)$data["simplePetit"][0]>0?route('admin.inboxpetitions.index',[$company,'newentrys','simple']):"javascript:")}}">Generales ({{$data["simplePetit"][0]}})</a></li>
                            <li><a class="{{($slectd=='complaint'&&$inbox=='newentrys'?'active':'')}}" href="{{((INT)$data["complAint"][0]>0?route('admin.inboxpetitions.index',[$company,'newentrys','complaint']):"javascript:")}}">Quejas ({{$data["complAint"][0]}})</a></li>
                        </ul>
                      </li>
                      <li>
                        {{-- {{route('admin.inboxpetitions.index',[$company,'aproved'])}} --}}
                        <a class="" href="javascript:"><i class="icon-regular i-Check"></i> Aprobadas ({{$data["tots"][1]}})</a>
                        <ul class="inbox-main-nav">
                            <li><a class="{{($slectd=='timeperms'&&$inbox=='aproved'?'active':'')}}" href="{{((INT)$data["hourPerm"][1]>0?route('admin.inboxpetitions.index',[$company,'aproved','timeperms']):"javascript:")}}">Permisos RT ({{$data["hourPerm"][1]}})</a></li>
                            <li><a class="{{($slectd=='defhour'&&$inbox=='aproved'?'active':'')}}" href="{{((INT)$data["defHourPerm"][1]>0?route('admin.inboxpetitions.index',[$company,'aproved','defhour']):"javascript:")}}">Permisos SRT ({{$data["defHourPerm"][1]}})</a></li>
                            <li><a class="{{($slectd=='economic'&&$inbox=='aproved'?'active':'')}}" href="{{((INT)$data["ecoPetit"][1]>0?route('admin.inboxpetitions.index',[$company,'aproved','economic']):"javascript:")}}">Economicas ({{$data["ecoPetit"][1]}})</a></li>
                            <li><a class="{{($slectd=='simple'&&$inbox=='aproved'?'active':'')}}" href="{{((INT)$data["simplePetit"][1]>0?route('admin.inboxpetitions.index',[$company,'aproved','simple']):"javascript:")}}">Generales ({{$data["simplePetit"][1]}})</a></li>
                            <li><a class="{{($slectd=='complaint'&&$inbox=='aproved'?'active':'')}}" href="{{((INT)$data["complAint"][1]>0?route('admin.inboxpetitions.index',[$company,'aproved','complaint']):"javascript:")}}">Quejas ({{$data["complAint"][1]}})</a></li>
                        </ul>
                      </li>
                      <li>
                        {{-- {{route('admin.inboxpetitions.index',[$company,'denied'])}} --}}
                        <a class="" href="javascript:"><i class="icon-regular i-Close"></i> Denegadas ({{$data["tots"][2]}})</a>
                        <ul class="inbox-main-nav">
                            <li><a class="{{($slectd=='timeperms'&&$inbox=='denied'?'active':'')}}" href="{{((INT)$data["hourPerm"][2]>0?route('admin.inboxpetitions.index',[$company,'denied','timeperms']):"javascript:")}}">Permisos RT ({{$data["hourPerm"][2]}})</a></li>
                            <li><a class="{{($slectd=='defhour'&&$inbox=='denied'?'active':'')}}" href="{{((INT)$data["defHourPerm"][2]>0?route('admin.inboxpetitions.index',[$company,'denied','defhour']):"javascript:")}}">Permisos SRT ({{$data["defHourPerm"][2]}})</a></li>
                            <li><a class="{{($slectd=='economic'&&$inbox=='denied'?'active':'')}}" href="{{((INT)$data["ecoPetit"][2]>0?route('admin.inboxpetitions.index',[$company,'denied','economic']):"javascript:")}}">Economicas ({{$data["ecoPetit"][2]}})</a></li>
                            <li><a class="{{($slectd=='simple'&&$inbox=='denied'?'active':'')}}" href="{{((INT)$data["simplePetit"][2]>0?route('admin.inboxpetitions.index',[$company,'denied','simple']):"javascript:")}}">Generales ({{$data["simplePetit"][2]}})</a></li>
                            <li><a class="{{($slectd=='complaint'&&$inbox=='denied'?'active':'')}}" href="{{((INT)$data["complAint"][2]>0?route('admin.inboxpetitions.index',[$company,'denied','complaint']):"javascript:")}}">Quejas ({{$data["complAint"][2]}})</a></li>
                        </ul>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>

                <!-- end of main-content -->
</div>
    <script type="text/javascript">
    function showMsg(urlt){
      window.location=urlt;
    }
    function selectMe(estu){
      let wind = '{{route('admin.inboxpetitions.index',['idcomp'])}}';
        wind = wind.replace('idcomp',estu.value);
        window.location = wind;

    }
    </script>
    <style media="screen">
      #buzon ul.chosen-results{
        padding:0px;
      }
      #buzon a,#buzon b,#buzon label{
        color:#663399!important;
      }
      #buzon .text-muted{
        color:#70657b !important
      }
      #buzon label{
        font-weight: bold;
      }
      #buzon ul{
        padding-left: 25px;

      }
      #buzon .inbox-main-nav .inbox-main-nav li a{
        padding:5px 0px ;
        line-height:15px;
      }
      #buzon .active{
        color:#3b0079!important;
        font-weight: bold;
      }
    </style>
@endsection
