@extends('admin.layouts.master')
@section('main-content')
<script type="text/javascript">
var kad={0:{type:"hddn",nm:"_token",vl:'{{ csrf_token() }}'},
         1:{type:"txt",tl:"Título",nm:"name",elv:"0"}
        };
    valdis={clase:"red",text:1};
</script>
   <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
					<h3 class="card-title">
            <a href="javascript:"class="" onclick="newfloatv2(kad);">
            <i class="ion-ios7-plus-outline "></i>
            &nbsp;&nbsp;Nuevo tipo de petición
          </a>
          </h3>
					<div class="card-options">
						<a href="#" class="card-options-collapse" data-toggle="card-collapse">
              <i class="fe fe-chevron-up"></i>
            </a>
						<a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen">
              <i class="fe fe-maximize"></i>
            </a>
						<a href="#" class="card-options-remove" data-toggle="card-remove">
              <i class="fe fe-x"></i>
            </a>
					</div>
				</div>
        <div class="card-body">
          <div class="table-responsive">
            <table i class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Peticiones</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @php $criti=array(); @endphp
                @foreach ($data as $dataRes)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td > {{$dataRes->name}}</td>
                    <td class="td-actions text-left table-subtitle">
                        <a  href="javascript:"
                            onclick="modifyfloat('{{$dataRes->id}}',kad,criteria);">
                            <i class="text-20"data-feather="edit-3"></i>
                        </a>
                        <a  href="javascript:"
                            onclick="deleteD('{{$dataRes->id}}','{{ csrf_token() }}');">
                            <i class="text-12" data-feather="trash"></i>
                        </a>
                    </td>
                  </tr>
                    @php
                    $criti[$dataRes->id]=[$dataRes->name];
                    @endphp
                  @endforeach
						    </tbody>
               </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <script type="text/javascript">
      var criteria = @json($criti);
    </script>

@endsection
