ALTER TABLE admin_products MODIFY COLUMN images text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN sizes text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN colors text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN description text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN category varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN subcategory varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN lot varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN code varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE admin_products MODIFY COLUMN dateIn date NULL;
