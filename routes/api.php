<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\AuthenticationController;
use App\Http\Controllers\api\loggsController;
use App\Http\Controllers\api\permissionsController;
use App\Http\Controllers\api\listPetitionsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/create-account', [AuthenticationController::class, 'createAccount']);

Route::post('/signin', [AuthenticationController::class, 'signin']);
//using middleware
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function(Request $request) {
        return auth()->user();
    });
    Route::post('/sign-out', [AuthenticationController::class, 'logout']);
    Route::post('/loggs',[loggsController::class, 'store']);
    Route::get('/shedules',[loggsController::class, 'shedules']);
    Route::post('/storeWork',[loggsController::class, 'storeWorkday']);
    // Route::get('/shedules',[loggsController::class, 'shedules']);
    Route::get('/getPetitions',[loggsController::class, 'getPetitions']);
    Route::post('/timepermitions',[permissionsController::class, 'setPetitionTime']);
    Route::post('/defaulttimeperm',[permissionsController::class, 'setPetitionTimeDefault']);
    Route::post('/economicpetition',[permissionsController::class, 'economicpetition']);
    Route::post('/simplepetitions',[permissionsController::class, 'simplepetitions']);
    Route::post('/complaintments',[permissionsController::class, 'complaintments']);
    Route::get('/getListPetitions',[listPetitionsController::class, 'listPetitions']);


    Route::any('/listado_mensajes', 'api\mensajes_userController@index');
    Route::any('/ver_mensaje', 'api\mensajes_userController@show');
    Route::any('/crear_mensaje', 'api\mensajes_userController@create');
    Route::any('/enviar_mensaje', 'api\mensajes_userController@store');
    Route::any('/responder_mensaje', 'api\mensajes_userController@update');
    Route::any('/alerta_mensajes', 'api\mensajes_userController@alerta');


});

// Route::any('/birthday',[cumplesapiController::class, 'birthday']);

Route::any('/birthday', 'api\cumplesapiController@birthday');

Route::any('/pago', 'api\pagos_apiController@total_pago');
Route::any('/guardar_pago', 'api\pagos_apiController@guardar_pago');
