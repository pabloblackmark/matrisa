<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
Route::prefix('/')->name('admin.')->namespace('Admin')->group(function(){
  Route::namespace('Auth')->group(function(){
    Route::get('/login', 'LoginController@showLoginView')->name('login');
    Route::post('/login', 'LoginController@Login')->name('login.submit');
    Route::get('/logout', 'LoginController@logout')->name('logout');
  });
  Route::get('/home', 'adminController@index');
  Route::get('/', 'adminController@index')->name('home');
  Route::resource('/permissions', 'permissionsController');
  Route::resource('/users', 'usersController');
  Route::resource('/roles', 'rolesController');

  Route::resource('/puestos', 'puestosController');
  Route::put('/puestos/update/{id}', 'puestosController@update');
  Route::delete('/puestos/destroy/{id}', 'puestosController@destroy');

  Route::resource('/categories', 'categoriesController');
  Route::post('/storeSubCategory/{id}', 'categoriesController@storeSubCategory');
  Route::put('/updateSubCategory/{id}', 'categoriesController@updateSubCategory');
  Route::delete('/categories/deleteSubCategory/{id}', 'categoriesController@deleteSubCategory');

  Route::resource('/colors', 'colorsController');
  Route::resource('/sizes', 'sizesController');
  Route::resource('/warehouse', 'warehouseController');
  Route::get('/wareproducts', 'warehouseController@products')->name("filterproducts");
  Route::resource('/products', 'productsController');
  Route::delete('/products/deleteFiles/{id}/{idPr}', 'productsController@delFile')->name("deleteFiles");
  Route::resource('/prodaddtoware', 'productsWarehouseController');

  Route::resource('/providers', 'providersController');
  Route::resource('/solicitudes', 'solicitudesController');

  Route::resource('/colaboradores', 'colaboradoresController');
  Route::get('/editar_colaboradores/{id?}', 'colaboradoresController@show')->name('editar_colaboradores');
  Route::post('/asignar_colaboradores', 'colaboradoresController@multipleAssign')->name('asignar_colaboradores');

  Route::get('/colaboradores/company/{id}', 'colaboradoresCompanyController@index')->name('colaboradoresCompany');
  Route::post('/colaboradores/company', 'colaboradoresCompanyController@assign')->name('colaboradoresCompanyAssign');
  Route::get('/colaboradores/company/{cid}/{id}', 'colaboradoresCompanyController@unassign')->name('colaboradoresCompanyUnAssign');

  Route::post('/modifyWork/{id}/{idComp}/{nWk?}/{nYr?}', 'colaboradoresCompanyController@updateWorkDone')->name('workModify');

  Route::get('/colaboradores/client/{com_id}/{cli_id}/{assoc_id}', 'colaboradoresClientController@index')->name('colaboradoresClient');
  Route::post('/colaboradores/client/{com_id}/{cli_id}/{assoc_id}', 'colaboradoresClientController@assign')->name('colaboradoresClientAssign');
  Route::get('/colaboradores/client//{com_id}/{cli_id}/{id}', 'colaboradoresClientController@unassign')->name('colaboradoresClientUnAssign');

  Route::resource('/listpetitions', 'petitionslistsController');

  Route::resource('/documentos', 'documentosController');
  Route::get('/documentos_download', 'documentosController@download')->name('documentos_download');

  Route::resource('/company', 'companies\companyController');
  Route::resource('/clients', 'companies\clientsController');
  Route::resource('/clientscompany', 'companies\companyclientsController');
  Route::resource('/clientsjobs', 'companies\clientsJobsController');
  Route::resource('/clientsjobstask', 'companies\clientsJobsTasksController');

  Route::resource('/clientsbranch', 'companies\clientsBranchOfficeController');
  Route::delete('/clientsbranch/{idCli}/{id}', 'companies\clientsBranchOfficeController@destroy');

  Route::resource('/clientsbrancharea', 'companies\clientsBranchAreaController');
  Route::delete('/clientsbrancharea/{idc}/{id}', 'companies\clientsBranchAreaController@destroy');

  Route::resource('/workdone', 'companies\workinghoursController');

  Route::get('/colaboradores/companyReports/{id}/{idComp}/{nWk?}/{nYr?}', 'companies\workinghoursController@workReport')->name('colabCompanyWork');
  Route::post('/colaboradores/companyReports/{id}/{idComp}/{nWk?}/{nYr?}', 'companies\workinghoursController@workReportSave')->name('colabCompanyWorkSave');
  Route::post('/colaboradores/saveVacations/{id}/{idComp}/{nWk?}/{nYr?}', 'companies\workinghoursController@vacationSave')->name('saveVacations');
  Route::post('/colaboradores/savePermission/{id}/{idComp}/{nWk?}/{nYr?}', 'companies\workinghoursController@permissionsSave')->name('savePermission');
  // Route::resource('/shedulesreports', 'companies\shedulesCompanyController');



  Route::resource('/bancos', 'BancosController');
  Route::resource('/contratos', 'ContratosController');

  Route::resource('/archivos', 'archivosController');
  Route::post('/archivos/{id}', 'archivosController@store');
  Route::put('/archivos/{id_file_manager}/{id_file}', 'archivosController@update');
  Route::delete('/archivos/{id_file_manager}/{id_file}', 'archivosController@destroy');

  Route::resource('/tasks', 'tasksController');
  Route::post('/tasks/{id}', 'tasksController@store');
  Route::put('/tasks/{id_task_manager}/{id_task}', 'tasksController@update');
  Route::delete('/tasks/{id_task_manager}/{id_task}', 'tasksController@destroy');

  Route::resource('/horary', 'horaryController');
  Route::post('/horary/{id}', 'horaryController@store');
  Route::put('/horary/{id_task_manager}/{id_task}', 'horaryController@update');
  Route::delete('/horary/{id_task_manager}/{id_task}', 'horaryController@destroy');

  Route::resource('/changeLog', 'changeLogController');
  Route::get('/changeLog/{tipo}/{element_id}', 'changeLogController@show');

  Route::resource('/bodegas', 'bodegasController');
  Route::get('/bodegasCliente/{company_id}', 'bodegasController@showBodegasClientes');
  Route::resource('/bodegasEquipo', 'bodegasEquipoController');

  Route::resource('/egresos', 'egresosController');
  Route::resource('/ingresos', 'ingresosController');

  Route::get('/egresosClientes/{company_id}/{bodega_id}', 'egresosClientesController@index');
  Route::get('/ingresosClientes/{company_id}/{bodega_id}', 'ingresosClientesController@index');

  Route::resource('/egresosEquipo', 'egresosEquipoController');
  Route::resource('/ingresosEquipo', 'ingresosEquipoController');

  Route::resource('/facturas', 'facturasController');
  Route::post('/facturas/{bodega_id}', 'facturasController@store');
  Route::get('/facturasEquipo/{bodega_id}', 'facturasController@showFacturasEquipo');
  Route::post('/facturasEquipo/{bodega_id}', 'facturasController@store');

  Route::get('/productosFactura/{bodega_id}/{factura_id}', 'facturasController@showProductos');
  Route::post('/productosFactura/{bodega_id}/{factura_id}', 'facturasController@storeProductos');
  Route::put('/productosFactura/{bodega_id}/{factura_id}/{ingreso_id}', 'facturasController@updateProductos');
  Route::delete('/productosFactura/{bodega_id}/{factura_id}/destroy/{ingreso_id}', 'facturasController@destroy');

  Route::get('/equiposFactura/{bodega_id}/{factura_id}', 'facturasController@showEquipos');
  Route::post('/equiposFactura/{bodega_id}/{factura_id}', 'facturasController@storeEquipos');
  Route::put('/equiposFactura/{bodega_id}/{factura_id}/{ingreso_id}', 'facturasController@updateEquipos');

  Route::resource('/productos', 'productosController');
  Route::resource('/equipos', 'equiposController');

  Route::resource('/stockEmpresas', 'stockEmpresasController');
  Route::get('/stockEmpresas/{company_id}/{bodega_id}', 'stockEmpresasController@show');
  Route::put('/stockEmpresas/{company_id}/{bodega_id}/{producto_id}', 'stockEmpresasController@egreso');

  Route::resource('/stockEmpresasEquipo', 'stockEmpresasEquipoController');
  Route::put('/stockEmpresasEquipo/{bodega_id}/{equipo_id}', 'stockEmpresasEquipoController@egreso');
  Route::put('/stockEmpresasEquipoIngreso/{bodega_id}/{equipo_id}', 'stockEmpresasEquipoController@ingreso');
  Route::put('/stockEmpresasEquipoBaja/{bodega_id}/{equipo_id}', 'stockEmpresasEquipoController@baja');

  Route::resource('/stockClientes', 'stockClientesController');
  Route::get('/stockClientes/{company_id}/{bodega_id}', 'stockClientesController@show');
  Route::put('/stockClientes/{company_id}/{bodega_id}/{producto_id}', 'stockClientesController@egreso');

  Route::resource('/unidades', 'unidadesController');
  Route::resource('/conversiones', 'conversionesController');
  Route::get('/inboxpetitions/{comany?}/{inbox?}/{type?}/{id?}', 'petitionsinboxController@index')->name("inboxpetitions.index");
  Route::post('/inboxpetitions/{comany?}/{inbox?}/{type?}/{id?}', 'petitionsinboxController@store')->name("inboxpetitions.store");


  Route::get('/cumples', 'cumplesController@index')->name('cumples.index');
  Route::post('/cumples_update', 'cumplesController@update');
  Route::resource('/reportsColabs', 'stockEmpresasController');

  Route::any('/planillas', 'accounting\payrollController@index')->name("planillas.index");
  Route::get('/planillas/{id?}/{month?}', 'accounting\payrollController@show')->name("planillas.show");

  // Route::get('/planillascolabs/{id}/{idComp}/{month?}/{nYr?}', 'accounting\payrollController@payrollinfo')->name("planillascolabs");
  Route::get('/planillascolabs', 'accounting\payrollController@payrollinfo')->name("planillascolabs");

  Route::post('/planillasaprovar', 'accounting\payrollController@aprovationPayroll')->name("aproveplanilla");

  Route::post('/aprobar_planilla', 'accounting\payrollController@aprobar_planilla')->name("aprobar_planilla");
  Route::post('/update_planilla', 'accounting\payrollController@update_planilla')->name("update_planilla");


  // horas extras
  Route::any('/horas', 'accounting\horasController@index')->name("horas.index");
  Route::get('/horas_colab', 'accounting\horasController@horas_colab')->name("horas_colab");
  Route::post('/aprobar_colab_horas', 'accounting\horasController@aprobar_colab')->name("aprobar_colab");
  Route::post('/aprobar_horas', 'accounting\horasController@aprobar_horas')->name("aprobar_horas");
  Route::post('/update_horas', 'accounting\horasController@update_horas')->name("update_horas");


  // viaticos
  Route::any('/viaticos', 'accounting\viaticosController@index')->name("viaticos.index");
  Route::get('/viaticos_colab', 'accounting\viaticosController@viaticos_colab')->name("viaticos_colab");
  Route::post('/aprobar_colab_viaticos', 'accounting\viaticosController@aprobar_colab')->name("aprobar_colab");
  Route::post('/aprobar_viaticos', 'accounting\viaticosController@aprobar_viaticos')->name("aprobar_viaticos");
  Route::post('/update_viaticos', 'accounting\viaticosController@update_viaticos')->name("update_viaticos");

  // pagos
  Route::any('/pagos', 'accounting\pagosController@index')->name("pagos.index");
  Route::get('/pagos_colab', 'accounting\pagosController@pagos_colab')->name("pagos_colab");
  Route::get('/pagos/export_pdf', 'accounting\pagosController@export_pdf');

  // descuentos
  Route::resource('/descuentos', 'accounting\descuentosController');
  Route::post('/update_descuento', 'accounting\descuentosController@update_descuento')->name("update_descuento");

  // prestaciones
  Route::any('/prestaciones', 'accounting\prestacionesController@index')->name("prestaciones.index");
  Route::get('/prestaciones_colab', 'accounting\prestacionesController@prestaciones_colab')->name("prestaciones_colab");
  Route::post('/aprobar_colab_prestaciones', 'accounting\prestacionesController@aprobar_colab')->name("aprobar_colab");
  Route::post('/aprobar_prestaciones', 'accounting\prestacionesController@aprobar_prestaciones')->name("aprobar_prestaciones");
  Route::post('/update_prestaciones', 'accounting\prestacionesController@update_prestaciones')->name("update_prestaciones");



  // liquidacion
  Route::any('/liquidacion', 'accounting\liquidacionController@index')->name("liquidacion.index");
  Route::get('/liquidacion_colab', 'accounting\liquidacionController@liquidacion_colab')->name("liquidacion_colab");
  Route::post('/aprobar_colab_liquidacion', 'accounting\liquidacionController@aprobar_colab')->name("aprobar_colab");
  Route::post('/aprobar_liquidacion', 'accounting\liquidacionController@aprobar_liquidacion')->name("aprobar_liquidacion");
  Route::post('/update_liquidacion', 'accounting\liquidacionController@update_liquidacion')->name("update_liquidacion");



  // reporte clientes
  Route::any('/reporte_clientes', 'reportes\clientesController@index')->name("reporte_cliente.index");
  Route::get('/reporte_colab', 'reportes\clientesController@reporte_colab')->name("reporte_colab");
  Route::get('/reporte_clientes/export_pdf', 'reportes\clientesController@export_pdf');





  Route::resource('/mensajes', 'mensajesController');
  Route::delete('/mensajes/destroy/{id}', 'mensajesController@destroy');
  Route::delete('/mensajes/{id}/destroy/{idx}', 'mensajesController@destroy');

  Route::resource('/mensajes_user', 'mensajes_userController');


  Route::resource('/mensajes_grupos', 'mensajes_gruposController');
  Route::delete('/mensajes_grupos/{id}/destroy/{idx}', 'mensajes_gruposController@destroy');

  Route::resource('/codigos', 'codigosController');

  Route::resource('/cotis', 'cotisController');
  Route::get('/cotis/export_pdf/{id}', 'cotisController@export_pdf');

  Route::get('/cotis/export_pdf2/{id}', 'cotisController@export_pdf2');
  Route::get('/cotis/ver_pdf/{id}', 'cotisController@ver_pdf');


  Route::get('/ver_email', 'codigosController@ver_email');

  Route::get('/crear_codigo/{id_factura}', 'codigosController@crear_codigo');

  // Route::get('/prestaciones_demo', 'prestacionesController@index')->name('prestaciones.index');
  // Route::get('/liquidacion_demo', 'prestacionesController@liquidacion')->name('liquidacion.index');

  Route::post('get_clientes', 'companies\companyclientsController@get_clientes');
  Route::post('get_sucursales', 'companies\clientsBranchOfficeController@get_sucursales');
  Route::post('get_areas', 'companies\clientsBranchAreaController@get_areas');

  Route::post('get_colaboradores', 'colaboradoresCompanyController@get_colaboradores');

  Route::post('/upcsv', 'upcsvController@store')->name("upcsv.store");
  Route::resource('/upcsv', 'upcsvController');

});


View::composer('*', function($view){
    $view->alertas = Controller::list_alertas();
});
