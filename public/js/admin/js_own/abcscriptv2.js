// var patron2 = new Array(4,4);
var cntt,to,vri='',radiochk=0,pase='',auxi='',valdis;
var cad='',titlemodal='';
var objls;
var target=getParameterByName('target',window.location.href);
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


  function loadingmig(ti,tx){

    let pathi = "/images/cristales_p_f.gif",
        tit=(ti==undefined?"Procesando":ti),
        txt=(tx==undefined?"Un momento por favor":tx);
        swal.fire({
        title: tit,
        text: txt,
        imageUrl: pathi,
        showConfirmButton: false,
        allowOutsideClick: false
      });
        //return true;
    }
function mascara(d,sep,pat,nums,min,max)
{
    if(d.valant != d.value){
	val = d.value;
	largo = val.length;
	val = val.split(sep);
	val2 = '';
	for(r=0;r<val.length;r++){
	    val2 += val[r];
	}
	if(nums){
	    for(z=0;z<val2.length;z++){
		if(isNaN(val2.charAt(z))){
		    letra = new RegExp(val2.charAt(z),"g");
		    val2 = val2.replace(letra,"");
		}
	    }
	}
	val = '';
	val3 = new Array();
	for(s=0; s<pat.length; s++){
	    val3[s] = val2.substring(0,pat[s]);
	    val2 = val2.substr(pat[s]);
	}
	for(q=0;q<val3.length; q++){
	    if(q ==0){
            val = val3[q];
	    }
	    else{
            if(val3[q] != ""){
                val += sep + val3[q];
            }
	    }
	}

	if (max!=undefined&&min!=undefined&&val>=min&&val<=max||
        max==undefined&&min==undefined) {
        //console.log(max);
	    d.value  = val;
	}else{
	    //alert("Cantidad incorrecta");
	    d.value  = "";
	}
	d.valant = val;
    }
}


function maxmin(d,min,max)
{
    if(d.value>=min&&d.value<=max){
    }else{
        d.value="";
        d.focus();
    }
}



function newfloatv2(cadena,editobjet,url,nobots,litUrl,changeLog)
{
    var ckd=func=wte='';
    let $typeForm='POST',edit;
    var APP_URL = window.location.pathname.split( '/' );
    // console.log(APP_URL,window.location);
    $(".fndwindow").show();
    $(".windowed").show();
    edit=(editobjet!=undefined?true:false);
    if (typeof litUrl === 'string'&&nobots=='literalUrl'){
        turl =litUrl;
    }
    // else
    // if(APP_URL.length>4&&edit==true&&nobots!='literal'){
    //   turl=window.location.origin+'/'+APP_URL[1]+'/'+APP_URL[2]+'/'+APP_URL[3]
    //   +'/'+(nobots!=undefined?nobots+'/':'')+url;
    // }
    else if (url==undefined&&edit==false){
        turl='';
    }
    else if (typeof url === 'string'&&edit==true){
      turl=window.location.href+'/'+url;
    }
    else if (typeof url === 'string'&&edit==true){
      turl =window.location.href+'?cod='+url;
    }
    else if (typeof url === 'string'){
        turl =url;
    }
    // console.log(turl,nobots,litUrl);
    if (cadena!=undefined&&(typeof cadena)!='object'&&(typeof cadena)=='string'){
            ckd=cadena;
    }
    else if((typeof cadena)=='object')
    {
        elems=(cadena.form!=undefined?cadena.form:cadena);
        $typeForm=(editobjet!=undefined&&editobjet!=null?'PUT':'POST');
        func=(cadena.ff!=undefined?cadena.ff:"return validar(valdis);");
        ckd=formgenerator(elems,editobjet);
        // console.log(editobjet,(typeof cadena),turl,$typeForm);
    }
    swal.fire({title: '',
               html:`
                        <form action="${turl}" id="formflotant" method="POST" role="form" enctype="multipart/form-data">
                            <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                                <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                                    <div class="modal-body">
                                        <div class="row">
                                            <input type="hidden" name="_method" value="${$typeForm}">
                                            ${ckd}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    `,
              title:`<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                        <h5 class="modal-title" id="verifyModalContent_title">${(editobjet!=undefined?'Editar':'Agregar')}</h5>
                    </div>`,
              // showCloseButton: true,
              showCancelButton: true,
              showConfirmButton: true,
              customClass:{
                  title: 'sweet-alert-custom-header',
                  content: 'sweet-alert-custom-content',
                  confirmButton: 'btn btn-primary mr-5',
                  cancelButton: 'btn btn-secondary',
              },
              confirmButtonText: 'Guardar',
              cancelButtonText: 'Cancelar',
              buttonsStyling: false,
              width: 600,
              preConfirm: function(){
                let vals = validar({text:1,select:1,textarea:1,number:1});
                // console.log(vals);
                if(!vals){
                  Swal.showValidationMessage('<i class="fa fa-info-circle"></i> Complete todos los campos por favor');
                }else{
                  if (changeLog){
                      return {
                          form: $('#formflotant'),
                          changeLog: true
                      }
                  }else{
                      $('#formflotant').submit();
                      swal.fire({
                         title: "Procesando",
                         text: "Un momento por favor",
                         imageUrl: "../images/cristales_p_f.gif",
                         showConfirmButton: false,
                         allowOutsideClick: false
                       });
                   }
                }
            }
        })
        .then((result) => {
            if (result.value && result.value.changeLog){
                Swal.fire({
                    title: `<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                              <h5 class="modal-title" id="verifyModalContent_title">Comentario de edición</h5>
                          </div>`,
                    html:`
                         <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                             <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                                 <div class="modal-body">
                                     <div class="row">
                                         <label class="col-form-label">Comentario</label>
                                         <textarea id="changeLogComment" class="form-control"></textarea>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         `,
                     preConfirm: function(){
                         let val = $('#changeLogComment').val(),
                             cad = `<textarea id="changeLogComment" name="comment" class="form-control">${val}</textarea>`,
                             form = result.value.form;

                         if(val == ""){
                           Swal.showValidationMessage('<i class="fa fa-info-circle"></i> Por favor coloque un comentario de edición.');
                         }else{
                             form.append(cad)
                             return {
                                 form: form
                             }
                         }
                   }
                }).then((result) => {
                    let form = result.value.form
                    $(document.body).append(form)
                    form.submit();
                })
            }
          });
          // <button class="btn btn-raised btn-raised-info mr-5" type="submit" onclick="return valar({text:1})"style="margin-left: 36%;" >Guardasr</button>\
          // <button class="btn btn-raised btn-raised-danger">Cancelar</button>
      $('.titlewindow').html((editobjet==undefined?'Agregar ':'Editar '));
}
// +
// (nobots!=0?'<div class="col-lg-12 col-md-12 col-sm-12"><input type ="submit" class="btn btn-teal" onclick="'+func+'" value="Guardar"/>\
//  <input type ="button" value="Cancelar" class="btn btn-pinterest" onclick="swal.close(); $(\'body\').css(\'overflow\',\'auto\');"/></div>':'')

function formgenerator(elems,$objed)
{
    var cad='',trfinal='',cont=0;
    for (var ii in elems)
    {
        var input=elems[ii];
        var nxtet=elems[parseInt(ii)+1];
        var dis=0;
        var stub = false;
        addx=(input.add!=undefined&&input.add!='')?input.add:'';
        ids=(input.id!=undefined&&input.id!='')?'id="'+input.id+'"':'';
        //console.log(nxtet.nxt);
        classe=(input.clss!=undefined&&input.clss!='')?input.clss:'';
        namess=(input.nm==undefined?'':'name="'+input.nm+'"');
        // console.log($objed);
        switch (input.type)
        {
            case 'lbl':{
                cad='<label class="col-form-label">'+input.tl+'</label>';
            }break;
            case 'label':{
                cad='<label class="col-form-label"></label>';
            }break;
            case 'stub':{
                cad="";
                stub=true;
            }break;
            case 'txt':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="text" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
            case 'clr':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="color" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
            case 'nbr':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="number" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
            case 'formato_moneda':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<div style="width:100%; display:flex; padding-top:6px;"><span style="margin-top:5px;">Q. </span><input class="form-control '+classe+'" type="number" '+namess+' '+ids+' '+$vald+' '+addx+'/></div>';
            }break;
            case 'hddn':{
                $vald='value="'+($objed!=undefined&&$objed[input.elv]!=undefined?$objed[input.elv]:input.vl)+'"';
                cad='<input type="hidden" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
            case 'txtarea':{
                $vald=($objed!=undefined&&input.elv!=undefined)?($objed[input.elv]!=undefined?$objed[input.elv]:''):($objed==undefined&&input.vl!=undefined?input.vl:'');
                cad='<textarea class="form-control '+classe+'" '+namess+' '+ids+' '+addx+' >'+$vald+'</textarea>';
            }break;
            case 'pss':{
                cad='<input class="form-control '+classe+'" type="password" '+namess+' '+ids+' '+addx+'/>';
            }break;
            case 'dtmpckr':{
              $vald='value="'+($objed!=undefined&&$objed[input.elv]!=undefined?$objed[input.elv]:input.vl)+'"';
                cad='<div class="input-group date">\
                      <div class="input-group-prepend">\
                        <div class="input-group-text br-tl-3 br-bl-3">\
                          <i class="fa fa-calendar"></i>\
                        </div>\
                      </div>\
                      <input type="date" class="form-control pull-right  '+classe+'" '+$vald+' '+namess+' '+ids+' '+addx+'>\
                    </div>';
            }break;
            case 'fl':{
              // <div class="input-group mb-3">
              //     <div class="custom-file">
              //         <input class="custom-file-input" id="inputGroupFile02" type="file" />
              //         <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Elegir archivo</label>
              //     </div>
              //     <div class="input-group-append"><span class="input-group-text" id="inputGroupFileAddon02">Upload</span></div>
              // </div>
                //alert((typeof input.chkfl));
                verifile=(input.chkfl!=undefined)?'onchange="valfile('+input.chkfl+',this);"':'';
                //alert(verifile);
                cad='<div class="input-group">\
                        <input class="custom-file-input" id="inputGroupFile02" type="file" '+namess+'  '+ids+verifile+' '+addx+'/>\
                        <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Elegir archivo</label>\
                </div>';
            }break;
            case 'btn':{
                cad='<button class="'+classe+'" type="button" '+ids+' '+addx+'>'+input.nm+'</button>';
            }break;
            case 'rd':{
                cad='<div class="custom-controls-stacked">';
                for (i=0;i<input.vl.length;i++) {
                    $vald=($objed!=undefined&&input.elv!=undefined&&$objed[input.elv]==input.vl[i][1])?'checked':'';
                    cad+='<label class="custom-control custom-radio">\
                    <input type="radio" '+namess+' id="'+ids[i]+'"\
                    value="'+input.vl[i][1]+'" '+$vald+" "+addx+'\
                    class="custom-control-input"/>\
                    <span class="custom-control-label">'+
                    input.vl[i][0]+(input.$brl!=undefined?'':
                    '</span></label>');
                }
                cad+='</div>';
            }break;
            case 'chkbxstyl':{
              $vald=($objed!=undefined&&input.elv!=undefined?
                $objed[input.elv]:input.vl[0][0]);
              check=($objed!=undefined&&input.vl!=undefined&&
                input.vl[0][0]==$objed[input.elv]?'checked="true"':'');
              cad='<label class=""><input type="checkbox" '+ids+' '+namess+
                    'value="'+$vald+'" '+check+' '+addx+'>'+input.vl[0][1]+
                    '</label>';
            }break;
            case 'chckbx':{
                cad='';
                //console.log(input.vl.length);
                for (i=0;i<input.vl.length;i++) {
                    if($objed!=undefined&&input.elv!=undefined){
                        for (jj=0;jj<($objed[input.elv].length);jj++) {
                            if($objed[input.elv][jj]==input.vl[i][0]){
                                $vald='checked="checked"';
                                break;
                            }else{
                                $vald='';
                            }
                        }
                    }
                    $nomb=(input.nm!=undefined &&(typeof input.nm=='array' || typeof input.nm=='object'))?input.nm[i]:input.nm;
                    $id=(ids!=undefined)?ids:'';
                    cad+='<label class="custom-checkbox custom-control">\
                              <input type="checkbox" name="'+$nomb+'" '+$id+'\
                                  class="custom-control-input"\
                                  value="'+input.vl[i][0]+'" '+addx+' '+$vald+'/>\
                            <span class="custom-control-label" style="float:left;">\
                              '+input.vl[i][1]+'\
                            </span>\
                         </label>';
                }
            }break;
            case 'slct':{
                cad='<select '+namess+' '+ids+' '+addx+' class="form-control">';
                if (input.vl!==undefined) {
                    for (i=0;i<input.vl.length;i++)
                    {
                        //for (jj=0;jj<($objed[input.elv].length);jj++) {
                        $vald=($objed!=undefined&&input.elv!=undefined&&$objed[input.elv]==input.vl[i][0])?'selected':'';
                        //}
                        cad+=(input.vl[i][0]=='optgrupolb')?'<optgroup label="'+input.vl[i][1]+'">':
                        '<option value="'+input.vl[i][0]+'" '+$vald+'>'+input.vl[i][1]+'</option>';
                    }
                }
                cad+='</select>';
            }break;
            case 'img':{
                $vald=($objed!=undefined&&input.elv!=undefined)?$objed[input.elv]:'';
                cad='<br><img src="'+$vald+'" '+ids+' '+addx+' class="d-block w-30 rounded rounded '+classe+'"/>';
            }break;
            case 'lnk':{
                $vald=($objed!=undefined&&input.elv!=undefined)?$objed[input.elv]:'';
                $filne=($objed!=undefined&&input.fln!=undefined)?$objed[input.fln]:
                    (input.altname!=undefined)?input.altname:'';
                cad='<a href="'+$vald+'" '+ids+' '+addx+' class="'+classe+'">'+$filne+'</a>';
            }break;
            case 'dfl':{
                cad=input.cont;
            }break;
            case 'tltwnd':{
                cad='';

                if ($objed==undefined) {
                    kedit='nuevo';
                }else{
                    kedit='editar';
                }
                document.getElementsByClassName('titlewindow')[0].innerHTML=kedit+" "+(input.vl==undefined?'':input.vl);
            }break;
            case 'time':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="time" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
        }
        if (input.type=='img'&&$objed==undefined
          ||input.type=='lnk'&&$objed==undefined&&input.type=='tltwnd'
          ||input.nonew!=undefined&&$objed==undefined) {
            dis=1;
        }
        tittle=(input.tl==undefined?'':input.tl);
        trfinal+=(dis==0?(input.type!='dfl'&&input.type!='tltwnd'?
         (input.type!='hddn'?'<div class="'+
         (input.nxt!=undefined?'col-lg-'+input.nxt+(input.tdinf!=undefined?input.tdinf:' form-group'):'col-lg-12 form-group')+'">'+
         (input.type==='chckbx'?'<div class="form-label">'+tittle+'</div>':'')+
         (input.type==='rd'||input.type==='dtmpckr'?'<label>'+tittle+'</label>':'')+
         (input.type==='chckbx'||input.type==='btn'||
          input.type==='rd'||input.type==='dtmpckr'?'':
         (stub ? '' : '<label class="float-left">'+tittle+':</label>')    )+
         (input.ttl!=undefined?input.ttl:''):'')+cad+(input.type!='hddn'?
         '</div>':''):'<div class="'+classe+'">'+cad+'</div>'):'');
        cad='';
        $vald='';
        addx='';
        ids='';
    }
    return trfinal;
}

//CODIGO,OBJETODEFORMULARIO,OBJETODEDATOS,url
function modifyfloat(codrx,formobj,objcont,targetref,addUrl,changeLog)
{
  // console.log(objcont);
    if (objcont!=undefined) {
    	var dta=objcont[codrx];
    	newfloatv2(formobj,dta,codrx,targetref,addUrl,changeLog);
    }else{
        alert("no funciona");
    }
}

function username(nme,codm)
{
    var palabras = nme.value.split(' ').length;
    var pala = nme.value.split(' '),useer="";
    switch(palabras){
        case 1: {
                useer+=pala[0].substr(0,1);
                usesr=useer;
        }break;
        case 2: {
            if(nme.value!=''){
                useer+=usesr+pala[1];
                carta=useer;
            }
        }break;
        case 3: {
            if(nme.value!=''){
                useer+=usesr+pala[2];
                carta=useer;
            }
        }break;
        default:{
            useer=carta;
        }break;
    }
    $('#userex').val(useer.toLowerCase());
    $('#passwx').val(nme);
}
function deleteD(code,token,urlx)
{
    console.log('test')
  let APP_URL = window.location;
  // console.log(urlx);

  if(urlx!=undefined&&isFinite(urlx))
  {
    // console.log(urlx.indexOf("https"));
    let altern = APP_URL.pathname.split( '/' );
    formurl='/';
    for(let il=1;il<=urlx;il++){
      formurl+=altern[il]+(il<urlx?'/':'');
    }
    url=window.location.origin+formurl+'/'+code;
    // console.log(url);
  }
  else if(urlx!=undefined&&typeof urlx === 'string'&&urlx.indexOf("http")>=0)
  {
        url=urlx;
  }
  else if(urlx!=undefined&&typeof urlx === 'string'){
    url=APP_URL.origin+APP_URL.pathname+"/"+urlx+"/"+code;
  }else{
    url=APP_URL.origin+APP_URL.pathname+"/"+code;
  }

  swal.fire({
      title: 'Confirma que desea borrar',
      text: "Esta operación no puede ser revertida",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00b8d9',
      cancelButtonColor: '#ef4a4a',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar'
  }).then((result) =>{
      if(result.value==true){
        $form = $('<form method="post" action="'+url+'"></form>');
        $form.append('<input type="hidden" name="_token" value="'+token+'">');
        $form.append('<input type="hidden" name="_method" value="DELETE">');
        $(document.body).append($form);

        $form.submit();
        loadingmig();
      }
  });
}
function vrv2(act,code,codeaux,actaux)
{
    var ves='',go='allow';
    $confe=act.substring(0, 5);
     ves=(code!=''&&code!=undefined)?'&cod='+code:'';
    ves+=(codeaux!=''&&codeaux!=undefined)?'&cdaux='+codeaux:'';
    ves+=(actaux!=''&&actaux!=undefined)?'&wh='+actaux.replace(' ', '+'):'';

        window.location='?target='+target+'&act='+act+ves;
}
function linkTo(link){
  window.location=link;
}
function locat(uere)
{
    window.location=uere;
}



function asignIcon(ic) {
    $("#imag").val(ic);
    $('.iconsdib').remove();
}
function getWord(numChars)
{
    var word = "",
        i;
    for (i = 0; i < numChars; i++)
        word += chars.charAt(Math.floor(Math.random() * chars.length));
    return word;
}

function getWords(numWords, numCharsPerWord)
{
    var words = [],
        i;
    for (i = 0; i < numWords; i++)
        words.push(getWord(numCharsPerWord));
    return words.join(" ");
}
function imgError(image) {
    image.onerror = "";
    image.src = "./images/not-available.jpg";
    return true;
}
function flotingblank(estu)
{
  let icons=new Array('i-Gears','i-Filter-2','i-Dashboard',
                      'i-Library','i-Support','i-Double-Tap','i-Bar-Code','i-Cash-register-2','i-Full-Cart',
                      'i-Receipt','i-Shop','i-Tag-3','i-Management','i-University1','i-Book',
                      'i-Add-User','i-Add-UserStar','i-Administrator','i-Business-Mens','i-Conference',
                      'i-ID-3','i-Paper-Plane','i-Suitcase','i-Check','i-Inbox-Out','i-University',
                      'i-Data-Center','i-Address-Book','i-Pie-Chart-2','i-Line-Chart','i-Bar-Chart','i-Newspaper','i-Monitor-5',);
  let $icons='';
  for(i=0;i<icons.length;i++){
    $icons+='<i class="text-20 '+icons[i]+'" style="margin-right:5px;" onclick="asignIcon(\''+icons[i]+'\');"></i>';
  }
  // console.log($icons);
  $(estu).parent().parent().parent().append('<div class="iconsdib" style="z-index: 99999;position: absolute;top: 0px;left: 0;">\
    <div class="row">\
      <div class="col-md-12">\
        <div class="card">\
          <div class="card-header">\
            <h4 class="card-title">Seleccionar icono</h4>\
            <div class="card-options">\
					     <a href="javascript:" class="card-options-remove" onclick="$(\'.iconsdib\').remove();" data-toggle="card-remove"><i class="fe fe-x"></i></a>\
						</div>\
          </div><div class="card-body">'
          +$icons+
            '</div></div>\
        </div>\
     </div>\
    </div>');
}


///-------------------Visualizador de datos
function viewFloat(id, elems, object)
{
    let body = viewGenerator(elems, object[id])
    swal.fire({title: '',
               html:`

                            <div class="modal-dialog" role="document" style="margin: auto; max-width:700px;">
                                <div class="modal-content" style="border-left:none; border-right: none; border-radius:0; margin:auto;">
                                    <div class="modal-body">
                                        <div class="row">
                                            ${body}
                                        </div>
                                    </div>
                                </div>
                            </div>
                    `,
              title:`<div class="modal-header" style="padding: 0; margin: auto; border:none;">
                        <h5 class="modal-title" id="verifyModalContent_title">Ver datos</h5>
                    </div>`,
              // showCloseButton: true,
              showCancelButton: true,
              showConfirmButton: true,
              customClass:{
                  title: 'sweet-alert-custom-header',
                  content: 'sweet-alert-custom-content',
                  confirmButton: 'btn btn-primary mr-5',
                  cancelButton: 'btn btn-secondary',
              },
              confirmButtonText: 'Ok',
              buttonsStyling: false,
              width: 600,
        })
        .then((result) => {});
}

function viewGenerator(elems, $objed)
{
    var cad='',trfinal='',cont=0;
    for (var ii in elems)
    {
        var input=elems[ii];
        var nxtet=elems[parseInt(ii)+1];
        var disable = 0;
        addx=(input.add!=undefined&&input.add!='')?input.add:'';
        ids=(input.id!=undefined&&input.id!='')?'id="'+input.id+'"':'';
        //console.log(nxtet.nxt);
        classe=(input.clss!=undefined&&input.clss!='')?input.clss:'';
        namess=(input.nm==undefined?'':'name="'+input.nm+'"');
        // console.log($objed);
        switch (input.type)
        {
            case 'label':{
                cad='<label class="col-form-label"></label>';
            }break;
            case 'text':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="text" '+namess+' '+ids+' '+$vald+' '+addx+' disabled/>';
            }break;
            case 'array':{
                let arr_input = $objed[input.elv];
                for (let i = 0; i < arr_input.length; i++){
                    $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+arr_input[i][input.elv2]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                    cad += '<div class="col-lg-10 offset-2 form-group"> <input class="form-control '+classe+'" type="text" '+namess+' '+ids+' '+$vald+' '+addx+' disabled/></div>';
                }
            }break;
            case 'color':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="color" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
            case 'number':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="number" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
            case 'textarea':{
                $vald=($objed!=undefined&&input.elv!=undefined)?($objed[input.elv]!=undefined?$objed[input.elv]:''):($objed==undefined&&input.vl!=undefined?input.vl:'');
                cad='<textarea class="form-control '+classe+'" '+namess+' '+ids+' '+addx+' >'+$vald+'</textarea>';
            }break;
            case 'password':{
                cad='<input class="form-control '+classe+'" type="password" '+namess+' '+ids+' '+addx+'/>';
            }break;
            case 'datetimepicker':{
              $vald='value="'+($objed!=undefined&&$objed[input.elv]!=undefined?$objed[input.elv]:input.vl)+'"';
                cad='<div class="input-group date">\
                      <div class="input-group-prepend">\
                        <div class="input-group-text br-tl-3 br-bl-3">\
                          <i class="fa fa-calendar"></i>\
                        </div>\
                      </div>\
                      <input type="date" class="form-control pull-right  '+classe+'" '+$vald+' '+namess+' '+ids+' '+addx+'>\
                    </div>';
            }break;
            case 'radio':{
                cad='<div class="custom-controls-stacked">';
                for (i=0;i<input.vl.length;i++) {
                    $vald=($objed!=undefined&&input.elv!=undefined&&$objed[input.elv]==input.vl[i][1])?'checked':'';
                    cad+='<label class="custom-control custom-radio">\
                    <input type="radio" '+namess+' id="'+ids[i]+'"\
                    value="'+input.vl[i][1]+'" '+$vald+" "+addx+'\
                    class="custom-control-input"/>\
                    <span class="custom-control-label">'+
                    input.vl[i][0]+(input.$brl!=undefined?'':
                    '</span></label>');
                }
                cad+='</div>';
            }break;
            case 'checkboxstyle':{
              $vald=($objed!=undefined&&input.elv!=undefined?
                $objed[input.elv]:input.vl[0][0]);
              check=($objed!=undefined&&input.vl!=undefined&&
                input.vl[0][0]==$objed[input.elv]?'checked="true"':'');
              cad='<label class=""><input type="checkbox" '+ids+' '+namess+
                    'value="'+$vald+'" '+check+' '+addx+'>'+input.vl[0][1]+
                    '</label>';
            }break;
            case 'checkbox':{
                cad='';
                //console.log(input.vl.length);
                for (i=0;i<input.vl.length;i++) {
                    if($objed!=undefined&&input.elv!=undefined){
                        for (jj=0;jj<($objed[input.elv].length);jj++) {
                            if($objed[input.elv][jj]==input.vl[i][0]){
                                $vald='checked="checked"';
                                break;
                            }else{
                                $vald='';
                            }
                        }
                    }
                    $nomb=(input.nm!=undefined &&(typeof input.nm=='array'))?input.nm[i]:input.nm;
                    $id=(ids!=undefined)?ids:'';
                    cad+='<label class="custom-checkbox custom-control">\
                              <input type="checkbox" name="'+$nomb+'" '+$id+'\
                                  class="custom-control-input"\
                                  value="'+input.vl[i][0]+'" '+addx+' '+$vald+'/>\
                            <span class="custom-control-label" style="float:left;">\
                              '+input.vl[i][1]+'\
                            </span>\
                         </label>';
                }
            }break;
            case 'select':{
                cad='<select '+namess+' '+ids+' '+addx+' class="form-control">';
                if (input.vl!==undefined) {
                    for (i=0;i<input.vl.length;i++)
                    {
                        //for (jj=0;jj<($objed[input.elv].length);jj++) {
                        $vald=($objed!=undefined&&input.elv!=undefined&&$objed[input.elv]==input.vl[i][0])?'selected':'';
                        //}
                        cad+=(input.vl[i][0]=='optgrupolb')?'<optgroup label="'+input.vl[i][1]+'">':
                        '<option value="'+input.vl[i][0]+'" '+$vald+'>'+input.vl[i][1]+'</option>';
                    }
                }
                cad+='</select>';
            }break;
            case 'img':{
                $vald=($objed!=undefined&&input.elv!=undefined)?$objed[input.elv]:'';
                cad='<br><img src="'+$vald+'" '+ids+' '+addx+' class="d-block w-30 rounded rounded '+classe+'"/>';
            }break;
            case 'link':{
                $vald=($objed!=undefined&&input.elv!=undefined)?$objed[input.elv]:'';
                $filne=($objed!=undefined&&input.fln!=undefined)?$objed[input.fln]:
                    (input.altname!=undefined)?input.altname:'';
                cad='<a href="'+$vald+'" '+ids+' '+addx+' class="'+classe+'">'+$filne+'</a>';
            }break;
            case 'time':{
                $vald=($objed!=undefined&&input.elv!=undefined)?'value="'+$objed[input.elv]+'"':($objed==undefined&&input.vl!=undefined?'value="'+input.vl+'"':'');
                cad='<input class="form-control '+classe+'" type="time" '+namess+' '+ids+' '+$vald+' '+addx+'/>';
            }break;
        }
        if (input.type == 'img' && $objed == undefined
          || input.type == 'lnk' && $objed == undefined && input.type == 'tltwnd'
          || input.nonew != undefined && $objed == undefined) {
            disable = 1;
        }

        title = (input.tl == undefined ? '' : input.tl + ':');

        if (disable == 0){
            if (input.type != 'dfl' && input.type != 'tltwnd') {
                let classes = 'col-lg-12 form-group'
                if (input.nxt != undefined){
                    classes = 'col-lg-' + input.nxt + ' form-group';
                }
                if (input.offset != undefined){
                    classes += ' offset-' + input.offset;
                }
                trfinal += '<div class="' + classes + '">' +
                (input.type === 'checkbox' ? '<div class="form-label">' + title + '</div>' : '') +
                (input.type === 'radio' || input.type === 'datetimepicker' ? '<label>' + title + '</label>' : '') +
                (input.type === 'checkbox' || input.type === 'radio' || input.type === 'datetimepicker' ? '' : '<label class="float-left">' + title + '</label>') +
                (input.ttl != undefined ? input.ttl : '') + cad + '</div>';
            }else{
                trfinal += '<div class="' + classe + '">' + cad + '</div>';
            }
        }

        cad='';
        $vald='';
        addx='';
        ids='';
    }
    return trfinal;
}
